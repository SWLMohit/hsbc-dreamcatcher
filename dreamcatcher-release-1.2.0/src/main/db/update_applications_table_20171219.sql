ALTER TABLE `applications` CHANGE existing_life_insurance_coverage tempcoverage int(10);
ALTER TABLE `applications` ADD existing_life_insurance_coverage float unsigned DEFAULT NULL;
UPDATE `applications` SET existing_life_insurance_coverage=tempcoverage;
ALTER TABLE `applications` DROP tempcoverage;