/*
  HSBCIDCU-1057, HSBCIDCU-1013
  Item 1
*/
select * from faq where question like 'How do I get advice on which product is suitable for me%';

update faq set answer = '<p>We do not offer any financial advice, however we encourage you to use our calculators available under <a href=\"/your-dreams/\">\"Your Dreams\"</a> to find out if you are on track to achieving your financial aspirations.</p>' where question like 'How do I get advice on which product is suitable for me%' limit 1;
