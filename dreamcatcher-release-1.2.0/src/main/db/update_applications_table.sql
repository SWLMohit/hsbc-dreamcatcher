
ALTER TABLE `applications`
  ADD COLUMN `product_code` varchar(10) DEFAULT NULL,
  ADD COLUMN `term_duration` varchar(10) DEFAULT NULL,
  ADD COLUMN `term_years` int(10) unsigned DEFAULT NULL,
  ADD COLUMN `existing_life_insurance_coverage` int(10) unsigned DEFAULT NULL;