/*
  HSBCIDCU-1018 (Previously resolved via HSBCIDCU-285)

  FAQ: Item 19
*/

select * from faq where question like 'How can I pay for my future premiums%';

update faq set answer = '<p>Your future premiums will automatically be deducted from the credit card used at the time of purchase. However, to facilitate your recurring premiums, you may set up an Interbank GIRO payment using your local bank account or your credit card account. </p><p>Click <a href=\"../assets/forms/Inter-Bank_GIRO_form.pdf\" style=\"text-decoration:underline;\">here</a> to download, the Interbank GIRO and Credit Card Authorisation Form.</p><p>The completed Interbank GIRO and Credit Card Authorisation Form together with documentary proof (where applicable) is to be mailed to:</p><p>HSBC Insurance (Singapore) Pte Ltd<br>Robinson Road Post Office<br>P.O. Box 1538<br>Singapore 903038</p><p>Alternatively, you may visit our Customer Service Centre to complete the form.</p>' where question like 'How can I pay for my future premiums%' limit 1;
