ALTER TABLE applications
MODIFY COLUMN uw_lifestyle_height float unsigned;

ALTER TABLE applications
MODIFY COLUMN uw_lifestyle_weight float unsigned;