<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- MODAL C4 & B2-->


<div class="modal fade modal-retrieve" id="modalformnotfoundsimple" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header error">
                <h2>Form not found</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>We are unable to retrieve your&nbsp;form.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <c:choose>
                    <c:when test="${param.applicationType.equals('online-protector')}">
                        <a class="primary-btn-slate" href="/our-plans/online-protector/#quick_quote">Restart your application</a>
                    </c:when>    
                    <c:otherwise>
                        <a class="primary-btn-slate" href="/our-plans/direct-value-term/#quick_quote">Restart your application</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
