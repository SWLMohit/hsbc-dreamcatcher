<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="row">
    <div class="col-xs-12 col-sm-10">
        <p class="intro">To receive a new verification code for your completed form, please enter your email address and mobile number.</p>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-8">
    
        <jsp:include page="/WEB-INF/ourplans/includes/applicant_email.jsp"></jsp:include>

        <jsp:include page="/WEB-INF/ourplans/includes/applicant_mobile.jsp"></jsp:include>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-8">
        <div class="secondary-buttons">
            <span><a href="/ourplans" onclick="window.history.back();return false;"><i class="icon icon-chevron-left"></i> Back</a></span>
        </div>

        <div class="primary-button">
            <div>
                <a class="btn btn-red" href="#" data-form-action="submit">Continue</a>
            </div>
        </div>
    </div>
</div>           