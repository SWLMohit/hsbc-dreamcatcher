<%--
  Created by IntelliJ IDEA.
  User: doppel
  Date: 22/11/2017
  Time: 8:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!-- Tracking tag -->
<script type="text/javascript">
	var hastag = location.hash;
	if (hastag === '#quick_quote') {
		value = 'QuickQuote';
	} else {
		value = 'Detail';
	}
    var utag_data = {
        "page_name"     : "DC:OurPlans:InsuranceOnlineProtector:"+value,
        "page_url"      : window.location.href,
        "page_type"     : "Product "+value,
        "page_language" : "en",
        "page_security_level"   :   "0",
        "page_business_line"   :   "Insurance",
        "page_customer_group"   :   "General"
    };
</script>
<!-- /Tracking tag -->