<%--
  Created by IntelliJ IDEA.
  User: keilin.olsen@heathwallace.com
  Date: 17/11/2017
  Time: 5:42 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="row">
  <div class="col-xs-12 col-md-5 control_singapore"><label for="form-firstname">Given Name <small>(as appears on <span data-dynamic-label="applicant-nric-passport-label">NRIC/Passport</span>)</small></label></div>
  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(requestScope.errors.containsKey('applicant_first_name')) ? 'error' : ''}"></c:out>" data-field-name="applicant_first_name">
    <input name="applicant_first_name" id="form-firstname" class="form-control" type="text" value="${fn:escapeXml(requestScope.data['applicant_first_name'])}" maxlength="50" aria-describedby="applicantName">
    <c:if test="${requestScope.errors.containsKey('applicant_first_name')}">
        <span class="error-message" aria-label="Error" id="applicantName">
            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            ${requestScope.errors['applicant_first_name']}
        </span>
    </c:if>
  </div>
</div>
