<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- MODAL E4 & D4-->

<div class="modal fade modal-retrieve" id="modalformnotfound" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header error">
                <h2>Form not found</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>We are unable to retrieve your completed form as your details do not match our records. Please email us your details to <span class="no-wrap"><a href="mailto:e-surance@hsbc.com.sg">e-surance@hsbc.com.sg</a></span> and our customer service will contact you within 4 business days.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <a class="primary-btn-slate" href="mailto:e-surance@hsbc.com.sg">Email us</a>
            </div>
        </div>
    </div>
</div>
