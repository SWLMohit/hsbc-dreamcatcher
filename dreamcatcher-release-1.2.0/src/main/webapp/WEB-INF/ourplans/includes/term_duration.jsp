<%--
  Created by IntelliJ IDEA.
  User: keilin.olsen@heathwallace.com
  Date: 20/11/2017
  Time: 9:35 PM
  To change this template use File | Settings | File Templates.

  param: quickQuote : boolean
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="row">
    <div class="col-xs-10">
        <p><label for="form-policyterm" id="form-policyterm-label">Policy Term</label></p>
        <div class="form-group <c:out value="${(requestScope.errors.containsKey('term_duration')) ? 'error' : ''}"></c:out>" data-field-name="term_duration">
            <div class="form-select_wrap">
                <select name="term_duration" class="form-control" id="form-policyterm" aria-describedby="policyTermDuration">
                    <option value="">Please select a policy term</option>
                    <option value="5Y" <c:if test="${requestScope.data['term_duration'] == '5Y'}">selected</c:if>>5 Years</option>
                    <option value="20Y" <c:if test="${requestScope.data['term_duration'] == '20Y'}">selected</c:if>>20 Years</option>
                    <option value="@65" <c:if test="${requestScope.data['term_duration'] == '@65'}">selected</c:if>>Up to age 65</option>
                </select>
            </div>
            <c:if test="${requestScope.errors.containsKey('term_duration') || param.quickQuote}">
                <div class="error-message" data-field-name="term_duration" aria-label="Error" id="policyTermDuration">
                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                    <span>
                        <c:out value="${requestScope.errors['term_duration']}"></c:out>
                    </span>
                </div>
            </c:if>
        </div>
    </div>
</div>
