<%--
  Created by IntelliJ IDEA.
  User: keilin.olsen@heathwallace.com
  Date: 17/11/2017
  Time: 5:59 PM

  NOTE: includes both country code and mobile num
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="row">
  <fieldset>
    <legend class="col-xs-12 col-md-5">Mobile number</legend>
    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
      <div class="form-group__multi form-group__multi--phone">
          <span><label for="form-mobilecountrycode" class="visuallyhidden">Country code</label>
                <input name="applicant_mobile_country_code" type="text" id="form-mobilecountrycode" 
                       class="form-control <c:out value="${(requestScope.errors.containsKey('applicant_mobile_country_code')) ? 'error' : ''}"></c:out>"
                       value="${not empty data['applicant_mobile_country_code'] ? fn:escapeXml(requestScope.data['applicant_mobile_country_code']) :'+65'}" data-field-name="applicant_mobile_country_code" aria-describedby="applicantCcode"></span>
          <span><label for="form-mobilenumber" class="visuallyhidden">Number</label>
                <input name="applicant_mobile_num" id="form-mobilenumber" type="text"
                       class="form-control <c:out value="${(requestScope.errors.containsKey('applicant_mobile_num')) ? 'error' : ''}"></c:out>"
                       value="${fn:escapeXml(requestScope.data['applicant_mobile_num'])}" data-field-name="applicant_mobile_num" aria-describedby="applicantNumber"></span>
      </div>
      <c:if test="${requestScope.errors.containsKey('applicant_mobile_country_code')}">
        <div>
            <span class="error-message" aria-label="Error" id="applicantCcode">
                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                <c:out value="${requestScope.errors['applicant_mobile_country_code']}"></c:out>
            </span>
        </div>
    </c:if>
    <c:if test="${requestScope.errors.containsKey('applicant_mobile_num')}">
        <div>
            <span class="error-message" aria-label="Error" id="applicantNumber">
                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                <c:out value="${requestScope.errors['applicant_mobile_num']}"></c:out>
            </span>
        </div>
    </c:if>
    </div>
  </fieldset>
</div>
