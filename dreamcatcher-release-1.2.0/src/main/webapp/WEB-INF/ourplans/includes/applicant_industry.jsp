<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<option value="">Please select...</option>
<option <c:if test="${data[param.key] == 'Banking'}">selected</c:if>>Banking</option>
<option <c:if test="${data[param.key] == 'Financial Services'}">selected</c:if>>Financial Services</option>
<option <c:if test="${data[param.key] == 'Insurance'}">selected</c:if>>Insurance</option>
<option <c:if test="${data[param.key] == 'Casino & Gambling'}">selected</c:if>>Casino & Gambling</option>
<option <c:if test="${data[param.key] == 'Charity/Nonprofit organisation'}">selected</c:if>>Charity/Nonprofit organisation</option>
<option <c:if test="${data[param.key] == 'Construction & Engineering'}">selected</c:if>>Construction & Engineering</option>
<option <c:if test="${data[param.key] == 'Defence'}">selected</c:if>>Defence</option>
<option <c:if test="${data[param.key] == 'Government/State-Owned bodies'}">selected</c:if>>Government/State-Owned bodies</option>
<option <c:if test="${data[param.key] == 'Hotel/Restaurant/Food Catering'}">selected</c:if>>Hotel/Restaurant/Food Catering</option>
<option <c:if test="${data[param.key] == 'Manufacturing/Whole saler/Retail'}">selected</c:if>>Manufacturing/Whole saler/Retail</option>
<option <c:if test="${data[param.key] == 'Medical & Health Services'}">selected</c:if>>Medical & Health Services</option>
<option <c:if test="${data[param.key] == 'Military product'}">selected</c:if>>Military product</option>
<option <c:if test="${data[param.key] == 'Military production distributors'}">selected</c:if>>Military production distributors</option>
<option <c:if test="${data[param.key] == 'Money Service Business'}">selected</c:if>>Money Service Business</option>
<option <c:if test="${data[param.key] == 'Money Changer'}">selected</c:if>>Money Changer</option>
<option <c:if test="${data[param.key] == 'Money Remittance'}">selected</c:if>>Money Remittance</option>
<option <c:if test="${data[param.key] == 'Oil & Gas/Petroleum/Oil Rigs'}">selected</c:if>>Oil & Gas/Petroleum/Oil Rigs</option>
<option <c:if test="${data[param.key] == 'Professional'}">selected</c:if>>Professional</option>
<option <c:if test="${data[param.key] == 'Legal'}">selected</c:if>>Legal</option>
<option <c:if test="${data[param.key] == 'Accountants'}">selected</c:if>>Accountants</option>
<option <c:if test="${data[param.key] == 'Consultants'}">selected</c:if>>Consultants</option>
<option <c:if test="${data[param.key] == 'Sales'}">selected</c:if>>Sales </option>
<option <c:if test="${data[param.key] == 'Others'}">selected</c:if>>Others</option>