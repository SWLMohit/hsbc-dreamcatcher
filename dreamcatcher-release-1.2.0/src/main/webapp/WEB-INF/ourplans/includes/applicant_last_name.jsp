<%--
  Created by IntelliJ IDEA.
  User: keilin.olsen@heathwallace.com
  Date: 17/11/2017
  Time: 5:42 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="row">
  <div class="col-xs-12 col-md-5 control_singapore"><label for="form-firstname">Surname <small>(as appears on <span data-dynamic-label="applicant-nric-passport-label">NRIC/Passport</span>)</small></label></div>
  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(requestScope.errors.containsKey('applicant_last_name')) ? 'error' : ''}"></c:out>" data-field-name="applicant_last_name">
    <input name="applicant_last_name" id="form-firstname" class="form-control" type="text" value="${fn:escapeXml(requestScope.data['applicant_last_name'])}" maxlength="50" aria-describedby="applicantSurname">
    <c:if test="${requestScope.errors.containsKey('applicant_last_name')}">
        <span class="error-message" aria-label="Error" id="applicantSurname">
            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            <c:out value="${requestScope.errors['applicant_last_name']}"></c:out>
        </span>
    </c:if>
  </div>
</div>
