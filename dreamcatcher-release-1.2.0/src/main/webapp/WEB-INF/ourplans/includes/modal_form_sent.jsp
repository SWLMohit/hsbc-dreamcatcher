<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!-- MODAL D6 -->


<div class="modal fade modal-retrieve" id="modalformsent" tabindex="-1" role="dialog" aria-modal="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header confirm">
                <h2>Your completed form has been&nbsp;sent</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>We have resent your form to your email <span class="no-wrap">${fn:escapeXml(data['applicant_email'])}</span>, and your verification code via SMS to your mobile number <span class="no-wrap">${fn:escapeXml(data['applicant_mobile_country_code'])} ${fn:escapeXml(data['applicant_mobile_num'])}.</span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <a class="primary-btn-slate" href="/">Go to homepage</a>
            </div>
        </div>
    </div>
</div>
