<%--
  Created by IntelliJ IDEA.
  User: keilin.olsen@heathwallace.com
  Date: 17/11/2017
  Time: 6:05 PM

--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
    <c:when test="${param.formpage}">
        <div class="row">
          <fieldset>
            <legend class="col-xs-12 col-md-5">Gender</legend>
    </c:when>    
    <c:otherwise>
        <fieldset>
          <legend><dt>Gender</dt></legend>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${param.formpage}">
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(requestScope.errors.containsKey('applicant_gender')) ? 'error' : ''}"></c:out>" data-field-name="applicant_gender">
    </c:when>    
    <c:otherwise>
        <dd class="form-group <c:out value="${(requestScope.errors.containsKey('applicant_gender')) ? 'error' : ''}"></c:out>" data-field-name="applicant_gender">
    </c:otherwise>
</c:choose>


      <div class="newradio--inline">
          <input id="id_gender_option1" type="radio" name="applicant_gender" value="M" <c:if test="${requestScope.data['applicant_gender'] == 'M'}">checked</c:if>>
          <label for="id_gender_option1"><span><span></span></span>Male</label>
      </div>
      <div class="newradio--inline">
          <input id="id_gender_option2" type="radio" name="applicant_gender" value="F" <c:if test="${requestScope.data['applicant_gender'] == 'F'}">checked</c:if> aria-describedby="applicantGender">
          <label for="id_gender_option2"><span><span></span></span>Female</label>
      </div>
      <c:if test="${requestScope.errors.containsKey('applicant_gender') || param.quickQuote}">
          <div class="error-message" data-field-name="applicant_gender" aria-label="Error" id="applicantGender">
            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            <span><c:out value="${requestScope.errors['applicant_gender']}"></c:out></span>
          </div>
      </c:if>

<c:choose>
    <c:when test="${param.formpage}">
        </div>
        </fieldset>
        </div>
    </c:when>    
    <c:otherwise>
        </dd>
        </fieldset>
    </c:otherwise>
</c:choose>


