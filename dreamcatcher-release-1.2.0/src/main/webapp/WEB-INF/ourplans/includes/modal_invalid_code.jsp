<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- MODAL B1 -->

<div class="modal fade modal-retrieve" id="modalinvalidcode" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header error">
                <h2>Invalid code</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Please check your verification code or request for a new&nbsp;one.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <c:choose>
                    <c:when test="${param.applicationType.equals('online-protector')}">
                        <a class="primary-btn-slate" href="/our-plans/online-protector/retrieve-verification-code/">Request for new verification code</a>
                    </c:when>    
                    <c:otherwise>
                        <a class="primary-btn-slate" href="/our-plans/direct-value-term/retrieve-verification-code/">Request for new verification code</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
