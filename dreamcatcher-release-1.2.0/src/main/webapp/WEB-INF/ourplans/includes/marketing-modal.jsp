<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="modal fade" id="marketingmodal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4>Are you sure you want to opt out of receiving HSBC news and alerts on new products and special promotions offered by HSBC Insurance and its group companies?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="marketingmodal-yes btn btn-white" data-dismiss="modal">Yes</button>
                <button type="button" class="marketingmodal-no btn btn-red" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
