<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="row">
    <div class="col-xs-12 col-sm-10">
        <p class="intro">To continue your form, please enter your email address followed by the 8-digit verification code sent to your mobile number via SMS.</p>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-8">
        <jsp:include page="/WEB-INF/ourplans/includes/applicant_email.jsp"></jsp:include>

        <div class="row">
            <div class="col-xs-12 col-md-5">Verification code</div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('verify_code')) ? 'error' : ''}"></c:out>" data-field-name="verify_code">
                <input name="verify_code" type="text" class="form-control">
                <c:if test="${requestScope.errors.containsKey('verify_code')}">
                    <span class="error-message">
                        <i class="icon icon-circle-delete"></i>
                        <c:out value="${requestScope.errors['verify_code']}"></c:out>
                    </span>
                </c:if>
            </div>
        </div>
    </div>
</div>

<div class="row">
                        
    <div class="col-xs-12 col-sm-9 col-md-8">
        <div class="secondary-buttons">
            <span><a href="/our-plans/<c:out value='${param.applicationType}'></c:out>/retrieve-verification-code/">Resend verification code<i class="icon icon-chevron-right"></i></a></span> <br />
            <span><a href="/our-plans/" onclick="window.history.back();return false;"><i class="icon icon-chevron-left"></i> Back</a></span>
        </div>

        <div class="primary-button">
            <div>
                <a class="btn btn-red" href="#" data-form-action="submit">Continue</a>
            </div>
        </div>
    </div>
</div>     