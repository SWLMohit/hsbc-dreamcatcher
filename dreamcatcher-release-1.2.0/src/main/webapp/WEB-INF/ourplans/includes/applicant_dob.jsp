<%--
  Created by IntelliJ IDEA.
  User: keilin.olsen@heathwallace.com
  Date: 17/11/2017
  Time: 5:57 PM

    param: quickQuote : boolean
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" import="sg.com.hsbc.dreamcatcher.helpers.FieldConstants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
    <c:when test="${param.formpage}">
        <div class="row">
            <div class="col-xs-12 col-md-5"><label for="date">Date of birth</label></div>
    </c:when>    
    <c:otherwise>
        <dt><label for="date">Date of birth</label></dt>
    </c:otherwise>
</c:choose>


<c:choose>
    <c:when test="${param.formpage}">
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(requestScope.errors.containsKey(FieldConstants.APPLICANT_DOB)) ? 'error' : ''}"></c:out>" data-field-name="${FieldConstants.APPLICANT_DOB}">
    </c:when>    
    <c:otherwise>
        <dd class="form-group <c:out value="${(requestScope.errors.containsKey(FieldConstants.APPLICANT_DOB)) ? 'error' : ''}"></c:out>" data-field-name="${FieldConstants.APPLICANT_DOB}">
    </c:otherwise>
</c:choose>


    <input id="date" type="text" name="${FieldConstants.APPLICANT_DOB}" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-dob" 
           value="${fn:escapeXml(requestScope.data[FieldConstants.APPLICANT_DOB])}" aria-describedby="applicantDob">
    <c:if test="${requestScope.errors.containsKey(FieldConstants.APPLICANT_DOB) || param.quickQuote}">
        <span class="error-message" data-field-name="${FieldConstants.APPLICANT_DOB}" aria-label="Error" id="applicantDob">
            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            <span>
                <c:out value="${requestScope.errors[FieldConstants.APPLICANT_DOB]}"></c:out>
            </span>
        </span>
    </c:if>

<c:choose>
    <c:when test="${param.formpage}">
        </div>
        </div>
    </c:when>    
    <c:otherwise>
        </dd>
    </c:otherwise>
</c:choose>
