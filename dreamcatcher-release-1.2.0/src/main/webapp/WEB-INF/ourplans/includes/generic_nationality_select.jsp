<%--
  Created by IntelliJ IDEA.
  User: doppel
  Date: 17/11/2017
  Time: 10:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="row" <c:if test="${param.hidden != null}">data-question-group="multiple_nationalities"</c:if> >

    <div class="col-xs-12 col-md-5" ><label for="${fn:escapeXml(param.key)}" id="${fn:escapeXml(param.key)}_label" >${fn:escapeXml(param.label)}</label></div>
    <div <c:if test="${param.hidden != null}">data-question-group="multiple_nationalities"</c:if> class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(requestScope.errors.containsKey(param.key)) ? 'error' : ''}"></c:out>" data-field-name="${fn:escapeXml(param.key)}">
        <div class="form-select_wrap">
            <select name="${fn:escapeXml(param.key)}" id="${fn:escapeXml(param.key)}" class="form-control" aria-describedby="aria_${fn:escapeXml(param.key)}">
                <option value="">Please select...</option>
                <option <c:if test="${requestScope.data[param.key] == 'Singapore'}">selected</c:if>>Singapore</option>
                <option <c:if test="${requestScope.data[param.key] == 'Australia'}">selected</c:if>>Australia</option>
                <option <c:if test="${requestScope.data[param.key] == 'Austria'}">selected</c:if>>Austria</option>
                <option <c:if test="${requestScope.data[param.key] == 'Bahrain'}">selected</c:if>>Bahrain</option>
                <option <c:if test="${requestScope.data[param.key] == 'Belgium'}">selected</c:if>>Belgium</option>
                <option <c:if test="${requestScope.data[param.key] == 'Bermuda'}">selected</c:if>>Bermuda</option>
                <option <c:if test="${requestScope.data[param.key] == 'Brunei'}">selected</c:if>>Brunei</option>
                <option <c:if test="${requestScope.data[param.key] == 'Canada'}">selected</c:if>>Canada</option><%--JIRA HSBCIDCU-457 --%>
                <option <c:if test="${requestScope.data[param.key] == 'China'}">selected</c:if>>China</option>
                <option <c:if test="${requestScope.data[param.key] == 'Denmark'}">selected</c:if>>Denmark</option>
                <option <c:if test="${requestScope.data[param.key] == 'Finland'}">selected</c:if>>Finland</option>
                <option <c:if test="${requestScope.data[param.key] == 'France'}">selected</c:if>>France</option>
                <option <c:if test="${requestScope.data[param.key] == 'Germany'}">selected</c:if>>Germany</option>
                <option <c:if test="${requestScope.data[param.key] == 'Hong Kong'}">selected</c:if>>Hong Kong</option>
                <option <c:if test="${requestScope.data[param.key] == 'India'}">selected</c:if>>India</option>
                <option <c:if test="${requestScope.data[param.key] == 'Indonesia'}">selected</c:if>>Indonesia</option>
                <option <c:if test="${requestScope.data[param.key] == 'Ireland'}">selected</c:if>>Ireland</option>
                <option <c:if test="${requestScope.data[param.key] == 'Italy'}">selected</c:if>>Italy</option>
                <option <c:if test="${requestScope.data[param.key] == 'Japan'}">selected</c:if>>Japan</option>
                <option <c:if test="${requestScope.data[param.key] == 'Liechtenstein'}">selected</c:if>>Liechtenstein</option>
                <option <c:if test="${requestScope.data[param.key] == 'Luxembourg'}">selected</c:if>>Luxembourg</option>
                <option <c:if test="${requestScope.data[param.key] == 'Macau'}">selected</c:if>>Macau</option><%--JIRA HSBCIDCU-457 --%>
                <option <c:if test="${requestScope.data[param.key] == 'Malaysia'}">selected</c:if>>Malaysia</option>
                <option <c:if test="${requestScope.data[param.key] == 'Monaco'}">selected</c:if>>Monaco</option><%--JIRA HSBCIDCU-457 --%>
                <option <c:if test="${requestScope.data[param.key] == 'Netherlands'}">selected</c:if>>Netherlands</option>
                <option <c:if test="${requestScope.data[param.key] == 'New Zealand'}">selected</c:if>>New Zealand</option>
                <option <c:if test="${requestScope.data[param.key] == 'Norway'}">selected</c:if>>Norway</option>
                <option <c:if test="${requestScope.data[param.key] == 'Philippines'}">selected</c:if>>Philippines</option>
                <option <c:if test="${requestScope.data[param.key] == 'South Korea'}">selected</c:if>>South Korea</option>
                <option <c:if test="${requestScope.data[param.key] == 'Spain'}">selected</c:if>>Spain</option>
                <option <c:if test="${requestScope.data[param.key] == 'Sweden'}">selected</c:if>>Sweden</option>
                <option <c:if test="${requestScope.data[param.key] == 'Switzerland'}">selected</c:if>>Switzerland</option>
                <option <c:if test="${requestScope.data[param.key] == 'Taiwan'}">selected</c:if>>Taiwan</option>
                <option <c:if test="${requestScope.data[param.key] == 'Thailand'}">selected</c:if>>Thailand</option>
                <option <c:if test="${requestScope.data[param.key] == 'United Arab Emirates'}">selected</c:if>>United Arab Emirates</option>
                <option <c:if test="${requestScope.data[param.key] == 'United Kingdom'}">selected</c:if>>United Kingdom</option>
                <option <c:if test="${requestScope.data[param.key] == 'United States of America'}">selected</c:if>>United States of America</option><%--JIRA HSBCIDCU-457 --%>
                <option <c:if test="${requestScope.data[param.key] == 'Vatican City '}">selected</c:if>>Vatican City </option>
                <option <c:if test="${requestScope.data[param.key] == 'Vietnam'}">selected</c:if>>Vietnam</option>
                <option value="NA" <c:if test="${requestScope.data[param.key] == 'NA'}">selected</c:if>>None of the above</option>
            </select>
        </div>
        <c:if test="${requestScope.errors.containsKey(param.key)}">
            <span class="error-message" aria-label="Error" id="aria_${fn:escapeXml(param.key)}">
                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                <c:out value="${requestScope.errors[param.key]}"></c:out>
            </span>
        </c:if>
    </div>
    
</div>
