<%--
  Created by IntelliJ IDEA.
  User: keilin.olsen@heathwallace.com
  Date: 17/11/2017
  Time: 5:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="row">
  <div class="col-xs-12 col-md-5"><label for="form-residencystatus" id="form-residencystatus-label">What is your current status of residency in Singapore?</label></div>
  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(requestScope.errors.containsKey('applicant_residency')) ? 'error' : ''}"></c:out>" data-field-name="applicant_residency">
    <div class="form-select_wrap">
        <select name="applicant_residency" id="form-residencystatus" class="form-control" aria-describedby="applicantResidency">
            <option value="">Please select...</option>
            <option <c:if test="${requestScope.data['applicant_residency'] == 'Singaporean'}">selected</c:if>>Singaporean</option>
            <option <c:if test="${requestScope.data['applicant_residency'] == 'Singapore PR'}">selected</c:if>>Singapore PR</option>
            <option <c:if test="${requestScope.data['applicant_residency'] == 'Employment Pass'}">selected</c:if>>Employment Pass</option>
            <option <c:if test="${requestScope.data['applicant_residency'] == 'Skilled Pass'}">selected</c:if>>Skilled Pass</option>
            <option <c:if test="${requestScope.data['applicant_residency'] == 'Personalised Employment Pass'}">selected</c:if>>Personalised Employment Pass</option>
            <option <c:if test="${requestScope.data['applicant_residency'] == 'Dependent Pass'}">selected</c:if>>Dependent Pass</option>
            <option <c:if test="${requestScope.data['applicant_residency'] == 'Student Pass'}">selected</c:if>>Student Pass</option>
            <option value="NA" <c:if test="${requestScope.data['applicant_residency'] == 'NA'}">selected</c:if>>None of the above</option>
        </select>
    </div>
    <c:if test="${requestScope.errors.containsKey('applicant_residency')}">
        <span class="error-message" aria-label="Error" id="applicantResidency">
            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            <c:out value="${requestScope.errors['applicant_residency']}"></c:out>
        </span>
    </c:if>
  </div>
</div>
