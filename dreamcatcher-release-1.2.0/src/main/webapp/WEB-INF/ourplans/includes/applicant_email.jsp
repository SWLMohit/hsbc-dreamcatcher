<%--
  Created by IntelliJ IDEA.
  User: keilin.olsen@heathwallace.com
  Date: 17/11/2017
  Time: 6:05 PM

  NOTE: includes both country code and mobile num
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="row">
  <div class="col-xs-12 col-md-5"><label for="form-email">Email address</label></div>
  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(requestScope.errors.containsKey('applicant_email')) ? 'error' : ''}"></c:out>" data-field-name="applicant_email">
    <input name="applicant_email" id="form-email" type="email" class="form-control" value="${fn:escapeXml(requestScope.data['applicant_email'])}" maxlength="255" aria-describedby="applicantEmail">
    <c:if test="${requestScope.errors.containsKey('applicant_email')}">
        <span class="error-message" aria-label="Error" id="applicantEmail">
            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            <c:out value="${requestScope.errors['applicant_email']}"></c:out>
        </span>
    </c:if>
  </div>
</div>
