<%--
  Created by IntelliJ IDEA.
  User: doppel
  Date: 17/11/2017
  Time: 10:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" errorPage="showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fieldset>
  <legend><dt>Has the applicant smoked in the last 12 months?</dt></legend>
  <dd class="form-group" data-field-name="applicant_smoker">
      <div class="newradio--inline">
          <input id="id_applicant_smoker_option1" type="radio" name="applicant_smoker" value="Y">
          <label for="id_applicant_smoker_option1"><span><span></span></span>Yes</label>
      </div>
      <div class="newradio--inline">
          <input id="id_applicant_smoker_option2" type="radio" name="applicant_smoker" value="N" aria-describedby="applicantSmoker">
          <label for="id_applicant_smoker_option2"><span><span></span></span>No</label>
      </div>
      <div class="error-message" data-field-name="applicant_smoker" aria-label="Error" id="applicantSmoker">
        <i class="icon icon-circle-delete" aria-hidden="true"></i>
        <span></span>
      </div>
  </dd>
</fieldset>
