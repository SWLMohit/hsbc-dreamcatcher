<%@ page contentType="text/html; charset=UTF-8"%>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Our Plans"></jsp:param>
    <jsp:param name="metadataKeywords" value="life insurance, online insurance plans, compare insurance online"></jsp:param>
	<jsp:param name="metadataDescription" value="Compare and choose the online life insurance plan that best protects you and your loved ones. Click here get a quote now."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/our-plans/"></jsp:param>
</jsp:include>

<section class="container-fluid">
    <div class="header-page header-our-plan" data-event="our-plans">
        <div class="container">
            <div class="featured">
                <h1>Our Online Insurance Plans</h1>
                <p>Taking time to plan ahead can be hard. <br>Fortunately, getting the right protection online is not. <br>Now's Good.</p>

            </div>
        </div>
    </div>
</section>

<!--Life Insurance-->
<section class="container life-insurance-container">
    <h2>Life insurance plans to protect your loved ones</h2>
    <div class="row">
        <!--START: 1 Online Protector-->
        <div class="col-sm-6">

            <h3>HSBC Insurance OnlineProtector</h3>
            <p>Here's a simple and convenient term life insurance plan that gives you guaranteed protection up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote: </span>1</sup></a> 90 with guaranteed renewable every 10 years, safeguarding your family's future.</p>
            <!--START: Get a quote & Retrieve your form cta-->
            <div class="row">
                <div class="btn-left">
                    <a href="/our-plans/online-protector/#quick_quote" class="secondary-btn-slate" data-event="quote-retireve" data-name="Online Protector">Get a quote</a>
                </div>
                <div class="btn-right">
                    <a href="/our-plans/online-protector/retrieve/" class="outline-btn-slate" data-event="retrieve-your-form" data-name="HSBC Insurance OnlineProtector">Retrieve your form</a></div>
            </div>
            <!--END: Get a quote & Retrieve your form cta-->
            <!-- <a href="/ourplans/onlineprotector#quick_quote" class="secondary-btn-slate">Get a quote</a>
            <a href="/our-plans/online-protector/retrieve/" class="outline-btn-slate">Retrieve your form</a> -->

            <div class="more-generic-text hidden-lg hidden-md hidden-sm">
                <a class="btn btn-primary collapsed" role="button" data-toggle="collapse" href="#life-insurance-protector-cover" aria-expanded="false" aria-controls="life-insurance-protector-cover">
                    More
                </a>
                <div class="collapse" id="life-insurance-protector-cover">
                    <h4>Protection Cover</h4>
                    <p>Up to S$1,000,000</p>
                    <h4>Protection Period</h4>
                    <p>10 years, guaranteed renewable up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:  </span>1</sup></a> 90</p>
                    <h4>Add-on</h4>

                    <p>HSBC Insurance Online Critical Illness Rider <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." aria-label="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></p>
                    <h3>Premium Discounts</h3>
                    <p>Stand to enjoy discounted premiums by answering 4 lifestyle questions.</p>
                </div>
                <a href="/our-plans/online-protector/" class="secondary-btn-slate find-out-more-btn" data-product-name="OnlineProtector" data-link-name="FindOutMore" data-event="find-out-more" data-name="Online Protector">Find out more <span class="visuallyhidden">for premium discounts</span></a>
                <hr>
            </div>

            <div class="hidden-xs">
                <h4>Protection Cover</h4>
                <p>Up to S$1,000,000</p>
                <h4>Protection Period</h4>
                <p>10 years, guaranteed renewable up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote: </span>1</sup></a> 90</p>
                <h4>Add-on</h4>
                <p>HSBC Insurance Online Critical Illness Rider <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." aria-label="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></p>
                <h4>Premium Discounts</h4>
                <p>Stand to enjoy discounted premiums by answering 4 lifestyle questions.</p>
                <a href="/our-plans/online-protector/" class="secondary-btn-slate find-out-more-btn" data-product-name="OnlineProtector" data-link-name="FindOutMore" data-event="find-out-more" data-name="Online Protector">Find out more <span class="visuallyhidden">for premium discounts</span></a>
            </div>
        </div>
        <!--END: 1 Online Protector-->

        <!--START: 2 Direct ValueTerm-->
        <div class="col-sm-6">

            <h3>DIRECT – ValueTerm</h3>
            <p>If a fuss-free and flexible term life plan is what you need, look no further. Get essential coverage and enjoy the freedom to choose from a policy term of 5 years, 20 years or up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote: </span>1</sup></a> 65.</p><!--START: Get a quote & Retrieve your form cta-->

            <div class="row">
                <div class="btn-left"> <a href="/our-plans/direct-value-term/#quick_quote" class="secondary-btn-slate" data-event="quote-retireve" data-name="Direct Value Term">Get a quote</a></div>
                <div class="btn-right"> <a href="/our-plans/direct-value-term/retrieve/" class="outline-btn-slate" data-event="retrieve-your-form" data-name="DIRECT – ValueTerm">Retrieve your form</a></div>
            </div>
            <!--END: Get a quote & Retrieve your form cta-->
            <!-- <a href="#" class="secondary-btn-slate">Get a quote</a>
            <a href="#" class="outline-btn-slate">Retrieve your form</a> -->

            <div class="more-generic-text hidden-lg hidden-md hidden-sm">
                    <a class="btn btn-primary collapsed" role="button" data-toggle="collapse" href="#life-insurance-direct-valueterm" aria-expanded="false" aria-controls="life-insurance-direct-valueterm">
                        More
                    </a>
                    <div class="collapse" id="life-insurance-direct-valueterm">
                        <h4>Protection Cover</h4>
                        <p>Up to S$400,000</p>
                        <h4>Protection Period</h4>
                        <p>Choose from 3 options</p>
                        <ul>
                            <li>5 years with renewable option up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:  </span>1</sup></a> 65;</li>
                            <li>20 years or,</li>
                            <li>Up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote: </span>1</sup></a> 65</li>
                        </ul>
                        <h4>Add-on</h4>
                        <p>DIRECT – Critical Illness Rider
                        <button type="button" class="help-tooltip" label="help" tabindex="0" data-container="body" data-toggle="popover" label="help" data-placement="top" aria-label="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-content="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title="">
                                <i class="icon icon-circle-help-solid"></i>
                            </button>
                        </p>

                    </div>
                    <a href="#" class="secondary-btn-slate" data-event="find-out-more" data-name="Direct Value Term">Find out more <span class="visuallyhidden">for premium discounts</span></a>
                    <hr class="hidden-xs">
                </div>

                <div class="hidden-xs">
                    <h4>Protection Cover</h4>
                    <p>Up to S$400,000</p>
                    <h4>Protection Period</h4>
                    <p>Choose from 3 options</p>
                    <ul>
                        <li>5 years with renewable option up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote: </span>1</sup></a> 65;</li>
                        <li>20 years or,</li>
                        <li>Up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote: </span>1</sup></a> 65</li>
                    </ul>
                    <h4>Add-on</h4>

                    <p>DIRECT – Critical Illness Rider 
                    <button type="button" class="help-tooltip" label="help" tabindex="0" data-container="body" data-toggle="popover" label="help" data-placement="top" aria-label="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-content="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></p>

                    <a href="/our-plans/direct-value-term/" class="secondary-btn-slate" data-product-name="DirectValueTerm" data-link-name="FindOutMore" data-event="find-out-more" data-name="Direct Value Term">Find out more<span class="visuallyhidden"> for premium discounts</span></a>

                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <p class="tnc" style="padding-top:30px" id="footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup>Age refers to age next birthday and denotes the insured's age at a particular time with the addition of 1 year.
        </p>
        </div>
    </div>
</section>
<div class="space60"></div>
<div class="container">
    <hr>
</div>
<div class="space60"></div>

<!--Comparison-->
<section class="container comparison-chart">
    <div class="panel-group panel-hsbc" id="comparison" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default hsbc-panel">
            <div class="panel-heading hsbc-heading" role="tab" id="heading">
                <h4 class="panel-title hsbc-title"> <a  class="collapsed" role="button" data-toggle="collapse" data-parent="#comparison" href="#collapse1" aria-expanded="true" aria-controls="collapse">Choose the plan that best protects your loved ones
                </a></h4> </div>
            <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading">
                <div class="panel-body hsbc-content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <h2>HSBC Insurance OnlineProtector</h2></th>
                                <th>
                                    <h2>DIRECT - ValueTerm</h2></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">
                                    <h3>Protection Cover<span class="small-std">(Death, Terminal Illness and Total and Permanent Disability)</span></h3></th>
                                <td>Minimum: S$400,000
                                    <br> Maximum: S$1,000,000</td>
                                <td>Minimum: S$50,000
                                    <br> Maximum: S$400,000</td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <h3>Protection Period</h3></th>
                                <td>10 years</td>
                                <td>5 years, 20 years or up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 65</td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <h3>Guaranteed Renewability</h3></th>
                                <td>Every 10 years (last renewal is at age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 80 and policy will expire at age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 90)</td>
                                <td>Only applicable to a 5-year term (last renewal is at age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 60 and policy will expire at age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 65)</td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <h3>Critical Illness Rider <span class="small-std">(Optional)</span></h3> </th>
                                <td>
                                    <p>HSBC Insurance Online Critical Illness Rider</p>
                                    <p>Up to S$650,000, subject to terms and conditions</p>
                                </td>
                                <td>
                                    <p>DIRECT - Critical Illness Rider</p>
                                    <p>Up to S$400,000, subject to terms and conditions</p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <h3>Premium Discount</h3> </th>
                                <td> Stand to receive discounted premiums by answering 4 lifestyle questions </td>
                                <td> Not available </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <h3>Unemployment Benefit</h3> </th>
                                <td> Available </td>
                                <td> Not available </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <p class="tnc" style="margin-top: 15px;"><a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a>Age refers to age next birthday and denotes the insured’s age at a particular time with the addition of 1 year.</p>
            </div>
        </div>
    </div>
</section>
<div class="space60"></div>
<div class="container">
    <hr>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="faqGroup" value="plans" />
</jsp:include>

<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC - Our Plans - Homepage",
        "page_url"      : window.location.href,
        "page_type"     : "Homepage",
        "page_language" : "en",
        "page_security_level"   :   "0",
        "page_business_line"   :   "Insurance",
        "page_customer_group"   :   "General"
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>
