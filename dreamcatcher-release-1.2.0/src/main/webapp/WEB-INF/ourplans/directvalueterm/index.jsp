<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Direct Value Term"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-homepage"></jsp:param>
    <jsp:param name="metadataKeywords" value="insurance quotes online, flexible term life insurance, easy insurance"></jsp:param>
	<jsp:param name="metadataDescription" value="HSBC's flexible term life insurance plan that comes with three policy term options. Click here to get your insurance quote and apply online today."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/our-plans/direct-value-term/"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<c:set var="data" scope="request" value="${data}"></c:set>
<c:set var="errors" scope="request" value="${errors}"></c:set>

<section class="container-fluid">
    <div class="header-page header-directvalueterm">
        <div class="container">
            <div class="featured">
                <h1>DIRECT - ValueTerm</h1>
                <p>Making long-term plans for your loved ones can be hard.
                    <br>
                    Fortunately, providing them with a safety net to fall back on is not.
                    <br>
                    Now's Good.
                </p>
            </div>
        </div>
    </div>
</section>

<div class="main directvalueterm-main">
    <!--    Protect Your Love Ones-->
    <section class="container protect-love">
        <div class="row">
            <div class="col-sm-7">
                <div class="protect-love-box">
                    <h2>Term is the easy way forward</h2>
                    <p>No one can predict life's future events. If you find it hard to plan for the long run, here's a straightforward and
                        flexible term life insurance plan that covers you for a set period. Choose from the options of a policy term of 5
                        years that is renewable up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 65, 20 years, or up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 65 - with an add-on to cover for critical illness.</p>

                    <a href="#quick_quote" class="secondary-btn-slate" data-event="quote-retireve" data-name="Direct Value Term">Get a quote</a>
                    <a href="/our-plans/direct-value-term/retrieve/" class="outline-btn-slate" data-event="retrieve-your-form" data-name="DIRECT – ValueTerm">Retrieve your form</a>
                </div>
            </div>
            <div class="col-sm-5">
                <h3>Protection Cover</h3>
                <p>Up to S$400,000</p>
                <h3>Protection Period</h3>
                <p>Choose from 3 options:</p>
                <ul>
                    <li>5 years with renewable option up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 65;</li>
                    <li>20 years or,</li>
                    <li>Up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 65</li>
                </ul>
                <h3>Add-ons</h3>
                <ul>
                    <li>
                        DIRECT – Critical Illness Rider
                        <button type="button" class="help-tooltip" label="help" tabindex="0" data-container="body" data-toggle="popover" data-placement="top"
                                aria-label="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-content="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title="">
                            <i class="icon icon-circle-help-solid"></i>
                        </button>
                    </li>
                </ul>

            </div>
        </div>
    </section>
    <div class="space60"></div>
    <div class="container">
        <hr> </div>
    <div class="space60"></div>
    <!--    Enhance Your Coverage-->
    <div class="container directvalueterm-benefits">
        <h2>How DIRECT - ValueTerm benefits you</h2>
        <div class="r1-c3">
            <div class="row">
                <div class="col-sm-4">
                    <div class="tn-img">
                        <%-- FIXME: referencing onlineprotector image as placeholder --%>
                        <a href="#"><img src="/assets/images/directvalueterm/1-benefit-1260x688-ratio16-9.jpg" alt="" class="img-fullwidth"></a>
                    </div>

                    <h3><a href="#">Flexible coverage with 3 policy term options</a></h3>
                    <p>Choose from a policy term that suits your needs - 5 years that is renewable up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 65, 20 years, or up to the age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 65.
                    </p>
                </div>
                <div class="col-sm-4">
                    <div class="tn-img">
                        <%-- FIXME: referencing onlineprotector image as placeholder --%>
                        <a href="#"><img src="/assets/images/directvalueterm/2-benefit-1260x688-ratio16-9.jpg" alt="" class="img-fullwidth"></a>
                    </div>
                    <h3><a href="#">Guaranteed policy renewals for the 5-year term</a></h3>
                    <p>You will qualify for policy renewals every 5 years until you turn 65, without the need for medical proof. Premiums may vary with every renewal.</p>
                </div>
                <div class="col-sm-4">
                    <div class="tn-img">
                        <%-- FIXME: referencing onlineprotector image as placeholder --%>
                        <a href="#"><img src="/assets/images/directvalueterm/3-benefit-1260x688-ratio16-9.jpg" alt="" class="img-fullwidth"></a>
                    </div>
                    <h3><a href="#">Insurance that's easy to understand</a></h3>

                    <p>Learn about your policy the straightforward way. Simply answer a few questions to receive a quote on your premium.
                    </p>
                </div>
            </div>
            <p class="tnc info" id="footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup> Age refers to age next birthday and denotes the insured's age at a particular time with the addition of 1 year.</p>
        </div>
    </div>
    <div class="space60"></div>
    <div class="container">
        <hr> </div>
    <div class="space60"></div>
    <div class="container enhance-coverage">
        <h2>Enhance your coverage</h2>
        <div class="row">
            <div class="col-sm-6">
                <h3>DIRECT - Critical Illness Rider</h3>

                <p>In the unfortunate event of a critical illness, a sum insured of up to S$400,000 will be paid out.</p>
                <p><a name="ci_modal_open" class="text-link-pop">The rider covers 30 critical illnesses</a> including stroke, certain types of cancer and heart diseases. </p>
            </div>
        </div>
    </div>
    <div class="space60"></div>
    <div class="container">
        <hr> </div>
    <div class="space60"></div>
    <!--Comparison-->
    <section class="container prdt-reference">

        <h2>Product reference materials</h2>
        <div class="space10"></div>
        <h3>DIRECT - ValueTerm (Base Plan):</h3>

        <div>
            <ul class="downloads-list">
                <li>
                    <a href="/assets/downloads/GPPSDirectValueTerm5.pdf" target="_blank" data-event="file-download" data-name="GPPSDirectValueTerm5.pdf" aria-label="Download Product Summary and General Provisions 5 year in PDF format">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Product Summary and General Provisions 5 year (PDF, 451KB)</span>
                    </a>
                </li>
                <li>
                    <a href="/assets/downloads/GPPSDirectValueTerm20Age65.pdf" target="_blank" data-event="file-download" data-name="GPPSDirectValueTerm20Age65.pdf" aria-label="Download Product Summary and General Provisions 20 year and age 65 in PDF format">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Product Summary and General Provisions 20 year and age 65 (PDF, 446KB)</span>
                    </a>
                </li>    
                <li>
                    <a href="/assets/downloads/FXDPI.pdf" target="_blank" data-event="file-download" data-name="FXDPI.pdf" aria-label="Download Direct Purchase Insurance Factsheet in PDF format">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Direct Purchase Insurance Factsheet (PDF, 169KB)</span>
                    </a>
                </li>    
            </ul>
        </div>

        <div class="space20"></div>
        <h3>DIRECT - Critical Illness Rider:</h3>

        <div>
            <ul class="downloads-list">
                <li>
                    <a href="/assets/downloads/AXPSDirectCI.pdf" target="_blank" data-event="file-download" data-name="AXPSDirectCI.pdf" aria-label="Download Product Summary and Annexure in PDF format">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Product Summary and Annexure (PDF, 397KB)</span>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <div class="space60"></div>
    <section id="quick_quote" class="container-fluid estimate-form directvalueterm">
        <div class="container">
            <div class="estimate-form-container">
                <div class="estimate-form-box">
                    <form id="quick-quote-form" data-tag-quick-quote="DVTQuickQuote" data-form-name="directvalueterm-quickquote">
                        <div class="row is-flex">
                            <div class="col-xs-12 col-sm-12 col-md-2">
                                <h2>Get a quote</h2>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-5">
                                <h3>Tell us about yourself</h3>
                                <dl>
                                    <jsp:include page="../includes/applicant_gender.jsp">
                                        <jsp:param name="quickQuote" value="true"></jsp:param>
                                    </jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_dob.jsp">
                                        <jsp:param name="quickQuote" value="true"></jsp:param>
                                    </jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/term_duration.jsp" flush="true">
                                        <jsp:param name="quickQuote" value="true"></jsp:param>
                                    </jsp:include>

                                    <jsp:include page="../includes/applicant_smoker.jsp"></jsp:include>
                                </dl>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-5">
                                <h3>Coverage and add-on(s)</h3>
                                <p style="margin-bottom: 5px;">Enter your policy coverage or drag the slider to adjust the amount:</p>
                                <div class="input-group currency-text-box">
                                    <span class="input-group-addon">S$</span>
                                    <fmt:formatNumber var="fmtSumAssured" value="${fn:escapeXml(data['sum_assured'])}" type="number" maxFractionDigits="0"/>
                                    <input name="sum_assured" class="form-control number-only-input" value="${fmtSumAssured}" type="text">
                                </div>
                                <div aria-hidden="true" tabindex="-1">
                                    <fmt:formatNumber var="fmtMinSumAssured" value="${fn:escapeXml(data['min_sum_assured'])}" type="number" maxFractionDigits="0" groupingUsed="false"/>
                                    <fmt:formatNumber var="fmtMaxSumAssured" value="${fn:escapeXml(data['max_sum_assured'])}" type="number" maxFractionDigits="0" groupingUsed="false"/>
                                    <div class="slider slider-coverage" data-min="${fmtMinSumAssured}" data-max="${fmtMaxSumAssured}"></div>
                                </div>

                                <p class="ci-rider-addon">Add-on</p>
                                <div class="ci-rider-addon">

                                    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
                                        <input type="checkbox" name="include_ci_rider" id="include_ci_rider">
                                        <span></span>
                                        DIRECT - Critical Illness Rider
                                        <button type="button" class="help-tooltip" label="help" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" aria-label="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-content="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title="">
                                            <i class="icon icon-circle-help-solid"></i>
                                        </button>

                                    </label>
                                    <div data-question-group="ci_rider">
                                        <div style="white-space:nowrap">
                                            <div class="input-group currency-text-box readonly">
                                                <span class="input-group-addon">S$</span>
                                                <fmt:formatNumber var="fmtRiderSumAssured" value="${fn:escapeXml(data['rider_sum_assured'])}" type="number" maxFractionDigits="0"/>
                                                <input name="rider_sum_assured" class="form-control number-only-input" value="${fmtRiderSumAssured}" type="text" readonly>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="space20"></div>
                                </div>
                                
                                <label for="promocode">Enter promo code (if applicable):</label>
                                <div class="row">
                                    <div class="col-xs-8 col-md-9 form-group" style="border:none;">
                                        <input name="campaign_code" type="text" class="form-control auto-width" id="promocode" maxlength="20">
                                        <div class="help-block"><small>Only letters and numbers are allowed</small></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8 col-md-9 form-group">
                                        <button class="primary-btn-slate" data-action="quote">Get a quote</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="estimate-form-result-wrapper" aria-atomic="true" aria-live="polite">
                </div>
            </div>
        </div>
    </section>
<% /*
    <!--    Review-->
    <section class="container display-review">
        <h1>What our customers are saying</h1>
        <div class="row">
            <div class="col-sm-6 review-content">
                <p>XX of XX people found the following review helpful.</XX>
                <div class="rating"> <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span> </div>
                <P> By (name here)
                    <br> Review posted XX months ago</p>
                <p>Put the full review here</p> <a href="#" class="outline-btn-slate"><i class="icon icon-agree "></i> Yes, I recommend this product.</a>
                <p>Is this review helpful?</p>
            </div>
            <div class="col-sm-6 review-content">
                <p>XX of XX people found the following review helpful.</XX>
                <div class="rating"> <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span> </div>
                <P>By (name here)
                    <br> Review posted XX months ago</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p> <a href="#" class="outline-btn-slate"><i class="icon icon-agree "></i> Yes, I recommend this product.</a>
                <p>Is this review helpful?</p>
            </div>
        </div>
    </section>
    <div class="space60"></div>
    <div class="container">
        <hr> </div>
    <div class="space60"></div>
*/ %>
</div>

<div class="modal fade" id="ci-modal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close”><span aria-hidden="true”>&times;</span></button>
                <h4 class="modal-title">30 Critical Illnesses</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li>Alzheimer's Disease / Severe Dementia</li>
                    <li>Angioplasty and Other Invasive Treatment For Coronary Artery</li>
                    <li>Aplastic Anaemia</li>
                    <li>Bacterial Meningitis</li>
                    <li>Benign Brain Tumor</li>
                    <li>Blindness (Loss of Sight)</li>
                    <li>Coma</li>
                    <li>Coronary Artery By-pass Surgery</li>
                    <li>Deafness (Loss of Hearing)</li>
                    <li>End Stage Liver Failure</li>
                    <li>End Stage Lung Disease</li>
                    <li>Fulminant Hepatitis</li>
                    <li>Heart Attack of Specified Severity</li>
                    <li>Heart Value Surgery</li>
                    <li>HIV Due to Blood Transfusion and Occupationally Acquired HIV</li>
                    <li>Kidney Failure</li>
                    <li>Loss of Speech</li>
                    <li>Major Burns</li>
                    <li>Major Cancers</li>
                    <li>Major Head Trauma</li>
                    <li>Major Organ / Bone Marrow Transplantation</li>
                    <li>Motor Neurone Disease</li>
                    <li>Multiple Sclerosis</li>
                    <li>Muscular Dystrophy</li>
                    <li>Paralysis (Loss of Use of Limbs)</li>
                    <li>Parkinson's Disease</li>
                    <li>Primary Pulmonary Hypertension</li>
                    <li>Stroke</li>
                    <li>Surgery to Aorta</li>
                    <li>Viral Encephalitis</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn primary-btn-slate">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="promo-terms-modal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close”><span aria-hidden="true”>&times;</span></button>
            </div>
            <div class="modal-body">
                <ol type="1">
                    <li>In order to be eligible for the Promotion, you must have a valid campaign code</li>
                    <li>HSBC Insurance's decision on all matters relating to the Promotion including, without limitation, determining the eligibility of participants shall be final and binding on all participants</li>
                    <li>HSBC Insurance reserves the right to vary the terms of this Promotion or to terminate the Promotion at any time without prior notice</li>
                </ol>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn primary-btn-slate">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="eligibility-validation-modal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close”><span aria-hidden="true”>&times;</span></button>
                <h4 class="modal-title">Based on your response(s), we are unable to proceed with your online application.</h4>
            </div>
            <div class="modal-body">
                <p>However, we recommend that you visit the nearest HSBC Branch or complete this <a href="/contact-us/" style="text-decoration: underline;">contact form</a> so that our financial advisors can provide you with financial advice.</p>
                <p>Please note that the charges, features and benefits for advised products will be different from products offered on this online insurance platform.</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn primary-btn-slate">Close</button>
            </div>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="faqGroup" value="direct_value_term" />
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_index.jsp"></jsp:include>

<script>
$(document).ready(function() {
    $('a[name="ci_modal_open"]').click(function(e) {
        $('#ci-modal').modal('show');
    });

    $('i[name="promo_terms_open"]').click(function(e) {
        $('#promo-terms-modal').modal('show');
    });
	
	var formDirectvaluetermQuickQuote = $('[data-form-name="directvalueterm-quickquote"]'),
		dataDirectvaluetermQuickQuote = formDirectvaluetermQuickQuote.find('input, select, textarea, .selectBox'),
		startFormDirectvaluetermQuickQuote = false;
	
	dataDirectvaluetermQuickQuote.focus(function () {
		if (!startFormDirectvaluetermQuickQuote) {
			TMS.trackEvent({
				'event_category': 'Calculator Quick Quote',
				'event_action':   'Start',
				'event_form':     'Form Start',
				'event_content':  'Direct Value Term Calculator Quick Quote'
			});
			startFormDirectvaluetermQuickQuote = true;
		}
	});

    $('button[data-action=quote]').click(function(e) {
        var $button = $(this);
        $button.attr("disabled", "disabled");

        $('.estimate-form-result-wrapper *').remove();
        $('.estimate-form .error-message').hide();
        $('.estimate-form .form-group').removeClass('error');
		
		setTimeout(function () {
			$('.error').each(function () {
				TMS.trackEvent({
					'event_category': 'Calculator Quick Quote',
					'event_action':   'Error',
					'event_error':    $(this).data('field-name'),
					'event_content':  'Direct Value Term Calculator Quick Quote'
				});
			});
			
			if ($('.error').length === 0) {
				TMS.trackEvent({
					'event_category': 'Calculator Quick Quote',
					'event_action':   'Finish',
					'event_form':     'Form Finish',
					'event_content':  'Direct Value Term Calculator Quick Quote'
				});
			}
		}, 500);

        $.post('/our-plans/direct-value-term/quote', $('.estimate-form-box form').serialize(), function(data) {
            $('.estimate-form-result-wrapper').append(data);

            // button transformation
            $button
                .removeClass('primary-btn-slate')
                .addClass('secondary-btn-slate')
                .text('Recalculate');

            $button.removeAttr("disabled")

        });


        return false;
    });

    $('input[name="include_ci_rider"]').change(function() {
        $('[data-question-group=ci_rider]').toggle($('input[name=include_ci_rider]').is(':checked'));

        var sum_assured = $('[name=sum_assured]').val();

        $('[name=rider_sum_assured]').val(sum_assured).trigger('change');

    });
    if ($('input[name=include_ci_rider]').is(':checked')) {
        $('[data-question-group=ci_rider]').show();
    }
});
</script>


<style>
.directvalueterm form {
    max-width: none;
}
.estimate-form-box dl {
    margin: 0;
}
.estimate-form-box dd,
.estimate-form-box dt {
    clear: both;
    display: block;
    float: none;
    margin: 0;
    width: 100%;
    padding-left: 0;
    padding-bottom: 15px;
}

@media (min-width: 768px) {
    .estimate-form-box dd {
        padding-bottom: 25px;
    }
}

[data-question-group] {
 display: none;
}

.error-message {
    display: none;
}
</style>
