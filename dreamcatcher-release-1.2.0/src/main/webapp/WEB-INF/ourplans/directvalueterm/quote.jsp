<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="data" scope="request" value="${data}"></c:set>
<c:set var="errors" scope="request" value="${errors}"></c:set>

<c:if test="${fn:length(errors) eq 0}">

    <div class="hr" aria-hidden="true"></div>
    <div class="estimate-form-result">
        <div class="row">
            <div class="col-md-4">
                <h2>Your quote</h2>
                <c:choose>
                    <c:when test="${includeCriticalIllnessRider}">
                        <p>Under a policy term of <c:out value="${termDuration}"></c:out> with coverage of <strong>S$<c:out value="${sumAssured}"></c:out></strong> along with HSBC Insurance Online Critical Illness Rider with <strong>S$<c:out value="${riderSumAssured}"></c:out></strong> cover, here is an estimate of your monthly and annual premiums.</p>
                    </c:when>
                    <c:otherwise>
                        <p>Under a policy term of <c:out value="${termDuration}"></c:out> with coverage of <strong>S$<c:out value="${sumAssured}"></c:out></strong>, here is an estimate of your monthly and annual premiums.</p>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Monthly <span>payments</span></h3>
                        <p>S$<span class="large-text"><c:out value="${monthlyPremium}"></c:out></span></p>
                    </div>
                    <div class="col-md-6">
                        <h3>Annual <span>payments</span></h3>
                        <p>S$<span class="large-text"><c:out value="${yearlyPremium}"></c:out></span></p>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <p>This quote is only an estimate. <strong>Purchase this plan now to secure your protection needs</strong>.</p>
                <a href="#" class="primary-btn-slate" data-action="buy">Buy now</a>
            </div>
        </div>
    </div>

</c:if>

<script>
	var flag = false;
    $('a[data-action=buy]').click(function(evt) {
		evt.preventDefault();
		if (!flag) {
			flag = true;
			TMS.trackEvent({
				"event_category" : "Calculator Quick Quote",
				'event_action':   'Buy Now',
				'event_form':     'Form Buy Now',
				"event_content": "Direct Value Term Calculator (Buy Now)"
			});
			
			setTimeout(function () {
				location.href = '/our-plans/direct-value-term/1?' + $('#quick-quote-form').serialize();
				flag = false;
			}, 2000);
		}
    });

    <c:forEach items="${errors}" var="error">
    $('.error-message[data-field-name="<c:out value="${error.key}"></c:out>"] span').text("<c:out value="${error.value}"></c:out>");
    $('.error-message[data-field-name="<c:out value="${error.key}"></c:out>"]').show();
    $('.form-group[data-field-name="<c:out value="${error.key}"></c:out>"]').addClass('error');
    </c:forEach>

</script>
