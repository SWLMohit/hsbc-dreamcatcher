<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-occupationdetails"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>
<div class="main directvalueterm-step">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>
                    <form class="buynow-form" method="post" data-form-name="directvalueterm-step">
                        <div class="col-xs-12 col-sm-8">
                            <div class="row">
                                <div class="col-xs-12 col-md-5"><label for="form-employment" id="form-employment-label"><c:out value="${application.getApplicantFirstName()}"></c:out>, what is your current employment status?</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_employment')) ? 'error' : ''}"></c:out>" data-field-name="applicant_employment">

                                    <div class="form-select_wrap">
                                        <select name="applicant_employment" class="form-control" id="form-employment" aria-describedby="ariaEmployment">
                                            <option value="">Please select...</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Salaried'}">selected</c:if>>Salaried</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Self-employed'}">selected</c:if>>Self-employed</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Homemaker'}">selected</c:if>>Homemaker</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Retired'}">selected</c:if>>Retired</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Unemployed'}">selected</c:if>>Unemployed</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Student'}">selected</c:if>>Student</option>
                                        </select>
                                    </div>
                                    <c:if test="${errors.containsKey('applicant_employment')}">
                                        <span class="error-message" aria-label="Error" id="ariaEmployment">
                                            <i class="icon icon-circle-delete" aria-hidden="true" aria-hidden="true"></i>
                                            <c:out value="${errors['applicant_employment']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row" data-question-group="working_applicant">
                                <div class="col-xs-12 col-md-5"><label for="form-occupation"><c:out value="${application.getApplicantFirstName()}"></c:out>, what is your occupation?</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_occupation')) ? 'error' : ''}"></c:out>" data-field-name="applicant_occupation">
                                    <input name="applicant_occupation" id="form-occupation" class="form-control" value="${fn:escapeXml(data['applicant_occupation'])}" maxlength="100" aria-describedby="applicantOccupation">
                                    <c:if test="${errors.containsKey('applicant_occupation')}">
                                    <span class="error-message" aria-label="Error" id="applicantOccupation">
                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                        <c:out value="${errors['applicant_occupation']}"></c:out>
                                    </span>
                                    </c:if>
                                </div>
                            </div>

                            <div class="row" data-question-group="student_applicant">
                                <div class="col-xs-12 col-md-5"><label for="date-studies"><c:out value="${application.getApplicantFirstName()}"></c:out>, what is the end date of your studies?</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_studies_end_date')) ? 'error' : ''}"></c:out>" data-field-name="applicant_studies_end_date">
                                    <input id="date-studies" type="text" name="applicant_studies_end_date" placeholder="mm/yyyy" maxlength="7" class="date-validate-month auto-width form-control date-studies"
                                           value="${fn:escapeXml(data['applicant_studies_end_date'])}" aria-describedby="applicantStudiesEnd">
                                    <c:if test="${errors.containsKey('applicant_studies_end_date')}">

                                        <span class="error-message" aria-label="Error" id="applicantStudiesEnd">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['applicant_studies_end_date']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>

                            <div class="row" data-question-group="working_applicant">
                                <div class="col-xs-12 col-md-5"><label for="form-industry" id="form-industry-label">Which industry do you work in?</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_industry')) ? 'error' : ''}"></c:out>" data-field-name="applicant_industry">
                                    <div class="form-select_wrap">
                                        <select name="applicant_industry" class="form-control" id="form-industry" aria-describedby="applicantIndustry">
											<jsp:include page="/WEB-INF/ourplans/includes/applicant_industry.jsp" flush="true">
                                        	 	<jsp:param name="key" value="applicant_industry"></jsp:param>
                                        	</jsp:include>
                                        </select>
                                    </div>
                                    <c:if test="${errors.containsKey('applicant_industry')}">
                                        <span class="error-message" aria-label="Error" id="applicantIndustry">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['applicant_industry']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row" data-question-group="working_applicant">
                                <div class="col-xs-12 col-md-5" data-label-group="work-salaried" data-question-group="work-salaried">
                                    <label for="form-companyname">Employer name</label>
                                </div>
                                <div class="col-xs-12 col-md-5" data-label-group="work-self">
                                    <label for="form-companyname">Company name</label>
                                </div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_company_name')) ? 'error' : ''}"></c:out>" data-field-name="applicant_company_name">
                                    <input name="applicant_company_name" id="form-companyname" class="form-control" value="${fn:escapeXml(data['applicant_company_name'])}" maxlength="100" aria-describedby="companyName">
                                    <c:if test="${errors.containsKey('applicant_company_name')}">
                                        <span class="error-message" aria-label="Error" id="companyName">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['applicant_company_name']}"></c:out>
                                        </span>
                                    </c:if>

                                </div>
                            </div>
                            <div class="row" data-question-group="working_applicant">
                                <div class="col-xs-12 col-md-5"><label for="form-city">City</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_company_city')) ? 'error' : ''}"></c:out>" data-field-name="applicant_company_city">
                                    <input name="applicant_company_city" id="form-city" class="form-control" value="${fn:escapeXml(data['applicant_company_city'])}" maxlength="50" aria-describedby="companyCity">
                                    <c:if test="${errors.containsKey('applicant_company_city')}">
                                        <span class="error-message" aria-label="Error" id="companyCity">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['applicant_company_city']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row" data-question-group="working_applicant">
                                <div class="col-xs-12 col-md-5"><label for="form-country">Country</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_company_country')) ? 'error' : ''}"></c:out>" data-field-name="applicant_company_country">
                                    <input name="applicant_company_country" id="form-country" class="form-control" value="${fn:escapeXml(data['applicant_company_country'])}" maxlength="50" aria-describedby="companyCountry">
                                    <c:if test="${errors.containsKey('applicant_company_country')}">
                                        <span class="error-message" aria-label="Error" id="companyCountry">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['applicant_company_country']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-5"><label for="form-income">What is your current annual income?</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_income')) ? 'error' : ''}"></c:out>" data-field-name="applicant_income">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <fmt:formatNumber var="fmtApplicantIncome" value="${fn:escapeXml(data['applicant_income'])}" type="number" maxFractionDigits="0"/>
                                        <input name="applicant_income" id="form-income" class="form-control number-only-input" value="${fmtApplicantIncome}" type="text" aria-describedby="applicantIncome">
                                    </div>
                                    <c:if test="${errors.containsKey('applicant_income')}">
                                            <span class="error-message" aria-label="Error" id="applicantIncome">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_income']}"></c:out>
                                            </span>
                                    </c:if><div class="help-block" style="display: none;"><small>Please enter a number</small></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/direct-value-term/9">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                        </div>
                    </form>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
