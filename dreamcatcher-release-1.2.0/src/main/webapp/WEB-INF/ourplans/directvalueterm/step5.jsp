<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-uwbasic"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12 col-sm-9 col-md-8">
                        <form class="buynow-form" method="post" data-form-name="directvalueterm-step">
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-6">Have you smoked in the last 12 months?</legend>
                                    <div class="col-xs-12 col-md-5 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_smoker')) ? 'error' : ''}"></c:out>" data-field-name="applicant_smoker">
                                        <div class="newradio--inline">
                                            <input id="id_applicant_smoker_option1" type="radio" name="applicant_smoker" value="Y" <c:if test="${data['applicant_smoker']}">checked</c:if>>
                                            <label for="id_applicant_smoker_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_applicant_smoker_option2" type="radio" name="applicant_smoker" value="N" <c:if test="${data['applicant_smoker'] eq false}">checked</c:if> aria-describedby="applicantSmoker">
                                            <label for="id_applicant_smoker_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_smoker')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="applicantSmoker">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_smoker']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6"><label for="form-existinglifeinsurance">What is your existing life insurance coverage amount?</label></div>
                                <div class="col-xs-12 col-md-5 col-md-offset-1 form-group <c:out value="${(errors.containsKey('existing_life_insurance_coverage')) ? 'error' : ''}"></c:out>" data-field-name="existing_life_insurance_coverage">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <fmt:formatNumber var="fmtExistingCoverage" value="${fn:escapeXml(data['existing_life_insurance_coverage'])}" type="number" maxFractionDigits="0"/>
                                        <input id="form-existinglifeinsurance" name="existing_life_insurance_coverage" class="form-control number-only-input" value="${fmtExistingCoverage}" type="text" aria-describedby="existingCoverage">
                                    </div>
                                    <c:if test="${errors.containsKey('existing_life_insurance_coverage')}">
                                        <span class="error-message" aria-label="Error" id="existingCoverage">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['existing_life_insurance_coverage']}"></c:out>
                                        </span>
                                    </c:if><div class="help-block" style="display: none;"><small>Please enter a number</small></div>
                                </div>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-6">Have you ever been diagnosed with cancer, heart disease or stroke?</legend>
                                    <div class="col-xs-12 col-md-5 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_medical1')) ? 'error' : ''}"></c:out>" data-field-name="insured_medical1">
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical1_option1" type="radio" name="insured_medical1" value="Y" <c:if test="${data['insured_medical1']}">checked</c:if>>
                                            <label for="id_insured_medical1_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical1_option2" type="radio" name="insured_medical1" value="N" <c:if test="${data['insured_medical1'] eq false}">checked</c:if> aria-describedby="medical1">
                                            <label for="id_insured_medical1_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('insured_medical1')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="medical1">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_medical1']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-6">In the last 5 years, have you been diagnosed with or suffered from diabetes, HIV/AIDS or any medical condition affecting your brain, blood, heart, lungs, liver, kidneys for which you were required to undergo medical treatment for more than 14 days?</legend>
                                    <div class="col-xs-12 col-md-5 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_medical2')) ? 'error' : ''}"></c:out>" data-field-name="insured_medical2">
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical2_option1" type="radio" name="insured_medical2" value="Y" <c:if test="${data['insured_medical2']}">checked</c:if>>
                                            <label for="id_insured_medical2_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical2_option2" type="radio" name="insured_medical2" value="N" <c:if test="${data['insured_medical2'] eq false}">checked</c:if> aria-describedby="medical2">
                                            <label for="id_insured_medical2_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('insured_medical2')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="medical2">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_medical2']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-6">In the last 12 months, have you had any symptom such as unexplained bleeding, weight loss, lump or growth, blood in the stool, chest pain or weakness of limbs for which you are still under investigation or have not yet sought medical advice?</legend>
                                    <div class="col-xs-12 col-md-5 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_medical3')) ? 'error' : ''}"></c:out>" data-field-name="insured_medical3">
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical3_option1" type="radio" name="insured_medical3" value="Y" <c:if test="${data['insured_medical3']}">checked</c:if>>
                                            <label for="id_insured_medical3_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical3_option2" type="radio" name="insured_medical3" value="N" <c:if test="${data['insured_medical3'] eq false}">checked</c:if> aria-describedby="medical3">
                                            <label for="id_insured_medical3_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('insured_medical3')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="medical3">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_medical3']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>

                                </fieldset>
                            <div class="space30"></div>
                            <div>
                                <button type="button" class="back_btn" data-form-action="prev"
                                        data-url="/our-plans/direct-value-term/4">Back
                                </button>
                                <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
