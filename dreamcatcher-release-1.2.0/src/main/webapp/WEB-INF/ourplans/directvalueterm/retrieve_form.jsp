<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Retrieve Your Application - Direct Value Term"></jsp:param>
    <jsp:param name="metadataKeywords" value="online insurance application, direct value term application, retrieve direct value term"></jsp:param>
    <jsp:param name="metadataDescription" value="All it takes is two simple steps to resume your online insurance application for HSBC's Direct ValueTerm plan."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/our-plans/direct-value-term/retrieve/"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>


<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/our-plans/direct-value-term/retrieve/">Retrieve your form</a>
        <a href="/our-plans/direct-value-term/retrieve-form/" style="text-decoration: underline;" aria-current="page">Continue your form</a>
    </nav>
</div>

<div class="main">
    <article>
        <section class="directvalueterm verification
            <c:out value="${(errors.containsKey('invalid_code')) ? 'modalinvalidcode' : ''}"></c:out>
            <c:out value="${(errors.containsKey('form_not_found')) ? 'formnotfoundsimple' : ''}"></c:out>">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <form method="post">

                           <jsp:include page="/WEB-INF/ourplans/includes/retrieve_form_inc.jsp" > 
                               <jsp:param name="applicationType" value="direct-value-term" />
                           </jsp:include>

                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/modal_form_not_found_simple.jsp">
    <jsp:param name="applicationType" value="direct-value-term" />
</jsp:include>
<jsp:include page="/WEB-INF/ourplans/includes/modal_form_not_found.jsp">
    <jsp:param name="applicationType" value="direct-value-term" />
</jsp:include>
<jsp:include page="/WEB-INF/ourplans/includes/modal_invalid_code.jsp">
    <jsp:param name="applicationType" value="direct-value-term" />
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>