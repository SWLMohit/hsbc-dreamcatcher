<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-premiumconfirmation"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<div class="main directvalueterm-step">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm" data-dob="${application.getApplicantDob()}" >
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>

                    <form class="buynow-form" method="post" data-form-name="directvalueterm-step">
                        <div class="col-xs-12 col-sm-6">
                            <h2>Drag the slider to adjust the policy coverage:</h2>
                            <p>The coverage information displayed was captured during the 'Get a quote' process. You can choose to make changes here.</p>
                            <div class="row" data-field-name="sum_assured">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <fmt:formatNumber var="fmtSumAssured" value="${fn:escapeXml(data['sum_assured'])}" type="number" maxFractionDigits="0" groupingUsed="false"/>
                                        <input name="sum_assured" class="sumAssured form-control number-only-input" value="${fmtSumAssured}" type="text">
                                    </div>
                                </div>
                                <div class="col-xs-12" aria-hidden="true" tabindex="-1">
                                    <fmt:formatNumber var="fmtMinSumAssured" value="${fn:escapeXml(application.getMinSumAssured())}" type="number" maxFractionDigits="0" groupingUsed="false"/>
                                    <fmt:formatNumber var="fmtMaxSumAssured" value="${fn:escapeXml(application.getMaxSumAssured())}" type="number" maxFractionDigits="0" groupingUsed="false"/>
                                    <div class="slider slider-coverage" data-min="${fmtMinSumAssured}" data-max="${fmtMaxSumAssured}"></div>
                                    <c:if test="${errors.containsKey('sum_assured')}">
                                        <span class="error-message" aria-label="Error">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['sum_assured']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>

                            <jsp:include page="/WEB-INF/ourplans/includes/term_duration.jsp" flush="true"></jsp:include>

                            <div class="space30"></div>

                            <div class="ci-rider-addon">
                                <h3>Add-on(s)</h3>

                                <div class="row" data-field-name="include_ci_rider">
                                    <div class="col-xs-12">
                                        <label class="checkbox label-inline" role="checkbox" tabindex="0" aria-checked="false">
                                            <input type="checkbox" name="include_ci_rider" value="Y" <c:if test="${data['include_ci_rider']}">checked</c:if>>
                                            <span></span>
                                            DIRECT – Critical Illness Rider
                                        </label>
                                        <button type="button" class="help-tooltip" label="help" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" aria-label="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-content="A rider that pays out the life insured a lump sum of up to S$400,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title="">
                                            <i class="icon icon-circle-help-solid"></i>
                                        </button>
                                    </div>
                                </div>


                                <div class="row" data-question-group="ci_rider">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="input-group currency-text-box readonly step7" data-field-name="rider_sum_assured">
                                            <span class="input-group-addon">S$</span>
                                            <fmt:formatNumber var="fmtRiderSumAssured" value="${fn:escapeXml(data['rider_sum_assured'])}" type="number" maxFractionDigits="0" groupingUsed="false"/>
                                            <input name="rider_sum_assured" class="form-control number-only-input" value="${fmtRiderSumAssured}" type="text" readonly>
                                        </div>
                                    </div>

                                </div>
                                <div class="row" data-question-group="ci_rider">
                                    <div class="pointed pointed--down" style="margin-left: 15px; margin-right: 15px; width: auto;">
                                        <hr>
                                    </div>
                                    <fieldset  class="col-xs-12">
                                        <legend><p>Have any of your parents or siblings ever suffered from cancer, heart disease, stroke or any hereditary condition before age 60?</p></legend>
                                        <div class="form-group <c:out value="${(errors.containsKey('insured_medical4')) ? 'error' : ''}"></c:out>" data-field-name="insured_medical4">
                                            <div class="newradio--inline">
                                                <input id="id_insured_medical4_option1" type="radio" name="insured_medical4" value="Y" <c:if test="${data['insured_medical4']}">checked</c:if>>
                                                <label for="id_insured_medical4_option1"><span><span></span></span>Yes</label>
                                            </div>
                                            <div class="newradio--inline">
                                                <input id="id_insured_medical4_option2" type="radio" name="insured_medical4" value="N" <c:if test="${data['insured_medical4'] eq false}">checked</c:if> aria-describedby="medical4">
                                                <label for="id_insured_medical4_option2"><span><span></span></span>No</label>
                                            </div>
                                            <c:if test="${errors.containsKey('insured_medical4')}">
                                                <div>
                                                    <span class="error-message" aria-label="Error" id="medical4">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['insured_medical4']}"></c:out>
                                                    </span>
                                                </div>
                                            </c:if>
                                        </div>
                                    </fieldset>
                                </div>
                                
                            </div>

                            <div class="space30"></div>

                            <div class="row">
                                <div class="col-xs-8">
                                    <p><label for="form-paymentfrequency"  id="form-paymentfrequency-label">Preferred Payment frequency:</label></p>
                                    <div class="form-group <c:out value="${(errors.containsKey('payment_mode')) ? 'error' : ''}"></c:out>" data-field-name="payment_mode">
                                        <div class="form-select_wrap">
                                            <select name="payment_mode" class="form-control" id="form-paymentfrequency" aria-describedby="paymentFrequency">
                                                <option value="">Please select...</option>
                                                <option value="12" <c:if test="${data['payment_mode'] == 12}">selected</c:if>>Monthly</option>
                                                <option value="4" <c:if test="${data['payment_mode'] == 4}">selected</c:if>>Quarterly</option>
                                                <option value="2" <c:if test="${data['payment_mode'] == 2}">selected</c:if>>Semi-Annually</option>
                                                <option value="1" <c:if test="${data['payment_mode'] == 1}">selected</c:if>>Annually</option>
                                            </select>
                                        </div>

                                        <c:if test="${errors.containsKey('payment_mode')}">
                                            <span class="error-message" aria-label="Error" id="paymentFrequency"> 
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['payment_mode']}"></c:out>
                                            </span>
                                        </c:if>
                                    </div>
                                </div>
                            </div>

                          <div class="space60"></div>

                        </div>
						<div class="col-xs-12 col-sm-5 col-md-4 double-left-padding premium-table-container"  id="premium-table" name="summary" >
                            <table modalFactor="${data.get('modalFactor')}" actualdiscount5="${data.get('actualDiscount5') }" actualdiscount20="${data.get('actualDiscount20') }"  premiumrate="${data.get('premiumRate')}" premiumrateunitamount="${data.get('premiumRateUnitAmount')}"
                             actualdiscount65="${data.get('actualDiscount65') }" underwritingclassFactor="${data.get('underwritingClassFactor')}" ciadjustedPremium="${data.get('ciAdjustedPremium')}" ciadjustedPremium20="${data.get('ciAdjustedPremium20')}" ciadjustedPremium65="${data.get('ciAdjustedPremium65')}" adjustedPremium="${data.get('adjustedPremium')}" adjustedPremium20="${data.get('adjustedPremium20')}" adjustedPremium65="${data.get('adjustedPremium65')}"
                             premiumrate20="${data.get('premiumRate20')}" premiumrateunitamount20="${data.get('premiumRateUnitAmount20')}" 
                             premiumrate65="${data.get('premiumRate65')}" premiumrateunitamount65="${data.get('premiumRateUnitAmount65')}" class="premium-table data-table" style="border-bottom:6px solid #008480;">
                                <thead>
                                    <tr >
                                        <th colspan="3">Premium Summary:</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th colspan="3" style="padding-right:20px;"><strong style="float:right;" id="payment-method">Monthly</strong></th>
                                    </tr>
                                    <tr>
                                        <td>Basic Plan</td>
                                        <td style="width:23%;"><span id="basic_mode"><c:out value="${data.get('summary_modal')}"></c:out></span></td>
                                    </tr>
                                    
                                        <tr id="cirider-row" <c:if test="${empty data.get('ciriderincluded') || !data.get('ciriderincluded')}"> style="display:none;" </c:if>>
                                            <td>HSBC Insurance Online Critical Illness rider</td>
                                            <td><span id="ci_mode"><c:out value="${data.get('summary_ci_modal')}"></c:out></span></td>
                                        </tr>
                                    
                                    
                                       
									
									<tr>
                                        <td><strong style="float:right">Total<strong></td>
                                        <td><strong id="total"><c:out value="${data.get('summary_total_modal')}"></c:out></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <input type="hidden" name="modalFactor" value="${data.get('modalFactor')}"/> 
						<input type="hidden" name="underwritingclassfactor"  value="${data.get('underwritingClassFactor')}"/> 
						<input type="hidden" name="ciadjustedpremium" value="${data.get('ciAdjustedPremium')}"/> 
						<input type="hidden" name="premiumrate" value="${data.get('premiumRate')}"/> 
						<input type="hidden" name="premiumrateunitamount" value="${data.get('premiumRateUnitAmount')}"/> 
						
						<input type="hidden" name="adjustedpremium" value="${data.get('adjustedPremium')}"/>
						
						<input type="hidden" name="actualdiscount5" value="${data.get('actualDiscount5') }" />
						
						<input type="hidden" name="ciadjustedpremium20" value="${data.get('ciAdjustedPremium20')}"/> 
						<input type="hidden" name="premiumrate20" value="${data.get('premiumRate20')}"/> 
						<input type="hidden" name="premiumrateunitamount20" value="${data.get('premiumRateUnitAmount20')}"/> 
						
						<input type="hidden" name="adjustedpremium20" value="${data.get('adjustedPremium20')}"/>
						
						<input type="hidden" name="actualdiscount20" value="${data.get('actualDiscount20') }" />
						
						<input type="hidden" name="ciadjustedpremium65" value="${data.get('ciAdjustedPremium65')}"/> 
						<input type="hidden" name="premiumrate65" value="${data.get('premiumRate65')}"/> 
						<input type="hidden" name="premiumrateunitamount65" value="${data.get('premiumRateUnitAmount65')}"/> 
						
						<input type="hidden" name="adjustedpremium65" value="${data.get('adjustedPremium65')}"/>
						
						<input type="hidden" name="actualdiscount65" value="${data.get('actualDiscount65') }" />
						
						<input type="hidden" name="basic" value="${data.get('summary_modal') }" />
						<input type="hidden" name="ci" value="${data.get('summary_ci_modal') }" />
						
                        
                        <div class="col-xs-12 col-sm-8">
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/direct-value-term/5">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                        </div>
                    </form>
                </div>
            </div>

        </section>
    </article>
</div>


<div class="modal fade" id="eligibility-validation-modal" tabindex="-1" role="dialog"  aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Based on your response(s), we are unable to proceed with your online application.</h4>
            </div>
            <div class="modal-body">
                <p>However, we recommend that you visit the nearest HSBC Branch or complete this <a href="/contactus" style="text-decoration: underline;">contact form</a> so that our financial advisors can provide you with financial advice.</p>
                <p>Please note that the charges, features and benefits for advised products will be different from products offered on this online insurance platform.</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn primary-btn-slate">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ridermodal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4>Are you sure you want to proceed without selecting the DIRECT - Critical Illness Rider?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class=" btn btn-white" data-dismiss="modal">Yes</button>
                <button type="button" class=" btn btn-red" data-dismiss="modal">No, let me select</button>
            </div>
        </div>
    </div>
</div>


<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
