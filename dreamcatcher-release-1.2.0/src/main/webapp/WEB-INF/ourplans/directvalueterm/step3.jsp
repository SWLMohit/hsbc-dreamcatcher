<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-applicantinfo"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<c:set var="data" scope="request" value="${data}"></c:set>
<c:set var="errors" scope="request" value="${errors}"></c:set>

<div class="main step3 directvalueterm-step">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12">
                        <form class="buynow-form" method="post" data-form-name="directvalueterm-step" data-form-dropout="<c:out value="${(errors.containsKey('dropout')) ? '1' : ''}"></c:out>">

                            <div class="row">
                                <div class="col-xs-12 col-sm-10">
                                    <h2>Tell us more about yourself before proceeding with your application</h2>
                                    <p>The products and its related features offered on this online platform are available only to customers who are currently residing
                                        <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help"
                                        aria-label="Residing in Singapore means anyone who is physically present in Singapore for 183 days or more during the year."
                                        data-content="Residing in Singapore means anyone who is physically present in Singapore for 183 days or more during the year.">
                                            <i class="icon icon-circle-help-solid"></i>
                                        </button>
                                        in Singapore.
                                    </p>
                                </div>
                            </div>
                            <div class="space20"></div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-9 col-md-8">
                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_residency.jsp" flush="true"></jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_first_name.jsp" flush="true"></jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_last_name.jsp" flush="true"></jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_dob.jsp" flush="true">
                                        <jsp:param name="formpage" value="true" />
                                    </jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_mobile.jsp" flush="true"></jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_email.jsp" flush="true"></jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_gender.jsp" flush="true">
                                        <jsp:param name="formpage" value="true" />
                                    </jsp:include>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-9">
                                <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
