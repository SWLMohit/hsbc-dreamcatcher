<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-idvuploadandpayment"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>
                    <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="directvalueterm-step">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-10">
                                    <h2>Credit card details</h2>
                                    <p>Payment has to be made by a card issued in your name. We currently only accept cards issued in Singapore.</p>
                                    <p>Premiums paid using HSBC Credit Cards or HSBC Debit Cards will enjoy an additional S$20 Cash Credit reward.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-9 col-md-8">
                       
                                    <div class="row">
                                        <fieldset>
                                            <legend class="col-xs-12 col-md-5">Payment type</legend>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('cc_type')) ? 'error' : ''}"></c:out>" data-field-name="cc_type">
                                                <div class="newradio--inline">
                                                    <input id="id_cc_type_option1" type="radio" name="cc_type" value="V" <c:if test="${data['cc_type'] == 'V'}">checked</c:if>>
                                                    <label for="id_cc_type_option1"><span><span></span></span>VISA</label>
                                                </div>
                                                <div class="newradio--inline">
                                                    <input id="id_cc_type_option2" type="radio" name="cc_type" value="M" <c:if test="${data['cc_type'] == 'M'}">checked</c:if> aria-describedby="applicantCCType">
                                                    <label for="id_cc_type_option2"><span><span></span></span>MasterCard</label>
                                                </div>
                                                <c:if test="${errors.containsKey('cc_type')}">
                                                    <div>
                                                            <span class="error-message" aria-label="Error" id="applicantCCType">
                                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                                <c:out value="${errors['cc_type']}"></c:out>
                                                            </span>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </legend>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-5 control_singapore"><label for="form-ccname">Name <small>(as appears on your credit card)</small></label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('cc_name')) ? 'error' : ''}"></c:out>" data-field-name="cc_name">
                                            <input name="cc_name" id="form-ccname" class="form-control" type="text" value="${fn:escapeXml(data['cc_name'])}" maxlength="100" aria-describedby="applicantCCName" autocomplete="off">
                                            <c:if test="${errors.containsKey('cc_name')}">
                                                <span class="error-message" aria-label="Error" id="applicantCCName">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['cc_name']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-5 control_singapore"><label for="form-ccno">Card number</label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('cc_num')) ? 'error' : ''}"></c:out>" data-field-name="cc_num">
                                            <input name="cc_num" id="form-ccno" class="form-control" type="text" value="${fn:escapeXml(data['cc_num'])}" maxlength="16" autocomplete="off" aria-describedby="applicantCCNum">
                                            <c:if test="${errors.containsKey('cc_num')}">
                                                <span class="error-message" aria-label="Error" id="applicantCCNum">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['cc_num']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <fieldset>
                                            <legend class="col-xs-12 col-md-5">Expiry date</legend>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('cc_expiry')) ? 'error' : ''}"></c:out>" data-field-name="cc_expiry">
                                                <div class="form-group__multi form-group__multi--date">
                                                    <span>
                                                        <label for="form-ccexprirymonth" class="visuallyhidden">Month of expiry date</label>
                                                        <input name="cc_expiry_month" id="form-ccexprirymonth"  type="text" class="form-control date-month date-credit-card" placeholder="MM" value="${fn:escapeXml(data['cc_expiry_month'])}" maxlength="2" autocomplete="off">
                                                    </span>
                                                    <span>
                                                        <label for="form-ccexpriryyear" class="visuallyhidden">Year of expiry date</label>
                                                        <input name="cc_expiry_year" id="form-ccexpriryyear" type="text" class="form-control date-year date-credit-card" placeholder="YY" value="${fn:escapeXml(data['cc_expiry_year'])}" maxlength="2" aria-describedby="applicantCCExp" autocomplete="off">
                                                    </span>
                                                </div>
                                                <c:if test="${errors.containsKey('cc_expiry')}">
                                                        <span class="error-message" aria-label="Error" id="applicantCCExp">
                                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['cc_expiry']}"></c:out>
                                                        </span>
                                                </c:if>
                                            </div>
                                        </fieldset>
                                    </div>

    								<%--NRIC will be shown to Singaporean and Singaporean PR. Rest will be shown passport as a part of HSBCIDCU-661--%>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-5">

                                                <label for="applicant-idv-upload-input">
                                                    <c:choose>
                                                      <c:when test="${application.applicantIsSingaporean()||application.applicantIsPermanentResident()}">
                                                      <c:out value="${application.getApplicantFirstName()}"></c:out>, please upload a copy of the front of your NRIC
                                                  </c:when>
                                                  <c:otherwise>
                                                      <c:out value="${application.getApplicantFirstName()}"></c:out>, please upload a copy of your Passport
                                                    </c:otherwise>
                                                  </c:choose>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 ">
                                                <div style="width:0;height:0;overflow:hidden;">
                                                    <input name="applicant_idv_upload" id="applicant-idv-upload-input" type="file" accept=".jpg,.jpeg" aria-hidden="true" tabindex="-1" class="visuallyhidden" aria-describedby="upload1">
                                                </div>
                                                <a data-file-input="applicant-idv-upload-input" class="btn secondary-btn-slate btn-file-upload">
                                                    <span aria-hidden="true"></span>
												 <c:choose>
                                                   <c:when test="${docUpload.containsKey('applicant_idv_previous_upload')}">
                                                      File selected
                                                   </c:when>
                                                   <c:otherwise>
                                                     Upload
                                                  </c:otherwise>
                                                 </c:choose>
												
												</a>
                                                <c:if test="${errors.containsKey('applicant_idv_upload')}">
                                                        <div class="error-message" aria-label="Error" id="upload1">
                                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['applicant_idv_upload']}"></c:out>
                                                        </div>
                                                </c:if>
                                            </div>
                                        </div>

                                    <%--                            
                                        only show IF not singaporean or not PR for relevant pass field JIRA 661
                                    --%>

    								<c:if test="${not application.applicantIsSingaporean() and not application.applicantIsPermanentResident()}">
    									<div class="row">
                                            <div class="col-xs-12 col-md-5">

                            <label for="applicant-idv-two-upload-input">
                              <c:out value="${application.getApplicantFirstName()}"></c:out>, please upload a copy of the front of your relevant pass
                            </label>
    									   </div>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 ">
        										<div style="width: 0; height: 0; overflow: hidden;">
        											<input name="applicant_idv_two_upload"
                                                        aria-describedby="upload2"
        												id="applicant-idv-two-upload-input" type="file"
        												accept=".jpg,.jpeg" aria-hidden="true" tabindex="-1" class="visuallyhidden">
        										</div>
        										<a data-file-input="applicant-idv-two-upload-input"
        											class="btn secondary-btn-slate btn-file-upload">
                                                    <span aria-hidden="true"></span>
													<c:choose>
                                                     <c:when test="${docUpload.containsKey('applicant_idv_previous_upload_2')}">
                                                       File selected
                                                     </c:when>
                                                    <c:otherwise>
                                                      Upload
                                                     </c:otherwise>
                                                    </c:choose>
												</a>
        										<c:if test="${errors.containsKey('applicant_idv_two_upload')}">
        											<div class="error-message" aria-label="Error" id="upload2">
        												<i class="icon icon-circle-delete"></i>
        												<c:out value="${errors['applicant_idv_two_upload']}"></c:out>
        											</div>
        										</c:if>
    									       </div>
                                        </div>

                                        <div class="row">
                                        <div class="col-xs-12 col-md-5">
		                                <c:out value="${application.getApplicantFirstName()}"></c:out>, please upload a copy of the back of your relevant pass
		                                </div>
		                                 <div class="col-xs-12 col-md-6 col-md-offset-1 ">
		                                    <div style="width:0;height:0;overflow:hidden;">
		                                        <input name="applicant_relevant_back_upload" id="applicant-relevant-back-upload-input" type="file" accept=".jpg,.jpeg" aria-describedby="upload3"> 
		                                    </div>
		                                    <a data-file-input="applicant-relevant-back-upload-input" class="btn secondary-btn-slate btn-file-upload">
                                                <span aria-hidden="true"></span>
		                                         <c:choose>
                                                     <c:when test="${docUpload.containsKey('applicant_relevant_back_upload_previous')}">
                                                       File selected
                                                     </c:when>
                                                     <c:otherwise>
                                                       Upload
                                                     </c:otherwise>
                                                   </c:choose>
		                                    
		                                    
		                                    </a>
		                                    <c:if test="${errors.containsKey('applicant_relevant_back_upload')}">
		                                            <div class="error-message" aria-label="Error" id="upload3">
		                                                <i class="icon icon-circle-delete"></i>
		                                                <c:out value="${errors['applicant_relevant_back_upload']}"></c:out>
		                                            </div>
		                                    </c:if>
		                                </div>
		    							</div>
    								</c:if>

    								<c:if test="${application.applicantIsSingaporean() or application.applicantIsPermanentResident()}">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-5">
                                                <label for="applicant-idv-back-nric-upload-input">
                                                    <c:out value="${application.getApplicantFirstName()}"></c:out>, please upload a copy of the back of your NRIC
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 ">
                                                <div style="width:0;height:0;overflow:hidden;">
                                                    <input name="applicant_idv_back_nric_upload" id="applicant-idv-back-nric-upload-input" type="file" accept=".jpg,.jpeg" aria-hidden="true" tabindex="-1" class="visuallyhidden" aria-describedby="upload4">
                                                </div>
                                                <a data-file-input="applicant-idv-back-nric-upload-input" class="btn secondary-btn-slate btn-file-upload">
                                                    <span aria-hidden="true"></span>
												   <c:choose>
                                                     <c:when test="${docUpload.containsKey('applicant_idv_back_nric_previous_upload')}">
                                                      File selected
                                                     </c:when>
                                                      <c:otherwise>
                                                         Upload
                                                    </c:otherwise>
                                                 </c:choose>
												
												</a>
                                                <c:if test="${errors.containsKey('applicant_idv_back_nric_upload')}">
                                                        <div class="error-message" aria-label="Error" id="upload4">
                                                            <i class="icon icon-circle-delete"></i>
                                                            <c:out value="${errors['applicant_idv_back_nric_upload']}"></c:out>
                                                        </div>
                                                </c:if>
                                            </div>
                                        </div>
    								</c:if>
    								<%-- Applicant Second Nationality documents upload as per HSBCIDCU-820 starts here --%>
                                    <c:if test="${application.getApplicantMultipleNationalities()&& !application.getApplicantNationality2().isEmpty()}">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-5">
                                                <label for="applicant-nationality-two-idv-upload-input">
                                                    <c:out value="${application.getApplicantFirstName()}"></c:out>, please upload a copy of your passport for your second nationality.
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 ">
                                                <div style="width:0;height:0;overflow:hidden;">
                                                    <input name="applicant_nationality_two_idv_upload" id="applicant-nationality-two-idv-upload-input" type="file" accept=".jpg,.jpeg" aria-hidden="true" tabindex="-1" class="visuallyhidden" aria-describedby="upload5">
                                                </div>
                                                <a data-file-input="applicant-nationality-two-idv-upload-input" class="btn secondary-btn-slate btn-file-upload">
                                                    <span aria-hidden="true"></span>
												   <c:choose>
                                                      <c:when test="${docUpload.containsKey('applicant_nationality_two_idv_previous_upload')}">
                                                         File selected
                                                      </c:when>
                                                     <c:otherwise>
                                                       Upload
                                                     </c:otherwise>
                                                    </c:choose>
												
												</a>
                                                <c:if test="${errors.containsKey('applicant_nationality_two_idv_upload')}">
                                                    <div class="error-message"  aria-label="Error" id="upload5">
                                                        <i class="icon icon-circle-delete"></i>
                                                        <c:out value="${errors['applicant_nationality_two_idv_upload']}"></c:out>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </div>
                                    </c:if>

                                    <%-- Applicant Second Nationality documents upload as per HSBCIDCU-820 ends here here --%>
                                    <%--Applicant Third Nationality documents upload as per HSBCIDCU-820 starts here--%>
                                    <c:if test="${application.getApplicantMultipleNationalities()&& !application.getApplicantNationality3().isEmpty()}">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-5">
                                                <label for="applicant-nationality-three-idv-upload-input">
                                                    <c:out value="${application.getApplicantFirstName()}"></c:out>, please upload a copy of your passport for your third nationality. 
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 ">
                                                <div style="width:0;height:0;overflow:hidden;">
                                                    <input name="applicant_nationality_three_idv_upload" id="applicant-nationality-three-idv-upload-input" type="file" accept=".jpg,.jpeg" aria-hidden="true" tabindex="-1" class="visuallyhidden" aria-describedby="upload6">
                                                </div>
                                                <a data-file-input="applicant-nationality-three-idv-upload-input" class="btn secondary-btn-slate btn-file-upload">
                                                    <span aria-hidden="true"></span>
												  <c:choose>
                                             <c:when test="${docUpload.containsKey('applicant_nationality_three_idv_previous_upload')}">
                                               File selected
                                             </c:when>
                                            <c:otherwise>
                                              Upload
                                             </c:otherwise>
                                         </c:choose>
												
												</a>
                                                <c:if test="${errors.containsKey('applicant_nationality_three_idv_upload')}">
                                                    <div class="error-message" aria-label="Error" id="upload6">
                                                        <i class="icon icon-circle-delete"></i>
                                                        <c:out value="${errors['applicant_nationality_three_idv_upload']}"></c:out>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </div>
                                    </c:if>

								   <%--Applicant Third Nationality documents upload as per HSBCIDCU-820 ends here--%>
                                   
                                   <%--Resident Address proof starts  here --%>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5"><label for="applicant-address-upload-input">Please upload a copy of any one of the following documents as proof of address:<br><br>
                                                A utility/telco bill; a bank statement; any correspondence from a Singapore government body issued within the last 3 months</label>
                                        </div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 ">
                                            <div style="width:0;height:0;overflow:hidden;">
                                                <input name="applicant_address_upload" id="applicant-address-upload-input" type="file" accept=".jpg,.jpeg" aria-hidden="true" tabindex="-1" class="visuallyhidden" aria-describedby="upload7">
                                            </div>
                                            <a data-file-input="applicant-address-upload-input" class="btn secondary-btn-slate btn-file-upload">
                                                <span aria-hidden="true"></span>
											    <c:choose>
                                              <c:when test="${docUpload.containsKey('applicant_address_previous_upload')}">
                                                  File selected
                                              </c:when>
                                              <c:otherwise>
                                                  Upload
                                              </c:otherwise>
                                           </c:choose>
											
											</a>
                                            <c:if test="${errors.containsKey('applicant_address_upload')}">
                                                <div class="error-message" aria-label="Error" id="upload7">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_address_upload']}"></c:out>
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
    								<%--Resident Address proof ends here --%>
    								
                                    <%--Permanent Resident Address proof starts here  HSBCIDCU-820 --%>
      							   <c:if test="${not application.applicantIsSingaporean() and not(application.applicantIsPermanentResident()
                                    and application.getApplicantAddressIsPermanent())}">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-5">
                                                <label for="applicant-permanent-address-upload-input">
                                                    Please upload a copy of proof of permanent address
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1">
                                                <div style="width:0;height:0;overflow:hidden;">
                                                    <input name="applicant_permanent_address_upload" id="applicant-permanent-address-upload-input" type="file" accept=".jpg,.jpeg" aria-hidden="true" tabindex="-1" class="visuallyhidden" aria-describedby="upload8">
                                                </div>
                                                <a data-file-input="applicant-permanent-address-upload-input" class="btn secondary-btn-slate btn-file-upload">
                                                    <span aria-hidden="true"></span>
												  <c:choose>
                                              <c:when test="${docUpload.containsKey('applicant_permanent_address_previous_upload')}">
                                                  File selected
                                              </c:when>
                                              <c:otherwise>
                                                  Upload
                                              </c:otherwise>
                                           </c:choose>
												
												</a>
                                                <c:if test="${errors.containsKey('applicant_permanent_address_upload')}">
                                                    <div class="error-message" aria-label="Error" id="upload8">
                                                        <i class="icon icon-circle-delete"></i>
                                                        <c:out value="${errors['applicant_permanent_address_upload']}"></c:out>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </div>
                                    </c:if>

                                   <%--Permanent Resident Address proof ends here --%>
                                   <%--Beneficial owner documents starts here HSBCIDCU-820 --%>
                                    <c:if test="${!application.getApplicantIsBeneficalOwner()}">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-5">
                                                <label for="beneficial-idv-upload-input">
                                                    <c:out value="${application.getBeneficialOwnerFirstName()}"></c:out>, please upload a copy of the front and back of your NRIC/passport 
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1">
                                                <div style="width:0;height:0;overflow:hidden;">
                                                    <input name="beneficial_idv_upload" id="beneficial-idv-upload-input" type="file" accept=".jpg,.jpeg" aria-hidden="true" tabindex="-1" class="visuallyhidden" aria-describedby="upload9">
                                                </div>
                                                <a data-file-input="beneficial-idv-upload-input" class="btn secondary-btn-slate btn-file-upload">
                                                    <span aria-hidden="true"></span>
												   <c:choose>
                                              <c:when test="${docUpload.containsKey('beneficial_idv_previous_upload')}">
                                                  File selected
                                              </c:when>
                                              <c:otherwise>
                                                  Upload
                                              </c:otherwise>
                                           </c:choose>
												
												</a>
                                                <c:if test="${errors.containsKey('beneficial_idv_upload')}">
                                                    <div class="error-message" aria-label="Error" id="upload9">
                                                        <i class="icon icon-circle-delete"></i>
                                                        <c:out value="${errors['beneficial_idv_upload']}"></c:out>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </div>
                                    </c:if>
     								<%--Beneficial owner documents ends here HSBCIDCU-820 --%>

                                    <div>
                                        <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
                                            <input type="checkbox" id="form-mailoption" name="mail_option" value="1" checked>
                                            <span></span>
                                            Would you like to receive your Policy documents via email?
                                        </label>
                                    </div>


                                    <div class="space40"></div>

                                    <div>
                                        <p>As the policy applicant and also the cardholder, I authorise HSBC Insurance to charge the initial, subsequent, backdated and extra premium(s) (if any) to my credit card for the above insurance application(s). I understand that my insurance policy(ies) will not be effective until the required premium(s) has been paid and I receive a written confirmation from HSBC Insurance  that my application(s) has/have been approved. This payment authorisation will continue to be in effect until I notify HSBC Insurance in writing to cancel such authorisation.</p>
                                        <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
                                            <input type="checkbox" name="declare_accept" value="Y" aria-describedby="declareAccept" <c:if test="${data['declare_accept'] == 'Y'}">checked</c:if>>
                                            <span></span>
                                            I accept (<c:out value="${application.getApplicantFullName()}"></c:out>)
                                        </label>
                                        <c:if test="${errors.containsKey('declare_accept')}">
                                            <div class="error-message" aria-label="Error" style="padding-bottom:10px;" id="declareAccept">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['declare_accept']}"></c:out>
                                            </div>
                                        </c:if>
                                    </div>
                                    <div class="space40"></div>

                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-4 double-left-padding">
                                    <table class="data-table" style="border-bottom:6px solid #008480;">
                                        <thead>
                                            <tr>

                                                <th width="50%" scope="col">Summary:</th>
                                                <th colspan="2" scope="colgroup">
                                                    <c:out value="${data.get('summary_total_modal')}"></c:out> (<c:out value="${application.getPaymentMode() == 1  ? 'Annual' : application.getPaymentModeLabel()}"></c:out>)
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <th scope="col" id="co1"><strong><c:out value="${application.getPaymentMode() == 1 ? 'Annual' : application.getPaymentModeLabel()}"></c:out></strong></th>
                                            </tr>
                                            <tr>
                                                <th scope="row" id="c1"><strong>Basic Plan:</strong></th>
                                                <td headers="co1 c1"><c:out value="${data.get('summary_modal')}"></c:out></td>
                                            </tr>
                                            <c:if test="${application.getIncludeCriticalIllnessRider()}">
                                            <tr>
                                                <th scope="row" id="c2"><strong>Critical Illness Cover:</strong></th>
                                                <td headers="co1 c2"><c:out value="${data.get('summary_ci_modal')}"></c:out></td>
                                            </tr>
                                            </c:if>
                                            <tr>
                                                <th colspan="3" scope="colgroup" id="c3">
                                                    <strong>Promo code applied:</strong>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td colspan="3" headers="c3" style="padding-top: 7px;">
                                                    <c:choose>
                                                      <c:when test="${application.getCampaignCode() != null&&fn:length(fn:trim(application.getCampaignCode()))>0}">
                                                      ${application.getCampaignCode()}
                                                      </c:when>
                                                      <c:otherwise>
                                                      Nil
                                                      </c:otherwise>
                                                    </c:choose>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th colspan="3" scope="colgroup" id="c4">
                                                    <strong>Payment frequency:</strong>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td colspan="3" headers="c4" style="padding-top: 7px;">
                                                    <c:out value="${application.getPaymentModeLabel()}"></c:out>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" style="border-top:1px solid #999;" scope="colgroup">
                                                    <a href="/our-plans/direct-value-term/7" type="button" class="back_btn btn" style="color:#333;font-size:1em;padding-top:10px;">Edit your plan</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="space40"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-9">
                                    <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/direct-value-term/13">Back</button>
                                    <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>

