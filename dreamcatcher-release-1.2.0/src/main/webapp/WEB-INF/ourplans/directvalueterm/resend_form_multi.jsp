<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Retrieve Your Application - Direct Value Term"></jsp:param>
    <jsp:param name="metadataKeywords" value="online insurance application, direct value term application, retrieve direct value term"></jsp:param>
    <jsp:param name="metadataDescription" value="All it takes is two simple steps to resume your online insurance application for HSBC's Direct ValueTerm plan."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/our-plans/direct-value-term/retrieve/"></jsp:param>
</jsp:include>

<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/our-plans/direct-value-term/retrieve/">Retrieve your form</a>
        <a href="/our-plans/direct-value-term/resend-form/">Resend your completed form</a>
        <c:choose>
            <c:when test="${data['retrieve_pass_code']!= null}">
                <a href="/our-plans/direct-value-term/retrieve-pass-code/" style="text-decoration: underline;" aria-current="page">Resend verification code for completed form</a>
            </c:when>
        </c:choose>
        <a href="/our-plans/direct-value-term/resend-form-multi/" style="text-decoration: underline;" aria-current="page">Select form</a>
    </nav>
</div>


<div class="main">
    <article>
        <section class="directvalueterm verification
            <c:out value="${(errors.containsKey('success'))&&(errors.containsKey('retrieve_pass_code'))? 'modalcodesent' : (errors.containsKey('success')) ? 'modalformsent':''}"></c:out>">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <form method="post">

                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-sm-10">
                                        <p class="intro">
                                            <c:choose>
                                                <c:when test="${data['retrieve_pass_code']!= null}">
                                                    You have multiple forms associated with the email address and mobile number provided. <br />
                                                    Please select the form that you need the verification code for.
                                                </c:when>
                                                <c:otherwise>
                                                    You have multiple forms associated with the email address and mobile number provided. <br />
                                                    Please select the form that you would like to receive.
                                                </c:otherwise>
                                            </c:choose>
                                        </p>
                                    </legend>
                                    <br />
                                    <div class="col-xs-12 col-md-8 form-group <c:out value="${(errors.containsKey('form_self')) ? 'error' : ''}"></c:out>">
                                        <p>
                                            <c:if test="${errors.containsKey('not_choose')}">
                                            <span class="error-message" aria-label="Error">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['not_choose']}"></c:out>
                                            </span>
                                            </c:if>
                                        </p>

                                        <c:forEach var="applicant_name" varStatus="arr_count" items="${data['applicant_map']}" >
                                            <div class="newradio--inline">
                                                <input id="${applicant_name.key}" type="radio" name="form_self" value="${applicant_name.key}" <c:if test="${data['form_self']}">checked</c:if>>
                                                <label for="${applicant_name.key}"><span><span></span></span>HSBC Insurance Direct - ValueTerm for &lt;${applicant_name.value}&gt;</label>
                                            </div>
                                        </c:forEach>
                                        <c:if test="${errors.containsKey('form_self')}">
                                            <div>
                                                <span class="error-message">
                                                    <i class="icon icon-circle-delete"></i>
                                                    <c:out value="${errors['form_self']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </fieldset>
                            </div>


                            <div class="row">
                                <div class="col-xs-12 col-sm-8">
                                    <div class="secondary-buttons">
                                        <span><a href="/ourplans" onclick="window.history.back();return false;"><i class="icon icon-chevron-left"></i> Back</a></span>
                                    </div>

                                    <div class="primary-button">
                                        <div>
                                            <a class="btn btn-red" href="#" data-form-action="submit">Continue</a>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>


<jsp:include page="/WEB-INF/ourplans/includes/modal_form_sent.jsp">
    <jsp:param name="applicationType" value="direct-value-term" />
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/includes/modal_code_sent.jsp">
    <jsp:param name="applicationType" value="direct-value-term" />
</jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>