<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-eligibility"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<c:set var="data" scope="request" value="${data}"></c:set>
<c:set var="errors" scope="request" value="${errors}"></c:set>

<div class="main directvalueterm-step">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12">
                        <form class="buynow-form" method="post" data-form-name="directvalueterm-step" data-form-dropout="<c:out value="${(errors.containsKey('dropout')) ? '1' : ''}"></c:out>">
                            <h2><c:out value="${application.getApplicantFirstName()}"></c:out>, we need some information about you in order to proceed with the application</h2>
                            <div class="row">
                                <div class="col-xs-12 col-sm-9 col-md-8">

                                    <jsp:include page="/WEB-INF/ourplans/includes/generic_nationality_select.jsp">
                                        <jsp:param name="label" value="What is your country of nationality?"></jsp:param>
                                        <jsp:param name="key" value="applicant_nationality"></jsp:param>
                                    </jsp:include>

                                    <div class="row">
                                        <fieldset>
                                            <legend class="col-xs-12 col-md-5">Do you have multiple nationalities?</legend>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_multiple_nationalities')) ? 'error' : ''}"></c:out>" data-field-name="applicant_multiple_nationalities">
                                                <div class="newradio--inline">
                                                    <input id="id_multiple_nationalities_option1" type="radio" name="applicant_multiple_nationalities" value="Y" <c:if test="${data['applicant_multiple_nationalities']}">checked</c:if>>
                                                    <label for="id_multiple_nationalities_option1"><span><span></span></span>Yes</label>
                                                </div>
                                                <div class="newradio--inline">
                                                    <input id="id_multiple_nationalities_option2" type="radio" name="applicant_multiple_nationalities" value="N" <c:if test="${data['applicant_multiple_nationalities'] eq false}">checked</c:if> aria-describedby="applicantMultipleNationalities">
                                                    <label for="id_multiple_nationalities_option2"><span><span></span></span>No</label>
                                                </div>
                                                <c:if test="${errors.containsKey('applicant_multiple_nationalities')}">
                                                    <div>
                                                        <span class="error-message" aria-label="Error" id="applicantMultipleNationalities">
                                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['applicant_multiple_nationalities']}"></c:out>
                                                        </span>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <jsp:include page="/WEB-INF/ourplans/includes/generic_nationality_select.jsp">
                                        <jsp:param name="label" value="Country of nationality 2"></jsp:param>
                                        <jsp:param name="key" value="applicant_nationality2"></jsp:param>
                                        <jsp:param name="hidden" value="true"></jsp:param>
                                    </jsp:include>

                                    <jsp:include page="/WEB-INF/ourplans/includes/generic_nationality_select.jsp">
                                        <jsp:param name="label" value="Country of nationality 3"></jsp:param>
                                        <jsp:param name="key" value="applicant_nationality3"></jsp:param>
                                        <jsp:param name="hidden" value="true"></jsp:param>
                                    </jsp:include>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-5"><label for="form-birthcountry" id="form-birthcountry-label">What is your country of birth?</label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_birth_country')) ? 'error' : ''}"></c:out>" data-field-name="applicant_birth_country">
                                            <div class="form-select_wrap">
                                                <select name="applicant_birth_country" id="form-birthcountry" class="form-control" aria-describedby="applicantCountryBirth">
                                                    <option value="">Please select...</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Afghanistan'}">selected</c:if>>Afghanistan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Albania'}">selected</c:if>>Albania</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Algeria'}">selected</c:if>>Algeria</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'American Samoa'}">selected</c:if>>American Samoa</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Andorra'}">selected</c:if>>Andorra</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Angola'}">selected</c:if>>Angola</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Anguilla'}">selected</c:if>>Anguilla</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Antarctica'}">selected</c:if>>Antarctica</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Antigua and Barbuda'}">selected</c:if>>Antigua and Barbuda</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Argentina'}">selected</c:if>>Argentina</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Armenia'}">selected</c:if>>Armenia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Aruba'}">selected</c:if>>Aruba</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Australia'}">selected</c:if>>Australia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Austria'}">selected</c:if>>Austria</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Azerbaijan'}">selected</c:if>>Azerbaijan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bahama'}">selected</c:if>>Bahama</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bahrain'}">selected</c:if>>Bahrain</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bangladesh'}">selected</c:if>>Bangladesh</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Barbados'}">selected</c:if>>Barbados</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Belarus'}">selected</c:if>>Belarus</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Belgium'}">selected</c:if>>Belgium</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Belize'}">selected</c:if>>Belize</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Benin'}">selected</c:if>>Benin</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bermuda'}">selected</c:if>>Bermuda</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bhutan'}">selected</c:if>>Bhutan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bolivia'}">selected</c:if>>Bolivia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bosnia and Herzegovina'}">selected</c:if>>Bosnia and Herzegovina</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Botswana'}">selected</c:if>>Botswana</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bouvet Island'}">selected</c:if>>Bouvet Island</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Brazil'}">selected</c:if>>Brazil</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'British Indian Ocean Territory'}">selected</c:if>>British Indian Ocean Territory</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'British Virgin Islands'}">selected</c:if>>British Virgin Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Brunei Darussalam'}">selected</c:if>>Brunei Darussalam</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Bulgaria'}">selected</c:if>>Bulgaria</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Burkina Faso'}">selected</c:if>>Burkina Faso</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Burundi'}">selected</c:if>>Burundi</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Cambodia'}">selected</c:if>>Cambodia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Cameroon'}">selected</c:if>>Cameroon</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Canada'}">selected</c:if>>Canada</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Cape Verde'}">selected</c:if>>Cape Verde</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Cayman Islands'}">selected</c:if>>Cayman Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Central African Republic'}">selected</c:if>>Central African Republic</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Chad'}">selected</c:if>>Chad</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Chile'}">selected</c:if>>Chile</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'China'}">selected</c:if>>China</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Christmas Island'}">selected</c:if>>Christmas Island</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Cocos (Keeling) Islands'}">selected</c:if>>Cocos (Keeling) Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Colombia'}">selected</c:if>>Colombia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Comoros'}">selected</c:if>>Comoros</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Congo'}">selected</c:if>>Congo</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Cook Islands'}">selected</c:if>>Cook Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Costa Rica'}">selected</c:if>>Costa Rica</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Croatia'}">selected</c:if>>Croatia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Cuba'}">selected</c:if>>Cuba</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Cyprus'}">selected</c:if>>Cyprus</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Czech Republic'}">selected</c:if>>Czech Republic</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Denmark'}">selected</c:if>>Denmark</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Djibouti'}">selected</c:if>>Djibouti</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Dominica'}">selected</c:if>>Dominica</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Dominican Republic'}">selected</c:if>>Dominican Republic</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'East Timor'}">selected</c:if>>East Timor</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Ecuador'}">selected</c:if>>Ecuador</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Egypt'}">selected</c:if>>Egypt</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'El Salvador'}">selected</c:if>>El Salvador</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Equatorial Guinea'}">selected</c:if>>Equatorial Guinea</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Eritrea'}">selected</c:if>>Eritrea</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Estonia'}">selected</c:if>>Estonia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Ethiopia'}">selected</c:if>>Ethiopia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Falkland Islands (Malvinas)'}">selected</c:if>>Falkland Islands (Malvinas)</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Faroe Islands'}">selected</c:if>>Faroe Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Fiji'}">selected</c:if>>Fiji</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Finland'}">selected</c:if>>Finland</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'France'}">selected</c:if>>France</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'France, Metropolitan'}">selected</c:if>>France, Metropolitan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'French Guiana'}">selected</c:if>>French Guiana</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'French Polynesia'}">selected</c:if>>French Polynesia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'French Southern Territories'}">selected</c:if>>French Southern Territories</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Gabon'}">selected</c:if>>Gabon</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Gambia'}">selected</c:if>>Gambia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Georgia'}">selected</c:if>>Georgia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Germany'}">selected</c:if>>Germany</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Ghana'}">selected</c:if>>Ghana</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Gibraltar'}">selected</c:if>>Gibraltar</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Greece'}">selected</c:if>>Greece</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Greenland'}">selected</c:if>>Greenland</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Grenada'}">selected</c:if>>Grenada</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Guadeloupe'}">selected</c:if>>Guadeloupe</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Guam'}">selected</c:if>>Guam</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Guatemala'}">selected</c:if>>Guatemala</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Guinea'}">selected</c:if>>Guinea</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Guinea-Bissau'}">selected</c:if>>Guinea-Bissau</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Guyana'}">selected</c:if>>Guyana</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Haiti'}">selected</c:if>>Haiti</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Heard Island and McDonald Islands'}">selected</c:if>>Heard Island and McDonald Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Honduras'}">selected</c:if>>Honduras</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Hong Kong'}">selected</c:if>>Hong Kong</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Hungary'}">selected</c:if>>Hungary</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Iceland'}">selected</c:if>>Iceland</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'India'}">selected</c:if>>India</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Indonesia'}">selected</c:if>>Indonesia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Islamic Republic of Iran'}">selected</c:if>>Islamic Republic of Iran</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Iraq'}">selected</c:if>>Iraq</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Ireland'}">selected</c:if>>Ireland</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Israel'}">selected</c:if>>Israel</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Italy'}">selected</c:if>>Italy</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Jamaica'}">selected</c:if>>Jamaica</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Japan'}">selected</c:if>>Japan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Jordan'}">selected</c:if>>Jordan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Kazakhstan'}">selected</c:if>>Kazakhstan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Kenya'}">selected</c:if>>Kenya</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Kiribati'}">selected</c:if>>Kiribati</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Korea, Democratic People\\\'s Republic of'}">selected</c:if>>Korea, Democratic People's Republic of</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Korea, Republic of'}">selected</c:if>>Korea, Republic of</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Kuwait'}">selected</c:if>>Kuwait</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Kyrgyzstan'}">selected</c:if>>Kyrgyzstan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Lao People\\\'s Democratic Republic'}">selected</c:if>>Lao People's Democratic Republic</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Latvia'}">selected</c:if>>Latvia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Lebanon'}">selected</c:if>>Lebanon</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Lesotho'}">selected</c:if>>Lesotho</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Liberia'}">selected</c:if>>Liberia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Libyan Arab Jamahiriya'}">selected</c:if>>Libyan Arab Jamahiriya</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Liechtenstein'}">selected</c:if>>Liechtenstein</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Lithuania'}">selected</c:if>>Lithuania</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Luxembourg'}">selected</c:if>>Luxembourg</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Macau'}">selected</c:if>>Macau</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Madagascar'}">selected</c:if>>Madagascar</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Malawi'}">selected</c:if>>Malawi</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Malaysia'}">selected</c:if>>Malaysia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Maldives'}">selected</c:if>>Maldives</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Mali'}">selected</c:if>>Mali</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Malta'}">selected</c:if>>Malta</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Marshall Islands'}">selected</c:if>>Marshall Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Martinique'}">selected</c:if>>Martinique</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Mauritania'}">selected</c:if>>Mauritania</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Mauritius'}">selected</c:if>>Mauritius</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Mayotte'}">selected</c:if>>Mayotte</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Mexico'}">selected</c:if>>Mexico</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Micronesia'}">selected</c:if>>Micronesia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Moldova, Republic of'}">selected</c:if>>Moldova, Republic of</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Monaco'}">selected</c:if>>Monaco</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Mongolia'}">selected</c:if>>Mongolia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Monserrat'}">selected</c:if>>Monserrat</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Morocco'}">selected</c:if>>Morocco</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Mozambique'}">selected</c:if>>Mozambique</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Myanmar'}">selected</c:if>>Myanmar</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Nambia'}">selected</c:if>>Nambia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Nauru'}">selected</c:if>>Nauru</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Nepal'}">selected</c:if>>Nepal</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Netherlands'}">selected</c:if>>Netherlands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Netherlands Antilles'}">selected</c:if>>Netherlands Antilles</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'New Caledonia'}">selected</c:if>>New Caledonia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'New Zealand'}">selected</c:if>>New Zealand</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Nicaragua'}">selected</c:if>>Nicaragua</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Niger'}">selected</c:if>>Niger</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Nigeria'}">selected</c:if>>Nigeria</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Niue'}">selected</c:if>>Niue</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Norfolk Island'}">selected</c:if>>Norfolk Island</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Northern Mariana Islands'}">selected</c:if>>Northern Mariana Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Norway'}">selected</c:if>>Norway</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Oman'}">selected</c:if>>Oman</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Pakistan'}">selected</c:if>>Pakistan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Palau'}">selected</c:if>>Palau</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Panama'}">selected</c:if>>Panama</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Papua New Guinea'}">selected</c:if>>Papua New Guinea</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Paraguay'}">selected</c:if>>Paraguay</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Peru'}">selected</c:if>>Peru</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Philippines'}">selected</c:if>>Philippines</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Pitcairn'}">selected</c:if>>Pitcairn</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Poland'}">selected</c:if>>Poland</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Portugal'}">selected</c:if>>Portugal</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Puerto Rico'}">selected</c:if>>Puerto Rico</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Qatar'}">selected</c:if>>Qatar</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Romania'}">selected</c:if>>Romania</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Russian Federation'}">selected</c:if>>Russian Federation</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Rwanda'}">selected</c:if>>Rwanda</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'S. Georgia & the S. Sandwich Islands'}">selected</c:if>>S. Georgia & the S. Sandwich Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Saint Lucia'}">selected</c:if>>Saint Lucia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Samoa'}">selected</c:if>>Samoa</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'San Marino'}">selected</c:if>>San Marino</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Sao Tome and Principe'}">selected</c:if>>Sao Tome and Principe</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Saudi Arabia'}">selected</c:if>>Saudi Arabia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Senegal'}">selected</c:if>>Senegal</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Seychelles'}">selected</c:if>>Seychelles</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Sierra Leone'}">selected</c:if>>Sierra Leone</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Slovakia'}">selected</c:if>>Slovakia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Slovenia'}">selected</c:if>>Slovenia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Solomon Islands'}">selected</c:if>>Solomon Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Somalia'}">selected</c:if>>Somalia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'South Africa'}">selected</c:if>>South Africa</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Spain'}">selected</c:if>>Spain</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Sri Lanka'}">selected</c:if>>Sri Lanka</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'St. Helena'}">selected</c:if>>St. Helena</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'St. Kitts and Nevis'}">selected</c:if>>St. Kitts and Nevis</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'St. Pierre and Miquelon'}">selected</c:if>>St. Pierre and Miquelon</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'St. Vincent and the Grenadines'}">selected</c:if>>St. Vincent and the Grenadines</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Sudan'}">selected</c:if>>Sudan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Suriname'}">selected</c:if>>Suriname</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Svalbard and Jan Mayen Islands'}">selected</c:if>>Svalbard and Jan Mayen Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Swaziland'}">selected</c:if>>Swaziland</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Sweden'}">selected</c:if>>Sweden</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Switzerland'}">selected</c:if>>Switzerland</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Syrian Arab Republic'}">selected</c:if>>Syrian Arab Republic</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Taiwan, Province of China'}">selected</c:if>>Taiwan, Province of China</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Tajikistan'}">selected</c:if>>Tajikistan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Thailand'}">selected</c:if>>Thailand</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Togo'}">selected</c:if>>Togo</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Tokelau'}">selected</c:if>>Tokelau</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Tonga'}">selected</c:if>>Tonga</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Trinidad and Tobago'}">selected</c:if>>Trinidad and Tobago</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Tunisia'}">selected</c:if>>Tunisia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Turkey'}">selected</c:if>>Turkey</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Turkmenistan'}">selected</c:if>>Turkmenistan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Turks and Caicos Islands'}">selected</c:if>>Turks and Caicos Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Tuvalu'}">selected</c:if>>Tuvalu</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Uganda'}">selected</c:if>>Uganda</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Ukraine'}">selected</c:if>>Ukraine</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'United Arab Emirates'}">selected</c:if>>United Arab Emirates</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'United Kingdom (Great Britain)'}">selected</c:if>>United Kingdom (Great Britain)</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'United Republic of Tanzania'}">selected</c:if>>United Republic of Tanzania</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'United States of America'}">selected</c:if>>United States of America</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'United States Minor Outlying Islands'}">selected</c:if>>United States Minor Outlying Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'United States Virgin Islands'}">selected</c:if>>United States Virgin Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Uruguay'}">selected</c:if>>Uruguay</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Uzbekistan'}">selected</c:if>>Uzbekistan</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Vanuatu'}">selected</c:if>>Vanuatu</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Vatican City State (Holy See)'}">selected</c:if>>Vatican City State (Holy See)</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Venezuela'}">selected</c:if>>Venezuela</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Viet Nam'}">selected</c:if>>Viet Nam</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Wallis and Futuna Islands'}">selected</c:if>>Wallis and Futuna Islands</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Western Sahara'}">selected</c:if>>Western Sahara</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Yemen'}">selected</c:if>>Yemen</option><%--JIRA HSBCIDCU-671 --%>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Yugoslavia'}">selected</c:if>>Yugoslavia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Zaire'}">selected</c:if>>Zaire</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Zambia'}">selected</c:if>>Zambia</option>
                                                    <option <c:if test="${data['applicant_birth_country'] == 'Zimbabwe'}">selected</c:if>>Zimbabwe</option>
                                                    <option value="NA" <c:if test="${data['applicant_birth_country'] == 'NA'}">selected</c:if>>None of the above</option>
                                                </select>
                                            </div>
                                            <c:if test="${errors.containsKey('applicant_birth_country')}">
                                                <span class="error-message" aria-label="Error" id="applicantCountryBirth">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_birth_country']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5"><label for="form-nric">What is your <c:choose><c:when test="${application.applicantIsSingaporean() || application.applicantIsPermanentResident()}">NRIC</c:when><c:otherwise>passport number</c:otherwise></c:choose>?</label></div>
        								<c:choose>
        									<c:when
        										test="${application.applicantIsSingaporean() || application.applicantIsPermanentResident()}">
        										<div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_nric')) ? 'error' : ''}"></c:out>">
        											<input name="applicant_nric" id="form-nric" class="form-control" type="text"
                                                        value="${fn:escapeXml(data['applicant_nric'])}"
                                                        aria-describedby="applicantNRIC"
                                                        maxlength="9">
                                                    <c:if test="${errors.containsKey('applicant_nric')}">
                                                        <span class="error-message" aria-label="Error" id="applicantNRIC">
                                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['applicant_nric']}"></c:out>
                                                        </span>
                                                    </c:if>
        										</div>
        									</c:when>
        									<c:otherwise>
        										<div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_passport')) ? 'error' : ''}"></c:out>">
        											<input name="applicant_nric" id="form-nric" class="form-control" type="text"
                                                        value="${fn:escapeXml(data['applicant_nric'])}"
                                                        aria-describedby="applicantNRIC"
                                                        maxlength="20">
                                                    <c:if test="${errors.containsKey('applicant_passport')}">
                                                        <span class="error-message" aria-label="Error" id="applicantNRIC">
                                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['applicant_passport']}"></c:out>
                                                        </span>
                                                    </c:if>
        										</div>
        									</c:otherwise>
        								</c:choose>
                                    </div>

                                    <div class="row">
                                        <fieldset>
                                            <legend class="col-xs-12 col-md-5">Are you proficient in the English Language (spoken and written) and have you completed at least a secondary school education or its equivalent?</legend>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_en_proficient')) ? 'error' : ''}"></c:out>">
                                                <div class="newradio--inline">
                                                    <input id="id_en_proficient_option1" type="radio" name="applicant_en_proficient" value="Y" <c:if test="${data['applicant_en_proficient']}">checked</c:if>>
                                                    <label for="id_en_proficient_option1"><span><span></span></span>Yes</label>
                                                </div>
                                                <div class="newradio--inline">
                                                    <input id="id_en_proficient_option2" type="radio" name="applicant_en_proficient" value="N" <c:if test="${data['applicant_en_proficient'] eq false}">checked</c:if> aria-describedby="applicantEn">
                                                    <label for="id_en_proficient_option2"><span><span></span></span>No</label>
                                                </div>
                                                <c:if test="${errors.containsKey('applicant_en_proficient')}">
                                                    <div>
                                                        <span class="error-message" aria-label="Error" id="applicantEn">
                                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['applicant_en_proficient']}"></c:out>
                                                        </span>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-9">
                                    <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/direct-value-term/3">Back</button>
                                    <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
