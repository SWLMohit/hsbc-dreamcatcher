<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" errorPage="/WEB-INF/ourplans/includes/showError.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<p>In proceeding to make this purchase you are confirming that this product is an appropriate solution for your needs.</p>

<ul>
    <li>I understand that I am purchasing DIRECT-ValueTerm plan ("the Product") and the DIRECT-Critical Illness Rider ("the CI Rider") without seeking advice from any Financial Advisory representative. I understand that I am encouraged to go through the following items before I determine if this Insurance policy meets my financial circumstances and needs:
        <ul>
            <li><a href="https://www.cpf.gov.sg/eSvc/Web/Schemes/InsuranceEstimator/InsuranceEstimator" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">Insurance Estimator</a> to calculate the amount of life insurance coverage I would need</li>
            <li><a href="http://www.moneysense.gov.sg/financial-planning/financial-calculators/budget-calculator.aspx" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">Budget Calculator</a> to check if the premium payable is affordable based on my current income and expenditure</li>
            <li><a href="http://www.comparefirst.sg" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">Comparefirst</a> to compare the features and premiums of Direct Purchase Insurance (DPI) and other types of life policies</li>
            <li>Considered the different types of DPI and other types of life insurance products that are available and whether the insurance policy that I intend to purchase is suitable for my financial circumstances and needs</li>
        </ul>
    </li>
    <li><b>I understand the nature, objective and benefits of the Product and the CI Rider and have read and acknowledged following information</b></li>
        <ul>
            <li>in the <a href="/our-plans/direct-value-term/bi" aria-label="Download Benefit Illustration in PDF format" target="_blank">
                <i class="icon icon-download" aria-hidden="true"></i>
                <span style="color:red;text-decoration: underline;">Benefit Illustration</span>
                </a>
            </li>
            <li>in the 
                <a
                    <c:choose>
                       <c:when test="${requestScope.application.getTermYears() == 5}">href="/assets/downloads/GPPSDirectValueTerm5.pdf"</c:when>
                       <c:otherwise>href="/assets/downloads/GPPSDirectValueTerm20Age65.pdf"</c:otherwise>
                    </c:choose>
                    target="_blank" aria-label="Download Product Summary and General Provisions in PDF format" data-tag-file-download="Product Summary and General Provisions">
                    <i class="icon icon-download" aria-hidden="true"></i>
                    <span style="color:red;text-decoration: underline;">Product Summary and General Provision</span>
                </a>
            </li>
            <li>in the <a href="/assets/downloads/FXDPI.pdf" target="_blank" aria-label="Download DIRECT - ValueTerm Fact Sheet in PDF format" data-tag-file-download="DIRECT - ValueTerm Fact Sheet">
                <i class="icon icon-download" aria-hidden="true"></i>
                <span style="color:red;text-decoration: underline;">DIRECT-ValueTerm Fact Sheet</span>
                </a>
            </li>
            <li>in the <a href="/assets/downloads/AXPSDirectCI.pdf" target="_blank" aria-label="Download Critical Illness Rider Product Summary & Annexure in PDF format" data-tag-file-download="Critical Illness Rider Product Summary & Annexure (PDF)">
                <i class="icon icon-download" aria-hidden="true"></i>
                <span style="color:red;text-decoration: underline;">Product Summary and Annexure related to DIRECT-Critical Illness Rider</span>
                </a>
            </li>
         </ul>
    </ul>
    <p style="margin-left: 23px;">I am also aware that I will be receiving a copy of the documents in PDF listed above via e-mail upon completion of my application.</p>
    <ul>
    <li>I understand the features and the price difference between the Product versus the Product with CI Rider included</li>
    <li>I understand and acknowledge the exclusions and risks of the Product, which include, but are not limited to, the following, that:
        <ul>
            <li>I may lose my protection cover if I do not make my premium payment when due</li>
            <c:if test="${requestScope.application.getTermYears() == 5}">
               <li>whilst the premiums for the Product in the first 5 years are guaranteed, the renewal premiums are not guaranteed</li>
            </c:if>
            <li>the policy will exclude Total and Permanent Disability Benefit if my job or leisure pursuits involve working at heights, working underground, using explosives or chemicals, motor racing, flying other than as a fare paying passenger on a commercial airline, diving below 30m or mountain climbing (4,000m and above)</li>
            <li>the policy will exclude any claim related directly or indirectly to any pre-existing condition (as defined in the Policy General Provisions) which I have not disclosed to HSBC Insurance</li>
         </ul>
    </li>
    
    <li>I understand and acknowledge the exclusions and risks of the CI Rider which include, but are not limited to, the following, that: 
        <ul>
            <li>no payout will be made for critical illness claim(s) due to Major Cancers, Heart Attack of Specified Severity, Coronary Artery By-pass Surgery or Angioplasty and Other Invasive Treatment for Coronary Artery which is diagnosed within the first 90 days of the policy issuance or reinstatement and claim(s) due to any illnesses which existed before I applied for this policy</li>
            <li>critical Illness claims are admissible only if it meets the critical illness definitions as prescribed by the Life Insurance Association for Singapore which is available in the policy document</li>
            <li>the premiums for the CI Rider is not guaranteed</li>
            <li>the policy will exclude any claim related directly or indirectly to any pre-existing condition (as defined in the Critical Illness Benefit Exclusions Section) which I have not disclosed to HSBC Insurance</li>
        </ul>
    </li>
</ul>


<div class="space40"></div>
