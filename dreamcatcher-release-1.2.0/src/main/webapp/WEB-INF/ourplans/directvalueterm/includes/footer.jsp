<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<div class="modal fade" id="buynow-timeout-modal" tabindex="-1" role="dialog"  aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Sorry</h4>
            </div>
            <div class="modal-body">
                You will be logout in 2 minutes' time. Click Continue to dismiss this message.
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn">Continue</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="buynow-dropout-modal" tabindex="-1" role="dialog"  aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Sorry</h4>
            </div>
            <div class="modal-body">
                <p>Based on your response(s), we are unable to proceed with your online application.</p>
                <div class="add1" style="display:none;">
                    <p>However, we recommend that you visit the nearest HSBC Branch or complete this <a href="https://www.apps.asiapacific.hsbc.com/1/2/sgh2/insn-cam-contact-us?WABFormEntryCommand=cmd_init&hidden.type=Dreamcatcher" target="_blank" style="text-decoration:underline;">contact form</a> so that our financial advisors can provide you with financial advice.</p>
                    <p>Please note that the charges, features and benefits for advised products will be different from products offered on this online insurance platform.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn">Back to application</button>
            </div>
        </div>
    </div>
</div>


<jsp:include page="/WEB-INF/includes/exit_modal_product.jsp">
    <jsp:param name="modal" value="dvt" />
</jsp:include>