<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<p>
    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">

        <input type="checkbox" name="declare_accept2" value="Y" aria-describedby="ack1" <c:if test="${data['declare_accept2'] == 'Y'}">checked</c:if>>

        <span></span>
        I am aware that the product that I am applying for is authorised for sale only in Singapore. I acknowledge that I am 
        responsible for ensuring, and I represent and warrant, that I am in compliance with all the laws and regulations applicable 
        to my nationality(ies) and country(ies) of residence and that such laws and regulations allow my purchase of and 
        payments under this product. I understand that no liability can or will be accepted by HSBC Insurance for any 
        consequences under the laws of any country other than Singapore or any tax implications that may arise in connection 
        with my purchase of this product.
    </label>
    <c:if test="${errors.containsKey('declare_accept2')}">
        <div class="error-message" style="padding-bottom:10px;" aria-label="Error"  id="ack1">

            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            <c:out value="${errors['declare_accept2']}"></c:out>
        </div>
    </c:if>
</p>

<p>
    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
        <input type="checkbox" name="declare_accept3" value="Y"  aria-describedby="ack2" <c:if test="${data['declare_accept3'] == 'Y'}">checked</c:if>>

        <span></span>
        I declare that:
    </label>
    <c:if test="${errors.containsKey('declare_accept3')}">
        <div class="error-message" style="padding-bottom:10px;" aria-label="Error" id="ack2">

            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            <c:out value="${errors['declare_accept3']}"></c:out>
        </div>
    </c:if>
</p>

<div class="checkbox-declare-indent">
    <ul>
        <li intended="true">I am not an undischarged bankrupt</li>
        <li intended="true">I am agreeable to inform HSBC Insurance if there is any change in the state of health, occupation or activity of the Life Insured and Policyowner between the date of this online application and the issue of my policy.  On receiving this information, HSBC Insurance is entitled to accept or reject my application</li>
        <li intended="true">I have reviewed all existing life insurance policies that I own, or I am in the process of applying for in the application form</li>
        <li intended="true">I am aware that it is generally disadvantageous to replace an existing life insurance policy with a new one as, amongst others, I may lose the financial benefit accumulated over the years</li>
        <li intended="true"><span style="text-decoration: underline;"><b>I have initiated the purchase of this product and the entire application is made while I am a resident in Singapore</b></span></li>
        <c:if test="${application.getApplicantNationality() == 'Japan' or application.getApplicantNationality2() == 'Japan' or application.getApplicantNationality3() == 'Japan'}">
            <li intended="true">I am agreeable to complete the Declaration of Non-Japanese Residency in the Supplementary Proposal Form as part of my application for this policy</li>
        </c:if>
        <li intended="true">
            in compliance with US laws and regulations and other laws having extra-territorial effect:
            <ul>
                <li inner-intended="true">I am not (and will not be) physically located in US throughout the entire policy application process through to policy issuance</li>
                <li inner-intended="true">I am aware of and understand the policy servicing restrictions* applicable to any and all persons residing temporarily or permanently in the US (*List of policy servicing restrictions is set out in our <a href="http://www.insurance.hsbc.com.sg/1/2/sghi/customer-service" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">website</a>)</li>
                <li inner-intended="true">I will inform HSBC Insurance should I decide to reside in the US either temporarily or permanently</li>
            </ul>

        </li>
        <li intended="true">
             all my existing and pending DIRECT–ValueTerm applications do not exceed an aggregate coverage of S$400,000 per life          
        </li>
        <li intended="true"><span style="color:red">the information given by me to HSBC Insurance in this application form or in any form or document relating to this application is true, complete and accurate. I have not withheld any material facts or information (i.e. facts or information that are likely to influence the assessment and acceptance of the application). If anything is found to be untrue, incorrect or incomplete, the insurance policy will be cancelled and void</span></li>
        <li intended="true">I am aware that if a material fact is not disclosed in this application, any policy issued may be void. I understand that if I am in doubt whether a fact is material, I am advised to disclose it</li>
        <li intended="true">I am aware that I can view or download the following LIA consumer disclosure guides for my reference
            <ul>
            <li inner-intended="true">
                <a href="/assets/downloads/Your_Guide_to_Life_Insurance.pdf" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;" aria-label="Download Your Guide to Life Insurance in PDF format">
                    <i class="icon icon-download" aria-hidden="true"></i>
                    <span>Your Guide to Life Insurance</span>
                </a>
            </li>
            <li inner-intended="true">
                <a href="/assets/downloads/Your_Guide_to_Health_Insurance.pdf" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;" aria-label="Download Your Guide to Health Insurance (if applicable) in PDF format">
                    <i class="icon icon-download" aria-hidden="true"></i>
                    <span>Your Guide to Health Insurance (if applicable)</span>
                </a>
            </li>
            </ul>
        </li>
    </ul>
</div>
<div class="space20"></div>

<div>
    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
        <input type="checkbox" name="declare_accept4" value="Y" <c:if test="${data['declare_accept4'] == 'Y'}">checked</c:if>>
        <span></span>
        <b>I <c:out value="${application.getApplicantFullName()}"></c:out> agree with the statement below</b>
    </label>
    <c:if test="${errors.containsKey('declare_accept4')}">
        <div class="error-message" style="padding-bottom:10px;">
            <i class="icon icon-circle-delete"></i>
            <c:out value="${errors['declare_accept4']}"></c:out>
        </div>
    </c:if>
</div>

<div class="checkbox-declare-indent">
If I am a customer of, or become a customer of, HSBC Bank (Singapore) Limited ("HSBC Bank"), I agree that 
HSBC Insurance may disclose my personal data provided in this form to HSBC Bank, to enable HSBC Bank to 
provide me with a consolidated view of all the products I may hold with HSBC Bank, HSBC Insurance and (where 
relevant) any other member of the HSBC Group.
</div>

<div>
    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
        <input type="checkbox" name="declare_accept5" value="Y" checked>
        <span></span>
        <b>I <c:out value="${application.getApplicantFullName()}"></c:out> agree with the statement below</b>
    </label>
    <c:if test="${errors.containsKey('declare_accept5')}">
        <div class="error-message" style="padding-bottom:10px;">
            <i class="icon icon-circle-delete"></i>
            <c:out value="${errors['declare_accept5']}"></c:out>
        </div>
    </c:if>
</div>

<div class="checkbox-declare-indent">
I consent to the use and disclosure of my personal data provided in this form to the HSBC Group and its respective 
agents, authorised service parties and relevant third parties for the purpose of sending me news and alerts on new 
products and special promotions offered by HSBC Insurance and its group companies.
</div>
<div class="space20"></div>