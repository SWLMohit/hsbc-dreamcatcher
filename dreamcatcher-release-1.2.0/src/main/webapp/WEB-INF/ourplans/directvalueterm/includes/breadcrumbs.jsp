<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/our-plans/">Our plans</a>
        <a href="/our-plans/direct-value-term/">HSBC Insurance DIRECT - ValueTerm</a>
        <a style="text-decoration: underline;" aria-current="page">HSBC Insurance DIRECT - ValueTerm application</a>
    </nav>
</div>
