<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-feedbackreceived"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12 col-sm-8">
                        <form class="buynow-form" method="post">
							<c:choose>
							  <c:when test="${application.isFeedbackCompleted()}">
							    <fieldset>
							       <legend>Thank you for completing the survey</legend>
							    </fieldset>
							    <p>Your feedback will help us to improve our site and serve you better. Have an inquiry? Find out how you can reach us by clicking on the 'Contact us' link below.</p>
							  </c:when>
							  <c:otherwise>
							    <fieldset>
							       <legend>Thank you for insuring with us</legend>
							    </fieldset>
							    <p>Have an inquiry? Find out how you can reach us by clicking on the 'Contact us' link below.</p>
							  </c:otherwise>
							</c:choose>
                            <div>
                                <button type="button" class="back_btn" data-form-action="prev" data-url="/">Back to homepage</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
