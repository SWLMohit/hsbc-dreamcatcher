<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-acknowledgmentplayback"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<div class="main directvalueterm-step">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>

                    <form class="buynow-form" method="post" data-form-name="directvalueterm-step">
                        <div class="col-xs-12 col-sm-10">
                            <h2>Declaration</h2>


                            <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/ack2_plan.jsp"/>

                            <div>
                                <div class="space40"></div>

                                
                                <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false" data-field-name="declare_accept">

                                    <input type="checkbox" name="declare_accept" aria-describedby="declare1" value="Y" <c:if test="${data['declare_accept'] == 'Y'}">checked</c:if> >

                                    <span></span>
                                    <b>By submitting this application, I <c:out value="${application.getApplicantFullName()}"></c:out> acknowledge that I have 
                                    read, understood and agree with the statements above.</b>
                                </label>
                                <c:if test="${errors.containsKey('declare_accept')}">
                                    <div class="error-message" style="padding-bottom:10px;" aria-label="Error" id="declare1">
                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                        <c:out value="${errors['declare_accept']}"></c:out>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="space40"></div>
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/direct-value-term/12">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Submit</button>
                        </div>

                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
