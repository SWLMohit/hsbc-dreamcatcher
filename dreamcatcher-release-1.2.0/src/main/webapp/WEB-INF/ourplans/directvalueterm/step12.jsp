<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-acknowledgment"></jsp:param>
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/utag_tracking.jsp" flush="true"></jsp:include>

<div class="main directvalueterm-step">
    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="directvalueterm">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/header.jsp" flush="true"/>

                    <form class="buynow-form" method="post" data-form-name="directvalueterm-step">
                        <div class="col-xs-12 col-sm-10">
                            <h2>Acknowledgement</h2>

                            <c:choose>
                                <c:when test="${application.getIncludeCriticalIllnessRider()}">
                                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/ack1_plan_ci.jsp"/>
                                </c:when>
                                <c:otherwise>
                                    <jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/ack1_plan.jsp"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/direct-value-term/11">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                        </div>
                    </form>
                </div>
            </div>

            <jsp:include page="/WEB-INF/ourplans/includes/marketing-modal.jsp"/>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/directvalueterm/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
