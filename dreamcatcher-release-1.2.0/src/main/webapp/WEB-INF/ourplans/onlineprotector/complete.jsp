<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-thankyou&feedback"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12 col-sm-8">
                        <form class="buynow-form" method="post">
                            <h2>Thank you for insuring with us</h2>
                            <p>You will receive an SMS and email shortly acknowledging your submission. The email will also include details of the immediate coverage you are entitled to (subject to Terms and Conditions).</p>
                            <p>Please allow us 7  working days to review your application and process your payment.</p>

                            <div class="space40"></div>

                            <h3>We would like to hear from you and share your experience with our customers.</h3>
                            <dl>
                                <dt>Overall, how satisfied are you with your application experience?</dt>
                                <dd>
                                    <fieldset class="star-rating feedback-experience">
                                      <input value="0" id="op-star-exp-0" type="radio" name="op-rate-exp" class="visuallyhidden">
                                        <label for="op-star-exp-0" class="visuallyhidden">
                                            <span class="visuallyhidden">0 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 1 -->
                                      <input value="1" id="op-star-exp-1" type="radio" name="op-rate-exp" class="visuallyhidden">
                                        <label for="op-star-exp-1">
                                            <span class="visuallyhidden">1 Star</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 2 -->
                                      <input value="2" id="op-star-exp-2" type="radio" name="op-rate-exp" class="visuallyhidden">
                                        <label for="op-star-exp-2">
                                            <span class="visuallyhidden">2 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 3 -->
                                      <input value="3" id="op-star-exp-3" type="radio" name="op-rate-exp" class="visuallyhidden">
                                        <label for="op-star-exp-3">
                                            <span class="visuallyhidden">3 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 4 -->
                                      <input value="4" id="op-star-exp-4" type="radio" name="op-rate-exp" class="visuallyhidden" checked>
                                        <label for="op-star-exp-4">
                                            <span class="visuallyhidden">4 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 5 -->
                                      <input value="5" id="op-star-exp-5" type="radio" name="op-rate-exp" class="visuallyhidden">
                                        <label for="op-star-exp-5">
                                            <span class="visuallyhidden">5 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <button type="submit" class="visuallyhidden btn-op-exp">Submit rating</button>
                                    </fieldset>
                                </dd>

                                <dt>How simple was the application process?</dt>
                                <dd>
                                    <fieldset class="star-rating feedback-ease">
                                      <input value="0" id="op-star-ease-0" type="radio" name="op-rate-ease" class="visuallyhidden">
                                        <label for="op-star-ease-0" class="visuallyhidden">
                                            <span class="visuallyhidden">0 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 1 -->
                                      <input value="1" id="op-star-ease-1" type="radio" name="op-rate-ease" class="visuallyhidden">
                                        <label for="op-star-ease-1">
                                            <span class="visuallyhidden">1 Star</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 2 -->
                                      <input value="2" id="op-star-ease-2" type="radio" name="op-rate-ease" class="visuallyhidden">
                                        <label for="op-star-ease-2">
                                            <span class="visuallyhidden">2 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 3 -->
                                      <input value="3" id="op-star-ease-3" type="radio" name="op-rate-ease" class="visuallyhidden">
                                        <label for="op-star-ease-3">
                                            <span class="visuallyhidden">3 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 4 -->
                                      <input value="4" id="op-star-ease-4" type="radio" name="op-rate-ease" class="visuallyhidden" checked>
                                        <label for="op-star-ease-4">
                                            <span class="visuallyhidden">4 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <!-- Star 5 -->
                                      <input value="5" id="op-star-ease-5" type="radio" name="op-rate-ease" class="visuallyhidden">
                                        <label for="op-star-ease-5">
                                            <span class="visuallyhidden">5 Stars</span>
                                            <i class="star"></i>
                                        </label>

                                      <button type="submit" class="visuallyhidden btn-op-ease">Submit rating</button>
                                    </fieldset>
                                </dd>

                                <dt>Would you recommend a friend to apply for HSBC Insurance Online? If no, why?</dt>
                                <dd class="form-group">
                                    <div class="newradio--inline">
                                        <input id="id_feedback_recommend_option1" type="radio" name="feedback_recommend" value="Y">
                                        <label for="id_feedback_recommend_option1"><span><span></span></span>Yes</label>
                                    </div>
                                    <div class="newradio--inline">
                                        <input id="id_feedback_recommend_option2" type="radio" name="feedback_recommend" value="N">
                                        <label for="id_feedback_recommend_option2"><span><span></span></span>No</label>
                                    </div>
                                    <div data-question-group="feedback_norecommend">
                                        <textarea class="form-control" name="feedback_norecommend_reason"></textarea>
                                    </div>
                                </dd>

                                <dt>Is there anything we can do to improve our application process?</dt>
                                <dd>
                                    <textarea class="form-control" name="feedback_improve"></textarea>
                                </dd>

                                <dt>Any additional feedback?</dt>
                                <dd>
                                    <textarea class="form-control" name="feedback_additional"></textarea>
                                </dd>
                            </dl>
                            <div>
                                <button type="button" class="back_btn" data-form-action="prev" data-url="/">Back to homepage</button>
                                <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<style>
.rating span {
    cursor: pointer;
}
[data-question-group=feedback_norecommend] {
    margin-top: 10px;
}
</style>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
