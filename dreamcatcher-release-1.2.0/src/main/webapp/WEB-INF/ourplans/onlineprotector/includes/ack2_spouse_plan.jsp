<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<ul>
    <li>A life insurance policy is not a savings account or deposit</li>
    <li>We are aware that we may not get back the premiums paid (partially or in full) if we terminate or surrender the policy early</li>
    <li>We understand that there may be some benefits of a life insurance policy which are not guaranteed (only if applicable)</li>
    <li><b>We understand that we have a 30-day free-look period to cancel a policy after HSBC Insurance confirms 
    acceptance of our application at standard rates. If we cancel the policy within 30 days, HSBC Insurance will 
    refund the premium we paid without interest after deducting medical and underwriting expenses incurred in 
    accepting our application</b></li>
    <li>We are aware that we can request HSBC Insurance to explain any of the product features</li>
    <li>We understand that we can also separately seek advice on the suitability of the policy</li>
    <li>In the event that we choose not to seek advice on the suitability of the life insurance policy, we have considered and decided that it is 
    suitable for our financial circumstances and needs</li>
    <li>We have declared our current financial situation, such as income, in our application</li>
    <li><span style="color:red">We understand that the commencement of risk coverage under the policy is subject to the receipt by HSBC Insurance 
    of the first premium payment and the completion of internal administrative checks, such as customer due diligence</span></li>
</ul>
