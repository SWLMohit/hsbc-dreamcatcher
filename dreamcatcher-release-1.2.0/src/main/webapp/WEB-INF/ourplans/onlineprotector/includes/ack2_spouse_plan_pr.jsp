<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<h3>Futher Acknowledgements</h3>
<p>We acknowledge the following important terms and conditions:</p>
<ul>
    <li>A life insurance policy is not a savings account or deposit;</li>
    <li>We are aware that we may not get back the premiums paid (partially or in full) if we terminate or surrender the policy early;</li>
    <li>We understand that there may be some benefits of a life insurance policy which are not guaranteed.</li>
    <li>We have 30 days or such other period granted by HSBC Insurance to cancel our policy after HSBC Insurance confirms acceptance of the application.</li>
    <li>We are aware that we can request HSBC Insurance  to explain any of the product features.</li>
    <li>We understand that  we can also separately seek advice on the suitability  of the policy.</li>
    <li>In the event that we choose  not to seek advice on the suitability of the policy, we have considered and decided that it is suitable for our financial circumstances and needs.</li>
    <li>We understand that the policy will exclude any claim related directly or indirectly to any Pre-existing Condition (as defined in the Policy General Provisions or in the Critical Illness Benefit Exclusions Section) which I have not disclosed to HSBC Insurance.</li>
    <li>We are aware that the policy will exclude Total and Permanent Disability Benefit if the Life Insured's job or leisure pursuits involve working at heights, working underground, using explosives or chemicals, motor racing, flying other than as a fare paying passenger on a commercial airline, diving below 30m or mountain climbing (4,000m and above).</li>
    <li>We understand that the commencement of risk coverage under the policy is subject to the receipt by the Company of the first premium payment and the completion of internal administrative checks, such as customer due diligence.</li>
    <li>We understand that the policy will exclude any claim related directly or indirectly to any Pre-existing Condition (as defined in the Policy General Provisions or in the Critical Illness Benefit Exclusions Section) which I have not disclosed to HSBC Insurance.</li>
    <li>Refund of Premium rider only provides us a refund at the end of 10 years and we get no refund back if we stop paying premiums within the 10 years.</li>
</ul>
