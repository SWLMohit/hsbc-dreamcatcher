<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<p>
    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
        <input type="checkbox" name="declare_accept2" value="Y" <c:if test="${data['declare_accept2'] == 'Y'}">checked</c:if>>
        <span></span>
        We are aware that the product that we are applying for is authorised for sale only in Singapore. We acknowledge that 
        we are responsible for ensuring, and we represent and warrant, that we are in compliance with all the laws and 
        regulations applicable to our nationality(ies) and country(ies) of residence and that such laws and regulations allow our 
        purchase of and payments under this product. We understand that no liability can or will be accepted by HSBC Insurance 
        for any consequences under the laws of any country other than Singapore or any tax implications that may arise in 
        connection with our purchase of this product.
    </label>
    <c:if test="${errors.containsKey('declare_accept2')}">
        <div class="error-message" style="padding-bottom:10px;">
            <i class="icon icon-circle-delete"></i>
            <c:out value="${errors['declare_accept2']}"></c:out>
        </div>
    </c:if>
</p>

<p>
    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
        <input type="checkbox" name="declare_accept3" value="Y" <c:if test="${data['declare_accept3'] == 'Y'}">checked</c:if>>
        <span></span>
       We declare that:
    </label>
    <c:if test="${errors.containsKey('declare_accept3')}">
        <div class="error-message" style="padding-bottom:10px;">
            <i class="icon icon-circle-delete"></i>
            <c:out value="${errors['declare_accept3']}"></c:out>
        </div>
    </c:if>
</p>

<div class="checkbox-declare-indent">
    <ul>
        <li intended="true">We are not undischarged bankrupt(s)</li>
        <li intended="true">we are agreeable to inform HSBC Insurance if there is any change in the state of health, occupation or activity of 
		the Life Insured and Policyowner between the date of this online application and the issue of our policy. On 
		receiving this information, HSBC Insurance is entitled to accept or reject our application</li>
        <li intended="true">we have reviewed all existing life insurance policies that we own, or are in the process of applying for in the application form</li>
        <li intended="true">we are aware that it is generally disadvantageous to replace an existing life insurance policy with a new one as, amongst others, 
		we may lose the financial benefit accumulated over the years</li>
        <li intended="true"><span style="text-decoration: underline;"><b>we have initiated the purchase of this product and the entire application is made while we are residents in Singapore</b></span></li>
        <c:if test="${application.getApplicantNationality() == 'Japan' or application.getApplicantNationality2() == 'Japan' or application.getApplicantNationality3() == 'Japan' or application.getInsuredNationality() == 'Japan' or application.getInsuredNationality2() == 'Japan' or application.getInsuredNationality3() == 'Japan'}">
            <li intended="true">we are agreeable to complete the Declaration of Non-Japanese Residency in the Supplementary Proposal Form as 
          		part of our application for this policy</li>
        </c:if>
        <li intended="true">in compliance with US laws and regulations and other laws having extra-territorial effect: 
		<ul>
			<li inner-intended="true">
				We are not (and will not be) physically located in US throughout the entire policy application process 
				through to policy issuance
			</li>
			<li inner-intended="true">
				We are aware of and understand the policy servicing restrictions* applicable to any and all persons residing 
				temporarily or permanently in the US (*List of policy servicing restrictions is set out in our  
				<a href="http://www.insurance.hsbc.com.sg/1/2/sghi/customer-service" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">website</a>)
			</li>
			<li inner-intended="true">
				We will inform HSBC Insurance should we decide to reside in the US either temporarily or permanently
			</li>
		</ul>
        </li>
        <li intended="true">all our existing and pending HSBC Insurance OnlineProtector applications do not exceed an aggregate coverage of 
            <c:choose>
                <c:when test="${application.insuredIsWorking()}">
                    S$1million per life
                </c:when>
                <c:otherwise>
                    S$500,000 per life
                </c:otherwise>
            </c:choose>
        </li>
        <li intended="true"><span style="color:red">the information given by us to HSBC Insurance in this application form or in any form or document relating to this 
		application is true, complete and accurate. We have not withheld any material facts or information (i.e. facts or 
		information that are likely to influence the assessment and acceptance of the application). If anything is found to 
		be untrue, incorrect or incomplete, the insurance policy will be cancelled and void </span></li>
		<li intended="true">we are aware that if a material fact is not disclosed in this application, any policy issued may be void. We understand 
		that if we are in doubt	whether a fact is material, we are advised to disclose it</li>
		<li intended="true">we are aware that we can view or download the following LIA consumer disclosure guides for our reference
		<ul>
			<li inner-intended="true">
				<a href="/assets/downloads/Your_Guide_to_Life_Insurance.pdf" target="_blank" aria-label="Download Your Guide to Life Insurance in PDF format" style="overflow-wrap:break-word;text-decoration: underline;">
				    <i class="icon icon-download" aria-hidden="true"></i>
					<span>Your Guide to Life Insurance</span>
				</a>
			</li>
			<li inner-intended="true">
				<a href="/assets/downloads/Your_Guide_to_Health_Insurance.pdf" aria-label="Download Your Guide to Health Insurance (if applicable) in PDF format" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">
				    <i class="icon icon-download" aria-hidden="true"></i>
					<span>Your Guide to Health Insurance (if applicable)</span>
				</a>
			</li>
		</ul>
	    </li>
    </ul>
</div>

<div class="space20"></div>

<div>
    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
        <input type="checkbox" name="declare_accept4" value="Y" <c:if test="${data['declare_accept4'] == 'Y'}">checked</c:if> aria-describedby="ack1">
        <span></span>
        <b>We <c:out value="${application.getApplicantFullName()}"></c:out> and <c:out value="${ application.getInsuredFullName() }"></c:out> agree with the statement below</b>
    </label>
    <c:if test="${errors.containsKey('declare_accept4')}">
        <div class="error-message" aria-label="Error" style="padding-bottom:10px;" id="ack1">
            <i class="icon icon-circle-delete" aria-hidden="true"></i>
            <c:out value="${errors['declare_accept4']}"></c:out>
        </div>
    </c:if>
</div>

<div class="checkbox-declare-indent">
If we are customer(s) of, or become a customer of, HSBC Bank (Singapore) Limited ("HSBC Bank"), we agree 
that HSBC Insurance may disclose our personal data provided in this form to HSBC Bank, to enable HSBC Bank 
to provide us with a consolidated view of all the products we may hold with HSBC Bank, HSBC Insurance and 
(where relevant) any other member of the HSBC Group.
</div>

<div>
    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
        <input type="checkbox" name="declare_accept5" value="Y" checked aria-describedby="ack2">
        <span></span>
        <b>We <c:out value="${application.getApplicantFullName()}"></c:out> and <c:out value="${ application.getInsuredFullName() }"></c:out> agree with the statement below</b>
    </label>
    <c:if test="${errors.containsKey('declare_accept5')}">
        <div class="error-message" aria-label="Error" style="padding-bottom:10px;" id="ack2">
            <i class="icon icon-circle-delete"  aria-hidden="true"></i>
            <c:out value="${errors['declare_accept5']}"></c:out>
        </div>
    </c:if>
</div>

<div class="checkbox-declare-indent">
We consent to the use and disclosure of our personal data provided in this form to the HSBC Group and its respective 
agents, authorised service parties and relevant third parties for the purpose of sending us news and alerts on new 
products and special promotions offered by HSBC Insurance and its group companies.
</div>

