<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<h3>Futher Acknowledgement</h3>
<p>I acknowledge the following important terms and conditions:</p>
<ul>
    <li>A life insurance policy is not a savings account or deposit;</li>
    <li>I am aware that I may not get back the premiums paid (partially or in full) if I terminate or surrender the policy early;</li>
    <li>I understand that there may be some benefits of a life insurance policy which are not guaranteed.</li>
    <li>I have 30 days or such other period granted by HSBC Insurance to cancel my policy after HSBC Insurance confirms acceptance of the application.</li>
    <li>I am aware that I can request HSBC Insurance to explain any of the product features.</li>
    <li>I understand that  I can also separately seek advice on the suitability  of the policy.</li>
    <li>In the event that I choose  not to seek advice on the suitability of the policy, I have considered and decided that it is suitable for my financial circumstances and needs.</li>
    <li>I understand that the policy will exclude any claim related directly or indirectly to any Pre-existing Condition (as defined in the Policy General Provisions or in the Critical Illness Benefit Exclusions Section) which I have not disclosed to HSBC Insurance.</li>
    <li>I am aware that the policy will exclude Total and Permanent Disability Benefit if my job or leisure pursuits involve working at heights, working underground, using explosives or chemicals, motor racing, flying other than as a fare paying passenger on a commercial airline, diving below 30m or mountain climbing (4,000m and above).</li>
    <li>I understand that the commencement of risk coverage under the policy is subject to the receipt by the Company of the first premium payment and the completion of internal administrative checks, such as customer due diligence.</li>
    <li>I am aware that the Refund of Premium rider only provides me a full refund of my total premiums paid (Maturity Benefit) if I pay all  the 10 scheduled Annual Premiums for the HSBC Insurance OnlineProtector Plan and  Refund of Premium rider.</li>
    <li>I am aware that I will get a reduced Maturity Benefit if I stop paying premiums on or after the 5th scheduled Annual Premiums. I understand that I will get no refund back if I stop premium payments before the 5th scheduled Annual Premiums.</li>
</ul>
