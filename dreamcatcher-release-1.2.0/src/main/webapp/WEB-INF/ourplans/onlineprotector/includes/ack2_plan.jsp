<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<ul>
    <li>A life insurance policy is not a savings account or deposit
    <li>I am aware that I may not get back the premiums paid (partially or in full) if I terminate or surrender the policy early</li>
    <li>I understand that there may be some benefits of a life insurance policy which are not guaranteed (only if applicable)</li>
    <li><b>I understand that I have a 30-day free-look period to cancel a policy after HSBC Insurance confirms acceptance of my application 
	    at standard rates. If I cancel the policy within 30 days, HSBC Insurance will refund the premium I paid without interest after deducting 
		medical and underwriting expenses incurred in accepting my application</b></li>
    <li>I am aware that I can request HSBC Insurance to explain any of the product features</li>
    <li>I understand that I can also separately seek advice on the suitability of the policy</li>
    <li>In the event that I choose not to seek advice on the suitability of the life insurance policy, I have considered and decided that 
	    it is suitable for my financial circumstances and needs</li>
    <li>I have declared my current financial situation, such as income, in my application</li>
    <li><span style="color:red">I understand that the commencement of risk coverage under the policy is subject to the receipt by HSBC Insurance of the first premium payment 
	    and the completion of internal administrative checks, such as customer due diligence</span></li>
</ul>