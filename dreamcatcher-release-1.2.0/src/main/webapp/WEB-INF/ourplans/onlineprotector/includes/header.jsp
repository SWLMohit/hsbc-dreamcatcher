<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<div class="col-xs-12">

    <h1>HSBC Insurance OnlineProtector application</h1>

    <c:if test="${currentStep>2}">


        <a href="/" class="buynow-exit-button">Exit</a>
    


        <div class="onlineprotector__breadcrumb" aria-label="breadcrumb" data-current-step="<c:out value="${currentStep}"></c:out>" data-max-step="<c:out value="${maxStep}"></c:out>" data-otp-sent="<c:out value="${otpSent}"></c:out>">
            <a href="#" class="<c:choose><c:when test="${currentStep > 0}">current</c:when><c:otherwise>disabled</c:otherwise></c:choose>"<c:choose><c:when test="${currentStep > 0 && currentStep <= 5}">aria-current="step"</c:when><c:otherwise>aria-disabled="true"</c:otherwise></c:choose>><span><em>1/4</em>Eligibility Check</span></a>
            <a href="#" class="<c:choose><c:when test="${currentStep >= 6}">current</c:when><c:otherwise>disabled</c:otherwise></c:choose>"<c:choose><c:when test="${currentStep >= 6 && currentStep <= 9}">aria-current="step"</c:when><c:otherwise>aria-disabled="true"</c:otherwise></c:choose>><span><em>2/4</em>Your Plan</span></a>
            <a href="#" class="<c:choose><c:when test="${currentStep >= 10}">current</c:when><c:otherwise>disabled</c:otherwise></c:choose>"<c:choose><c:when test="${currentStep >= 10 && currentStep <= 13}">aria-current="step"</c:when><c:otherwise>aria-disabled="true"</c:otherwise></c:choose>><span><em>3/4</em>Your Details</span></a>
            <a href="#" class="<c:choose><c:when test="${currentStep >= 14}">current</c:when><c:otherwise>disabled</c:otherwise></c:choose>"<c:choose><c:when test="${currentStep >= 14}">aria-current="step"</c:when><c:otherwise>aria-disabled="true"</c:otherwise></c:choose>><span><em>4/4</em>Payment</span></a>
        </div>
    </c:if>
</div>
