<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/our-plans/">Our plans</a>
        <a href="/our-plans/online-protector/">HSBC Insurance OnlineProtector</a>
        <a style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineProtector application</a>
    </nav>
</div>
