<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<p>In proceeding to make this purchase we are confirming that this product is an appropriate solution for our needs.</p>


<ul>
    <li>We understand that we are purchasing HSBC Insurance OnlineProtector plan ("the Product") without seeking advice from any Financial Advisory representative. We understand that we are encouraged to go through the following items before we determine if this insurance policy meets our financial circumstances and needs:
        <ul>
            <li><a href="https://www.cpf.gov.sg/eSvc/Web/Schemes/InsuranceEstimator/InsuranceEstimator" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">Insurance Estimator</a> to calculate the amount of life insurance coverage we would need</li>
            <li><a href="http://www.moneysense.gov.sg/financial-planning/financial-calculators/budget-calculator.aspx" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">Budget Calculator</a> to check if the premium payable is affordable based on our current income and expenditure</li>
            <li><a href="http://www.comparefirst.sg" target="_blank" style="overflow-wrap:break-word;text-decoration: underline;">Comparefirst</a> to compare the features and premiums of Direct Purchase Insurance (DPI) and other types of life policies</li>
            <li>Considered the different types of DPI and other types of life insurance products that are available and whether the insurance policy that we intend to purchase is suitable for our financial circumstances and needs</li>
        </ul>
    </li>
    <li><b>We understand the nature, objective and benefits of the Product and have read and acknowledged the information</b></li>
		<ul>
		    <li>
		        in the <a href="/our-plans/online-protector/bi" target="_blank" aria-label="Download Benefit Illustration in PDF format" data-event="file-download" data-name="Benefit_Illustratio.pdf">
		            <i class="icon icon-download" aria-hidden="true"></i>
		            <span style="color:red;text-decoration: underline;">Benefit Illustration</span>
		        </a>
		    </li>
		    <li>
		        in the <a href="/assets/downloads/GPPSOnlineProtector10.pdf" target="_blank" aria-label="Download Basic Plan Product Summary and General Provisions in PDF format" data-event="file-download" data-name="GPPSOnlineProtector10.pdf">
		            <i class="icon icon-download" aria-hidden="true"></i>
		            <span style="color:red;text-decoration: underline;">Product Summary and General Provision</span>
		        </a>
		    </li>
		</ul> 
		</ul>
    <p style="margin-left: 23px;">We are also aware that we will be receiving a copy of the documents in PDF listed above via e-mail upon completion of our application.</p>
    <ul>
    <li>We understand and acknowledge the exclusions and risks of the Product, which include, but are not limited to the following, that:
        <ul>
            <li>we may lose our protection cover if we do not make our premium payment when due</li>
            <li>whilst the premiums for the Product in the first 10 years are guaranteed, the renewal premiums are not guaranteed</li>
            <li>the policy will exclude Total and Permanent Disability Benefit if our job or leisure pursuits involve working at heights, working underground, using explosives or chemicals, motor racing, flying other than as a fare paying passenger on a commercial airline, diving below 30m or mountain climbing (4,000m and above)</li>
            <li>the policy will exclude any claim related directly or indirectly to any pre-existing condition (as defined in the Policy General Provisions) which we have not disclosed to HSBC Insurance</li>           
        </ul>
    </li>
</ul>



