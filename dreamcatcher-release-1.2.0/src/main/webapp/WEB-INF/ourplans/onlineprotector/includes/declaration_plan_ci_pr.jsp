<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<h3>Notice to client</h3>
<p>In proceeding to make this purchase you are confirming that this product is an appropriate solution for your needs.</p>

<ul>
    <li>I understand that I am purchasing HSBC Insurance OnlineProtector plan ("the Product") with the HSBC Insurance Online Premium Refund Rider ("the PR Rider") and the HSBC Insurance Online Critical Illness Rider ("the CI Rider") without seeking advice from any Financial Advisory representative.  I understand that I am encouraged to go through the following items before I determine if this Insurance policy meets my financial circumstances and needs:
        <ul>
            <li>Insurance Estimator: <a href="https://www.cpf.gov.sg/eSvc/Web/Schemes/InsuranceEstimator/InsuranceEstimator" target="_blank" style="overflow-wrap:break-word;">https://www.cpf.gov.sg/eSvc/Web/Schemes/InsuranceEstimator/InsuranceEstimator</a> to calculate the amount of life insurance coverage I would need;</li>
            <li>Budget Calculator: <a href="http://www.moneysense.gov.sg/financial-planning/financial-calculators/budget-calculator.aspx" target="_blank" style="overflow-wrap:break-word;">http://www.moneysense.gov.sg/financial-planning/financial-calculators/budget-calculator.aspx</a> to check if the premium payable is affordable based on my current income and expenditure;</li>
            <li>Comparefirst: <a href="http://www.comparefirst.sg" target="_blank" style="overflow-wrap:break-word;">http://www.comparefirst.sg</a> to compare the features and premiums of Direct Purchase Insurance (DPI) and other types of life policies; and</li>
            <li>Considered the different types of DPI and other types of life insurance products that are available and whether the insurance policy that I intend to purchase is suitable for my financial circumstances and needs.</li>
        </ul>
    </li>
    <li>I have read and understood the information in the Product Summary, General Provision, Annexures and the Benefit Illustration.</li>
    <li>I understand the nature, objective and benefits of the Product, the PR Rider and the CI Rider.</li>
    <li>I understand and acknowledge the exclusions and risks of the Product, the PR Rider and the CI Rider, which include, but are not limited to, the following:
        <ul>
            <li>that I may lose my protection cover if I do not make my premium payment when due;</li>
            <li>that whilst the annual premium for the Product in the first 10 years are guaranteed, the renewal premiums are not guaranteed;</li>
            <li>that the policy will exclude Total and Permanent Disability Benefit if  my job or leisure pursuits involve working at heights, working underground, using explosives or chemicals, motor racing, flying other than as a fare paying passenger on a commercial airline, diving below 30m or mountain climbing (4,000m and above);</li>
            <li>that the policy will exclude any claim related directly or indirectly to any Pre-existing Condition (as defined in the Policy General Provisions or in the Critical Illness Benefit Exclusions Section) which I have not disclosed to HSBC Insurance;</li>
            <li>that if I do not pay all the premiums for the Product and / or the PR Rider, I will not get the refund of premium maturity benefit as set out in the Benefit Illustration;</li>
            <li>that if there is a claim (Death, Total Permanent Disability, or Critical Illness) on the policy, I will get the Sum Insured but I will not get any maturity benefit as set out in the Benefit Illustration;</li>
            <li>in particular for the CI Rider, I understand that there are exclusions which could impact the claim;</li>
            <li>that the Annual Premium for the CI Rider is not guaranteed; and</li>
            <li>that if the Product is still in force after the Critical Illness sum insured payout, my premium for the Product will be reduced accordingly. My Product  will terminate if I do not pay the revised premium.</li>
        </ul>
    </li>
    <li>I understand the features and the price differences between the Product versus the Product with the CI Rider and the PR Rider included.</li>
</ul>

<div class="space40"></div>
<p><strong>Product Details</strong></p>
<ul class="downloads-list" style="margin:10px 0 30px 0;">
    <li>
        <a href="/our-plans/online-protector/bi" target="_blank" aria-label="Download Benefit Illustration in PDF format">
            <i class="icon icon-download" aria-hidden="true"></i>
            <span>Benefit Illustration (PDF)</span>
        </a>
    </li>
    <li>
        <a href="/assets/downloads/GPPSOnlineProtector10.pdf" target="_blank" aria-label="Download Basic Plan Product Summary and General Provisions in PDF format">
            <i class="icon icon-download" aria-hidden="true"></i>
            <span>Basic Plan Product Summary and General Provisions (PDF)</span>
        </a>
    </li>
</ul>

<p><strong>HSBC  Insurance  Online Critical Illness Rider</strong></p>
<ul class="downloads-list" style="margin:10px 0 30px 0;">
    <li>
        <a href="/assets/downloads/AXPSOnlineCI.pdf" target="_blank" aria-label="Download ">
            <i class="icon icon-download" aria-hidden="true"></i>
            <span>Critical Illness Rider Product Summary and Annexure (PDF)</span>
        </a>
    </li>
</ul>

<p><strong>HSBC Insurance Online Premium Refund Rider</strong></p>
<ul class="downloads-list" style="margin:10px 0 30px 0;">
    <li>
        <a href="/assets/downloads/AXPSOnlinePremiumRefund.pdf" target="_blank" aria-label="Download Premium Refund Rider Product Summary and Annexure in PDF format">
            <i class="icon icon-download" aria-hidden="true"></i>
            <span>Premium Refund Rider Product Summary and Annexure (PDF)</span>
        </a>
    </li>
</ul>

<p>You will also receive a copy of the PDFs listed in the table above via email upon completion of your application.</p>
