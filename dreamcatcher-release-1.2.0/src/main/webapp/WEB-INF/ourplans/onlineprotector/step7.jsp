<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-premiumconfirmation"></jsp:param>
</jsp:include>

<div class="main onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>
                    <form class="buynow-form" method="post" data-form-name="onlineprotector-step">
                        <div class="col-xs-12 col-sm-6">
                            <h2>Coverage and add-on(s)</h2>
                            <p>Enter your policy coverage or drag the slider to adjust the amount <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="The coverage information displayed was captured during the &lsquo;Get a quote&rsquo; process. You can choose to make changes here." aria-label="The coverage information displayed was captured during the &lsquo;Get a quote&rsquo; process. You can choose to make changes here."  data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button>:</p>
                            
                            <div class="row" data-field-name="sum_assured">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <fmt:formatNumber var="fmtSumAssured" value="${fn:escapeXml(data['sum_assured'])}" type="number" maxFractionDigits="0"/>

                                        <input name="sum_assured" class="form-control number-only-input" value="${fmtSumAssured}" type="text" aria-describedby="sumAssured">
                                    </div>
                                </div>
                                <div class="col-xs-12" aria-hidden="true" tabindex="-1">
                                    <fmt:formatNumber var="fmtMaxSumAssured" value="${fn:escapeXml(application.getMaxSumAssured())}" type="number" maxFractionDigits="0" groupingUsed="false"/>
                                    <div class="slider slider-coverage" data-min="400000" data-max="${fmtMaxSumAssured}"></div>
                                    <c:if test="${errors.containsKey('sum_assured')}">

                                        <span class="error-message" aria-label="Error" id="sumAssured">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['sum_assured']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>

                            <h3>Add-on(s)</h3>

                            <div class="row" data-field-name="include_ci_rider">
                                <div class="col-xs-12">
                                    <label class="checkbox label-inline" role="checkbox" tabindex="0" aria-checked="false">
                                        <input type="checkbox" name="include_ci_rider" value="Y" <c:if test="${data['include_ci_rider']}">checked</c:if>>
                                        <span></span>
                                        HSBC Insurance Online Critical Illness Rider
                                    </label>
                                    <button type="button" class="help-tooltip" label="help" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" aria-label="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-content="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title="">
                                        <i class="icon icon-circle-help-solid"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="row" data-question-group="ci_rider" data-field-name="ci_rider">
                                <div class="pointed pointed--down" style="margin-left: 15px; margin-right: 15px; width: auto;">
                                    <hr>
                                </div>

                                <div>
                                    <fieldset class="col-xs-12">
                                        <legend>
                                            <c:if test="${application.insuredIsSelf()}">
                                                <p>Have any of your parents or siblings ever suffered from cancer, heart disease, stroke or any hereditary condition before age 60?</p>
                                            </c:if>
                                            <c:if test="${not application.insuredIsSelf()}">
                                                <p>Have any of <c:out value="${application.getInsuredPossessiveLabel(true)}"></c:out> parents or siblings ever suffered from cancer, heart disease, stroke or any hereditary condition before age 60?</p>
                                            </c:if>
                                        </legend>
                                        <div class="form-group <c:out value="${(errors.containsKey('insured_medical4')) ? 'error' : ''}"></c:out>" data-field-name="insured_medical4">
                                            <div class="newradio--inline">
                                                <input id="id_insured_medical4_option1" type="radio" name="insured_medical4" value="Y" <c:if test="${data['insured_medical4']}">checked</c:if>>
                                                <label for="id_insured_medical4_option1"><span><span></span></span>Yes</label>
                                            </div>
                                            <div class="newradio--inline">
                                                <input id="id_insured_medical4_option2" type="radio" name="insured_medical4" value="N" <c:if test="${data['insured_medical4'] eq false}">checked</c:if> aria-describedby="insuredMedical4">
                                                <label for="id_insured_medical4_option2"><span><span></span></span>No</label>
                                            </div>
                                            <c:if test="${errors.containsKey('insured_medical4')}">
                                                <div>
                                                    <span class="error-message" aria-label="Error" id="insuredMedical4">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['insured_medical4']}"></c:out>
                                                    </span>
                                                </div>
                                            </c:if>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-xs-12 col-sm-12">

                                    <p><label class="label-inline" for="form-policyslider">Enter your policy coverage or drag the slider to adjust the amount </label>
                                  <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="The coverage information displayed was captured during the &lsquo;Get a quote&rsquo; process. You can choose to make changes here." aria-label="The coverage information displayed was captured during the &lsquo;Get a quote&rsquo; process. You can choose to make changes here." data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button>:</p>

                                    <div class="input-group currency-text-box" data-field-name="rider_sum_assured">
                                        <span class="input-group-addon">S$</span>
                                        <fmt:formatNumber var="fmtRiderSumAssured" value="${fn:escapeXml(data['rider_sum_assured'])}" type="number" maxFractionDigits="0"/>
                                        <input name="rider_sum_assured" id="form-policyslider" class="form-control number-only-input" value="${fmtRiderSumAssured}" type="text">
                                    </div>
                                </div>
                                <div class="col-xs-12" aria-hidden="true" tabindex="-1">
                                    <fmt:formatNumber var="fmtMaxRiderSumAssured" value="${fn:escapeXml(application.getMaxRiderSumAssured())}" type="number" maxFractionDigits="0" groupingUsed="false"/>
                                    <div class="slider slider-rider-coverage" data-min="100000" data-max="${fmtMaxRiderSumAssured}"></div>
                                </div>
                            </div>
  							 <!-- Removed as a part of 885 -->
                           <c:if test="${application.enableROP() and data['eligible_for_refund_rider']}">
                                <div class="row">
                                    <div class="col-xs-12" data-field-name="include_pr_rider">
                                        <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
                                            <input type="checkbox" name="include_pr_rider" value="Y" <c:if test="${data['include_pr_rider']}">checked</c:if>>
                                            <span></span>
                                            HSBC Insurance Online Premium Refund Rider <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="A rider that provides the life insured a return of premiums paid for OnlineProtector and Premium Refund Rider at the end of the 10-year policy term, if no claims have been made and the policy is still in force." aria-label="A rider that provides the life insured a return of premiums paid for OnlineProtector and Premium Refund Rider at the end of the 10-year policy term, if no claims have been made and the policy is still in force." data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button>
                                        </label>
                                    </div>
                                </div>
                            </c:if>

                            <div class="space30"></div>
                            <div class="row">
                                <div class="col-xs-8">
                                    <p><label for="form-paymentfrequency" id="form-paymentfrequency-label">Preferred Payment frequency:</label></p>
                                    <div class="form-select_wrap" data-field-name="payment_mode">
                                        <select name="payment_mode" class="form-control" id="form-paymentfrequency" aria-describedby="paymentFrequency">
                                            <option value="">Please select...</option>
                                            <option value="12" <c:if test="${data['payment_mode'] == 12}">selected</c:if>>Monthly</option>
                                            <option value="4" <c:if test="${data['payment_mode'] == 4}">selected</c:if>>Quarterly</option>
                                            <option value="2" <c:if test="${data['payment_mode'] == 2}">selected</c:if>>Semi-Annually</option>
                                            <option value="1" <c:if test="${data['payment_mode'] == 1}">selected</c:if>>Annually</option>
                                        </select>
                                    </div>
                                    <c:if test="${errors.containsKey('payment_mode')}">
                                        <span class="error-message" aria-label="Error" id="paymentFrequency">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['payment_mode']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>

                            <div class="space60"></div>

                        </div>
                        
                        <div class="col-xs-12 col-sm-5 col-md-4 double-left-padding premium-table-container" ci="${application.getIncludeCriticalIllnessRider()}" rop="${application.getIncludeRefundRider()}" payment-mode="${application.getPaymentMode()}" id="premium-table" name="summary" >
                            <table modalFactor="${data.get('modalFactor')}" underwritingclassFactor="${data.get('underwritingClassFactor')}" ciadjustedPremium="${data.get('ciAdjustedPremium')}"  
                            premiumrate="${data.get('premiumRate')}" premiumrateunitamount="${data.get('premiumRateUnitAmount')}"
                              actualdiscount="${data.get('actualDiscount') }"  adjustedPremium="${data.get('adjustedPremium')}" class="premium-table data-table" style="border-bottom:6px solid #008480;">
                                <thead>
                                    <tr >
                                        <th colspan="3">Premium Summary:</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        
                                        <th colspan="3" style="padding-right:20px;"><strong style="float:right;" id="payment-method">Monthly</strong></th>
                                    </tr>
                                    <tr>
                                        <td>Basic Plan</td>
                                        <td style="width:23%;"><span id="basic_mode"><c:out value="${data.get('summary_modal')}"></c:out></span></td>
                                    </tr>
                                    
                                        <tr id="cirider-row" <c:if test="${empty application.getIncludeCriticalIllnessRider()|| !application.getIncludeCriticalIllnessRider()}">style="display:none;" </c:if>>
                                            <td>HSBC Insurance Online Critical Illness rider</td>
                                            <td><span id="ci_mode"><c:out value="${data.get('summary_ci_modal')}"></c:out></span></td>
                                        </tr>
                                    
                                    
                                        <tr id="roprider-row" <c:if test="${empty application.getIncludeRefundRider() || !application.getIncludeRefundRider()}">style="display:none;" </c:if>>
                                            <td>HSBC Insurance Online Premium Refund rider</td>
                                            <td><span id="rop_mode"><c:out value="${data.get('summary_rop_modal')}"></c:out></span></td>
                                        </tr>
                                    
									
									<tr>
                                        <td><strong style="float:right">Total<strong></td>
                                        <td><strong id="total"><c:out value="${data.get('summary_total_modal')}"></c:out></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
						<input type="hidden" name="modalFactor" value="${data.get('modalFactor')}"/> 
						<input type="hidden" name="underwritingclassfactor"  value="${data.get('underwritingClassFactor')}"/> 
						<input type="hidden" name="ciadjustedpremium" value="${data.get('ciAdjustedPremium')}"/> 
						<input type="hidden" name="premiumrate" value="${data.get('premiumRate')}"/> 
						<input type="hidden" name="premiumrateunitamount" value="${data.get('premiumRateUnitAmount')}"/> 					
						<input type="hidden" name="adjustedpremium" value="${data.get('adjustedPremium')}"/>
						<input type="hidden" name="basic" value="${data.get('summary_modal') }" />
						<input type="hidden" name="actualdiscount" value="${data.get('actualDiscount') }" />
						
						
                        <div class="col-xs-12 col-sm-8">
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/5">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                        </div>
                        
                    
                    </form>
                </div>
            </div>

        </section>
    </article>
</div>

<div class="modal fade" id="ridermodal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4>Are you sure you want to proceed without selecting the HSBC Insurance Online Critical Illness Rider?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class=" btn btn-white" data-dismiss="modal">Yes</button>
                <button type="button" class=" btn btn-red" data-dismiss="modal">No, let me select</button>
            </div>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
