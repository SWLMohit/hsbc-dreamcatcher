<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="OnlineProtector"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-homepage"></jsp:param>
    <jsp:param name="metadataKeywords" value="insurance quotes, renew insurance, guaranteed renewable"></jsp:param>
	<jsp:param name="metadataDescription" value="HSBC's guaranteed renewable insurance plan that offers you protection up to age 90. Apply online and obtain your insurance premium discount today."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/our-plans/online-protector/"></jsp:param>
</jsp:include>

<section class="container-fluid">
    <div class="header-page header-onlineprotector">
        <div class="container">
            <div class="featured">
                <h1>HSBC Insurance OnlineProtector</h1>
                <p>Taking care of life all the time can be hard. <br> Fortunately, securing your loved ones' future is not.<br> Now's Good.</p>
            </div>
        </div>
    </div>
</section>

<div class="main onlineprotector-main">
    <!--    Protect Your Love Ones-->
    <section class="container protect-love">
        <div class="row">
            <div class="col-sm-8">
                <div class="protect-love-box">
                    <h2>Protect your loved ones today, tomorrow and in the future</h2>

                    <p>Term life insurance shouldn't be hard to understand. That's why we created a fuss-free insurance plan that helps take care of your loved ones and secure their future goals – in the event you're unable to. What's more, you have the flexibility to stretch your coverage by including the optional critical illness rider for a greater peace of mind.</p>
                    <a href="#quick_quote" class="secondary-btn-slate" data-event="quote-retireve" data-name="Online Protector">Get a quote</a>
                    <a href="/our-plans/online-protector/retrieve/" class="outline-btn-slate" data-event="retrieve-your-form" data-name="HSBC Insurance OnlineProtector">Retrieve your form</a>
                </div>
            </div>
            <div class="col-sm-4">
                <h3>Protection Cover</h3>
                <p>Up to S$1,000,000</p>
                <h3>Protection Period</h3>

                <p>10 years, guaranteed renewable up to age<a href="#footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup></a> 90</p>
                <h3>Add-on</h3>
                <p>HSBC Insurance Online Critical Illness Rider 
                        <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." aria-label="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title="">
                            <i class="icon icon-circle-help-solid"></i>
                        </button>
                </p>

                <h3>Premium Discounts</h3>
                <p>Stand to enjoy discounted premiums by answering 4 lifestyle questions.</p>
            </div>
        </div>
    </section>
    <div class="space60"></div>
    <div class="container">
        <hr> </div>
    <div class="space60"></div>
    <!--    Enhance Your Coverage-->
    <div class="container onlineprotector-benefits">
        <h2>How HSBC Insurance OnlineProtector benefits you</h2>
        <div class="r1-c3">
            <div class="row">
                <div class="col-sm-4">
                    <div class="tn-img">
                        <img src="/assets/images/onlineprotector/guaranteed-10-year-policy-renewals.jpg" alt="" class="img-fullwidth">
                    </div>
                    <h3>Guaranteed policy renewals every 10 years</h3>
                    <p>You will qualify for policy renewals every 10 years until you turn 90, without the need for medical proof provided the policy is still in force.</p>
                </div>
                <div class="col-sm-4">
                    <div class="tn-img">
                        <img src="/assets/images/onlineprotector/stand-to-enjoy-premium-discounts.jpg" alt="" class="img-fullwidth">
                    </div>
                    <h3>Stand to enjoy premium discounts</h3>
                    <p>Answer 4 lifestyle questions to find out if you are entitled to a discounted premium instantly.</p>
                </div>
                <div class="col-sm-4">
                    <div class="tn-img">
                        <img src="/assets/images/onlineprotector/unemployment-benefit.jpg" alt="" class="img-fullwidth">
                    </div>
                    <h3>Unemployment benefit</h3>
                    <p>Enjoy the flexibility to defer your premium(s) for up to a maximum of one year without interest.</p>
                </div>
            </div>
            <p class="tnc info" id="footnote1"><sup><span class="visuallyhidden">Footnote:</span>1</sup> Age refers to age next birthday and denotes the insured's age at a particular time with the addition of 1 year.</p>
        </div>
    </div>
    <div class="space60"></div>
    <div class="container">
        <hr> </div>
    <div class="space60"></div>
    <div class="container enhance-coverage">
        <h2>Enhance your coverage</h2>
        <div class="row">
            <div class="col-sm-12">
                <h2>HSBC Insurance Online Critical Illness Rider</h2>
                <p>In the unfortunate event of a critical illness, a sum insured of up to S$650,000 will be paid out. The overall sum insured of the plan will then be reduced by the CI amount paid out.</p>
                <p><a href="#" name="ci_modal_open" class="text-link-pop">The rider covers 30 critical illnesses</a> including stroke, certain types of cancer and heart diseases. </p>
            </div>
        </div>
    </div>
    <div class="space60"></div>
    <div class="container">
        <hr> </div>
    <div class="space60"></div>
    <!--Comparison-->
    <section class="container prdt-reference">
        <h2>Product reference materials</h2>
        <div class="space20"></div>

        <div>
            <ul class="downloads-list">
                <li>
                    <a href="/assets/downloads/GPPSOnlineProtector10.pdf" target="_blank" data-event="file-download" data-name="GPPSOnlineProtector10.pdf" aria-label="Download OnlineProtector (Base Plan) - Product Summary and General Provisions in PDF format">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>OnlineProtector (Base Plan) - Product Summary and General Provisions (PDF, 663KB)</span>
                    </a>
                </li>
                <li>
                    <a href="/assets/downloads/AXPSOnlineCI.pdf" target="_blank" data-event="file-download" data-name="AXPSOnlineCI.pdf" aria-label="Download HSBC Insurance Online Critical Illness Rider - Product Summary and Annexure in PDF format">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>HSBC Insurance Online Critical Illness Rider - Product Summary and Annexure (PDF, 714KB)</span>
                    </a>
                </li>    
                
            </ul>
        </div>
    </section>
    <div class="space60"></div>
    <section id="quick_quote" class="container-fluid estimate-form onlineprotector">
        <div class="container">
            <div class="estimate-form-container">
                <div class="estimate-form-box">
                    <form id="quick-quote-form" data-form-name="onlineprotector-quickquote">
                        <div class="row is-flex">
                            <div class="col-xs-12 col-sm-12 col-md-2">
                                <h2>Get a quote</h2>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-5">
                                <h3>Tell us about the applicant</h3>
                                <dl>
                                    <fieldset>
                                        <legend><dt>Gender</dt></legend>
                                        <dd class="form-group" data-field-name="applicant_gender">
                                            <div class="newradio--inline">
                                                <input id="id_gender_option1" type="radio" name="applicant_gender" value="M" <c:if test="${data['applicant_gender'] == 'M'}">checked</c:if>>
                                                <label for="id_gender_option1"><span><span></span></span>Male</label>
                                            </div>
                                            <div class="newradio--inline">
                                                <input id="id_gender_option2" type="radio" name="applicant_gender" value="F" <c:if test="${data['applicant_gender'] == 'F'}">checked</c:if>>
                                                <label for="id_gender_option2"><span><span></span></span>Female</label>
                                            </div>
                                            <div class="error-message" aria-label="Error" data-field-name="applicant_gender">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <span></span>
                                            </div>
                                        </dd>
                                    </fieldset>

                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_dob.jsp">
                                        <jsp:param name="quickQuote" value="true"></jsp:param>
                                    </jsp:include>

                                    <fieldset>
                                        <legend><dt>Has the applicant smoked in the last 12 months?</dt></legend>
                                        <dd class="form-group" data-field-name="applicant_smoker">
                                            <div class="newradio--inline">
                                                <input id="id_applicant_smoker_option1" type="radio" name="applicant_smoker" value="Y">
                                                <label for="id_applicant_smoker_option1"><span><span></span></span>Yes</label>
                                            </div>
                                            <div class="newradio--inline">
                                                <input id="id_applicant_smoker_option2" type="radio" name="applicant_smoker" value="N">
                                                <label for="id_applicant_smoker_option2"><span><span></span></span>No</label>
                                            </div>
                                            <div class="error-message" aria-label="Error" data-field-name="applicant_smoker">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <span></span>
                                            </div>
                                        </dd>
                                    </fieldset>
                                </dl>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-5">
                                <h3>Coverage and add-on(s)</h3>
                                <p style="margin-bottom: 5px;">Enter your policy coverage or drag the slider to adjust the amount:</p>
                                <div style="white-space:nowrap">
                                    <div class="input-group currency-text-box" data-event="onlineprotector-directvalueterm">
                                        <span class="input-group-addon">S$</span>
                                        <fmt:formatNumber var="fmtSumAssured" value="${fn:escapeXml(data['sum_assured'])}" type="number" maxFractionDigits="0"/>

                                        <input name="sum_assured" class="form-control number-only-input" value="${fmtSumAssured}" type="text">
                                    </div>
                                </div>
                                <div aria-hidden="true" tabindex="-1">
                                    <div class="slider slider-coverage" data-min="400000" data-max="1000000"></div>
                                </div>

                                <p>Add-ons:</p>
                                <div class="ci_rider_option">
                                    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
                                        <input type="checkbox" name="include_ci_rider" data-event="onlineprotector-directvalueterm">
                                        <span></span>
                                        HSBC Insurance Online Critical Illness <br>Rider
                                        <button type="button" class="help-tooltip" label="help" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" aria-label="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-content="A rider that pays out the life insured a lump sum of up to S$650,000 in the unfortunate event of any 30 critical illnesses including stroke, certain types of cancer and heart diseases." data-original-title="" title="">
                                              <i class="icon icon-circle-help-solid"></i>
                                        </button>
                                    </label>
                                    <div data-question-group="ci_rider">
                                        <p style="margin-bottom: 5px;">Enter your policy coverage or drag the slider to adjust the amount:</p>
                                        <div style="white-space:nowrap">
                                            <div class="input-group currency-text-box" data-event="onlineprotector-directvalueterm" data-field-name="currency-text-box">
                                                <span class="input-group-addon">S$</span>
                                                <fmt:formatNumber var="fmtRiderSumAssured" value="${fn:escapeXml(data['rider_sum_assured'])}" type="number" maxFractionDigits="0"/>
                                                <input name="rider_sum_assured" class="form-control number-only-input" value="${fmtRiderSumAssured}" type="text">
                                               </div>
                                        </div>
                                        <div class="error-message" aria-label="Error" data-field-name="rider_sum_assured">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <span></span>
                                        </div>

                                        <div aria-hidden="true" tabindex="-1">
                                            <div class="slider slider-rider-coverage" data-min="100000" data-max="650000"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Removed as a part of 885 -->

                                <c:if test="${data['rop_enabled']}">
                                  <div class="pr_rider_option"> 
                                    <label class="checkbox-label checkbox" style="white-space:nowrap;">
                                        <input type="checkbox" name="include_pr_rider" id="include_pr_rider">
                                        <span></span>
                                        HSBC Insurance Online Premium Refund <br>Rider
                                        <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" data-content="A rider that provides the life insured a return of premiums paid for OnlineProtector and Premium Refund Rider at the end of the 10-year policy term, if no claims have been made and the policy is still in force." data-original-title="" title="">
                                            <i class="icon icon-circle-help-solid"></i>
                                        </button>
                                    </label>
                                  </div>
                                </c:if>
                                <div class="space20"></div>
                                <label for="promocode">Enter promo code (if applicable):</label>
                                <!-- <p>Get a 10% discount on your premium for the first 3 months by entering the promo code: <br> HSBCONLINE10</p> -->
                                <div class="row">
                                    <div class="col-xs-8 col-md-9 form-group" style="border:none;">
                                        <input name="campaign_code" type="text" class="form-control auto-width" id="promocode" maxlength="20">
                                         <div class="help-block"><small>Only letters and numbers are allowed</small></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8 col-md-9 form-group">
                                        <button class="primary-btn-slate" data-action="quote">Get a quote</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="estimate-form-result-wrapper" aria-atomic="true" aria-live="polite">
                </div>
            </div>
        </div>
    </section>
<% /*
    <!--    Review-->
    <section class="container display-review">
        <h1>What our customers are saying</h1>
        <div class="row">
            <div class="col-sm-6 review-content">
                <p>XX of XX people found the following review helpful.</XX>
                <div class="rating"> <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span> </div>
                <P> By (name here)
                    <br> Review posted XX months ago</p>
                <p>Put the full review here</p> <a href="#" class="outline-btn-slate"><i class="icon icon-agree "></i> Yes, I recommend this product.</a>
                <p>Is this review helpful?</p>
            </div>
            <div class="col-sm-6 review-content">
                <p>XX of XX people found the following review helpful.</XX>
                <div class="rating"> <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span> </div>
                <P>By (name here)
                    <br> Review posted XX months ago</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p>
                <p>Put the full review here</p> <a href="#" class="outline-btn-slate"><i class="icon icon-agree "></i> Yes, I recommend this product.</a>
                <p>Is this review helpful?</p>
            </div>
        </div>
    </section>
    <div class="space60"></div>
    <div class="container">
        <hr> </div>
    <div class="space60"></div>
*/ %>
</div>

<div class="modal fade" id="ci-modal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">30 Critical Illnesses</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li>Alzheimer's Disease / Severe Dementia</li>
                    <li>Angioplasty and Other Invasive Treatment For Coronary Artery</li>
                    <li>Aplastic Anaemia</li>
                    <li>Bacterial Meningitis</li>
                    <li>Benign Brain Tumor</li>
                    <li>Blindness (Loss of Sight)</li>
                    <li>Coma</li>
                    <li>Coronary Artery By-pass Surgery</li>
                    <li>Deafness (Loss of Hearing)</li>
                    <li>End Stage Liver Failure</li>
                    <li>End Stage Lung Disease</li>
                    <li>Fulminant Hepatitis</li>
                    <li>Heart Attack of Specified Severity</li>
                    <li>Heart Value Surgery</li>
                    <li>HIV Due to Blood Transfusion and Occupationally Acquired HIV</li>
                    <li>Kidney Failure</li>
                    <li>Loss of Speech</li>
                    <li>Major Burns</li>
                    <li>Major Cancers</li>
                    <li>Major Head Trauma</li>
                    <li>Major Organ / Bone Marrow Transplantation</li>
                    <li>Motor Neurone Disease</li>
                    <li>Multiple Sclerosis</li>
                    <li>Muscular Dystrophy</li>
                    <li>Paralysis (Loss of Use of Limbs)</li>
                    <li>Parkinson's Disease</li>
                    <li>Primary Pulmonary Hypertension</li>
                    <li>Stroke</li>
                    <li>Surgery to Aorta</li>
                    <li>Viral Encephalitis</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn primary-btn-slate">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="promo-terms-modal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <ol type="1">
                    <li>In order to be eligible for the Promotion, you must have a valid campaign code</li>
                    <li>HSBC Insurance's decision on all matters relating to the Promotion including, without limitation, determining the eligibility of participants shall be final and binding on all participants</li>
                    <li>HSBC Insurance reserves the right to vary the terms of this Promotion or to terminate the Promotion at any time without prior notice</li>
                </ol>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn primary-btn-slate">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="eligibility-validation-modal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Based on your response(s), we are unable to proceed with your online application.</h4>
            </div>
            <div class="modal-body">
                <p>However, we recommend that you visit the nearest HSBC Branch or complete this <a href="/contact-us/" style="text-decoration: underline;">contact form</a> so that our financial advisors can provide you with financial advice.</p>
                <p>Please note that the charges, features and benefits for advised products will be different from products offered on this online insurance platform.</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn primary-btn-slate">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="age-validation-modal" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title">Selected date is in the future.</h4>
            </div>
            <div class="modal-body">
                <p> Please select a valid birthday.</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn primary-btn-slate">Close</button>
            </div>
        </div>
    </div>
</div>


<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="faqGroup" value="online_protector" />
</jsp:include>


<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_index.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<script>
$(document).ready(function() {

    $('a[name="ci_modal_open"]').click(function(e) {
        $('#ci-modal').modal('show');
        return false;
    });

    $('i[name="promo_terms_open"]').click(function(e) {
        $('#promo-terms-modal').modal('show');
    });
	
	var formOnlineprotectorQuickQuote = $('[data-form-name="onlineprotector-quickquote"]'),
		dataOnlineprotectorQuickQuote = formOnlineprotectorQuickQuote.find('input, select, textarea, .selectBox'),
		startFormOnlineprotectorQuickQuote = false;

	dataOnlineprotectorQuickQuote.focus(function () {
		if (!startFormOnlineprotectorQuickQuote) {
			TMS.trackEvent({
				'event_category': 'Calculator Quick Quote',
				'event_action':   'Start',
				'event_form':     'Form Start',
				'event_content':  'Online Protector Calculator Quick Quote'
			});
			startFormOnlineprotectorQuickQuote = true;
		}
	});

    $('button[data-action=quote]').click(function() {
		var $button = $(this);
		
		var errorMsg="";
		
		if( parseFloat($("[name=sum_assured]").val().replace(/,/g,''))<parseFloat($("[name=rider_sum_assured]").val().replace(/,/g,''))){
			errorMsg="Please input up to max policy coverage of SGD"+parseFloat($("[name=sum_assured]").val().replace(/,/g,''));
		}else if(parseFloat($("[name=rider_sum_assured]").val().replace(/,/g,''))>$(".slider-rider-coverage").data('max')){
			errorMsg="Please input up to max policy coverage of SGD"+parseFloat($(".slider-rider-coverage").data('max'));
		}else if(parseFloat($("[name=rider_sum_assured]").val().replace(/,/g,''))<$(".slider-rider-coverage").data('max')){
			errorMsg="Please input up to min policy coverage of SGD"+parseFloat($(".slider-rider-coverage").data('min'));
		}
		
		$('.error-message[data-field-name="rider_sum_assured"] span').text(errorMsg);
		$('.error-message[data-field-name="rider_sum_assured"]').css("display","block");
		$('.form-group[data-field-name="rider_sum_assured"]').addClass('error');
		$('.estimate-form-result-wrapper *').remove();
		$('.estimate-form .error-message').hide();
		$('.estimate-form .form-group').removeClass('error');

		if ( $('[data-question-group="ci_rider"] input').val() === 'false') {
			$('[data-question-group="ci_rider"] input').val('50,000');
		}
		
		setTimeout(function () {
			$('.error').each(function () {
				TMS.trackEvent({
					'event_category': 'Calculator Quick Quote',
					'event_action':   'Error',
					'event_error':    $(this).data('field-name'),
					'event_content':  'Online Protector Calculator Quick Quote'
				});
			});
			
			if ($('.error').length === 0) {
				TMS.trackEvent({
					'event_category': 'Calculator Quick Quote',
					'event_action':   'Finish',
					'event_form':     'Form Finish',
					'event_content':  'Online Protector Calculator Quick Quote'
				});
			}
		}, 500);
        
           


        $.post('/our-plans/online-protector/quote', $('.estimate-form-box form').not(".is-false").serialize(), function(data) {
            $('.estimate-form-result-wrapper').append(data);

            // button transformation
            $button
                .removeClass('primary-btn-slate')
                .addClass('secondary-btn-slate')
                .text('Recalculate');

        });


        return false;
    });

    $('input[name="include_ci_rider"]').change(function() {
        $('[data-question-group=ci_rider]').toggle($('input[name=include_ci_rider]').is(':checked'));
    });
    if ($('input[name=include_ci_rider]').is(':checked')) {
        $('[data-question-group=ci_rider]').show();
    }
});
</script>
<style>

.onlineprotector form {
    max-width: none;
}
.estimate-form-box dl {
    margin: 0;
}
.estimate-form-box dd,
.estimate-form-box dt {
    clear: both;
    display: block;
    float: none;
    margin: 0;
    width: 100%;
    padding-left: 0;
    padding-bottom: 15px;
}

@media (min-width: 768px) {
    .estimate-form-box dd {
        padding-bottom: 25px;
    }
}

[data-question-group] {
 display: none;
}
.error-message {
    display: none;
}
</style>
