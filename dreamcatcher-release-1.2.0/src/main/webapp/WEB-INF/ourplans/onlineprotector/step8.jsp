<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-plansnapshot"></jsp:param>
</jsp:include>

<div class="main onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>

                    <form class="buynow-form" method="post" data-form-name="onlineprotector-step">
                        <div class="col-xs-12 col-sm-9 col-md-8">
                            <c:if test="${application.isEligiblePreferentialRates()}">
                                <div class="row">       
                                    <fieldset>                  
                                        <legend class="col-xs-12 col-md-6">Would you like to answer 4 lifestyle questions and stand a chance to enjoy premium discounts? </legend>
                                        <div class="col-xs-12 col-md-5 col-md-offset-1 form-group <c:out value="${(errors.containsKey('uw_lifestyle_answer')) ? 'error' : ''}"></c:out>" data-field-name="uw_lifestyle_questions_optin">
                                            <div class="newradio--inline">
                                                <input id="id_uw_lifestyle_questions_optin_option1" type="radio" name="uw_lifestyle_questions_optin" value="Y" <c:if test="${data['uw_lifestyle_questions_optin']}">checked</c:if>>
                                                <label for="id_uw_lifestyle_questions_optin_option1"><span><span></span></span>Yes</label>
                                            </div>
                                            <div class="newradio--inline">
                                                <input id="id_uw_lifestyle_questions_optin_option2" type="radio" name="uw_lifestyle_questions_optin" value="N" <c:if test="${data['uw_lifestyle_questions_optin'] eq false}">checked</c:if> aria-describedby="lifestyleQuestions">
                                                <label for="id_uw_lifestyle_questions_optin_option2"><span><span></span></span>No</label>
                                            </div>
                                            <c:if test="${errors.containsKey('uw_lifestyle_questions_optin')}">
                                                <div>
                                                        <span class="error-message" aria-label="Error" id="lifestyleQuestions">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['uw_lifestyle_questions_optin']}"></c:out>
                                                        </span>
                                                </div>
                                            </c:if>
                                        </div>
                                    </fieldset>   
                                </div>
                                <div class="row" data-question-group="uw_lifestyle"> 
                                    <div class="col-xs-12 col-md-5">
                                        <label for="form-lastmortgage" id="form-lastmortgage-label">
                                            <c:if test="${application.insuredIsSelf()}">
                                                When was your last mortgage approved (excluding refinance loans)?
                                            </c:if>
                                            <c:if test="${not application.insuredIsSelf()}">
                                                When was <c:out value="${application.getInsuredFirstName()}"></c:out>'s last mortgage approved (excluding refinance loans)?
                                            </c:if>
                                        </label>
                                    </div>
                                    <div data-question-group="uw_lifestyle" data-question-group="uw_lifestyle_mortgage" class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('uw_lifestyle_mortgage')) ? 'error' : ''}"></c:out>" data-field-name="uw_lifestyle_mortgage">
                                        <div class="form-select_wrap">
                                            <select name="uw_lifestyle_mortgage" id="form-lastmortgage" class="form-control" aria-describedby="lifestyleMortgage" >
                                                <option value="">Please select...</option>
                                                <option value="1" <c:if test="${data['uw_lifestyle_mortgage'] == 1}">selected</c:if>>In the last 12 months</option>
                                                <option value="2" <c:if test="${data['uw_lifestyle_mortgage'] == 2}">selected</c:if>>In the last 24 months</option>
                                                <option value="3" <c:if test="${data['uw_lifestyle_mortgage'] == 3}">selected</c:if>>More than 24 months ago</option>
                                                <option value="0" <c:if test="${data['uw_lifestyle_mortgage'] == 0}">selected</c:if>>Not applicable</option>
                                            </select>
                                        </div>
                                        <c:if test="${errors.containsKey('uw_lifestyle_mortgage')}">
                                                <span class="error-message" aria-label="Error" id="lifestyleMortgage">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['uw_lifestyle_mortgage']}"></c:out>
                                                </span>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="row" data-question-group="uw_lifestyle"> 
                                    <div class="col-xs-12 col-md-5">
                                        <label for="form-hsbcprofile" id="form-hsbcprofile-label">
                                            <c:if test="${application.insuredIsSelf()}">
                                                What is your HSBC customer profile?
                                            </c:if>
                                            <c:if test="${not application.insuredIsSelf()}">
                                                What is <c:out value="${application.getInsuredFirstName()}"></c:out>'s HSBC customer profile?
                                            </c:if>
                                        </label>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('uw_lifestyle_profile')) ? 'error' : ''}"></c:out>" data-field-name="uw_lifestyle_profile">
                                        <div class="form-select_wrap">
                                            <select name="uw_lifestyle_profile" id="form-hsbcprofile" class="form-control" aria-describedby="lifestyleCustomerProfile">
                                                <option value="">Please select...</option>
                                                <option value="1" <c:if test="${data['uw_lifestyle_profile'] == 1}">selected</c:if>>HSBC Jade or Private Banking</option>
                                                <option value="2" <c:if test="${data['uw_lifestyle_profile'] == 2}">selected</c:if>>HSBC Premier</option>
                                                <option value="3" <c:if test="${data['uw_lifestyle_profile'] == 3}">selected</c:if>>HSBC Advance</option>
                                                <option value="4" <c:if test="${data['uw_lifestyle_profile'] == 4}">selected</c:if>>HSBC Others</option>
                                                <option value="0" <c:if test="${data['uw_lifestyle_profile'] == 0}">selected</c:if>>Non-HSBC</option>
                                            </select>
                                        </div>
                                        <c:if test="${errors.containsKey('uw_lifestyle_profile')}">
                                                <span class="error-message" aria-label="Error" id="lifestyleCustomerProfile">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['uw_lifestyle_profile']}"></c:out>
                                                </span>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="row" data-question-group="uw_lifestyle"> 
                                    <fieldset>
                                        <legend class="col-xs-12 col-md-5">
                                            <c:if test="${application.insuredIsSelf()}">
                                                What is your height and weight?
                                            </c:if>
                                            <c:if test="${not application.insuredIsSelf()}">
                                                What is <c:out value="${application.getInsuredFirstName()}"></c:out>'s height and weight?
                                            </c:if>
                                        </legend>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="form-height">Height:</label>
                                                    <div class="input-group">
                                                        <input name="uw_lifestyle_height" id="form-height" type="text" class="form-control <c:out value="${(errors.containsKey('uw_lifestyle_height')) ? 'error' : ''}"></c:out>" value="${fn:escapeXml(data['uw_lifestyle_height'])}" maxlength="5" aria-describedby="uwLifestyleHeight" data-field-name="uw_lifestyle_height">

                                                        <span class="input-group-addon">cm</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                   <label for="form-weight">Weight:</label>
                                                    <div class="input-group">
                                                        <input name="uw_lifestyle_weight" id="form-weight" type="text" class="form-control <c:out value="${(errors.containsKey('uw_lifestyle_weight')) ? 'error' : ''}"></c:out>" value="${fn:escapeXml(data['uw_lifestyle_weight'])}" maxlength="5" aria-describedby="uwLifestyleWeight" data-field-name="uw_lifestyle_weight">

                                                        <span class="input-group-addon">kg</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <c:if test="${errors.containsKey('uw_lifestyle_height')}">
                                                <div>
                                                        <span class="error-message" id="uwLifestyleHeight" aria-label="Error">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['uw_lifestyle_height']}"></c:out>
                                                        </span>
                                                </div>
                                            </c:if>
                                            <c:if test="${errors.containsKey('uw_lifestyle_weight')}">
                                                <div>
                                                        <span class="error-message" id="uwLifestyleWeight" aria-label="Error">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['uw_lifestyle_weight']}"></c:out>
                                                        </span>
                                                </div>
                                            </c:if>
                                        </div>

                                    </fieldset>
                                </div>
                                <div class="row" data-question-group="uw_lifestyle"> 
                                    <div class="col-xs-12 col-md-5">
                                        <label for="form-exercise" id="form-exercise-label">
                                            <c:if test="${application.insuredIsSelf()}">
                                                How often do you exercise?
                                            </c:if>
                                            <c:if test="${not application.insuredIsSelf()}">
                                                How often does <c:out value="${application.getInsuredFirstName()}"></c:out> exercise?
                                            </c:if>
                                        </label>
                                    </div>
                                    <div data-question-group="uw_lifestyle" data-question-group="uw_lifestyle_exercise" class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('uw_lifestyle_exercise')) ? 'error' : ''}"></c:out>" data-field-name="uw_lifestyle_exercise">
                                        <div class="form-select_wrap">
                                            <select name="uw_lifestyle_exercise" id="form-exercise" class="form-control" aria-describedby="lifestyleExercise">
                                                <option value="">Please select...</option>
                                                <option value="1" <c:if test="${data['uw_lifestyle_exercise'] == 1}">selected</c:if>>Daily (for at least 30 minutes each time)</option>
                                                <option value="2" <c:if test="${data['uw_lifestyle_exercise'] == 2}">selected</c:if>>3 times a week</option>
                                                <option value="3" <c:if test="${data['uw_lifestyle_exercise'] == 3}">selected</c:if>>Every weekend</option>
                                                <option value="0" <c:if test="${data['uw_lifestyle_exercise'] == 0}">selected</c:if>>Not applicable</option>
                                            </select>
                                        </div>
                                        <c:if test="${errors.containsKey('uw_lifestyle_exercise')}">
                                                <span class="error-message" aria-label="Error" id="lifestyleExercise">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['uw_lifestyle_exercise']}"></c:out>
                                                </span>
                                        </c:if>
                                    </div>
                                </div>
                            </c:if>
                        </div>

                        <div class="col-xs-12 col-sm-5 col-md-4 double-left-padding" ci="${application.getIncludeCriticalIllnessRider()}" rop="${application.getIncludeRefundRider()}" payment-mode="${application.getPaymentMode()}" id="pay-table" name="summary" >
                            <table class="data-table" style="border-bottom:6px solid #008480;">
                                <thead>
                                    <tr>

                                        <th width="50%" scope="col">Summary:</th>
                                        <th colspan="2" scope="colgroup">             
                                            <span id="summary_mode"><c:out value="${data.get('summary_total_modal')}"></c:out></span> (<c:out value="${application.getPaymentMode() == 1  ? 'Annual' : application.getPaymentModeLabel()}"></c:out>)
                                        </th>
                                    </tr>
                                </thead>


                                <tbody>
                            <tr>
                                <td></td>
                                <th scope="col" id="co1"><strong><c:out value="${application.getPaymentMode() == 1 ? 'Annual' : application.getPaymentModeLabel()}"></c:out></strong></th>
                            </tr>
                            <tr>
                                <th scope="row" id="c1"><strong>Basic Plan:</strong></th>
                                <td headers="co1 c1"><span id="basic_mode"><c:out value="${data.get('summary_modal')}"></c:out></span></td>
                            </tr>
                            <c:if test="${application.getIncludeCriticalIllnessRider()}">
                                <tr>
                                    <th scope="row" id="c2"><strong>Critical Illness Cover:</strong></th>
                                    <td headers="co1 c2"><span id="ci_mode"><c:out value="${data.get('summary_ci_modal')}"></c:out></span></td>

                                </tr>
                            </c:if>
                            <c:if test="${application.getIncludeRefundRider()}">
                                <tr>
                                    <th scope="row" id="c3"><strong>Premium Refund Cover:</strong></th>
                                    <td headers="co1 c3"><span id="rop_mode"><c:out value="${data.get('summary_rop_modal')}"></c:out></span></td>
                                </tr>
                            </c:if>
                                <tr>
                                    <th colspan="3" scope="colgroup" id="c4">
                                        <strong>Promo code applied:</strong>
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="3" headers="c4" style="padding-top: 7px;">
                                      <c:choose>
                                        <c:when test="${application.getCampaignCode() != null&&fn:length(fn:trim(application.getCampaignCode()))>0}">
                                          ${application.getCampaignCode()}
                                       </c:when>
                                       <c:otherwise>
                                          Nil
                                       </c:otherwise>
                                       </c:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="3" scope="colgroup" id="c5">
                                        <strong>Payment frequency:</strong>
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="3" headers="c5" style="padding-top: 7px;">
                                        <c:out value="${application.getPaymentModeLabel()}"></c:out>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="border-top:1px solid #999;" scope="colgroup">
                                        <a href="/our-plans/online-protector/7" type="button" class="back_btn btn" style="color:#333;font-size:1em;padding-top:10px;">Edit your plan</a>
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                        </div>

                        <div class="col-xs-12 col-sm-9">
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/7">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                        </div>
                    </form>
                                        
                </div>
            </div>

        </section>
    </article>
</div>

<style>
    .form-group__multi > span:before {
        content: "";
    }
</style>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
