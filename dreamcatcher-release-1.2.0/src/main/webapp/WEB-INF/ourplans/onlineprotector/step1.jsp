<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-pdpa"></jsp:param>
</jsp:include>

<div class="main step1 onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>

    <section class="container-fluid" style="padding:0;">
        <div class="header-page header-onlineprotector" style="margin-bottom:0;">
            <div class="container">
            </div>
        </div>
    </section>

    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12 col-sm-10">
                        <h2>Personal Data Protection Act (PDPA)</h2>
                        <p>Your privacy is important to us. By proceeding with this transaction, you consent to the collection, use and disclosure of your personal data by HSBC Insurance (Singapore) Pte Limited ("HSBC Insurance"), our agents and authorised service providers as well as relevant third parties, for purposes reasonably required by HSBC Insurance to provide the products or services which you are applying for. Our Data Protection Policy outlines how your personal data will be managed in accordance with the Personal Data Protection Act ("PDPA") which strives to protect personal data of individuals. Our Data Protection Policy (and any updates to the same) is published at <a style="text-decoration:underline;" target="_blank" href="http://www.insurance.hsbc.com.sg/1/2/sghi/miscellaneous/privacy-policy">http://www.insurance.hsbc.com.sg/1/2/sghi/miscellaneous/privacy-policy</a>. Subject to your rights at law, the prevailing terms of our Data Protection Policy shall apply to you.</p>
                        <div style="margin-top:50px;">
                            <form class="buynow-form" method="post">
                                <a class="btn btn-red" href="#" data-form-action="accept" role="button">I agree</a>
                                <a type="button" class="btn btn-white" data-form-action="reject" role="button">I disagree</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
