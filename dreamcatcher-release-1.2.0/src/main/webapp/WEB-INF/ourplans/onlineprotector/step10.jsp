<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-occupationdetails"></jsp:param>
</jsp:include>
<div class="main onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>

                    <form class="buynow-form" method="post" data-form-name="onlineprotector-step">
                        <div class="col-xs-12 col-sm-9 col-md-8">
                            <c:if test="${application.applicantIsWorking()}">
                                <div class="row">
                                    <div class="col-xs-12 col-md-5"><label for="form-occupation"><c:out value="${application.getApplicantFirstName()}"></c:out>, what is your occupation?</label></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_occupation')) ? 'error' : ''}"></c:out>" data-field-name="applicant_occupation">
                                        <input name="applicant_occupation" class="form-control" id="form-occupation" value="${fn:escapeXml(data['applicant_occupation'])}" maxlength="100" aria-describedby="applicantOccupation">

                                        <c:if test="${errors.containsKey('applicant_occupation')}">
                                                <span class="error-message" aria-label="Error" id="applicantOccupation">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_occupation']}"></c:out>
                                                </span>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-5"><label for="form-industry" id="form-industry-label">Which industry do you work in?</label></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_industry')) ? 'error' : ''}"></c:out>" data-field-name="applicant_industry">
                                        <div class="form-select_wrap">
                                            <select name="applicant_industry" id="form-industry" aria-describedby="formIndustry" class="form-control">
                                                <jsp:include page="/WEB-INF/ourplans/includes/applicant_industry.jsp">
                                            	 <jsp:param name="key" value="applicant_industry"></jsp:param>
                                            	</jsp:include>
                                            </select>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_industry')}">
                                            <span class="error-message" aria-label="Error" id="formIndustry">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_industry']}"></c:out>
                                            </span>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-5">
                                        <label for="form-companyname">
                                            <c:if test="${application.applicantIsSelfEmployed()}">Company name</c:if>
                                            <c:if test="${not application.applicantIsSelfEmployed()}">Employer name</c:if>
                                        </label>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_company_name')) ? 'error' : ''}"></c:out>" data-field-name="applicant_company_name">
                                        <input name="applicant_company_name" id="form-companyname" class="form-control" value="${fn:escapeXml(data['applicant_company_name'])}" maxlength="50" aria-describedby="applicantCompanyName">

                                        <c:if test="${errors.containsKey('applicant_company_name')}">
                                                <span class="error-message" aria-label="Error" id="applicantCompanyName">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_company_name']}"></c:out>
                                                </span>
                                        </c:if>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-5"><label for="form-city">City</label></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_company_city')) ? 'error' : ''}"></c:out>" data-field-name="applicant_company_city">
                                        <input name="applicant_company_city" id="form-city" class="form-control" value="${fn:escapeXml(data['applicant_company_city'])}" maxlength="50" aria-describedby="applicantCompanyCity">

                                        <c:if test="${errors.containsKey('applicant_company_city')}">
                                                <span class="error-message" aria-label="Error" id="applicantCompanyCity">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_company_city']}"></c:out>
                                                </span>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-5"><label for="form-country">Country</label></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_company_country')) ? 'error' : ''}"></c:out>" data-field-name="applicant_company_country">
                                        <input name="applicant_company_country" id="form-country" class="form-control" value="${fn:escapeXml(data['applicant_company_country'])}" maxlength="50" aria-describedby="applicantCompanyCountry">

                                        <c:if test="${errors.containsKey('applicant_company_country')}">
                                        <span class="error-message" aria-label="Error" id="applicantCompanyCountry">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['applicant_company_country']}"></c:out>
                                        </span>
                                        </c:if>
                                    </div>
                                </div>
                            </c:if>
                                    
                            <c:if test="${application.applicantIsStudying()}">
                                <div class="row">
                                    <div class="col-xs-12 col-md-5"><label for="date-studiesapplicant"><c:out value="${application.getApplicantFirstName()}"></c:out>, what is the end date of your studies?</label></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_studies_end_date')) ? 'error' : ''}"></c:out>" data-field-name="applicant_studies_end_date">
                                        <input id="date-studiesapplicant" type="text" name="applicant_studies_end_date" placeholder="mm/yyyy" maxlength="7" class="date-validate-month auto-width form-control date-studies" aria-describedby="applicantStudiesEnd"
                                           value="${fn:escapeXml(data['applicant_studies_end_date'])}">
                                        <c:if test="${errors.containsKey('applicant_studies_end_date')}">
                                            <span class="error-message" aria-label="Error" id="applicantStudiesEnd">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_studies_end_date']}"></c:out>
                                            </span>
                                        </c:if>
                                    </div>

                                </div>
                            </c:if>
                            
                            <c:if test="${not application.insuredIsSelf()}">
                                <c:if test="${application.insuredIsWorking()}">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5"><label for="form-occupationspouse">What is <c:out value="${application.getInsuredPossessiveLabel(true)}"></c:out> occupation?</label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_occupation')) ? 'error' : ''}"></c:out>" data-field-name="insured_occupation">
                                            <input name="insured_occupation" id="form-occupationspouse" class="form-control" value="${fn:escapeXml(data['insured_occupation'])}" aria-describedby="insuredOccupation">

                                            <c:if test="${errors.containsKey('insured_occupation')}">
                                                <span class="error-message" aria-label="Error" id="insuredOccupation">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_occupation']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5"><label for="form-industryspouse" id="form-industryspouse-label">Which industry does <c:out value="${application.getInsuredFirstName()}"></c:out> work in?</label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_industry')) ? 'error' : ''}"></c:out>" data-field-name="insured_industry" aria-describedby="insuredIndustry">
                                            <div class="form-select_wrap">
                                                <select name="insured_industry" class="form-control" id="form-industryspouse">
                                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_industry.jsp" flush="true">
                                            	 		<jsp:param name="key" value="insured_industry"></jsp:param>
                                                    </jsp:include>
                                                </select>
                                            </div>
                                            <c:if test="${errors.containsKey('insured_industry')}">
                                            <span class="error-message" aria-label="Error" id="insuredIndustry">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['insured_industry']}"></c:out>
                                            </span>

                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5">
                                            <label for="form-companynamespouse">
                                                <c:if test="${application.insuredIsSelfEmployed()}">Company name</c:if>
                                                <c:if test="${not application.insuredIsSelfEmployed()}">Employer name</c:if>
                                            </label>
                                        </div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_company_name')) ? 'error' : ''}"></c:out>" data-field-name="insured_company_name">
                                            <input name="insured_company_name" id="form-companynamespouse" class="form-control" value="${fn:escapeXml(data['insured_company_name'])}" aria-describedby="insuredCompanyName">

                                            <c:if test="${errors.containsKey('insured_company_name')}">
                                                <span class="error-message" aria-label="Error" id="insuredCompanyName">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_company_name']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                </c:if>

                                <c:if test="${application.insuredIsStudying()}">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5"><label for="date-studiesinsured">What is the end date of <c:out value="${application.getInsuredPossessiveLabel(true)}"></c:out> studies?</label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_studies_end_date')) ? 'error' : ''}"></c:out>" data-field-name="insured_studies_end_date">
                                            <input id="date-studiesinsured" type="text" name="insured_studies_end_date" placeholder="mm/yyyy" maxlength="7" class="date-validate-month auto-width form-control date-studies" aria-describedby="insuredStudiesEnd"
                                               value="${fn:escapeXml(data['insured_studies_end_date'])}">
                                            <c:if test="${errors.containsKey('insured_studies_end_date')}">
                                                <span class="error-message" aria-label="Error" id="insuredStudiesEnd">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_studies_end_date']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>

                                    </div>
                                </c:if>
                            </c:if>
                            
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="space40"></div>
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/9">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                        </div>
                        
                    </form>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
