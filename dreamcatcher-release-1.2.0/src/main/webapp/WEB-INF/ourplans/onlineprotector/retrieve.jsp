<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Retrieve Your Application - Onilne Protector"></jsp:param>
    <jsp:param name="metadataKeywords" value="hsbc online protector application, retrieve online protector, continue insurance application"></jsp:param>
	<jsp:param name="metadataDescription" value="All it takes is two simple steps to retrieve and continue your online insurance application for HSBC's OnlineProtector plan."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/our-plans/online-protector/retrieve/"></jsp:param>
</jsp:include>


<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/our-plans/">Our plans</a>
        <a href="/our-plans/online-protector/">HSBC Insurance OnlineProtector</a>
        <a href="/our-plans/online-protector/retrieve/" style="text-decoration: underline;" aria-current="page">Retrieve your form</a>
    </nav>
</div>

<section class="container-fluid">
    <div class="header-page header-onlineprotector">
        <div class="container">
            <div class="featured">
                <h1>Retrieve your OnlineProtector application</h1>
                <p>All it takes is two simple steps to retrieve your form.</p>
            </div>
        </div>
    </div>
</section>

<div class="main onlineprotector-main">
    <article>
        <section class="retrieve">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <form method="post" class="retrieve">
                           <div class="row">
                                <div class="col-sm-6">
                                    <h2><a href="/our-plans/online-protector/retrieve-form/">Continue your form<i class="icon icon-chevron-right"></i></a></h2>
                                    <p>Enjoy the convenience of continuing from where you left off.</p>
                                </div>

                                <div class="col-sm-6">
                                    <h2><a href="/our-plans/online-protector/resend-form/">Resend your completed form<i class="icon icon-chevron-right"></i></a></h2>
                                    <p>
                                        Didn't get an email or SMS upon submission of your form?<br />
                                        Retrieve your completed form here. 
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>
<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC:OurPlans:OnlinepPotector:RetrieveApplicationForm",
        "page_url"      : window.location.href,
        "page_type"     : "ApplicationForm",
        "page_language" : "en",
		"page_security_level"   :   "0",
        "page_business_line"   :   "Insurance",
        "page_customer_group"   :   "General"
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>
