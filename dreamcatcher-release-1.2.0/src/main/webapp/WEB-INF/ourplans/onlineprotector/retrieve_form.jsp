<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Retrieve Your Application - Onilne Protector"></jsp:param>
    <jsp:param name="metadataKeywords" value="hsbc online protector application, retrieve online protector, continue insurance application"></jsp:param>
    <jsp:param name="metadataDescription" value="All it takes is two simple steps to retrieve and continue your online insurance application for HSBC's OnlineProtector plan."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/our-plans/online-protector/retrieve/"></jsp:param>
</jsp:include>

<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/our-plans/online-protector/retrieve/">Retrieve your form</a>
        <a href="/our-plans/online-protector/retrieve-form/" style="text-decoration: underline;" aria-current="page">Continue your form</a>
    </nav>
</div>

<div class="main">
    <article>
        <section class="onlineprotector verification
            <c:out value="${(errors.containsKey('invalid_code')) ? 'modalinvalidcode' : ''}"></c:out>
            <c:out value="${(errors.containsKey('form_not_found')) ? 'formnotfoundsimple' : ''}"></c:out>">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <form method="post">  

                            <jsp:include page="/WEB-INF/ourplans/includes/retrieve_form_inc.jsp">
                                <jsp:param name="applicationType" value="online-protector" />
                            </jsp:include>
                            
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/modal_form_not_found_simple.jsp">
    <jsp:param name="applicationType" value="online-protector" />
</jsp:include>
<jsp:include page="/WEB-INF/ourplans/includes/modal_form_not_found.jsp">
    <jsp:param name="applicationType" value="online-protector" />
</jsp:include>
<jsp:include page="/WEB-INF/ourplans/includes/modal_invalid_code.jsp">
    <jsp:param name="applicationType" value="online-protector" />
</jsp:include>

<jsp:include page="/WEB-INF/ourplans/includes/directvalueterm_tracking_application.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>
