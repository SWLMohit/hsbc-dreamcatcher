<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-addressdetails"></jsp:param>
</jsp:include>

<div class="main onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>
                    <form class="buynow-form" method="post" data-form-name="onlineprotector-step">
                        <div class="col-xs-12 col-sm-9 col-md-8">
                            <div class="row">
                                <div class="col-xs-12 col-md-5"><c:out value="${application.getApplicantFirstName()}"></c:out>, what is your residential and mailing address (in Singapore)?</div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
                                    <div class="form-group__multi form-group__multi--residential_line1">
                                        <span>
                                            <p><label for="form-postcode">Postal code</label></p>
                                            <input name="applicant_address_postal_code" id="form-postcode" data-lookup="postalCode" class="form-control <c:out value="${(errors.containsKey('applicant_address_postal_code')) ? 'error' : ''}"></c:out>" value="${fn:escapeXml(data['applicant_address_postal_code'])}" maxlength="6" aria-describedby="applicantAddressPostalCode" data-field-name="applicant_address_postal_code">
                                        </span>
                                        <span>
                                            <p><label for="form-houseno">House / Blk number</label></p>
                                            <input name="applicant_address_house" id="form-houseno" data-lookup="buildingNumber" class="form-control <c:out value="${(errors.containsKey('applicant_address_house')) ? 'error' : ''}"></c:out>" value="${fn:escapeXml(data['applicant_address_house'])}" maxlength="10" aria-describedby="applicantAddressHouse" data-field-name="applicant_address_house">

                                        </span>
                                    </div>
                                    <c:if test="${errors.containsKey('applicant_address_postal_code')}">
                                        <div>
                                            <span class="error-message" aria-label="Error" id="applicantAddressPostalCode">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_address_postal_code']}"></c:out>
                                            </span>
                                        </div>
                                    </c:if>
                                    <c:if test="${errors.containsKey('applicant_address_house')}">
                                        <div>
                                            <span class="error-message" aria-label="Error" id="applicantAddressHouse">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_address_house']}"></c:out>
                                            </span>
                                        </div>
                                    </c:if>

                                </div>
                                <div class="col-xs-12 col-md-6 col-md-offset-6 form-group <c:out value="${(errors.containsKey('applicant_address_unit')) ? 'error' : ''}"></c:out>" data-field-name="applicant_address_unit">
                                    <fieldset>
                                        <legend>Unit number</legend>
                                        <div class="form-group__multi form-group__multi--residential_line2">
                                            <span>
                                                <label for="form-floorno" class="visuallyhidden">Floor number</label>
                                                <input name="applicant_address_unit_floor" id="form-floorno" class="form-control" value="${fn:escapeXml(data['applicant_address_unit_floor'])}" maxlength="3">
                                            </span>
                                            <span>
                                                <label for="form-unitno" class="visuallyhidden">Unit number</label>
                                              <input name="applicant_address_unit_num" id="form-unitno" class="form-control" value="${fn:escapeXml(data['applicant_address_unit_num'])}" maxlength="10" aria-describedby="applicantAddressUnit">
                                            </span>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_address_unit')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="applicantAddressUnit">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_address_unit']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </fieldset>
                                </div>
                                <div class="col-xs-12 col-md-6 col-md-offset-6 form-group <c:out value="${(errors.containsKey('applicant_address_street')) ? 'error' : ''}"></c:out>" data-field-name="applicant_address_street">
                                    <p><label for="form-street">Street</label></p>
                                    <input name="applicant_address_street" id="form-street" data-lookup="streetName" class="form-control" value="${fn:escapeXml(data['applicant_address_street'])}" maxlength="50" aria-describedby="applicantAddressStreet">
                                    <c:if test="${errors.containsKey('applicant_address_street')}">
                                        <div>
                                            <span class="error-message" aria-label="Error" id="applicantAddressStreet">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_address_street']}"></c:out>
                                            </span>
                                        </div>
                                    </c:if>
                                </div>
                                <div class="col-xs-12 col-md-6 col-md-offset-6 form-group <c:out value="${(errors.containsKey('applicant_address_building')) ? 'error' : ''}"></c:out>" data-field-name="applicant_address_building">
                                    <p><label for="form-building">Building name <small>(optional)</small></label></p>
                                    <input name="applicant_address_building" id="form-building" data-lookup="buildingName" class="form-control" value="${fn:escapeXml(data['applicant_address_building'])}" maxlength="50" aria-describedby="applicantAddressBuilding">

                                    <c:if test="${errors.containsKey('applicant_address_building')}">
                                        <div>
                                            <span class="error-message" aria-label="Error" id="applicantAddressBuilding">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_address_building']}"></c:out>
                                            </span>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            
                            <c:if test="${application.applicantIsSingaporean() or application.applicantIsPermanentResident()}">
                                <div class="row">
                                    <fieldset>
                                        <legend class="col-xs-12 col-md-5">Is the above residential address the same as stated on your NRIC?</legend>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_address_registered')) ? 'error' : ''}"></c:out>" data-field-name="applicant_address_registered">
                                            <div class="newradio--inline">
                                                <input id="id_applicant_address_same_registered_option1" type="radio" name="applicant_address_registered" value="Y" <c:if test="${data['applicant_address_registered']}">checked</c:if>>
                                                <label for="id_applicant_address_same_registered_option1"><span><span></span></span>Yes</label>
                                            </div>
                                            <div class="newradio--inline">
                                                <input id="id_applicant_address_same_registered_option2" type="radio" name="applicant_address_registered" value="N" <c:if test="${data['applicant_address_registered'] eq false}">checked</c:if> aria-describedby="applicantAddressRegistered">
                                                <label for="id_applicant_address_same_registered_option2"><span><span></span></span>No</label>
                                            </div>
                                            <c:if test="${errors.containsKey('applicant_address_registered')}">
                                                <div>
                                                        <span class="error-message" aria-label="Error" id="applicantAddressRegistered">
                                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['applicant_address_registered']}"></c:out>
                                                        </span>
                                                </div>
                                            </c:if>
                                        </div>
                                    </fieldset> 
                                </div>
                            </c:if>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-5">How long have you been living at the residential address that you provided above?</legend>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_address_period_stay')) ? 'error' : ''}"></c:out>" data-field-name="applicant_address_period_stay">
                                        <div class="form-group__multi form-group__multi--residential_line1">
                                            <span>
                                              <p><label for="form-yearsaddress">Years</label></p>
                                              <input type="text" id="form-yearsaddress" name="applicant_address_period_stay_years" class="form-control" value="${fn:escapeXml(data['applicant_address_period_stay_years'])}" maxlength="3">
                                            </span>
                                            <span>
                                              <p><label for="form-monthsaddress">Months</label></p>
                                              <input type="text" id="form-monthsaddress" name="applicant_address_period_stay_months" class="form-control" value="${fn:escapeXml(data['applicant_address_period_stay_months'])}" maxlength="2" aria-describedby="applicantAddressPeriodStay">

                                            </span>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_address_period_stay')}">
                                                <span class="error-message" aria-label="Error" id="applicantAddressPeriodStay">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_address_period_stay']}"></c:out>
                                                </span>
                                        </c:if>

                                    </div>
                                </fieldset>
                            </div>
                            <div class="row" data-question-group="address_less_than_5yrs">
                                <div class="col-xs-12 col-md-5"><label for="formpreviousaddresscountry"  id="form-previousaddresscountry-label">Please state the country of your previous residential address as you have not resided at your current address for 5 years or more.</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_previous_address_country')) ? 'error' : ''}"></c:out>" data-field-name="applicant_previous_address_country">
                                    <div class="form-select_wrap">
                                        <select name="applicant_previous_address_country" id="formpreviousaddresscountry"  class="form-control control_country_select" aria-describedby="applicantPreviousCountry">
                                            <option value="">Please select...</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Afghanistan'}">selected</c:if>>Afghanistan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Albania'}">selected</c:if>>Albania</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Algeria'}">selected</c:if>>Algeria</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'American Samoa'}">selected</c:if>>American Samoa</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Andorra'}">selected</c:if>>Andorra</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Angola'}">selected</c:if>>Angola</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Anguilla'}">selected</c:if>>Anguilla</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Antarctica'}">selected</c:if>>Antarctica</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Antigua and Barbuda'}">selected</c:if>>Antigua and Barbuda</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Argentina'}">selected</c:if>>Argentina</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Armenia'}">selected</c:if>>Armenia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Aruba'}">selected</c:if>>Aruba</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Australia'}">selected</c:if>>Australia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Austria'}">selected</c:if>>Austria</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Azerbaijan'}">selected</c:if>>Azerbaijan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bahama'}">selected</c:if>>Bahama</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bahrain'}">selected</c:if>>Bahrain</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bangladesh'}">selected</c:if>>Bangladesh</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Barbados'}">selected</c:if>>Barbados</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Belarus'}">selected</c:if>>Belarus</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Belgium'}">selected</c:if>>Belgium</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Belize'}">selected</c:if>>Belize</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Benin'}">selected</c:if>>Benin</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bermuda'}">selected</c:if>>Bermuda</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bhutan'}">selected</c:if>>Bhutan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bolivia'}">selected</c:if>>Bolivia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bosnia and Herzegovina'}">selected</c:if>>Bosnia and Herzegovina</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Botswana'}">selected</c:if>>Botswana</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bouvet Island'}">selected</c:if>>Bouvet Island</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Brazil'}">selected</c:if>>Brazil</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'British Indian Ocean Territory'}">selected</c:if>>British Indian Ocean Territory</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'British Virgin Islands'}">selected</c:if>>British Virgin Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Brunei Darussalam'}">selected</c:if>>Brunei Darussalam</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Bulgaria'}">selected</c:if>>Bulgaria</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Burkina Faso'}">selected</c:if>>Burkina Faso</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Burundi'}">selected</c:if>>Burundi</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Cambodia'}">selected</c:if>>Cambodia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Cameroon'}">selected</c:if>>Cameroon</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Canada'}">selected</c:if>>Canada</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Cape Verde'}">selected</c:if>>Cape Verde</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Cayman Islands'}">selected</c:if>>Cayman Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Central African Republic'}">selected</c:if>>Central African Republic</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Chad'}">selected</c:if>>Chad</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Chile'}">selected</c:if>>Chile</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'China'}">selected</c:if>>China</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Christmas Island'}">selected</c:if>>Christmas Island</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Cocos (Keeling) Islands'}">selected</c:if>>Cocos (Keeling) Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Colombia'}">selected</c:if>>Colombia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Comoros'}">selected</c:if>>Comoros</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Congo'}">selected</c:if>>Congo</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Cook Islands'}">selected</c:if>>Cook Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Costa Rica'}">selected</c:if>>Costa Rica</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Croatia'}">selected</c:if>>Croatia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Cuba'}">selected</c:if>>Cuba</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Cyprus'}">selected</c:if>>Cyprus</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Czech Republic'}">selected</c:if>>Czech Republic</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Denmark'}">selected</c:if>>Denmark</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Djibouti'}">selected</c:if>>Djibouti</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Dominica'}">selected</c:if>>Dominica</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Dominican Republic'}">selected</c:if>>Dominican Republic</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'East Timor'}">selected</c:if>>East Timor</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Ecuador'}">selected</c:if>>Ecuador</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Egypt'}">selected</c:if>>Egypt</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'El Salvador'}">selected</c:if>>El Salvador</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Equatorial Guinea'}">selected</c:if>>Equatorial Guinea</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Eritrea'}">selected</c:if>>Eritrea</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Estonia'}">selected</c:if>>Estonia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Ethiopia'}">selected</c:if>>Ethiopia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Falkland Islands (Malvinas)'}">selected</c:if>>Falkland Islands (Malvinas)</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Faroe Islands'}">selected</c:if>>Faroe Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Fiji'}">selected</c:if>>Fiji</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Finland'}">selected</c:if>>Finland</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'France'}">selected</c:if>>France</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'France, Metropolitan'}">selected</c:if>>France, Metropolitan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'French Guiana'}">selected</c:if>>French Guiana</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'French Polynesia'}">selected</c:if>>French Polynesia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'French Southern Territories'}">selected</c:if>>French Southern Territories</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Gabon'}">selected</c:if>>Gabon</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Gambia'}">selected</c:if>>Gambia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Georgia'}">selected</c:if>>Georgia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Germany'}">selected</c:if>>Germany</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Ghana'}">selected</c:if>>Ghana</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Gibraltar'}">selected</c:if>>Gibraltar</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Greece'}">selected</c:if>>Greece</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Greenland'}">selected</c:if>>Greenland</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Grenada'}">selected</c:if>>Grenada</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Guadeloupe'}">selected</c:if>>Guadeloupe</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Guam'}">selected</c:if>>Guam</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Guatemala'}">selected</c:if>>Guatemala</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Guinea'}">selected</c:if>>Guinea</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Guinea-Bissau'}">selected</c:if>>Guinea-Bissau</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Guyana'}">selected</c:if>>Guyana</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Haiti'}">selected</c:if>>Haiti</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Heard Island and McDonald Islands'}">selected</c:if>>Heard Island and McDonald Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Honduras'}">selected</c:if>>Honduras</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Hong Kong'}">selected</c:if>>Hong Kong</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Hungary'}">selected</c:if>>Hungary</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Iceland'}">selected</c:if>>Iceland</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'India'}">selected</c:if>>India</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Indonesia'}">selected</c:if>>Indonesia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Islamic Republic of Iran'}">selected</c:if>>Islamic Republic of Iran</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Iraq'}">selected</c:if>>Iraq</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Ireland'}">selected</c:if>>Ireland</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Israel'}">selected</c:if>>Israel</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Italy'}">selected</c:if>>Italy</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Jamaica'}">selected</c:if>>Jamaica</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Japan'}">selected</c:if>>Japan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Jordan'}">selected</c:if>>Jordan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Kazakhstan'}">selected</c:if>>Kazakhstan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Kenya'}">selected</c:if>>Kenya</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Kiribati'}">selected</c:if>>Kiribati</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Korea, Democratic People\\\'s Republic of'}">selected</c:if>>Korea, Democratic People's Republic of</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Korea, Republic of'}">selected</c:if>>Korea, Republic of</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Kuwait'}">selected</c:if>>Kuwait</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Kyrgyzstan'}">selected</c:if>>Kyrgyzstan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Lao People\\\'s Democratic Republic'}">selected</c:if>>Lao People's Democratic Republic</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Latvia'}">selected</c:if>>Latvia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Lebanon'}">selected</c:if>>Lebanon</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Lesotho'}">selected</c:if>>Lesotho</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Liberia'}">selected</c:if>>Liberia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Libyan Arab Jamahiriya'}">selected</c:if>>Libyan Arab Jamahiriya</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Liechtenstein'}">selected</c:if>>Liechtenstein</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Lithuania'}">selected</c:if>>Lithuania</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Luxembourg'}">selected</c:if>>Luxembourg</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Macau'}">selected</c:if>>Macau</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Madagascar'}">selected</c:if>>Madagascar</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Malawi'}">selected</c:if>>Malawi</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Malaysia'}">selected</c:if>>Malaysia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Maldives'}">selected</c:if>>Maldives</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Mali'}">selected</c:if>>Mali</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Malta'}">selected</c:if>>Malta</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Marshall Islands'}">selected</c:if>>Marshall Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Martinique'}">selected</c:if>>Martinique</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Mauritania'}">selected</c:if>>Mauritania</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Mauritius'}">selected</c:if>>Mauritius</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Mayotte'}">selected</c:if>>Mayotte</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Mexico'}">selected</c:if>>Mexico</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Micronesia'}">selected</c:if>>Micronesia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Moldova, Republic of'}">selected</c:if>>Moldova, Republic of</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Monaco'}">selected</c:if>>Monaco</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Mongolia'}">selected</c:if>>Mongolia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Monserrat'}">selected</c:if>>Monserrat</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Morocco'}">selected</c:if>>Morocco</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Mozambique'}">selected</c:if>>Mozambique</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Myanmar'}">selected</c:if>>Myanmar</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Nambia'}">selected</c:if>>Nambia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Nauru'}">selected</c:if>>Nauru</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Nepal'}">selected</c:if>>Nepal</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Netherlands'}">selected</c:if>>Netherlands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Netherlands Antilles'}">selected</c:if>>Netherlands Antilles</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'New Caledonia'}">selected</c:if>>New Caledonia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'New Zealand'}">selected</c:if>>New Zealand</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Nicaragua'}">selected</c:if>>Nicaragua</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Niger'}">selected</c:if>>Niger</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Nigeria'}">selected</c:if>>Nigeria</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Niue'}">selected</c:if>>Niue</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Norfolk Island'}">selected</c:if>>Norfolk Island</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Northern Mariana Islands'}">selected</c:if>>Northern Mariana Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Norway'}">selected</c:if>>Norway</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Oman'}">selected</c:if>>Oman</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Pakistan'}">selected</c:if>>Pakistan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Palau'}">selected</c:if>>Palau</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Panama'}">selected</c:if>>Panama</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Papua New Guinea'}">selected</c:if>>Papua New Guinea</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Paraguay'}">selected</c:if>>Paraguay</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Peru'}">selected</c:if>>Peru</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Philippines'}">selected</c:if>>Philippines</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Pitcairn'}">selected</c:if>>Pitcairn</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Poland'}">selected</c:if>>Poland</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Portugal'}">selected</c:if>>Portugal</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Puerto Rico'}">selected</c:if>>Puerto Rico</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Qatar'}">selected</c:if>>Qatar</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Romania'}">selected</c:if>>Romania</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Russian Federation'}">selected</c:if>>Russian Federation</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Rwanda'}">selected</c:if>>Rwanda</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'S. Georgia & the S. Sandwich Islands'}">selected</c:if>>S. Georgia & the S. Sandwich Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Saint Lucia'}">selected</c:if>>Saint Lucia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Samoa'}">selected</c:if>>Samoa</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'San Marino'}">selected</c:if>>San Marino</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Sao Tome and Principe'}">selected</c:if>>Sao Tome and Principe</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Saudi Arabia'}">selected</c:if>>Saudi Arabia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Senegal'}">selected</c:if>>Senegal</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Seychelles'}">selected</c:if>>Seychelles</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Sierra Leone'}">selected</c:if>>Sierra Leone</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Slovakia'}">selected</c:if>>Slovakia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Slovenia'}">selected</c:if>>Slovenia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Solomon Islands'}">selected</c:if>>Solomon Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Somalia'}">selected</c:if>>Somalia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'South Africa'}">selected</c:if>>South Africa</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Spain'}">selected</c:if>>Spain</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Sri Lanka'}">selected</c:if>>Sri Lanka</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'St. Helena'}">selected</c:if>>St. Helena</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'St. Kitts and Nevis'}">selected</c:if>>St. Kitts and Nevis</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'St. Pierre and Miquelon'}">selected</c:if>>St. Pierre and Miquelon</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'St. Vincent and the Grenadines'}">selected</c:if>>St. Vincent and the Grenadines</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Sudan'}">selected</c:if>>Sudan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Suriname'}">selected</c:if>>Suriname</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Svalbard and Jan Mayen Islands'}">selected</c:if>>Svalbard and Jan Mayen Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Swaziland'}">selected</c:if>>Swaziland</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Sweden'}">selected</c:if>>Sweden</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Switzerland'}">selected</c:if>>Switzerland</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Syrian Arab Republic'}">selected</c:if>>Syrian Arab Republic</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Taiwan, Province of China'}">selected</c:if>>Taiwan, Province of China</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Tajikistan'}">selected</c:if>>Tajikistan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Thailand'}">selected</c:if>>Thailand</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Togo'}">selected</c:if>>Togo</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Tokelau'}">selected</c:if>>Tokelau</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Tonga'}">selected</c:if>>Tonga</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Trinidad and Tobago'}">selected</c:if>>Trinidad and Tobago</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Tunisia'}">selected</c:if>>Tunisia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Turkey'}">selected</c:if>>Turkey</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Turkmenistan'}">selected</c:if>>Turkmenistan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Turks and Caicos Islands'}">selected</c:if>>Turks and Caicos Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Tuvalu'}">selected</c:if>>Tuvalu</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Uganda'}">selected</c:if>>Uganda</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Ukraine'}">selected</c:if>>Ukraine</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'United Arab Emirates'}">selected</c:if>>United Arab Emirates</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'United Kingdom (Great Britain)'}">selected</c:if>>United Kingdom (Great Britain)</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'United Republic of Tanzania'}">selected</c:if>>United Republic of Tanzania</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'United States of America'}">selected</c:if>>United States of America</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'United States Minor Outlying Islands'}">selected</c:if>>United States Minor Outlying Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'United States Virgin Islands'}">selected</c:if>>United States Virgin Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Uruguay'}">selected</c:if>>Uruguay</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Uzbekistan'}">selected</c:if>>Uzbekistan</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Vanuatu'}">selected</c:if>>Vanuatu</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Vatican City State (Holy See)'}">selected</c:if>>Vatican City State (Holy See)</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Venezuela'}">selected</c:if>>Venezuela</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Viet Nam'}">selected</c:if>>Viet Nam</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Wallis and Futuna Islands'}">selected</c:if>>Wallis and Futuna Islands</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Western Sahara'}">selected</c:if>>Western Sahara</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Yugoslavia'}">selected</c:if>>Yugoslavia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Zaire'}">selected</c:if>>Zaire</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Zambia'}">selected</c:if>>Zambia</option>
                                            <option <c:if test="${data['applicant_previous_address_country'] == 'Zimbabwe'}">selected</c:if>>Zimbabwe</option>
                                            <option value="NA" <c:if test="${data['applicant_previous_address_country'] == 'NA'}">selected</c:if>>None of the above</option>
                                        </select>
                                    </div>
                                    <c:if test="${errors.containsKey('applicant_previous_address_country')}">
                                            <span class="error-message" aria-label="Error" id="applicantPreviousCountry">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_previous_address_country']}"></c:out>
                                            </span>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row">
                                <c:if test="${application.applicantIsPermanentResident() || application.applicantIsPassHolder()}">
                                    <c:if test="${application.applicantIsPermanentResident()}">
                                        <fieldset>
                                            <legend class="col-xs-12 col-md-5">Is the above residential address also your permanent address?<button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content='"Permanent address" refers to an address where a customer intends to stay permanently.' aria-label='"Permanent address" refers to an address where a customer intends to stay permanently.'><i class="icon icon-circle-help-solid"></i></button></legend>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_address_permanent')) ? 'error' : ''}"></c:out>" data-field-name="applicant_address_permanent">
                                                <div class="newradio--inline">
                                                    <input id="id_applicant_address_permanent_option1" type="radio" name="applicant_address_permanent" value="Y" <c:if test="${data['applicant_address_permanent']}">checked</c:if>>
                                                    <label for="id_applicant_address_permanent_option1"><span><span></span></span>Yes</label>
                                                </div>
                                                <div class="newradio--inline">
                                                    <input id="id_applicant_address_permanent_option2" type="radio" name="applicant_address_permanent" value="N" <c:if test="${data['applicant_address_permanent'] eq false}">checked</c:if> aria-describedby="applicantAddressPermanent">
                                                    <label for="id_applicant_address_permanent_option2"><span><span></span></span>No</label>
                                                </div>
                                                <c:if test="${errors.containsKey('applicant_address_permanent')}">
                                                    <div>
                                                        <span class="error-message" aria-label="Error" id="applicantAddressPermanent">
                                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['applicant_address_permanent']}"></c:out>
                                                        </span>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </fieldset>
                                    </c:if>
                                    <div class="col-xs-12 col-md-5" data-question-group="<c:if test="${application.applicantIsPermanentResident()}">permanent_address</c:if>">Permanent address
                                        <button type="button" class="help-tooltip" tabindex="0" data-container="body" label="help" data-toggle="popover" data-placement="top" data-content='"Permanent address" refers to an address where a customer intends to stay permanently.' aria-label='"Permanent address" refers to an address where a customer intends to stay permanently.'><i class="icon icon-circle-help-solid"></i></button>
                                        <small>(For foreigners, please indicate your overseas permanent address)</small>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1" data-question-group="<c:if test="${application.applicantIsPermanentResident()}">permanent_address</c:if>" class="form-group <c:out value="${(errors.containsKey('applicant_permanent_address_line1')) ? 'error' : ''}"></c:out>" data-field-name="applicant_permanent_address_line1">
                                        <p><label for="form-permanentaddressline1">Address Line 1:</label></p>
                                        <input name="applicant_permanent_address_line1" id="form-permanentaddressline1" class="form-control" value="${fn:escapeXml(data['applicant_permanent_address_line1'])}" maxlength="50" aria-describedby="applicantPermanentAddress">

                                        <c:if test="${errors.containsKey('applicant_permanent_address_line1')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="applicantPermanentAddress">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_permanent_address_line1']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-6 <c:out value="${(errors.containsKey('applicant_permanent_address_line2')) ? 'error' : ''}"></c:out>" data-question-group="<c:if test="${application.applicantIsPermanentResident()}">permanent_address</c:if>" data-field-name="applicant_permanent_address_line2">

                                        <p><label for="form-permanentaddressline2">Address Line 2:</label></p>
                                        <input name="applicant_permanent_address_line2" id="form-permanentaddressline2" class="form-control" value="${fn:escapeXml(data['applicant_permanent_address_line2'])}" maxlength="50" aria-describedby="applicantPermanentAddress2">
                                        <c:if test="${errors.containsKey('applicant_permanent_address_line2')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="applicantPermanentAddress2">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_permanent_address_line2']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-6 <c:out value="${(errors.containsKey('applicant_permanent_address_postal_code')) ? 'error' : ''}"></c:out>" data-question-group="<c:if test="${application.applicantIsPermanentResident()}">permanent_address</c:if>" data-field-name="applicant_permanent_address_postal_code">
                                        <p><label for="form-permanentpostcode">Postal code</label></p>
                                        <input name="applicant_permanent_address_postal_code" id="form-permanentpostcode" class="form-control auto-width" value="${fn:escapeXml(data['applicant_permanent_address_postal_code'])}" maxlength="10" aria-describedby="applicantPermanentAddressPostalCode">

                                        <c:if test="${errors.containsKey('applicant_permanent_address_postal_code')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="applicantPermanentAddressPostalCode">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_permanent_address_postal_code']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-6 <c:out value="${(errors.containsKey('applicant_permanent_address_country')) ? 'error' : ''}"></c:out>" data-question-group="<c:if test="${application.applicantIsPermanentResident()}">permanent_address</c:if>" data-field-name="applicant_permanent_address_country">
                                        <p><label for="form-permanentcountry" id="form-permanentcountry-label">Country</label></p>
                                        <div class="form-select_wrap">
                                            <select name="applicant_permanent_address_country" id="form-permanentcountry" class="form-control" aria-describedby="applicantPermanentAddressCountry">
                                                <option value="">Please select...</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Afghanistan'}">selected</c:if>>Afghanistan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Albania'}">selected</c:if>>Albania</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Algeria'}">selected</c:if>>Algeria</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'American Samoa'}">selected</c:if>>American Samoa</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Andorra'}">selected</c:if>>Andorra</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Angola'}">selected</c:if>>Angola</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Anguilla'}">selected</c:if>>Anguilla</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Antarctica'}">selected</c:if>>Antarctica</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Antigua and Barbuda'}">selected</c:if>>Antigua and Barbuda</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Argentina'}">selected</c:if>>Argentina</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Armenia'}">selected</c:if>>Armenia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Aruba'}">selected</c:if>>Aruba</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Australia'}">selected</c:if>>Australia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Austria'}">selected</c:if>>Austria</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Azerbaijan'}">selected</c:if>>Azerbaijan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bahama'}">selected</c:if>>Bahama</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bahrain'}">selected</c:if>>Bahrain</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bangladesh'}">selected</c:if>>Bangladesh</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Barbados'}">selected</c:if>>Barbados</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Belarus'}">selected</c:if>>Belarus</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Belgium'}">selected</c:if>>Belgium</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Belize'}">selected</c:if>>Belize</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Benin'}">selected</c:if>>Benin</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bermuda'}">selected</c:if>>Bermuda</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bhutan'}">selected</c:if>>Bhutan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bolivia'}">selected</c:if>>Bolivia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bosnia and Herzegovina'}">selected</c:if>>Bosnia and Herzegovina</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Botswana'}">selected</c:if>>Botswana</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bouvet Island'}">selected</c:if>>Bouvet Island</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Brazil'}">selected</c:if>>Brazil</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'British Indian Ocean Territory'}">selected</c:if>>British Indian Ocean Territory</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'British Virgin Islands'}">selected</c:if>>British Virgin Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Brunei Darussalam'}">selected</c:if>>Brunei Darussalam</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Bulgaria'}">selected</c:if>>Bulgaria</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Burkina Faso'}">selected</c:if>>Burkina Faso</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Burundi'}">selected</c:if>>Burundi</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Cambodia'}">selected</c:if>>Cambodia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Cameroon'}">selected</c:if>>Cameroon</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Canada'}">selected</c:if>>Canada</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Cape Verde'}">selected</c:if>>Cape Verde</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Cayman Islands'}">selected</c:if>>Cayman Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Central African Republic'}">selected</c:if>>Central African Republic</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Chad'}">selected</c:if>>Chad</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Chile'}">selected</c:if>>Chile</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'China'}">selected</c:if>>China</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Christmas Island'}">selected</c:if>>Christmas Island</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Cocos (Keeling) Islands'}">selected</c:if>>Cocos (Keeling) Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Colombia'}">selected</c:if>>Colombia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Comoros'}">selected</c:if>>Comoros</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Congo'}">selected</c:if>>Congo</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Cook Islands'}">selected</c:if>>Cook Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Costa Rica'}">selected</c:if>>Costa Rica</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Croatia'}">selected</c:if>>Croatia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Cuba'}">selected</c:if>>Cuba</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Cyprus'}">selected</c:if>>Cyprus</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Czech Republic'}">selected</c:if>>Czech Republic</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Denmark'}">selected</c:if>>Denmark</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Djibouti'}">selected</c:if>>Djibouti</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Dominica'}">selected</c:if>>Dominica</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Dominican Republic'}">selected</c:if>>Dominican Republic</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'East Timor'}">selected</c:if>>East Timor</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Ecuador'}">selected</c:if>>Ecuador</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Egypt'}">selected</c:if>>Egypt</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'El Salvador'}">selected</c:if>>El Salvador</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Equatorial Guinea'}">selected</c:if>>Equatorial Guinea</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Eritrea'}">selected</c:if>>Eritrea</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Estonia'}">selected</c:if>>Estonia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Ethiopia'}">selected</c:if>>Ethiopia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Falkland Islands (Malvinas)'}">selected</c:if>>Falkland Islands (Malvinas)</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Faroe Islands'}">selected</c:if>>Faroe Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Fiji'}">selected</c:if>>Fiji</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Finland'}">selected</c:if>>Finland</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'France'}">selected</c:if>>France</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'France, Metropolitan'}">selected</c:if>>France, Metropolitan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'French Guiana'}">selected</c:if>>French Guiana</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'French Polynesia'}">selected</c:if>>French Polynesia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'French Southern Territories'}">selected</c:if>>French Southern Territories</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Gabon'}">selected</c:if>>Gabon</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Gambia'}">selected</c:if>>Gambia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Georgia'}">selected</c:if>>Georgia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Germany'}">selected</c:if>>Germany</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Ghana'}">selected</c:if>>Ghana</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Gibraltar'}">selected</c:if>>Gibraltar</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Greece'}">selected</c:if>>Greece</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Greenland'}">selected</c:if>>Greenland</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Grenada'}">selected</c:if>>Grenada</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Guadeloupe'}">selected</c:if>>Guadeloupe</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Guam'}">selected</c:if>>Guam</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Guatemala'}">selected</c:if>>Guatemala</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Guinea'}">selected</c:if>>Guinea</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Guinea-Bissau'}">selected</c:if>>Guinea-Bissau</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Guyana'}">selected</c:if>>Guyana</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Haiti'}">selected</c:if>>Haiti</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Heard Island and McDonald Islands'}">selected</c:if>>Heard Island and McDonald Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Honduras'}">selected</c:if>>Honduras</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Hong Kong'}">selected</c:if>>Hong Kong</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Hungary'}">selected</c:if>>Hungary</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Iceland'}">selected</c:if>>Iceland</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'India'}">selected</c:if>>India</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Indonesia'}">selected</c:if>>Indonesia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Islamic Republic of Iran'}">selected</c:if>>Islamic Republic of Iran</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Iraq'}">selected</c:if>>Iraq</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Ireland'}">selected</c:if>>Ireland</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Israel'}">selected</c:if>>Israel</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Italy'}">selected</c:if>>Italy</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Jamaica'}">selected</c:if>>Jamaica</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Japan'}">selected</c:if>>Japan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Jordan'}">selected</c:if>>Jordan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Kazakhstan'}">selected</c:if>>Kazakhstan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Kenya'}">selected</c:if>>Kenya</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Kiribati'}">selected</c:if>>Kiribati</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Korea, Democratic People\\\'s Republic of'}">selected</c:if>>Korea, Democratic People's Republic of</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Korea, Republic of'}">selected</c:if>>Korea, Republic of</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Kuwait'}">selected</c:if>>Kuwait</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Kyrgyzstan'}">selected</c:if>>Kyrgyzstan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Lao People\\\'s Democratic Republic'}">selected</c:if>>Lao People's Democratic Republic</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Latvia'}">selected</c:if>>Latvia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Lebanon'}">selected</c:if>>Lebanon</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Lesotho'}">selected</c:if>>Lesotho</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Liberia'}">selected</c:if>>Liberia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Libyan Arab Jamahiriya'}">selected</c:if>>Libyan Arab Jamahiriya</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Liechtenstein'}">selected</c:if>>Liechtenstein</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Lithuania'}">selected</c:if>>Lithuania</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Luxembourg'}">selected</c:if>>Luxembourg</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Macau'}">selected</c:if>>Macau</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Madagascar'}">selected</c:if>>Madagascar</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Malawi'}">selected</c:if>>Malawi</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Malaysia'}">selected</c:if>>Malaysia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Maldives'}">selected</c:if>>Maldives</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Mali'}">selected</c:if>>Mali</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Malta'}">selected</c:if>>Malta</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Marshall Islands'}">selected</c:if>>Marshall Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Martinique'}">selected</c:if>>Martinique</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Mauritania'}">selected</c:if>>Mauritania</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Mauritius'}">selected</c:if>>Mauritius</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Mayotte'}">selected</c:if>>Mayotte</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Mexico'}">selected</c:if>>Mexico</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Micronesia'}">selected</c:if>>Micronesia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Moldova, Republic of'}">selected</c:if>>Moldova, Republic of</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Monaco'}">selected</c:if>>Monaco</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Mongolia'}">selected</c:if>>Mongolia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Monserrat'}">selected</c:if>>Monserrat</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Morocco'}">selected</c:if>>Morocco</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Mozambique'}">selected</c:if>>Mozambique</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Myanmar'}">selected</c:if>>Myanmar</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Nambia'}">selected</c:if>>Nambia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Nauru'}">selected</c:if>>Nauru</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Nepal'}">selected</c:if>>Nepal</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Netherlands'}">selected</c:if>>Netherlands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Netherlands Antilles'}">selected</c:if>>Netherlands Antilles</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'New Caledonia'}">selected</c:if>>New Caledonia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'New Zealand'}">selected</c:if>>New Zealand</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Nicaragua'}">selected</c:if>>Nicaragua</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Niger'}">selected</c:if>>Niger</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Nigeria'}">selected</c:if>>Nigeria</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Niue'}">selected</c:if>>Niue</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Norfolk Island'}">selected</c:if>>Norfolk Island</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Northern Mariana Islands'}">selected</c:if>>Northern Mariana Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Norway'}">selected</c:if>>Norway</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Oman'}">selected</c:if>>Oman</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Pakistan'}">selected</c:if>>Pakistan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Palau'}">selected</c:if>>Palau</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Panama'}">selected</c:if>>Panama</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Papua New Guinea'}">selected</c:if>>Papua New Guinea</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Paraguay'}">selected</c:if>>Paraguay</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Peru'}">selected</c:if>>Peru</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Philippines'}">selected</c:if>>Philippines</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Pitcairn'}">selected</c:if>>Pitcairn</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Poland'}">selected</c:if>>Poland</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Portugal'}">selected</c:if>>Portugal</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Puerto Rico'}">selected</c:if>>Puerto Rico</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Qatar'}">selected</c:if>>Qatar</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Romania'}">selected</c:if>>Romania</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Russian Federation'}">selected</c:if>>Russian Federation</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Rwanda'}">selected</c:if>>Rwanda</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'S. Georgia & the S. Sandwich Islands'}">selected</c:if>>S. Georgia & the S. Sandwich Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Saint Lucia'}">selected</c:if>>Saint Lucia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Samoa'}">selected</c:if>>Samoa</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'San Marino'}">selected</c:if>>San Marino</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Sao Tome and Principe'}">selected</c:if>>Sao Tome and Principe</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Saudi Arabia'}">selected</c:if>>Saudi Arabia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Senegal'}">selected</c:if>>Senegal</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Seychelles'}">selected</c:if>>Seychelles</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Sierra Leone'}">selected</c:if>>Sierra Leone</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Slovakia'}">selected</c:if>>Slovakia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Slovenia'}">selected</c:if>>Slovenia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Solomon Islands'}">selected</c:if>>Solomon Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Somalia'}">selected</c:if>>Somalia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'South Africa'}">selected</c:if>>South Africa</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Spain'}">selected</c:if>>Spain</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Sri Lanka'}">selected</c:if>>Sri Lanka</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'St. Helena'}">selected</c:if>>St. Helena</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'St. Kitts and Nevis'}">selected</c:if>>St. Kitts and Nevis</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'St. Pierre and Miquelon'}">selected</c:if>>St. Pierre and Miquelon</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'St. Vincent and the Grenadines'}">selected</c:if>>St. Vincent and the Grenadines</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Sudan'}">selected</c:if>>Sudan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Suriname'}">selected</c:if>>Suriname</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Svalbard and Jan Mayen Islands'}">selected</c:if>>Svalbard and Jan Mayen Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Swaziland'}">selected</c:if>>Swaziland</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Sweden'}">selected</c:if>>Sweden</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Switzerland'}">selected</c:if>>Switzerland</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Syrian Arab Republic'}">selected</c:if>>Syrian Arab Republic</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Taiwan, Province of China'}">selected</c:if>>Taiwan, Province of China</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Tajikistan'}">selected</c:if>>Tajikistan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Thailand'}">selected</c:if>>Thailand</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Togo'}">selected</c:if>>Togo</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Tokelau'}">selected</c:if>>Tokelau</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Tonga'}">selected</c:if>>Tonga</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Trinidad and Tobago'}">selected</c:if>>Trinidad and Tobago</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Tunisia'}">selected</c:if>>Tunisia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Turkey'}">selected</c:if>>Turkey</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Turkmenistan'}">selected</c:if>>Turkmenistan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Turks and Caicos Islands'}">selected</c:if>>Turks and Caicos Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Tuvalu'}">selected</c:if>>Tuvalu</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Uganda'}">selected</c:if>>Uganda</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Ukraine'}">selected</c:if>>Ukraine</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'United Arab Emirates'}">selected</c:if>>United Arab Emirates</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'United Kingdom (Great Britain)'}">selected</c:if>>United Kingdom (Great Britain)</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'United Republic of Tanzania'}">selected</c:if>>United Republic of Tanzania</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'United States of America'}">selected</c:if>>United States of America</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'United States Minor Outlying Islands'}">selected</c:if>>United States Minor Outlying Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'United States Virgin Islands'}">selected</c:if>>United States Virgin Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Uruguay'}">selected</c:if>>Uruguay</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Uzbekistan'}">selected</c:if>>Uzbekistan</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Vanuatu'}">selected</c:if>>Vanuatu</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Vatican City State (Holy See)'}">selected</c:if>>Vatican City State (Holy See)</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Venezuela'}">selected</c:if>>Venezuela</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Viet Nam'}">selected</c:if>>Viet Nam</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Wallis and Futuna Islands'}">selected</c:if>>Wallis and Futuna Islands</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Western Sahara'}">selected</c:if>>Western Sahara</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Yugoslavia'}">selected</c:if>>Yugoslavia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Zaire'}">selected</c:if>>Zaire</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Zambia'}">selected</c:if>>Zambia</option>
                                                <option <c:if test="${data['applicant_permanent_address_country'] == 'Zimbabwe'}">selected</c:if>>Zimbabwe</option>
                                                <option value="NA" <c:if test="${data['applicant_permanent_address_country'] == 'NA'}">selected</c:if>>None of the above</option>
                                            </select>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_permanent_address_country')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="applicantPermanentAddressCountry">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_permanent_address_country']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </c:if>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-5">
                                    Please indicate the countries you are paying taxes and the Taxpayer Identification Number (TIN) issued to you by the specific country‘s tax authority. Some countries use NRIC or social number as the TIN.
                                </div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1">

                                    <div class="tax-country-wrapper">
                                        <p><label for="form-tincountry1" id="form-tincountry1-label">Country 1:</label></p>
                                        <div class="form-select_wrap tax-country">
                                            <select name="applicant_tax_country1" id="form-tincountry1" class="form-control <c:out value="${(errors.containsKey('applicant_tax_country1_mandatory')) ? 'error' : ''}"></c:out>" data-field-name="applicant_tax_country1" aria-describedby="applicantTaxCountry1">
                                                <option value="">Please select...</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Afghanistan'}">selected</c:if>>Afghanistan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Albania'}">selected</c:if>>Albania</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Algeria'}">selected</c:if>>Algeria</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'American Samoa'}">selected</c:if>>American Samoa</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Andorra'}">selected</c:if>>Andorra</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Angola'}">selected</c:if>>Angola</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Anguilla'}">selected</c:if>>Anguilla</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Antarctica'}">selected</c:if>>Antarctica</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Antigua and Barbuda'}">selected</c:if>>Antigua and Barbuda</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Argentina'}">selected</c:if>>Argentina</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Armenia'}">selected</c:if>>Armenia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Aruba'}">selected</c:if>>Aruba</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Australia'}">selected</c:if>>Australia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Austria'}">selected</c:if>>Austria</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Azerbaijan'}">selected</c:if>>Azerbaijan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bahama'}">selected</c:if>>Bahama</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bahrain'}">selected</c:if>>Bahrain</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bangladesh'}">selected</c:if>>Bangladesh</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Barbados'}">selected</c:if>>Barbados</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Belarus'}">selected</c:if>>Belarus</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Belgium'}">selected</c:if>>Belgium</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Belize'}">selected</c:if>>Belize</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Benin'}">selected</c:if>>Benin</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bermuda'}">selected</c:if>>Bermuda</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bhutan'}">selected</c:if>>Bhutan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bolivia'}">selected</c:if>>Bolivia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bosnia and Herzegovina'}">selected</c:if>>Bosnia and Herzegovina</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Botswana'}">selected</c:if>>Botswana</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bouvet Island'}">selected</c:if>>Bouvet Island</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Brazil'}">selected</c:if>>Brazil</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'British Indian Ocean Territory'}">selected</c:if>>British Indian Ocean Territory</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'British Virgin Islands'}">selected</c:if>>British Virgin Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Brunei Darussalam'}">selected</c:if>>Brunei Darussalam</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Bulgaria'}">selected</c:if>>Bulgaria</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Burkina Faso'}">selected</c:if>>Burkina Faso</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Burundi'}">selected</c:if>>Burundi</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Cambodia'}">selected</c:if>>Cambodia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Cameroon'}">selected</c:if>>Cameroon</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Canada'}">selected</c:if>>Canada</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Cape Verde'}">selected</c:if>>Cape Verde</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Cayman Islands'}">selected</c:if>>Cayman Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Central African Republic'}">selected</c:if>>Central African Republic</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Chad'}">selected</c:if>>Chad</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Chile'}">selected</c:if>>Chile</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'China'}">selected</c:if>>China</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Christmas Island'}">selected</c:if>>Christmas Island</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Cocos (Keeling) Islands'}">selected</c:if>>Cocos (Keeling) Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Colombia'}">selected</c:if>>Colombia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Comoros'}">selected</c:if>>Comoros</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Congo'}">selected</c:if>>Congo</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Cook Islands'}">selected</c:if>>Cook Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Costa Rica'}">selected</c:if>>Costa Rica</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Croatia'}">selected</c:if>>Croatia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Cuba'}">selected</c:if>>Cuba</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Cyprus'}">selected</c:if>>Cyprus</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Czech Republic'}">selected</c:if>>Czech Republic</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Denmark'}">selected</c:if>>Denmark</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Djibouti'}">selected</c:if>>Djibouti</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Dominica'}">selected</c:if>>Dominica</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Dominican Republic'}">selected</c:if>>Dominican Republic</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'East Timor'}">selected</c:if>>East Timor</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Ecuador'}">selected</c:if>>Ecuador</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Egypt'}">selected</c:if>>Egypt</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'El Salvador'}">selected</c:if>>El Salvador</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Equatorial Guinea'}">selected</c:if>>Equatorial Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Eritrea'}">selected</c:if>>Eritrea</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Estonia'}">selected</c:if>>Estonia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Ethiopia'}">selected</c:if>>Ethiopia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Falkland Islands (Malvinas)'}">selected</c:if>>Falkland Islands (Malvinas)</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Faroe Islands'}">selected</c:if>>Faroe Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Fiji'}">selected</c:if>>Fiji</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Finland'}">selected</c:if>>Finland</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'France'}">selected</c:if>>France</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'France, Metropolitan'}">selected</c:if>>France, Metropolitan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'French Guiana'}">selected</c:if>>French Guiana</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'French Polynesia'}">selected</c:if>>French Polynesia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'French Southern Territories'}">selected</c:if>>French Southern Territories</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Gabon'}">selected</c:if>>Gabon</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Gambia'}">selected</c:if>>Gambia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Georgia'}">selected</c:if>>Georgia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Germany'}">selected</c:if>>Germany</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Ghana'}">selected</c:if>>Ghana</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Gibraltar'}">selected</c:if>>Gibraltar</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Greece'}">selected</c:if>>Greece</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Greenland'}">selected</c:if>>Greenland</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Grenada'}">selected</c:if>>Grenada</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Guadeloupe'}">selected</c:if>>Guadeloupe</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Guam'}">selected</c:if>>Guam</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Guatemala'}">selected</c:if>>Guatemala</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Guinea'}">selected</c:if>>Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Guinea-Bissau'}">selected</c:if>>Guinea-Bissau</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Guyana'}">selected</c:if>>Guyana</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Haiti'}">selected</c:if>>Haiti</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Heard Island and McDonald Islands'}">selected</c:if>>Heard Island and McDonald Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Honduras'}">selected</c:if>>Honduras</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Hong Kong'}">selected</c:if>>Hong Kong</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Hungary'}">selected</c:if>>Hungary</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Iceland'}">selected</c:if>>Iceland</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'India'}">selected</c:if>>India</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Indonesia'}">selected</c:if>>Indonesia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Islamic Republic of Iran'}">selected</c:if>>Islamic Republic of Iran</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Iraq'}">selected</c:if>>Iraq</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Ireland'}">selected</c:if>>Ireland</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Israel'}">selected</c:if>>Israel</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Italy'}">selected</c:if>>Italy</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Jamaica'}">selected</c:if>>Jamaica</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Japan'}">selected</c:if>>Japan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Jordan'}">selected</c:if>>Jordan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Kazakhstan'}">selected</c:if>>Kazakhstan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Kenya'}">selected</c:if>>Kenya</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Kiribati'}">selected</c:if>>Kiribati</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Korea, Democratic People\\\'s Republic of'}">selected</c:if>>Korea, Democratic People's Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Korea, Republic of'}">selected</c:if>>Korea, Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Kuwait'}">selected</c:if>>Kuwait</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Kyrgyzstan'}">selected</c:if>>Kyrgyzstan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Lao People\\\'s Democratic Republic'}">selected</c:if>>Lao People's Democratic Republic</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Latvia'}">selected</c:if>>Latvia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Lebanon'}">selected</c:if>>Lebanon</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Lesotho'}">selected</c:if>>Lesotho</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Liberia'}">selected</c:if>>Liberia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Libyan Arab Jamahiriya'}">selected</c:if>>Libyan Arab Jamahiriya</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Liechtenstein'}">selected</c:if>>Liechtenstein</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Lithuania'}">selected</c:if>>Lithuania</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Luxembourg'}">selected</c:if>>Luxembourg</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Macau'}">selected</c:if>>Macau</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Madagascar'}">selected</c:if>>Madagascar</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Malawi'}">selected</c:if>>Malawi</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Malaysia'}">selected</c:if>>Malaysia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Maldives'}">selected</c:if>>Maldives</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Mali'}">selected</c:if>>Mali</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Malta'}">selected</c:if>>Malta</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Marshall Islands'}">selected</c:if>>Marshall Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Martinique'}">selected</c:if>>Martinique</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Mauritania'}">selected</c:if>>Mauritania</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Mauritius'}">selected</c:if>>Mauritius</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Mayotte'}">selected</c:if>>Mayotte</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Mexico'}">selected</c:if>>Mexico</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Micronesia'}">selected</c:if>>Micronesia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Moldova, Republic of'}">selected</c:if>>Moldova, Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Monaco'}">selected</c:if>>Monaco</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Mongolia'}">selected</c:if>>Mongolia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Monserrat'}">selected</c:if>>Monserrat</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Morocco'}">selected</c:if>>Morocco</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Mozambique'}">selected</c:if>>Mozambique</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Myanmar'}">selected</c:if>>Myanmar</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Nambia'}">selected</c:if>>Nambia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Nauru'}">selected</c:if>>Nauru</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Nepal'}">selected</c:if>>Nepal</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Netherlands'}">selected</c:if>>Netherlands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Netherlands Antilles'}">selected</c:if>>Netherlands Antilles</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'New Caledonia'}">selected</c:if>>New Caledonia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'New Zealand'}">selected</c:if>>New Zealand</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Nicaragua'}">selected</c:if>>Nicaragua</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Niger'}">selected</c:if>>Niger</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Nigeria'}">selected</c:if>>Nigeria</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Niue'}">selected</c:if>>Niue</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Norfolk Island'}">selected</c:if>>Norfolk Island</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Northern Mariana Islands'}">selected</c:if>>Northern Mariana Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Norway'}">selected</c:if>>Norway</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Oman'}">selected</c:if>>Oman</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Pakistan'}">selected</c:if>>Pakistan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Palau'}">selected</c:if>>Palau</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Panama'}">selected</c:if>>Panama</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Papua New Guinea'}">selected</c:if>>Papua New Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Paraguay'}">selected</c:if>>Paraguay</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Peru'}">selected</c:if>>Peru</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Philippines'}">selected</c:if>>Philippines</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Pitcairn'}">selected</c:if>>Pitcairn</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Poland'}">selected</c:if>>Poland</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Portugal'}">selected</c:if>>Portugal</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Puerto Rico'}">selected</c:if>>Puerto Rico</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Qatar'}">selected</c:if>>Qatar</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Romania'}">selected</c:if>>Romania</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Russian Federation'}">selected</c:if>>Russian Federation</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Rwanda'}">selected</c:if>>Rwanda</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'S. Georgia & the S. Sandwich Islands'}">selected</c:if>>S. Georgia & the S. Sandwich Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Saint Lucia'}">selected</c:if>>Saint Lucia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Samoa'}">selected</c:if>>Samoa</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'San Marino'}">selected</c:if>>San Marino</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Sao Tome and Principe'}">selected</c:if>>Sao Tome and Principe</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Saudi Arabia'}">selected</c:if>>Saudi Arabia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Senegal'}">selected</c:if>>Senegal</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Seychelles'}">selected</c:if>>Seychelles</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Sierra Leone'}">selected</c:if>>Sierra Leone</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Slovakia'}">selected</c:if>>Slovakia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Slovenia'}">selected</c:if>>Slovenia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Solomon Islands'}">selected</c:if>>Solomon Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Somalia'}">selected</c:if>>Somalia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'South Africa'}">selected</c:if>>South Africa</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Spain'}">selected</c:if>>Spain</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Sri Lanka'}">selected</c:if>>Sri Lanka</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'St. Helena'}">selected</c:if>>St. Helena</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'St. Kitts and Nevis'}">selected</c:if>>St. Kitts and Nevis</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'St. Pierre and Miquelon'}">selected</c:if>>St. Pierre and Miquelon</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'St. Vincent and the Grenadines'}">selected</c:if>>St. Vincent and the Grenadines</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Sudan'}">selected</c:if>>Sudan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Suriname'}">selected</c:if>>Suriname</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Svalbard and Jan Mayen Islands'}">selected</c:if>>Svalbard and Jan Mayen Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Swaziland'}">selected</c:if>>Swaziland</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Sweden'}">selected</c:if>>Sweden</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Switzerland'}">selected</c:if>>Switzerland</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Syrian Arab Republic'}">selected</c:if>>Syrian Arab Republic</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Taiwan, Province of China'}">selected</c:if>>Taiwan, Province of China</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Tajikistan'}">selected</c:if>>Tajikistan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Thailand'}">selected</c:if>>Thailand</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Togo'}">selected</c:if>>Togo</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Tokelau'}">selected</c:if>>Tokelau</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Tonga'}">selected</c:if>>Tonga</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Trinidad and Tobago'}">selected</c:if>>Trinidad and Tobago</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Tunisia'}">selected</c:if>>Tunisia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Turkey'}">selected</c:if>>Turkey</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Turkmenistan'}">selected</c:if>>Turkmenistan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Turks and Caicos Islands'}">selected</c:if>>Turks and Caicos Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Tuvalu'}">selected</c:if>>Tuvalu</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Uganda'}">selected</c:if>>Uganda</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Ukraine'}">selected</c:if>>Ukraine</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'United Arab Emirates'}">selected</c:if>>United Arab Emirates</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'United Kingdom (Great Britain)'}">selected</c:if>>United Kingdom (Great Britain)</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'United Republic of Tanzania'}">selected</c:if>>United Republic of Tanzania</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'United States of America'}">selected</c:if>>United States of America</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'United States Minor Outlying Islands'}">selected</c:if>>United States Minor Outlying Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'United States Virgin Islands'}">selected</c:if>>United States Virgin Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Uruguay'}">selected</c:if>>Uruguay</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Uzbekistan'}">selected</c:if>>Uzbekistan</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Vanuatu'}">selected</c:if>>Vanuatu</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Vatican City State (Holy See)'}">selected</c:if>>Vatican City State (Holy See)</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Venezuela'}">selected</c:if>>Venezuela</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Viet Nam'}">selected</c:if>>Viet Nam</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Wallis and Futuna Islands'}">selected</c:if>>Wallis and Futuna Islands</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Western Sahara'}">selected</c:if>>Western Sahara</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Yugoslavia'}">selected</c:if>>Yugoslavia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Zaire'}">selected</c:if>>Zaire</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Zambia'}">selected</c:if>>Zambia</option>
                                                <option <c:if test="${data['applicant_tax_country1'] == 'Zimbabwe'}">selected</c:if>>Zimbabwe</option>
                                            </select>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_tax_country1_mandatory')}">

                                            <div>
                                                <span class="error-message" aria-label="Error" id="applicantTaxCountry1">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_tax_country1_mandatory']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                        <div class="tax-tin">
                                            <p><label for="form-tinnumber1">Taxpayer Identification Number (TIN)</label></p>
                                            <input type="text" name="applicant_tax_country1_tin" id="form-tinnumber1" class="form-control <c:out value="${(errors.containsKey('applicant_tax_country1_tin')) ? 'error' : ''}"></c:out>" value="${fn:escapeXml(data['applicant_tax_country1_tin'])}" maxlength="20" aria-describedby="applicantTaxCountry1TIN" data-field-name="applicant_tax_country1_tin">

                                            <c:if test="${errors.containsKey('applicant_tax_country1_tin')}">
                                                <div>
                                                    <span class="error-message" aria-label="Error" id="applicantTaxCountry1TIN">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['applicant_tax_country1_tin']}"></c:out>
                                                    </span>
                                                </div>
                                            </c:if>
                                            <!-- Ticket 611 to hid the TIN is not select ROP -->
                                            <c:if test="${application.getIncludeRefundRider()}">

                                                <fieldset>
                                                    <legend>If a TIN is unavailable, please select the appropriate reason below:</legend>
                                                    <div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country1_tin_option1" type="radio" name="applicant_tax_country1_tin_reason" value="A" <c:if test="${data['applicant_tax_country1_tin_reason'] == 'A'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country1_tin_option1"><span><span></span></span>A: The country where you are liable to pay tax does not issue TINs to its residents.</label>
                                                        </div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country1_tin_option2" type="radio" name="applicant_tax_country1_tin_reason" value="B" <c:if test="${data['applicant_tax_country1_tin_reason'] == 'B'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country1_tin_option2"><span><span></span></span>B: You are otherwise unable to obtain a TIN or equivalent number. (Please provide an explanation for this if you have selected this reason.)</label>
                                                            <div class="tax-tin-reason">
                                                                <p>Explanation:</p>
                                                                <input type="text" name="applicant_tax_country1_tin_reason_custom" value="${fn:escapeXml(data['applicant_tax_country1_tin_reason_custom'])}" class="form-control <c:out value="${(errors.containsKey('applicant_tax_country1_tin_reason_custom')) ? 'error' : ''}"></c:out>" aria-describedby="applicantTaxCountry1TinReasonCustom" data-field-name="applicant_tax_country1_tin_reason_custom">
                                                            <c:if test="${errors.containsKey('applicant_tax_country1_tin_reason_custom')}">
                                                    		<div>
                                                        	<span class="error-message" aria-label="Error" id="applicantTaxCountry1TinReasonCustom">
                                                        	<i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        	<c:out value="${errors['applicant_tax_country1_tin_reason_custom']}"></c:out>
                                                    		</span>
                                                    		</div>
                                               			</c:if>
                                                            </div>
                                                        </div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country1_tin_option3" type="radio" name="applicant_tax_country1_tin_reason" value="C" <c:if test="${data['applicant_tax_country1_tin_reason'] == 'C'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country1_tin_option3"><span><span></span></span>C: No TIN is required. (Only select this reason if the authorities of the country of tax residence do not require the TIN to be disclosed.)</label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="tax-country-wrapper">
                                        <p><label for="form-tincountry2" id="form-tincountry2-label">Country 2:</label></p>
                                        <div class="form-select_wrap tax-country">
                                            <select name="applicant_tax_country2" class="form-control" id="form-tincountry2">
                                                <option value="">Please select...</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Afghanistan'}">selected</c:if>>Afghanistan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Albania'}">selected</c:if>>Albania</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Algeria'}">selected</c:if>>Algeria</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'American Samoa'}">selected</c:if>>American Samoa</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Andorra'}">selected</c:if>>Andorra</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Angola'}">selected</c:if>>Angola</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Anguilla'}">selected</c:if>>Anguilla</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Antarctica'}">selected</c:if>>Antarctica</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Antigua and Barbuda'}">selected</c:if>>Antigua and Barbuda</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Argentina'}">selected</c:if>>Argentina</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Armenia'}">selected</c:if>>Armenia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Aruba'}">selected</c:if>>Aruba</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Australia'}">selected</c:if>>Australia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Austria'}">selected</c:if>>Austria</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Azerbaijan'}">selected</c:if>>Azerbaijan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bahama'}">selected</c:if>>Bahama</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bahrain'}">selected</c:if>>Bahrain</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bangladesh'}">selected</c:if>>Bangladesh</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Barbados'}">selected</c:if>>Barbados</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Belarus'}">selected</c:if>>Belarus</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Belgium'}">selected</c:if>>Belgium</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Belize'}">selected</c:if>>Belize</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Benin'}">selected</c:if>>Benin</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bermuda'}">selected</c:if>>Bermuda</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bhutan'}">selected</c:if>>Bhutan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bolivia'}">selected</c:if>>Bolivia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bosnia and Herzegovina'}">selected</c:if>>Bosnia and Herzegovina</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Botswana'}">selected</c:if>>Botswana</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bouvet Island'}">selected</c:if>>Bouvet Island</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Brazil'}">selected</c:if>>Brazil</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'British Indian Ocean Territory'}">selected</c:if>>British Indian Ocean Territory</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'British Virgin Islands'}">selected</c:if>>British Virgin Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Brunei Darussalam'}">selected</c:if>>Brunei Darussalam</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Bulgaria'}">selected</c:if>>Bulgaria</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Burkina Faso'}">selected</c:if>>Burkina Faso</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Burundi'}">selected</c:if>>Burundi</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Cambodia'}">selected</c:if>>Cambodia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Cameroon'}">selected</c:if>>Cameroon</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Canada'}">selected</c:if>>Canada</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Cape Verde'}">selected</c:if>>Cape Verde</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Cayman Islands'}">selected</c:if>>Cayman Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Central African Republic'}">selected</c:if>>Central African Republic</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Chad'}">selected</c:if>>Chad</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Chile'}">selected</c:if>>Chile</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'China'}">selected</c:if>>China</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Christmas Island'}">selected</c:if>>Christmas Island</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Cocos (Keeling) Islands'}">selected</c:if>>Cocos (Keeling) Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Colombia'}">selected</c:if>>Colombia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Comoros'}">selected</c:if>>Comoros</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Congo'}">selected</c:if>>Congo</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Cook Islands'}">selected</c:if>>Cook Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Costa Rica'}">selected</c:if>>Costa Rica</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Croatia'}">selected</c:if>>Croatia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Cuba'}">selected</c:if>>Cuba</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Cyprus'}">selected</c:if>>Cyprus</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Czech Republic'}">selected</c:if>>Czech Republic</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Denmark'}">selected</c:if>>Denmark</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Djibouti'}">selected</c:if>>Djibouti</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Dominica'}">selected</c:if>>Dominica</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Dominican Republic'}">selected</c:if>>Dominican Republic</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'East Timor'}">selected</c:if>>East Timor</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Ecuador'}">selected</c:if>>Ecuador</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Egypt'}">selected</c:if>>Egypt</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'El Salvador'}">selected</c:if>>El Salvador</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Equatorial Guinea'}">selected</c:if>>Equatorial Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Eritrea'}">selected</c:if>>Eritrea</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Estonia'}">selected</c:if>>Estonia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Ethiopia'}">selected</c:if>>Ethiopia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Falkland Islands (Malvinas)'}">selected</c:if>>Falkland Islands (Malvinas)</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Faroe Islands'}">selected</c:if>>Faroe Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Fiji'}">selected</c:if>>Fiji</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Finland'}">selected</c:if>>Finland</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'France'}">selected</c:if>>France</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'France, Metropolitan'}">selected</c:if>>France, Metropolitan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'French Guiana'}">selected</c:if>>French Guiana</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'French Polynesia'}">selected</c:if>>French Polynesia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'French Southern Territories'}">selected</c:if>>French Southern Territories</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Gabon'}">selected</c:if>>Gabon</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Gambia'}">selected</c:if>>Gambia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Georgia'}">selected</c:if>>Georgia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Germany'}">selected</c:if>>Germany</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Ghana'}">selected</c:if>>Ghana</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Gibraltar'}">selected</c:if>>Gibraltar</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Greece'}">selected</c:if>>Greece</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Greenland'}">selected</c:if>>Greenland</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Grenada'}">selected</c:if>>Grenada</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Guadeloupe'}">selected</c:if>>Guadeloupe</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Guam'}">selected</c:if>>Guam</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Guatemala'}">selected</c:if>>Guatemala</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Guinea'}">selected</c:if>>Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Guinea-Bissau'}">selected</c:if>>Guinea-Bissau</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Guyana'}">selected</c:if>>Guyana</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Haiti'}">selected</c:if>>Haiti</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Heard Island and McDonald Islands'}">selected</c:if>>Heard Island and McDonald Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Honduras'}">selected</c:if>>Honduras</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Hong Kong'}">selected</c:if>>Hong Kong</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Hungary'}">selected</c:if>>Hungary</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Iceland'}">selected</c:if>>Iceland</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'India'}">selected</c:if>>India</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Indonesia'}">selected</c:if>>Indonesia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Islamic Republic of Iran'}">selected</c:if>>Islamic Republic of Iran</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Iraq'}">selected</c:if>>Iraq</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Ireland'}">selected</c:if>>Ireland</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Israel'}">selected</c:if>>Israel</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Italy'}">selected</c:if>>Italy</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Jamaica'}">selected</c:if>>Jamaica</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Japan'}">selected</c:if>>Japan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Jordan'}">selected</c:if>>Jordan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Kazakhstan'}">selected</c:if>>Kazakhstan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Kenya'}">selected</c:if>>Kenya</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Kiribati'}">selected</c:if>>Kiribati</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Korea, Democratic People\\\'s Republic of'}">selected</c:if>>Korea, Democratic People's Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Korea, Republic of'}">selected</c:if>>Korea, Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Kuwait'}">selected</c:if>>Kuwait</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Kyrgyzstan'}">selected</c:if>>Kyrgyzstan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Lao People\\\'s Democratic Republic'}">selected</c:if>>Lao People's Democratic Republic</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Latvia'}">selected</c:if>>Latvia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Lebanon'}">selected</c:if>>Lebanon</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Lesotho'}">selected</c:if>>Lesotho</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Liberia'}">selected</c:if>>Liberia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Libyan Arab Jamahiriya'}">selected</c:if>>Libyan Arab Jamahiriya</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Liechtenstein'}">selected</c:if>>Liechtenstein</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Lithuania'}">selected</c:if>>Lithuania</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Luxembourg'}">selected</c:if>>Luxembourg</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Macau'}">selected</c:if>>Macau</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Madagascar'}">selected</c:if>>Madagascar</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Malawi'}">selected</c:if>>Malawi</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Malaysia'}">selected</c:if>>Malaysia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Maldives'}">selected</c:if>>Maldives</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Mali'}">selected</c:if>>Mali</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Malta'}">selected</c:if>>Malta</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Marshall Islands'}">selected</c:if>>Marshall Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Martinique'}">selected</c:if>>Martinique</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Mauritania'}">selected</c:if>>Mauritania</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Mauritius'}">selected</c:if>>Mauritius</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Mayotte'}">selected</c:if>>Mayotte</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Mexico'}">selected</c:if>>Mexico</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Micronesia'}">selected</c:if>>Micronesia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Moldova, Republic of'}">selected</c:if>>Moldova, Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Monaco'}">selected</c:if>>Monaco</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Mongolia'}">selected</c:if>>Mongolia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Monserrat'}">selected</c:if>>Monserrat</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Morocco'}">selected</c:if>>Morocco</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Mozambique'}">selected</c:if>>Mozambique</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Myanmar'}">selected</c:if>>Myanmar</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Nambia'}">selected</c:if>>Nambia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Nauru'}">selected</c:if>>Nauru</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Nepal'}">selected</c:if>>Nepal</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Netherlands'}">selected</c:if>>Netherlands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Netherlands Antilles'}">selected</c:if>>Netherlands Antilles</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'New Caledonia'}">selected</c:if>>New Caledonia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'New Zealand'}">selected</c:if>>New Zealand</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Nicaragua'}">selected</c:if>>Nicaragua</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Niger'}">selected</c:if>>Niger</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Nigeria'}">selected</c:if>>Nigeria</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Niue'}">selected</c:if>>Niue</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Norfolk Island'}">selected</c:if>>Norfolk Island</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Northern Mariana Islands'}">selected</c:if>>Northern Mariana Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Norway'}">selected</c:if>>Norway</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Oman'}">selected</c:if>>Oman</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Pakistan'}">selected</c:if>>Pakistan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Palau'}">selected</c:if>>Palau</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Panama'}">selected</c:if>>Panama</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Papua New Guinea'}">selected</c:if>>Papua New Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Paraguay'}">selected</c:if>>Paraguay</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Peru'}">selected</c:if>>Peru</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Philippines'}">selected</c:if>>Philippines</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Pitcairn'}">selected</c:if>>Pitcairn</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Poland'}">selected</c:if>>Poland</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Portugal'}">selected</c:if>>Portugal</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Puerto Rico'}">selected</c:if>>Puerto Rico</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Qatar'}">selected</c:if>>Qatar</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Romania'}">selected</c:if>>Romania</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Russian Federation'}">selected</c:if>>Russian Federation</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Rwanda'}">selected</c:if>>Rwanda</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'S. Georgia & the S. Sandwich Islands'}">selected</c:if>>S. Georgia & the S. Sandwich Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Saint Lucia'}">selected</c:if>>Saint Lucia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Samoa'}">selected</c:if>>Samoa</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'San Marino'}">selected</c:if>>San Marino</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Sao Tome and Principe'}">selected</c:if>>Sao Tome and Principe</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Saudi Arabia'}">selected</c:if>>Saudi Arabia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Senegal'}">selected</c:if>>Senegal</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Seychelles'}">selected</c:if>>Seychelles</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Sierra Leone'}">selected</c:if>>Sierra Leone</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Slovakia'}">selected</c:if>>Slovakia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Slovenia'}">selected</c:if>>Slovenia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Solomon Islands'}">selected</c:if>>Solomon Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Somalia'}">selected</c:if>>Somalia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'South Africa'}">selected</c:if>>South Africa</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Spain'}">selected</c:if>>Spain</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Sri Lanka'}">selected</c:if>>Sri Lanka</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'St. Helena'}">selected</c:if>>St. Helena</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'St. Kitts and Nevis'}">selected</c:if>>St. Kitts and Nevis</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'St. Pierre and Miquelon'}">selected</c:if>>St. Pierre and Miquelon</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'St. Vincent and the Grenadines'}">selected</c:if>>St. Vincent and the Grenadines</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Sudan'}">selected</c:if>>Sudan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Suriname'}">selected</c:if>>Suriname</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Svalbard and Jan Mayen Islands'}">selected</c:if>>Svalbard and Jan Mayen Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Swaziland'}">selected</c:if>>Swaziland</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Sweden'}">selected</c:if>>Sweden</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Switzerland'}">selected</c:if>>Switzerland</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Syrian Arab Republic'}">selected</c:if>>Syrian Arab Republic</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Taiwan, Province of China'}">selected</c:if>>Taiwan, Province of China</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Tajikistan'}">selected</c:if>>Tajikistan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Thailand'}">selected</c:if>>Thailand</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Togo'}">selected</c:if>>Togo</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Tokelau'}">selected</c:if>>Tokelau</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Tonga'}">selected</c:if>>Tonga</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Trinidad and Tobago'}">selected</c:if>>Trinidad and Tobago</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Tunisia'}">selected</c:if>>Tunisia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Turkey'}">selected</c:if>>Turkey</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Turkmenistan'}">selected</c:if>>Turkmenistan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Turks and Caicos Islands'}">selected</c:if>>Turks and Caicos Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Tuvalu'}">selected</c:if>>Tuvalu</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Uganda'}">selected</c:if>>Uganda</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Ukraine'}">selected</c:if>>Ukraine</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'United Arab Emirates'}">selected</c:if>>United Arab Emirates</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'United Kingdom (Great Britain)'}">selected</c:if>>United Kingdom (Great Britain)</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'United Republic of Tanzania'}">selected</c:if>>United Republic of Tanzania</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'United States of America'}">selected</c:if>>United States of America</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'United States Minor Outlying Islands'}">selected</c:if>>United States Minor Outlying Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'United States Virgin Islands'}">selected</c:if>>United States Virgin Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Uruguay'}">selected</c:if>>Uruguay</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Uzbekistan'}">selected</c:if>>Uzbekistan</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Vanuatu'}">selected</c:if>>Vanuatu</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Vatican City State (Holy See)'}">selected</c:if>>Vatican City State (Holy See)</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Venezuela'}">selected</c:if>>Venezuela</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Viet Nam'}">selected</c:if>>Viet Nam</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Wallis and Futuna Islands'}">selected</c:if>>Wallis and Futuna Islands</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Western Sahara'}">selected</c:if>>Western Sahara</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Yugoslavia'}">selected</c:if>>Yugoslavia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Zaire'}">selected</c:if>>Zaire</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Zambia'}">selected</c:if>>Zambia</option>
                                                <option <c:if test="${data['applicant_tax_country2'] == 'Zimbabwe'}">selected</c:if>>Zimbabwe</option>
                                            </select>
                                        </div>
                                        <div class="tax-tin">
                                            <p><label for="form-tinnumber2">Taxpayer Identification Number (TIN)</label></p>
                                            <input type="text" name="applicant_tax_country2_tin" id="form-tinnumber2" class="form-control <c:out value="${(errors.containsKey('applicant_tax_country2_tin')) ? 'error' : ''}"></c:out>" value="${fn:escapeXml(data['applicant_tax_country2_tin'])}" maxlength="20" aria-describedby="applicantTaxCountry2Tin" data-field-name="applicant_tax_country2_tin">

                                             <c:if test="${errors.containsKey('applicant_tax_country2_tin')}">
                                                <div>
                                                    <span class="error-message" aria-label="Error" id="applicantTaxCountry2Tin">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['applicant_tax_country2_tin']}"></c:out>
                                                    </span>
                                                </div>
                                            </c:if>
                                            <c:if test="${application.getIncludeRefundRider()}">
                                                <fieldset>
                                                    <legend>If a TIN is unavailable, please select the appropriate reason below:</legend>
                                                    <div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country2_tin_option1" type="radio" name="applicant_tax_country2_tin_reason" value="A" <c:if test="${data['applicant_tax_country2_tin_reason'] == 'A'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country2_tin_option1"><span><span></span></span>A: The country where you are liable to pay tax does not issue TINs to its residents.</label>
                                                        </div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country2_tin_option2" type="radio" name="applicant_tax_country2_tin_reason" value="B" <c:if test="${data['applicant_tax_country2_tin_reason'] == 'B'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country2_tin_option2"><span><span></span></span>B: You are otherwise unable to obtain a TIN or equivalent number. (Please provide an explanation for this if you have selected this reason.)</label>
                                                            <div class="tax-tin-reason">
                                                                <p>Explanation:</p>
                                                                <input type="text" name="applicant_tax_country2_tin_reason_custom" value="${fn:escapeXml(data['applicant_tax_country2_tin_reason_custom'])}" class="form-control <c:out value="${(errors.containsKey('applicant_tax_country2_tin_reason_custom')) ? 'error' : ''}"></c:out>" aria-describedby="applicantTaxCountry2TinReasonCustom" data-field-name="applicant_tax_country2_tin_reason_custom">
                                                            <c:if test="${errors.containsKey('applicant_tax_country2_tin_reason_custom')}">
                                                    		<div>
                                                        	<span class="error-message" aria-label="Error" id="applicantTaxCountry2TinReasonCustom">
                                                        	<i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        	<c:out value="${errors['applicant_tax_country2_tin_reason_custom']}"></c:out>
                                                    		</span>
                                                    		</div>
                                               			</c:if>
                                                            </div>
                                                        </div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country2_tin_option3" type="radio" name="applicant_tax_country2_tin_reason" value="C" <c:if test="${data['applicant_tax_country2_tin_reason'] == 'C'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country2_tin_option3"><span><span></span></span>C: No TIN is required. (Only select this reason if the authorities of the country of tax residence do not require the TIN to be disclosed.)</label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="tax-country-wrapper">
                                        <p><label for="form-tincountry3" id="form-tincountry3-label">Country 3:</label></p>
                                        <div class="form-select_wrap tax-country">
                                            <select name="applicant_tax_country3"  id="form-tincountry3" class="form-control">
                                                <option value="">Please select...</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Afghanistan'}">selected</c:if>>Afghanistan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Albania'}">selected</c:if>>Albania</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Algeria'}">selected</c:if>>Algeria</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'American Samoa'}">selected</c:if>>American Samoa</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Andorra'}">selected</c:if>>Andorra</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Angola'}">selected</c:if>>Angola</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Anguilla'}">selected</c:if>>Anguilla</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Antarctica'}">selected</c:if>>Antarctica</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Antigua and Barbuda'}">selected</c:if>>Antigua and Barbuda</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Argentina'}">selected</c:if>>Argentina</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Armenia'}">selected</c:if>>Armenia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Aruba'}">selected</c:if>>Aruba</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Australia'}">selected</c:if>>Australia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Austria'}">selected</c:if>>Austria</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Azerbaijan'}">selected</c:if>>Azerbaijan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bahama'}">selected</c:if>>Bahama</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bahrain'}">selected</c:if>>Bahrain</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bangladesh'}">selected</c:if>>Bangladesh</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Barbados'}">selected</c:if>>Barbados</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Belarus'}">selected</c:if>>Belarus</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Belgium'}">selected</c:if>>Belgium</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Belize'}">selected</c:if>>Belize</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Benin'}">selected</c:if>>Benin</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bermuda'}">selected</c:if>>Bermuda</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bhutan'}">selected</c:if>>Bhutan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bolivia'}">selected</c:if>>Bolivia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bosnia and Herzegovina'}">selected</c:if>>Bosnia and Herzegovina</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Botswana'}">selected</c:if>>Botswana</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bouvet Island'}">selected</c:if>>Bouvet Island</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Brazil'}">selected</c:if>>Brazil</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'British Indian Ocean Territory'}">selected</c:if>>British Indian Ocean Territory</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'British Virgin Islands'}">selected</c:if>>British Virgin Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Brunei Darussalam'}">selected</c:if>>Brunei Darussalam</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Bulgaria'}">selected</c:if>>Bulgaria</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Burkina Faso'}">selected</c:if>>Burkina Faso</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Burundi'}">selected</c:if>>Burundi</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Cambodia'}">selected</c:if>>Cambodia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Cameroon'}">selected</c:if>>Cameroon</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Canada'}">selected</c:if>>Canada</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Cape Verde'}">selected</c:if>>Cape Verde</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Cayman Islands'}">selected</c:if>>Cayman Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Central African Republic'}">selected</c:if>>Central African Republic</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Chad'}">selected</c:if>>Chad</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Chile'}">selected</c:if>>Chile</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'China'}">selected</c:if>>China</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Christmas Island'}">selected</c:if>>Christmas Island</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Cocos (Keeling) Islands'}">selected</c:if>>Cocos (Keeling) Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Colombia'}">selected</c:if>>Colombia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Comoros'}">selected</c:if>>Comoros</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Congo'}">selected</c:if>>Congo</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Cook Islands'}">selected</c:if>>Cook Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Costa Rica'}">selected</c:if>>Costa Rica</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Croatia'}">selected</c:if>>Croatia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Cuba'}">selected</c:if>>Cuba</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Cyprus'}">selected</c:if>>Cyprus</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Czech Republic'}">selected</c:if>>Czech Republic</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Denmark'}">selected</c:if>>Denmark</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Djibouti'}">selected</c:if>>Djibouti</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Dominica'}">selected</c:if>>Dominica</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Dominican Republic'}">selected</c:if>>Dominican Republic</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'East Timor'}">selected</c:if>>East Timor</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Ecuador'}">selected</c:if>>Ecuador</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Egypt'}">selected</c:if>>Egypt</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'El Salvador'}">selected</c:if>>El Salvador</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Equatorial Guinea'}">selected</c:if>>Equatorial Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Eritrea'}">selected</c:if>>Eritrea</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Estonia'}">selected</c:if>>Estonia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Ethiopia'}">selected</c:if>>Ethiopia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Falkland Islands (Malvinas)'}">selected</c:if>>Falkland Islands (Malvinas)</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Faroe Islands'}">selected</c:if>>Faroe Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Fiji'}">selected</c:if>>Fiji</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Finland'}">selected</c:if>>Finland</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'France'}">selected</c:if>>France</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'France, Metropolitan'}">selected</c:if>>France, Metropolitan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'French Guiana'}">selected</c:if>>French Guiana</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'French Polynesia'}">selected</c:if>>French Polynesia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'French Southern Territories'}">selected</c:if>>French Southern Territories</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Gabon'}">selected</c:if>>Gabon</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Gambia'}">selected</c:if>>Gambia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Georgia'}">selected</c:if>>Georgia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Germany'}">selected</c:if>>Germany</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Ghana'}">selected</c:if>>Ghana</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Gibraltar'}">selected</c:if>>Gibraltar</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Greece'}">selected</c:if>>Greece</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Greenland'}">selected</c:if>>Greenland</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Grenada'}">selected</c:if>>Grenada</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Guadeloupe'}">selected</c:if>>Guadeloupe</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Guam'}">selected</c:if>>Guam</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Guatemala'}">selected</c:if>>Guatemala</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Guinea'}">selected</c:if>>Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Guinea-Bissau'}">selected</c:if>>Guinea-Bissau</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Guyana'}">selected</c:if>>Guyana</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Haiti'}">selected</c:if>>Haiti</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Heard Island and McDonald Islands'}">selected</c:if>>Heard Island and McDonald Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Honduras'}">selected</c:if>>Honduras</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Hong Kong'}">selected</c:if>>Hong Kong</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Hungary'}">selected</c:if>>Hungary</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Iceland'}">selected</c:if>>Iceland</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'India'}">selected</c:if>>India</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Indonesia'}">selected</c:if>>Indonesia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Islamic Republic of Iran'}">selected</c:if>>Islamic Republic of Iran</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Iraq'}">selected</c:if>>Iraq</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Ireland'}">selected</c:if>>Ireland</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Israel'}">selected</c:if>>Israel</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Italy'}">selected</c:if>>Italy</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Jamaica'}">selected</c:if>>Jamaica</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Japan'}">selected</c:if>>Japan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Jordan'}">selected</c:if>>Jordan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Kazakhstan'}">selected</c:if>>Kazakhstan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Kenya'}">selected</c:if>>Kenya</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Kiribati'}">selected</c:if>>Kiribati</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Korea, Democratic People\\\'s Republic of'}">selected</c:if>>Korea, Democratic People's Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Korea, Republic of'}">selected</c:if>>Korea, Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Kuwait'}">selected</c:if>>Kuwait</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Kyrgyzstan'}">selected</c:if>>Kyrgyzstan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Lao People\\\'s Democratic Republic'}">selected</c:if>>Lao People's Democratic Republic</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Latvia'}">selected</c:if>>Latvia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Lebanon'}">selected</c:if>>Lebanon</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Lesotho'}">selected</c:if>>Lesotho</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Liberia'}">selected</c:if>>Liberia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Libyan Arab Jamahiriya'}">selected</c:if>>Libyan Arab Jamahiriya</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Liechtenstein'}">selected</c:if>>Liechtenstein</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Lithuania'}">selected</c:if>>Lithuania</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Luxembourg'}">selected</c:if>>Luxembourg</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Macau'}">selected</c:if>>Macau</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Madagascar'}">selected</c:if>>Madagascar</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Malawi'}">selected</c:if>>Malawi</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Malaysia'}">selected</c:if>>Malaysia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Maldives'}">selected</c:if>>Maldives</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Mali'}">selected</c:if>>Mali</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Malta'}">selected</c:if>>Malta</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Marshall Islands'}">selected</c:if>>Marshall Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Martinique'}">selected</c:if>>Martinique</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Mauritania'}">selected</c:if>>Mauritania</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Mauritius'}">selected</c:if>>Mauritius</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Mayotte'}">selected</c:if>>Mayotte</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Mexico'}">selected</c:if>>Mexico</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Micronesia'}">selected</c:if>>Micronesia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Moldova, Republic of'}">selected</c:if>>Moldova, Republic of</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Monaco'}">selected</c:if>>Monaco</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Mongolia'}">selected</c:if>>Mongolia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Monserrat'}">selected</c:if>>Monserrat</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Morocco'}">selected</c:if>>Morocco</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Mozambique'}">selected</c:if>>Mozambique</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Myanmar'}">selected</c:if>>Myanmar</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Nambia'}">selected</c:if>>Nambia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Nauru'}">selected</c:if>>Nauru</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Nepal'}">selected</c:if>>Nepal</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Netherlands'}">selected</c:if>>Netherlands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Netherlands Antilles'}">selected</c:if>>Netherlands Antilles</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'New Caledonia'}">selected</c:if>>New Caledonia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'New Zealand'}">selected</c:if>>New Zealand</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Nicaragua'}">selected</c:if>>Nicaragua</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Niger'}">selected</c:if>>Niger</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Nigeria'}">selected</c:if>>Nigeria</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Niue'}">selected</c:if>>Niue</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Norfolk Island'}">selected</c:if>>Norfolk Island</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Northern Mariana Islands'}">selected</c:if>>Northern Mariana Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Norway'}">selected</c:if>>Norway</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Oman'}">selected</c:if>>Oman</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Pakistan'}">selected</c:if>>Pakistan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Palau'}">selected</c:if>>Palau</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Panama'}">selected</c:if>>Panama</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Papua New Guinea'}">selected</c:if>>Papua New Guinea</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Paraguay'}">selected</c:if>>Paraguay</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Peru'}">selected</c:if>>Peru</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Philippines'}">selected</c:if>>Philippines</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Pitcairn'}">selected</c:if>>Pitcairn</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Poland'}">selected</c:if>>Poland</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Portugal'}">selected</c:if>>Portugal</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Puerto Rico'}">selected</c:if>>Puerto Rico</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Qatar'}">selected</c:if>>Qatar</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Romania'}">selected</c:if>>Romania</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Russian Federation'}">selected</c:if>>Russian Federation</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Rwanda'}">selected</c:if>>Rwanda</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'S. Georgia & the S. Sandwich Islands'}">selected</c:if>>S. Georgia & the S. Sandwich Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Saint Lucia'}">selected</c:if>>Saint Lucia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Samoa'}">selected</c:if>>Samoa</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'San Marino'}">selected</c:if>>San Marino</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Sao Tome and Principe'}">selected</c:if>>Sao Tome and Principe</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Saudi Arabia'}">selected</c:if>>Saudi Arabia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Senegal'}">selected</c:if>>Senegal</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Seychelles'}">selected</c:if>>Seychelles</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Sierra Leone'}">selected</c:if>>Sierra Leone</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Singapore'}">selected</c:if>>Singapore</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Slovakia'}">selected</c:if>>Slovakia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Slovenia'}">selected</c:if>>Slovenia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Solomon Islands'}">selected</c:if>>Solomon Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Somalia'}">selected</c:if>>Somalia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'South Africa'}">selected</c:if>>South Africa</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Spain'}">selected</c:if>>Spain</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Sri Lanka'}">selected</c:if>>Sri Lanka</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'St. Helena'}">selected</c:if>>St. Helena</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'St. Kitts and Nevis'}">selected</c:if>>St. Kitts and Nevis</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'St. Pierre and Miquelon'}">selected</c:if>>St. Pierre and Miquelon</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'St. Vincent and the Grenadines'}">selected</c:if>>St. Vincent and the Grenadines</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Sudan'}">selected</c:if>>Sudan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Suriname'}">selected</c:if>>Suriname</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Svalbard and Jan Mayen Islands'}">selected</c:if>>Svalbard and Jan Mayen Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Swaziland'}">selected</c:if>>Swaziland</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Sweden'}">selected</c:if>>Sweden</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Switzerland'}">selected</c:if>>Switzerland</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Syrian Arab Republic'}">selected</c:if>>Syrian Arab Republic</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Taiwan, Province of China'}">selected</c:if>>Taiwan, Province of China</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Tajikistan'}">selected</c:if>>Tajikistan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Thailand'}">selected</c:if>>Thailand</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Togo'}">selected</c:if>>Togo</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Tokelau'}">selected</c:if>>Tokelau</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Tonga'}">selected</c:if>>Tonga</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Trinidad and Tobago'}">selected</c:if>>Trinidad and Tobago</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Tunisia'}">selected</c:if>>Tunisia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Turkey'}">selected</c:if>>Turkey</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Turkmenistan'}">selected</c:if>>Turkmenistan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Turks and Caicos Islands'}">selected</c:if>>Turks and Caicos Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Tuvalu'}">selected</c:if>>Tuvalu</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Uganda'}">selected</c:if>>Uganda</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Ukraine'}">selected</c:if>>Ukraine</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'United Arab Emirates'}">selected</c:if>>United Arab Emirates</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'United Kingdom (Great Britain)'}">selected</c:if>>United Kingdom (Great Britain)</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'United Republic of Tanzania'}">selected</c:if>>United Republic of Tanzania</option>                                                
                                                <option <c:if test="${data['applicant_tax_country3'] == 'United States of America'}">selected</c:if>>United States of America</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'United States Minor Outlying Islands'}">selected</c:if>>United States Minor Outlying Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'United States Virgin Islands'}">selected</c:if>>United States Virgin Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Uruguay'}">selected</c:if>>Uruguay</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Uzbekistan'}">selected</c:if>>Uzbekistan</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Vanuatu'}">selected</c:if>>Vanuatu</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Vatican City State (Holy See)'}">selected</c:if>>Vatican City State (Holy See)</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Venezuela'}">selected</c:if>>Venezuela</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Viet Nam'}">selected</c:if>>Viet Nam</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Wallis and Futuna Islands'}">selected</c:if>>Wallis and Futuna Islands</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Western Sahara'}">selected</c:if>>Western Sahara</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Yugoslavia'}">selected</c:if>>Yugoslavia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Zaire'}">selected</c:if>>Zaire</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Zambia'}">selected</c:if>>Zambia</option>
                                                <option <c:if test="${data['applicant_tax_country3'] == 'Zimbabwe'}">selected</c:if>>Zimbabwe</option>
                                            </select>
                                        </div>
                                        <div class="tax-tin">
                                            <p><label for="form-tinnumber3">Taxpayer Identification Number (TIN)</label></p>
                                            <input type="text" id="form-tinnumber3" name="applicant_tax_country3_tin" class="form-control <c:out value="${(errors.containsKey('applicant_tax_country3_tin')) ? 'error' : ''}"></c:out>" value="${fn:escapeXml(data['applicant_tax_country3_tin'])}" maxlength="20" aria-describedby="applicantTaxCountry3Tin" data-field-name="applicant_tax_country3_tin">

                                            <c:if test="${errors.containsKey('applicant_tax_country3_tin')}">
                                                <div>
                                                <span class="error-message" aria-label="Error" id="applicantTaxCountry3Tin">
                                                    <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_tax_country3_tin']}"></c:out>
                                                </span>
                                                </div>
                                            </c:if>
                                            <c:if test="${application.getIncludeRefundRider()}">
                                                <fieldset>
                                                    <legend>If a TIN is unavailable, please select the appropriate reason below:</legend>
                                                    <div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country3_tin_option1" type="radio" name="applicant_tax_country3_tin_reason" value="A" <c:if test="${data['applicant_tax_country3_tin_reason'] == 'A'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country3_tin_option1"><span><span></span></span>A: The country where you are liable to pay tax does not issue TINs to its residents.</label>
                                                        </div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country3_tin_option2" type="radio" name="applicant_tax_country3_tin_reason" value="B" <c:if test="${data['applicant_tax_country3_tin_reason'] == 'B'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country3_tin_option2"><span><span></span></span>B: You are otherwise unable to obtain a TIN or equivalent number. (Please provide an explanation for this if you have selected this reason.)</label>
                                                            <div class="tax-tin-reason">
                                                                <p>Explanation:</p>
                                                                <input type="text" name="applicant_tax_country3_tin_reason_custom" value="${fn:escapeXml(data['applicant_tax_country3_tin_reason_custom'])}" class="form-control <c:out value="${(errors.containsKey('applicant_tax_country3_tin_reason_custom')) ? 'error' : ''}"></c:out>" aria-describedby="applicantTaxCountry3TinReasonCustom" data-field-name="applicant_tax_country3_tin_reason_custom">
                                                           <c:if test="${errors.containsKey('applicant_tax_country3_tin_reason_custom')}">
                                                    		<div>
                                                        	<span class="error-message" aria-label="Error" id="applicantTaxCountry3TinReasonCustom">
                                                        	<i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        	<c:out value="${errors['applicant_tax_country3_tin_reason_custom']}"></c:out>
                                                    		</span>
                                                    		</div>
                                               			</c:if>
                                                            </div>
                                                        </div>
                                                        <div class="newradio--inline">
                                                            <input id="id_applicant_tax_country3_tin_option3" type="radio" name="applicant_tax_country3_tin_reason" value="C" <c:if test="${data['applicant_tax_country3_tin_reason'] == 'C'}">checked</c:if>>
                                                            <label for="id_applicant_tax_country3_tin_option3"><span><span></span></span>C: No TIN is required. (Only select this reason if the authorities of the country of tax residence do not require the TIN to be disclosed.)</label>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" data-question-group="no_tax_country">
                                <c:if test="${application.getIncludeRefundRider()}">
                                    <div class="col-xs-12 col-md-5"><label for="form-notaxcountry">You have not listed the country of residential/mailing address as the country/one of the countries where the applicant has a tax obligation, please provide a reason for this.</label></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_address_no_tax_reason')) ? 'error' : ''}"></c:out>" data-field-name="applicant_address_no_tax_reason">
                                        <input name="applicant_address_no_tax_reason" id="form-notaxcountry" class="form-control" value="${fn:escapeXml(data['applicant_address_no_tax_reason'])}" maxlength="100" aria-describedby="applicantAddressNoTaxReason">

                                        <c:if test="${errors.containsKey('applicant_address_no_tax_reason')}">
                                            <div>
                                                    <span class="error-message" aria-label="Error" id="applicantAddressNoTaxReason">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['applicant_address_no_tax_reason']}"></c:out>
                                                    </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </c:if>
                            </div>
                            <c:if test="${!application.getUnderwritingLifestyleQuestionsOptin().booleanValue() || application.getUnderwritingLifestyleProfile()=='0'}">
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-5">Are you an HSBC customer?</legend>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_hsbc_customer')) ? 'error' : ''}"></c:out>" data-field-name="applicant_hsbc_customer">
                                        <div class="newradio--inline">
                                            <input id="id_applicant_hsbc_customer_option1" type="radio" name="applicant_hsbc_customer" value="Y" <c:if test="${data['applicant_hsbc_customer']}">checked</c:if>>
                                            <label for="id_applicant_hsbc_customer_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_applicant_hsbc_customer_option2" type="radio" name="applicant_hsbc_customer" value="N" <c:if test="${data['applicant_hsbc_customer'] eq false}">checked</c:if> aria-describedby="hsbcCustomer">
                                            <label for="id_applicant_hsbc_customer_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_hsbc_customer')}">
                                            <div>
                                                    <span class="error-message" aria-label="Error" id="hsbcCustomer">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['applicant_hsbc_customer']}"></c:out>
                                                    </span>
                                            </div>
                                        </c:if>

                                    </div>
                                </fieldset>
                            </div>
                            </c:if>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-5">Have you or any person in connection with this policy&nbsp;
                                        <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" 
                                        data-content="This includes policy owner, life insured, beneficial owner(s) and beneficiary(ies)
                                        A PEP is an individual who is or has been entrusted with prominent public positions in Singapore, a foreign country or an international organisation, which includes a current or former senior official in the executive, legislative, administrative, military or judicial branches of a government or a government agency, a member of a ruling royal family, a senior official of a political party, a senior executive of a government-owned or government-funded
                                        corporation, institution or charity and a senior executive of an international organisation."
                                        aria-label="This includes policy owner, life insured, beneficial owner(s) and beneficiary(ies)
                                        A PEP is an individual who is or has been entrusted with prominent public positions in Singapore, a foreign country or an international organisation, which includes a current or former senior official in the executive, legislative, administrative, military or judicial branches of a government or a government agency, a member of a ruling royal family, a senior official of a political party, a senior executive of a government-owned or government-funded
                                        corporation, institution or charity and a senior executive of an international organisation."
                                        ><i class="icon icon-circle-help-solid"></i></button>
                                         ever been a Politically Exposed Person (PEP), a family member of a PEP or close associate of a PEP?</legend>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_pep')) ? 'error' : ''}"></c:out>" data-field-name="applicant_pep">
                                        <div class="newradio--inline">
                                            <input id="id_applicant_pep_option1" type="radio" name="applicant_pep" value="Y" <c:if test="${data['applicant_pep']}">checked</c:if>>
                                            <label for="id_applicant_pep_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_applicant_pep_option2" type="radio" name="applicant_pep" value="N" <c:if test="${data['applicant_pep'] eq false}">checked</c:if> aria-describedby="applicantPep">
                                            <label for="id_applicant_pep_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_pep')}">
                                            <div>
                                                    <span class="error-message" aria-label="Error" id="applicantPep">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['applicant_pep']}"></c:out>
                                                    </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-5">Are you the Beneficial Owner of the Policy? 
                                        <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help"                
                                        aria-label="You are the Beneficial Owner if you are the individual who either controls the applicant or on whose behalf the policy was purchased. However it does not constitute nominating a Beneficiary who receives the policy proceeds."
                                        data-content="You are the Beneficial Owner if you are the individual who either controls the applicant or on whose behalf the policy was purchased. However it does not constitute nominating a Beneficiary who receives the policy proceeds."><i class="icon icon-circle-help-solid"></i></button>
                                    </legend>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_bo')) ? 'error' : ''}"></c:out>" data-field-name="applicant_bo">
                                        <div class="newradio--inline">
                                            <input id="id_applicant_bo_option1" type="radio" name="applicant_bo" value="Y" <c:if test="${data['applicant_bo']}">checked</c:if>>
                                            <label for="id_applicant_bo_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_applicant_bo_option2" type="radio" name="applicant_bo" value="N" <c:if test="${data['applicant_bo'] eq false}">checked</c:if> aria-describedby="beneficialOwner">
                                            <label for="id_applicant_bo_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_bo')}">
                                            <div>
                                                    <span class="error-message" aria-label="Error" id="beneficialOwner">
                                                        <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['applicant_bo']}"></c:out>
                                                    </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="row" data-question-group="bo_details">
                                <div class="col-xs-12 col-md-5 control_singapore"><label for="form-boname">Beneficial Owner's Given Name <small>(as appears on NRIC/Passport)</small></label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('bo_first_name')) ? 'error' : ''}"></c:out>" data-field-name="bo_first_name">
                                    <input name="bo_first_name" class="form-control" id="form-boname" type="text" value="${fn:escapeXml(data['bo_first_name'])}" maxlength="50" aria-describedby="boFirstName">

                                    <c:if test="${errors.containsKey('bo_first_name')}">
                                        <span class="error-message" aria-label="Error" id="boFirstName">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['bo_first_name']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row" data-question-group="bo_details">
                                <div class="col-xs-12 col-md-5 control_singapore"><label for="form-bonsurame">Beneficial Owner's Surname <small>(as appears on NRIC/Passport)</small></label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('bo_last_name')) ? 'error' : ''}"></c:out>" data-field-name="bo_last_name">
                                    <input name="bo_last_name" class="form-control" id="form-bonsurame" type="text" value="${fn:escapeXml(data['bo_last_name'])}" maxlength="50" aria-describedby="boLastName">

                                    <c:if test="${errors.containsKey('bo_last_name')}">
                                        <span class="error-message" aria-label="Error" id="boLastName">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['bo_last_name']}"></c:out>
                                        </span>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row" data-question-group="bo_details">
                                <div class="col-xs-12 col-md-5"><label for="form-bonric">Beneficial Owner's NRIC/Passport number?</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('bo_nric')) ? 'error' : ''}"></c:out>" data-field-name="bo_nric">
                                    <input name="bo_nric" class="form-control" id="form-bonric" type="text" value="${fn:escapeXml(data['bo_nric'])}" maxlength="50" aria-describedby="boNRIC">

                                    <c:if test="${errors.containsKey('bo_nric')}">
                                            <span class="error-message" aria-label="Error" id="boNRIC">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['bo_nric']}"></c:out>
                                            </span>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row" data-question-group="bo_details">
                                <div class="col-xs-12 col-md-5"><label for="form-borelationship">Relationship to Beneficial Owner</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('bo_relation')) ? 'error' : ''}"></c:out>" data-field-name="bo_relation">
                                    <input name="bo_relation" class="form-control" id="form-borelationship" type="text" value="${fn:escapeXml(data['bo_relation'])}" maxlength="50" aria-describedby="boRelation">

                                    <c:if test="${errors.containsKey('bo_relation')}">
                                            <span class="error-message" aria-label="Error" id="boRelation">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['bo_relation']}"></c:out>
                                            </span>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <c:if test="${application.getIncludeRefundRider()}">
                            <p>By clicking 'Continue' below, I acknowledge that the information contained in this form and information regarding me and any Reportable Account(s) may be provided to the tax authorities of the country in which this account(s) is / are maintained and exchanged with tax authorities of another country or countries in which I may be tax resident pursuant to intergovernmental agreements to exchange financial account information.</p>
                            <p>I undertake to advise HSBC within 30 days of any change in circumstances which affects the tax residency status of me identified in this form or causes the information contained herein to become incorrect, and to provide HSBC with a suitably updated Self-Certification Form within 90 days of such change in circumstances.</p>
                            <p>&nbsp;</p>
                            </c:if>
                            <c:choose>
                                <c:when test="${application.applicantIsWorking()||application.applicantIsStudying()}">
                                    <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/10">Back</button>
                                </c:when>
                                <c:when test="${not application.insuredIsSelf()}">
                                    <c:choose>
                                        <c:when test="${application.insuredIsWorking()||application.insuredIsStudying()}">
                                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/10">Back</button>
                                        </c:when>
                                        <c:otherwise>
                                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/9">Back</button>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/9">Back</button>
                                </c:otherwise>
                            </c:choose>
                            <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                        </div>
                        
                    </form>
                </div>
            </div>

        </section>
    </article>
</div>

<style>
    .tax-country-wrapper {
        margin-top: 20px;
    }
    .tax-country-wrapper:first-child {
        margin-top: 0;
    }
    .tax-tin {
        display: none;
    }
    .tax-tin-reason {
        margin: 10px 0;
        display: none;
    }

    <c:if test="${application.applicantIsPassHolder()}">
    [data-question-group=""] {
        display: block;
    }
    </c:if>
</style>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
