<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-acknowledgment"></jsp:param>
</jsp:include>

<div class="main onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>


                    <form class="buynow-form" method="post" data-form-name="onlineprotector-step">
                        <div class="col-xs-12 col-sm-10">
                            <h2>Acknowledgement</h2>

                            <c:if test="${application.insuredIsSelf()}">
                                <c:choose>
                                    <c:when test="${application.getIncludeCriticalIllnessRider()}">
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack1_ci.jsp"/>
                                    </c:when>
                                    <c:otherwise>
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack1_no_ci.jsp"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <c:if test="${not application.insuredIsSelf()}">
                                <c:choose>
                                    <c:when test="${application.getIncludeCriticalIllnessRider()}">
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack1_spouse_ci.jsp"/>
                                    </c:when>
                                    <c:otherwise>
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack1_spouse_no_ci.jsp"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="space40"></div>
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/11">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                        </div>

                    </form>
                    
                </div>
            </div>
            <jsp:include page="/WEB-INF/ourplans/includes/marketing-modal.jsp"/>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
