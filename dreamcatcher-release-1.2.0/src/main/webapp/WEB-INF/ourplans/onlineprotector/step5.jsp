<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-uwbasic"></jsp:param>
</jsp:include>

<div class="main onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12 col-sm-9 col-md-8">
                        <form class="buynow-form" method="post" data-form-name="onlineprotector-step">
                            <div class="row">
                                <div class="col-xs-12 col-md-5"><label for="form-employementstatus" id="form-employementstatus-label"><c:out value="${application.getApplicantFirstName()}"></c:out>, what is your current employment status?</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_employment')) ? 'error' : ''}"></c:out>" data-field-name="applicant_mobile_num">
                                    <div class="form-select_wrap">
                                        <select name="applicant_employment" id="form-employementstatus" class="form-control" aria-describedby="applicantEmploymentStatus">
                                            <option value="">Please select...</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Salaried'}">selected</c:if>>Salaried</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Self-employed'}">selected</c:if>>Self-employed</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Homemaker'}">selected</c:if>>Homemaker</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Retired'}">selected</c:if>>Retired</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Unemployed'}">selected</c:if>>Unemployed</option>
                                            <option <c:if test="${data['applicant_employment'] == 'Student'}">selected</c:if>>Student</option>
                                        </select>
                                    </div>
                                    <c:if test="${errors.containsKey('applicant_employment')}">
                                        <span class="error-message" aria-label="Error" id="applicantEmploymentStatus">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['applicant_employment']}"></c:out>
                                        </span>
                                    </c:if>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-5"><label for="form-income">What is your current annual income?</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_income')) ? 'error' : ''}"></c:out>" data-field-name="applicant_mobile_num">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <fmt:formatNumber var="fmtApplicantIncome" value="${fn:escapeXml(data['applicant_income'])}" type="number" maxFractionDigits="0"/>
                                        <input name="applicant_income" id="form-income" class="form-control number-only-input" value="${fmtApplicantIncome}" type="text" aria-describedby="applicantIncome">

                                    </div>
                                    <c:if test="${errors.containsKey('applicant_income')}">
                                            <span class="error-message" id="applicantIncome" aria-label="Error">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['applicant_income']}"></c:out>
                                            </span>
                                    </c:if><div class="help-block" style="display: none;"><small>Please enter a number</small></div>

                                </div>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-5">Have you smoked in the last 12 months?</legend>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_smoker')) ? 'error' : ''}"></c:out>" data-field-name="applicant_smoker">
                                        <div class="newradio--inline">
                                            <input id="id_applicant_smoker_option1" type="radio" name="applicant_smoker" value="Y" <c:if test="${data['applicant_smoker']}">checked</c:if>>
                                            <label for="id_applicant_smoker_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_applicant_smoker_option2" type="radio" name="applicant_smoker" value="N" <c:if test="${data['applicant_smoker'] eq false}">checked</c:if> aria-describedby="applicantSmoker">
                                            <label for="id_applicant_smoker_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('applicant_smoker')}">
                                            <div>
                                                    <span class="error-message" aria-label="Error" id="applicantSmoker">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                        <c:out value="${errors['applicant_smoker']}"></c:out>
                                                    </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="row">
                                <c:if test="${not application.insuredIsSelf()}">
                                    <div class="col-xs-12 col-md-5"><label for="form-employementstatusspouse" id="form-employementstatusspouse-label">What is <c:out value="${application.getInsuredPossessiveLabel(true)}"></c:out> current employment status?</label></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_employment')) ? 'error' : ''}"></c:out>" data-field-name="applicant_smoker">
                                        <div class="form-select_wrap">
                                            <select name="insured_employment" id="form-employementstatusspouse" class="form-control" aria-describedby="insuredEmploymentStatus">
                                                <option value="">Please select...</option>
                                                <option <c:if test="${data['insured_employment'] == 'Salaried'}">selected</c:if>>Salaried</option>
                                                <option <c:if test="${data['insured_employment'] == 'Self-employed'}">selected</c:if>>Self-employed</option>
                                                <option <c:if test="${data['insured_employment'] == 'Homemaker'}">selected</c:if>>Homemaker</option>
                                                <option <c:if test="${data['insured_employment'] == 'Retired'}">selected</c:if>>Retired</option>
                                                <option <c:if test="${data['insured_employment'] == 'Unemployed'}">selected</c:if>>Unemployed</option>
                                                <option <c:if test="${data['insured_employment'] == 'Student'}">selected</c:if>>Student</option>
                                            </select>
                                        </div>
                                        <c:if test="${errors.containsKey('insured_employment')}">
                                            <span class="error-message" aria-label="Error" id="insuredEmploymentStatus">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                <c:out value="${errors['insured_employment']}"></c:out>
                                            </span>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-5"><label for="form-incomespouse">What is <c:out value="${application.getInsuredPossessiveLabel(true)}"></c:out> current annual income?</label></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_income')) ? 'error' : ''}"></c:out>" data-field-name="applicant_smoker">
                                        <div class="input-group currency-text-box">
                                            <span class="input-group-addon">S$</span>
                                            <fmt:formatNumber var="fmtApplicantIncome" value="${fn:escapeXml(data['insured_income'])}" type="number" maxFractionDigits="0"/>
                                            <input name="insured_income" id="form-incomespouse" class="form-control number-only-input" value="${fmtApplicantIncome}" type="text" aria-describedby="insuredIncome">

                                        </div>
                                        <c:if test="${errors.containsKey('insured_income')}">
                                                <span class="error-message" id="insuredIncome" aria-label="Error">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_income']}"></c:out>
                                                </span>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="row">
                                    <fieldset>
                                        <legend class="col-xs-12 col-md-5">Has <c:out value="${application.getInsuredFirstName()}"></c:out> smoked in the last 12 months?</legend>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_smoker')) ? 'error' : ''}"></c:out>" data-field-name="applicant_smoker">
                                            <div class="newradio--inline">
                                                <input id="id_insured_smoker_option1" type="radio" name="insured_smoker" value="Y" <c:if test="${data['insured_smoker']}">checked</c:if>>
                                                <label for="id_insured_smoker_option1"><span><span></span></span>Yes</label>
                                            </div>
                                            <div class="newradio--inline">
                                                <input id="id_insured_smoker_option2" type="radio" name="insured_smoker" value="N" <c:if test="${data['insured_smoker'] eq false}">checked</c:if> aria-describedby="insuredSmoker">
                                                <label for="id_insured_smoker_option2"><span><span></span></span>No</label>
                                            </div>
                                            <c:if test="${errors.containsKey('insured_smoker')}">
                                                <div>
                                                        <span class="error-message" aria-label="Error" id="insuredSmoker">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['insured_smoker']}"></c:out>
                                                        </span>
                                                </div>
                                            </c:if>
                                        </div>
                                    </fieldset>
                                </c:if>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-5">
                                        <c:if test="${application.insuredIsSelf()}">
                                            Have you ever been diagnosed with cancer, heart disease or stroke?
                                        </c:if>
                                        <c:if test="${not application.insuredIsSelf()}">
                                            Has <c:out value="${application.getInsuredFirstName()}"></c:out> ever been diagnosed with cancer, heart disease or stroke?
                                        </c:if>
                                    </legend>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_medical1')) ? 'error' : ''}"></c:out>" data-field-name="insured_medical1">
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical1_option1" type="radio" name="insured_medical1" value="Y" <c:if test="${data['insured_medical1']}">checked</c:if>>
                                            <label for="id_insured_medical1_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical1_option2" type="radio" name="insured_medical1" value="N" <c:if test="${data['insured_medical1'] eq false}">checked</c:if> aria-describedby="insuredMedical1">
                                            <label for="id_insured_medical1_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('insured_medical1')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="insuredMedical1">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_medical1']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>

                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-5">
                                        <c:if test="${application.insuredIsSelf()}">
                                            In the last 5 years, have you been diagnosed with or suffered from diabetes, HIV/AIDS or any medical condition affecting your brain, blood, heart, lungs, liver, kidneys for which you were required to undergo medical treatment for more than 14 days?
                                        </c:if>
                                        <c:if test="${not application.insuredIsSelf()}">
                                            In the last 5 years, has <c:out value="${application.getInsuredFirstName()}"></c:out> been diagnosed with or suffered from diabetes, HIV/AIDS or any medical condition affecting the brain, blood, heart, lungs, liver, kidneys for which <c:out value="${application.getInsuredPronounLabel()}"></c:out> was required to undergo medical treatment for more than 14 days?
                                        </c:if>
                                    </legend>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_medical2')) ? 'error' : ''}"></c:out>" data-field-name="insured_medical2">
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical2_option1" type="radio" name="insured_medical2" value="Y" <c:if test="${data['insured_medical2']}">checked</c:if>>
                                            <label for="id_insured_medical2_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical2_option2" type="radio" name="insured_medical2" value="N" <c:if test="${data['insured_medical2'] eq false}">checked</c:if>  aria-describedby="insuredMedical2">
                                            <label for="id_insured_medical2_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('insured_medical2')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="insuredMedical2">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_medical2']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>

                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend class="col-xs-12 col-md-5">
                                        <c:if test="${application.insuredIsSelf()}">
                                            In the last 12 months, have you had any symptom such as unexplained bleeding, weight loss, lump or growth, blood in the stool, chest pain or weakness of limbs for which you are still under investigation or have not yet sought medical advice?
                                        </c:if>
                                        <c:if test="${not application.insuredIsSelf()}">
                                            In the last 12 months, has <c:out value="${application.getInsuredFirstName()}"></c:out> had any symptom such as unexplained bleeding, weight loss, lump or growth, blood in the stool, chest pain or weakness of limbs for which <c:out value="${application.getInsuredPronounLabel()}"></c:out> is still under investigation or have not yet sought medical advice?
                                        </c:if>
                                    </legend>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_medical3')) ? 'error' : ''}"></c:out>" data-field-name="insured_medical3">
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical3_option1" type="radio" name="insured_medical3" value="Y" <c:if test="${data['insured_medical3']}">checked</c:if>>
                                            <label for="id_insured_medical3_option1"><span><span></span></span>Yes</label>
                                        </div>
                                        <div class="newradio--inline">
                                            <input id="id_insured_medical3_option2" type="radio" name="insured_medical3" value="N" <c:if test="${data['insured_medical3'] eq false}">checked</c:if> aria-describedby="insuredMedical3">
                                            <label for="id_insured_medical3_option2"><span><span></span></span>No</label>
                                        </div>
                                        <c:if test="${errors.containsKey('insured_medical3')}">
                                            <div>
                                                <span class="error-message" aria-label="Error" id="insuredMedical3">
                                                <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_medical3']}"></c:out>
                                                </span>
                                            </div>
                                        </c:if>
                                    </div>
                                </fieldset>
                            </div>
                            <div>
                                <c:if test="${application.insuredIsSelf()}">
                                    <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/4">Back</button>
                                </c:if>
                                <c:if test="${not application.insuredIsSelf()}">
                                    <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/4a">Back</button>
                                </c:if>
                                <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
