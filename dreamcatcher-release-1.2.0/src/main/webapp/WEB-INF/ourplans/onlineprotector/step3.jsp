<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-applicantinfo"></jsp:param>
</jsp:include>

<c:set var="data" scope="request" value="${data}"></c:set>
<c:set var="errors" scope="request" value="${errors}"></c:set>

<div class="main step3 onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12">
                        <form class="buynow-form" method="post" data-form-name="onlineprotector-step" data-form-dropout="<c:out value="${(errors.containsKey('dropout')) ? '1' : ''}"></c:out>">

                            <div class="row">
                                <div class="col-xs-12 col-sm-10">
                                    <h2>Tell us more about yourself before proceeding with your application</h2>
                                    <p>The products and its related features offered on this online platform are available only to customers who are currently residing&nbsp;<button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="Residing in Singapore means anyone who is physically present in Singapore for 183 days or more during the year." aria-label="Residing in Singapore means anyone who is physically present in Singapore for 183 days or more during the year.">
                                            <i class="icon icon-circle-help-solid"></i>
                                        </button>
                                        in Singapore.
                                    </p>
                                </div>
                            </div>
                            <div class="space20"></div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-9 col-md-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5"><label for="form-resiedencystatus" id="form-residencystatus-label">What is your current status of residency in Singapore?</label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_residency')) ? 'error' : ''}"></c:out>" data-field-name="applicant_residency">
                                            <div class="form-select_wrap">
                                                <select name="applicant_residency" id="form-resiedencystatus" class="form-control" aria-describedby="residencyStatus">
                                                    <option value="">Please select...</option>
                                                    <option <c:if test="${data['applicant_residency'] == 'Singaporean'}">selected</c:if>>Singaporean</option>
                                                    <option <c:if test="${data['applicant_residency'] == 'Singapore PR'}">selected</c:if>>Singapore PR</option>
                                                    <option <c:if test="${data['applicant_residency'] == 'Employment Pass'}">selected</c:if>>Employment Pass</option>
                                                    <option <c:if test="${data['applicant_residency'] == 'Skilled Pass'}">selected</c:if>>Skilled Pass</option>
                                                    <option <c:if test="${data['applicant_residency'] == 'Personalised Employment Pass'}">selected</c:if>>Personalised Employment Pass</option>
                                                    <option <c:if test="${data['applicant_residency'] == 'Dependent Pass'}">selected</c:if>>Dependent Pass</option>
                                                    <option <c:if test="${data['applicant_residency'] == 'Student Pass'}">selected</c:if>>Student Pass</option>
                                                    <option value="NA" <c:if test="${data['applicant_residency'] == 'NA'}">selected</c:if>>None of the above</option>
                                                </select>
                                            </div>
                                            <c:if test="${errors.containsKey('applicant_residency')}">
                                                <span class="error-message" aria-label="Error" id="residencyStatus">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_residency']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5 control_singapore"><label for="form-firstname">Given Name <small>(as appears on <span data-dynamic-label="applicant-nric-passport-label">NRIC/Passport</span>)</small></label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_first_name')) ? 'error' : ''}"></c:out>" data-field-name="applicant_first_name">
                                            <input name="applicant_first_name" id="form-firstname" class="form-control" type="text" value="${fn:escapeXml(data['applicant_first_name'])}" maxlength="50" aria-describedby="applicantFirstName">

                                            <c:if test="${errors.containsKey('applicant_first_name')}">
                                                <span class="error-message" id="applicantFirstName" aria-label="Error">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_first_name']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-5 control_singapore"><label for="form-surname">Surname <small>(as appears on <span data-dynamic-label="applicant-nric-passport-label">NRIC/Passport</span>)</small></label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('applicant_last_name')) ? 'error' : ''}"></c:out>" data-field-name="applicant_last_name">
                                            <input name="applicant_last_name" id="form-surname" class="form-control" type="text" value="${fn:escapeXml(data['applicant_last_name'])}" maxlength="50" aria-describedby="applicantLastName">

                                            <c:if test="${errors.containsKey('applicant_last_name')}">
                                                <span class="error-message" id="applicantLastName" aria-label="Error">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['applicant_last_name']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                    
                                    <jsp:include page="/WEB-INF/ourplans/includes/applicant_dob.jsp" flush="true">
                                        <jsp:param name="formpage" value="true" />
                                    </jsp:include>
                                    
                                    <div class="row">
                                        <fieldset>
                                            <legend class="col-xs-12 col-md-5">Who are you buying the policy for?</legend>
                                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('life_insured')) ? 'error' : ''}"></c:out>" data-field-name="insured_residency">
                                                <div class="newradio--inline">
                                                    <input id="id_insured_option1" type="radio" name="life_insured" value="self" <c:if test="${data['life_insured'] == 'self'}">checked</c:if>>
                                                    <label for="id_insured_option1"><span><span></span></span>Myself</label>
                                                </div>
                                                <div class="newradio--inline">
                                                    <input id="id_insured_option2" type="radio" name="life_insured" value="spouse" <c:if test="${data['life_insured'] == 'spouse'}">checked</c:if> aria-describedby="lifeInsured">
                                                    <label for="id_insured_option2"><span><span></span></span>My spouse</label>
                                                </div>
                                                <c:if test="${errors.containsKey('life_insured')}">
                                                    <div>
                                                        <span class="error-message" aria-label="Error" id="lifeInsured">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                            <c:out value="${errors['life_insured']}"></c:out>
                                                        </span>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="row" data-question-group="spouse">
                                        <div class="col-xs-12 col-md-5"><label for="form-resiedencystatusspouse" id="form-resiedencystatusspouse-label">What is your spouse's current status of residency in Singapore?</label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_residency')) ? 'error' : ''}"></c:out>" data-field-name="insured_residency">
                                            <div class="form-select_wrap">
                                                <select name="insured_residency" id="form-resiedencystatusspouse" class="form-control" aria-describedby="insuredResidencyStatus">
                                                    <option value="">Please select...</option>
                                                    <option <c:if test="${data['insured_residency'] == 'Singaporean'}">selected</c:if>>Singaporean</option>
                                                    <option <c:if test="${data['insured_residency'] == 'Singapore PR'}">selected</c:if>>Singapore PR</option>
                                                    <option <c:if test="${data['insured_residency'] == 'Employment Pass'}">selected</c:if>>Employment Pass</option>
                                                    <option <c:if test="${data['insured_residency'] == 'Skilled Pass'}">selected</c:if>>Skilled Pass</option>
                                                    <option <c:if test="${data['insured_residency'] == 'Personalised Employment Pass'}">selected</c:if>>Personalised Employment Pass</option>
                                                    <option <c:if test="${data['insured_residency'] == 'Dependent Pass'}">selected</c:if>>Dependent Pass</option>
                                                    <option <c:if test="${data['insured_residency'] == 'Student Pass'}">selected</c:if>>Student Pass</option>
                                                    <option value="NA" <c:if test="${data['insured_residency'] == 'NA'}">selected</c:if>>None of the above</option>
                                                </select>
                                            </div>
                                            <c:if test="${errors.containsKey('insured_residency')}">
                                                <span class="error-message" aria-label="Error" id="insuredResidencyStatus">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_residency']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="row" data-question-group="spouse">
                                        <div class="col-xs-12 col-md-5 control_singapore"><label for="form-firstnamespouse">Your spouse's given name <small>(as appears on <span data-dynamic-label="insured-nric-passport-label">NRIC/Passport</span>)</small></label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_first_name')) ? 'error' : ''}"></c:out>" data-field-name="insured_first_name">
                                            <input name="insured_first_name" id="form-firstnamespouse"  class="form-control" type="text" value="${fn:escapeXml(data['insured_first_name'])}" maxlength="50" aria-describedby="insuredFirstName">

                                            <c:if test="${errors.containsKey('insured_first_name')}">
                                                <span class="error-message" id="insuredFirstName" aria-label="Error">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_first_name']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="row" data-question-group="spouse">
                                        <div class="col-xs-12 col-md-5 control_singapore"><label for="form-lastnamespouse">Your spouse's surname <small>(as appears on <span data-dynamic-label="insured-nric-passport-label">NRIC/Passport</span>)</small></label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_last_name')) ? 'error' : ''}"></c:out>" data-field-name="insured_last_name">
                                            <input name="insured_last_name" id="form-lastnamespouse"  class="form-control" type="text" value="${fn:escapeXml(data['insured_last_name'])}" maxlength="50" aria-describedby="insuredLastName">

                                            <c:if test="${errors.containsKey('insured_last_name')}">
                                                <span class="error-message" id="insuredLastName" aria-label="Error">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_last_name']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="row" data-question-group="spouse">
                                        <div class="col-xs-12 col-md-5"><label for="date">Your spouse's date of birth</label></div>
                                        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group <c:out value="${(errors.containsKey('insured_dob')) ? 'error' : ''}"></c:out>" data-field-name="insured_dob_day">
                                            <input id="date" type="text" name="insured_dob" placeholder="dd/mm/yyyy" maxlength="10" 
                                           class="date-validate auto-width form-control" value="${fn:escapeXml(data['insured_dob'])}" aria-describedby="spouseDob">
                                            <c:if test="${errors.containsKey('insured_dob')}">
                                                <span class="error-message" aria-label="Error" id="spouseDob">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                                    <c:out value="${errors['insured_dob']}"></c:out>
                                                </span>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-9">
                                    <button type="button" class="btn btn-red" data-form-action="next">Continue</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
