<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-veryimportantnotes"></jsp:param>
</jsp:include>

<div class="main step2 onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>

    <section class="container-fluid" style="padding:0;">
        <div class="header-page header-onlineprotector" style="margin-bottom:0;">
            <div class="container">
            </div>
        </div>
    </section>

    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>
                    <div class="col-xs-12 col-sm-10">
                        <h2>Very important notes</h2>
                        <p>Due to US insurance regulatory requirements, you are not to enter the US or any territory subject to US jurisdiction during the application process, otherwise the request effected hereunder may be made void.</p>
                        <p>This is a self-directed online platform to purchase insurance products. HSBC Insurance does not provide any advice to clients transacting on this platform.</p>
                        <p>You agree that the information provided in this online application form, such other forms, documents and questionnaires provided by you  in relation to this proposal will form the basis of the contract of insurance between you and HSBC Insurance.</p>
                        <p>Please note, we are required under the Insurance Act to inform you that you must fully and faithfully disclose all the facts which you know or ought to know (including any existing medical conditions). Otherwise, in accordance with Section 25(5) of the Insurance Act (Cap. 142), your policy may be cancelled and you may receive no benefits.</p>
                        <div style="margin-top:50px;">
                            <form class="buynow-form" method="post">
                                <a class="btn btn-red" href="#" data-form-action="accept" role="button">I agree</a>
                                <a type="button" class="btn btn-white" data-form-action="reject" role="button">I disagree</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
