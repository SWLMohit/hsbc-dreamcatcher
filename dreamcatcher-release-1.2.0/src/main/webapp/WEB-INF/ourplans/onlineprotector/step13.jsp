<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineprotector-acknowledgmentplayback"></jsp:param>
</jsp:include>

<div class="main onlineprotector-step">
    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineprotector">
            <div class="container">
                <div class="row">
                    <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/header.jsp" flush="true"/>


                    <form class="buynow-form" method="post" data-form-name="onlineprotector-step">
                        <div class="col-xs-12 col-sm-10">
                            <h2>Declaration</h2>

                            <c:if test="${application.insuredIsSelf()}">
                                <c:choose>
                                    <c:when test="${application.getIncludeCriticalIllnessRider() and application.getIncludeRefundRider()}">
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack2_plan_ci_pr.jsp"/>
                                    </c:when>
                                    <c:when test="${application.getIncludeCriticalIllnessRider()}">
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack2_plan_ci.jsp"/>
                                    </c:when>
                                    <c:when test="${application.getIncludeRefundRider()}">
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack2_plan_pr.jsp"/>
                                    </c:when>
                                    <c:otherwise>
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack2_plan.jsp"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <c:if test="${not application.insuredIsSelf()}">
                                <c:choose>
                                    <c:when test="${application.getIncludeCriticalIllnessRider() and application.getIncludeRefundRider()}">
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack2_spouse_plan_ci_pr.jsp"/>
                                    </c:when>
                                    <c:when test="${application.getIncludeCriticalIllnessRider()}">
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack2_spouse_plan_ci.jsp"/>
                                    </c:when>
                                    <c:when test="${application.getIncludeRefundRider()}">
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack2_spouse_plan_pr.jsp"/>
                                    </c:when>
                                    <c:otherwise>
                                        <jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/ack2_spouse_plan.jsp"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <div>
                                <div class="space40"></div>
                                <c:if test="${application.insuredIsSelf()}">

                                     <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
                                        <input type="checkbox" name="declare_accept" aria-describedby="ack1" value="Y" <c:if test="${data['declare_accept'] == 'Y'}">checked</c:if>>
                                        <span></span>
                                        <b>By submitting this application, I <c:out value="${application.getApplicantFullName()}"></c:out> acknowledge 
                                        that I have read, understood
                                        and agree with the statements above.</b>
                                    </label>
                                    <c:if test="${errors.containsKey('declare_accept')}">
                                       <div class="error-message" aria-label="Error" style="padding-bottom:10px;" id="ack1">
                                           <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                           <c:out value="${errors['declare_accept']}"></c:out>
                                       </div>
                                    </c:if>
                                </c:if>
                            </div>

                            <c:if test="${!application.insuredIsSelf()}">
                                <div>
                                    <label class="checkbox" role="checkbox" tabindex="0" aria-checked="false">
                                        <input type="checkbox" name="declare_accept_insured" value="Y" <c:if test="${data['declare_accept_insured'] == 'Y'}">checked</c:if> aria-describedby="ack2">
                                        <span></span>
                                        <b>By submitting this application, we, <c:out value="${application.getApplicantFullName()}"></c:out> and 
                                        <c:out value="${ application.getInsuredFullName() }"></c:out> acknowledge that we have 
                                        read, understood and agree with the statements above.
                                    </label>
                                    <c:if test="${errors.containsKey('declare_accept_insured')}">
                                        <div class="error-message" aria-label="Error" style="padding-bottom:10px;" id="ack2">
                                            <i class="icon icon-circle-delete" aria-hidden="true"></i>
                                            <c:out value="${errors['declare_accept_insured']}"></c:out>
                                        </div>
                                    </c:if>
                                </div>
                            </c:if>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <div class="space40"></div>
                            <button type="button" class="back_btn" data-form-action="prev" data-url="/our-plans/online-protector/12">Back</button>
                            <button type="button" class="btn btn-red" data-form-action="next">Submit</button>
                        </div>

                    </form>
                
                </div>
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/ourplans/includes/onlineprotector_tracking_application.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/ourplans/onlineprotector/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/includes/footer.jsp"/>
