<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${fn:length(errors) eq 0}">

    <div class="hr" aria-hidden="true"></div>
    <div class="estimate-form-result">
        <div class="row">
            <div class="col-md-4">
                <h2>Your quote</h2>
                <c:choose>
                    <c:when test="${includeCriticalIllnessRider and includeRefundRider}">
                        <p>To get a basic coverage of <strong>S$<c:out value="${sumAssured}"></c:out></strong>  and inclusion of HSBC Insurance Online Critical Illness Rider with <strong>S$<c:out value="${riderSumAssured}"></c:out></strong> cover and HSBC Insurance Online Premium Refund Rider, here is an estimate of your monthly and annual premiums.</p>
                    </c:when>
                    <c:when test="${includeCriticalIllnessRider}">
                        <p>To get a basic coverage of <strong>S$<c:out value="${sumAssured}"></c:out></strong>  and inclusion of HSBC Insurance Online Critical Illness Rider with <strong>S$<c:out value="${riderSumAssured}"></c:out></strong> cover, here is an estimate of your monthly and annual premiums</p>
                    </c:when>
                    <c:when test="${includeRefundRider}">
                        <p>To get a basic coverage of <strong>S$<c:out value="${sumAssured}"></c:out></strong> along with HSBC Insurance Online Premium Refund Rider, here is an estimate of your monthly and annual premiums.</p>
                    </c:when>
                    <c:otherwise>
                        <p>To get a basic coverage of <strong>S$<c:out value="${sumAssured}"></c:out></strong>, here is an estimate of your monthly and annual premiums.</p>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Monthly <span>payments</span></h3>
                        <p>S$<span class="large-text"><c:out value="${monthlyPremium}"></c:out></span></p>
                    </div>
                    <div class="col-md-6">

                        <h3>Annual <span>payments</span></h3>

                        <p>S$<span class="large-text"><c:out value="${yearlyPremium}"></c:out></span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <p>This quote is only an estimate. <strong>Purchase this plan now to secure your protection needs</strong>.</p>
                <a href="#" class="primary-btn-slate" data-action="buy">Buy now</a>
            </div>
        </div>
    </div>

</c:if>

<script>
	var flag = false;
	$('a[data-action=buy]').click(function(evt) {
		evt.preventDefault();
		if (!flag) {
			flag = true;
			TMS.trackEvent({
				'event_category': 'Calculator Quick Quote',
				'event_action':   'Buy Now',
				'event_form':     'Form Buy Now',
				'event_content':  'Online Protector Calculator (By Now)'
			});
			setTimeout(function () {
				location.href = '/our-plans/online-protector/1?' + $('#quick-quote-form').serialize();
				flag = false;
			}, 2000);
		}
	});

    <c:if test="${fn:length(errors) gt 0}">
        <c:forEach items="${errors}" var="error">
            var value="${error.value}";

    		var sumassured="${sumAssured}";

    		if(value.indexOf("SGDX")!=-1){
    			value=value.replace("SGDX", "SGD"+sumassured);
    		}
    	   $('.error-message[data-field-name="<c:out value="${error.key}"></c:out>"] span').text(value);
    	   $('.error-message[data-field-name="<c:out value="${error.key}"></c:out>"]').show();
    	   $('.form-group[data-field-name="<c:out value="${error.key}"></c:out>"]').addClass('error');
    	</c:forEach>
    </c:if>
    <c:if test="${fn:length(errors) eq 0}">
    	$('a[data-action=quote]')
    		.addClass('secondary-btn-slate')
    		.text('Recalculate')
    		.blur();
    </c:if>

</script>
