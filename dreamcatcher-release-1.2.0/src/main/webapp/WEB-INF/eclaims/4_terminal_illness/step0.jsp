<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-homepage"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/4_terminal_illness/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor eclaims__page">
            <div class="container">

                <div class="eclaims__titler">
                    <h1>Documents Required</h1>
                    <a href="/" class="buynow-exit-button absolute">Exit</a>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" data-form-name="eclaims" data-form-type="Terminal Illness">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                          <p>Note that all documents submitted must be in English. Any non-English documents are to be translated to English by a certified translator.</p>   
                          
                          <ol style="list-style-type: decimal;">
                            <li>Medical reports from attending Doctor(s)</li> 
                            <li><a href="/assets/forms/CAAF_eclaim.pdf" target="_blank">Duly completed Clinical Abstract Application Form</a></li>
                          </ol>
                        </div>
                        <div class="article__leader__main__btnset col-xs-12 col-md-9 ">
                            <button type="button" class="btn btn-red" onclick="submitForm()">Continue</button>
                            <button type="button" class="back_btn" onclick="openPage('../4-terminal-illness')">Back</button>
                        </div><!-- /.item -->
        
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->

            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/4_terminal_illness/includes/eclaims4_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
