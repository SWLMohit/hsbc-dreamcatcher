<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/4_terminal_illness/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">
            
                <div class="eclaims__titler">
                    <h1>eClaims &mdash; Terminal Illness &mdash; Declaration &amp; Authorisation</h1>
                    <a href="/" class="buynow-exit-button">Exit</a>
                </div>
                <div class="onlineinvestor__breadcrumb">
                    <a href="../4-terminal-illness/1">Claim information</a>
                    <a href="../4-terminal-illness/2">Other information</a>
                    <a href="../4-terminal-illness/3">Payment option</a>
                    <a href="#">Declaration</a>
                </div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" data-form-name="eclaims" data-form-type="Terminal Illness">
                        <jsp:include page="/WEB-INF/eclaims/includes/declaration.jsp"></jsp:include>
                      <div class="article__leader__main__btnset">
                          <button type="button" class="btn btn-red" onclick="submitForm()">Submit</button>
                          <c:choose>
                              <c:when test="${data['payment'] == 3}">
                                  <button type="button" class="back_btn" onclick="openPage('../4-terminal-illness/4a')">Back</button>
                              </c:when>
                              <c:otherwise>
                                  <button type="button" class="back_btn" onclick="openPage('../4-terminal-illness/4')">Back</button>
                              </c:otherwise>
                          </c:choose>
                      </div>
        
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>

</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/4_terminal_illness/includes/eclaims4_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
