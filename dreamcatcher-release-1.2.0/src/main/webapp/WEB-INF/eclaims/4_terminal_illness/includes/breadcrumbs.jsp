<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/eclaims/">eClaims</a>
        <a href="/eclaims/4-terminal-illness" style="text-decoration: underline;" aria-current="page">Terminal Illness</a>
    </nav>
</div>
