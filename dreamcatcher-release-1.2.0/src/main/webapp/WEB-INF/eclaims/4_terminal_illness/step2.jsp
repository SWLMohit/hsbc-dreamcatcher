<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/4_terminal_illness/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">
            
              <div class="eclaims__titler">
                <h1>eClaims &mdash; Terminal Illness &mdash; Other information</h1>
                <a href="/" class="buynow-exit-button">Exit</a>
              </div>
                <div class="onlineinvestor__breadcrumb">
                  <a href="../4-terminal-illness/1">Claim information</a>
                  <a href="#">Other information</a>
                  <a href="#" class="disabled">Payment option</a>
                  <a href="#" class="disabled">Declaration</a>
                </div>

              <div class="row">
                <div class="col-xs-12">
                  <form class="buynow-form" method="post" data-form-name="eclaims" data-form-type="Terminal Illness">
                    <div class="onlineinvestor_carousel">
                      <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                        <div class="row onlineinvestor__questionnaire">
                          <fieldset>
                            <div class="col-xs-12 col-md-5"><legend>Has the Claimant, Life Insured or Policyowner been bankrupt or insolvent or has either executed any deed or transfer for the benefit of creditors since becoming interested in the policy?</legend></div>
                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="bankrupt">
                              <div class="newradio--inline">
                                <input id="id_bankrupt_yes" type="radio" name="bankrupt" value="true" <c:if test="${data['bankrupt'] == true}">checked</c:if>>
                                <label for="id_bankrupt_yes"><span><span></span></span>Yes</label>
                              </div>
                              <div class="newradio--inline">
                                <input id="id_bankrupt_no" type="radio" name="bankrupt" value="false" <c:if test="${data['bankrupt'] == false}">checked</c:if> aria-describedby="ariaIsBankrupt">
                                <label for="id_bankrupt_no"><span><span></span></span>No</label>
                              </div>
                              <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaIsBankrupt"><small>Please select an option</small></div>
                            </div>
                          </fieldset>
                        </div>
                        <div class="onlineinvestor__questionnaire bankrupt_wrapper" <c:if test="${data['bankrupt'] == null || data['bankrupt'] == false}">style="display: none"</c:if>>
                          <div class="bankrupt-container">
                            <h2>Please provide details</h2>
                            <c:forEach var="bankrupt" varStatus="arr_count" items="${data['bankrupt_list']}" >
                              <div class="bankrupt" id="id_bankrupt_${arr_count.count}">
                                <div class="row">
                                  <fieldset>
                                    <div class="col-xs-12 col-md-5"><legend>Name of bankrupt person <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if></legend></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="bankrupt_name">
                                      <div class="form-group__multi name">
                                        <span><input type="text" class="form-control <c:if test="${data['bankrupt'] == null || data['bankrupt'] == false}">optional</c:if>" placeholder="Given name" name="bankrupt_first_name" value="${fn:escapeXml(bankrupt['bankrupt_first_name'])}"></span>
                                        <span><input type="text" class="form-control <c:if test="${data['bankrupt'] == null || data['bankrupt'] == false}">optional</c:if>" placeholder="Surname" name="bankrupt_last_name" value="${fn:escapeXml(bankrupt['bankrupt_last_name'])}" aria-describedby="ariaBankruptName"></span>
                                      </div>
                                      <div>
                                        <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankruptName"><small>Please enter a name</small></div>
                                        <div class="help-block secondary-error" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Your name contains invalid characters</small></div>
                                      </div>
                                    </div>
                                  </fieldset>
                                </div>

                                <div class="row">
                                  <fieldset>
                                    <div class="col-xs-12 col-md-5"><legend>Country/State that issued the bankrupt order</legend></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="bankrupt_country">
                                      <div class="form-group__multi">
                                        <span>
                                          <div class="form-select_wrap">
                                            <select class="form-control control_country_select <c:if test="${data['bankrupt'] == null || data['bankrupt'] == false}">optional</c:if>" name="bankrupt_country" aria-describedby="ariaBankruptCountry">
                                              <option disabled selected>Please select&hellip;</option>
                                              <c:forEach var="country" varStatus="arr_count" items="${formData['country_list']}" >
                                                <c:choose>
                                                  <c:when test="${country == 'None of the above'}">
                                                    <option <c:if test="${bankrupt['bankrupt_country'] == 'none_of_above'}">selected</c:if> value="none_of_above">${country}</option>
                                                  </c:when>
                                                  <c:otherwise>
                                                    <option <c:if test="${bankrupt['bankrupt_country'] == country}">selected</c:if>>${country}</option>
                                                  </c:otherwise>
                                                </c:choose>
                                              </c:forEach>
                                            </select>
                                          </div>
                                        </span>
                                        <span>
                                          <input class="form-control <c:if test="${data['bankrupt'] == null || data['bankrupt'] == false}">optional</c:if>" type="text" placeholder="State" name="bankrupt_state" value="${fn:escapeXml(bankrupt['bankrupt_state'])}">
                                        </span>
                                      </div>
                                      <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankruptCountry"><small>Please select an option</small></div>
                                    </div>
                                  </fieldset>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12 col-md-5"><label for="date-bankrupcy">Date of Bankruptcy</label></div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown" data-field-name="bankrupt_bankruptcy_date">
                                    <input id="date-bankrupcy" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate date-clone auto-width form-control date-bankrupcy <c:if test="${data['bankrupt'] == null || data['bankrupt'] == false}">optional</c:if>" name="bankrupt_bankruptcy_date_day"
                                           value="${fn:escapeXml(bankrupt['bankrupt_bankruptcy_date_day'])}" aria-describedby="ariaBankruptDate">
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankruptDate">
                                      <small>Please enter a valid date</small>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </c:forEach>
                          </div>
                          <!-- Add More Something Button -->
                          <div class="row">
                            <div class="col-xs-12 col-md-6 container-fluid">
                              <button type="button" class="blocklink" onclick="addBankrupt()">
                                <i class="icon icon-add"></i> Add the details of another bankrupt person, country / state that issued the bankrupt order
                              </button>
                            </div>
                          </div>
                          <div class="space20"></div>
                          <!-- Add More Something Button -->
                        </div>
            
                        <div class="row onlineinvestor__questionnaire">
                          <fieldset>
                            <div class="col-xs-12 col-md-5"><legend>Has the Deceased or Life Insured been insured with other insurance company(ies)?</legend></div>
                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="other_insured">
                              <div class="newradio--inline">
                                <input id="id_other_insured_yes" type="radio" name="other_insured" value="true" <c:if test="${data['other_insured'] == true}">checked</c:if>>
                                <label for="id_other_insured_yes"><span><span></span></span>Yes</label>
                              </div>
                              <div class="newradio--inline">
                                <input id="id_other_insured_no" type="radio" name="other_insured" value="false" <c:if test="${data['other_insured'] == false}">checked</c:if> aria-describedby="ariaLifeInsured">
                                <label for="id_other_insured_no"><span><span></span></span>No</label>
                              </div>
                              <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaLifeInsured"><small>Please select an option</small></div>
                            </div>
                          </fieldset>
                        </div>
                        <div class="onlineinvestor__questionnaire other_insured_wrapper" <c:if test="${data['other_insured'] == null || data['other_insured'] == false}">style="display: none"</c:if>>

                          <div class="insurer-container">
                            <h2>Please provide details</h2>
                            <c:forEach var="other_insured" varStatus="arr_count" items="${data['other_insured_list']}" >
                              <div class="insurer" id="id_insurer_${arr_count.count}">
                                <div class="row">
                                  <div class="col-xs-12 col-md-5">Name of Insurance Company <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if></div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="other_insured_company_name">
                                    <input class="form-control <c:if test="${data['other_insured'] == null || data['other_insured'] == false}">optional</c:if>" type="text" name="other_insured_company_name" value="${fn:escapeXml(other_insured['other_insured_company_name'])}" aria-describedby="ariaLifeInsuredCompany">
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaLifeInsuredCompany"><small>Please enter a value</small></div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12 col-md-5"><label for="date-policy">Issue Date of Policy</label></div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown" data-field-name="other_insured_policy_issue_date">
                                    <input id="date-policy" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate date-clone auto-width form-control date-policy <c:if test="${data['other_insured'] == null || data['other_insured'] == false}">optional</c:if>" name="other_insured_policy_issue_date_day"
                                           value="${fn:escapeXml(other_insured['other_insured_policy_issue_date_day'])}" aria-describedby="ariaLifeInsuredDate">
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaLifeInsuredDate">
                                      <small>Please enter a valid date</small>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12 col-md-5">Type of Plan</div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="other_insured_plan_type">
                                    <input class="form-control <c:if test="${data['other_insured'] == null || data['other_insured'] == false}">optional</c:if>" type="text" name="other_insured_plan_type" value="${fn:escapeXml(other_insured['other_insured_plan_type'])}" aria-describedby="ariaLifeInsuredPlan">
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaLifeInsuredPlan"><small>Please enter a value</small></div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12 col-md-5">Claim Amount</div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="other_insured_claim_amount">
                                    <div class="input-group currency-text-box" style="width: unset">
                                      <span class="input-group-addon">S$</span>
                                      <input class="form-control number-only-input <c:if test="${data['other_insured'] == null || data['other_insured'] == false}">optional</c:if>" type="text" name="other_insured_claim_amount" value="${fn:escapeXml(other_insured['other_insured_claim_amount'])}"  aria-describedby="ariaLifeInsuredClaim">
                                    </div>
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaLifeInsuredClaim"><small>Please enter a value</small></div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12 col-md-5">Claim Admitted</div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="other_insured_claim_admitted_${arr_count.count}">
                                    <div class="newradio--inline">
                                      <input id="id_admitted_option1_${arr_count.count}" type="radio" value="True" class="admitted_option_yes <c:if test="${data['other_insured'] == null || data['other_insured'] == false}">optional</c:if>" name="other_insured_claim_admitted_${arr_count.count}" <c:if test="${other_insured['other_insured_claim_admitted'] == true}">checked</c:if> >
                                      <label for="id_admitted_option1_${arr_count.count}" class="admitted_option_label_yes"><span><span></span></span>Yes</label>
                                    </div>
                                    <div class="newradio--inline">
                                      <input id="id_admitted_option2_${arr_count.count}" type="radio" value="False" class="admitted_option_no <c:if test="${data['other_insured'] == null || data['other_insured'] == false}">optional</c:if>" name="other_insured_claim_admitted_${arr_count.count}" <c:if test="${other_insured['other_insured_claim_admitted'] == false}">checked</c:if> aria-describedby="ariaLifeInsuredClaimAccept">
                                      <label for="id_admitted_option2_${arr_count.count}" class="admitted_option_label_no"><span><span></span></span>No</label>
                                    </div>
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaLifeInsuredClaimAccept"><small>Please select an option</small></div>
                                  </div>
                                </div>

                              </div>
                            </c:forEach>
                          </div>
                          <!-- Add More Something Button -->
                          <div class="row">
                            <div class="col-xs-12 col-md-6 container-fluid">
                              <button type="button" class="blocklink" onclick="addInsurer()">
                                <i class="icon icon-add"></i> Add the details of another insurance company
                              </button>
                            </div>
                          </div>
                          <div class="space20"></div>
                          <!-- Add More Something Button -->
                        </div>
                      </div>
                      <div class="article__leader__main__btnset col-xs-12 col-md-9 ">
                        <button type="button" class="btn btn-red" onclick="validateThenOpenPage()">Continue</button>
                        <button type="button" class="back_btn" onclick="openPage('../4-terminal-illness/1a')">Back</button>
                      </div><!-- /.item -->

                    </div>
                  </form>
                </div>
              </div><!-- /.row -->
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/4_terminal_illness/includes/eclaims4_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function(){
//        $('.bankrupt_wrapper').hide();
//        $('.other_insured_wrapper').hide();
//
//        for (var day = 1; day <= 31; day++) {
//            $(".control_day_select").append("<option>" + pad(day, 2) + "</option>");
//        }
//        $(".control_day_select").selectBox('refresh');
//
//        for (var month = 1; month <= 12; month++) {
//            $(".control_month_select").append("<option>" + pad(month, 2) + "</option>");
//        }
//        $(".control_month_select").selectBox('refresh');
//
//        var currYear = (new Date()).getFullYear();
//
//        for (var year = 1970; year <= currYear; year++) {
//            $(".control_year_select").append("<option>" + year + "</option>");
//        }
//        $(".control_year_select").selectBox('refresh');
    });

</script>
