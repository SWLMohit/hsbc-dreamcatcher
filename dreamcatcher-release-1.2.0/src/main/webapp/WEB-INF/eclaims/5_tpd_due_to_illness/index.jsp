<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-homepage"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/5_tpd_due_to_illness/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">

                <div class="eclaims__titler">
                    <h1>eClaims &mdash; TPD Due to Illness &mdash; Make a Claim</h1>
                    <a href="/" class="buynow-exit-button">Exit</a>
                </div>
                <div class="space20"></div>
              <div class="row">
                <div class="col-xs-12">
                  <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="eclaims" data-form-type="Tpd Due To Illness">
                    <div class="onlineinvestor_carousel">
                      <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                        <jsp:include page="/WEB-INF/eclaims/includes/policyownerdetails.jsp" flush="true"/>
                      </div>
                      <div class="article__leader__main__btnset col-xs-12 col-sm-9">
                        <button type="button" class="btn btn-red" onclick="validateThenOpenPage()">Continue</button>
                        <button type="button" class="back_btn" onclick="openPage('../eclaims/')">Back</button>
                      </div><!-- /.item -->

                    </div>
                  </form>
                </div>
              </div><!-- /.row -->

            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/5_tpd_due_to_illness/includes/eclaims5_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function() {
        $('.not_policy_owner_wrapper').hide();
    });
</script>