<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/5_tpd_due_to_illness/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">

                <div class="eclaims__titler">
                    <h1>eClaims &mdash; TPD Due to Illness &mdash; Claim information</h1>
                    <a href="/" class="buynow-exit-button">Exit</a>
                </div>
                <div class="onlineinvestor__breadcrumb">
                    <a href="#">Claim information</a>
                    <a href="#" class="disabled">Other information</a>
                    <a href="#" class="disabled">Payment option</a>
                    <a href="#" class="disabled">Declaration</a>
                </div>

                <div class="row">
                  <div class="col-xs-12 ">
                    <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="eclaims" data-form-type="Tpd Due To Illness">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                          <div class="onlineinvestor__questionnaire">
                            <div class="attended-container">
                              <h2>Please provide the details of any doctor(s) or specialist(s) who
                                  have been consulted in connection with your illness. Please also
                                  provide details of your usual doctor(s).
                              </h2>

                              <c:forEach var="doctor" varStatus="arr_count" items="${data['doctor_list']}">
                                <div class="attended" id="id_attended_${arr_count.count}">
                                  <div class="row">
                                    <fieldset>
                                      <div class="col-xs-12 col-md-5"><legend>Consultation <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if> Period</legend></div>
                                      <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown_from" data-field-name="doctor_from_date">
                                        <p><label for="date-from">From:</label></p>
                                        <input id="date-from" type="text" name="doctor_from_date" placeholder="mm/yyyy" maxlength="7" class="date-validate-month auto-width form-control date-consult"
                                               value="${fn:escapeXml(doctor['doctor_from_date'])}" aria-describedby="ariaConsultationFrom">
                                        <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaConsultationFrom"><small>Please enter a valid date in the MM/YYYY format</small></div>
                                      </div>
                                   
                                      <div class="col-xs-12 col-md-6 col-md-offset-6 form-group date_dropdown_to" data-field-name="doctor_to_date">
                                        <p><label for="date-to">To:</label></p>
                                        <input id="date-to" type="text" name="doctor_to_date" placeholder="mm/yyyy" maxlength="7" class="date-validate-month auto-width form-control date-consult"
                                               value="${fn:escapeXml(doctor['doctor_to_date'])}" aria-describedby="ariaConsultationTo">
                                        <div id="ariaConsultationTo">
                                          <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a valid date in the MM/YYYY format</small></div>
                                          <div class="help-block date-out-of-range secondary-error">
                                              <small>Please enter a valid date range</small>
                                          </div>
                                        </div>
                                      </div>
                                    </fieldset>
                                  </div>

                                  <div class="row">
                                    <div class="col-xs-12 col-md-5">Name of Doctor / Specialist <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="doctor_name">
                                        <input class="form-control" type="text" name="doctor_name"
                                               value="${fn:escapeXml(doctor['doctor_name'])}" aria-describedby="ariaIllnessDoctor">
                                        <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaIllnessDoctor">
                                            <small>Please enter a value</small>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-xs-12 col-md-5">Name of Hospital / Clinic <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="doctor_hospital">
                                        <input class="form-control" type="text"
                                               name="doctor_hospital"
                                                aria-describedby="ariaIllnessHospital"
                                               value="${fn:escapeXml(doctor['doctor_hospital'])}">
                                        <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaIllnessHospital">
                                            <small>Please enter a value</small>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-xs-12 col-md-5"><legend>Address of Hospital / Clinic <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if></legend></div>
                                      <div class="col-xs-12 col-md-6 col-md-offset-1 form-group city_dropdown" data-field-name="doctor_hospital_state">
                                        <p>Country</p>
                                        <div class="form-select_wrap">
                                          <select class="form-control control_country_select" name="doctor_hospital_state" aria-describedby="ariaIllnessHospitalCountry">
                                            <option disabled selected value="">Choose one&hellip;</option>
                                            <c:forEach var="country" varStatus="arr_count" items="${formData['country_list']}">
                                              <c:choose>
                                                <c:when test="${country == 'None of the above'}">
                                                  <option
                                                          <c:if test="${doctor['doctor_hospital_state'] == 'none_of_above'}">selected</c:if>
                                                          value="none_of_above">${fn:escapeXml(country)}</option>
                                                </c:when>
                                                <c:otherwise>
                                                  <option
                                                          <c:if test="${doctor['doctor_hospital_state'] == country}">selected</c:if>>${fn:escapeXml(country)}</option>
                                                </c:otherwise>
                                              </c:choose>
                                            </c:forEach>
                                          </select>
                                        </div>
                                        <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaIllnessHospitalCountry">
                                            <small>Please choose an option</small>
                                        </div>
                                      </div>
                                   
                                      <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="doctor_hospital_street1">
                                          <p>Street</p>
                                          <input type="text" class="form-control bottom_margined"
                                                 placeholder="" name="doctor_hospital_street1"
                                                 value="${fn:escapeXml(doctor['doctor_hospital_street1'])}">
                                      </div>
                                   
                                      <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="doctor_hospital_street2">
                                          <p>Street line 2 <small>(optional)</small></p>
                                          <input type="text" class="form-control optional"
                                                 placeholder=""
                                                  aria-describedby="ariaIllnessHospitalStreet"
                                                 name="doctor_hospital_street2"
                                                 value="${fn:escapeXml(doctor['doctor_hospital_street2'])}">
                                          <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaIllnessHospitalStreet">
                                              <small>Please enter a value</small>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                              </c:forEach>
                            </div>
                                            <!-- Add More Something Button -->
                            <div class="row">
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="blocklink" onclick="addAttended()">
                                    <i class="icon icon-add"></i> Add the details of another doctor, hospital / clinic
                                </button>
                              </div>
                            </div>
                            <div class="space30"></div>
                                  <!-- Add More Something Button -->
                          </div>
                                    
                          <div class="onlineinvestor__questionnaire">
                            <div class="row">
                              <div class="col-xs-12 col-md-5">Please upload a copy of your medical reports from your attending Doctor(s)</div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="medical_report">
                                <a data-file-input="medical_report"
                                   class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when
                                        test="${data['medical_report'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                <input type="file"
                                        aria-describedby="ariaUpload1"
                                       <c:if test="${data['medical_report'] != null}">class="optional"</c:if>

                                       name="medical_report" value="${fn:escapeXml(data['medical_report'])}" accept="image/jpeg,image/jpg,image/png,application/pdf">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaUpload1">
                                    <small>Please enter a value</small>
                                </div>
                              </div>
                            </div>

                            <div class="row"> 
                              <div class="col-xs-12 col-md-5">Please upload a duly signed Clinical Abstract Application form</div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="clinical_abstract_form">
                                <a data-file-input="clinical_abstract_form"
                                   class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when
                                        test="${data['clinical_abstract_form'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                <input type="file"
                                      aria-describedby="ariaUpload2"
                                       <c:if test="${data['clinical_abstract_form'] != null}">class="optional"</c:if>
                                       name="clinical_abstract_form"

                                       value="${fn:escapeXml(data['clinical_abstract_form'])}" accept="image/jpeg,image/jpg,image/png,application/pdf">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaUpload2">
                                    <small>Please enter a value</small>
                                </div>
                              </div>
                            </div>
                                    
                          </div><!-- /.item -->

                        </div>

                        <div class="article__leader__main__btnset col-xs-12 col-sm-9">
                          <button type="button" class="btn btn-red" onclick="validateThenOpenPage()">
                              Continue
                          </button>
                          <button type="button" class="back_btn"
                                  onclick="openPage('../5-tpd-due-to-illness/1a')">Back
                          </button>
                        </div>
                      </form>
                  </div>
              </div><!-- /.row -->
          </div>
      </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>

<jsp:include page="/WEB-INF/eclaims/5_tpd_due_to_illness/includes/eclaims5_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    //    $(document).ready(function(){
    //        for (var month = 1; month <= 12; month++) {
    //            $(".control_month_select").append("<option>" + pad(month, 2) + "</option>");
    //        }
    //        $(".control_month_select").selectBox('refresh');
    //
    //        var currYear = (new Date()).getFullYear();
    //
    //        for (var year = 1970; year <= currYear; year++) {
    //            $(".control_year_select").append("<option>" + year + "</option>");
    //        }
    //        $(".control_year_select").selectBox('refresh');
    //    });

</script>
