<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/7_unemployment/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">
            
              <div class="eclaims__titler">
                  <h1>eClaims &mdash; Unemployment &mdash; Declaration &amp; Authorisation</h1>
                  <a href="/" class="buynow-exit-button">Exit</a>
              </div>
              <div class="row">
                <form class="buynow-form" method="post" data-form-name="eclaims" data-form-type="Unemployment">
                  <div class=" col-xs-12 col-sm-10">
                    <jsp:include page="/WEB-INF/eclaims/includes/declaration.jsp"></jsp:include>
                  </div>
                  <div class="article__leader__main__btnset col-xs-12 col-sm-10">
                    <button type="button" class="btn btn-red" onclick="submitForm()">Submit</button>
                    <button type="button" class="back_btn" onclick="openPage('../7-unemployment/0')">Back</button>
                  </div>
                </form>
              </div>
            </div><!-- /.row -->
        </section>
    </article>

</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/7_unemployment/includes/eclaims7_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
