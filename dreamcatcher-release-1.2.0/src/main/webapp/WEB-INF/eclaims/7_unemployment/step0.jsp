<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-homepage"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/7_unemployment/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor eclaims__page">
            <div class="container">

                <div class="eclaims__titler">
                    <h1>Documents Required</h1>
                    <a href="/" class="buynow-exit-button absolute">Exit</a>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="eclaims" data-form-type="Unemployment">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                            <p>Note that all documents submitted must be in English. Any non-English documents are to be translated to English by a certified translator.</p>

                            <div class="row onlineinvestor__questionnaire">
                                <div class="col-xs-12 col-md-5">Please upload an official letter of unemployment from your employer stating the effective date of your unemployment.</div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_unemployment_document">
                                    <a data-file-input="policyowner_unemployment_document" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${fn:escapeXml(data['policyowner_unemployment_document'] == null)}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                    <input type="file" name="policyowner_unemployment_document" value="${data['policyowner_unemployment_document']}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="ariaUpload">
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaUpload"><small>Please upload a file</small></div>
                                </div>
                            </div>
                        </div><!-- /.item -->
                        <div class="article__leader__main__btnset col-xs-12 col-sm-9">
                            <button type="button" class="btn btn-red" onclick="validateThenOpenPage()">Continue</button>
                            <button type="button" class="back_btn" onclick="openPage('../7-unemployment')">Back</button>
                        </div>
        
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->

            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/7_unemployment/includes/eclaims7_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
