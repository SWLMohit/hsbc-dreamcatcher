<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/6_tpd_due_to_accident/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">

                <div class="eclaims__titler">
                    <h1>eClaims &mdash; TPD Due to Accident &mdash; Claim information</h1>
                    <a href="/" class="buynow-exit-button">Exit</a>
                </div>
                <div class="onlineinvestor__breadcrumb">
                    <a href="#">Claim information</a>
                    <a href="#" class="disabled">Other information</a>
                    <a href="#" class="disabled">Payment option</a>
                    <a href="#" class="disabled">Declaration</a>
                </div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" data-form-name="eclaims" data-form-type="Tpd Due To Accident">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                          <div class="onlineinvestor__questionnaire">
                            
                            <div class="row">
                              <div class="col-xs-12 col-md-5">Please describe in detail all symptoms and / or nature of injuries / disability suffered</div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="illness_suffered">
                                <input class="form-control" type="text" name="illness_suffered" value="${fn:escapeXml(data['illness_suffered'])}" aria-describedby="ariaSymptoms">
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaSymptoms"><small>Please enter a value</small></div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="date-last-work">Date on which you last worked prior to your disability</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown_from" data-field-name="disability_last_work_date">
                                <input id="date-last-work" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-last-work" name="disability_last_work_date_day"
                                         value="${fn:escapeXml(data['disability_last_work_date_day'])}" aria-describedby="ariaLastWorked"> 
                                <div id="ariaLastWorked">
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Your last working date is missing</small></div>
                                  <div class="help-block secondary-error" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please select a valid date</small></div>
                                  <input type="hidden" name= "policy_owner_birthday" value="${application.getPolicyownerBirthday()}">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="date-return-work">Date on which you returned or expect to return to work <small>(optional)</small></label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown_to" data-field-name="disability_return_work_date">
                                <input id="date-return-work" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-return-work optional" name="disability_return_work_date_day"
                                     value="${fn:escapeXml(data['disability_return_work_date_day'])}" aria-describedby="ariaReturnWork">
                                <div id="ariaReturnWork">
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error">
                                      <small>Please select an option</small>
                                  </div>
                                  <div class="help-block date-out-of-range secondary-error">
                                      <small>Please enter a valid date range</small>
                                  </div>
                                </div>
                              </div>
                            </div>
              
                          </div>

                        </div>
                        <div class="article__leader__main__btnset col-xs-12 col-sm-9">
                          <button type="button" class="btn btn-red" onclick="validateThenOpenPage()">Continue</button>
                          <button type="button" class="back_btn" onclick="openPage('../6-tpd-due-to-accident/1a')">Back</button>
                        </div><!-- /.item -->
        
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/6_tpd_due_to_accident/includes/eclaims6_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
