<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/3_critical_illness/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">
            
                <div class="eclaims__titler">
                    <h1>eClaims &mdash; Critical Illness &mdash; Claim information</h1>
                    <a href="/" class="buynow-exit-button">Exit</a>
                </div>
                <div class="onlineinvestor__breadcrumb">
                    <a href="#">Claim information</a>
                    <a href="#" class="disabled">Other information</a>
                    <a href="#" class="disabled">Payment option</a>
                    <a href="#" class="disabled">Declaration</a>
                </div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" data-form-name="eclaims" data-form-type="Critical Illness">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                          <div class="onlineinvestor__questionnaire">
                            <div class="row">
                              <div class="col-xs-12 col-md-5">Please describe fully the extent and nature of your illness</div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="illness_nature">
                                <input class="form-control" type="text" name="illness_nature" value="${fn:escapeXml(data['illness_nature'])}" aria-describedby="ariaIllness">
                                  <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaIllness"><small>Please enter a value</small></div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="date-first-consult">When did you first consult a doctor in connection with your illness?</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown" data-field-name="illness_doctor_consult_date">
                                <input id="date-first-consult" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-first-consult" name="illness_doctor_consult_date_day"
                                       value="${fn:escapeXml(data['illness_doctor_consult_date_day'])}" aria-describedby="ariaFirstConsult">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaFirstConsult"><small>Please enter a valid date</small></div>
                              </div>
                            </div>

                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Have you previously suffered from, or received treatment for a similar or related illness?</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="illness_previously_suffered">
                                  <div class="newradio--inline">
                                    <input id="id_previous_suffered_yes" type="radio" name="illness_previously_suffered"
                                           value="true" <c:if test="${data['illness_previously_suffered'] == true}">checked</c:if>>
                                    <label for="id_previous_suffered_yes"><span><span></span></span>Yes</label>
                                  </div>
                                  <div class="newradio--inline">
                                    <input id="id_previous_suffered_no" type="radio" name="illness_previously_suffered"
                                           value="false" <c:if test="${data['illness_previously_suffered'] == false}">checked</c:if> aria-describedby="ariaPreviousSuffered">
                                    <label for="id_previous_suffered_no"><span><span></span></span>No</label>
                                  </div>
                                  <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaPreviousSuffered"><small>Please select an option</small></div>
                                </div>
                              </fieldset>
                            </div>

                            <div class="previous_suffered_container" <c:if test="${data['illness_previously_suffered'] == null || data['illness_previously_suffered'] == false}">style="display: none"</c:if>>
                              <div class="row">
                                  <div class="col-xs-12 col-md-5">Please give full details</div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="illness_previously_suffered_detail">
                                      <input class="form-control <c:if test="${data['illness_previously_suffered'] == null || data['illness_previously_suffered'] == false}">optional</c:if>" type="text" name="illness_previously_suffered_detail" value="${fn:escapeXml(data['illness_previously_suffered_detail'])}" aria-describedby="ariaPreviousSufferedDetail">
                                      <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaPreviousSufferedDetail"><small>Please enter a value</small></div>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div><!-- /.item -->
                        <div class="article__leader__main__btnset col-xs-12 col-md-9">
                          <button type="button" class="btn btn-red" onclick="validateThenOpenPage()">Continue</button>
                          <button type="button" class="back_btn" onclick="openPage('../3-critical-illness/0')">Back</button>
                        </div>
        
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/3_critical_illness/includes/eclaims3_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function(){
//        $('.previous_suffered_container').hide();
//
//        for (var day = 1; day <=31; day++){
//            $(".control_day_select").append("<option>" + pad(day,2)+ "</option>");
//        }
//        $(".control_day_select").selectBox('refresh');
//
//        for (var month = 1; month <=12; month++){
//            $(".control_month_select").append("<option>" + pad(month,2)+ "</option>");
//        }
//        $(".control_month_select").selectBox('refresh');
//
//        var currYear = (new Date()).getFullYear();
//
//        for (var year = 1970; year <= currYear; year++){
//            $(".control_year_select").append("<option>" + year + "</option>");
//        }
//        $(".control_year_select").selectBox('refresh');
    });

</script>

