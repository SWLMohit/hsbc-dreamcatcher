<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/eclaims/">eClaims</a>
        <a href="/eclaims/3-critical-illness" style="text-decoration: underline;" aria-current="page">Critical Illness</a>
    </nav>
</div>
