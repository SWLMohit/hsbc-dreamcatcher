<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/3_critical_illness/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">
            
                <div class="eclaims__titler">
                    <h1>eClaims &mdash; Critical Illness &mdash; Thank You</h1>
                    <a href="/" class="buynow-exit-button">Exit</a>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-9">
                    <form>
                      <h2>Thank you for submitting your claim</h2>
                      <p>
                          Your reference is ${fn:escapeXml(data['referenceNumber'])}
                      </p>
                      <p>
                        You will receive an email shortly acknowledging your submission.
                      </p>
                       <c:if test="${fn:escapeXml(data['fileSizeError']=='true')}">
							<p>Files upload fail: The total size has exceeded 10MB</p>
							<p>Our claims officer will contact you to complete your claim submission. We apologise for the inconvenience caused.</p>
					  </c:if>
                      <div class="article__leader__main__btnset">
                        <button type="button" class="btn btn-red" onclick="openPage('/')">Back to homepage</button>
                        <button type="button" class="back_btn" onclick="openPage('../3-critical-illness/4')">Back</button>
                      </div>
        
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>

</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/3_critical_illness/includes/eclaims3_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<jsp:include page="/WEB-INF/eclaims/includes/eclaims_tracking_finish.jsp"></jsp:include>
