<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">

                <div class="eclaims__titler">
                    <h1>eClaims &mdash; Death Due to Accident/Unnatural Causes &mdash; Payment option</h1>
                    <a href="/" class="buynow-exit-button">Exit</a>
                </div>
              <div class="onlineinvestor__breadcrumb">
                <a href="../1-death-due-to-accident/1">Claimant information</a>
                <a href="../1-death-due-to-accident/2">Claim information</a>
                <a href="../1-death-due-to-accident/3">Other information</a>
                <a href="#">Payment option</a>
                <a href="#" class="disabled">Declaration</a>
              </div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="eclaims" data-form-type="Death Due To Accident">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                          <jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/bankdetails.jsp" flush="true"/>
                          
                        </div><!-- /.item -->
                        <div class="article__leader__main__btnset col-xs-12 col-sm-9">
                          <button type="button" class="btn btn-red" data-form-action="next" onclick="validateThenOpenPage()">Continue</button>
                          <button type="button" class="back_btn" onclick="openPage('../1-death-due-to-accident/4')">Back</button>
                        </div>
        
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/eclaims1_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function(){
        // $('.bank_address_wrapper').hide();
    });

</script>
