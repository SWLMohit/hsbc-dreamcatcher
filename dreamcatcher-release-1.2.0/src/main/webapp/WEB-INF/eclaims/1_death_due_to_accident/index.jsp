<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-homepage"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">

              <div class="eclaims__titler">
                <h1>eClaims &mdash; Death Due to Accident/Unnatural Causes &mdash; Make a Claim</h1>
                <a href="/" class="buynow-exit-button">Exit</a>
              </div>
              <div class="space60"></div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="eclaims" data-form-type="Death Due To Accident">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                          <div class="onlineinvestor__questionnaire">
                            <div class="policy-number-container row ">
                              <div class="col-xs-12 col-md-5">Policy Number(s)</div>
                              <c:choose>
                                <c:when test="${data['policy_number_list'] != null}">
                                  <c:forEach var="policy_number" varStatus="arr_count" items="${data['policy_number_list']}" >
                                    <div class="policy-num col-xs-12 col-md-6 col-md-offset-6 form-group " data-field-name="policy_number">
                                      <label for="id_policy_${arr_count.count}" class="visuallyhidden">Policy Number</label>
                                      <input class="form-control alphanum-input" type="text" id="id_policy_${arr_count.count}" name="policy_number" value="${fn:escapeXml(policy_number)}" aria-describedby="aria_id_policy_${arr_count.count}">
                                      <div id="aria_id_policy_${arr_count.count}">
                                        <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please enter a valid policy number</small></div>
                                        <div class="help-block secondary-error" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please enter a unique policy number</small></div>
                                      </div>
                                    </div>
                                  </c:forEach>
                                </c:when>
                                <c:otherwise>
                                  <div class="policy-num col-xs-12 col-md-6 col-md-offset-1 form-group " data-field-name="policy_number">
                                    <label for="id_policy_1" class="visuallyhidden">Policy Number</label>
                                    <input class="form-control alphanum-input" type="text" id="id_policy_1" name="policy_number" aria-describedby="policyNnum2Aria">
                                    <div id="policyNnumAria2">
                                      <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please enter a valid policy number</small></div>
                                      <div class="help-block secondary-error" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please enter a unique policy number</small></div>
                                    </div>
                                  </div>
                                </c:otherwise>
                              </c:choose>

                            </div>
                            <!-- Add More Something Button -->
                            <div class="row">
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="blocklink" onclick="addPolicyNum()">
                                  <i class="icon icon-add"></i> Add another policy number
                                </button>
                              </div>
                            </div>
                            <div class="space30"></div>
                            <!-- Add More Something Button -->
              
                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Name of Policyowner</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_name">
                                  <div class="form-group__multi name">
                                    <span>
                                      <label for="form-firstname" class="visuallyhidden">First Name</label>
                                      <input type="text" id="form-firstname" class="form-control" name="policyowner_first_name" placeholder="Given name" value="${fn:escapeXml(data['policyowner_first_name'])}" aria-describedby="policyNameAria">
                                    </span>
                                    <span>
                                      <label for="form-lastname" class="visuallyhidden"> Last Name</label>
                                      <input type="text" id="form-lastname" class="form-control" name="policyowner_last_name" placeholder="Surname" value="${fn:escapeXml(data['policyowner_last_name'])}" aria-describedby="policyNameAria">
                                    </span>
                                  </div>
                                  <div id="policyNameAria">
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please enter a name</small></div>
                                    <div class="help-block secondary-error" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Your name contains invalid characters</small></div>
                                  </div>
                                </div>
                              </fieldset>
                            </div>

                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="nric">NRIC of Claimant</label></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group nric" data-field-name="policy_owner_nric">
                                  <input class="form-control alphanum-input" type="text" id="nric" name="policy_owner_nric" value="${fn:escapeXml(data['policy_owner_nric'])}" aria-describedby="policyNRICAria">
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyNRICAria"><small>Please enter a valid NRIC number</small></div>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="date">Date of Birth of Claimant</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown" data-field-name="policy_owner_birth">
                                <input id="date" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-dob" name="policy_owner_birth_day" value="${fn:escapeXml(data['policy_owner_birth_day'])}" aria-describedby="policyDOBAria">
                                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyDOBAria">
                                  <small>Please enter a valid date</small>
                                </div>
                              </div>
                            </div>

							<div class=" document-number-container row">
                              <div class="col-xs-12 col-md-5"><label for="form-nricupload">Please upload a copy of the NRIC / Passport of the Claimant</label></div>
                             
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_passport">
                              <div class="nric-container document-number-container ">
                              <input type="hidden" name="policyowner_passport_docs" class="policyowner_passport_docs" value="1"/>
                              <c:choose>
	                            <c:when test="${fn:length(data['policyowner_passport'])>0}">
	                              <c:forEach var="doc" varStatus="docNo" items="${data['policyowner_passport']}">   
                                  <div  class="document-num" id="document_no_${docNo.index}">
		                                <a data-file-input="policyowner_passport_${docNo.index}" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['policyowner_passport'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
		                                <input type="file" id="form-nricupload" <c:if test="${data['policyowner_passport'] != null}">class="optional"</c:if> name="policyowner_passport_${docNo.index}" value="${fn:escapeXml(doc)}" accept="image/jpeg,image/jpg,application/pdf,image/png" aria-describedby="policyowner_passport_${docNo.index}">
		                                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyowner_passport_${docNo.index}"><small>Please upload a file</small></div>
                                 </div>
                                </c:forEach>
                                </c:when>
                                <c:otherwise>
                                <div class="document-num" id="document_no_0">
		                                <a data-file-input="policyowner_passport_0" class="btn secondary-btn-slate btn-file-upload"><c:choose><c:when test="${data['policyowner_passport'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
		                                <input type="file" id="form-nricupload" <c:if test="${data['policyowner_passport'] != null}">class="optional"</c:if> name="policyowner_passport_0" value="${fn:escapeXml(data['policyowner_passport'])}" accept="image/jpeg,image/jpg,application/pdf,image/png" aria-describedby="policyowner_passport_0">
		                                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyowner_passport_0"><small>Please upload a file</small></div>
                                 </div>
                                 </c:otherwise>
                                 </c:choose>
                              </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="add-document blocklink" onclick="addNewDocument('nric-container','policyowner_passport')">
                                  <i class="icon icon-add"></i> Add another document
                                </button>
                              </div>
                            </div>

                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Contact no. of Claimant</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_contact">
                                  <div class="form-group__multi form-group__multi--phone">
                                    <span>
                                      <label for="form-mobilecountrycode" class="visuallyhidden">Country code</label>
                                      <input type="text" id="form-mobilecountrycode" class="form-control number-only-input" name="policyowner_contact_country" value="${not empty data['policyowner_contact_country'] ? fn:escapeXml(data['policyowner_contact_country']) :'+65'}" aria-describedby="policyMobCcAria">
                                    </span>
                                    <span>
                                      <label for="form-mobilenumber" class="visuallyhidden">Number</label>
                                      <input type="text" class="form-control number-only-input" placeholder="" name="policyowner_contact_number" value="${fn:escapeXml(data['policyowner_contact_number'])}" aria-describedby="policyMobAria">
                                    </span>
                                  </div>
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Your phone number is invalid</small></div>
                                  <div class="help-block-multi error-1" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyMobCcAria"><small>Your country code is too long</small></div>
                                  <div class="help-block-multi error-2" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyMobAria"><small>Your phone number is invalid</small></div>
                                </div>
                              </fieldset>
                            </div>

                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="form-email">Email address of Claimant</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group email" data-field-name="policyowner_email">
                                <input type="email" class="form-control" id="form-email" name="policyowner_email" value="${fn:escapeXml(data['policyowner_email'])}" aria-describedby="policyEmailAria">
                                <div id="policyEmailAria">
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Your email is missing</small></div>
                                  <div class="help-block-secondary" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please enter a valid email</small></div>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="form-confirmemail">Confirm email address</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group confirm-email" data-field-name="policyowner_confirm_email" aria-describedby="policyEmailConfAria">
                                <input type="email" id="form-confirmemail" class="form-control" value="${fn:escapeXml(data['policyowner_email'])}">
                                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyEmailConfAria"><small>Email does not match</small></div>
                              </div>
                            </div>

                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Is the Policyowner the Life Insured?</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured">
                                  <div class="newradio--inline">
                                    <input id="id_is_policyowner_yes" type="radio" name="policyowner_life_insured" value="true"
                                           <c:if test="${data['policyowner_life_insured'] == true}">checked</c:if>>
                                    <label for="id_is_policyowner_yes"><span><span></span></span>Yes</label>
                                  </div>
                                  <div class="newradio--inline">
                                    <input id="id_is_policyowner_no" type="radio" name="policyowner_life_insured" value="false" aria-describedby="policyInsuredAria"
                                           <c:if test="${data['policyowner_life_insured'] == false}">checked</c:if>>
                                    <label for="id_is_policyowner_no"><span><span></span></span>No</label>
                                  </div>
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"  id="policyInsuredAria"><small>Please select an option</small></div>
                                </div>
                              </fieldset>
                            </div>

                            <div class="row not_policy_owner_wrapper" <c:if test="${data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true}">style="display: none"</c:if>>
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Name of Life Insured </legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured_name">
                                  <div class="form-group__multi name">
                                    <span>
                                      <label for="form-firstnameinsured" class="visuallyhidden">First Name of Life Insured</label>
                                      <input type="text" id="form-firstnameinsured" class="form-control optional" name="policyowner_life_insured_first_name" placeholder="Given name" value="${fn:escapeXml(data['policyowner_life_insured_first_name'])}">
                                    </span>
                                    <span>
                                      <label for="form-lastnameinsured" class="visuallyhidden"> Last Name of Life Insured</label>
                                      <input type="text" id="form-lastnameinsured" class="form-control optional" name="policyowner_life_insured_last_name" placeholder="Surname" value="${fn:escapeXml(data['policyowner_life_insured_last_name'])}" aria-describedby="insuredNameAria">
                                    </span>
                                  </div>
                                  <div id="insuredNameAria">
                                    <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please enter a name</small></div>
                                    <div class="help-block secondary-error" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Your name contains invalid characters</small></div>
                                  </div>
                                </div>
                              </fieldset>
                            </div>
                            
                            <div class="row document-number-container not_policy_owner_wrapper" <c:if test="${data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true}">style="display: none"</c:if>>
                              <div class="col-xs-12 col-md-5"><label for="form-nricinsured">Please upload a copy of the NRIC/Passport of the Life Insured</label></div>

                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured_passport">
                              <div class="nric-insured-container document-number-container ">
                               <input type="hidden" name="policyowner_life_insured_passport_docs" class="policyowner_life_insured_passport_docs" value="1"/>
                            
                          		<c:choose>
	                            <c:when test="${fn:length(data['policyowner_life_insured_passport'])>0}">
	                              <c:forEach var="doc" varStatus="docNo" items="${data['policyowner_life_insured_passport']}">  
	                                <div  class="document-num " id="document_no_${docNo.index }">
		                                <a data-file-input="policyowner_life_insured_passport_${docNo.index }" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['policyowner_life_insured_passport'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
		                                <input type="file" id="form-nricinsured" <c:if test="${(data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true) || doc != null}">class="optional"</c:if> name="policyowner_life_insured_passport_${docNo.index }" value="${fn:escapeXml(doc)}" accept="image/jpeg,image/jpg,image/png,application/pdf"  aria-describedby="policyowner_life_insured_passport_${docNo.index }">
		                                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyowner_life_insured_passport_${docNo.index }"><small>Please upload a file</small></div>
	                               </div>
	                              </c:forEach>
	                              </c:when>
	                              <c:otherwise>
	                              	<div class="document-num " id="document_no_0">
		                                <a data-file-input="policyowner_life_insured_passport_0" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['policyowner_life_insured_passport'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
		                                <input type="file" id="form-nricinsured" <c:if test="${(data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true) || data['policyowner_life_insured_passport'] != null}">class="optional"</c:if> name="policyowner_life_insured_passport_0" value="${fn:escapeXml(data['policyowner_life_insured_passport'])}" accept="image/jpeg,image/jpg,image/png,application/pdf"  aria-describedby="policyowner_life_insured_passport_0">
		                                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="policyowner_life_insured_passport_0"><small>Please upload a file</small></div>
	                               </div>
	                              </c:otherwise>
                              </c:choose>
                              </div>
                              </div>
                              
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="add-document blocklink" onclick="addNewDocument('nric-insured-container','policyowner_life_insured_passport')">
                                  <i class="icon icon-add"></i> Add another document
                                </button>
                              </div>
                          
                            </div>

                        
                        </div><!-- /.item -->
                      </div> 

                      <div class="col-xs-12 col-md-9 article__leader__main__btnset">
                        <button type="button" class="btn btn-red" data-form-action="next" onclick="validateThenOpenPage()">Continue</button>
                        <button type="button" class="back_btn" onclick="openPage('../eclaims/')">Back</button>
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->
               </div><!-- /.container -->
        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/eclaims1_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function() {
        // $('.not_policy_owner_wrapper').hide();
    });
</script>
