<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">
            
                <div class="eclaims__titler">
                    <h1>eClaims &mdash; Death Due to Accident/Unnatural Causes &mdash; Claim information</h1>
                    <a href="/" class="buynow-exit-button">Exit</a>
                </div>
                <div class="onlineinvestor__breadcrumb">
                    <a href="../1-death-due-to-accident/1">Claimant information</a>
                    <a href="#">Claim information</a>
                    <a href="#" class="disabled">Other information</a>
                    <a href="#" class="disabled">Payment option</a>
                    <a href="#" class="disabled">Declaration</a>
                </div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" data-form-name="eclaims" data-form-type="Death Due To Accident">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                          <div class="onlineinvestor__questionnaire">
                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="date">Date of accident</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown" data-field-name="accident_date_day">
                                <input id="date" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-accident" name="accident_date_day" value="${fn:escapeXml(data['accident_date_day'])}"  aria-describedby="ariaAccidentDate">
                                <div id="ariaAccidentDate">
                                  <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Your date of accident is missing</small></div>
                                  <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Please select a valid date of accident</small></div>
                                  <input type="hidden" name="policy_owner_birthday" value="${application.getPolicyownerBirthday()}">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="form-placeofaccident">Place of accident</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="accident_place">
                                 <input class="form-control" id="form-placeofaccident" type="text" name="accident_place" value="${fn:escapeXml(data['accident_place'])}" aria-describedby="ariaAccidentPlace">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaAccidentPlace"><small>Please enter a value</small></div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="form-detailsofaccident">Details of accident</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="accident_details">
                                <input class="form-control" id="form-detailsofaccident"  type="text" name="accident_details" value="${fn:escapeXml(data['accident_details'])}" aria-describedby="ariaAccidentDetails">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaAccidentDetails"><small>Please enter a value</small></div>
                              </div>
                            </div>
              
                          </div>
                          
                        </div><!-- /.item -->
                        <div class="article__leader__main__btnset col-xs-12 col-sm-9">
                          <button type="button" class="btn btn-red" data-form-action="next" onclick="validateThenOpenPage('')">Continue</button>
                          <button type="button" class="back_btn" onclick="openPage('../1-death-due-to-accident/1')">Back</button>
                        </div>
        
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/eclaims1_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function () {

//        for (var day = 1; day <= 31; day++) {
//            $(".control_day_select").append("<option>" + pad(day, 2) + "</option>");
//        }
//        $(".control_day_select").selectBox('refresh');
//
//        for (var month = 1; month <= 12; month++) {
//            $(".control_month_select").append("<option>" + pad(month, 2) + "</option>");
//        }
//        $(".control_month_select").selectBox('refresh');
//
//        var currYear = (new Date()).getFullYear();
//
//        for (var year = 1970; year <= currYear; year++) {
//            $(".control_year_select").append("<option>" + year + "</option>");
//        }
//        $(".control_year_select").selectBox('refresh');
    });
</script>
