<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-homepage"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor eclaims__page">
            <div class="container">

              <div class="eclaims__titler">
                <h1>Documents Required</h1>
                <a href="/" class="buynow-exit-button absolute">Exit</a>
              </div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" data-form-name="eclaims" data-form-type="Death Due To Accident">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9">
                          <p><strong>Please note that all documents submitted must be in English. Any non-English documents are to be translated to English by a certified translator.</strong></p>
                          
                          <ol style="list-style-type: decimal;">
                            <li>Death Certificate</li> 
                            <li>Police investigation report, Post-Mortem or Toxicology report, Coroner&rsquo;s Inquest report </li> 
                            <li>NRIC / Passport of all claimant(s) </li> 
                            <li>
                              Proof of the Claimant(s)&rsquo;s residential address, if address is not stated on Identification Documents    
                              <br>
                              (A utility/telco bill; a bank statement; any correspondence from a government body issued within the last 3 months)
                            </li> 
                            <li>
                              Proof of relationship or rights to administer deceased&rsquo;s estate as below:
                              <table>
                                <tr>
                                  <th>Spouse</th>
                                  <td>Marriage Certificate</td>
                                </tr>
                                <tr>
                                  <th>Children</th>
                                  <td>Birth Certificate of children</td>
                                </tr>
                                <tr>
                                  <th>Parents</th>
                                  <td>Birth Certificate of deceased</td>
                                </tr>
                                <tr>
                                  <th>Siblings</th>
                                  <td>Birth Certificate of deceased &amp; Birth Certificate of siblings</td>
                                </tr>
                                <tr>
                                  <th>Executor</th>
                                  <td>Letter of Probate</td>
                                </tr>
                                <tr>
                                  <th>Administrator</th>
                                  <td>Letter of Administration</td>
                                </tr>
                              </table>
                            </li> 
                            <li>Grant of Probate / Letter of Administration (where applicable)</li> 
                            <li>
                              For overseas death:     
                              <ol style="list-style-type: lower-roman;">
                                <li>
                                  For Singaporeans who passed away overseas, death certificate and all documents are to be certified by Singapore Embassy in the country of death.
                                  <br>
                                  For non-Singaporeans, death certificate and all documents are to be certified by the Notary Public of the country.
                                </li>
                                <li>For Singaporean / Permanent Resident, letter from ICA (Singapore Immigration and Checkpoint) confirming receipt of the Singapore IC, passport and overseas death certificate.</li>
                              </ol>  
                            </li>
                          </ol>
                          <p><strong>Upon approval of the claim, we would require the certified true copies of the above documents. You may mail the certified true documents (by your lawyer or the notary public) to us or present the original documents to our HSBC staff for verification before collection of the claim payment.</strong></p>

                          
                        </div><!-- /.item -->
                        <div class="article__leader__main__btnset col-xs-12 col-sm-10 ">
                          <button type="button" class="btn btn-red" data-form-action="next" onclick="submitForm()">Continue</button>
                          <button type="button" class="back_btn" onclick="openPage('../1-death-due-to-accident')">Back</button>
                        </div>
        
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->

            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/eclaims1_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function() {
        $('.not_policy_owner_wrapper').hide();
    });
</script>