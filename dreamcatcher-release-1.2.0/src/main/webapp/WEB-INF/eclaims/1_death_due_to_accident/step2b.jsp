<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">
            
              <div class="eclaims__titler">
                <h1>eClaims &mdash; Death Due to Accident/Unnatural Causes &mdash; Claim information</h1>
                <a href="/" class="buynow-exit-button">Exit</a>
              </div>
                <div class="onlineinvestor__breadcrumb">
                    <a href="../1-death-due-to-accident/1">Claimant information</a>
                    <a href="#">Claim information</a>
                    <a href="#" class="disabled">Other information</a>
                    <a href="#" class="disabled">Payment option</a>
                    <a href="#" class="disabled">Declaration</a>
                </div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="eclaims" data-form-type="Death Due To Accident">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                   
                          <div class="onlineinvestor__questionnaire">
				
                            <div class="row">

                              <div class="col-xs-12 col-md-5"><label for="death_certificate_upload">Please upload a copy of the Death Certificate</label></div>
         					 <!-- multiple fields start here -->    
         					  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="death_certificate">
                              <div class="death-certificate-container document-number-container ">
                              <input type="hidden" name="death_certificate_docs" class="death_certificate_docs" value="1"/>              
                              <c:choose>
                              	<c:when test="${fn:length(data['death_certificate'])>0}">
                                    <c:forEach var="doc" varStatus="docNo" items="${data['death_certificate']}">
										<div class="document-num" id="document_no_${docNo.index}">
											<a data-file-input="death_certificate_${docNo.index}" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${doc == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
		                                      <input type="file" name="death_certificate_${docNo.index}" <c:if test="${doc != null}">class="optional"</c:if> value="${doc}" accept="image/jpeg,image/jpg,image/png,application/pdf" id="death_certificate_upload"  aria-describedby="death_certificate_${docNo.index}">
		                                      <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaUpload1"><small>Please upload a file</small></div>
                                        </div>
                                    </c:forEach>
                                   </c:when>
                                   <c:otherwise>
                                    <div class="document-num" id="document_no_0">
                                      <a data-file-input="death_certificate_0" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['death_certificate'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                      <input type="file" name="death_certificate_0" <c:if test="${data['death_certificate'] != null}">class="optional"</c:if>value="${data['death_certificate']}"  accept="image/jpeg,image/jpg,image/png,application/pdf"  id="death_certificate_upload"  aria-describedby="death_certificate_0">
                                        <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="death_certificate_0"><small>Please upload a file</small></div>
                                    </div>
                                    </c:otherwise>
                                   </c:choose>
								</div>

                            </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="blocklink add-document" onclick="addNewDocument('death-certificate-container','death_certificate')">
                                  <i class="icon icon-add"></i> Add another document
                                </button>
                              </div>
                            </div>
                             <!-- multiple fields ends here -->       
                              
                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="police_investigation_upload">Please upload a copy of the Police Investigation report</label></div> 
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="police_investigation">
                              
                              <!-- multiple fields start here -->
                              <div class="police-investigation-container document-number-container ">
                              <input type="hidden" name="police_investigation_docs" class="police_investigation_docs" value="1"/>                 
                              <c:choose>
                              	<c:when test="${fn:length(data['police_investigation'])>0}">
                                    <c:forEach var="doc" varStatus="docNo" items="${data['police_investigation']}">
		                               <div class="document-num" id="document_no_${docNo.index}">
										<a data-file-input="police_investigation_${docNo.index}" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${doc == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
		                                <input type="file" name="police_investigation_${docNo.index}" <c:if test="${doc!=null}">class="optional"</c:if> value="${doc}"  accept="image/jpeg,image/jpg,image/png,application/pdf" id="police_investigation_upload" aria-describedby="police_investigation_${docNo.index}">
		                                 <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="police_investigation_${docNo.index}"><small>Please upload a file</small></div>
                                        </div>
                                    </c:forEach>
                                 </c:when>
                                 <c:otherwise>
                                    <div class="document-num" id="document_no_0">
                                      <a data-file-input="police_investigation_0" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span>Upload</a>
                                      <input type="file" name="police_investigation_0" <c:if test="${data['police_investigation'] != null}">class="optional"</c:if> value="${data['police_investigation']}"  accept="image/jpeg,image/jpg,image/png,application/pdf" id="police_investigation_upload" aria-describedby="police_investigation_0">
                                       <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="police_investigation_0"><small>Please upload a file</small></div>
                                    </div>
                                 </c:otherwise>
                             </c:choose>
							</div>
                            </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="blocklink add-document" onclick="addNewDocument('police-investigation-container','police_investigation')">
                                  <i class="icon icon-add"></i> Add another document
                                </button>

                              </div>
                            </div>
                            <!-- multiple fields ends here --> 
                        
                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Was a post-mortem, autopsy done or Coroner's Inquest held?</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="post_mortem">
                                  <div class="newradio--inline">
                                    <input id="id_post_mortem_yes" type="radio" name="post_mortem" value="true" <c:if test="${data['post_mortem'] == true}">checked</c:if>>
                                    <label for="id_post_mortem_yes"><span><span></span></span>Yes</label>
                                  </div>
                                  <div class="newradio--inline">
                                    <input id="id_post_mortem_no" type="radio" name="post_mortem" value="false" <c:if test="${data['post_mortem'] == false}">checked</c:if> aria-describedby="ariaAutopsy">
                                    <label for="id_post_mortem_no"><span><span></span></span>No</label>
                                  </div>
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAutopsy"><small>Please select an option</small></div>
                                </div>
                              </fieldset>
                            </div>
                            
                            <div class="row post_mortem_wrapper" <c:if test="${data['post_mortem'] == null || data['post_mortem'] == false}">style="display: none"</c:if>>
                              <div class="col-xs-12 col-md-5">Please upload a copy of the Post-Mortem or Toxicology report or Coroner's Inquest report</div>
                              
                            <!-- multiple fields start here -->  
                             <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="post_mortem_document">
                              	<div class="post-mortem-document-container document-number-container ">
                              		<input type="hidden" name="post_mortem_document_docs" class="post_mortem_document_docs" value="1"/>
                               		<c:choose>
	                            		<c:when test="${fn:length(data['post_mortem_document'])>0}">
	                              			<c:forEach var="doc" varStatus="docNo" items="${data['post_mortem_document']}">   
                                  			<div class="document-num" id="document_no_${docNo.index}"> 
                                   				<a data-file-input="post_mortem_document_${docNo.index}" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${doc == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                				<input type="file" <c:if test="${(data['post_mortem'] == null || data['post_mortem'] == false) || doc != null}">class="optional"</c:if> name="post_mortem_document_${docNo.index}" value="${fn:escapeXml(doc)}"  accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="post_mortem_document_${docNo.index}">
                                				 <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="post_mortem_document_${docNo.index}"><small>Please upload a file</small></div>
                                			</div>
                                			</c:forEach>
                                		</c:when>
                                	<c:otherwise>
                                	<div class="document-num" id="document_no_0">
                                		<a data-file-input="post_mortem_document_0" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['post_mortem_document'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                		<input type="file" <c:if test="${(data['post_mortem'] == null || data['post_mortem'] == false) || data['post_mortem_document'] != null}">class="optional"</c:if> name="post_mortem_document_0" value="${fn:escapeXml(data['post_mortem_document'])}"  accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="post_mortem_document_0">
                                		 <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="post_mortem_document_0"><small>Please upload a file</small></div>
                                  	</div>
                                 	</c:otherwise>
                         		</c:choose>
                              </div>
                            </div>
                           </div>
                           <div class="row post_mortem_wrapper"<c:if test="${data['post_mortem'] == null || data['post_mortem'] == false}">style="display: none"</c:if>>
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="blocklink add-document" onclick="addNewDocument('post-mortem-document-container','post_mortem_document')">
                                  <i class="icon icon-add"></i> Add another document
                                </button>
                              </div>
                            </div>
                             <!-- multiple fields ends here --> 

                           <!-- </div> --> 
                            
                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Was the death due to suicide?</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="suicide">
                                  <div class="newradio--inline">
                                    <input id="id_suicide_yes" type="radio" name="suicide" value="true" <c:if test="${data['suicide'] == true}">checked</c:if>>
                                    <label for="id_suicide_yes"><span><span></span></span>Yes</label>
                                  </div>
                                  <div class="newradio--inline">
                                    <input id="id_suicide_no" type="radio" name="suicide" value="false" <c:if test="${data['suicide'] == false}">checked</c:if> aria-describedby="ariaSuicide">
                                    <label for="id_suicide_no"><span><span></span></span>No</label>
                                  </div>
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaSuicide"><small>Please select an option</small></div>
                                </div>
                              </fieldset>
                            </div>

                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Did the Deceased left a Will?</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="left_a_will">
                                  <div class="newradio--inline">
                                    <input id="id_will_yes" type="radio" name="left_a_will" value="true" <c:if test="${data['left_a_will'] == true}">checked</c:if>>
                                    <label for="id_will_yes"><span><span></span></span>Yes</label>
                                  </div>
                                  <div class="newradio--inline">
                                    <input id="id_will_no" type="radio" name="left_a_will" value="false" <c:if test="${data['left_a_will'] == false}">checked</c:if> aria-describedby="ariaWill">
                                    <label for="id_will_no"><span><span></span></span>No</label>
                                  </div>
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaWill"><small>Please select an option</small></div>
                                </div>
                              </fieldset>
                            </div>

                            <div class="row will_wrapper" <c:if test="${data['left_a_will'] == null || data['left_a_will'] == false}">style="display: none"</c:if>>
                            	<div class="col-xs-12 col-md-5">Please upload a copy of the Will</div>
    
                            	<!-- multiple fields start here -->
                            	<div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="left_a_will_document">
                              		<div class="left-a-will-document-container document-number-container ">
                              			<input type="hidden" name="left_a_will_document_docs" class="left_a_will_document_docs" value="1"/>
			                            <c:choose>
				                        	<c:when test="${fn:length(data['left_a_will_document'])>0}">
				                       			<c:forEach var="doc" varStatus="docNo" items="${data['left_a_will_document']}">   
                           					 		<div class="document-num" id="document_no_${docNo.index}">
							                        	<a data-file-input="left_a_will_document_${docNo.index}" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${doc == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
							                            <input type="file" <c:if test="${(data['left_a_will'] == null || data['left_a_will'] == false) || doc != null}">class="optional"</c:if> name="left_a_will_document_${docNo.index}" value="${fn:escapeXml(doc)}"  accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="left_a_will_document_${docNo.index}">
							                             <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="left_a_will_document_${docNo.index}"><small>Please upload a file</small></div>
							                        </div>
							                        </c:forEach>
                                			</c:when>
                                			<c:otherwise>
                                				<div class="document-num" id="document_no_0">
                                					<a data-file-input="left_a_will_document_0" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['left_a_will_document'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
					                                <input type="file" <c:if test="${(data['left_a_will'] == null || data['left_a_will'] == false) || data['left_a_will_document'] != null}">class="optional"</c:if> name="left_a_will_document_0" value="${fn:escapeXml(data['left_a_will_document'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="left_a_will_document_0">
					                                 <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="left_a_will_document_0"><small>Please upload a file</small></div>
					                           </div>
                                 			</c:otherwise>
                                 		</c:choose>
                                 	</div>
                                </div>
                              </div>
                             <div class="row will_wrapper" <c:if test="${data['left_a_will'] == null || data['left_a_will'] == false}">style="display: none"</c:if>>
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="blocklink add-document" onclick="addNewDocument('left-a-will-document-container','left_a_will_document')">
                                  <i class="icon icon-add"></i> Add another document
                                </button>
                              </div>
                            </div>
                             <!-- multiple fields ends here --> 

                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Did the death occur in Singapore?</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="occur_in_singapore">
                                  <div class="newradio--inline">
                                    <input id="id_local_death_yes" type="radio" name="occur_in_singapore" value="true" <c:if test="${data['occur_in_singapore'] == true}">checked</c:if>>
                                    <label for="id_local_death_yes"><span><span></span></span>Yes</label>
                                  </div>
                                  <div class="newradio--inline">
                                    <input id="id_local_death_no" type="radio" name="occur_in_singapore" value="false" <c:if test="${data['occur_in_singapore'] == false}">checked</c:if> aria-describedby="ariaDeathSg">
                                    <label for="id_local_death_no"><span><span></span></span>No</label>
                                  </div>
                                  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaDeathSg"><small>Please select an option</small></div>
                                </div>
                              </fieldset>
                            </div>

                    
                            <div class="row local_death_wrapper" <c:if test="${data['occur_in_singapore'] == null || data['occur_in_singapore'] == true}">style="display: none"</c:if>>
                              <div class="col-xs-12 col-md-5">Please upload a copy of the letter from ICA confirming receipt of the Singapore IC, Passport and overseas Death Certificate</div>

                               <!-- multiple fields start here -->                  
                        		<div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="occur_in_singapore_document">
                              		<div class="occur-in-singapore-document document-number-container ">
                             			<input type="hidden" name="occur_in_singapore_document_docs" class="occur_in_singapore_document_docs" value="1"/>
                              			<c:choose>
	                            			<c:when test="${fn:length(data['occur_in_singapore_document'])>0}">
	                              				<c:forEach var="doc" varStatus="docNo" items="${data['occur_in_singapore_document']}">   
                            						<div  class="document-num" id="document_no_${docNo.index}">
                            					  		<a data-file-input="occur_in_singapore_document_${docNo.index}" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${doc == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                				  		<input type="file" <c:if test="${(data['occur_in_singapore'] == null || data['occur_in_singapore'] == true) || doc != null}">class="optional"</c:if> name="occur_in_singapore_document_${docNo.index}" value="${fn:escapeXml(doc)}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="occur_in_singapore_document_${docNo.index}">
                                				 		 <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="occur_in_singapore_document_${docNo.index}"><small>Please upload a file</small></div>
                                				  	</div>
                                				  </c:forEach>  
                                			</c:when>
                                			<c:otherwise>
                                				<div class="document-num" id="document_no_0">
                                					<a data-file-input="occur_in_singapore_document_0" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['occur_in_singapore_document'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                					<input type="file" <c:if test="${(data['occur_in_singapore'] == null || data['occur_in_singapore'] == true) || data['occur_in_singapore_document'] != null}">class="optional"</c:if> name="occur_in_singapore_document_0" value="${fn:escapeXml(data['occur_in_singapore_document'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="occur_in_singapore_document_0">
                                					 <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="occur_in_singapore_document_0"><small>Please upload a file</small></div>
                               					</div>
                                 			</c:otherwise>
                                 		</c:choose>
                                 	</div>
                                </div>
                                </div>
                                <div class="row local_death_wrapper" <c:if test="${data['occur_in_singapore'] == null || data['occur_in_singapore'] == true}">style="display: none"</c:if>>		
                              		<div class="col-xs-12 col-md-6 container-fluid">
                                		<button type="button" class="blocklink add-document" onclick="addNewDocument('occur-in-singapore-document','occur_in_singapore_document')">
                                  			<i class="icon icon-add"></i> Add another document
                                		</button>
                              		</div>
                            	</div>
                             <!-- multiple fields ends here -->   

                          </div> 
                        </div><!-- /.item -->
                        <div class="article__leader__main__btnset col-xs-12 col-sm-9">
                          <button type="button" class="btn btn-red" data-form-action="next" onclick="validateThenOpenPage()">Continue</button>
                          <button type="button" class="back_btn" onclick="openPage('../1-death-due-to-accident/2')">Back</button>
                        </div>
                      
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/1_death_due_to_accident/includes/eclaims1_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function(){

//        $('.post_mortem_wrapper').hide();
//        $('.will_wrapper').hide();
//        $('.local_death_wrapper').hide();
    });
</script>
