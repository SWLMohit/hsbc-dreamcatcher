<div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
        <a href="/">Home</a>
        <a href="/eclaims/">eClaims</a>
        <a href="/eclaims/1-death-due-to-accident" style="text-decoration: underline;" aria-current="page">Death Due to Accident/Unnatural Causes</a>
    </nav>
</div>
