<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h2>
    Please note that any bank charges may be deducted from the amount payable (if applicable)
</h2>

<div class="onlineinvestor__questionnaire">
    <div class="row">
        <div class="col-xs-12 col-md-5">Name of Bank </div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
            <input class="form-control" type="text" name="bank_name" value="${fn:escapeXml(data['bank_name'])}">
            <div class="help-block"><small>Please enter a value</small></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Account Number / IBAN</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
            <input class="form-control" type="text" name="account_number" value="${fn:escapeXml(data['account_number'])}">
            <div class="help-block"><small>Please enter a value</small></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Country of Bank</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
            <div class="form-select_wrap">
                <select class="form-control bank_country_select" name="bank_country" value="${fn:escapeXml(data['bank_country'])}">
                    <option disabled selected>Please select&hellip;</option>
                    <c:forEach var="country" varStatus="arr_count" items="${formData['country_list']}" >
                        <c:choose>
                            <c:when test="${country == 'None of the above'}">
                                <option <c:if test="${data['bank_country'] == 'none_of_above'}">selected</c:if> value="none_of_above">${country}</option>
                            </c:when>
                            <c:otherwise>
                                <option <c:if test="${data['bank_country'] == country}">selected</c:if>>${country}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select></div>
            <div class="help-block"><small>Please select an option</small></div>
        </div>
    </div>


    <div class="bank_address_wrapper" <c:if test="${data['bank_country'] == null || data['bank_country'] == 'Singapore'}">style="display: none;" </c:if>>

        <div class="row">
            <div class="col-xs-12 col-md-5">Address of Bank</div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
                <p>City</p>
                <input class="form-control <c:if test="${data['bank_city'] == null || data['bank_city'] == false}">optional</c:if>" type="text" placeholder="City" name="bank_city" data="${bankrupt['bank_city']}">
                <div class="help-block"><small>Please select an option</small></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group">
                <p>Postal Code</p>
                <input type="text" class="form-control optional auto-width" name="bank_postal_code" value="${fn:escapeXml(data['bank_postal_code'])}">
                <div class="help-block"><small>Please enter a value</small></div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group">
                <p>Building/Blk Number</p>
                <input type="text" class="form-control optional" name="bank_building_number" value="${fn:escapeXml(data['bank_building_number'])}">
                <div class="help-block"><small>Please enter a value</small></div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group unit-number">
                <fieldset>
                    <legend>Unit number <small>(optional)</small></legend>
                    <div class="form-group__multi ">
                        <span><input type="text" class="form-control number-only-input optional" name="bank_unit_number_1" value="${fn:escapeXml(data['bank_unit_number_1'])}"></span>
                        <span><input type="text" class="form-control number-only-input optional" name="bank_unit_number_2" value="${fn:escapeXml(data['bank_unit_number_2'])}"></span>
                    </div>
                </fieldset>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group street">
                <p>Street</p>
                <input type="text" class="form-control bottom_margined optional" placeholder=""  name="bank_street_1" value="${fn:escapeXml(data['bank_street_1'])}">
                <div class="help-block"><small>Please enter a value</small></div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group street">
                <p>Street line 2 <small>(optional)</small></p>
                <input type="text" class="form-control optional" placeholder="" name="bank_street_2" value="${fn:escapeXml(data['bank_street_2'])}">
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group building-name">
                <p>Building name <small>(optional)</small></p>
                <input type="text" class="form-control optional" placeholder="" name="bank_building_name" value="${fn:escapeXml(data['bank_building_name'])}">
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-5">Swift Code / Swift Address</div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
                <input class="form-control optional" type="text" name="bank_swift_code" value="${fn:escapeXml(data['bank_swift_code'])}">
                <div class="help-block"><small>Please enter a value</small></div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Please submit a valid copy of your bank book / statement for account verification</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
        <!-- multiple fields starts here -->
        	<div class="account-document-container document-number-container ">
            	<input type="hidden" name="account_document_docs" class="account_document_docs" value="1"/>
                <c:choose>
	            	<c:when test="${fn:length(data['account_document'])>0}">
	                   <c:forEach var="doc" varStatus="docNo" items="${data['account_document']}">  
	                   		<div class="document-num" id="document_no_${docNo.index}"> 
            						<a data-file-input="account_document_${docNo.index}" class="btn secondary-btn-slate btn-file-upload"><c:choose><c:when test="${doc == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
            						<input type="file" name="account_document_${docNo.index}" <c:if test="${doc != null}">class="optional"</c:if> value="${fn:escapeXml(doc)}" accept=".jpg,.jpeg">
            						<div class="help-block"><small>Please upload a file</small></div>
        					</div>
    					</c:forEach>
                    </c:when>
                    <c:otherwise>
    					<div class="document-num" id="document_no_0">
    						<a data-file-input="account_document_0" class="btn secondary-btn-slate btn-file-upload"><c:choose><c:when test="${data['account_document'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
            				<input type="file" name="account_document_0" <c:if test="${data['account_document'] != null}">class="optional"</c:if> value="${fn:escapeXml(data['account_document'])}" accept=".jpg,.jpeg">
            				<div class="help-block"><small>Please upload a file</small></div>			
    					</div>
    				</c:otherwise>
               	</c:choose>
            </div>
		</div>
	</div>
	<div class="row">
    	<div class="col-xs-12 col-md-6 container-fluid">
        	<button type="button" class="blocklink add-document" onclick="addNewDocument('account-document-container','account_document')">
            	<i class="icon icon-add"></i> Add another document
           	</button>
         </div>
    </div>
<!-- multiple fields ends here -->