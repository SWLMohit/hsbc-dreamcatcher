<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="onlineinvestor__questionnaire">
    <div class="policy-number-container row">
        <div class="col-xs-12 col-md-5">Policy Number(s)</div>
        <c:choose>
            <c:when test="${data['policy_number_list'] != null}">
                <c:forEach var="policy_number" varStatus="arr_count" items="${data['policy_number_list']}" >
                    <div class="policy-num col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="policy_number">
                        <input class="form-control alphanum-input" type="text" id="id_policy_${arr_count.count}" name="policy_number" value="${fn:escapeXml(policy_number)}" aria-describedby="ariaPolicyNum">
                        <div id="ariaPolicyNum">
                            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a valid policy number</small></div>
                            <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a unique policy number</small></div>
                        </div>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div class="policy-num col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policy_number">
                    <input class="form-control alphanum-input" type="text" id="id_policy_1" name="policy_number" aria-describedby="ariaPolicyNum2">
                    <div id="ariaPolicyNum2">
                        <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaPolicyNum2"><small>Please enter a valid policy number</small></div>
                        <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a unique policy number</small></div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>

    </div>
    <!-- Add More Something Button -->
    <div class="row">
        <div class="col-xs-12 col-md-6 container-fluid">
            <button type="button" class="blocklink" onclick="addPolicyNum()">
                <i class="icon icon-add"></i> Add another policy number
            </button>
        </div>
    </div>
    <div class="space20"></div>
    <!-- Add More Something Button -->

    <div class="row">
        <fieldset>
            <div class="col-xs-12 col-md-5"><legend>Name of Policyowner</legend></div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_name">
                <div class="form-group__multi name">
                    <span><input type="text" class="form-control" name="policyowner_first_name" placeholder="Given name" value="${fn:escapeXml(data['policyowner_first_name'])}" aria-describedby="ariaName"></span>
                    <span><input type="text" class="form-control" name="policyowner_last_name" placeholder="Surname" value="${fn:escapeXml(data['policyowner_last_name'])}" aria-describedby="ariaName"></span>
                </div>
                <div  id="ariaName">
                    <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a name</small></div>
                    <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Your name contains invalid characters</small></div>
                </div>
            </div>
        </fieldset>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">NRIC of  Claimant</div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group nric" data-field-name="policy_owner_nric">
                <input class="form-control alphanum-input" type="text" id="nric" name="policy_owner_nric" value="${fn:escapeXml(data['policy_owner_nric'])}" aria-describedby="ariaNRIC">
                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaNRIC"><small>Please enter a valid NRIC number</small></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5"><label for="date">Date of Birth of Claimant</label></div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown" data-field-name="policy_owner_birth_day">
            <input id="date" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-dob" name="policy_owner_birth_day" value="${fn:escapeXml(data['policy_owner_birth_day'])}" aria-describedby="ariaDOB">
            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaDOB">
                <small>Please enter a valid date</small>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Please upload a copy of the NRIC / Passport of Claimant</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_passport">
            <a data-file-input="policyowner_passport" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['policyowner_passport'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
            <input type="file" <c:if test="${data['policyowner_passport'] != null}">class="optional"</c:if> name="policyowner_passport" value="${fn:escapeXml(data['policyowner_passport'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="ariaUpload1">
            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaUpload1"><small>Please upload a file</small></div>
        </div>
    </div>

    <div class="row">
        <fieldset>
            <div class="col-xs-12 col-md-5"><legend>Contact no. of Claimant</legend></div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_contact">
                <div class="form-group__multi form-group__multi--phone">
                    <span><input type="text" class="form-control number-only-input" name="policyowner_contact_country" value="${not empty data['policyowner_contact_country'] ? fn:escapeXml(data['policyowner_contact_country']) :'+65'}" aria-describedby="ariaContact"></span>
                    <span><input type="text" class="form-control number-only-input" placeholder="" name="policyowner_contact_number" value="${fn:escapeXml(data['policyowner_contact_number'])}" aria-describedby="ariaContactNo"></span>
                </div>
                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Your phone number is invalid</small></div>
                <div class="help-block-multi error-1" role="alert" aria-atomic="true" aria-label="Error" id="ariaContact"><small>Your country code is too long</small></div>
                <div class="help-block-multi error-2" role="alert" aria-atomic="true" aria-label="Error" id="ariaContactNo"><small>Your phone number is invalid</small></div>
            </div>
        </fieldset>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Email address of Claimant</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group email" data-field-name="policyowner_email">
            <input type="email" class="form-control" name="policyowner_email" value="${fn:escapeXml(data['policyowner_email'])}" aria-describedby="ariaEmail">
            <div id="ariaEmail">
                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Your email is missing</small></div>
                <div class="help-block-secondary" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a valid email</small></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Confirm email address</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group confirm-email" data-field-name="policyowner_confirm_email">
            <input type="email" class="form-control" value="${fn:escapeXml(data['policyowner_email'])}" aria-describedby="ariaEmailConf">
            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaEmailConf"><small>Email does not match</small></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Mailing Address of Claimant</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_city">
            <p>Country</p>
            <div class="form-select_wrap">
                <select class="form-control control_country_select" name="policyowner_city" aria-describedby="ariaAddress">
                    <option value="" disabled selected>Choose one&hellip;</option>

                    <c:forEach var="country" varStatus="arr_count" items="${ formData['country_list']}" >

                        <c:choose>
                            <c:when test="${country == 'None of the above'}">
                                <option <c:if test="${data['policyowner_city'] == 'none_of_above'}">selected</c:if> value="none_of_above">${country}</option>
                            </c:when>
                            <c:otherwise>
                                <option <c:if test="${data['policyowner_city'] == country}">selected</c:if>>${country}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select></div>
            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaAddress"><small>Please choose an option</small></div>
        </div>

        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="policyowner_postal_code">
            <p>Postal Code</p>
            <input type="text" class="form-control auto-width" data-lookup="postalCode" name="policyowner_postal_code" value="${fn:escapeXml(data['policyowner_postal_code'])}" aria-describedby="ariaPostCode">
            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaPostCode"><small>Please enter a value</small></div>
        </div>

        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="policyowner_building_number">
            <p>Building/Blk Number</p>
            <input type="text" class="form-control" data-lookup="buildingNumber" name="policyowner_building_number" value="${fn:escapeXml(data['policyowner_building_number'])}" aria-describedby="ariaBuilingNum">
            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaBuilingNum"><small>Please enter a value</small></div>
        </div>
   
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="policyowner_unit_number">
            <fieldset>
                <legend style="margin-bottom: 1em;">Unit number <small>(optional)</small></legend>
                <div class="form-group__multi">
                    <span><input type="text" class="form-control number-only-input optional" name="policyowner_unit_number_1" value="${fn:escapeXml(data['policyowner_unit_number_1'])}"></span>
                    <span><input type="text" class="form-control number-only-input optional" name="policyowner_unit_number_2" value="${fn:escapeXml(data['policyowner_unit_number_2'])}"></span>
                </div>
            </fieldset>
        </div>

        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="policyowner_street_1">
            <p>Street</p>
            <input type="text" class="form-control bottom_margined" data-lookup="streetName" placeholder="" name="policyowner_street_1" value="${fn:escapeXml(data['policyowner_street_1'])}" aria-describedby="ariaStreet">
            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaStreet"><small>Please enter a value</small></div>
        </div>

        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="policyowner_street_2">
            <p>Street line 2 <small>(optional)</small></p>
            <input type="text" class="form-control optional" placeholder="" name="policyowner_street_2" value="${fn:escapeXml(data['policyowner_street_2'])}">
        </div>

        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="policyowner_building_name">
            <p>Building name
                <small>(optional)</small>
            </p>
            <input type="text" class="form-control optional" placeholder="" data-lookup="buildingName" name="policyowner_building_name" value="${fn:escapeXml(data['policyowner_building_name'])}">
        </div>
    </div>
    <div class="row">
        <fieldset>
            <div class="col-xs-12 col-md-5"><legend>Is the Policyowner the Life Insured?</legend></div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured">
                <div class="newradio--inline">
                    <input id="id_is_policyowner_yes" type="radio" name="policyowner_life_insured" value="true"
                       <c:if test="${data['policyowner_life_insured'] == true}">checked</c:if>>
                    <label for="id_is_policyowner_yes"><span><span></span></span>Yes</label>
                </div>
                <div class="newradio--inline">
                    <input id="id_is_policyowner_no" type="radio" name="policyowner_life_insured" value="false"
                       <c:if test="${data['policyowner_life_insured'] == false}">checked</c:if> aria-describedby="ariaLifeInsured">
                    <label for="id_is_policyowner_no"><span><span></span></span>No</label>
                </div>
                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaLifeInsured"><small>Please select an option</small></div>
            </div>
        </fieldset>
    </div>

    <div class="row not_policy_owner_wrapper" <c:if test="${data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true}">style="display: none"</c:if>>
        <fieldset>
            <div class="col-xs-12 col-md-5"><legend>Name of Life Insured </legend></div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured_name">
                <div class="form-group__multi name">
                    <span><input type="text" class="form-control optional" name="policyowner_life_insured_first_name" placeholder="Given name" value="${fn:escapeXml(data['policyowner_life_insured_first_name'])}"></span>
                    <span><input type="text" class="form-control optional" name="policyowner_life_insured_last_name" placeholder="Surname" value="${fn:escapeXml(data['policyowner_life_insured_last_name'])}" aria-describedby="ariaLifeInsuredName"></span>
                </div>
                <div id="ariaLifeInsuredName">
                    <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a name</small></div>
                    <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Your name contains invalid characters</small></div>
                </div>
            </div>
        </fieldset>
    </div>

    <div class="row not_policy_owner_wrapper" <c:if test="${data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true}">style="display: none"</c:if>>
        <div class="col-xs-12 col-md-5">Please upload a copy of the NRIC/Passport of the Life Insured</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured_passport">

            <a data-file-input="policyowner_life_insured_passport" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['policyowner_life_insured_passport'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
            <input type="file" <c:if test="${(data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true) || data['policyowner_life_insured_passport'] != null}">class="optional"</c:if> name="policyowner_life_insured_passport" value="${fn:escapeXml(data['policyowner_life_insured_passport'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="ariaUpload2">
            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaUpload2"><small>Please upload a file</small></div>

        </div>
    </div>
</div>
