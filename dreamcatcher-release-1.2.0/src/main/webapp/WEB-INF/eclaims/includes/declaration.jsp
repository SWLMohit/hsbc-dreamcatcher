<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h2>Declaration &amp; Authorisation:</h2>
<ol>
    <li>
        I declare that all the information given by me in this claim form is, to the best of my knowledge and belief, true, complete and accurate, and that no material information has been withheld nor is any relevant circumstances omitted.
    </li>
    <li>
        I authorise HSBC Insurance (Singapore) Pte. Limited (the "Company") to seek medical information from any doctor who, at any time, has attended to me/the life insured concerning anything that affects my/the life insured's health, seek information from any insurance offices to which an insurance proposal has been made, seek information from any other sources (including employer, government institution, bank or other organisation or person) and disclose information including medical information about me/the life insured to other insurers, reinsurers or other third parties assisting with this claim.
    </li>
    <li>
        I agree that all written statements and affidavits, of all doctors who has attended to me/the life insured or any other sources, and all other papers called for by instructions hereon shall constitute and they are hereby made a part of these Proofs of medical evidence and further agree that the furnishing of this form or any other forms supplemental thereof, by the Company shall not constitute nor be considered an admission by it that there is an insurance in force on the life in question, nor a waiver of any rights or defences.
    </li>
    <li>
        I understand and agree that the Company shall have full access to the information above and a photographic copy of this authorisation shall be as valid as the original.
    </li>
    <li>
        I understand and acknowledge that the personal data which I have submitted is being collected for the purposes stated in the HSBC Data Protection Policy (<a href="http://www.insuranceonline.hsbc.com.sg/privacy-policy/" target="_blank">http://www.insuranceonline.hsbc.com.sg/privacy-policy/</a>) and consent to the collection, use and disclosure of my personal data accordingly.
    </li>
</ol>