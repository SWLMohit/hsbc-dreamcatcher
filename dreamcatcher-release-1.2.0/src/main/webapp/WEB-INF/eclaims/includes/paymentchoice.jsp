<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="row onlineinvestor__questionnaire">
    <fieldset>
        <div class="col-xs-12 col-md-5"><legend>Please indicate the option you wish to receive your payment</legend></div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="payment">
            <div class="newradio">
                <input id="id_payment_mail" type="radio" name="payment" value="1" data-show="#radio_hint_1" data-hide=".radio_hint" <c:if test="${data['payment'] == 1}">checked</c:if>>
                <label for="id_payment_mail">
                    <span><span></span></span> Cheque via postage mail
                </label>
            </div>
            <div class="newradio">
                <input id="id_payment_cheque" type="radio" name="payment" value="2" data-show="#radio_hint_2" data-hide=".radio_hint" <c:if test="${data['payment'] == 2}">checked</c:if>>
                <label for="id_payment_cheque">
                    <span><span></span></span> Self-collect cheque at Customer Service Centre <small>(21 Collyer Quay #02-01 Singapore 049320)</small>
                </label>
            </div>
            <div class="newradio">
                <input id="id_payment_bank" type="radio" name="payment" value="3" data-show="#radio_hint_3" data-hide=".radio_hint" <c:if test="${data['payment'] == 3}">checked</c:if> aria-describedby="ariaPaymentChoice">
                <label for="id_payment_bank">
                    <span><span></span></span> Transfer into my bank account
                </label>
            </div>
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaPaymentChoice"><small>Please select an option</small></div>
        </div>
    </fieldset>
</div>
