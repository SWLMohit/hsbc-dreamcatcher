<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="onlineinvestor__questionnaire">
	<div class="claimant-container">
		<c:set var="counter" value="0" />
		<c:forEach var="claimant" varStatus="arr_count" items="${data['claimant_list']}" >
			<c:set var="counter" value="${arr_count.count}" />
			<div class="claimant" id="id_claimant_${counter}">
				<div class="row">
					<fieldset>
						<div class="col-xs-12 col-md-5"><legend>Name of Claimant <c:if test="${counter > 1}">${counter}</c:if></legend></div>
						<div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="claimant_name">
							<div class="form-group__multi name">
                            <span>
                                <label for="form-firstname" class="visuallyhidden">First Name of Claimant</label>
                                <input type="text" id="form-firstname" class="form-control" placeholder="Given name" name="claimant_first_name" value="${fn:escapeXml(claimant['claimant_first_name'])}" aria-describedby="policyNameAria">
                            </span>
								<span>
                                <label for="form-lastname" class="visuallyhidden"> Last Name of Claimant</label>
                                <input type="text" id="form-lastname" class="form-control" placeholder="Surname" name="claimant_last_name" value="${fn:escapeXml(claimant['claimant_last_name'])}" aria-describedby="ariaName">
                            </span>
                        </div>
                        <div id="ariaName">
                          <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Please enter a name</small></div>
                          <div class="help-block secondary-error" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error"><small>Your name contains invalid characters</small></div>
                        </div>
                    </div>
                  </fieldset>
                </div>

                <div class="row">
                  <div class="col-xs-12 col-md-5"><label for="form-claimantrelationship" id="form-claimantrelationship-label">Relationship to the Deceased</label></div> 
                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="claimant_relationship">
                      <div class="form-select_wrap">
                          <select class="form-control control_relationship_select" id="form-claimantrelationship" name="claimant_relationship"  aria-describedby="ariaRelationship">
                              <option value="" selected disabled>Choose one&hellip;</option>
                              <option <c:if test="${claimant['claimant_relationship'] == 'Spouse'}">selected</c:if>>Spouse</option>
                              <option <c:if test="${claimant['claimant_relationship'] == 'Children'}">selected</c:if>>Children</option>
                              <option <c:if test="${claimant['claimant_relationship'] == 'Parents'}">selected</c:if>>Parents</option>
                              <option <c:if test="${claimant['claimant_relationship'] == 'Siblings'}">selected</c:if>>Siblings</option>
                              <option <c:if test="${claimant['claimant_relationship'] == 'Executor'}">selected</c:if>>Executor</option>
                              <option <c:if test="${claimant['claimant_relationship'] == 'Administrator'}">selected</c:if>>Administrator</option>
                          </select>
                      </div>
                      <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaRelationship"><small>Please choose an option</small></div>
                  </div>
                </div>

				<div class="row">
					<div class="col-xs-12 col-md-5"><label for="form-claimantproof">Please upload a copy of the document showing proof of relationship / rights to administrator Deceased's Estate</label>
					</div>
					<div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="claimant_relationship_document">
						<div class="document-number-container claimant_relationship_document-container${counter}">
							<c:set var="claimantRelationshipDoc" value="claimant_relationship_document_${counter}" />
							<c:choose>
								<c:when test="${empty claimant[claimantRelationshipDoc]}">
									<div class="document-num " id="document_no_${counter}_1">
										<a data-file-input="claimant_relationship_document_${counter}_1"
										   class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span>
											<c:choose><c:when test="${empty claimant['claimant_relationship_document_1']}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose>
										</a>
										<input type="file" id="form-claimantproof" name="claimant_relationship_document_${counter}_1"
											   value="${fn:escapeXml(claimant['claimant_relationship_document_1'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="claimant_relationship_document_${counter}_1">
									<div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="claimant_relationship_document_${counter}_1"><small>Please upload a file</small></div>
									</div>

								</c:when>
								<c:otherwise>
									<c:forEach var="relationshipDoc"  varStatus="relationship_doc_no" items="${claimant[claimantRelationshipDoc]}">
										<div class="document-num <c:if test="${relationship_doc_no.count!=1}">multiple</c:if>" id="document_no_${counter}_${relationship_doc_no.count}">
											<a data-file-input="claimant_relationship_document_${counter}_${relationship_doc_no.count}"
											   class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span>
												File Selected</a>
											<input type="file" id="form-claimantproof" name="claimant_relationship_document_${counter}_${relationship_doc_no.count}"
												   value="${fn:escapeXml(relationshipDoc)}" accept="image/jpeg,image/jpg,image/png,application/pdf"  aria-describedby="claimant_relationship_document_${counter}_${relationship_doc_no.count}">
										<div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="claimant_relationship_document_${counter}_${relationship_doc_no.count}"><small>Please upload a file</small></div>
										</div>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</div>
						
					</div>
				</div>

                <div class="row">
                  <div class="col-xs-12 col-md-5"><label for="form-claimantaddress" id="form-claimantaddress-label">Residential Address of Claimant</label></div>
                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="claimant_city">
                      <p><label for="form-country">Country</label></p>
                      <div class="form-select_wrap">
                          <select class="form-control control_country_select" id="form-country" name="claimant_city"  aria-describedby="ariaAddress">
                              <option value="" disabled selected>Choose one&hellip;</option>
                              <c:forEach var="country" varStatus="arr_count" items="${formData['country_list']}" >
                                  <c:choose>
                                      <c:when test="${country == 'None of the above'}">
                                          <option <c:if test="${claimant['claimant_city'] == 'none_of_above'}">selected</c:if> value="none_of_above">${country}</option>
                                      </c:when>
                                      <c:otherwise>
                                          <option <c:if test="${claimant['claimant_city'] == country}">selected</c:if>>${country}</option>
                                      </c:otherwise>
                                  </c:choose>
                              </c:forEach>
                          </select>
                      </div>
                      <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAddress"><small>Please choose an option</small></div>
                  </div>
                
                  <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="claimant_postal_code">
                      <p><label for="form-postcode">Postal Code</label></p>
                      <input type="text" class="form-control auto-width" id="form-postcode" data-lookup="postalCode" name="claimant_postal_code" value="${fn:escapeXml(claimant['claimant_postal_code'])}" aria-describedby="ariaPostCode">
                      <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaPostCode"><small>Please enter a value</small></div>
                  </div>
                
                  <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="claimant_building_number">
                      <p><label for="form-buildingnum">Building/Blk Number</label></p>
                      <input type="text" class="form-control" id="form-buildingnum" data-lookup="buildingNumber" name="claimant_building_number" value="${fn:escapeXml(claimant['claimant_building_number'])}" aria-describedby="ariaBuilding">
                      <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBuilding"><small>Please enter a value</small></div>
                  </div>
                
                  <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="claimant_unit_number">
                    <fieldset>
                      <div style="margin-bottom: 1em;"><legend>Unit number <small>(optional)</small></legend></div>
                      <div class="form-group__multi">
                          <span>
                              <label for="form-unitnumber1" class="visuallyhidden">Unit number part 1</label>
                              <input type="text" id="form-unitnumber1" class="form-control number-only-input optional" name="claimant_unit_number_1" value="${fn:escapeXml(claimant['claimant_unit_number_1'])}">
                          </span>
						  <span>
                              <label for="form-unitnumber2" class="visuallyhidden">Unit number part 2</label>
                              <input type="text" id="form-unitnumber2" class="form-control number-only-input optional" name="claimant_unit_number_2" value="${fn:escapeXml(claimant['claimant_unit_number_2'])}">
                          </span>
                      </div>
                    </fieldset>
                  </div>
               
                  <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="claimant_street_1">
                      <p><label for="form-street">Street</label></p>
                      <input type="text" id="form-street" class="form-control bottom_margined" placeholder="" data-lookup="streetName" name="claimant_street_1" value="${fn:escapeXml(claimant['claimant_street_1'])}" aria-describedby="ariaStreet">
                      <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaStreet" ><small>Please enter a value</small></div>
                  </div>
               
                  <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="claimant_street_2">
                      <p><label for="form-street2">Street line 2 <small>(optional)</small></label></p>
                      <input type="text" id="form-street2" class="form-control optional" placeholder="" name="claimant_street_2" value="${fn:escapeXml(claimant['claimant_street_2'])}">
                  </div>
               
                  <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="claimant_building_name">
                      <p><label for="form-buildingname">Building name
                              <small>(optional)</small>
                          </label>
                      </p>
                      <input type="text" id="form-buildingname" class="form-control optional" placeholder="" data-lookup="buildingName" name="claimant_building_name" value="${fn:escapeXml(claimant['claimant_building_name'])}">
                  </div>
                </div>

				<div class="row">
					<div class="col-xs-12 col-md-5"><label for="form-addressproof">If the Residential Address is not indicated on Claimant's Identification document,
						please upload document showing proof of Claimant's Residential Address</label></div>

					<div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="claimant_address_document">
						<div class="document-number-container claimant_address_document-container${counter}">
							<c:set var="claimantAddressDoc" value="claimant_address_document_${counter}" />

							<c:choose>
								<c:when test="${empty claimant[claimantAddressDoc]}">
									<div class="document-num " id="document_no_${counter}_1">
										<a data-file-input="claimant_address_document_${counter}_1"
										   class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span>
											<c:choose><c:when test="${empty claimant[claimant_address_document_1]}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
										<input type="file" id="form-claimantproof" name="claimant_address_document_${counter}_1"
											  class="optional"
											   value="${fn:escapeXml(claimant['claimant_address_document_1'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="claimant_address_document_${counter}_1">
											<div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="claimant_address_document_${counter}_1"><small>Please upload a file</small></div>
									</div>

								</c:when>
								<c:otherwise>
									<c:forEach var="addressDoc" varStatus="address_doc_no" items="${claimant[claimantAddressDoc]}">
										<div class="document-num <c:if test="${address_doc_no.count!=1}">multiple</c:if>" id="document_no_${counter}_${address_doc_no.count}">
											<a data-file-input="claimant_address_document_${counter}_${address_doc_no.count}"
											   class="btn secondary-btn-slate btn-file-upload"> <span aria-hidden="true"></span>
												File Selected</a>
											<input type="file" id="form-claimantproof" name="claimant_address_document_${counter}_${address_doc_no.count}"
												   <c:if test="${not empty addressDoc}">class="optional"</c:if>
												   value="${fn:escapeXml(addressDoc)}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="claimant_address_document_${counter}_${address_doc_no.count}">
												  <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="claimant_address_document_${counter}_${address_doc_no.count}"><small>Please upload a file</small></div>
										</div>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>

	<!-- Add More Something Button -->
	<div class="row">
		<div class="col-xs-12 col-md-6 container-fluid">
			<button type="button" class="blocklink" onclick="addNewClaimant()">
				<i class="icon icon-add"></i> Add another Claimant
			</button>
		</div>
	</div>
	<div class="space20"></div>
	<!-- Add More Something Button -->

</div>

