<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h2>Before Disability:</h2>
<div class="onlineinvestor__questionnaire">
    <div class="row">
        <div class="col-xs-12 col-md-5">Occupation</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="before_occupation">
            <input class="form-control" type="text" name="before_occupation" value="${fn:escapeXml(data['before_occupation'])}" aria-describedby="ariaBeforeOccupation">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBeforeOccupation"><small>Please enter a value</small></div>
        </div>
    </div>
              
    <div class="row">
        <div class="col-xs-12 col-md-5">Name of Employer</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="before_employer_name">
            <input class="form-control" type="text" name="before_employer_name" value="${fn:escapeXml(data['before_employer_name'])}" aria-describedby="ariaBeforeEmployer">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBeforeEmployer"><small>Please enter a value</small></div>
        </div>
    </div>
              
    <div class="row">
        <div class="col-xs-12 col-md-5">Address of Employer</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="before_employer_city">
            <p>City</p>
            <div class="form-select_wrap">
                <select class="form-control control_country_select" name="before_employer_city" aria-describedby="ariaBeforeEmployerAddress">
                    <option disabled selected value="">Choose one&hellip;</option>
                    <c:forEach var="country" varStatus="arr_count" items="${formData['country_list']}" >
                        <c:choose>
                            <c:when test="${country == 'None of the above'}">
                                <option <c:if test="${data['before_employer_city'] == 'none_of_above'}">selected</c:if> value="none_of_above">${country}</option>
                            </c:when>
                            <c:otherwise>
                                <option <c:if test="${data['before_employer_city'] == country}">selected</c:if>>${country}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select></div>
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBeforeEmployerAddress"><small>Please choose an option</small></div>
        </div>
   
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="before_employer_postal_code">
            <p>Postal Code</p>
            <input type="text" data-lookup="postalCode" class="form-control auto-width" name="before_employer_postal_code" value="${fn:escapeXml(data['before_employer_postal_code'])}" aria-describedby="ariaBeforeEmployerPostCode">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBeforeEmployerPostCode"><small>Please enter a value</small></div>
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="before_employer_building_number">
            <p>Building/Blk Number</p>
            <input type="text" data-lookup="buildingNumber" class="form-control" name="before_employer_building_number" value="${fn:escapeXml(data['before_employer_building_number'])}" aria-describedby="ariaBeforeEmployerBuilding">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBeforeEmployerBuilding"><small>Please enter a value</small></div>
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="before_employer_unit_number">
            <p>Unit number <small>(optional)</small></p>
            <div class="form-group__multi">
                <span><input type="text" class="form-control number-only-input optional" name="before_employer_unit_number_1" value="${fn:escapeXml(data['before_employer_unit_number_1'])}"></span>
                <span><input type="text" class="form-control number-only-input optional" name="before_employer_unit_number_2" value="${fn:escapeXml(data['before_employer_unit_number_2'])}"></span>
            </div>
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="before_employer_street_1">
            <p>Street</p>
            <input type="text" class="form-control bottom_margined alphanum-input" data-lookup="streetName" placeholder="" name="before_employer_street_1" value="${fn:escapeXml(data['before_employer_street_1'])}" aria-describedby="ariaBeforeEmployerStreet">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBeforeEmployerStreet"><small>Please enter a value</small></div>
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="before_employer_street_2">
            <p>Street line 2 <small>(optional)</small></p>
            <input type="text" class="form-control alphanum-input optional" placeholder="" name="before_employer_street_2" value="${fn:escapeXml(data['before_employer_street_2'])}">
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="before_employer_building_name">
            <p>Building name<small>(optional)</small></p>
            <input type="text" class="form-control alphanum-input optional" data-lookup="buildingName" placeholder="" name="before_employer_building_name" value="${fn:escapeXml(data['before_employer_building_name'])}">
        </div>
    
        <div class="col-xs-12 col-md-5">Average monthly salary</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="before_salary">
            <div class="input-group currency-text-box">
                <span class="input-group-addon">S$</span>
                <input class="form-control number-only-input" type="text" id="id_monthly_salary_before" name="before_salary" value="${fn:escapeXml(data['before_salary'])}" aria-describedby="ariaBeforeSalary">
            </div>
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBeforeSalary"><small>Please enter a value</small></div>
        </div>
    </div>
              
    <div class="row">
        <div class="col-xs-12 col-md-5">Exact duties performed at work</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="before_work_duty">
            <input class="form-control" type="text" name="before_work_duty" value="${fn:escapeXml(data['before_work_duty'])}" aria-describedby="ariaBeforeDuties">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBeforeDuties"><small>Please enter a value</small></div>
        </div>
    </div>
</div>

<h2>After Disability:</h2>
<div class="onlineinvestor__questionnaire">
    <div class="row">
        <div class="col-xs-12 col-md-5">Occupation</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="after_occupation">
            <input class="form-control" type="text" name="after_occupation" value="${fn:escapeXml(data['after_occupation'])}" aria-describedby="ariaAfterOccupation">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAfterOccupation"><small>Please enter a value</small></div>
        </div>
    </div>
                  
    <div class="row">
        <div class="col-xs-12 col-md-5">Name of Employer <small>(optional)</small></div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="after_employer_name">
            <input class="form-control optional" type="text" name="after_employer_name" value="${fn:escapeXml(data['after_employer_name'])}" aria-describedby="ariaAfterEmployer">
            <div class="help-block " role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAfterEmployer"><small>Please enter a value</small></div>
        </div>
    </div>
                  
    <div class="row">
        <div class="col-xs-12 col-md-5">Address of Employer <small>(optional)</small></div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="after_employer_city">
            <p>City <small>(optional)</small></p>
            <div class="form-select_wrap">
                <select class="form-control control_country_select optional" name="after_employer_city"  aria-describedby="ariaAfterEmployerCity">
                    <option disabled selected value="">Choose one&hellip;</option>
                    <c:forEach var="country" varStatus="arr_count" items="${formData['country_list']}" >
                        <c:choose>
                            <c:when test="${country == 'None of the above'}">
                                <option <c:if test="${data['after_employer_city'] == 'none_of_above'}">selected</c:if> value="none_of_above">${country}</option>
                            </c:when>
                            <c:otherwise>
                                <option <c:if test="${data['after_employer_city'] == country}">selected</c:if>>${country}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAfterEmployerCity"><small>Please choose an option</small></div>
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="after_employer_postal_code">
            <p>Postal Code <small>(optional)</small></p>
            <input type="text" class="form-control optional auto-width" data-lookup="postalCode" name="after_employer_postal_code" value="${fn:escapeXml(data['after_employer_postal_code'])}"  aria-describedby="ariaAfterEmployerPostCode">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAfterEmployerPostCode"><small>Please enter a value</small></div>
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="after_employer_building_number">
            <p>Building/Blk Number <small>(optional)</small></p>
            <input type="text" class="form-control optional" data-lookup="buildingNumber" name="after_employer_building_number" value="${fn:escapeXml(data['after_employer_building_number'])}" aria-describedby="ariaAfterEmployerBuilding">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAfterEmployerBuilding"><small>Please enter a value</small></div>
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="after_employer_unit_number">
            <p>Unit number <small>(optional)</small></p>
            <div class="form-group__multi">
                <span><input type="text" class="form-control number-only-input optional" name="after_employer_unit_number_1" value="${fn:escapeXml(data['after_employer_unit_number_1'])}"></span>
                <span><input type="text" class="form-control number-only-input optional" name="after_employer_unit_number_2" value="${fn:escapeXml(data['after_employer_unit_number_2'])}"></span>
            </div>
        </div>
   
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="after_employer_street_1">
            <p>Street <small>(optional)</small></p>
            <input type="text" class="form-control bottom_margined alphanum-input optional" data-lookup="streetName" placeholder="" name="after_employer_street_1" value="${fn:escapeXml(data['after_employer_street_1'])}"  aria-describedby="ariaAfterEmployerStreet">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAfterEmployerStreet"><small>Please enter a value</small></div>
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="after_employer_street_2">
            <p>Street line 2 <small>(optional)</small></p>
            <input type="text" class="form-control alphanum-input optional" placeholder="" name="after_employer_street_2" value="${fn:escapeXml(data['after_employer_street_2'])}">
        </div>
    
        <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="after_employer_building_name">
            <p>Building name
                <small>(optional)</small>
            </p>
            <input type="text" class="form-control alphanum-input optional" data-lookup="buildingName" placeholder="" name="after_employer_building_name" value="${fn:escapeXml(data['after_employer_building_name'])}">
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Average monthly salary <small>(optional)</small></div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="after_salary">
            <div class="input-group currency-text-box">
                <span class="input-group-addon">S$</span>
                <input class="form-control number-only-input optional" type="text" id="id_monthly_salary_after" name="after_salary" value="${fn:escapeXml(data['after_salary'])}" aria-describedby="ariaAfterSalary">
                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAfterSalary"><small>Please enter a value</small></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Exact duties performed at work <small>(optional)</small></div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="banafter_work_duty_name">
            <input class="form-control optional" type="text" name="after_work_duty" value="${fn:escapeXml(data['after_work_duty'])}"  aria-describedby="ariaAfterDuties">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaAfterDuties"><small>Please enter a value</small></div>
        </div>
    </div>
</div>
