<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h2>
    Please note that any bank charges may be deducted from the amount payable (if applicable)
</h2>

<div class="onlineinvestor__questionnaire">
    <div class="row">
        <div class="col-xs-12 col-md-5">Name of Bank </div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="bank_name">
            <input class="form-control" type="text" name="bank_name" value="${fn:escapeXml(data['bank_name'])}" aria-describedby="ariaBankName">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankName"><small>Please enter a value</small></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Account Number / IBAN</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="account_number">

            <input class="form-control" type="text" name="account_number" value="${fn:escapeXml(data['account_number'])}" autocomplete="off" aria-describedby="ariaBankNum">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankNum"><small>Please enter a value</small></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5"><label id="countrybank-label">Country of Bank</label></div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="bank_country">
            <div class="form-select_wrap">
                <select class="form-control bank_country_select" name="bank_country" value="${fn:escapeXml(data['bank_country'])}" aria-describedby="ariaBankCountry">
                    <option disabled selected>Please select&hellip;</option>
                    <c:forEach var="country" varStatus="arr_count" items="${formData['country_list']}" >
                        <c:choose>
                            <c:when test="${country == 'None of the above'}">
                                <option <c:if test="${data['bank_country'] == 'none_of_above'}">selected</c:if> value="none_of_above">${country}</option>
                            </c:when>
                            <c:otherwise>
                                <option <c:if test="${data['bank_country'] == country}">selected</c:if>>${country}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select></div>
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankCountry"><small>Please select an option</small></div>
        </div>
    </div>


    <div class="bank_address_wrapper" <c:if test="${data['bank_country'] == null || data['bank_country'] == 'Singapore'}">style="display: none;" </c:if>>

        <div class="row">
            <div class="col-xs-12 col-md-5">Address of Bank</div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="bank_city">
                <p>City</p>
                <input class="form-control <c:if test="${data['bank_city'] == null || data['bank_city'] == false}">optional</c:if>" type="text" placeholder="City" name="bank_city" data="${bankrupt['bank_city']}" aria-describedby="ariaBankCity">
                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankCity"><small>Please select an option</small></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="bank_postal_code">
                <p>Postal Code</p>
                <input type="text" class="form-control optional auto-width" name="bank_postal_code" value="${fn:escapeXml(data['bank_postal_code'])}" aria-describedby="ariaBankPostCode">
                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankPostCode"><small>Please enter a value</small></div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="bank_building_number">
                <p>Building/Blk Number</p>
                <input type="text" class="form-control optional" name="bank_building_number" value="${fn:escapeXml(data['bank_building_number'])}" aria-describedby="ariaBankBuilding">
                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankBuilding"><small>Please enter a value</small></div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group unit-number" data-field-name="bank_unit_number">
                <fieldset>
                    <legend>Unit number <small>(optional)</small></legend>
                    <div class="form-group__multi ">
                        <span><input type="text" class="form-control number-only-input optional" name="bank_unit_number_1" value="${fn:escapeXml(data['bank_unit_number_1'])}"></span>
                        <span><input type="text" class="form-control number-only-input optional" name="bank_unit_number_2" value="${fn:escapeXml(data['bank_unit_number_2'])}"></span>
                    </div>
                </fieldset>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group street" data-field-name="bank_street_1">
                <p>Street</p>
                <input type="text" class="form-control bottom_margined optional" placeholder=""  name="bank_street_1" value="${fn:escapeXml(data['bank_street_1'])}" aria-describedby="ariaBankStreet">
                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankStreet"><small>Please enter a value</small></div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group street" data-field-name="bank_street_2">
                <p>Street line 2 <small>(optional)</small></p>
                <input type="text" class="form-control optional" placeholder="" name="bank_street_2" value="${fn:escapeXml(data['bank_street_2'])}">
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6 form-group building-name" data-field-name="bank_building_name">
                <p>Building name <small>(optional)</small></p>
                <input type="text" class="form-control optional" placeholder="" name="bank_building_name" value="${fn:escapeXml(data['bank_building_name'])}">
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-md-5">Swift Code / Swift Address</div>
            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="bank_swift_code">
                <input class="form-control optional" type="text" name="bank_swift_code" value="${fn:escapeXml(data['bank_swift_code'])}"  aria-describedby="ariaBankSwift">
                <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankSwift"><small>Please enter a value</small></div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-12 col-md-5">Please submit a valid copy of your bank book / statement for account verification</div>
        <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="account_document">
            <a data-file-input="account_document" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['account_document'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
            <input type="file" name="account_document" <c:if test="${data['account_document'] != null}">class="optional"</c:if> value="${fn:escapeXml(data['account_document'])}" accept="image/jpeg,image/jpg,image/png,application/pdf"  aria-describedby="ariaBankUpload">
            <div class="help-block" role="alert" aria-live="assertive" aria-atomic="true" aria-label="Error" id="ariaBankUpload"><small>Please upload a file</small></div>
        </div>
    </div>

</div>

