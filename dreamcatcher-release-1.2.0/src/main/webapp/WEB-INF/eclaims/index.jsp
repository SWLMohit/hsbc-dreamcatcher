<%@ page contentType="text/html; charset=UTF-8"%>
	<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="eClaims"></jsp:param>
	<jsp:param name="metadataKeywords" value="insurance claim, eclaim, online insurance claim"></jsp:param>
	<jsp:param name="metadataDescription" value="It can be a difficult time when you have to make an insurance claim. So, we have made it convenient for you to submit your claim online as soon as possible."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/eclaims/"></jsp:param>
</jsp:include>



<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="/">Home</a>
            <a href="/eclaims/" aria-current="page">eClaims</a>
        </nav>
    </div>
    <section class="container-fluid no-padding">
      <div class="header-page header-eclaims">
          <div class="container">
              <div class="featured">
                  <h1>Online insurance claims</h1>
                  <p>Let us guide you through the process of making <span></span>a claim</p>
              </div>
          </div>
      </div>
  </section>

    <article>
        <section class="onlineinvestor eclaims">
            <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-sm-9 col-md-8">
                    <p class="feature-text">Tell us about the type of claim you are making</p>
                    <form data-form-name="eclaims" data-form-type="Online insurance claims">
                        <div class="form-item item1 active">
                          <div class="row onlineinvestor__questionnaire">
                            <fieldset>
                              <div class="col-xs-12 col-md-5"><label for="form-typeclaim">Type of claim</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group">
                                <div class="form-select_wrap">
                                  <select class="form-control eclaims-type" id="form-typeclaim" aria-describedby="ariaEclaimType">
                                    <option id="0" selected disabled>Choose one&hellip;</option>
                                    <option id="1">Death Due to Accident/Unnatural Causes</option>
                                    <option id="2">Death Due to Illness</option>
                                    <option id="3">Critical Illness</option>
                                    <option id="4">Terminal Illness</option>
                                    <option id="5">TPD Due to Illness</option>
                                    <option id="6">TPD Due to Accident</option>
                                    <option id="7">Unemployment</option>
                                  </select>
                                </div>
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaEclaimType"><small>Please select a claim</small></div>
                              </div>
                            </fieldset>
                          </div>
                          
                          <div class="article__leader__main__btnset">
                            <button type="button" class="btn btn-red" onclick="choosePage()">Continue</button>
                          </div>
                        </div><!-- /.item -->
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/eclaims_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>

<script>
    function choosePage() {
		var id = parseInt($('.eclaims-type').selectBox().children(":selected").attr("id"));
		TMS.trackEvent({
			'event_category': 'Claim Form',
			'event_action':   'Finish',
			'event_form':     'Form Finish',
			'event_content':  'Online insurance claims'
		});
        setTimeout(function () {
			switch (id){
				case 0:
					showErrorMessageOn($('.eclaims-type'));
					break;
				case 1:
					openPage('./1-death-due-to-accident');
					break;
				case 2:
					openPage('./2-death-due-to-illness');
					break;
				case 3:
					openPage('./3-critical-illness');
					break;
				case 4:
					openPage('./4-terminal-illness');
					break;
				case 5:
					openPage('./5-tpd-due-to-illness');
					break;
				case 6:
					openPage('./6-tpd-due-to-accident');
					break;
				case 7:
					openPage('./7-unemployment');
					break;

			}
		}, 2000);
    }
</script>
