<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="onlineinvestor-pdpa"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/2_death_due_to_illness/includes/breadcrumbs.jsp" flush="true"/>
    <article>
        <section class="onlineinvestor">
            <div class="container">
            
              <div class="eclaims__titler">
                <h1>eClaims &mdash; Death Due to Illness &mdash; Claim information</h1>
                <a href="/" class="buynow-exit-button">Exit</a>
              </div>
                <div class="onlineinvestor__breadcrumb">
                    <a href="../2-death-due-to-illness/1">Claimant information</a>
                    <a href="#">Claim information</a>
                    <a href="#" class="disabled">Other information</a>
                    <a href="#" class="disabled">Payment option</a>
                    <a href="#" class="disabled">Declaration</a>
                </div>

                <div class="row">
                  <div class="col-xs-12">
                    <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="eclaims" data-form-type="Death due to illness">
                      <div class="onlineinvestor_carousel">
                        <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                          <div class="onlineinvestor__questionnaire">
                            <div class="row">
                              <div class="col-xs-12 col-md-5"><label for="date-deceased-symptom">When did the deceased first present with symptoms of the illness?</label></div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown" data-field-name="deceased_symptoms_present_date_day">
                                <input id="date-deceased-symptom" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-deceased-symptom" name="deceased_symptoms_present_date_day"
                                       value="${fn:escapeXml(data['deceased_symptoms_present_date_day'])}" aria-describedby="ariaSymptomsDate">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaSymptomsDate">
                                  <small>Please enter a valid date</small>
                                </div>
                              </div>
                            </div>
                          
                            <div class="row">
                              <div class="col-xs-12 col-md-5">What symptoms did the Deceased present before consultation with doctors?</div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="deceased_symptoms">
                                <input class="form-control" type="text" name="deceased_symptoms" value="${fn:escapeXml(data['deceased_symptoms'])}" aria-describedby="ariaSymptoms">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaSymptoms"><small>Please enter a value</small></div>
                              </div>
                            </div>

                            <div class="attended-container">
                              <h2>Please provide names of all doctor(s) / hospital(s) / clinic(s) who have
                                attended to the Deceased for the illness(if applicable)
                              </h2>
                              <c:forEach var="doctor" varStatus="arr_count" items="${data['doctor_list']}" >
                              <div class="attended" id="id_attended_${arr_count.count}">
                                <div class="row">
                                  <fieldset>
                                    <div class="col-xs-12 col-md-5"><legend>Consultation <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if> Period <small>(optional)</small></legend></div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown_from" data-field-name="doctor_from_date">
                                      <p><label for="date-from">From:</label></p>
                                      <input id="date-from" type="text" name="doctor_from_date" placeholder="mm/yyyy" maxlength="7" class="date-validate-month auto-width form-control date-consult"
                                             value="${fn:escapeXml(doctor['doctor_from_date'])}" aria-describedby="ariaConsultDateStart">
                                      <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaConsultDateStart"><small>Please enter a valid date in the MM/YYYY format</small></div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-md-offset-6 form-group date_dropdown_to" data-field-name="doctor_to_date">
                                      <p><label for="date-to">To:</label></p>
                                      <input id="date-to" type="text" name="doctor_to_date" placeholder="mm/yyyy" maxlength="7" class="date-validate-month auto-width form-control date-consult"
                                             value="${fn:escapeXml(doctor['doctor_to_date'])}" aria-describedby="ariaConsultDateEnd">
                                      <div id="ariaConsultDateEnd">
                                        <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a valid date in the MM/YYYY format</small></div>
                                        <div class="help-block date-out-of-range secondary-error"><small>Please enter a valid date range</small></div>
                                      </div>
                                    </div>
                                  </fieldset>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12 col-md-5">Name of Doctor / Specialist <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if> <small>(optional)</small></div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="doctor_name">
                                    <input class="form-control optional" type="text" name="doctor_name" value="${fn:escapeXml(doctor['doctor_name'])}" aria-describedby="ariaConsultDoc">
                                    <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaConsultDoc"><small>Please enter a value</small></div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12 col-md-5">Name of Hospital / Clinic <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if> <small>(optional)</small></div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="doctor_hospital">
                                    <input class="form-control optional" type="text" name="doctor_hospital" value="${fn:escapeXml(doctor['doctor_hospital'])}" aria-describedby="ariaConsultHospital">
                                    <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaConsultHospital"><small>Please enter a value</small></div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12 col-md-5">Address of Hospital / Clinic <c:if test="${arr_count.count > 1}">${arr_count.count}</c:if> <small>(optional)</small></div>
                                  <div class="col-xs-12 col-md-6 col-md-offset-1 form-group city_dropdown" data-field-name="doctor_hospital_state">
                                    <p>Country <small>(optional)</small></p>
                                    <div class="form-select_wrap">
                                      <select class="form-control control_country_select optional" name="doctor_hospital_state" aria-describedby="ariaConsultCountry">
                                        <option disabled selected value="">Choose one&hellip;</option>
                                        <c:forEach var="country" varStatus="arr_count" items="${formData['country_list']}" >
                                          <c:choose>
                                            <c:when test="${country == 'None of the above'}">
                                              <option <c:if test="${doctor['doctor_hospital_state'] == 'none_of_above'}">selected</c:if> value="none_of_above">${country}</option>
                                            </c:when>
                                            <c:otherwise>
                                              <option <c:if test="${doctor['doctor_hospital_state'] == country}">selected</c:if>>${country}</option>
                                            </c:otherwise>
                                          </c:choose>
                                        </c:forEach>
                                      </select></div>
                                    <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaConsultCountry"><small>Please choose an option</small></div>
                                  </div>
                                
                                  <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="doctor_hospital_street1">
                                    <p>Street <small>(optional)</small></p>
                                    <input type="text" class="form-control optional bottom_margined" placeholder="" name="doctor_hospital_street1" value="${fn:escapeXml(doctor['doctor_hospital_street1'])}">
                                  </div>
                               
                                  <div class="col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="doctor_hospital_street2">
                                    <p>Street line 2 <small>(optional)</small></p>
                                    <input type="text" class="form-control optional" placeholder="" name="doctor_hospital_street2"  value="${fn:escapeXml(doctor['doctor_hospital_street2'])}" aria-describedby="ariaConsultStreet2">
                                    <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaConsultStreet2"><small>Please enter a value</small></div>
                                  </div>
                                </div>
                              </div>
                              </c:forEach>
                            </div>
                            <!-- Add More Something Button -->
                            <div class="row">
                              <div class="col-xs-12 col-md-6 container-fluid">
                                <button type="button" class="blocklink" onclick="addAttended()">
                                  <i class="icon icon-add"></i> Add the details of another doctor, hospital / clinic
                                </button>
                              </div>
                            </div>
                            <div class="space20"></div>
                              <!-- Add More Something Button -->

                            <div class="row">
                              <div class="col-xs-12 col-md-5">Please upload a copy of the Death Certificate</div>
                              <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="death_certificate">
                                <a data-file-input="death_certificate" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['death_certificate'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                <input type="file" name="death_certificate" <c:if test="${data['death_certificate'] != null}">class="optional"</c:if> value="${fn:escapeXml(data['death_certificate'])}" accept="image/jpeg,image/jpg,image/png,application/pdf"  aria-describedby="ariaConsultUpload">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaConsultUpload"><small>Please upload a file</small></div>
                              </div>
                            </div>

                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Did the death occur in Singapore?</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="occur_in_singapore">
                                  <div class="newradio--inline">
                                    <input id="id_local_death_yes" type="radio" name="occur_in_singapore" value="true" <c:if test="${data['occur_in_singapore'] == true}">checked</c:if>>
                                    <label for="id_local_death_yes"><span><span></span></span>Yes</label>
                                  </div>
                                  <div class="newradio--inline">
                                    <input id="id_local_death_no" type="radio" name="occur_in_singapore" value="false" <c:if test="${data['occur_in_singapore'] == false}">checked</c:if> aria-describedby="ariaDeathSg">
                                    <label for="id_local_death_no"><span><span></span></span>No</label>
                                  </div>
                                  <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaDeathSg"><small>Please select an option</small></div>
                                </div>
                              </fieldset>
                            </div>

                            <div class="row">
                              <div class="local_death_wrapper" <c:if test="${data['occur_in_singapore'] == null || data['occur_in_singapore'] == true}">style="display: none"</c:if>>
                                <div class="col-xs-12 col-md-5">Please upload a copy of the letter from ICA confirming receipt of the Singapore IC, Passport and overseas Death Certificate</div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="occur_in_singapore_document">

                                  <a data-file-input="occur_in_singapore_document" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['occur_in_singapore'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                  <input type="file" <c:if test="${(data['occur_in_singapore'] == null || data['occur_in_singapore'] == true) || data['occur_in_singapore_document'] != null}">class="optional"</c:if> name="occur_in_singapore_document" value="${fn:escapeXml(data['occur_in_singapore_document'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="ariaDeathUpload">
                                  <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaDeathUpload"><small>Please upload a file</small></div>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <fieldset>
                                <div class="col-xs-12 col-md-5"><legend>Did the Deceased left a Will?</legend></div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="left_a_will">
                                  <div class="newradio--inline">
                                    <input id="id_will_yes" type="radio" name="left_a_will" value="true" <c:if test="${data['left_a_will'] == true}">checked</c:if>>
                                    <label for="id_will_yes"><span><span></span></span>Yes</label>
                                  </div>
                                  <div class="newradio--inline">
                                    <input id="id_will_no" type="radio" name="left_a_will" value="false" <c:if test="${data['left_a_will'] == false}">checked</c:if> aria-describedby="ariaWill">
                                    <label for="id_will_no"><span><span></span></span>No</label>
                                  </div>
                                  <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaWill"><small>Please select an option</small></div>
                                </div>
                              </fieldset>
                            </div>

                            <div class="row">
                              <div class="will_wrapper" <c:if test="${data['left_a_will'] == null || data['left_a_will'] == false}">style="display: none"</c:if>>
                                <div class="col-xs-12 col-md-5">Please upload a copy of the Will</div>
                                <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="left_a_will_document">
                                  <a data-file-input="left_a_will_document" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['left_a_will_document'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                                  <input type="file" <c:if test="${(data['left_a_will'] == null || data['left_a_will'] == false) || data['left_a_will_document'] != null}">class="optional"</c:if> name="left_a_will_document" value="${fn:escapeXml(data['left_a_will_document'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="ariaWillUpload">
                                  <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaWillUpload"><small>Please upload a file</small></div>
                                </div>
                              </div> 
                            </div>
                          </div>
                        </div><!-- /.item -->
                        <div class="article__leader__main__btnset col-xs-12 col-sm-9">
                          <button type="button" class="btn btn-red"  data-form-action="next" onclick="validateThenOpenPage()">Continue</button>
                          <button type="button" class="back_btn" onclick="openPage('../2-death-due-to-illness/1')">Back</button>
                        </div>
        
                      </div>
                    </form>
                  </div>
                </div><!-- /.row -->
            </div>

        </section>
    </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/2_death_due_to_illness/includes/eclaims2_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function(){
//        $('.will_wrapper').hide();
//        $('.local_death_wrapper').hide();
//
//        for (var day = 1; day <= 31; day++) {
//            $(".control_day_select").append("<option>" + pad(day, 2) + "</option>");
//        }
//        $(".control_day_select").selectBox('refresh');
//
//        for (var month = 1; month <= 12; month++) {
//            $(".control_month_select").append("<option>" + pad(month, 2) + "</option>");
//        }
//        $(".control_month_select").selectBox('refresh');
//
//        var currYear = (new Date()).getFullYear();
//
//        for (var year = 1970; year <= currYear; year++) {
//            $(".control_year_select").append("<option>" + year + "</option>");
//        }
//        $(".control_year_select").selectBox('refresh');
    });

</script>
