<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
  <jsp:param name="pageTitle" value="${formTitle}"></jsp:param>
  <jsp:param name="lpSecondaryTag" value="onlineinvestor-homepage"></jsp:param>
</jsp:include>

<div class="main">
    <jsp:include page="/WEB-INF/eclaims/2_death_due_to_illness/includes/breadcrumbs.jsp" flush="true"/>
  <article>
      <section class="onlineinvestor">
          <div class="container">

            <div class="eclaims__titler">
              <h1>eClaims &mdash; Death Due to Illness &mdash; Make a Claim</h1>
              <a href="/" class="buynow-exit-button">Exit</a>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <form class="buynow-form" method="post" enctype="multipart/form-data" data-form-name="eclaims" data-form-type="Death due to illness">
                  <div class="onlineinvestor_carousel">
                    <div class="form-item item1 active col-xs-12 col-sm-9 col-md-8">
                      <div class="onlineinvestor__questionnaire">
                        <div class="policy-number-container row">
                          <div class="col-xs-12 col-md-5">Policy Number(s)</div>
                          <c:choose>
                            <c:when test="${data['policy_number_list'] != null}">
                              <c:forEach var="policy_number" varStatus="arr_count" items="${data['policy_number_list']}" >
                                <div class="policy-num col-xs-12 col-md-6 col-md-offset-6 form-group" data-field-name="policy_number">
                                  <input class="form-control alphanum-input" type="text" id="id_policy_${arr_count.count}" name="policy_number" value="${fn:escapeXml(policy_number)}" aria-describedby="ariaPolicyNnum">
                                  <div id="ariaPolicyNnum">
                                    <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a valid policy number</small></div>
                                    <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a unique policy number</small></div>
                                  </div>
                                </div>
                              </c:forEach>
                            </c:when>
                            <c:otherwise>
                              <div class="policy-num col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policy_number">
                                <input class="form-control alphanum-input" type="text" id="id_policy_1" name="policy_number" aria-describedby="ariaPolicyNnum2">
                                <div  id="ariaPolicyNnum2">
                                  <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a valid policy number</small></div>
                                  <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a unique policy number</small></div>
                                </div>
                              </div>
                            </c:otherwise>
                          </c:choose>

                        </div>
                        <!-- Add More Something Button -->
                        <div class="row">
                          <div class="col-xs-12 col-md-6 container-fluid">
                            <button type="button" class="blocklink" onclick="addPolicyNum()">
                              <i class="icon icon-add"></i> Add another policy number
                            </button>
                          </div>
                        </div>
                        <div class="space30"></div>
                        <!-- Add More Something Button -->

                        <div class="row">
                          <fieldset>
                            <div class="col-xs-12 col-md-5"><legend>Name of Policyowner</legend></div>
                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_name">
                              <div class="form-group__multi name">
                                <span><input type="text" class="form-control" name="policyowner_first_name" placeholder="Given name" value="${fn:escapeXml(data['policyowner_first_name'])}" aria-describedby="policyNameAria"></span>
                                <span><input type="text" class="form-control" name="policyowner_last_name" placeholder="Surname" value="${fn:escapeXml(data['policyowner_last_name'])}" aria-describedby="ariaPolicyName"></span>
                              </div>
                              <div id="ariaPolicyName">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a name</small></div>
                                <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Your name contains invalid characters</small></div>
                              </div>
                            </div>

                          </fieldset>
                        </div>

                        <div class="row">
                          <div class="col-xs-12 col-md-5">NRIC of Claimant</div>
                          <div class="col-xs-12 col-md-6 col-md-offset-1 form-group nric" data-field-name="policy_owner_nric">
                            <input class="form-control alphanum-input" type="text" id="nric" name="policy_owner_nric" value="${fn:escapeXml(data['policy_owner_nric'])}" aria-describedby="ariaPolicyNRIC">
                            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaPolicyNRIC"><small>Please enter a valid NRIC number</small></div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-xs-12 col-md-5"><label for="date">Date of Birth of Claimant</label></div>
                          <div class="col-xs-12 col-md-6 col-md-offset-1 form-group date_dropdown" data-field-name="policy_owner_birth">
                            <input id="date" type="text" placeholder="dd/mm/yyyy" maxlength="10" class="date-validate auto-width form-control date-dob" name="policy_owner_birth_day" value="${fn:escapeXml(data['policy_owner_birth_day'])}" aria-describedby="ariaPolicyDOB">
                            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaPolicyDOB">
                              <small>Please enter a valid date</small>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-xs-12 col-md-5">Please upload a copy of the NRIC / Passport of Claimant</div>
                          <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_passport">
                            <a data-file-input="policyowner_passport" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['policyowner_passport'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                            <input type="file" <c:if test="${data['policyowner_passport'] != null}">class="optional"</c:if> name="policyowner_passport" value="${fn:escapeXml(data['policyowner_passport'])}" accept="image/jpeg,image/jpg,image/png,application/pdf" aria-describedby="ariaUpload">
                            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaUpload"><small>Please upload a file</small></div>
                          </div>
                        </div>

                        <div class="row">
                          <fieldset>
                            <div class="col-xs-12 col-md-5"><legend>Contact no. of Claimant</legend></div>
                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_contact_country">
                              <div class="form-group__multi form-group__multi--phone">
                                <span><input type="text" class="form-control number-only-input" name="policyowner_contact_country" value="${not empty data['policyowner_contact_country'] ? fn:escapeXml(data['policyowner_contact_country']) :'+65'}" aria-describedby="ariaPolicyNumCc"></span>
                                <span><input type="text" class="form-control number-only-input" placeholder="" name="policyowner_contact_number" value="${fn:escapeXml(data['policyowner_contact_number'])}" aria-describedby="ariaPolicyNum"></span>
                              </div>
                              <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Your phone number is invalid</small></div>
                              <div class="help-block-multi error-1" role="alert" aria-atomic="true" aria-label="Error" id="ariaPolicyNumCc"><small>Your country code is too long</small></div>
                              <div class="help-block-multi error-2" role="alert" aria-atomic="true" aria-label="Error" id="ariaPolicyNum"><small>Your phone number is invalid</small></div>

                            </div>
                          </fieldset>
                        </div>

                        <div class="row">
                          <div class="col-xs-12 col-md-5">Email address of Claimant</div>
                          <div class="col-xs-12 col-md-6 col-md-offset-1 form-group email" data-field-name="policyowner_email">
                            <input type="email" class="form-control" name="policyowner_email" value="${fn:escapeXml(data['policyowner_email'])}" aria-describedby="ariaPolicyEmail">
                            <div id="ariaPolicyEmail">
                              <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Your email is missing</small></div>
                              <div class="help-block-secondary" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a valid email</small></div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-xs-12 col-md-5">Confirm email address</div>
                          <div class="col-xs-12 col-md-6 col-md-offset-1 form-group confirm-email" data-field-name="policyowner_confirm_email">
                            <input type="email" class="form-control" value="${fn:escapeXml(data['policyowner_email'])}"  aria-describedby="ariaPolicyEmailConf">
                            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaPolicyEmailConf"><small>Email does not match</small></div>
                          </div>
                        </div>

                        <div class="row">
                          <fieldset>
                            <div class="col-xs-12 col-md-5"><legend>Is the Policyowner the Life Insured?</legend></div>
                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured">
                              <div class="newradio--inline">
                                <input id="id_is_policyowner_yes" type="radio" name="policyowner_life_insured" value="true"
                                       <c:if test="${data['policyowner_life_insured'] == true}">checked</c:if>>
                                <label for="id_is_policyowner_yes"><span><span></span></span>Yes</label>
                              </div>
                              <div class="newradio--inline">
                                <input id="id_is_policyowner_no" type="radio" name="policyowner_life_insured" value="false"
                                       <c:if test="${data['policyowner_life_insured'] == false}">checked</c:if> aria-describedby="ariaLifeInsured">
                                <label for="id_is_policyowner_no"><span><span></span></span>No</label>
                              </div>
                              <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaLifeInsured"><small>Please select an option</small></div>
                            </div>
                          </fieldset>
                        </div>

                        <div class="row not_policy_owner_wrapper" <c:if test="${data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true}">style="display: none"</c:if>>
                          <fieldset>
                            <div class="col-xs-12 col-md-5"><legend>Name of Life Insured </legend></div>
                            <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured_name">
                              <div class="form-group__multi name">
                                <span><input type="text" class="form-control optional" name="policyowner_life_insured_first_name" placeholder="Given name" value="${fn:escapeXml(data['policyowner_life_insured_first_name'])}"></span>
                                <span><input type="text" class="form-control optional" name="policyowner_life_insured_last_name" placeholder="Surname" value="${fn:escapeXml(data['policyowner_life_insured_last_name'])}" aria-describedby="ariaLifeInsuredName"></span>
                              </div>
                              <div id="ariaLifeInsuredName">
                                <div class="help-block" role="alert" aria-atomic="true" aria-label="Error"><small>Please enter a name</small></div>
                                <div class="help-block secondary-error" role="alert" aria-atomic="true" aria-label="Error"><small>Your name contains invalid characters</small></div>
                              </div>
                            </div>
                          </fieldset>
                        </div>


                        <div class="row not_policy_owner_wrapper" <c:if test="${data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true}">style="display: none"</c:if>>
                          <div class="col-xs-12 col-md-5">Please upload a copy of the NRIC/Passport of the Life Insured</div>
                          <div class="col-xs-12 col-md-6 col-md-offset-1 form-group" data-field-name="policyowner_life_insured_passport">

                            <a data-file-input="policyowner_life_insured_passport" class="btn secondary-btn-slate btn-file-upload"><span aria-hidden="true"></span><c:choose><c:when test="${data['policyowner_life_insured_passport'] == null}">Upload</c:when><c:otherwise>File Selected</c:otherwise></c:choose></a>
                            <input type="file" <c:if test="${(data['policyowner_life_insured'] == null || data['policyowner_life_insured'] == true) || data['policyowner_life_insured_passport'] != null}">class="optional"</c:if> name="policyowner_life_insured_passport" value="${fn:escapeXml(data['policyowner_life_insured_passport'])}" accept="image/jpeg,image/jpg,image/png,application/pdf"  aria-describedby="ariaLifeInsuredUpload">
                            <div class="help-block" role="alert" aria-atomic="true" aria-label="Error" id="ariaLifeInsuredUpload"><small>Please upload a file</small></div>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div class="article__leader__main__btnset col-xs-12 col-md-9 ">
                      <button type="button" class="btn btn-red" data-form-action="next" onclick="validateThenOpenPage()">Continue</button>
                      <button type="button" class="back_btn" onclick="openPage('../eclaims/')">Back</button>
                    </div><!-- /.item -->

                  </div>
                </form>
              </div>
            </div><!-- /.row -->

      </div>

    </section>
  </article>
</div>

<jsp:include page="/WEB-INF/eclaims/includes/footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/eclaims/2_death_due_to_illness/includes/eclaims2_tracking.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function() {
        // $('.not_policy_owner_wrapper').hide();
    });
</script>
