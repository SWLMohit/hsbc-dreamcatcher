<%@ page contentType="text/html; charset=UTF-8"%>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
	<jsp:param name="pageTitle" value="dreamcatcher-homepage"></jsp:param>
   	<jsp:param name="lpSecondaryTag" value="homepage"></jsp:param>
	<jsp:param name="metadataKeywords" value="insurance singapore, hsbc insurance, online insurance singapore"></jsp:param>
	<jsp:param name="metadataDescription" value="HSBC's online insurance products are easy, fast and secure. Click here to see how our plans can help you build a better future."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/"></jsp:param>
</jsp:include>

<section class="container-fluid c-homepage">
    <div id="c-hsbc" class="carousel home-c slide" data-interval="false" data-wrap="false">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#c-hsbc" data-slide-to="0" class="active"> </li>
            <li data-target="#c-hsbc" data-slide-to="1"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <!-- Items-->
            <div class="item active"> <img src="/assets/images/homepage/c-image.jpg" alt=" " class="img-fullwidth hidden-xs hidden-sm">
                <div class="carousel-caption">
                    <h1>HSBC Insurance Online</h1>
                    <p>Taking the right steps to plan for the future can be hard. Fortunately, finding time to attain your financial goals online is not.<br> Now's Good.</p>
                </div>
            </div>
            <!-- Items-->
            <div class="item"> 
                <a href="/your-offers/">
                    <img src="/assets/images/homepage/campaigns-homepage-masthead.jpg" alt=" " class="img-fullwidth hidden-xs hidden-sm">
                    <div class="carousel-caption">
                        <h2>Your offers</h2>
                        <p>Getting protected and rewarded concurrently can be hard. Fortunately enjoying such perks with us is not.<br />Now's Good.</p>
                    </div>
                </a>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#c-hsbc" role="button" data-slide="prev"> <span class="icon icon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
        <a class="right carousel-control" href="#c-hsbc" role="button" data-slide="next"> <span class="icon icon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
    </div>
</section>

<section class="container r1-c4 right-border homepage">
    <div class="row is-flex">
        <div class="col-sm-6 col-md-3">
            <h2>Be empowered for the future</h2>
            <p>We help safeguard what you have today to achieve your future aspirations at your convenience.</p>
        </div>
        <div class="col-sm-6 col-md-3">
            <h2>Ensuring more value for you</h2>
            <p>Buying insurance on our non-advisory channel enables you to enjoy cost savings.</p>
        </div>
        <div class="col-sm-6 col-md-3">
            <h2>Insurance that's simple and transparent</h2>
            <p>Our plans are easy to understand with all features and benefits spelt out clearly, so you can have confidence in planning for the future.</p>
        </div>
        <div class="col-sm-6 col-md-3">
            <h2>Our legacy is built on protecting yours</h2>
            <p>With over 140 years of expertise and insights in Singapore, HSBC is here to provide the assurance you need to chart your life goals.</p>
        </div>
    </div>
    <div class="space30"></div>
    <hr> </section>
<div class="space60"></div>

<section class="container header-top0">
    <h2>Our plans</h2>
    <h3>See how our plans can help you build a better future</h3>
    <div class="r1-c2 home-special">
        <div class="row">
            <div class="col-sm-6">
                <div class="tn-img hidden-xs">
                    <a href="/our-plans/online-protector/" data-event="sub-banners" data-product-name="OnlineProtector"><img src="/assets/images/tn-insurance-online-protector.jpg" alt="HSBC Insurance OnlineProtector" class="img-fullwidth"></a>
                </div>
                <h4><a href="/our-plans/online-protector/" data-event="sub-banners" data-product-name="OnlineProtector">HSBC Insurance OnlineProtector<i class="icon icon-chevron-right-small" aria-hidden="true"></i></a></h4>
                <p>A convenient term life insurance plan that gives you guaranteed protection of up to S$1 million sum assured and exclusive premium discounts based on your current lifestyle. You also have the flexibility to stretch your coverage by adding the optional critical illness rider.</p>

            </div>
            <div class="col-sm-6">
                <div class="tn-img hidden-xs">
                    <a href="/our-plans/direct-value-term/" data-event="sub-banners" data-product-name="DirectValueTerm"><img src="/assets/images/tn-direct-value-term.jpg" alt="HSBC Insurance DIRECT - ValueTerm" class="img-fullwidth"></a>
                </div>
                <h4><a href="/our-plans/direct-value-term/" data-event="sub-banners" data-product-name="DirectValueTerm">DIRECT – ValueTerm<i class="icon icon-chevron-right-small" aria-hidden="true"></i></a></h4>
                <p>A hassle-free, essential term life insurance plan with coverage of up to S$400,000 sum assured and flexible policy terms to choose from.</p>
            </div>
        </div>
    </div>
</section>
<div class="space30"></div>
<!--
<section class="container-fluid dream-search" data-dreams-image-id="">
    <div class="container">
        <div class="dream-search-box">
            <h1>What is your dream?</h1>
            <h2>Find out how you can achieve your life goal or aspiration.</h2>
            <div class="form-group">
                <label class="sr-only" for="dreams-search-keywords">I want</label>
                <div class="input-group">
                    <div class="input-group-addon">I want</div>
                    <input type="text" class="form-control" id="dreams-search-keywords">
                </div>
            </div>
            <div class="dream-search-result-actions">
                <p class="dream-search-small">For example: Buy a house, Retire early, Save for my child's education</p>
                <p class="dream-search-small error-message">
                    <i class="icon icon-circle-delete "></i> Oops! We did not catch that. Type in something else.
                </p>
                <a href="/your-dreams/protection-calculator/" class="primary-btn-slate" data-action="goto-calculator">Let's begin</a>
                <div class="dream-suggestion-label">
                    <ul>
                        <li>
                            <p>Did you mean:</p>
                        </li>
                        <li>
                            <a href="#" data-term="start a family">Start a family</a>
                            <a href="#" data-term="save for my wedding">Save for my wedding</a>
                            <a href="#" data-term="start my own business">Start my own business</a>
                            <a href="#" data-term="see Northern Lights">See Northern Lights</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section> -->

<section class="container-fluid dream-search-inner">
    <div class="container">
        <div class="dream-search-box-inner">
            <h2>What is your dream?</h2>
            <h3>Find out how you can achieve your life goal or aspiration.</h3>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">I want</div>
                    <input type="text" class="form-control" id="dreams-search-keywords" aria-describedby="searchKeywords">
                </div>
            </div>
            <div class="dream-search-result-actions">
                <p class="dream-search-small">For example: to buy a house, to retire early, to save for my child's education</p>
                <p class="dream-search-small error-message" aria-label="Error" id="searchKeywords" role="alert" aria-live="assertive" aria-atomic="true">
                    <i class="icon icon-circle-delete" aria-hidden="true"></i> Oops! We did not catch that. Type in something else.
                </p>

                <a href="/your-dreams/protection-calculator/" class="primary-btn-slate" data-action="goto-calculator">Let's begin</a>
                <div class="dream-suggestion-label">
                    <ul>
                        <li>
                            <p>Did you mean:</p>
                        </li>
                        <li>
                           <a href="#" data-action="goto-calculator-btn" data-category="savings" data-term="start a family" data-name="Start a family" data-event="you-mean">Start a family</a>
                            <a href="#" data-action="goto-calculator-btn" data-category="savings" data-term="save for my wedding" data-name="Save for my wedding" data-event="you-mean">Save for my wedding</a>
                            <a href="#" data-action="goto-calculator-btn" data-category="savings" data-term="start my own business" data-name="Start my own business" data-event="you-mean">Start my own business</a>
                            <a href="#" data-action="goto-calculator-btn" data-category="savings" data-term="see Northern Lights" data-name="See Northern Lights" data-event="you-mean">See Northern Lights</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<jsp:include page="/WEB-INF/includes/dreams_footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="faqGroup" value="home" />
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>

<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC - Your Dreams - Homepage",
        "page_url"      : window.location.href,
        "page_type"     : "homepage",
        "page_language" : "en",
        "page_security_level"   :   "0",
        "page_business_line"   :   "Insurance",
        "page_customer_group"   :   "General"
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>

<script>
$(document).ready(function () {
    var $this = $('#c-hsbc');
    $this.children('.right.carousel-control').removeClass('deactive');
    $this.children('.left.carousel-control').addClass('deactive');

    $('#c-hsbc').on('slid.bs.carousel', checkitem);

    function checkitem()// check function
    {
        $this.children('.left.carousel-control').show();
        $this.children('.right.carousel-control').show();
        if ($('.carousel-inner .item:first').hasClass('active')) {
            $this.children('.left.carousel-control').addClass('deactive');
            $this.children('.right.carousel-control').removeClass('deactive');
        } else if ($('.carousel-inner .item:last').hasClass('active')) {
            $this.children('.right.carousel-control').addClass('deactive');
            $this.children('.left.carousel-control').removeClass('deactive');
        }
    }
});
</script>
