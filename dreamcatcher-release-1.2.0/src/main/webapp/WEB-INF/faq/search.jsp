<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty faqs}">
    <li>The FAQs provided are for general information only and do not constitute legal advice. Please read the product reference materials carefully to understand the products’ features and associated risks. </li>
    <c:forEach items="${faqs}" var="faq" varStatus="loop">
        <li class="collapsed">
            <div class="faq-question">
                <i>${loop.index + 1}.</i>
                <span><c:out value="${faq['question']}"></c:out></span>
                <a href="#" class="btn"></a>
                <div class="clearfix"></div>
            </div>
            <span class="faq-answer">
                <c:out value="${faq['answer']}" escapeXml="false"/>
            </span>
        </li>
    </c:forEach>

    <script>
        $('.faq-question').click(function() {
            $('.faq-question').not($(this)).parent().addClass('collapsed');
            $(this).parent().toggleClass('collapsed');
            return false;
        });
    </script>
</c:if>