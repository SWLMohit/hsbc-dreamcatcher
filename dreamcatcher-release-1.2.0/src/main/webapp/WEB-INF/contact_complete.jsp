<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Contact Us"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="contactushomepage"></jsp:param>
</jsp:include>

<!-- Header Image-->
<section class="container-fluid">
    <div class="header-page header-contact-us">
        <div class="container">
            <div class="featured">
                <h1>Contact us</h1>
                <p>Feel free to reach out to us if you require any assistance.</p>
            </div>
        </div>
    </div>
</section>

<!--Get In Touch-->
<div class="container get-in-touch">
    <h1>Here's how to get in touch with us for policy-related inquiries:</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-2 col-sm-1"><img src="/assets/images/contact-us/icon-phone.png" alt="phone" class="img-responsive center-block contact-icon"></div>
            <div class="col-xs-10 col-sm-5">
                <p><strong>Call us:</strong>
                    <br> (65) 6225 6111
                    <br>Monday to Friday, 9am to 5pm</p>
            </div>
            <div class="col-xs-2 col-sm-1"><img src="/assets/images/contact-us/icon-mail.png" alt="Visit Us" class="img-responsive center-block contact-icon"></div>
            <div class="col-xs-10 col-sm-5">
                <p><strong>Email us:</strong>
                    <br><a href="mailto:e-surance@hsbc.com.sg">e-surance@hsbc.com.sg</a></p>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-2 col-sm-1"><img src="/assets/images/contact-us/icon-marker.png" alt="Email" class="img-responsive center-block contact-icon"></div>
            <div class="col-xs-10 col-sm-5">
                <p><strong>Visit us:</strong>
                    <br> 21 Collyer Quay #02-01,
                    <br> Singapore 049320
                    <br> Monday to Friday, 9.30am to 5pm</p>
            </div>
            <div class="col-xs-2 col-sm-1"></div>
            <div class="col-xs-10 col-sm-5">
                <p><strong>Mail us:</strong>
                    <br> HSBC Insurance (Singapore) Pte Ltd
                    <br> Robinson Road Post Office
                    <br> P.O. Box 1538
                    <br>Singapore 903038 </p>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid container-contact-us">
    <div class="space40"></div>
    <section class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Submit a general inquiry, feedback or complaint</h1>
                <h2>Your request has been submitted</h2>
                <p>Thank you for your request. We will get back to you in the next 3 business days.</p>
            </div>
        </div>
    </section>
    <div class="space60"></div>
</section>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>
    <!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC:ContactUs:Form ",
        "page_url"      : window.location.href,
        "page_type"     : "ContactUsForm",
        "page_language" : "en"
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>