<%@ page contentType="text/html; charset=UTF-8"%>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Your Dreams"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="yourdreams-homepage"></jsp:param>
    <jsp:param name="metadataKeywords" value="financial independence, child education, financial goals"></jsp:param>
	<jsp:param name="metadataDescription" value="From financial independence to child education, let us help you to attain your financial goals for every stage of life. Get it started online today."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/your-dreams/"></jsp:param>
</jsp:include>

<section class="container-fluid dream-search-inner">
    <div class="container">
        <div class="dream-search-box-inner">
            <h1>What is your dream?</h1>
            <h2>Find out how you can achieve your life goal or aspiration.</h2>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">I want to</div>
                    <input type="text" class="form-control" name="dreams-search-keywords" id="dreams-search-keywords" aria-describedby="searchKeywords">
                </div>
            </div>
            <div class="dream-search-result-actions">
                <p class="dream-search-small">For example: buy a house, retire early, save for my child&rsquo;s education</p>
                <p class="dream-search-small error-message" aria-label="Error" id="searchKeywords" role="alert" aria-live="assertive" aria-atomic="true">
                    <i class="icon icon-circle-delete" aria-hidden="true"></i> Oops! We did not catch that. Type in something else.
                </p>

                <a href="/your-dreams/protection-calculator/" class="primary-btn-slate" data-action="goto-calculator" data-event="whats-your-dream">Let's begin</a>
                <div class="dream-suggestion-label">
                    <ul>
                        <li>
                            <p>Did you mean:</p>
                        </li>
                        <li>
                            <a href="#" data-action="goto-calculator-btn" data-category="savings" data-term="start a family" data-name="Start a family" data-event="you-mean">Start a family</a>
                            <a href="#" data-action="goto-calculator-btn" data-category="savings" data-term="save for my wedding" data-name="Save for my wedding" data-event="you-mean">Save for my wedding</a>
                            <a href="#" data-action="goto-calculator-btn" data-category="savings" data-term="start my own business" data-name="Start my own business" data-event="you-mean">Start my own business</a>
                            <a href="#" data-action="goto-calculator-btn" data-category="savings" data-term="see Northern Lights" data-name="See Northern Lights" data-event="you-mean">See Northern Lights</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--1 row 4 column-->
<section class="container r1-c4">
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <div class="tn-img">
                <img src="/assets/images/your-dreams/tn-protect.jpg" alt="" class="img-fullwidth">
            </div>
            <div class="box-out">
                <h2>Protect what matters</h2>
                <p>Put in place the right safeguards for your loved ones.</p>
            </div>
            <a href="/your-dreams/protection-calculator/" class="secondary-btn-slate-absolute" data-event="calculate-your-needs">Calculate your needs</a> </div>
        <div class="col-sm-6 col-md-3">
            <div class="tn-img">
                <img src="/assets/images/your-dreams/tn-secure.jpg" alt="" class="img-fullwidth">
            </div>
            <div class="box-out">
                <h2>Secure your child's future</h2>
                <p>Fulfill your child's ambitions by planning ahead of time.</p>
            </div>
            <a href="/your-dreams/education-calculator/" class="secondary-btn-slate-absolute" data-event="calculate-your-needs">Calculate your needs</a> </div>
        <div class="col-sm-6 col-md-3">
            <div class="tn-img">
                <img src="/assets/images/your-dreams/tn-enjoy.jpg" alt="" class="img-fullwidth">
            </div>
            <div class="box-out">
                <h2>Enjoy your retirement</h2>
                <p>Get the right head start to realise your retirement dreams.</p>
            </div>
            <a href="/your-dreams/retirement-calculator/" class="secondary-btn-slate-absolute" data-event="calculate-your-needs">Calculate your needs</a> </div>
        <div class="col-sm-6 col-md-3">
            <div class="tn-img">
                <img src="/assets/images/your-dreams/tn-grow.jpg" alt="" class="img-fullwidth">
            </div>
            <div class="box-out">
                <h2>Grow your savings</h2>
                <p>Take the first step to putting your personal finances on track.</p>
            </div>
            <a href="/your-dreams/savings-calculator/" class="secondary-btn-slate-absolute" data-event="calculate-your-needs">Calculate your needs</a> </div>
    </div>
    <div class="info-text"> The information and/or estimates generated by these calculators should not be construed as an offer to sell or a solicitation of an offer to purchase or subscribe for any insurance or investment nor shall it or any part of it form the basis of, or be relied on in connection with, any contract or commitment whatsoever. </div>
</section>
<div class="space60"></div>

<jsp:include page="/WEB-INF/yourdreams/includes/udo_calculator.jsp" flush="true"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp" flush="true"></jsp:include>

<jsp:include page="/WEB-INF/includes/dreams_footer.jsp"></jsp:include>
<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>
