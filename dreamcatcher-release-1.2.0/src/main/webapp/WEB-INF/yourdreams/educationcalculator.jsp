<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false"%>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Education Calculator"></jsp:param>
    <jsp:param name="pageDescription" value="How prepared are you in ensuring your child's future? Find out using our Education Calculator."></jsp:param>
    <jsp:param name="metadataKeywords" value="education calculator, child's future, simple education calculator"></jsp:param>
	<jsp:param name="metadataDescription" value="Learn about how prepared you are in securing your child's future. Click here to try our simple education calculator now."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/your-dreams/education-calculator/"></jsp:param>
</jsp:include>

<section class="calculators__landing">
    <div class="calculators__landing__img mobile" style="background-image: url(/assets/images/calculators/education/landing--mobile.jpg)"></div>
    <div class="calculators__landing__img desktop" style="background-image: url(/assets/images/calculators/education/landing.jpg)"></div>
    <div class="calculators__landing__main">
        <div class="container calculators__landing__container">
            <div class="calculators__landing__copy">
                <c:choose>  
                    <c:when test="${not empty requestScope.searchText}">
                        <h1>For you to ${requestScope.searchText}...</h1>
                    </c:when>
                    <c:otherwise>
                        <h1>Welcome to our Education Calculator</h1>
                    </c:otherwise>
                </c:choose>
                <p>
                    try our Education Calculator and discover how ready you are to realise this dream.
                </p>
                <button class="btn btn-red js-next"
                   data-hide=".calculators__landing"
                   data-show=".calculator__wrap"
                   data-activate-step="1"
                   data-tracking-start="EducationCalculator"
                   data-name="EducationCalculator"
                   data-event="calculator-start"
                   tabindex="0"
                   data-navigate="#/aboutyou">
                   Start
               </button>
            </div>
		</div>
    </div>
</section>



<div class="calculator__wrap hide">
	<div class="container">
		<div class="calculator__titler">
				<h2>Education Calculator</h2>
				<a href="/your-dreams/" class="buynow-exit-button">Exit</a>
		</div>

		<div class="calculator_breadcrumb" aria-label="breadcrumb">
			<button
					data-activate-step="1"
					data-navigate="#/aboutyou"
					class="current live" aria-current="step"><span><em>1/5</em> Step 1</span></button>
			<button
					data-activate-step="2"
					data-navigate="#/aboutyourchild"
                    aria-disabled="true"
					class="disabled"><span><em>2/5</em> Step 2</span></button>
			<button
					data-activate-step="3"
					data-navigate="#/coverage"
                    aria-disabled="true" 
					class="disabled"><span><em>3/5</em> Step 3</span></button>
			<button
					data-activate-step="4"
					data-navigate="#/liabilities"
                    aria-disabled="true"
					class="disabled"><span><em>4/5</em> Step 4</span></button>
			<button
					data-activate-step="5"
					data-navigate="#/results"
                    aria-disabled="true" 
					class="disabled"><span><em>5/5</em> Results</span></button>
		</div>
	</div>

    <div class="container">
        <!-- STEP 1 -->
        <div class="calculator_step" data-step="1">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>First, let us get to know you and your child better.</legend>

                            <dl class="calculator__fields">
                                <dt><label for="id_name">What&rsquo;s your name?</label></dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <input class="text-box form-control" type="text" id="id_name" autofocus aria-describedby="calc1">
                                    <div class="help-block" id="calc1"><small>Please enter your name</small></div>
                                </dd>

                                <dt><label for="id_age">How old are you?</label></dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <input class="form-control one-decimal-number-input" type="text" id="id_age" aria-describedby="calc2">
                                    <div id="calc2">
                                        <div class="help-block"><small>Please enter your age</small></div>
                                        <div class="help-block age-out-of-range secondary-error"><small>Please enter an age below 83</small></div>
                                    </div>
                                </dd>
                                <dt><label for="id_child_name">
                                    What&rsquo;s your child&rsquo;s name?
                                </label></dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <input class="text-box form-control" type="text" id="id_child_name" aria-describedby="calc3">
                                    <div class="help-block" id="calc3"><small>Please enter your childs' name</small></div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue" name="EducationCalculator">Continue</button>
                            <button data-activate-step="2"
                                    data-navigate="#/aboutyourchild"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-hide=".calculator__wrap"
                                    data-show=".calculators__landing"
                                    data-activate-step="0"
                                    data-navigate=""
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor1 education" style="background-image: url(/assets/images/calculators/education/1-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>As a parent, nothing can be more important than your child&rsquo;s needs. You&rsquo;d be glad to know:</h3>
                            <p>
                                <em>
                                    <img src="/assets/images/calculators/icons-user.svg" alt="">
                                  <span>55%</span>
                                </em>
                                of parents in Singapore prioritise saving for their child&rsquo;s education over their own retirement<sup><span class="visuallyhidden">Footnote:</span>1</sup>.
                            </p>
                        </div>

                        <div class="calculators__btnset">
                            <button data-activate-step="2"
                                    data-navigate="#/aboutyourchild"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote </span>1</sup>HSBC The Value of Education Report 2016</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 1 -->

        <!-- STEP 2 -->
        <div class="calculator_step" data-step="2">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Next, tell us more about <span data-education-calculator-value="childName"></span> and your savings goal.</legend>

                            <dl class="calculator__fields">
                                <dt><label for="id_child_age">How old is <span data-education-calculator-value="childName"></span> now?</label></dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <input class="form-control one-decimal-number-input" type="text" id="id_child_age" aria-describedby="calc4">
                                    <div class="help-block" id="calc4"><small>Please enter a number</small></div>
                                </dd>
                                <dt><label for="id_child_age_funds_needed">At what age would <span data-education-calculator-value="childName"></span> be when university funds are needed?</label></dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <input class="form-control one-decimal-number-input" type="text" id="id_child_age_funds_needed" aria-describedby="calc5">
                                    <div id="calc5">
                                        <div class="help-block"><small>Please enter a number</small></div>
                                        <div class="help-block age-smaller-than secondary-error"><small>Current age has to be smaller than age when funds are needed</small></div>
                                    </div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue">Continue</button>
                            <button data-activate-step="3"
                                    data-navigate="#/coverage"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-activate-step="1"
                                    data-navigate="#/aboutyou"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor2 education" style="background-image: url(/assets/images/calculators/education/2-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Hi <span data-education-calculator-value="name"></span>, is <span data-education-calculator-value="childName"></span> planning to study at a local university? You&rsquo;d be interested to know:</h3>
                            <p>
                                The starting monthly salary for fresh graduates from NUS, NTU and SMU hit a new high of
                                <em>
                                    <img src="/assets/images/calculators/icons-arrow_up.svg" alt="">
                                  <span>S$3,360 </span>
                                </em>
                                in 2016<sup><span class="visuallyhidden">Footnote:</span>2</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="3"
                                    data-navigate="#/coverage"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote </span> 2</sup>The Straits Times article – Fresh grads&rsquo; starting pay hits new high</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 2 -->

        <!-- STEP 3 -->
        <div class="calculator_step" data-step="3">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Moving on, share with us details on <span data-education-calculator-value="childName"></span>&rsquo;s university education.</legend>

                            <dl class="calculator__fields row">
                                <dt><label for="control_country_select" id="control_country_select-label">Which country does <span data-education-calculator-value="childName"></span> plan to study in?</label></dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                  <div class="form-select_wrap">
                                    <select class="form-control control_country_select" id="control_country_select">
                                      <option selected>Singapore</option>
                                      <option>Australia</option>
                                      <option>UK</option>
                                      <option>US</option>
                                      <option value="Other">None of the above</option>
                                    </select>
                                  </div>
                                    <label for="id_country" class="visuallyhidden">Enter a Country</label>
                                    <input class="text-box form-control" id="id_country" aria-placeholder="Enter a country" placeholder="Enter a country" style="display: none"  aria-describedby="calc6">
                                    <div class="help-block" id="calc6"><small>Please select a country</small></div>
                                </dd>
                                <dt><label for="id_course_fees">
                                  How much would <span data-education-calculator-value="childName"></span>&rsquo;s degree <span class="break"></span>cost?
                                </label>
                                  <button type="button" class="help-tooltip" tabindex="0" data-container="body"
                                          data-toggle="popover" data-placement="right"  label="help"
                                          aria-label="The estimated tuition fees for a four-year degree in Singapore, Australia, the UK or the US will be displayed automatically."
                                          data-content="The estimated tuition fees for a four-year degree in Singapore, Australia, the UK or the US will be displayed automatically."><i class="icon icon-circle-help-solid"></i></button>
                                  </dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_course_fees" value="218,009" aria-describedby="calc7">
                                    </div>
                                    <div id="calc7">
                                        <div class="help-block"><small>Please enter a number</small></div>
                                        <div class="help-block cost-out-of-range secondary-error"><small>Please enter a value up to 1,000,000</small></div>
                                    </div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue">Continue</button>
                            <button data-activate-step="4"
                                    data-navigate="#/liabilities"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-activate-step="2"
                                    data-navigate="#/aboutyourchild"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor3 education" style="background-image: url(/assets/images/calculators/education/3-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Ever wondered if parents prefer sending their children for overseas studies? It&rsquo;s been revealed:</h3>
                            <p>
                                <em>
                                    <img src="/assets/images/calculators/icons-users.svg" alt="">
                                  <span>43%</span>
                                </em>
                                of parents in Singapore would consider an overseas university education for their child<sup><span class="visuallyhidden">Footnote:</span>3</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="4"
                                    data-navigate="#/liabilities"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote </span> 3</sup>HSBC The Value of Education Report 2016</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 3 -->


        <!-- STEP 4 -->
        <div class="calculator_step" data-step="4">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Just two more questions left! We&rsquo;d like to know about what you owe and the amount you&rsquo;ve saved. </legend>

                            <dl class="calculator__fields">
                                <dt>
                                    <label for="id_liabilities">
                                        What is your current liability <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="right" aria-label="The value of debt that you owe such as personal loans, credit card, mortgages, etc." data-content="The value of debt that you owe such as personal loans, credit card, mortgages, etc."><i class="icon icon-circle-help-solid"></i></button>?
                                    </label>
                                </dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_liabilities" aria-describedby="calc8">
                                    </div>
                                    <div class="help-block" id="calc8"><small>Please enter a number</small></div>
                                </dd>
                                <dt><label for="id_savings_edu">How much have you saved for <span data-education-calculator-value="childName"></span>&rsquo;s education?</label></dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_savings_edu" aria-describedby="calc9">
                                    </div>
                                    <div class="help-block" id="calc9"><small>Please enter a number</small></div>
                                </dd>
                                <dt>
                                    <label for="id_rate_of_return">
                                        What is the expected rate of return on your savings?
                                    </label>&nbsp;<button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" aria-label="⁴A rate of return is the gain or loss on an investment over a specified time period, expressed as a percentage of the investment's cost. E.g. Average annual returns for the Singapore Savings Bonds (SSBs) if held to the 10-year maturity, will be 2.5 per cent." data-content="⁴A rate of return is the gain or loss on an investment over a specified time period, expressed as a percentage of the investment's cost. E.g. Average annual returns for the Singapore Savings Bonds (SSBs) if held to the 10-year maturity, will be 2.5 per cent."><i class="icon icon-circle-help-solid"></i></button>
                                    </dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <div class="input-group percentage-text-box">
                                        <input class="form-control two-decimal-number-input" type="text" id="id_rate_of_return" aria-describedby="calc10">
                                        <span class="input-group-addon">% p.a.</span>
                                    </div>
                                    <div id="calc10">
                                        <div class="help-block"><small>Please enter a number</small></div>
                                        <div class="help-block rate-over-20 secondary-error"><small>Please enter a value up to 20%</small></div>
                                    </div>
                                </dd>
                            </dl>

                            <dl class="calculator__fields">
                                <dt>
                                    <label for="id_inflation">
                                        <span class="hidden-xs">What is your expected inflation rate? <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁵" aria-label="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁵" data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></span>

                                        <span class="visible-xs">What is your expected <br>inflation rate? <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁵" aria-label="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁵" data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></span>
                                    </label>

                                </dt>
                                <dd class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                                    <div class="input-group percentage-text-box">
                                        <input class="form-control two-decimal-number-input" type="text" id="id_inflation" aria-describedby="calc11">
                                        <span class="input-group-addon">% p.a.</span>
                                    </div>
                                    <div class="help-block" id="calc11"><small>Please enter a number</small></div>

                                </dd>
                            </dl>

                        </fieldset>

                        <div class="calculators__btnset footnote">
                            <div><sup><span class="visuallyhidden">Footnote</span> 4</sup>askST: What are the average annual returns for Singapore Savings Bonds?</div>
                            <div><sup><span class="visuallyhidden">Footnote</span> 5</sup>Core Inflation Rate</div>
                        </div>
                        
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue js-finish"  data-event="calculator-finish" data-name="EducationCalculator">Continue</button>
                            <button data-activate-step="5"
                                    data-navigate="#/results"
                                    class="btn btn-red js-next js-np">See results</button>
                            <button data-activate-step="3"
                                    data-navigate="#/aboutyou"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor4 education" style="background-image: url(/assets/images/calculators/education/4-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Rising operating costs have pushed local university tuition fees higher every year since 2010<sup><span class="visuallyhidden">Footnote:</span>3</sup>. <br>It&rsquo;s been predicted:</h3>
                            <p>
                                A four-year degree in Singapore will cost
                                <em>
                                    <img src="/assets/images/calculators/icons-education.svg" alt="">
                                  <span>70%</span>
                                </em>
                                 of an individual's average yearly<br> income in 2030<sup><span class="visuallyhidden">Footnote:</span>6</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="5"
                                    data-navigate="#/results"
                                    class="btn btn-red js-next js-np">See results</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote </span> 6</sup>The Straits Times article – Cost of university</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 4 -->

        <!-- STEP 5 -->
        <div class="calculator_step" data-step="5">
            <div class="calculator__box calculator__results splitter">
                <div class="splitter__alpha calculator__results__alpha">
                    <div class="education-surplus" style="width:100%">
                        <h2><b>Congratulations, <span data-education-calculator-value="name"></span>! </b>You&rsquo;re on track to securing <span data-education-calculator-value="childName"></span>&rsquo;s future.</h2>
                        <h2 class="surplus-desc">
                            You have a surplus of <b>S$<span data-education-calculator-value="absResult"></span></b>
                            for <span data-education-calculator-value="childName"></span>&rsquo;s education. But we can help to strengthen your financial health.
                        </h2>
                    </div>
                    <div class="education-gap" style="width:100%">
                        <h2><b>Oops, <span data-education-calculator-value="name"></span>!</b> You&rsquo;re off the mark with saving for <span data-education-calculator-value="childName"></span>&rsquo;s education. </h2>
                        <h2 class="surplus-desc">You have a gap of <b>S$<span data-education-calculator-value="absResult"></span></b>
                            to cover before you can successfully fund
                            <span data-education-calculator-value="childName"></span>&rsquo;s education. Not to worry. We can help you stay on track with your savings goal.</h2>
                    </div>

                    <ol class="calculator__results__quickinfo">
                        <li>
                            <img src="/assets/images/calculators/white_icons--user.svg" alt="">
                            <span><span data-education-calculator-value="childName" class="name"></span> is </span>
    													<span>currently</span>
    													<br class="hidden-sm hidden-md hidden-lg">
                            <strong data-education-calculator-value="childAge"></strong>
                            <b class="years">Year</b> <b>Old</b>
                        </li>
                        <li>
                            <img src="/assets/images/calculators/white_icons--coins.svg" alt="">
    													<span>University funds are required when <span data-education-calculator-value="childName" class="name"></span> is</span>
    													<br class="hidden-sm hidden-md hidden-lg">
                            <strong data-education-calculator-value="childFutureAge"></strong>
                        </li>
                        <li>
                            <img src="/assets/images/calculators/white_icons--education.svg" alt="">
    													<span><span data-education-calculator-value="childName" class="name"></span> will start university education in </span>
    													<br class="hidden-sm hidden-md hidden-lg">
                            <strong data-education-calculator-value="yearsToFunding"></strong>
                            <b class="years quote">Years&rsquo;</b> <b>Time</b>
                        </li>
                    </ol>
                    <div class="social__sharing">
                        <span>Share:</span>
                        <a href="#" class="calculator__share fb js-facebook-share" data-sharing-message="How prepared are you in securing your child's future? Find out using our Education Calculator." target="_blank">Facebook</a>
                        <a href="#" class="calculator__share tw js-tw-share" data-sharing-message="How prepared are you in securing your child's future? Find out using our Education Calculator." target="_blank">Twitter</a>
                        <a href="mailto:?subject=Will you be able to fund your child’s education?&body=I’ve just used the HSBC Education Calculator to discover how prepared I am in securing my child’s future. Try it now for yourself. https://insuranceonline.hsbc.com.sg/your-dreams/education-calculator/" class="calculator__share em">Email</a>
                        <a href="#" class="calculator__share pr js-print">Print</a>
                    </div>
                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__results__beta">
                    <div class="calculator__results__beta_wrap">
    					<p>Adjust the slider to see how you can better save for <span data-education-calculator-value="childName"></span>&rsquo;s education.</p>
    					<br>
                        <p><label for="id_cost">The expected cost of <span data-education-calculator-value="childName"></span>&rsquo;s education</label></p>
                        <div class="form-group" data-name="EducationCalculator" data-event="yourdreams">
                            <div class="input-group currency-text-box">
                                <span class="input-group-addon">S$</span>
                                <input class="form-control slider-text-box number-only-input" data-type="currency-programmatic-range-number" value="218009" id="id_cost" aria-describedby="calc12">
                            </div>
                            <div id="calc12">
                                <div class="help-block"><small>Please enter a number</small></div>
                                <div class="help-block cost-out-of-range secondary-error"><small>Please enter a value up to 9,999,999,999</small></div>
                            </div>
                        </div>
                        <div class="form-group" data-name="EducationCalculator" data-event="yourdreams" aria-hidden="true" tabindex="-1">
                            <input class="currency-range" type="hidden" data-type="currency-programmatic-range" min="0" max="1000000" step="1000" value="218009">
                            <div class="clearfix">
                                <small class="min pull-left">S$0</small>
                                <small class="max pull-right">S$1,000,000</small>
                            </div>
                        </div>
                        <div class="form-group">
                        <button class="btn btn-slate js-recalculate" style="margin-top: -1em;">Recalculate</button>
						</div>
                        <aside>
                            <p>
                                <span class="hidden-xs">
                                    <strong>View our plans to get a quote<br> or explore our other calculators:</strong>
                                </span>
                                <span class="visible-xs">
                                    <strong>View our plans to get a quote or explore our other calculators:</strong>
                                </span>
                            </p>
                            <div class="other_calculators">
                              <a class="btn btn-red" href="<c:url value="/our-plans/"/>">Our plans</a>
                              <a class="btn btn-white" href="<c:url value="/your-dreams/protection-calculator/"/>">Protection</a>
                              <a class="btn btn-white" href="<c:url value="/your-dreams/savings-calculator/"/>">Savings</a>
                              <a class="btn btn-white" href="<c:url value="/your-dreams/retirement-calculator/"/>">Retirement</a>
                            </div>
                        </aside>
                    </div>
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 5 -->

    </div><!-- /.container -->
</div>

<jsp:include page="/WEB-INF/yourdreams/includes/footer.jsp"></jsp:include>

<jsp:include page="/WEB-INF/yourdreams/includes/udo_edu_calculator.jsp" flush="true"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp" flush="true"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
