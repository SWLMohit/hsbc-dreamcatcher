<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false"%>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Protection Calculator"></jsp:param>
    <jsp:param name="pageDescription" value="How ready are you in safeguarding the future of your loved ones? Find out using our Protection Calculator."></jsp:param>
    <jsp:param name="metadataKeywords" value="protection coverage, protection calculator, online protection calculator"></jsp:param>
	<jsp:param name="metadataDescription" value="Discover how ready you are in safeguarding the future of your loved ones. Click here to try our online protection calculator now."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/your-dreams/protection-calculator/"></jsp:param>
</jsp:include>

<section class="calculators__landing">
    <div class="calculators__landing__img mobile" style="background-image: url(/assets/images/calculators/protection/landing--mobile.png)"></div>
    <div class="calculators__landing__img desktop" style="background-image: url(/assets/images/calculators/protection/landing.jpg)"></div>
    <div class="calculators__landing__main">
        <div class="container calculators__landing__container">
            <div class="calculators__landing__copy">
				<c:choose>
					<c:when test="${not empty requestScope.searchText}">
						<h1>For you to ${requestScope.searchText}...</h1>
					</c:when>
					<c:otherwise>
						<h1>Welcome to our Protection Calculator</h1>
					</c:otherwise>
				</c:choose>
				<p>
                    Discover how ready you are in safeguarding the future of your loved ones.
                </p>

                <button class="btn btn-red js-next"
                   data-hide=".calculators__landing"
                   data-show=".calculator__wrap"
                   data-activate-step="1"
                   data-name="ProtectionCalculator"
                   data-event="calculator-start"
                   tabindex="0"
                   data-navigate="#/aboutyou">Start</button>
            </div>
        </div>
    </div>
</section>

<div class="calculator__wrap hide">
    <div class="container">
        <div class="calculator__titler">
            <h2>Protection Calculator</h2>
            <a href="/your-dreams/" class="buynow-exit-button">Exit</a>
        </div>
    
        <div class="calculator_breadcrumb" aria-label="breadcrumb">
            <button
                data-activate-step="1"
                data-navigate="#/aboutyou"
                aria-current="step"
                class="current live"><span><em>1/4</em> Step 1</span></button>
            <button
                data-activate-step="2"
                data-navigate="#/liabilities"
                aria-disabled="true"
                class="disabled"><span><em>2/4</em> Step 2</span></button>
            <button
                data-activate-step="3"
                data-navigate="#/coverage"
                aria-disabled="true"
                class="disabled"><span><em>3/4</em> Step 3</span></button>
            <button
                data-activate-step="4"
                data-navigate="#/results"
                aria-disabled="true"
                class="disabled"><span><em>4/4</em> Results</span></button>
        </div>
    </div>

    <div class="container">
        <!-- STEP 1 -->
        <div class="calculator_step" data-step="1">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>First, let us get to know you better.</legend>

                            <dl class="calculator__fields">
                                <dt><label for="id_name">What&rsquo;s your name?</label></dt>
                                <%--<div class="date-error has-error">--%>
                                    <%--<span class="help-block">Invalid date</span>--%>
                                <%--</div>--%>
                                <dd class="form-group" data-name="ProtectionCalculator" data-event="yourdreams">
                                    <input class="text-box form-control" type="text" id="id_name" autofocus aria-describedby="calc1">
                                    <div class="help-block" id="calc1"><small>Please enter your name</small></div>
                                </dd>

                                <dt><label for="id_age">How old are you?</label></dt>
                                <dd class="form-group" data-name="ProtectionCalculator" data-event="yourdreams">
                                    <input class="form-control one-decimal-number-input" type="text" id="id_age" aria-describedby="calc2">
                                    <div id="calc2">
                                        <div class="help-block"><small>Please enter your age</small></div>
                                        <div class="help-block age-out-of-range secondary-error"><small>Please enter an age below 83</small></div>
                                    </div>
                                </dd>
                                <dt><label for="id_income">

                                    How much do you earn each year?
                                </label>&nbsp;<button type="button" class="help-tooltip" tabindex="0" data-container="body"
                                            data-toggle="popover" data-placement="top"
                                            aria-label="This includes your salary and income from any alternative sources such as investments, rental and others across the year."
                                            data-content="This includes your salary and income from any alternative sources such as investments, rental and others across the year."><i class="icon icon-circle-help-solid"></i></button>
                                </dt>
                                <dd class="form-group" data-name="ProtectionCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_income" aria-describedby="calc3">
                                    </div>
                                    <div class="help-block" id="calc3"><small>Please enter a number</small></div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue" name="ProtectionCalculator">Continue</button>
                            <button data-activate-step="2"
                                    data-navigate="#/liabilities"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-hide=".calculator__wrap"
                                    data-show=".calculators__landing"
                                    data-activate-step="0"
                                    data-navigate=""
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor1 protection" style="background-image: url(/assets/images/calculators/protection/1-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Are you financially prepared for life&rsquo;s uncertainties? You&rsquo;d be interested <br> to know:</h3>
                            <p>
                                <em>
                                    <img src="/assets/images/calculators/icons-suit_tie.svg" alt="">
                                  <span>60%</span>
                                </em>
                                of Singaporeans have savings and other financial safeguards in place to prepare for unforeseen events<sup><span class="visuallyhidden">Footnote:</span>1</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="2"
                                    data-navigate="#/liabilities"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 1</sup>HSBC Power of Protection Report 2016</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 1 -->

        <!-- STEP 2 -->
        <div class="calculator_step" data-step="2">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Next, share with us details on what you owe.</legend>

                            <dl class="calculator__fields">
                                <dt>
                                    <label for="id_liabilities">
                                        What is your current liability <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="right" aria-label="The value of debt that you owe such as personal loans, credit card, mortgages, etc." data-content="The value of debt that you owe such as personal loans, credit card, mortgages, etc."><i class="icon icon-circle-help-solid"></i></button>?
                                    </label>
                                </dt>
                                <dd class="form-group" data-name="ProtectionCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_liabilities" aria-describedby="calc4">
                                    </div>
                                    <div class="help-block" id="calc4"><small>Please enter a number</small></div>

                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue">Continue</button>
                            <button data-activate-step="3"
                                    data-navigate="#/coverage"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-activate-step="1"
                                    data-navigate="#/aboutyou"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor2 protection" style="background-image: url(/assets/images/calculators/protection/2-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Hi <span data-protection-calculator-value="name"></span>, ever wondered what makes up the bulk of loans in Singapore?<br> It&rsquo;s been revealed:</h3>
                            <p>
                                Mortgage loans make up
                                <em>
                                    <img src="/assets/images/calculators/icons-coins.svg" alt="">
                                  <span>75%</span>
                                </em>
                                of household liabilities for Singaporeans<sup><span class="visuallyhidden">Footnote:</span>2</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="3"
                                    data-navigate="#/coverage"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 2</sup>SingStat – Household Sector Balance Sheet</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 2 -->

        <!-- STEP 3 -->
        <div class="calculator_step" data-step="3">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>A couple more questions to go! Let&rsquo;s talk about your coverage and how you&rsquo;re supporting your family.</legend>

                            <dl class="calculator__fields">
                                <dt><label for="id_insurance_coverage">What is your current life insurance coverage?</label></dt>
                                <dd class="form-group" data-name="ProtectionCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text"
                                               id="id_insurance_coverage" aria-describedby="calc5">
                                    </div>
                                    <div class="help-block" id="calc5"><small>Please enter a number</small></div>
                                </dd>

                                <dt><label for="id_dependants">How many more years do you need to support your dependants for?</label></dt>
                                <dd class="form-group" data-name="ProtectionCalculator" data-event="yourdreams">
                                    <input class="form-control two-digit-text-box number-only-input" type="text" id="id_dependants" aria-describedby="calc6">
                                    <div id="calc6">
                                        <div class="help-block"><small>Please enter a number</small></div>
                                        <div class="help-block dependants-out-of-range secondary-error"><small>Please enter a duration up to 50</small></div>
                                    </div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue js-finish" data-event="calculator-finish" data-name="ProtectionCalculator">Continue</button>
                            <button data-activate-step="4"
                                    data-navigate="#/results"
                                    class="btn btn-red js-next js-np">See results</button>
                            <button data-activate-step="2"
                                    data-navigate="#/liabilities"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor3 protection" style="background-image: url(/assets/images/calculators/protection/3-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>To know if your insurance coverage is enough, follow this rule of thumb:</h3>
                            <p>
                                Your basic life coverage should be approximately
                                <em>
                                    <img src="/assets/images/calculators/icons-target.svg" alt="">
                                  <span>11 times</span>
                                </em>
                                your annual income<sup><span class="visuallyhidden">Footnote:</span>3</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="4"
                                    data-navigate="#/results"
                                    class="btn btn-red js-next js-np">See results</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 3</sup>Life Insurance Association Singapore – Frequently Asked Questions</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 3 -->

        <!-- STEP 4 -->
        <div class="calculator_step" data-step="4">
            <div class="calculator__box calculator__results splitter">
                <div class="splitter__alpha calculator__results__alpha">
                    <div class="protection-surplus" style="width:100%">
                        <h2><b>Congratulations, <span data-protection-calculator-value="name"></span>!</b> You&rsquo;re on track to safeguarding the future of your loved ones.</h2>
                        <h2 class="surplus-desc">
                            You have a surplus of  <b>S$<span data-protection-calculator-value="absCoverageRequired"></span></b>
                            for your protection coverage. But we can help to strengthen your financial health.
                        </h2>
                    </div>
                    <div class="protection-gap" style="width:100%">
                        <h2><b>Oops, <span data-protection-calculator-value="name"></span>!</b> Your protection coverage is currently off the mark.</h2>
                        <h2 class="surplus-desc">You&rsquo;ll need an ideal coverage of <b>S$<span data-protection-calculator-value="absCoverageRequired"></span></b>
                            to help protect your loved ones. Not to worry. We can help you get on the right track.</h2>
                    </div>

                    <ol class="calculator__results__quickinfo">
                        <li>
                            <img src="/assets/images/calculators/white_icons--user.svg" alt="">
                            <span>You are </span>
                            <span>currently</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-protection-calculator-value="age"></strong>
                            <b class="years">Year</b> <b>Old</b>
                        </li>
                        <li>
                            <img src="/assets/images/calculators/white_icons--time.svg" alt="">
                            <span>You are</span>
                            <span>left with</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-protection-calculator-value="duration"></strong>
                            <span><b class="years">Years</b> to support</span>
                            <span>your dependants</span>
                        </li>
                        <li>
                            <img src="/assets/images/calculators/white_icons--users.svg" alt="">
                            <span>At the</span>
                            <span>age of</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-protection-calculator-value="endingAge"></strong>
                            <span>you will no longer have
                            to support
                            your dependants</span>
                        </li>
                    </ol>
                    <div class="social__sharing">
                        <span>Share:</span>
                        <a href="#" class="calculator__share fb js-facebook-share" data-sharing-message="How ready are you in safeguarding the future of your loved ones? Find out using our Protection Calculator." target="_blank">Facebook</a>
                        <a href="#" class="calculator__share tw js-tw-share" data-sharing-message="How ready are you in safeguarding the future of your loved ones? Find out using our Protection Calculator." target="_blank">Twitter</a>
                        <a href="mailto:?subject=Are your loved ones well-protected?&body=I’ve just tried the HSBC Protection Calculator to discover how ready I am in safeguarding my loved ones’ future. Try it now for yourself. https://insuranceonline.hsbc.com.sg/your-dreams/protection-calculator/" class="calculator__share em">Email</a>
                        <a href="#" class="calculator__share pr js-print">Print</a>
                    </div>
                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__results__beta">
                    <div class="calculator__results__beta_wrap">
                        <p>Adjust the slider to determine the coverage
                            that best suits your needs.</p>
                        <br>
                        <p><label for="id_duration">Number of years left to support your dependants</label></p>
                        <div class="form-group" data-name="ProtectionCalculator" data-event="yourdreams">
                            <div class="programmatic-range-number__labelled">
                                <input class="form-control slider-text-box dependant-years-input number-only-input two-digit-text-box" data-type="year-programmatic-range-number" value="25" id="id_duration" aria-describedby="calc7">
                                <small>years</small>
                            </div>
                            <div id="calc7">
                                <div class="help-block"><small>Please enter a number</small></div>
                                <div class="help-block dependants-out-of-range secondary-error"><small>Please enter a duration up to 50</small></div>
                            </div>
                        </div>
                        <div class="form-group" data-name="ProtectionCalculator" data-event="yourdreams" aria-hidden="true" tabindex="-1">
                            <input class="year-range" type="hidden" data-type="year-programmatic-range" min="0" max="50" step="1" value="25">
                            <div class="clearfix">
                                <small class="min pull-left">0 years</small>
                                <small class="max pull-right">50 years</small>
                            </div>
                        </div>
                        <div class="form-group">
                        <button class="btn btn-slate js-recalculate" style="margin-top: -1em;">Recalculate</button>
						</div>
                        <aside>
                            <p>
                                <span class="hidden-xs">
                                    <strong>View our plans to <a href="/our-plans/online-protector#quick_quote">get a quote</a><br> or explore our other calculators:</strong>
                                </span>
                                <span class="visible-xs">
                                    <strong>View our plans to <a href="/our-plans/online-protector#quick_quote">get a quote</a> or explore our other calculators:</strong>
                                </span>
                            </p>
                            <div class="other_calculators">
                                <a class="btn btn-red" href="<c:url value="/our-plans/"/>">Our plans</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/retirement-calculator/"/>">Retirement</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/education-calculator/"/>">Education</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/savings-calculator/"/>">Savings</a>
                            </div>
                        </aside>
                    </div>

                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 4 -->


    </div><!-- /.container -->
</div>

<jsp:include page="/WEB-INF/yourdreams/includes/footer.jsp"></jsp:include>

<jsp:include page="/WEB-INF/yourdreams/includes/udo_prot_calculator.jsp" flush="true"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp" flush="true"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
