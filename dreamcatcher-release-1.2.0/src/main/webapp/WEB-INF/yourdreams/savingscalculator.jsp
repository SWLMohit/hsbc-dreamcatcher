<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false"%>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Savings Calculator"></jsp:param>
    <jsp:param name="pageDescription" value="How ready are you in taking charge of your finances? Find out using our Savings Calculator."></jsp:param>
    <jsp:param name="metadataKeywords" value="savings calculator, financial health, hsbc savings calculator"></jsp:param>
	<jsp:param name="metadataDescription" value="Wonder how ready you are to take charge of your financial aspirations? Click here to try our savings calculator now."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/your-dreams/savings-calculator/"></jsp:param>
</jsp:include>

<section class="calculators__landing">
    <div class="calculators__landing__img mobile" style="background-image: url(/assets/images/calculators/savings/landing--mobile.jpg)"></div>
        <div class="calculators__landing__img desktop" style="background-image: url(/assets/images/calculators/savings/landing.jpg)"></div>
    <div class="calculators__landing__main">
        <div class="container calculators__landing__container">
            <div class="calculators__landing__copy">
				<c:choose>
					<c:when test='${not empty requestScope.searchText}'>
						<h1>For you to ${requestScope.searchText}...</h1>
					</c:when>
					<c:otherwise>
						<h1>Welcome to our Savings Calculator</h1>
					</c:otherwise>
				</c:choose>
				<p>
                    try our Savings Calculator and discover how ready you are to realise this dream.
                </p>
                <button class="btn btn-red js-next"
                   data-hide=".calculators__landing"
                   data-show=".calculator__wrap"
                   data-activate-step="1"
                   data-name="SavingsCalculator"
                   data-event="calculator-start"
                   tabindex="0"
                   data-navigate="#/aboutyou">Start</button>
            </div>
        </div>
    </div>
</section>

<div class="calculator__wrap hide">
    <div class="container">
        <div class="calculator__titler">
            <h2>Savings Calculator</h2>
            <a href="/your-dreams/" class="buynow-exit-button">Exit</a>
        </div>
    
        <div class="calculator_breadcrumb" aria-label="breadcrumb">
            <button
                data-activate-step="1"
                data-navigate="#/aboutyou"
                aria-current="step"
                class="current live"><span><em>1/4</em> Step 1</span></button>
            <button
                data-activate-step="2"
                data-navigate="#/liabilities"
                aria-disabled="true"
                class="disabled"><span><em>2/4</em> Step 2</span></button>
            <button
                data-activate-step="3"
                data-navigate="#/coverage"
                aria-disabled="true"
                class="disabled"><span><em>3/4</em> Step 3</span></button>
            <button
                data-activate-step="4"
                data-navigate="#/results"
                aria-disabled="true"
                class="disabled"><span><em>4/4</em> Results</span></button>
        </div>
    </div>
    <div class="container">
        <!-- STEP 1 -->
        <div class="calculator_step" data-step="1">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>First, let us get to know you better.</legend>

                            <dl class="calculator__fields">
                                <dt><label for="id_name">What&rsquo;s your name?</label></dt>
                                <dd class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                                    <input class="text-box form-control" type="text" id="id_name" autofocus aria-describedby="calc1">
                                    <div class="help-block" id="calc1"><small>Please enter your name</small></div>
                                </dd>

                                <dt><label for="id_age">How old are you?</label></dt>
                                <dd class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                                    <input class="one-decimal-number-input form-control savings_age" type="text" id="id_age" aria-describedby="calc2">
                                    <div class="help-block" id="calc2"><small>Please enter your age</small></div>
                                </dd>
                                <dt><label for="id_goal">
                                    How much do you need to achieve your dream?
                                </label></dt>
                                <dd class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_goal" aria-describedby="calc3">
                                    </div>
                                    <div  id="calc3">
                                        <div class="help-block"><small>Please enter a number</small></div>
                                        <div class="help-block savings-out-of-range secondary-error"><small>Please enter a value from 1 to 5,000,000</small></div>
                                    </div>

                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue" name="SavingsCalculator">Continue</button>
                            <button data-activate-step="2"
                                    data-navigate="#/liabilities"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-hide=".calculator__wrap"
                                    data-show=".calculators__landing"
                                    data-activate-step="0"
                                    data-navigate=""
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor1 savings" style="background-image: url(/assets/images/calculators/savings/1-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Curious about how Singaporeans prioritise savings? You&rsquo;d be interested to know:</h3>
                            <p>
                                <em>
                                    <img src="/assets/images/calculators/icons-target.svg" alt="">
                                  <span>53%</span>
                                </em>
                                of Singaporeans consider saving as a top financial priority<sup><span class="visuallyhidden">Footnote:</span>1</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="2"
                                    data-navigate="#/liabilities"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 1</sup>The Straits Times article – Investments of Singaporeans overweight with cash: Survey</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 1 -->

        <!-- STEP 2 -->
        <div class="calculator_step" data-step="2">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Next, tell us more about your savings goal.</legend>

                            <dl class="calculator__fields">
                                <dt><label for="id_length">In how many years would you need the money?</label></dt>
                                <dd class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                                    <input class="form-control two-digit-text-box number-only-input" type="text" id="id_length" aria-describedby="calc4">
                                    <div class="help-block" id="calc4"><small>Please enter a number</small></div>
                                    <%--<div class="help-block savings-years-out-of-range secondary-error"><small>Please enter a duration of 99 or below</small></div>--%>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue">Continue</button>
                            <button data-activate-step="3"
                                    data-navigate="#/coverage"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-activate-step="1"
                                    data-navigate="#/aboutyou"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor2 savings" style="background-image: url(/assets/images/calculators/savings/2-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Hi <span data-savings-calculator-value="name"></span>, if your financial concerns relate to security for the long run, you&rsquo;re not alone. The fact is:</h3>
                            <p>
                                Long-term financial security is the
                                <em>
                                    <img src="/assets/images/calculators/icons-umbrella.svg" alt="">
                                  <span>2<sup>ND</sup>most</span>
                                </em>
                                important concern for Singaporeans<sup><span class="visuallyhidden">Footnote:</span>2</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="3"
                                    data-navigate="#/coverage"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 2</sup>HSBC Power of Protection Report 2016</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 2 -->

        <!-- STEP 3 -->
        <div class="calculator_step" data-step="3">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Just one more question left! We&rsquo;d like to know how you foresee your savings potential to be.</legend>

                            <dl class="calculator__fields">
                                <dt>
                                    <label for="id_rate_of_return">What is the expected rate of return on your savings?
                                    </label>&nbsp;<button type="button" class="help-tooltip" tabindex="0"  label="help" data-container="body" data-toggle="popover" data-placement="top" aria-label="³A rate of return is the gain or loss on an investment over a specified time period, expressed as a percentage of the investment's cost. E.g. Average annual returns for the Singapore Savings Bonds (SSBs) if held to the 10-year maturity, will be 2.5 per cent." data-content="³A rate of return is the gain or loss on an investment over a specified time period, expressed as a percentage of the investment's cost. E.g. Average annual returns for the Singapore Savings Bonds (SSBs) if held to the 10-year maturity, will be 2.5 per cent."><i class="icon icon-circle-help-solid"></i></button>
                                </dt>
                                <dd class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                                    <div class="input-group percentage-text-box">
                                        <input class="form-control two-decimal-number-input" type="text" id="id_rate_of_return" aria-describedby="calc5">
                                        <span class="input-group-addon">% p.a.</span>
                                    </div>
                                    <div id="calc5">
                                        <div class="help-block"><small>Please enter a number</small></div>
                                        <div class="help-block rate-over-20 secondary-error"><small>Please enter a value up to 20%</small></div>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="calculator__fields">
                                <dt>
                                    <label for="id_inflation">
                                        <span class="hidden-xs">What is your expected inflation rate? <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁴" aria-label="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁴" data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></span>

                                        <span class="visible-xs">What is your expected <br>inflation rate? <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁴" aria-label="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁴" data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></span>
                                    </label>

                                </dt>
                                <dd class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                                    <div class="input-group percentage-text-box">
                                        <input class="form-control two-decimal-number-input" type="text" id="id_inflation" aria-describedby="calc6">
                                        <span class="input-group-addon">% p.a.</span>
                                    </div>
                                    <div class="help-block" id="calc6"><small>Please enter a number</small></div>

                                </dd>
                            </dl>

                        </fieldset>

                        <div class="calculators__btnset footnote">
                            <p class="footnote"><sup><span class="visuallyhidden">Footnote </span> 3</sup>askST: What are the average annual returns for Singapore Savings Bonds?</p>
                            <p><sup><span class="visuallyhidden">Footnote </span> 4</sup>Core Inflation Rate</p>
                        </div>

                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue js-finish" data-event="calculator-finish" data-name="SavingsCalculator">Continue</button>
                            <button data-activate-step="4"
                                    data-navigate="#/results"
                                    class="btn btn-red js-next js-np">See results</button>
                            <button data-activate-step="2"
                                    data-navigate="#/liabilities"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor3 savings" style="background-image: url(/assets/images/calculators/savings/3-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Building your wealth for the future? You&rsquo;d be pleased to know: </h3>
                            <p>
                                Singapore&rsquo;s wealth per adult is estimated to grow
                                <em>
                                  <img src="/assets/images/calculators/icons-graph.svg" alt="">
                                  <span>2.2%</span>
                                </em>
                                each year to reach
                                <em>
                                    <img src="/assets/images/calculators/icons-coins.svg" alt="">
                                  <span>US$309,000</span>
                                </em>
                                in 2021<sup><span class="visuallyhidden">Footnote:</span>5</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="4"
                                    data-navigate="#/results"
                                    class="btn btn-red js-next js-np">See results</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 5</sup>The Straits Times article – Singapore household wealth up despite slowing economy</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 3 -->

        <!-- STEP 4 -->
        <div class="calculator_step" data-step="4">
            <div class="calculator__box calculator__results splitter">
                <div class="splitter__alpha calculator__results__alpha">
                    <h2><b>Hey, <span data-savings-calculator-value="name"></span>.</b> Here&rsquo;s a snapshot
                        of your financial health.</h2>
                    <h2 class="surplus-desc">You&rsquo;ll require a monthly saving of <b>S$<span data-savings-calculator-value="monthlyCommitment"></span></b>
                        to achieve your savings goal. We can help you get on track.</h2>
                    <ol class="calculator__results__quickinfo">
                        <li>
                            <img src="/assets/images/calculators/white_icons--user.svg" alt="">
                            <span>You are </span>
                            <span>currently</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-savings-calculator-value="age"></strong>
                            <b class="years">Year</b> <b>Old</b>
                        </li>
                        <li>
                            <img src="/assets/images/calculators/white_icons--time.svg" alt="">
                            <span>It would </span>
                            <span>take you</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-savings-calculator-value="duration"></strong>
                            <span><b class="years">Years</b> to save</span>
                        </li>
                        <li>
                            <img src="/assets/images/calculators/white_icons--coins.svg" alt="">
                            <span>You will achieve</span>
                            <span>your savings</span>
                            <span>goal at</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-savings-calculator-value="endingAge"></strong>
                        </li>
                    </ol>
                    <div class="social__sharing">
                        <span>Share:</span>
                        <a href="#" class="calculator__share fb js-facebook-share" data-sharing-message="How ready are you in taking charge of your finances? Find out using our Savings Calculator." target="_blank">Facebook</a>
                        <a href="#" class="calculator__share tw js-tw-share" data-sharing-message="How ready are you in taking charge of your finances? Find out using our Savings Calculator." target="_blank">Twitter</a>
                        <a href="mailto:?subject=Are you in control of your finances?&body=I’ve just used the HSBC Savings Calculator to determine if my financial aspirations are on track. Try it now for yourself. https://insuranceonline.hsbc.com.sg/your-dreams/savings-calculator/" class="calculator__share em">Email</a>
                        <a href="#" class="calculator__share pr js-print">Print</a>
                    </div>
                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__results__beta">
                    <div class="calculator__results__beta_wrap">
                        <p>Adjust the sliders to see how you can better save for the future.</p>
                        <br>
                        <p><label for="id_savings">The amount you want to save:</label></p>
                        <div class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                            <div class="input-group currency-text-box">
                                <span class="input-group-addon">S$</span>
                                <input class="form-control slider-text-box number-only-input" data-type="currency-programmatic-range-number" value="2500000" id="id_savings" aria-describedby="calc7">
                            </div>
                            <div id="calc7">
                                <div class="help-block"><small>Please enter a number</small></div>
                                <div class="help-block savings-out-of-range secondary-error"><small>Please enter a value from 1 to 5,000,000</small></div>
                            </div>
                        </div>
                        <div class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                            <input class="currency-range" type="hidden" data-type="currency-programmatic-range" min="0" max="5000000" step="1000" value="2500000">
                            <div class="clearfix">
                                <small class="min pull-left">S$0</small>
                                <small class="max pull-right">S$5,000,000</small>
                            </div>
                        </div>
                        
                        <p><label for="id_years">The number of years it takes:</label></p>
                        <div class="form-group" data-name="SavingsCalculator" data-event="yourdreams">
                            <div class="programmatic-range-number__labelled">
                                <input class="form-control slider-text-box two-digit-text-box number-only-input" data-type="year-programmatic-range-number" value="25" id="id_years" aria-describedby="calc8">
                                <small>years</small>
                            </div>
                            <div class="help-block" id="calc8"><small>Please enter a number</small></div>
                            <%--<div class="help-block savings-years-out-of-range secondary-error"><small>Please enter a duration of 99 or below</small></div>--%>
                        </div>
                        <div class="form-group" data-name="SavingsCalculator" data-event="yourdreams" aria-hidden="true" tabindex="-1">
                            <input class="year-range" type="hidden" data-type="year-programmatic-range" min="0" max="99" step="1" value="25">
                            <div class="clearfix">
                                <small class="min pull-left">0 years</small>
                                <small class="max pull-right">99 years</small>
                            </div>
                        </div>
                        <div class="form-group">
                        <button class="btn btn-slate js-recalculate" style="margin-top: -1em;">Recalculate</button>
                        </div>
                        <aside>
                            <p>
                                <span class="hidden-xs">
                                    <strong>View our plans to get a quote<br> or explore our other calculators: </strong>
                                </span>
                                <span class="visible-xs">
                                    <strong>View our plans to get a quote or explore our other calculators: </strong>
                                </span>
                            </p>
                            <div class="other_calculators">
                                <a class="btn btn-red" href="<c:url value="/our-plans/"/>">Our plans</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/protection-calculator/"/>">Protection</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/education-calculator/"/>">Education</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/retirement-calculator/"/>">Retirement</a>
                            </div>
                        </aside>
                    </div>

                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 4 -->

    </div><!-- /.container -->
</div>

<jsp:include page="/WEB-INF/yourdreams/includes/footer.jsp"></jsp:include>

<jsp:include page="/WEB-INF/yourdreams/includes/udo_sav_calculator.jsp" flush="true"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp" flush="true"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
