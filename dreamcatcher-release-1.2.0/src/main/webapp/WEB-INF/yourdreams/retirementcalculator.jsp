<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false"%>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Retirement Calculator"></jsp:param>
    <jsp:param name="pageDescription" value="How ready are you in achieving your retirement dream? Find out using our Retirement Calculator."></jsp:param>
    <jsp:param name="metadataKeywords" value="retirement planning, retirement calculator, easy retirement calculator"></jsp:param>
	<jsp:param name="metadataDescription" value="You can now find out if you are on track to doing your retirement planning in just five simple steps. Click here to try our easy retirement calculator now."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/your-dreams/retirement-calculator/"></jsp:param>
</jsp:include>

<section class="calculators__landing">
    <div class="calculators__landing__img mobile" style="background-image: url(/assets/images/calculators/retirement/landing--mobile.jpg)"></div>
    <div class="calculators__landing__img desktop" style="background-image: url(/assets/images/calculators/retirement/landing.jpg)"></div>
    <div class="calculators__landing__main">
        <div class="container calculators__landing__container">
            <div class="calculators__landing__copy">
				<c:choose>
					<c:when test="${not empty requestScope.searchText}">
						<h1>For you to ${requestScope.searchText}...</h1>
					</c:when>
					<c:otherwise>
						<h1>Welcome to our Retirement Calculator</h1>
					</c:otherwise>
				</c:choose>
				<p>try our Retirement Calculator and discover how ready you are to realise this dream.</p>
                <button class="btn btn-red js-next"
                   data-hide=".calculators__landing"
                   data-show=".calculator__wrap"
                   data-activate-step="1"
                   data-name="RetirementCalculator"
                   data-event="calculator-start"
                   tabindex="0"
                   data-navigate="#/aboutyou">Start</button>
            </div>
        </div>
    </div>
</section>

<div class="calculator__wrap hide">
    <div class="container calculator-header-steps">
        <div class="calculator__titler">
            <h2>Retirement Calculator</h2>
            <a href="/your-dreams/" class="buynow-exit-button">Exit</a>
        </div>
    
        <div class="calculator_breadcrumb" aria-label="breadcrumb">
            <button
                data-activate-step="1"
                data-navigate="#/aboutyou"
                aria-current="step"
                class="current live"><span><em>1/6</em> Step 1</span></button>
            <button
                data-activate-step="2"
                data-navigate="#/liabilities"
                aria-disabled="true"
                class="disabled"><span><em>2/6</em> Step 2</span></button>
            <button
                data-activate-step="3"
                data-navigate="#/coverage"
                aria-disabled="true"
                class="disabled"><span><em>3/6</em> Step 3</span></button>
            <button
                data-activate-step="4"
                data-navigate="#/returns"
                aria-disabled="true"
                class="disabled"><span><em>4/6</em> Step 4</span></button>
            <button
                data-activate-step="5"
                data-navigate="#/spending"
                aria-disabled="true"
                class="disabled"><span><em>5/6</em> Step 5</span></button>
            <button
                data-activate-step="6"
                data-navigate="#/results"
                aria-disabled="true"
                class="disabled"><span><em>6/6</em> Results</span></button>
        </div>
    </div>

    <div class="container">
        <!-- STEP 1 -->
        <div class="calculator_step" data-step="1">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>First, let us get to know you better.</legend>

                            <dl class="calculator__fields retirement__calc">
                                <dt><label for="id_name">What&rsquo;s your name?</label></dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <input class="text-box form-control" type="text" id="id_name" autofocus aria-describedby="calc1">
                                    <div class="help-block" id="calc1"><small>Please enter your name</small></div>
                                </dd>

                                <dt><label for="id_age">How old are you?</label></dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <input class="one-decimal-number-input form-control" type="text" id="id_age" aria-describedby="calc2">
                                    <div id="calc2">
                                        <div class="help-block"><small>Please enter your age</small></div>
                                        <div class="help-block age-out-of-range secondary-error"><small>Please enter an age below 83</small></div>
                                    </div>
                                </dd>
                                <dt><label for="id_retirement_age">
                                    At what age would you like to retire?
                                </label></dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <input class="one-decimal-number-input form-control" type="text" id="id_retirement_age" aria-describedby="calc3">
                                    <div id="calc3">
                                        <div class="help-block"><small>Please enter your age</small></div>
                                        <div class="help-block age-out-of-range secondary-error"><small>Please enter an age below 83</small></div>
                                        <div class="help-block age-smaller-than secondary-error"><small>Retirement age has to be larger than current age</small></div>
                                    </div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue" name="RetirementCalculator">Continue</button>
                            <button data-activate-step="2"
                                    data-navigate="#/liabilities"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-hide=".calculator__wrap"
                                    data-show=".calculators__landing"
                                    data-activate-step="0"
                                    data-navigate=""
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor1 retirement" style="background-image: url(/assets/images/calculators/retirement/1-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Does a good retirement require careful planning? If you think so, you&rsquo;re not alone.<br> You&rsquo;d be glad to know:</h3>
                            <p>
                                <em>
                                    <img src="/assets/images/calculators/icons-suit_tie.svg" alt="">
                                  <span>53%</span>
                                </em>
                                of working adults who are financially informed feel they will enjoy a happy retirement<sup><span class="visuallyhidden">Footnote:</span>1</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="2"
                                    data-navigate="#/liabilities"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 1</sup>HSBC The Future of Retirement Report 2016</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 1 -->

        <!-- STEP 2 -->
        <div class="calculator_step" data-step="2">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Next, tell us more about your earnings and expenditure.</legend>

                            <dl class="calculator__fields">
                                <dt><label for="id_income">

                                  How much do you earn each <span class="break"></span>month?
                                </label><button type="button" class="help-tooltip" tabindex="0" data-container="body"
                                            data-toggle="popover" label="help" data-placement="top"
                                            aria-label="This includes your salary and income from any alternative sources such as investments, rental and others."
                                            data-content="This includes your salary and income from any alternative sources such as investments, rental and others."><i class="icon icon-circle-help-solid"></i></button>
                                </dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_income" aria-describedby="calc4">
                                    </div>
                                    <div class="help-block" id="calc4"><small>Please enter a number</small></div>
                                </dd>
                                <dt><label for="id_spending">How much do you spend each month?</label></dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_spending" aria-describedby="calc5">
                                    </div>
                                    <div class="help-block" id="calc5"><small>Please enter a number</small></div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue">Continue</button>
                            <button data-activate-step="3"
                                    data-navigate="#/coverage"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-activate-step="1"
                                    data-navigate="#/aboutyou"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor2 retirement" style="background-image: url(/assets/images/calculators/retirement/2-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy retirement-step-2">
                            <h3>Hi <span data-retirement-calculator-value="name"></span>, ever wondered how much you&rsquo;d need for a comfortable retirement? <br> Here&rsquo;s a tip:</h3>
                            <p>
                                It&rsquo;s estimated that retirees require
                                <em>
                                    <img src="/assets/images/calculators/icons-coins.svg" alt="">
                                  <span>60% to 70% </span>
                                </em>
                               of their pre-retirement income <br>to live comfortably<sup><span class="visuallyhidden">Footnote:</span>2</sup>.
                               </p>

                            <br>

                            <p>
                                This means that you&rsquo;ll need
                            <em class="monthlyIncome-retirement-step-2"><img src="/assets/images/calculators/icons-umbrella.svg" alt="">
                                <span class="monthlyIncome6070">
                                    S$<span data-retirement-calculator-value="monthlyIncome60"></span>
                                    <br>to<br>
                                    S$<span data-retirement-calculator-value="monthlyIncome70"></span>
                                </span>
                            </em>
                            per month.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="3"
                                    data-navigate="#/coverage"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 2</sup>TODAY article – Saving for retirement</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 2 -->

        <!-- STEP 3 -->
        <div class="calculator_step" data-step="3">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Share with us details on what you owe and own.</legend>

                            <dl class="calculator__fields">
                                <dt>
                                    <label for="id_liabilities">
                                        What is your current liability <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="right" aria-label="The value of debt that you owe such as personal loans, credit card, mortgages, etc." data-content="The value of debt that you owe such as personal loans, credit card, mortgages, etc."><i class="icon icon-circle-help-solid"></i></button>?
                                    </label>
                                </dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_liabilities" aria-describedby="calc6">
                                    </div>
                                    <div class="help-block" id="calc6"><small>Please enter a number</small></div>
                                </dd>
                                <dt><label for="id_investment_worth">How much are your existing investments worth?</label></dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_investment_worth" aria-describedby="calc7">
                                    </div>
                                    <div class="help-block" id="calc7"><small>Please enter a number</small></div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue">Continue</button>
                            <button data-activate-step="4"
                                    data-navigate="#/returns"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-activate-step="2"
                                    data-navigate="#/aboutyou"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor3 retirement" style="background-image: url(/assets/images/calculators/retirement/3-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Need to keep your finances in check? Do keep this general rule in mind:</h3>
                            <p>
                                Your monthly debt should not exceed
                                <em>
                                    <img src="/assets/images/calculators/icons-target.svg" alt="">
                                  <span>35%</span>
                                </em>
                                of your gross monthly income<sup><span class="visuallyhidden">Footnote:</span>3</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="4"
                                    data-navigate="#/returns"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 3</sup>MoneySENSE article – Getting Out of Debt</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 3 -->

        <!-- STEP 4 -->
        <div class="calculator_step" data-step="4">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Moving on, we&rsquo;d like to know what you foresee your savings potential to be. </legend>

                            <dl class="calculator__fields">
                                <dt>

                                    <label for="id_rate_of_return">What is the expected rate of return on your savings?</label>
                                    <button type="button" class="help-tooltip" tabindex="0" label="help" data-container="body" data-toggle="popover" data-placement="top" aria-label="⁴A rate of return is the gain or loss on an investment over a specified time period, expressed as a percentage of the investment's cost. E.g. Average annual returns for the Singapore Savings Bonds (SSBs) if held to the 10-year maturity, will be 2.5 per cent." data-content="⁴A rate of return is the gain or loss on an investment over a specified time period, expressed as a percentage of the investment's cost. E.g. Average annual returns for the Singapore Savings Bonds (SSBs) if held to the 10-year maturity, will be 2.5 per cent."><i class="icon icon-circle-help-solid"></i></button>
                                  </dt>

                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <div class="input-group percentage-text-box">
                                        <input class="form-control two-decimal-number-input" type="text" id="id_rate_of_return" aria-describedby="calc8"> <!-- slider text box that changes this also using same id-->
                                        <span class="input-group-addon">% p.a.</span>
                                    </div>
                                    <div id="calc8">
                                        <div class="help-block"><small>Please enter a number</small></div>
                                        <div class="help-block rate-over-20 secondary-error"><small>Please enter a value up to 20%</small></div>
                                    </div>

                                </dd>
                            </dl>
                            <dl class="calculator__fields">
                                <dt>
                                  <label for="id_inflation">
                                    <span class="hidden-xs">What is your expected inflation rate? <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁵" aria-label="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁵" data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></span>

                                    <span class="visible-xs">What is your expected <br>inflation rate? <button type="button" class="help-tooltip" tabindex="0" data-container="body" data-toggle="popover" data-placement="top" label="help" data-content="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁵" aria-label="Inflation Rate in Singapore averaged 1.42 percent from 2009 until 2018⁵" data-original-title="" title=""><i class="icon icon-circle-help-solid"></i></button></span>
                                  </label>
                                </dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <div class="input-group percentage-text-box">
                                        <input class="form-control two-decimal-number-input" type="text" id="id_inflation" aria-describedby="calc9">
                                        <span class="input-group-addon">% p.a.</span>
                                    </div>
                                    <div class="help-block" id="calc9"><small>Please enter a number</small></div>

                                </dd>
                            </dl>
                        </fieldset>

                        <div class="calculators__btnset footnote">
                            <div><span class="visuallyhidden">Footnote</span><sup> 4</sup>askST: What are the average annual returns for Singapore Savings Bonds?</div>
                            <div><span class="visuallyhidden">Footnote</span><sup> 5</sup>Core Inflation Rate</div>
                        </div>

                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue">Continue</button>
                            <button data-activate-step="5"
                                    data-navigate="#/spending"
                                    class="btn btn-red js-next js-np">Next</button>
                            <button data-activate-step="3"
                                    data-navigate="#/aboutyou"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor4 retirement" style="background-image: url(/assets/images/calculators/retirement/4-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>How prepared are pre-retirees for their golden years?<br> You&rsquo;d be surprised to know:</h3>
                            <p>
                                <em>
                                    <img src="/assets/images/calculators/icons-user.svg" alt="">
                                  <span>77%</span>
                                </em>
                                of pre-retirees have started saving for their retirement<sup><span class="visuallyhidden">Footnote:</span>6</sup>.
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="5"
                                    data-navigate="#/spending"
                                    class="btn btn-red js-next js-np">Next</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 6</sup>HSBC The Future of Retirement Report 2016</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 4 -->

        <!-- STEP 5 -->
        <div class="calculator_step" data-step="5">
            <div class="calculator__box calculator__form splitter">
                <div class="splitter__alpha calculator__form__alpha">
                    <form>
                        <fieldset>
                            <legend>Almost there! Let&rsquo;s talk about your spending when you retire.</legend>

                            <dl class="calculator__fields">
                                <dt><label for="id_monthly_expenses">

                                  How much will you spend each month during retirement?
                              </label>&nbsp;<button type="button" class="help-tooltip" tabindex="0" data-container="body"
                                            data-toggle="popover" data-placement="top" label="help"

                                            aria-label="This includes day-to-day expenses for food, transport, utilities as well as loan repayments, allowance for dependants and taxes."
                                            data-content="This includes day-to-day expenses for food, transport, utilities as well as loan repayments, allowance for dependants and taxes."><i class="icon icon-circle-help-solid"></i></button>
                                  </dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_monthly_expenses" aria-describedby="calc10"> <!-- slider text box that changes this also using same id-->
                                    </div>
                                    <div id="calc10">
                                        <div class="help-block"><small>Please enter a number</small></div>
                                        <div class="help-block mth-spending-out-of-range secondary-error"><small>Please enter a value up to 500,000</small></div>
                                    </div>
                                </dd>

                                <dt><label for="id_expenses">If you&rsquo;re planning for any big-ticket expenses, how much would you spend?</label></dt>
                                <dd class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                                    <div class="input-group currency-text-box">
                                        <span class="input-group-addon">S$</span>
                                        <input class="form-control number-only-input" type="text" id="id_expenses" aria-describedby="calc11">
                                    </div>
                                    <div class="help-block" id="calc11"><small>Please enter a number</small></div>
                                </dd>
                            </dl>

                        </fieldset>
                        <div class="calculators__btnset">
                            <button type="submit" class="btn btn-red js-continue js-finish"  data-event="calculator-finish" data-name="RetirementCalculator">Continue</button>
                            <button data-activate-step="6"
                                    data-navigate="#/results"
                                    class="btn btn-red js-next js-np">See results</button>
                            <button data-activate-step="4"
                                    data-navigate="#/liabilities"
                                    type="button" class="back_btn js-next">Back</button>
                        </div>
                    </form>

                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__form__beta bgcolor5 retirement" style="background-image: url(/assets/images/calculators/retirement/5-factoid-1280x1280-ratio1-1.jpg);">
                    <!-- HIDE BELOW IF INACTIVE -->
                    <div class="calculator__form__beta__inner">
                        <div class="calculator__form__beta__inner__copy">
                            <h3>Depending on lifestyle, retirement expenses may vary. But here&rsquo;s a monthly estimate based on basic needs:</h3>
                            <p>
                                Entertainment + Utility and phone bills + Public transport + Food =
                                <em>
                                    <img src="/assets/images/calculators/icons-coins.svg" alt="">
                                  <span>S$1,200<sup><span class="visuallyhidden">Footnote:</span>7</sup></span>
                                </em>
                            </p>
                        </div>
                        <div class="calculators__btnset">
                            <button data-activate-step="6"
                                    data-navigate="#/results"
                                    class="btn btn-red js-next js-np">See results</button>
                        </div>
                        <p class="footnote"><sup><span class="visuallyhidden">Footnote</span> 7</sup>Channel News Asia article – Retirement planning: How much is enough?</p>
                    </div>
                    <!-- HIDE ABOVE IF INACTIVE -->
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 5 -->

        <!-- STEP 6 -->
        <div class="calculator_step" data-step="6">
            <div class="calculator__box calculator__results splitter">
                <div class="splitter__alpha calculator__results__alpha">
                    <div class="retirement-surplus" style="width:100%">
                        <h2><b>Congratulations, <span data-retirement-calculator-value="name"></span>!</b> Your retirement
                            dreams are on track.</h2>
                        <h2 class="surplus-desc">
                            You have a surplus of <b>S$<span data-retirement-calculator-value="absResult"></span></b>
                            <button type="button" class="help-tooltip" tabindex="0" data-container="body"
                                    data-toggle="popover" data-placement="top"  label="help"
                                    aria-label="Based on an inflation rate of 2%."
                                    data-content="Based on an inflation rate of 2%."><i
                                    class="icon icon-circle-help-solid"></i></button>
                            for your retirement. But we can help to strengthen your financial health.
                        </h2>
                    </div>
                    <div class="retirement-gap" style="width:100%">
                        <h2><b>Oops, <span data-retirement-calculator-value="name"></span>!</b> Your retirement dreams are
                            off the mark.</h2>
                        <h2 class="surplus-desc">
                            You have a deficit of S$<span data-retirement-calculator-value="absResult"></span>
                            <button type="button" class="help-tooltip" tabindex="0" data-container="body"
                                    data-toggle="popover" data-placement="top"  label="help"
                                    aria-label="Based on an inflation rate of 2%."
                                    data-content="Based on an inflation rate of 2%."><i
                                    class="icon icon-circle-help-solid"></i></button>
                            for your retirement. Don't worry, we can help you get the right head start.
                        </h2>
                    </div>
                    <ol class="calculator__results__quickinfo">
                        <li>
                            <img src="/assets/images/calculators/white_icons--user.svg" alt="">
                            <span>You are</span>
                            <span>currently</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-retirement-calculator-value="age"></strong>
                            <b class="years">Years</b> <b>Old</b>
                        </li>
                        <li>
                            <img src="/assets/images/calculators/white_icons--time.svg" alt="">
                            <span>You plan to</span>
                            <span>retire at</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-retirement-calculator-value="retirementAge"></strong>
                        </li>
                        <li>
                            <img src="/assets/images/calculators/white_icons--house.svg" alt="">
                            <span>You are planning</span>
                            <span>for retirement in</span>
                            <br class="hidden-sm hidden-md hidden-lg">
                            <strong data-retirement-calculator-value="yearsToRetire"></strong>
                            <b class="years quote">Years&rsquo;</b> <b>Time</b>
                        </li>
                    </ol>
                    <p class="footnote"><sup><span class="visuallyhidden">Footnote </span> 8</sup>askST: What are the average annual returns for Singapore Savings Bonds?</p>
                    <div class="social__sharing">
                        <span>Share:</span>
                        <a href="#" class="calculator__share fb js-facebook-share" data-sharing-message="How ready are you in achieving your retirement dream? Find out using our Retirement Calculator." target="_blank">Facebook</a>
                        <a href="#" class="calculator__share tw js-tw-share" data-sharing-message="How ready are you in achieving your retirement dream? Find out using our Retirement Calculator." target="_blank">Twitter</a>
                        <a href="mailto:?subject=Are you well prepared for retirement?&body=I’ve just tried the HSBC Retirement Calculator to see if my retirement dream is on track. Try it now for yourself. https://insuranceonline.hsbc.com.sg/your-dreams/retirement-calculator/" class="calculator__share em">Email</a>
                        <a href="#" class="calculator__share pr js-print">Print</a>
                    </div>
                </div><!-- /.alpha -->
                <div class="splitter__beta calculator__results__beta">
                    <div class="calculator__results__beta_wrap">
                        <p>Adjust the sliders to see how you can be better prepared for retirement.</p>
                        <br>
                        <p>
                            <label for="id_rate_of_return_slider">Rate of return</label>
                            <button type="button" class="help-tooltip" tabindex="0" label="help" data-container="body" data-toggle="popover" data-placement="top" aria-label="⁸A rate of return is the gain or loss on an investment over a specified time period, expressed as a percentage of the investment's cost. E.g. Average annual returns for the Singapore Savings Bonds (SSBs) if held to the 10-year maturity, will be 2.5 per cent." data-content="⁸A rate of return is the gain or loss on an investment over a specified time period, expressed as a percentage of the investment's cost. E.g. Average annual returns for the Singapore Savings Bonds (SSBs) if held to the 10-year maturity, will be 2.5 per cent."><i class="icon icon-circle-help-solid"></i></button>
                        </p>
                        <div class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                            <div class="input-group percentage-text-box">
                                <input class="form-control slider-text-box two-decimal-number-input" data-type="percentage-programmatic-range-number" value="4" id="id_rate_of_return_slider" aria-describedby="calc12">
                                <span class="input-group-addon">% p.a.</span>
                            </div>
                            <div id="calc12">
                                <div class="help-block"><small>Please enter a number</small></div>
                                <div class="help-block rate-over-20 secondary-error"><small>Please enter a value up to 20%</small></div>
                            </div>
                        </div>
                        <div class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                            <input class="percentage-range" type="hidden" data-type="percentage-programmatic-range" min="0" max="20" step="0.01" value="4.75">
                            <div class="clearfix">
                                <small class="min pull-left">0% p.a.</small>
                                <small class="max pull-right">20% p.a.</small>
                            </div>
                        </div>

                        <p><label for="id_retirement_age_slider">Planned retirement age</label></p>
                        <div class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                            <div class="programmatic-range-number__labelled">
                                <input class="form-control slider-text-box one-decimal-number-input" data-type="year-programmatic-range-number" value="25" id="id_retirement_age_slider" aria-describedby="calc13">
                                <small>years old</small>
                            </div>
                            <div id="calc13">
                                <div class="help-block"><small>Please enter a number</small></div>
                                <div class="help-block age-out-of-range secondary-error"><small>Please enter an age below 83</small></div>
                                <div class="help-block age-smaller-than secondary-error"><small>Retirement age has to be larger than current age</small></div>
                            </div>
                        </div>
                        <div class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                            <input class="year-range" type="hidden" data-type="year-programmatic-range" min="0" max="82" step="1" value="25">
                            <div class="clearfix">
                                <small class="min pull-left">0</small>
                                <small class="max pull-right">82</small>
                            </div>
                        </div>

                    <p>
                        <label for="id_monthly_expenses_slider">Monthly expenses</label>
                        <button type="button" class="help-tooltip" tabindex="0" data-container="body"
                                data-toggle="popover" data-placement="top"  label="help"
                                aria-label="These include day-to-day expenses for food, transport, utilities as well as loan repayments, allowance for dependants and taxes."
                                data-content="These include day-to-day expenses for food, transport, utilities as well as loan repayments, allowance for dependants and taxes."><i class="icon icon-circle-help-solid"></i></button>
                      during retirement
                    </p>
                    <div class="form-group" data-name="RetirementCalculator" data-event="yourdreams">
                        <div class="input-group currency-text-box">
                            <span class="input-group-addon">S$</span>
                            <input class="form-control slider-text-box number-only-input" data-type="currency-programmatic-range-number" value="25000" id="id_monthly_expenses_slider" aria-describedby="calc14">
                        </div>
                        <div id="calc14">
                            <div class="help-block"><small>Please enter a number</small></div>
                            <div class="help-block mth-spending-out-of-range secondary-error"><small>Please enter a value up to 500,000</small></div>
                        </div>

                        </div>
                        <div class="form-group" data-name="RetirementCalculator" data-event="yourdreams" aria-hidden="true" tabindex="-1">
                            <input class="currency-range" type="hidden" data-type="currency-programmatic-range" min="0" max="500000" step="1000" value="25000">
                            <div class="clearfix">
                                <small class="min pull-left">S$0</small>
                                <small class="max pull-right">S$500,000</small>
                            </div>
                        </div>
                        <div class="form-group">
                        <button class="btn btn-slate js-recalculate" style="margin-top: -1em;">Recalculate</button>
						</div>
                        <aside>
                            <p>
                                <span class="hidden-xs">
                                    <strong>View our plans to get a quote<br> or explore our other calculators:</strong>
                                </span>
                                <span class="visible-xs">
                                    <strong>View our plans to get a quote or explore our other calculators:</strong>
                                </span>
                            </p>
                            <div class="other_calculators">
                                <a class="btn btn-red" href="<c:url value="/our-plans/"/>">Our plans</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/protection-calculator/"/>">Protection</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/education-calculator/"/>">Education</a>
                                <a class="btn btn-white" href="<c:url value="/your-dreams/savings-calculator/"/>">Savings</a>
                            </div>
                        </aside>
                    </div>
                </div><!-- /.beta -->
            </div>
        </div>
        <!-- //STEP 6 -->


    </div><!-- /.container -->
</div>

<jsp:include page="/WEB-INF/yourdreams/includes/footer.jsp"></jsp:include>

<jsp:include page="/WEB-INF/yourdreams/includes/udo_ret_calculator.jsp" flush="true"></jsp:include>
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp" flush="true"></jsp:include>

<jsp:include page="/WEB-INF/includes/footer.jsp"></jsp:include>
