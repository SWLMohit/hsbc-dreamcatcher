<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Terms of Use"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="Terms of Use"></jsp:param>
    <jsp:param name="metadataKeywords" value="terms of use, trademarks and copyright, hsbc terms of use"></jsp:param>
	<jsp:param name="metadataDescription" value="Check out the Terms of Use of HSBC Insurance's website here. Details include the use of information and materials, copyright and trademarks etc."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/terms-of-use/"></jsp:param>
</jsp:include>

<section class="container">
    <h1>HSBC Insurance's Terms of Use</h1>
    <p>This section contains the Terms of Use of this website. By accessing this website and any of its pages, you are agreeing to these Terms.</p>
    <p><strong>Use of Information and Materials</strong><br>
    These pages should not be regarded as an offer or solicitation to sell investments or make deposits in any country to any person to whom it is unlawful to make such an invitation or solicitation in such country.</p>
    <p>The information contained in these pages is not intended to provide professional advice. Persons accessing these pages should obtain appropriate professional advice when necessary.</p>
    <p><strong>Copyright and Trademarks</strong><br>
    HSBC Insurance and other parties own the trademarks, logos and service marks displayed on this website. These may not be used without the written permission of HSBC Insurance or the party owning these. Materials on this site are protected by copyright. No part of these materials may be modified, reproduced, stored in a retrieval system, transmitted, copied, distributed or used in any other way for commercial or public purposes without HSBC Insurance's prior written consent.</p>
    <p><strong>No Warranties</strong><br>
    Whilst every care has been taken in preparing the information materials contained in this website, such information and materials are provided "as is" without warranty of any kind, either express or implied. In particular, no warranty regarding non-infringement, security, accuracy, fitness for a purpose or freedom from computer viruses is given in connection with such information and materials.</p>
    <p><strong>Linked Websites</strong><br>
    HSBC Insurance or any other HSBC Group member is not responsible for the contents available on or the set-up of any other websites linked to this site. Access to and use of such other websites is at the user's own risk and subject to any terms and conditions applicable to such access/use. By providing hyperlinks to other websites, HSBC Insurance shall not be deemed to endorse, recommend, approve, guarantee or introduce any third parties or the service/products they provide on their web site, or have any form of cooperation with such third parties and web sites. HSBC Insurance is not a party to any contractual arrangements entered into between you and the provider of the external website unless otherwise expressly specified or agreed to by HSBC Insurance.</p>
    <p><strong>E-mail</strong><br>
    Email messages sent to HSBC Insurance over the Internet cannot be guaranteed to be completely secure. HSBC Insurance is not responsible for any damages incurred by users if they send a message to HSBC Insurance, or if HSBC Insurance sends a message to them at their request, over the Internet. HSBC Insurance is not responsible in any way for direct, indirect, special or consequential damages arising out of the use of this website.</p>
    <p><strong>Transmitting over The Internet</strong><br>
    Due to the nature of the Internet transactions may be subject to interruption, transmission blackout, delayed transmission and incorrect data transmission. HSBC Insurance is not liable for malfunctions in communications facilities not under its control that may affect the accuracy or timeliness of messages and transactions you send.</p>
    <p><strong>Downloading</strong><br>
    We do not represent or warrant that the Site will be available and meet your requirements, that access will not be interrupted, that there will be no delays, failures, errors or omissions or loss of transmitted information, that no viruses or other contaminating or destructive properties will be transmitted or that no damage will occur to your computer system. You have sole responsibility for adequate protection and back up of data and/or equipment and for undertaking reasonable and appropriate precautions to scan for computer viruses or other destructive properties. We make no representations or warranties regarding the accuracy, functionality or performance of any third party software that may be used in connection with the Site.</p>
</section>

<div class="space60"></div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>

<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC:TermsOfUse",
        "page_url"      : window.location.href,
        "page_type"     : "Information Pages",
        "page_language" : "en",
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>