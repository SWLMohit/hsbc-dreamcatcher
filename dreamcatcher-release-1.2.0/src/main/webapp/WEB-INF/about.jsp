<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="About Us"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="abouthsbcinsurancehompage"></jsp:param>
    <jsp:param name="metadataKeywords" value="insurance faq, online insurance faq, online insurance info"></jsp:param>
	<jsp:param name="metadataDescription" value="Check out the answers to your most frequently asked questions of HSBC's online insurance plans. Info include eligibility, service pledge and more."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/about-us/"></jsp:param>
</jsp:include>

<!-- Header Image-->
<section class="container-fluid">
    <div class="header-page header-about">
        <div class="container">
            <div class="featured">
                <h1>About HSBC Insurance Online</h1>
                <p>Making major life decisions can be hard. <br>Fortunately, planning for your financial future online is not. Now's Good.</p>
            </div>
        </div>
    </div>
</section>

<!--1 row 4 column-->
<section class="container r1-c4 aboutus">
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <div class="tn-img">
                <img src="/assets/images/about/about-4-1.jpg" alt="" class="img-fullwidth">
            </div>
            <h2>It's easy</h2>
            <p>Enjoy the ease of buying insurance on our platform, designed to make financial planning a simple step-by-step process.</p>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="tn-img">
                <img src="/assets/images/about/about-4-2.jpg" alt="" class="img-fullwidth">
            </div>
            <h2>It's fast</h2>
            <p>Maximise the convenience of getting a quick quote in just a few clicks. Next, complete your application and submit it online seamlessly.</p>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="tn-img">
                <img src="/assets/images/about/about-4-3.jpg" alt="" class="img-fullwidth">
            </div>

            <h2>It’s secure</h2>
            <%--Text updation as a part of HSBCIDCU-926 --%>
            <p>With our rigorous IT infrastructure as well as stringent data protection and privacy regulations, rest assured that any personal data shared with us is guarded with utmost confidentiality.</p>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="tn-img">
                <img src="/assets/images/about/about-4-4.jpg" alt="" class="img-fullwidth">
            </div>

            <h2>It's HSBC</h2>
            <p>HSBC is one of the world's largest banking and financial services organisations that serves around 38 million customers worldwide. We operate through long-established businesses and an international network of around 3,900 offices in 67 countries and territories.</p>
        </div>
    </div>
    <div class="space30 hidden-xs"></div>
</section>
<div class="container">
    <hr>
</div>
<div class="space60"></div>
<!--    Serving You Better-->
<section class="container serving-your-better">
    <h2>Serving you better</h2>
    <div class="panel-group panel-hsbc" id="notes" role="tablist" aria-multiselectable="true">
        <!-- Data-->
        <div class="panel panel-default hsbc-panel">
            <div class="panel-heading hsbc-heading" role="tab" id="heading">
                <h3 class="panel-title hsbc-title"> <a class="accordion" role="button" data-toggle="collapse" data-parent="#notes" href="#collapse1" aria-expanded="true" aria-controls="notes"> 30-day Service Pledge</a>
                </h3>
            </div>
            <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading">
                <div class="panel-body hsbc-content">We believe in giving you more time to evaluate an insurance plan. That is why we offer a 30-day free-look period instead of the usual 14 days. So, if you decide to change your mind after purchasing a plan, inform us and we will waive or refund the related fees or charges.</div>
            </div>
        </div>
        <!-- Data-->
        <div class="panel panel-default hsbc-panel">
            <div class="panel-heading hsbc-heading" role="tab" id="heading">
                <h3 class="panel-title hsbc-title"> <a class="collapsed accordion" role="button" data-toggle="collapse" data-parent="#notes" href="#collapse2" aria-expanded="true" aria-controls="notes"> Ease of claim reimbursement</a> </h3> </div>
            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading">
                <div class="panel-body hsbc-content">Submit your <a href="/eclaims/">claim online</a> to save yourself time and hassle. Upon receiving all required documents and the claim being admitted, we will reimburse you within 10 days. Alternatively, you can visit any of our Customer Service Centres to make a claim in person.</div>
            </div>
        </div>
        <!-- Data-->
        <div class="panel panel-default hsbc-panel">
            <div class="panel-heading hsbc-heading" role="tab" id="heading">
                <h3 class="panel-title hsbc-title"> <a class="collapsed accordion" role="button" data-toggle="collapse" data-parent="#notes" href="#collapse3" aria-expanded="true" aria-controls="notes"> Our financial strength</a> </h3> </div>
            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading">

                <div class="panel-body hsbc-content">HSBC Insurance Singapore's financial strength and credibility are reflected in our total investable assets of S$6.7 billion as at 31 December 2016, and an A+ rating from Standard & Poor's as at 31 August 2017.                        
                </div>
            </div>
        </div>
    </div>
</section>
<div class="space60"></div>
<div class="container">
    <hr>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="faqGroup" value="about" />
</jsp:include>

<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC:About",
        "page_url"      : window.location.href,
        "page_type"     : "Information Pages",
        "page_language" : "en",
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>
