<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Contact Us"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="contactushomepage"></jsp:param>
	<jsp:param name="metadataKeywords" value="hsbc insurance hotline, insurance policies, hsbc insurance contact"></jsp:param>
	<jsp:param name="metadataDescription" value="Call, email, write or visit to get in touch with HSBC Insurance today for any inquiries, feedback, and advices."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/contact-us/"></jsp:param>
</jsp:include>

<!-- Header Image-->
<section class="container-fluid">
    <div class="header-page header-contact-us">
        <div class="container">
            <div class="featured">
                <h1>Contact us</h1>
                <p>Feel free to reach out to us if you require any assistance.</p>
            </div>
        </div>
    </div>
</section>

<!--Get In Touch-->
<div class="container get-in-touch">

    <h2>Here’s how to get in touch with us for inquiries about our insurance policies:</h2>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-2 col-sm-1"><img src="/assets/images/contact-us/icon-phone.png" alt="" class="center-block contact-icon"></div>

            <div class="col-xs-10 col-sm-5">
                <p><strong>Hotline:</strong>
                    <br> <a href="tel:6562256111" target="_blank" data-event="call-us">(65) 6225 6111</a>
                    <br>Monday to Friday, 9am to 5pm</p>
            </div>
            <div class="col-xs-2 col-sm-1"><img src="/assets/images/contact-us/icon-mail.png" alt="" class="center-block contact-icon"></div>
            <div class="col-xs-10 col-sm-5">
                <p><strong>Email Address:</strong>
                    <br><a href="mailto:e-surance@hsbc.com.sg" data-event="email-us">e-surance@hsbc.com.sg</a></p>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-2 col-sm-1"><img src="/assets/images/contact-us/icon-marker.png" alt="" class="center-block contact-icon"></div>

            <div class="col-xs-10 col-sm-5">
                <p><strong>Location:</strong>
                    <br> 21 Collyer Quay #02-01,
                    <br> Singapore 049320
                    <br> Monday to Friday, 9.30am to 5pm</p>
            </div>
            <div class="col-xs-2 col-sm-1"></div>
            <div class="col-xs-10 col-sm-5">
                <p><strong>Mailing Address:</strong>
                    <br> HSBC Insurance (Singapore) Pte Ltd
                    <br> Robinson Road Post Office
                    <br> P.O. Box 1538
                    <br>Singapore 903038 </p>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid container-contact-us">
    <div class="space40"></div>
    <form id="contact-us-form" method="post" action="/contact-us/" data-form-name="contactus">
        <section class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Submit a general inquiry, feedback or complaint</h2>
                    <h3>To maintain security and confidentiality of your policies, we will not act on any policy related queries and instructions received through this form.</h3>
                    <h3>Before you contact us, please read our <a href="/privacy-policy/">Internet Privacy Statement</a>.</h3>
                    <div class="space40"></div>
                </div>
                <div class="col-xs-12 col-sm-10 col-md-8 contact-us-form-fields">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>Subject:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="form-group <c:if test="${not empty errors['subject']}"><c:out value="error"></c:out></c:if>" data-field-name="subject">
                                <div class="radio">
                                    <input checked="checked" id="id_subject_option1" type="radio" name="subject" value="enquiry" data-field-name="Enquiry" <c:if test="${not empty sanitized['subject'] && sanitized['subject'] == 'enquiry'}"><c:out value="checked"></c:out></c:if>>
                                    <label for="id_subject_option1"><span><span></span></span>&nbsp;General Enquiry</label>
                                </div>
                                <div class="radio">
                                    <input id="id_subject_option2" type="radio" name="subject" value="feedback" data-field-name="Feedback" <c:if test="${not empty sanitized['subject'] && sanitized['subject'] == 'feedback'}"><c:out value="checked"></c:out></c:if>>
                                    <label for="id_subject_option2"><span><span></span></span>&nbsp;Feedback</label>
                                </div>
                                <div class="radio">
                                    <input id="id_subject_option3" type="radio" name="subject" value="complaint" data-field-name="Complaint" <c:if test="${not empty sanitized['subject'] && sanitized['subject'] == 'complaint'}"><c:out value="checked"></c:out></c:if>>
                                    <label for="id_subject_option3"><span><span></span></span>&nbsp;Complaint</label>
                                </div>
                                <span class="error-message">
                                    <i class="icon icon-circle-delete "></i> <c:out value="${errors['subject']}"></c:out>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label for="form-comments">Comments/Questions:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="form-group <c:if test="${not empty errors['comments']}"><c:out value="error"></c:out></c:if>" data-field-name="comments">
                                <textarea class="form-control" name="comments" rows="6" id="form-comments" data-char-limit="175"><c:out value="${sanitized['comments']}"></c:out></textarea>
                                <span class="comments-char-count" role="alert" aria-live="assertive" aria-atomic="true"></span>
                                <span class="error-message">
                                    <i class="icon icon-circle-delete "></i> <c:out value="${errors['comments']}"></c:out>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="space20"></div>
                    <div class="pointed pointed--down">
                        <hr>
                    </div>
                    <div class="space40"></div>
                </div>
                <div class="col-xs-12 col-sm-10 col-md-8 contact-us-form-fields">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 contact-us-form-fields--title">
                            <label for="form-title">Title:</label>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group <c:if test="${not empty errors['title']}"><c:out value="error"></c:out></c:if>" data-field-name="title">
                                <div class="form-select_wrap"><select class="form-control control_country_select" id="form-title" name="title">
                                    <option value="">Please select...</option>
                                    <option <c:if test="${not empty sanitized['title'] && sanitized['title'] == 'Mr'}"><c:out value="selected"></c:out></c:if>>Mr</option>
                                    <option <c:if test="${not empty sanitized['title'] && sanitized['title'] == 'Mrs'}"><c:out value="selected"></c:out></c:if>>Mrs</option>
                                    <option <c:if test="${not empty sanitized['title'] && sanitized['title'] == 'Miss'}"><c:out value="selected"></c:out></c:if>>Miss</option>
                                    <option <c:if test="${not empty sanitized['title'] && sanitized['title'] == 'Ms'}"><c:out value="selected"></c:out></c:if>>Ms</option>
                                </select></div>
                                <span class="error-message">
                                    <i class="icon icon-circle-delete "></i> <c:out value="${errors['title']}"></c:out>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>Name:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="form-group <c:if test="${not empty errors['surname']}"><c:out value="error"></c:out></c:if>" data-field-name="surname">
                                <label for="form-surname">Surname</label>
                                <input type="text" class="form-control" id="form-surname" name="surname" value="${fn:escapeXml(sanitized['surname'])}">
                                <span class="error-message">
                                    <i class="icon icon-circle-delete "></i> <c:out value="${errors['surname']}"></c:out>
                                </span>
                            </div>
                            <div class="form-group <c:if test="${not empty errors['first_name']}"><c:out value="error"></c:out></c:if>" data-field-name="first_name">
                                <label for="form-firstname">First Name</label>
                                <input type="text" class="form-control" id="form-firstname" name="first_name" value="${fn:escapeXml(sanitized['first_name'])}">
                                <span class="error-message">
                                    <i class="icon icon-circle-delete "></i> <c:out value="${errors['first_name']}"></c:out>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row complaint-related" style="display: none;">
                        <div class="col-xs-12 col-sm-4">
                            <label for="form-nric">NRIC/Passport No.:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="form-group <c:if test="${not empty errors['nric']}"><c:out value="error"></c:out></c:if>" data-field-name="nric">
                                <input type="text" id="form-nric" class="form-control" name="nric" value="${fn:escapeXml(sanitized['nric'])}">
                                <span class="error-message">
                                    <i class="icon icon-circle-delete "></i> <c:out value="${errors['nric']}"></c:out>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row complaint-related" style="display: none;">
                        <div class="col-xs-12 col-sm-4">
                            <label for="form-country">Country:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="form-group <c:if test="${not empty errors['country']}"><c:out value="error"></c:out></c:if>" data-field-name="country">
                                <div class="form-select_wrap"><select class="form-control control_country_select" id="form-country" name="country">
                                    <option value="">Please select...</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Singapore'}"><c:out value="selected"></c:out></c:if>>Singapore</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Afghanistan'}"><c:out value="selected"></c:out></c:if>>Afghanistan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Albania'}"><c:out value="selected"></c:out></c:if>>Albania</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Algeria'}"><c:out value="selected"></c:out></c:if>>Algeria</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'American Samoa'}"><c:out value="selected"></c:out></c:if>>American Samoa</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Andorra'}"><c:out value="selected"></c:out></c:if>>Andorra</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Angola'}"><c:out value="selected"></c:out></c:if>>Angola</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Anguilla'}"><c:out value="selected"></c:out></c:if>>Anguilla</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Antarctica'}"><c:out value="selected"></c:out></c:if>>Antarctica</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Antigua and Barbuda'}"><c:out value="selected"></c:out></c:if>>Antigua and Barbuda</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Argentina'}"><c:out value="selected"></c:out></c:if>>Argentina</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Armenia'}"><c:out value="selected"></c:out></c:if>>Armenia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Aruba'}"><c:out value="selected"></c:out></c:if>>Aruba</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Australia'}"><c:out value="selected"></c:out></c:if>>Australia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Austria'}"><c:out value="selected"></c:out></c:if>>Austria</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Azerbaijan'}"><c:out value="selected"></c:out></c:if>>Azerbaijan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bahama'}"><c:out value="selected"></c:out></c:if>>Bahama</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bahrain'}"><c:out value="selected"></c:out></c:if>>Bahrain</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bangladesh'}"><c:out value="selected"></c:out></c:if>>Bangladesh</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Barbados'}"><c:out value="selected"></c:out></c:if>>Barbados</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Belarus'}"><c:out value="selected"></c:out></c:if>>Belarus</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Belgium'}"><c:out value="selected"></c:out></c:if>>Belgium</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Belize'}"><c:out value="selected"></c:out></c:if>>Belize</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Benin'}"><c:out value="selected"></c:out></c:if>>Benin</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bermuda'}"><c:out value="selected"></c:out></c:if>>Bermuda</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bhutan'}"><c:out value="selected"></c:out></c:if>>Bhutan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bolivia'}"><c:out value="selected"></c:out></c:if>>Bolivia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bosnia and Herzegovina'}"><c:out value="selected"></c:out></c:if>>Bosnia and Herzegovina</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Botswana'}"><c:out value="selected"></c:out></c:if>>Botswana</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bouvet Island'}"><c:out value="selected"></c:out></c:if>>Bouvet Island</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Brazil'}"><c:out value="selected"></c:out></c:if>>Brazil</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'British Indian Ocean Territory'}"><c:out value="selected"></c:out></c:if>>British Indian Ocean Territory</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'British Virgin Islands'}"><c:out value="selected"></c:out></c:if>>British Virgin Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Brunei Darussalam'}"><c:out value="selected"></c:out></c:if>>Brunei Darussalam</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Bulgaria'}"><c:out value="selected"></c:out></c:if>>Bulgaria</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Burkina Faso'}"><c:out value="selected"></c:out></c:if>>Burkina Faso</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Burundi'}"><c:out value="selected"></c:out></c:if>>Burundi</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Cambodia'}"><c:out value="selected"></c:out></c:if>>Cambodia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Cameroon'}"><c:out value="selected"></c:out></c:if>>Cameroon</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Canada'}"><c:out value="selected"></c:out></c:if>>Canada</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Cape Verde'}"><c:out value="selected"></c:out></c:if>>Cape Verde</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Cayman Islands'}"><c:out value="selected"></c:out></c:if>>Cayman Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Central African Republic'}"><c:out value="selected"></c:out></c:if>>Central African Republic</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Chad'}"><c:out value="selected"></c:out></c:if>>Chad</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Chile'}"><c:out value="selected"></c:out></c:if>>Chile</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'China'}"><c:out value="selected"></c:out></c:if>>China</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Christmas Island'}"><c:out value="selected"></c:out></c:if>>Christmas Island</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Cocos (Keeling) Islands'}"><c:out value="selected"></c:out></c:if>>Cocos (Keeling) Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Colombia'}"><c:out value="selected"></c:out></c:if>>Colombia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Comoros'}"><c:out value="selected"></c:out></c:if>>Comoros</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Congo'}"><c:out value="selected"></c:out></c:if>>Congo</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Cook Islands'}"><c:out value="selected"></c:out></c:if>>Cook Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Costa Rica'}"><c:out value="selected"></c:out></c:if>>Costa Rica</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Croatia'}"><c:out value="selected"></c:out></c:if>>Croatia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Cuba'}"><c:out value="selected"></c:out></c:if>>Cuba</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Cyprus'}"><c:out value="selected"></c:out></c:if>>Cyprus</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Czech Republic'}"><c:out value="selected"></c:out></c:if>>Czech Republic</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Denmark'}"><c:out value="selected"></c:out></c:if>>Denmark</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Djibouti'}"><c:out value="selected"></c:out></c:if>>Djibouti</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Dominica'}"><c:out value="selected"></c:out></c:if>>Dominica</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Dominican Republic'}"><c:out value="selected"></c:out></c:if>>Dominican Republic</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'East Timor'}"><c:out value="selected"></c:out></c:if>>East Timor</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Ecuador'}"><c:out value="selected"></c:out></c:if>>Ecuador</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Egypt'}"><c:out value="selected"></c:out></c:if>>Egypt</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'El Salvador'}"><c:out value="selected"></c:out></c:if>>El Salvador</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Equatorial Guinea'}"><c:out value="selected"></c:out></c:if>>Equatorial Guinea</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Eritrea'}"><c:out value="selected"></c:out></c:if>>Eritrea</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Estonia'}"><c:out value="selected"></c:out></c:if>>Estonia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Ethiopia'}"><c:out value="selected"></c:out></c:if>>Ethiopia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Falkland Islands (Malvinas)'}"><c:out value="selected"></c:out></c:if>>Falkland Islands (Malvinas)</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Faroe Islands'}"><c:out value="selected"></c:out></c:if>>Faroe Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Fiji'}"><c:out value="selected"></c:out></c:if>>Fiji</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Finland'}"><c:out value="selected"></c:out></c:if>>Finland</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'France'}"><c:out value="selected"></c:out></c:if>>France</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'France, Metropolitan'}"><c:out value="selected"></c:out></c:if>>France, Metropolitan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'French Guiana'}"><c:out value="selected"></c:out></c:if>>French Guiana</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'French Polynesia'}"><c:out value="selected"></c:out></c:if>>French Polynesia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'French Southern Territories'}"><c:out value="selected"></c:out></c:if>>French Southern Territories</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Gabon'}"><c:out value="selected"></c:out></c:if>>Gabon</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Gambia'}"><c:out value="selected"></c:out></c:if>>Gambia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Georgia'}"><c:out value="selected"></c:out></c:if>>Georgia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Germany'}"><c:out value="selected"></c:out></c:if>>Germany</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Ghana'}"><c:out value="selected"></c:out></c:if>>Ghana</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Gibraltar'}"><c:out value="selected"></c:out></c:if>>Gibraltar</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Greece'}"><c:out value="selected"></c:out></c:if>>Greece</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Greenland'}"><c:out value="selected"></c:out></c:if>>Greenland</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Grenada'}"><c:out value="selected"></c:out></c:if>>Grenada</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Guadeloupe'}"><c:out value="selected"></c:out></c:if>>Guadeloupe</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Guam'}"><c:out value="selected"></c:out></c:if>>Guam</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Guatemala'}"><c:out value="selected"></c:out></c:if>>Guatemala</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Guinea'}"><c:out value="selected"></c:out></c:if>>Guinea</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Guinea-Bissau'}"><c:out value="selected"></c:out></c:if>>Guinea-Bissau</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Guyana'}"><c:out value="selected"></c:out></c:if>>Guyana</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Haiti'}"><c:out value="selected"></c:out></c:if>>Haiti</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Heard Island and McDonald Islands'}"><c:out value="selected"></c:out></c:if>>Heard Island and McDonald Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Honduras'}"><c:out value="selected"></c:out></c:if>>Honduras</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Hong Kong'}"><c:out value="selected"></c:out></c:if>>Hong Kong</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Hungary'}"><c:out value="selected"></c:out></c:if>>Hungary</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Iceland'}"><c:out value="selected"></c:out></c:if>>Iceland</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'India'}"><c:out value="selected"></c:out></c:if>>India</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Indonesia'}"><c:out value="selected"></c:out></c:if>>Indonesia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Islamic Republic of Iran'}"><c:out value="selected"></c:out></c:if>>Islamic Republic of Iran</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Iraq'}"><c:out value="selected"></c:out></c:if>>Iraq</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Ireland'}"><c:out value="selected"></c:out></c:if>>Ireland</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Israel'}"><c:out value="selected"></c:out></c:if>>Israel</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Italy'}"><c:out value="selected"></c:out></c:if>>Italy</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Jamaica'}"><c:out value="selected"></c:out></c:if>>Jamaica</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Japan'}"><c:out value="selected"></c:out></c:if>>Japan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Jordan'}"><c:out value="selected"></c:out></c:if>>Jordan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Kazakhstan'}"><c:out value="selected"></c:out></c:if>>Kazakhstan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Kenya'}"><c:out value="selected"></c:out></c:if>>Kenya</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Kiribati'}"><c:out value="selected"></c:out></c:if>>Kiribati</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Korea, Democratic People\\\’s Republic of'}"><c:out value="selected"></c:out></c:if>>Korea, Democratic People's Republic of</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Korea, Republic of'}"><c:out value="selected"></c:out></c:if>>Korea, Republic of</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Kuwait'}"><c:out value="selected"></c:out></c:if>>Kuwait</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Kyrgyzstan'}"><c:out value="selected"></c:out></c:if>>Kyrgyzstan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Lao People\\\’s Democratic Republic'}"><c:out value="selected"></c:out></c:if>>Lao People's Democratic Republic</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Latvia'}"><c:out value="selected"></c:out></c:if>>Latvia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Lebanon'}"><c:out value="selected"></c:out></c:if>>Lebanon</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Lesotho'}"><c:out value="selected"></c:out></c:if>>Lesotho</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Liberia'}"><c:out value="selected"></c:out></c:if>>Liberia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Libyan Arab Jamahiriya'}"><c:out value="selected"></c:out></c:if>>Libyan Arab Jamahiriya</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Liechtenstein'}"><c:out value="selected"></c:out></c:if>>Liechtenstein</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Lithuania'}"><c:out value="selected"></c:out></c:if>>Lithuania</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Luxembourg'}"><c:out value="selected"></c:out></c:if>>Luxembourg</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Macau'}"><c:out value="selected"></c:out></c:if>>Macau</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Madagascar'}"><c:out value="selected"></c:out></c:if>>Madagascar</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Malawi'}"><c:out value="selected"></c:out></c:if>>Malawi</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Malaysia'}"><c:out value="selected"></c:out></c:if>>Malaysia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Maldives'}"><c:out value="selected"></c:out></c:if>>Maldives</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Mali'}"><c:out value="selected"></c:out></c:if>>Mali</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Malta'}"><c:out value="selected"></c:out></c:if>>Malta</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Marshall Islands'}"><c:out value="selected"></c:out></c:if>>Marshall Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Martinique'}"><c:out value="selected"></c:out></c:if>>Martinique</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Mauritania'}"><c:out value="selected"></c:out></c:if>>Mauritania</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Mauritius'}"><c:out value="selected"></c:out></c:if>>Mauritius</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Mayotte'}"><c:out value="selected"></c:out></c:if>>Mayotte</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Mexico'}"><c:out value="selected"></c:out></c:if>>Mexico</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Micronesia'}"><c:out value="selected"></c:out></c:if>>Micronesia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Moldova, Republic of'}"><c:out value="selected"></c:out></c:if>>Moldova, Republic of</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Monaco'}"><c:out value="selected"></c:out></c:if>>Monaco</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Mongolia'}"><c:out value="selected"></c:out></c:if>>Mongolia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Monserrat'}"><c:out value="selected"></c:out></c:if>>Monserrat</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Morocco'}"><c:out value="selected"></c:out></c:if>>Morocco</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Mozambique'}"><c:out value="selected"></c:out></c:if>>Mozambique</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Myanmar'}"><c:out value="selected"></c:out></c:if>>Myanmar</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Nambia'}"><c:out value="selected"></c:out></c:if>>Nambia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Nauru'}"><c:out value="selected"></c:out></c:if>>Nauru</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Nepal'}"><c:out value="selected"></c:out></c:if>>Nepal</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Netherlands'}"><c:out value="selected"></c:out></c:if>>Netherlands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Netherlands Antilles'}"><c:out value="selected"></c:out></c:if>>Netherlands Antilles</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'New Caledonia'}"><c:out value="selected"></c:out></c:if>>New Caledonia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'New Zealand'}"><c:out value="selected"></c:out></c:if>>New Zealand</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Nicaragua'}"><c:out value="selected"></c:out></c:if>>Nicaragua</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Niger'}"><c:out value="selected"></c:out></c:if>>Niger</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Nigeria'}"><c:out value="selected"></c:out></c:if>>Nigeria</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Niue'}"><c:out value="selected"></c:out></c:if>>Niue</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Norfolk Island'}"><c:out value="selected"></c:out></c:if>>Norfolk Island</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Northern Mariana Islands'}"><c:out value="selected"></c:out></c:if>>Northern Mariana Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Norway'}"><c:out value="selected"></c:out></c:if>>Norway</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Oman'}"><c:out value="selected"></c:out></c:if>>Oman</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Pakistan'}"><c:out value="selected"></c:out></c:if>>Pakistan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Palau'}"><c:out value="selected"></c:out></c:if>>Palau</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Panama'}"><c:out value="selected"></c:out></c:if>>Panama</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Papua New Guinea'}"><c:out value="selected"></c:out></c:if>>Papua New Guinea</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Paraguay'}"><c:out value="selected"></c:out></c:if>>Paraguay</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Peru'}"><c:out value="selected"></c:out></c:if>>Peru</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Philippines'}"><c:out value="selected"></c:out></c:if>>Philippines</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Pitcairn'}"><c:out value="selected"></c:out></c:if>>Pitcairn</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Poland'}"><c:out value="selected"></c:out></c:if>>Poland</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Portugal'}"><c:out value="selected"></c:out></c:if>>Portugal</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Puerto Rico'}"><c:out value="selected"></c:out></c:if>>Puerto Rico</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Qatar'}"><c:out value="selected"></c:out></c:if>>Qatar</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Romania'}"><c:out value="selected"></c:out></c:if>>Romania</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Russian Federation'}"><c:out value="selected"></c:out></c:if>>Russian Federation</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Rwanda'}"><c:out value="selected"></c:out></c:if>>Rwanda</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'S. Georgia & the S. Sandwich Islands'}"><c:out value="selected"></c:out></c:if>>S. Georgia & the S. Sandwich Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Saint Lucia'}"><c:out value="selected"></c:out></c:if>>Saint Lucia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Samoa'}"><c:out value="selected"></c:out></c:if>>Samoa</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'San Marino'}"><c:out value="selected"></c:out></c:if>>San Marino</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Sao Tome and Principe'}"><c:out value="selected"></c:out></c:if>>Sao Tome and Principe</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Saudi Arabia'}"><c:out value="selected"></c:out></c:if>>Saudi Arabia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Senegal'}"><c:out value="selected"></c:out></c:if>>Senegal</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Seychelles'}"><c:out value="selected"></c:out></c:if>>Seychelles</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Sierra Leone'}"><c:out value="selected"></c:out></c:if>>Sierra Leone</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Slovakia'}"><c:out value="selected"></c:out></c:if>>Slovakia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Slovenia'}"><c:out value="selected"></c:out></c:if>>Slovenia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Solomon Islands'}"><c:out value="selected"></c:out></c:if>>Solomon Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Somalia'}"><c:out value="selected"></c:out></c:if>>Somalia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'South Africa'}"><c:out value="selected"></c:out></c:if>>South Africa</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Spain'}"><c:out value="selected"></c:out></c:if>>Spain</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Sri Lanka'}"><c:out value="selected"></c:out></c:if>>Sri Lanka</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'St. Helena'}"><c:out value="selected"></c:out></c:if>>St. Helena</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'St. Kitts and Nevis'}"><c:out value="selected"></c:out></c:if>>St. Kitts and Nevis</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'St. Pierre and Miquelon'}"><c:out value="selected"></c:out></c:if>>St. Pierre and Miquelon</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'St. Vincent and the Grenadines'}"><c:out value="selected"></c:out></c:if>>St. Vincent and the Grenadines</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Sudan'}"><c:out value="selected"></c:out></c:if>>Sudan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Suriname'}"><c:out value="selected"></c:out></c:if>>Suriname</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Svalbard and Jan Mayen Islands'}"><c:out value="selected"></c:out></c:if>>Svalbard and Jan Mayen Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Swaziland'}"><c:out value="selected"></c:out></c:if>>Swaziland</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Sweden'}"><c:out value="selected"></c:out></c:if>>Sweden</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Switzerland'}"><c:out value="selected"></c:out></c:if>>Switzerland</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Syrian Arab Republic'}"><c:out value="selected"></c:out></c:if>>Syrian Arab Republic</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Taiwan, Province of China'}"><c:out value="selected"></c:out></c:if>>Taiwan, Province of China</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Tajikistan'}"><c:out value="selected"></c:out></c:if>>Tajikistan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Thailand'}"><c:out value="selected"></c:out></c:if>>Thailand</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Togo'}"><c:out value="selected"></c:out></c:if>>Togo</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Tokelau'}"><c:out value="selected"></c:out></c:if>>Tokelau</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Tonga'}"><c:out value="selected"></c:out></c:if>>Tonga</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Trinidad and Tobago'}"><c:out value="selected"></c:out></c:if>>Trinidad and Tobago</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Tunisia'}"><c:out value="selected"></c:out></c:if>>Tunisia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Turkey'}"><c:out value="selected"></c:out></c:if>>Turkey</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Turkmenistan'}"><c:out value="selected"></c:out></c:if>>Turkmenistan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Turks and Caicos Islands'}"><c:out value="selected"></c:out></c:if>>Turks and Caicos Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Tuvalu'}"><c:out value="selected"></c:out></c:if>>Tuvalu</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Uganda'}"><c:out value="selected"></c:out></c:if>>Uganda</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Ukraine'}"><c:out value="selected"></c:out></c:if>>Ukraine</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'United Arab Emirates'}"><c:out value="selected"></c:out></c:if>>United Arab Emirates</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'United Kingdom (Great Britain)'}"><c:out value="selected"></c:out></c:if>>United Kingdom (Great Britain)</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'United Republic of Tanzania'}"><c:out value="selected"></c:out></c:if>>United Republic of Tanzania</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'United States of America'}"><c:out value="selected"></c:out></c:if>>United States of America</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'United States Minor Outlying Islands'}"><c:out value="selected"></c:out></c:if>>United States Minor Outlying Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'United States Virgin Islands'}"><c:out value="selected"></c:out></c:if>>United States Virgin Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Uruguay'}"><c:out value="selected"></c:out></c:if>>Uruguay</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Uzbekistan'}"><c:out value="selected"></c:out></c:if>>Uzbekistan</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Vanuatu'}"><c:out value="selected"></c:out></c:if>>Vanuatu</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Vatican City State (Holy See)'}"><c:out value="selected"></c:out></c:if>>Vatican City State (Holy See)</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Venezuela'}"><c:out value="selected"></c:out></c:if>>Venezuela</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Viet Nam'}"><c:out value="selected"></c:out></c:if>>Viet Nam</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Wallis and Futuna Islands'}"><c:out value="selected"></c:out></c:if>>Wallis and Futuna Islands</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Western Sahara'}"><c:out value="selected"></c:out></c:if>>Western Sahara</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Yugoslavia'}"><c:out value="selected"></c:out></c:if>>Yugoslavia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Zaire'}"><c:out value="selected"></c:out></c:if>>Zaire</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Zambia'}"><c:out value="selected"></c:out></c:if>>Zambia</option>
                                    <option <c:if test="${not empty sanitized['country'] && sanitized['country'] == 'Zimbabwe'}"><c:out value="selected"></c:out></c:if>>Zimbabwe</option>
                                    <option value="none_of_above">None of the above</option>
                                </select></div>
                                <span class="error-message">
                                    <i class="icon icon-circle-delete "></i> <c:out value="${errors['country']}"></c:out>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row complaint-related" style="display: none;">
                        <div class="col-xs-12 col-sm-4">
                            <label>Contact:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="form-group <c:if test="${not empty errors['phone_country_code'] || not empty errors['phone_number']}"><c:out value="error"></c:out></c:if>" data-field-name="phone_country_code_number">
                                <div class="form-group__multi form-group__multi--phone">
                                    <span>
                                        <label for="form-mobilecountrycode" class="visuallyhidden">Number country code</label>
                                        <input type="text" id="form-mobilecountrycode"  class="form-control" placeholder="65" name="phone_country_code" value="${fn:escapeXml(sanitized['phone_country_code'])}">
                                    </span>
                                    <span>
                                        <label for="form-mobilenumber" class="visuallyhidden">Mobile number</label>
                                        <input type="text" id="form-mobilenumber" class="form-control" placeholder="" name="phone_number" value="${fn:escapeXml(sanitized['phone_number'])}">
                                    </span>
                                </div>
                                <c:if test="${not empty errors['phone_country_code']}">
                                    <span class="error-message">
                                        <i class="icon icon-circle-delete "></i> <c:out value="${errors['phone_country_code']}"></c:out>
                                    </span>
                                </c:if>
                                <c:if test="${not empty errors['phone_number']}">
                                    <span class="error-message">
                                        <i class="icon icon-circle-delete "></i> <c:out value="${errors['phone_number']}"></c:out>
                                    </span>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label for="form-email">Email address:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="form-group <c:if test="${not empty errors['email']}"><c:out value="error"></c:out></c:if>" data-field-name="email">
                                <input type="text" id="form-email" class="form-control" name="email" value="${fn:escapeXml(sanitized['email'])}">
                                <span class="error-message">
                                    <i class="icon icon-circle-delete "></i> <c:out value="${errors['email']}"></c:out>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4"></div>
                        <div class="col-xs-12 col-sm-8">
                            <a class="primary-btn-slate pull-right" role="button" data-action="submit" tabindex="0">Submit</a>
                        </div>
                    </div>
                </div>
                <div id="contact-us-complaint-notes" class="col-xs-12 complaint-related" style="display: none;">
                    <div>
                        <div class="space30"></div>
                        <hr>
                        <div class="space30"></div>
                        <h3>Complaint handling</h3>
                        <ul>
                            <li>If, for any reason, you are not entirely satisfied with any aspect of our service, we want to hear from you to help us make things right.</li>
                            <li>You have our assurance that we take your feedback seriously and we want to put matters right as soon as we can.</li>
                            <li>Where appropriate, we will take the necessary steps to prevent any recurrence of the concern you have raised.</li>
                            <li>Once your complaint is received, we will strive to acknowledge receipt within 1 business day, conduct a thorough review of the matter
                            and aim to respond promptly within our committed service level time frame of 5 business days.</li>
                            <li>Some matters may be more complex and may take a little longer to resolve, in which case we will keep you updated on our progress.</li>
                        </ul>
                        <h3>The Financial Industry Disputes Resolution Centre Ltd (FIDReC)</h3>
                        <p>HSBC has effective measures in place to monitor complaints, including regular audits and management review. If, despite our best efforts you are
                            not entirely satisfied with our suggested resolution, you have the right to refer the matter to FIDReC. Please visit <a href="http://www.fidrec.com.sg/website/index.html" target="_blank" style="overflow-wrap:break-word;">http://www.fidrec.com.sg/website/index.html</a></p>
                        <p>By providing the information set out above and submitting this form, I/we consent to the collection, use and disclosure of the personal data provided in this form by HSBC Insurance(Singapore) Pte. Limited, its agents and authorised service providers, as well as relevant third parties in order to fulfil my "contact me" request.</p>
                        <a class="primary-btn-slate pull-right" role="button" data-action="submit" tabindex="0">Submit</a>
                    </div>
                </div>
            </div>
        </section>
    </form>
</section>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>

<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC:ContactUs:Form",
        "page_url"      : window.location.href,
        "page_type"     : "ContactUsForm",
        "page_language" : "en"
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>