<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="com.mysql.jdbc.StringUtils" %>
<%@ page import="sg.com.hsbc.dreamcatcher.helpers.Database" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>

<%
    String faqGroup = request.getParameter("faqGroup");
    ArrayList<Map<String, String>> faqs = new ArrayList<>();
    if (!StringUtils.isNullOrEmpty(faqGroup)) {
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
        stmt = conn.prepareStatement("SELECT question, answer FROM faq f, faq_groups g WHERE g.group_handle = ? AND g.faq_id=f.id ORDER BY sort_order ASC;");
        stmt.setString(1, faqGroup);
            rs = stmt.executeQuery();
            while (rs.next()) {
                HashMap<String, String> item = new HashMap<>();
                item.put("question", rs.getString("question"));
                item.put("answer", rs.getString("answer"));
                faqs.add(item);
            }
            request.setAttribute("faqs", faqs);
        }catch (Exception ex) {

        } finally {
            db.closeAll(conn,stmt, rs);
        }
    }
%>

    <c:if test="${not empty faqs}">
        <div class="space60"></div>
        <section class="container footer-utility">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Online Insurance FAQ</h3>
                    <form id="faq-search">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="faq-search-keywords" name="term" placeholder="Ask a question" autocomplete="off" aria-describedby="faqerror">
                                <div class="input-group-addon" data-event="ask-a-question"><i class="icon icon-search" role="button" tabindex="0" aria-label="Search FAQ"></i></div>
                            </div>
                            <p class="error-message" role="alert" aria-live="assertive" aria-atomic="true" id="faqerror">
                                <i class="icon icon-circle-delete" aria-hidden="true"></i> Oops! Your search did not return any results. Please try again.
                            </p>
                        </div>
                    </form>
                    <div class="question-result"> <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" data-event="common-questions">
                        Common questions
                    </a>
                        <div class="collapse" id="collapseExample">
                            <div class="well">
                                <ul class="faq-search-results">
                                    <li>The FAQs provided are for general information only and do not constitute legal advice. Please read the product reference materials carefully to understand the products’ features and associated risks. </li>
                                    <c:forEach items="${faqs}" var="faq" varStatus="loop">
                                      <li class="collapsed" role="heading">
                                          <div id="accordHeader${loop.index + 1}" class="faq-question" aria-expanded="false" role="button" aria-controls="accordPanel${loop.index + 1}" data-event="faq-interaction">
                                              <i>${loop.index + 1}.</i>
                                              <span><c:out value="${faq['question']}"></c:out></span>
                                              <a href="#" class="btn"></a>
                                              <div class="clearfix"></div>
                                          </div>
                                          <span class="faq-answer" id="accordPanel${loop.index + 1}" role="region" aria-labelledby="accordHeader${loop.index + 1}">
                                              <c:out value="${faq['answer']}" escapeXml="false"/>
                                          </span>
                                      </li>

                                    </c:forEach>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <% if (!(request.getRequestURI().contains("/policy-forms/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/policy-forms/")))) { %>
                        <h3>Submit a claim</h3>
                        <p>We know it can be a difficult time when you have to make an insurance claim. So, we have made it convenient for you to submit your claim online as soon as possible.</p>
                        <p><a href="/eclaims/" class="secondary-btn-slate" data-event="make-a-claim">Submit</a></p>
                    <% } %>
                </div>
            </div>
        </section>
    </c:if>

    <c:if test="${empty param.hideImportantNotes}">
        <section class="important-notes">
            <!--mobile footer-->
            <!-- <div class="hidden-lg hidden-md hidden-sm">
                <h4 class="panel-title hsbc-title">
                    <a class="collapsed btn tertiary-btn-slate" role="button" data-toggle="collapse" data-parent="#importantNotes" href="#importantNotes" aria-expanded="false" aria-controls="collapse"> Important Notes
                    </a>
                </h4>
                <div class="collapse" id="importantNotes">
                    <div class="important-notes__body">
                        <div class="container container-border-btmnone">
                            <small id="collapse5" class="panel-collapse">
                                <% if ((request.getRequestURI().contains("/your-dreams/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your-dreams/")))) { %>
                                <small>The information and/or estimates generated by this calculator should not be construed as an offer to sell or a solicitation of an offer to purchase or subscribe for any insurance or investment nor shall it or any part of it form the basis of, or be relied on in connection with, any contract or commitment whatsoever.</small>
                                <% } else { %>
                                <small>All life insurance policies are underwritten by HSBC Insurance (Singapore) Pte. Limited (Reg. No. 195400150N), 21 Collyer Quay #02-01 Singapore 049320.</small>
                                <small>This is published for general information only and does not have regard to the specific investment objectives, financial situation and particular needs of any specific person. You may wish to seek advice from a financial consultant before making a commitment to purchase a life insurance policy. In the event that you choose not to seek advice from a financial consultant, you should consider whether the product in question is suitable for you. Buying a life insurance policy is a long-term commitment. An early termination of the policy incurs high costs and the surrender value payable may be less than the total premiums paid.</small>
                                <small>HSBC Insurance (Singapore) Pte. Limited is a member of the Policy Owners' Protection (PPF) Scheme that was established to protect policy owners in the event a life or general insurer which is a PPF Scheme member fails. The PPF Scheme provides 100% protection for the guaranteed benefits of your life insurance policies, subject to caps, where applicable. This will offer better protection to policy owners as defined in the Deposit Insurance and Policy Owners'Protection Scheme Act (Cap. 77B).</small>
                                <small>Information is correct as at 18 January 2018. This website is issued by HSBC Insurance (Singapore) Pte. Limited.</small>
                                <% } %>
                            </small>
                        </div>
                    </div>
                </div>
            </div> -->
            <!--desktop footer-->
            <div class="">
                <div class="important-notes__head">
                    <div class="container container-border-btmnone">
                        <h3 class="panel-title HSBC-title">
                            Important notes
                        </h3>
                    </div>
                </div>
                <div class="important-notes__body">
                    <div class="container container-border-btmnone">
                        <small id="collapse5" class="panel-collapse">
                            <c:if test="${param.campaign}">
                                <small>All promotions are offered by HSBC Insurance (Singapore) Pte. Limited (&ldquo;HSBC&rdquo;). HSBC may, at its discretion, revise any of these terms and conditions, including but not limited to varying the Promotional Period and/or any other date(s) specified above, or withdraw this promotion at any time without prior notice or assuming any liability to any customer. These terms and conditions are governed by the laws of the Republic of Singapore and the parties submit to the non-exclusive jurisdiction of the courts of Singapore.</small>
                            </c:if>

                            <% if ((request.getRequestURI().contains("/your-dreams/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your-dreams/")))) { %>
                            <small>The information and/or estimates generated by this calculator should not be construed as an offer to sell or a solicitation of an offer to purchase or subscribe for any insurance or investment nor shall it or any part of it form the basis of, or be relied on in connection with, any contract or commitment whatsoever.</small>
                            <%--Text updation as a part of HSBCIDCU-926 --%>
                            <% }else if ((request.getRequestURI().contains("/onlineprotector") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/onlineprotector")))) { %>
                            <small>HSBC Insurance OnlineProtector is underwritten by HSBC Insurance (Singapore) Pte. Limited (Reg. No. 195400150N), 21 Collyer Quay #02-01 Singapore 049320.</small>
                            <small>This webpage contains only general information and does not have regard to the specific investment objectives, financial situation and the particular needs of any specific person. It does not constitute an offer to buy or sell an insurance product or service. Copies of the product summaries in relation to the products offered are available and can be obtained on this website. A person interested in a product should read the relevant product summary for details before deciding whether to buy the product. Buying a life insurance policy is a long-term commitment. An early termination of the policy usually incurs a high cost and the surrender value payable may be less than the total premiums paid.</small>
                            <small>This policy is protected under the Policy Owners' Protection Scheme which is administered by the Singapore Deposit Insurance Corporation (SDIC). Coverage for your policy is automatic and no further action is required from you. For more information on the types of benefits that are covered under the scheme as well as the limits of coverage, where applicable, please contact us or visit the LIA or SDIC web-sites (www.lia.org.sg or www.sdic.org.sg).</small>
                            <small>Information is correct as at 5 February 2018. This website is issued by HSBC Insurance (Singapore) Pte. Limited.</small>
                            <%--Text updation as a part of HSBCIDCU-926 --%>
                            <% }else if ((request.getRequestURI().contains("/directvalueterm") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/directvalueterm")))) {%>
                            <small>DIRECT-ValueTerm is underwritten by HSBC Insurance (Singapore) Pte. Limited (Reg. No. 195400150N), 21 Collyer Quay #02-01 Singapore 049320.</small>
                            <small>This webpage contains only general information and does not have regard to the specific investment objectives, financial situation and the particular needs of any specific person. It does not constitute an offer to buy or sell an insurance product or service. Copies of the product summaries in relation to the products offered are available and can be obtained on this website. A person interested in a product should read the relevant product summary for details before deciding whether to buy the product. Buying a life insurance policy is a long-term commitment. An early termination of the policy usually incurs a high cost and the surrender value payable may be less than the total premiums paid.</small>
                            <small>This policy is protected under the Policy Owners' Protection Scheme which is administered by the Singapore Deposit Insurance Corporation (SDIC). Coverage for your policy is automatic and no further action is required from you. For more information on the types of benefits that are covered under the scheme as well as the limits of coverage, where applicable, please contact us or visit the LIA or SDIC web-sites (www.lia.org.sg or www.sdic.org.sg).</small>
                            <small>Information is correct as at 5 February 2018. This website is issued by HSBC Insurance (Singapore) Pte. Limited.</small>
                           	<% } else { %>
                            <small>All life insurance policies are underwritten by HSBC Insurance (Singapore) Pte. Limited (Reg. No. 195400150N), 21 Collyer Quay #02-01 Singapore 049320.</small>
                            <small>This webpage contains only general information and does not have regard to the specific investment objectives, financial situation and the particular needs of any specific person. It does not constitute an offer to buy or sell an insurance product or service. Copies of the product summaries in relation to the products offered are available and can be obtained on this website. A person interested in a product should read the relevant product summary for details before deciding whether to buy the product. Buying a life insurance policy is a long-term commitment. An early termination of the policy usually incurs a high cost and the surrender value payable may be less than the total premiums paid.</small>
                            <small>Information is correct as at 5 February 2018. This website is issued by HSBC Insurance (Singapore) Pte. Limited.</small>
                            <% } %>
                        </small>
                    </div>
                </div>
            </div>

        </section>
    </c:if>

    <section class="sub-footer">
        <div class="container container-border-btmnone">
            <div class="row">
                <div class="col-sm-4">
                    <p><a href="/policy-forms/" data-event="footer-naviation">Forms and documents<br><span class="small-link">Read the product guide or manage your policy<i class="icon icon-chevron-right-small"></i></span></a> </p>
                </div>
                <div class="col-sm-4">
                    <p><a href="/contact-us/" data-event="footer-naviation">Contact us<br><span class="small-link">Speak to us or give us your feedback<i class="icon icon-chevron-right-small"></i></span></a> </p>
                </div>
                <div class="col-sm-4">
                    <p><a href="/about-us/" data-event="footer-naviation">About HSBC Insurance Online<br><span class="small-link">Our commitment to you<i class="icon icon-chevron-right-small"></i></span></a> </p>
                </div>
            </div>
        </div>
    </section>
    <footer class="container container-border-btmnone">
        <div class="row">
            <div class="col-md-6">
                <ul>
                    <li><a href="/privacy-policy/" data-event="footer-naviation">Privacy Policy</a></li>
                    <li><a href="/terms-of-use/" data-event="footer-naviation">Terms of Use</a></li>
                    <li><a href="/hyperlink-policy/" data-event="footer-naviation">Hyperlink Policy</a></li>
                </ul>
            </div>
            <div class="col-md-6">&copy; Copyright. HSBC Insurance (Singapore) Pte. Limited 2018. All rights reserved.</div>
        </div>
    </footer>
    <script src="<%= request.getContextPath() %>/assets/js/jquery.min.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/jquery-ui.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/bootstrap.min.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/core.js"></script>

    <script src="<%= request.getContextPath() %>/assets/js/jquery-validate/jquery.validate.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/jquery-validate/additional-methods.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/jquery-steps/jquery.steps.min.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/rangeslider.min.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/jquery.selectBox.js"></script>

    <script src="<%= request.getContextPath() %>/assets/js/validation.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/calculators.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/eclaims.js"></script>

    <script src="<%= request.getContextPath() %>/assets/js/wNumb.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/nouislider.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/onlineprotector.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/directvalueterm.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/exit-prompt.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/tags.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/tracking.js"></script>

    <script>
    $(function () {
        $('[data-toggle="popover"]').popover({trigger: 'hover'})

        /* init selectbox */
        $('select').each(function(i, el){
            var $el = $(el);

            // init using this method to get the object reference
            // (necessary for using api methods)
            var $selectBox = new SelectBox($el, settings = {
                mobile: false,
                menuTransition: 'default',
                menuSpeed: 'normal',
                loopOptions: true,
                topPositionCorrelation: 0,
                bottomPositionCorrelation: 0,
                hideOnWindowScroll: true,
                keepInViewport: true
            });

            // HSBCIDCU-1063: init aria on selectbox on desktop only (selectbox does not init on mobile)
            if (!navigator.userAgent.match(/iPad|iPhone|Android|IEMobile|BlackBerry/i)) {
                var $control = $el.selectBox('control');
                var $options = $el.selectBox('options');

                // apply aria attributes to control
                // (generated <a> object displaying current selection)
                // use aria-live to let screen reader read field as it updates
                $control.attr({
                    'aria-haspopup': 'listbox',
                    'role': 'button',
                    'aria-live': 'assertive',
                });

                // apply aria attributes to options
                // (generated <ul> list of available options)
                $options.attr({
                    'id': 'selectBox-options-'+i,
                    'aria-activedescendant': $el.selectBox('value'),
                    'role': 'listbox',
                    'tabindex': 0
                });

                // apply aria attributes to option items
                // (generated <li> within options <ul>)
                // pass in ids to each option (needed for aria-live to work)
                // ids are based on select instance on page
                $options.find('li').each(function(i,el){
                    $(el).attr({
                        'id': $options[0].id + '-'+ i,
                        'role': 'option',
                        'tabindex': 0
                    });
                });

                // bind keydown event handlers to allow navigation through options
                $options.off('keydown.aria').on('keydown.aria', function(e){
                    e.stopPropagation();
                    e.preventDefault();

                    switch (e.keyCode) {
                        case 38: // up (select next option)
                            var $selected = $options.find('li.selectBox-selected');
                            var $prev = $selected.prev('li');
                            if ($prev.length) {
                                $selectBox.selectOption($prev, e);
                                $selectBox.keepOptionInView($next);
                            }
                            break;
                        case 40: // down (select prev option)
                            var $selected = $options.find('li.selectBox-selected');
                            var $next = $selected.next('li');
                            if ($next.length) {
                                $selectBox.selectOption($next, e);
                                $selectBox.keepOptionInView($next);
                            }
                            break;
                        case 37: // left
                            break;
                        case 39: // right
                            break;
                        case 13: // enter
                        case 27: // esc
                        case 32: // space
                        case 9: // tab
                            // switch focus back to select box control
                            $control.focus();
                            // hide selectbox menu
                            $selectBox.hideMenus();
                            break;
                        default:
                            break;
                    }
                });

                // update activedescendant on change
                // for aria-live to work
                $el.selectBox().on('change.aria', function(){
                    $options.attr({
                        'aria-activedescendant': $options.find('li a[rel="'+$el.selectBox('value')+'"]').parent()[0].id
                    });
                });
            }
        });
    });

      /* bubble slider start */

        var valueBubble = '<output class="rangeslider__value-bubble" />';

        function updateValueBubble(pos, value, context) {
            pos = pos || context.position;
            value = value || context.value;
            var $valueBubble = $('.rangeslider__value-bubble', context.$range);
            var tempPosition = pos + context.grabPos;
            var position = (tempPosition <= (context.handleDimension+30)) ? (context.handleDimension+30) : (tempPosition >= (context.maxHandlePos-30)) ? (context.maxHandlePos-30) : tempPosition;

            if ($valueBubble.length) {
                $valueBubble[0].style.left = Math.ceil(position) + 'px';
                $valueBubble[0].innerHTML = "S$" + value;
            }
        }

        $('input[type="bubble-range"]').rangeslider({
            polyfill: false,
            onInit: function() {
                this.$range.append($(valueBubble));
                updateValueBubble(null, null, this);

                //add css class
                $(".rangeslider--horizontal").addClass('has-bubble');
            },
            onSlide: function(pos, value) {
                updateValueBubble(pos, value, this);
            }
        });
      /* bubble slider end */
      /* legacy currency range slider start */
        var $rangeslider = $('input[type="programmatic-range"]');
        var $amount = $('input[type="programmatic-range-number"]');

        $rangeslider
            .rangeslider({
                polyfill: false,
                onInit: function(){

                },
                onSlide: function(pos, value){
                    $amount[0].value = prependCurrency(value);
                    if($amount.parent().hasClass('has-error')){
                        $amount.parent().removeClass('has-error');
                    }
                }
            });

        $amount.on('input', function() {

            var amt = stripPrefix(this.value);

            if(!isNaN(parseInt(amt))){
                if(amt >= 200 && amt <= 3600){
                    if($(this).parent().hasClass('has-error')){
                        $(this).parent().removeClass('has-error');
                    }
                    $rangeslider.val(amt).change();
                }else{
                    $(this).parent().addClass('has-error');
                }
                $(this).val(prependCurrency(amt));
            }

        });
      /* legacy currency range slider end */

        function prependCurrency(val){
            return 'S$' + val;
        }

        function stripPrefix(val){
            return parseInt(val.match(/\d+/)[0]);
        }

        /* default slider */
        $('input[type="range"]').rangeslider({
            polyfill: false
        });

      /*
       * Number padding function
       *
       * n: number to pad
       * width: total digits in a padded number
       * OPTIONAL - z: char to pad with (default: '0')
       */
      function pad(n, width, z) {
          z = z || '0';
          n = n + '';
          return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
      }

      function openPage(pageURL)
      {
          window.location.href = pageURL;
      }
    </script>
</body>

</html>
