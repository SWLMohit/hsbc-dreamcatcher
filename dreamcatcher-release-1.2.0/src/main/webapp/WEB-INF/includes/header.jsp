<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="sg.com.hsbc.dreamcatcher.helpers.Config" %>
<!DOCTYPE html>
<html lang="en">
<head>
 <link rel="canonical" href='<c:out value="${param.canonicalTag}"></c:out>' />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <c:if test="${not empty param.metadataKeywords}">
	<meta name="keywords" value="${fn:escapeXml(param.metadataKeywords)}">
    </c:if>
    <c:if test="${not empty param.metadataDescription}">
	<meta name="description" value="${fn:escapeXml(param.metadataDescription)}">
    </c:if>
    <title>
        <c:choose>
          <c:when test="${not empty param.pageTitle}">
            <c:choose>
              <c:when test="${param.pageTitle.equals('dreamcatcher-homepage')}">
                HSBC Insurance Singapore | Life Insurance Online
              </c:when>
              <c:otherwise>
                <c:out value="${param.pageTitle}"></c:out> | HSBC SG Insurance Online
              </c:otherwise>
            </c:choose>
           </c:when>
           <c:otherwise>
                HSBC Insurance Online
           </c:otherwise>
        </c:choose>
    </title>
    <link href="<%= request.getContextPath() %>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/jquery-ui.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/jquery.selectBox.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/rangeslider.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/main.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/media.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/fonts.min.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/HSBCIcon-Font.min.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/calculators.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/eclaims.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/nouislider.min.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/pages/landing.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/pages/onlineprotector.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/pages/onlineinvestor.css" rel="stylesheet">
    <link href="<%= request.getContextPath() %>/assets/css/pages/offers.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<%= request.getContextPath() %>/assets/js/html5shiv.min.js"></script>
    <script src="<%= request.getContextPath() %>/assets/js/respond.min.js"></script>
    <![endif]-->
    <c:if test="${Config.getString('livechat.site.env') == 'PROD'}">
        <script src="<%= request.getContextPath() %>/assets/js/le-mtagconfig_Prod.js"></script>
    </c:if>
    <c:if test="${Config.getString('livechat.site.env') != 'PROD'}">
        <script src="<%= request.getContextPath() %>/assets/js/le-mtagconfig.js"></script>
    </c:if>

    <script type="text/javascript">
        lpTag.section = ["dreamcatcher", "<c:out value="${param.lpSecondaryTag}"></c:out>"];
    </script>
    <script src="//tags.tiqcdn.com/utag/hsbc/sg-rbwm-insurance/prod/utag.sync.js"></script>
    <!-- START- favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-180x180.png" />
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-180x180.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<%= request.getContextPath() %>/assets/images/favicon/apple-touch-icon-57x57.png" />
    <link rel="shortcut icon" href="<%= request.getContextPath() %>/assets/images/favicon/favicon-16x16.ico?v=2">
    <link rel="icon" type="image/png" href="<%= request.getContextPath() %>/assets/images/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<%= request.getContextPath() %>/assets/images/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<%= request.getContextPath() %>/assets/images/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<%= request.getContextPath() %>/assets/images/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<%= request.getContextPath() %>/assets/images/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content=" " />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<%= request.getContextPath() %>/assets/images/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<%= request.getContextPath() %>/assets/images/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<%= request.getContextPath() %>/assets/images/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<%= request.getContextPath() %>/assets/images/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<%= request.getContextPath() %>/assets/images/favicon/mstile-310x310.png" />
    <!-- END- favicon-->

    <!-- START- sharing-->
    <c:choose>
        <c:when test="${not empty param.pageTitle}">
            <meta property="og:title" content="${fn:escapeXml(param.pageTitle)} | HSBC Insurance Online" />
        </c:when>
        <c:otherwise>
            <meta property="og:title" content="HSBC Insurance Online" />
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty param.pageDescription}">
            <meta property="og:description" content="${fn:escapeXml(param.pageDescription)}" />
        </c:when>
    </c:choose>
    <c:choose>
        <c:when test="${not empty param.pageImage}">
            <meta property="og:image" content="${fn:escapeXml(param.pageImage)}" />
        </c:when>
    </c:choose>
    <!-- END- sharing-->
</head>

<body class="<c:out value="${param.lpSecondaryTag}"></c:out>">
    <div class="nav-overlay" role="dialog" aria-modal="true">
        <div class="overlay-content">
            <div class="overlay-content-panel" tabindex="-1">
                <div class="container-fluid">
                    <div class="row overlay-content-panel-header">
                        <div class="col-xs-8">
                            <a href="/">Home</a>
                        </div>
                        <div class="col-xs-4">
                            <a href="#" aria-label="close"><span aria-hidden="true">&times;</span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="menu">
                                <li class="<%= (request.getRequestURI().contains("/our-plans/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/our-plans/"))) ? "active" : "" %>">
                                    <a href="/our-plans/">Our Plans</a>
                                    <a href="#" data-panel-id="plans" aria-label="Expand Our Plans submenu"><i class="icon icon-chevron-right"></i></a>
                                    <div class="clearfix"></div>
                                </li>
                                <li class="<%= (request.getRequestURI().contains("/your-dreams/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your-dreams/"))) ? "active" : "" %>">
                                    <a href="/your-dreams/">Your Dreams</a>
                                    <a href="#" data-panel-id="dreams" aria-label="Expand Your Dreams submenu"><i class="icon icon-chevron-right"></i></a>
                                    <div class="clearfix"></div>
                                </li>
                                <li><a href="/your-offers/" class="secondary">Your offers</a></li>
                                <li><a href="/policy-forms/" class="secondary">Forms and documents</a></li>
                                <li><a href="/contact-us/" class="secondary">Contact us</a></li>
                                <li><a href="/about-us/" class="secondary">About HSBC Insurance Online</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="overlay-content-panel" data-panel-id="plans" tabindex="-1">
                <div class="container-fluid">
                    <div class="row overlay-content-panel-header">
                        <div class="col-xs-8">
                            <a href="#" aria-label="Back to main menu"><i class="icon icon-chevron-left"></i> Plans</a>
                        </div>
                        <div class="col-xs-4">
                            <a href="#" aria-label="close"><span aria-hidden="true">&times;</span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="menu">
                                <li tabindex="0"><span>Life Insurance</span></li>
                                <li><a href="/our-plans/">HSBC Insurance OnlineProtector</a></li>
                                <li><a href="/our-plans/">Direct &mdash; ValueTerm</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="overlay-content-panel" data-panel-id="dreams" tabindex="-1">
                <div class="container-fluid">
                    <div class="row overlay-content-panel-header">
                        <div class="col-xs-8">
                            <a href="#" aria-label="Back to main menu"><i class="icon icon-chevron-left"></i> Dreams</a>
                        </div>
                        <div class="col-xs-4">
                            <a href="#" aria-label="close"><span aria-hidden="true">&times;</span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="menu">
                                <li tabindex="0"><span>Calculators</span></li>
                                <li class="<%= (request.getRequestURI().contains("/your-dreams/retirement-calculator/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your-dreams/retirement-calculator/"))) ? "active" : "" %>">
                                    <a href="/your-dreams/retirement-calculator/">Retirement</a>
                                </li>
                                <li class="<%= (request.getRequestURI().contains("/your-dreams/education-calculator/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your-dreams/education-calculator/"))) ? "active" : "" %>">
                                    <a href="/your-dreams/education-calculator/">Education</a>
                                </li>
                                <li class="<%= (request.getRequestURI().contains("/your-dreams/savings-calculator/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your-dreams/savings-calculator/"))) ? "active" : "" %>">
                                    <a href="/your-dreams/savings-calculator/">Savings</a>
                                </li>
                                <li class="<%= (request.getRequestURI().contains("/your-dreams/protection-calculator/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your-dreams/protection-calculator/"))) ? "active" : "" %>">
                                    <a href="/your-dreams/protection-calculator/">Protection</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<header class="header-block">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed"> <span class="sr-only">Toggle navigation</span> <i class="icon icon-menu"></i> </button>
                <div class="brand">
                    <a class="navbar-brand brand-main" href="/"><img src="<%= request.getContextPath() %>/assets/images/hsbc-insurance-logo-new-x2.png" alt="HSBC Insurance"></a>
                </div>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse collapse-main">
                <ul class="nav navbar-nav nav-main">
                    <li class="<%= (request.getRequestURI().contains("/our-plans/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/our/plans/"))) ? "active" : "" %>"><a href="/our-plans/">Our plans<br><span class="small small-height">Buy now</span><span class="sr-only">(current)</span></a></li>
                    <li class="<%= (request.getRequestURI().contains("/your-dreams/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your/dreams/"))) ? "active" : "" %>"><a href="/your-dreams/">Your dreams<br><span class="small small-height">Understand your needs</span></a></li>
                    <li class="<%= (request.getRequestURI().contains("/your-offers/") || (request.getAttribute("javax.servlet.forward.request_uri") != null && request.getAttribute("javax.servlet.forward.request_uri").toString().contains("/your/offers/"))) ? "active" : "" %>"><a href="/your-offers/">Your offers<br><span class="small small-height">Exciting promotions</span></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><div id="lpButtonDiv"></div></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    
</header>
