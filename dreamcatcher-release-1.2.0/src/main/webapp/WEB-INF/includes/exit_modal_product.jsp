<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>


<div class="modal fade <c:out value="${param.modal}"/>" id="buynow-exit-modal" tabindex="-1" role="dialog"  aria-modal="true">
    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" aria-label="close" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Do you really want to exit?</h4>
            </div>
            <div class="modal-body">
                <p>Your application progress will be saved when you leave the page. You will receive an email with instructions on how to retrieve your form and an SMS containing the 8-digit verification code.</p>
            </div>
            <div class="modal-footer">
                <a id="exit-sure"  class="btn">Leave page</a>
                <button data-dismiss="modal" class="btn">Resume application</button>
            </div>
        </div>
    </div>
</div>
