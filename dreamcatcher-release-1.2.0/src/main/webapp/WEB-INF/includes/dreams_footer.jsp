<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="sg.com.hsbc.dreamcatcher.helpers.Database" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.HashMap" %>

<style>
.header-page, .dream-search, .dream-search-inner { background: url(/assets/images/homepage/bg-dream-search.jpg) no-repeat left center/cover;  }
[data-dreams-image-id="savings-6"] {  background-image: url(/assets/images/dreams/1-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-7"] {  background-image: url(/assets/images/dreams/1-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-8"] {  background-image: url(/assets/images/dreams/1-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-9"] {  background-image: url(/assets/images/dreams/1-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-10"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-11"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-12"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-13"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-14"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-15"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-16"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-17"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-18"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-19"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-20"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-21"] {  background-image: url(/assets/images/dreams/2-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-22"] {  background-image: url(/assets/images/dreams/3-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-23"] {  background-image: url(/assets/images/dreams/3-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-24"] {  background-image: url(/assets/images/dreams/3-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-25"] {  background-image: url(/assets/images/dreams/3-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-26"] {  background-image: url(/assets/images/dreams/3-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-27"] {  background-image: url(/assets/images/dreams/4-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-28"] {  background-image: url(/assets/images/dreams/4-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-29"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-30"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-31"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-32"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-33"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-34"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-35"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-36"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-37"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-38"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-39"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-40"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-41"] {  background-image: url(/assets/images/dreams/5-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-42"] {  background-image: url(/assets/images/dreams/6-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-43"] {  background-image: url(/assets/images/dreams/6-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-44"] {  background-image: url(/assets/images/dreams/6-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-45"] {  background-image: url(/assets/images/dreams/6-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-46"] {  background-image: url(/assets/images/dreams/6-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-47"] {  background-image: url(/assets/images/dreams/6-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-48"] {  background-image: url(/assets/images/dreams/6-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-49"] {  background-image: url(/assets/images/dreams/6-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-50"] {  background-image: url(/assets/images/dreams/7-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-51"] {  background-image: url(/assets/images/dreams/7-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-52"] {  background-image: url(/assets/images/dreams/7-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-53"] {  background-image: url(/assets/images/dreams/7-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="savings-54"] {  background-image: url(/assets/images/dreams/8-savings-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-55"] {  background-image: url(/assets/images/dreams/1-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-56"] {  background-image: url(/assets/images/dreams/1-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-57"] {  background-image: url(/assets/images/dreams/1-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-58"] {  background-image: url(/assets/images/dreams/1-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-59"] {  background-image: url(/assets/images/dreams/1-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-60"] {  background-image: url(/assets/images/dreams/1-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-61"] {  background-image: url(/assets/images/dreams/1-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-63"] {  background-image: url(/assets/images/dreams/2-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-64"] {  background-image: url(/assets/images/dreams/2-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-65"] {  background-image: url(/assets/images/dreams/2-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-66"] {  background-image: url(/assets/images/dreams/2-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-67"] {  background-image: url(/assets/images/dreams/2-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-68"] {  background-image: url(/assets/images/dreams/2-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-69"] {  background-image: url(/assets/images/dreams/2-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-70"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-71"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-72"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-73"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-74"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-75"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-76"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-77"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-78"] {  background-image: url(/assets/images/dreams/3-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-79"] {  background-image: url(/assets/images/dreams/4-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-80"] {  background-image: url(/assets/images/dreams/4-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-81"] {  background-image: url(/assets/images/dreams/4-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="retirement-82"] {  background-image: url(/assets/images/dreams/4-retirement-mobile2x.jpg) !important; }
[data-dreams-image-id="education-83"] {  background-image: url(/assets/images/dreams/1-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-84"] {  background-image: url(/assets/images/dreams/1-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-85"] {  background-image: url(/assets/images/dreams/1-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-86"] {  background-image: url(/assets/images/dreams/1-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-87"] {  background-image: url(/assets/images/dreams/1-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-88"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-89"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-90"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-91"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-92"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-93"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-94"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-95"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="education-96"] {  background-image: url(/assets/images/dreams/2-education-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-97"] {  background-image: url(/assets/images/dreams/1-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-98"] {  background-image: url(/assets/images/dreams/1-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-99"] {  background-image: url(/assets/images/dreams/1-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-100"] {  background-image: url(/assets/images/dreams/1-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-101"] {  background-image: url(/assets/images/dreams/1-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-102"] {  background-image: url(/assets/images/dreams/2-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-103"] {  background-image: url(/assets/images/dreams/2-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-104"] {  background-image: url(/assets/images/dreams/2-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-105"] {  background-image: url(/assets/images/dreams/2-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-106"] {  background-image: url(/assets/images/dreams/2-protection-mobile2x.jpg) !important; }
[data-dreams-image-id="protection-107"] {  background-image: url(/assets/images/dreams/2-protection-mobile2x.jpg) !important; }

@media screen and (min-width: 992px) {
    [data-dreams-image-id="savings-6"] {  background-image: url(/assets/images/dreams/1-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-7"] {  background-image: url(/assets/images/dreams/1-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-8"] {  background-image: url(/assets/images/dreams/1-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-9"] {  background-image: url(/assets/images/dreams/1-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-10"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-11"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-12"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-13"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-14"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-15"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-16"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-17"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-18"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-19"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-20"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-21"] {  background-image: url(/assets/images/dreams/2-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-22"] {  background-image: url(/assets/images/dreams/3-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-23"] {  background-image: url(/assets/images/dreams/3-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-24"] {  background-image: url(/assets/images/dreams/3-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-25"] {  background-image: url(/assets/images/dreams/3-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-26"] {  background-image: url(/assets/images/dreams/3-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-27"] {  background-image: url(/assets/images/dreams/4-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-28"] {  background-image: url(/assets/images/dreams/4-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-29"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-30"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-31"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-32"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-33"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-34"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-35"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-36"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-37"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-38"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-39"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-40"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-41"] {  background-image: url(/assets/images/dreams/5-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-42"] {  background-image: url(/assets/images/dreams/6-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-43"] {  background-image: url(/assets/images/dreams/6-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-44"] {  background-image: url(/assets/images/dreams/6-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-45"] {  background-image: url(/assets/images/dreams/6-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-46"] {  background-image: url(/assets/images/dreams/6-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-47"] {  background-image: url(/assets/images/dreams/6-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-48"] {  background-image: url(/assets/images/dreams/6-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-49"] {  background-image: url(/assets/images/dreams/6-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-50"] {  background-image: url(/assets/images/dreams/7-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-51"] {  background-image: url(/assets/images/dreams/7-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-52"] {  background-image: url(/assets/images/dreams/7-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-53"] {  background-image: url(/assets/images/dreams/7-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="savings-54"] {  background-image: url(/assets/images/dreams/8-savings-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-55"] {  background-image: url(/assets/images/dreams/1-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-56"] {  background-image: url(/assets/images/dreams/1-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-57"] {  background-image: url(/assets/images/dreams/1-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-58"] {  background-image: url(/assets/images/dreams/1-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-59"] {  background-image: url(/assets/images/dreams/1-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-60"] {  background-image: url(/assets/images/dreams/1-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-61"] {  background-image: url(/assets/images/dreams/1-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-63"] {  background-image: url(/assets/images/dreams/2-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-64"] {  background-image: url(/assets/images/dreams/2-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-65"] {  background-image: url(/assets/images/dreams/2-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-66"] {  background-image: url(/assets/images/dreams/2-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-67"] {  background-image: url(/assets/images/dreams/2-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-68"] {  background-image: url(/assets/images/dreams/2-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-69"] {  background-image: url(/assets/images/dreams/2-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-70"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-71"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-72"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-73"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-74"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-75"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-76"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-77"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-78"] {  background-image: url(/assets/images/dreams/3-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-79"] {  background-image: url(/assets/images/dreams/4-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-80"] {  background-image: url(/assets/images/dreams/4-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-81"] {  background-image: url(/assets/images/dreams/4-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="retirement-82"] {  background-image: url(/assets/images/dreams/4-retirement-1440x450.jpg) !important; }
    [data-dreams-image-id="education-83"] {  background-image: url(/assets/images/dreams/1-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-84"] {  background-image: url(/assets/images/dreams/1-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-85"] {  background-image: url(/assets/images/dreams/1-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-86"] {  background-image: url(/assets/images/dreams/1-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-87"] {  background-image: url(/assets/images/dreams/1-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-88"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-89"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-90"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-91"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-92"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-93"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-94"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-95"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="education-96"] {  background-image: url(/assets/images/dreams/2-education-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-97"] {  background-image: url(/assets/images/dreams/1-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-98"] {  background-image: url(/assets/images/dreams/1-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-99"] {  background-image: url(/assets/images/dreams/1-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-100"] {  background-image: url(/assets/images/dreams/1-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-101"] {  background-image: url(/assets/images/dreams/1-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-102"] {  background-image: url(/assets/images/dreams/2-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-103"] {  background-image: url(/assets/images/dreams/2-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-104"] {  background-image: url(/assets/images/dreams/2-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-105"] {  background-image: url(/assets/images/dreams/2-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-106"] {  background-image: url(/assets/images/dreams/2-protection-1440x450.jpg) !important; }
    [data-dreams-image-id="protection-107"] {  background-image: url(/assets/images/dreams/2-protection-1440x450.jpg) !important; }

}
</style>
<div data-dreams-image-id="savings-6" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-7" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-8" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-9" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-10" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-11" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-12" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-13" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-14" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-15" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-16" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-17" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-18" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-19" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-20" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-21" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-22" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-23" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-24" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-25" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-26" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-27" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-28" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-29" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-30" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-31" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-32" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-33" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-34" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-35" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-36" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-37" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-38" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-39" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-40" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-41" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-42" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-43" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-44" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-45" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-46" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-47" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-48" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-49" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-50" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-51" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-52" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-53" style="width:0;height:0;"></div>
<div data-dreams-image-id="savings-54" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-55" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-56" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-57" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-58" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-59" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-60" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-61" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-63" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-64" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-65" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-66" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-67" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-68" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-69" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-70" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-71" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-72" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-73" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-74" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-75" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-76" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-77" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-78" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-79" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-80" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-81" style="width:0;height:0;"></div>
<div data-dreams-image-id="retirement-82" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-83" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-84" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-85" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-86" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-87" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-88" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-89" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-90" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-91" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-92" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-93" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-94" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-95" style="width:0;height:0;"></div>
<div data-dreams-image-id="education-96" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-97" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-98" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-99" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-100" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-101" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-102" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-103" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-104" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-105" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-106" style="width:0;height:0;"></div>
<div data-dreams-image-id="protection-107" style="width:0;height:0;"></div>

<%

%>
<% /*
<c:if test="${not empty dreams}">
<style>
.header-page, .dream-search, .dream-search-inner { background: url(/assets/images/homepage/bg-dream-search.jpg) no-repeat left center/cover;  }
<c:forEach items="${dreams}" var="dream">[data-dreams-image-id="${dream['category']}-${dream['id']}"] {  background-image: url(/assets/images/dreams/${dream['image_mobile']}) !important; }
</c:forEach>
@media screen and (min-width: 992px) {
  <c:forEach items="${dreams}" var="dream">[data-dreams-image-id="${dream['category']}-${dream['id']}"] {  background-image: url(/assets/images/dreams/${dream['image']}) !important; }
  </c:forEach>
}
</style>
<c:forEach items="${dreams}" var="dream"><div data-dreams-image-id="${dream['category']}-${dream['id']}" style="width:0;height:0;"></div>
</c:forEach>
</c:if>
*/ %>
