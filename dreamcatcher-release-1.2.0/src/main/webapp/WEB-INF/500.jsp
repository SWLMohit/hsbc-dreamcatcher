<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="500 Error"></jsp:param>
</jsp:include>

<section class="container" style="margin-top: 20px; margin-bottom: 60px;">
    <h1 style="border-top: 1px solid #666; padding-top: 40px;">An error has occured</h1>
    <div class="row">
        <h2 class="col-md-8" style="font-size: 20px; margin-bottom: 30px; margin-top: 0;">Sorry! We are currently fixing an unexpected problem. If you need immediate assistance, reach us via the following contact details.</h2>
    </div>

    <p style="font-size: 14px;">
        <b>Hotline:</b><br />
        (65) 6225 6111 <br />
        Monday to Friday, 9am to 5pm
    </p>

    <p style="font-size: 14px;">
        <b>Email address:</b><br />
        e-surance@hsbc.com.sg
    </p>
</section>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>
