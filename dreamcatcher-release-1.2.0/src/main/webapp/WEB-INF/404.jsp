<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="404 Error"></jsp:param>
</jsp:include>

<section class="container" style="margin-top: 20px; margin-bottom: 60px;">
    <h1 style="border-top: 1px solid #666; padding-top: 40px;">Page not found</h1>
    <div class="row">
        <h2 class="col-md-8" style="font-size: 20px; margin-bottom: 10px; margin-top: 0;">Oops! The page you are looking for cannot be found. Alternatively, you can visit our homepage or explore our plans using the links below.</h2>
    </div>
    <a href="/" class="primary-btn-slate">Go to homepage</a>

    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-4">
            <h4><a href="/our-plans/online-protector/">HSBC Insurance OnlineProtector<i class="icon icon-chevron-right"></i></a></h4>
        </div>
        <div class="col-sm-4">
            <h4><a href="/our-plans/direct-value-term/">DIRECT – ValueTerm<i class="icon icon-chevron-right"></i></a></h4>
        </div>
    <!--     <div class="col-sm-4">
            <h4><a href="/faq/">FAQ<i class="icon icon-chevron-right"></i></a></h4>
        </div> -->
    </div>
</section>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>

<style>
    
    @media (max-width: 767px) {

        .primary-btn-slate {
            width: 100%;
            text-align: center;
        }
    }

    @media (min-width: 767px) {
        .row {
            margin-top: 20px;
        }
    }
</style>