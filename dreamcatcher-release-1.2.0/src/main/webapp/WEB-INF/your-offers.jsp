<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Your Offers | HSBC SG Insurance Online"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="Your Offers | HSBC SG Insurance Online"></jsp:param>
    <jsp:param name="metadataKeywords" value="Insurance Rewards, Insurance Offers, Online Insurance Offers"></jsp:param>
    <jsp:param name="metadataDescription" value="Get rewarded while you're getting protected with HSBC's Online Insurance Offers. Click here to access your perks."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/your-offers/"></jsp:param>
</jsp:include>

<section class="container-fluid">
    <div class="header-page header-campaigns">
        <div class="container">
            <div class="featured">
                <h1>Your offers</h1>
                <p>Getting protected and rewarded concurrently can be hard. Fortunately, enjoying such perks with us is not.<br />Now's Good.</p>
            </div>
        </div>
    </div>
</section>


<section class="container campaign">

    <div class="row row-eq-height">

      <div class="col-sm-6">

        <h2>HSBC Insurance OnlineProtector launch promotion</h2>

        <p>Receive up to S$450 cash credits when you purchase HSBC Insurance <a href="/our-plans/online-protector/">OnlineProtector</a>, from now till 20 June 2018.</p>
        <p>Plus, receive additional S$20 cash credits when you charge your first premium payment to your HSBC Credit Card or HSBC Debit Card. Simply apply the promo code <b>DC11</b> in the &lsquo;Get a Quote&rsquo; section.</p>

        <a href="/our-plans/online-protector/#quick_quote" class="primary-btn-slate find-out-more-btn" data-event="quote-retireve" data-name="Online Protector">Get a quote</a>

        <table class="table">
          <thead>
              <tr>
                  <th>Tiers</th>
                  <th>Annualised First Year Premiums</th>
                  <th>Cash credits</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                <td>Tier 1</td>
                <td>S$600 - S$1800</td>
                <td>S$75</td>
              </tr>
              <tr>
                <td>Tier 2</td>
                <td>S$1801 - S$3600</td>
                <td>S$225</td>
              </tr>
              <tr>
                <td>Tier 3</td>
                <td>S$3601 and above</td>
                <td>S$450</td>
              </tr>
          </tbody>
        </table>

        <div class="tc-footnotes">
          <ul>
            <li>This promotion is valid for purchase of basic plan and/or inclusion of HSBC Insurance Online Critical Illness Rider.</li>
            <li>To be awarded your corresponding cash credits, please apply promo code DC11 in the Get a Quote section.</li>
            <li>Cash credits will be paid out in the form of a cheque and sent to your mailing address as indicated in your application form upon completion of 60 days of policy issuance.</li>
            <li>This promotion cannot be combined with any other promotions, except for the HSBC Credit Card or HSBC Debit Card cash credit promotion and Price Guarantee promotion.</li>
            <li>Additional S$20 cash credit is only eligible for HSBC Insurance OnlineProtector policies issued before 31 December 2018, and purchased using any HSBC Credit Card or HSBC Debit Card issued in Singapore and in the applicant's own name.</li>
            <li>This offer is valid across all payment modes, including monthly, quarterly, half-yearly or annual payment modes.</li>
          </ul>
        </div>

      </div>


       <div class="col-sm-6">

        <h2>Our price guarantee</h2>

        <p>With HSBC Insurance Online, you no longer have to worry about getting the best-priced term life insurance. As part of our promise in ensuring more value for you, we will offer you a 30% perpetual premium discount throughout your 10-year term if you find a cheaper plan than your HSBC Insurance <a href="/our-plans/online-protector/">OnlineProtector</a> policy.</p>
        <p>Simply email us at <a href="mailto:insuranceonline@hsbc.com.sg">insuranceonline@hsbc.com.sg</a> with a copy of the benefit illustration, product summary and premium difference. Alternatively, you can visit our customer service centre, Monday to Friday, 9.30am to 5pm at 21 Collyer Quay #02-01, HSBC Building, Singapore 049320.</p>

        <a href="/our-plans/online-protector/#quick_quote" class="outline-btn-slate find-out-more-btn" data-event="quote-retireve" data-name="Online Protector">Get a quote</a>

        <div class="tc-footnotes">
          <ul>
            <li>Promotion is only valid for applications submitted before 20 June 2018 and request must be received within 30 days of your application submission.</li>
            <li>To be eligible for the 30% perpetual premium discount, the comparison term plan must charge a lower premium and be identical to your HSBC Insurance OnlineProtector plan in all of the following aspects:
              <ul>
                <li>policy features (i.e. 10-year renewability, unemployment benefit and level premium across the 10-year term)</li>
                <li>coverage, and similar riders or add-ons as opted with your HSBC Insurance OnlineProtector, if any</li>
                <li>provided the underwriting questions asked are also identical underwriting questions</li>
                <li>similar premium payment mode - Yearly/Half-Yearly/Quarterly/Monthly</li>
                <li>premium comparison is considered without any extra discounts by HSBC or the other company in comparison</li>
              </ul>
            </li>
            <li>This discount is not valid for policy renewals beyond the initial 10-year term.</li>
          </ul>
        </div>

      </div>

    </div>

    <div class="row row-eq-height">

      <div class="col-sm-6">
        <h2>Refer today and be rewarded</h2>

        <p>Refer your loved ones, and both you and your family members or friends can receive up to S$310 cash credits each when they purchase HSBC Insurance <a href="/our-plans/online-protector/">OnlineProtector</a>, from now till 31 December 2018.</p>
        <p>Simply get them to apply the promo code <b>DMGMXXXXXXXX</b> (e.g. DMGM12345678) in the Get a Quote section, XXXXXXXX being your 8-digit policy number that can be retrieved from the cover letter of your welcome pack that will be mailed to you upon your application submission.</p>

        <a href="/our-plans/online-protector/#quick_quote" class="outline-btn-slate find-out-more-btn" data-event="quote-retireve" data-name="Online Protector">Get a quote</a>

        <table class="table">
          <thead>
              <tr>
                  <th>Tiers</th>
                  <th>Annualised First Year Premiums</th>
                  <th>Cash credit payable to Referrer and Referee</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                <td>Tier 1</td>
                <td>S$600 - S$1800</td>
                <td>S$60</td>
              </tr>
              <tr>
                <td>Tier 2</td>
                <td>S$1801 - S$3600</td>
                <td>S$160</td>
              </tr>
              <tr>
                <td>Tier 3</td>
                <td>S$3601 and above</td>
                <td>S$310</td>
              </tr>
          </tbody>
        </table>

        <div class="tc-footnotes">
          <ul>
            <li>This promotion is only valid for HSBC Insurance OnlineProtector policies issued before 31 December 2018, and for inclusion of HSBC Insurance Online Critical Illness Rider.</li>
            <li>For your referee to receive his/her referral reward, he/she will have to apply promo code DMGMXXXXXXXX (e.g. DMGM12345678) in the Get a Quote section, XXXXXXXX being your 8-digit policy number that can be retrieved from the cover letter of your welcome pack that will be mailed to you within 7 to 10 business days of your application submission.</li>
            <li>Cash credits will be paid out in the form of a cheque and sent to your mailing address as indicated in your application form upon completion of 60 days of policy issuance, and is subject to the referee’s policy being in force.</li>
            <li>This promotion cannot be combined with any other promotions, except for the HSBC Credit Card cash credit promotion.</li>
          </ul>
        </div>

      </div>

      <div class="col-sm-6">
        <img src="/assets/images/campaign/campaign-secondary.jpg" alt="" />
      </div>
    </div>

  </section>


<div class="space60"></div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
  <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>

<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name": "DC:YourOffers:ExcitingPromotions:Detail",
        "page_url": window.location.href,
        "page_type": "Offer Page",
        "page_language": "en",
		"page_security_level": "0",
        "page_business_line": "Insurance",
        "page_customer_group": "General"
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>