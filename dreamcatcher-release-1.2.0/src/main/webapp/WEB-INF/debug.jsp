<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Contact Us"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="contactushomepage"></jsp:param>
</jsp:include>

<div class="container">
    <h1>Debug</h1>
    <div class="row">
        <div class="col-xs-12">
            <div>
                <c:forEach items="${buildDetails}" var="detail">
                    <c:out value="${detail}"/><br>
                </c:forEach>
            </div>
            <br>
            <form method="post" action="/debug">
                <label>
                    Email:
                    <input name="email" type="text">
                </label>
                <label>
                    Mobile:
                    <input name="mobile" type="text">
                </label>
                <input type="submit">
            </form>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>