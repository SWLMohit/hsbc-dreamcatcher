<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Privacy Policy"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="privacypolicy"></jsp:param>
    <jsp:param name="metadataKeywords" value="data security, collection of personal information, insurance privacy policy"></jsp:param>
	<jsp:param name="metadataDescription" value="Click here to find out about HSBC Insurance's privacy principles. Details include collection of personal information, data security and more."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/privacy-policy/"></jsp:param>
</jsp:include>

<section class="container">
    <h1>HSBC Insurance's Privacy Policy</h1>
    <p>HSBC Insurance's Privacy Principles</p>

    <p>Our business has been built on trust between our customers and ourselves. To preserve the confidentiality of all information you provide to us, we maintain the following privacy principles:</p>

    <ul>
        <li>We only collect personal information that we believe to be relevant and required to understand your financial needs and to conduct our business.</li>
        <li>We use your personal information to provide you with better customer services and products.</li>
        <li>We may pass your personal information to other HSBC Group companies or agents, as permitted by law.</li>
        <li>We will not disclose your personal information to any external organisation unless we have your consent or are required by law or have previously informed you.</li>
        <li>We may be required from time to time to disclose your personal information to Governmental or judicial bodies or agencies or our regulators, but we will only do so under proper authority.</li>
        <li>We aim to keep your personal information on our records accurate and up-to-date.</li>
        <li>We maintain strict security systems designed to prevent unauthorised access to your personal information by anyone, including our staff.</li>
        <li>All HSBC Group companies, all our staff and all third parties with permitted access to your information are specifically required to observe our confidentiality obligations.</li>
    </ul>

    <p>By maintaining our commitment to these principles, we at HSBC will ensure that we respect the inherent trust that you place in us.</p>

    <p><strong>Your Privacy Matters to Us</strong><br>
    This section provides specific details of how we treat any personal information you might wish to provide us when you visit this site.</p>

    <p><strong>Use of Information and Materials</strong><br>
    Products and services referred to in this web site are offered only in jurisdictions where and when they may be lawfully offered by members of the HSBC Group. The materials on these pages are not intended for use by persons located in or resident in jurisdictions that restrict the distribution of such materials by us. Persons accessing these pages are required to inform themselves about and observe any relevant restrictions.</p>

    <p>These pages should not be regarded as an offer or solicitation to sell investments or make deposits in any jurisdiction to any person to whom it is unlawful to make such an invitation or solicitation in such jurisdiction.</p>

    <p>The information contained in these pages is not intended to provide professional advice and should not be relied upon in that regard. Persons accessing these pages are advised to obtain appropriate professional advice when necessary.</p>

    <p><strong>Copyright, Trade Marks</strong><br>
    HSBC Insurance and other parties own the trade marks, logos and service marks displayed on this site and users are prohibited from using the same without written permission of HSBC Insurance or such other parties.</p>

    <p>The materials on this site are protected by copyright and no part of such materials may be modified, reproduced, stored in a retrieval system, transmitted (in any form or by any means), copied, distributed, used for creating derivative works or used in any other way for commercial or public purposes without HSBC Insurance's prior written consent.</p>

    <p><strong>No Warranties</strong><br>
    While every care has been taken in preparing the information and materials contained in this site, such information and materials are provided to you "as is" without warranty of any kind either express or implied. In particular, no warranty regarding non-infringement, security, accuracy, fitness for a particular purpose or freedom from computer virus is given in conjunction with such information and materials.</p>

    <p><strong>Linked Web Sites</strong><br>
    HSBC Insurance or any other HSBC Group member is not responsible for the contents available on or the set-up of any other web sites linked to this site. Access to and use of such other web sites is at the user's own risk and subject to any terms and conditions applicable to such access/use.</p>

    <p><strong>E-mail</strong><br>
    E-mail messages sent to HSBC Insurance over the Internet cannot be guaranteed to be completely secure. HSBC Insurance will not be responsible for any damages incurred by users if they send a message to HSBC Insurance , or if HSBC Insurance sends a message to them at their request, over the Internet. HSBC Insurance is not responsible in any manner for direct, indirect, special or consequential damages arising out of the use of this web site.</p>

    <p><strong>Transmission over the Internet</strong><br>
    Due to the nature of the Internet, transactions may be subject to interruption, transmission blackout, delayed transmission and incorrect data transmission. HSBC Insurance will not be liable for malfunctions in communications facilities not under our control that may affect the accuracy or timeliness of messages and transactions you send.</p>

    <p><strong>Data Security</strong></p>
    <ul>
        <li>Security is our top priority. HSBC Insurance (Singapore) Pte. Limited ('HSBC Insurance') will strive at all times to ensure that your personal data will be protected against unauthorised or accidental access, processing or erasure. We maintain this commitment to data security by implementing appropriate physical, electronic and managerial measures to safeguard and secure your personal data.</li>
        <li>The secure area of our website supports the use of Secure SocketLayer(SSL)protocol and 128-encryption technology - an industry standard for encryption over the Internet to protect data. When you provide sensitive information such as credit card details, it will be automatically converted into codes before being securely dispatched over the Internet.</li>
        <li>Our web servers are protected behind "firewalls" and our systems are monitored to prevent any unauthorised access. We will not send personal information to you by ordinary email. As the security of ordinary email cannot be guaranteed, you should only send email to us using the secure email facility on our website.</li>
        <li>All practical steps will be taken to ensure that personal data will not be kept longer than necessary and that HSBC Insurance will comply with all statutory and regulatory requirements in Singapore concerning the retention of personally identifiable information.</li>
    </ul>

    <h2>Security Assurance</h2>
    <ul>
        <li>Both you and HSBC Insurance play an important role in protecting against online fraud. You should be careful that your HSBC Insurance policy details including your User ID and/or Password are not compromised by ensuring that you do not knowingly or accidentally share, provide or facilitate unauthorised use of it. Do not share your User ID and/or password or allow access or use of it by others. We endeavor to put in place high standards of security to protect your interests. If, in the unlikely event, unauthorised transactions have been conducted through your account through no fraud, fault or negligence on your part, we will see that you are covered for your direct loss up to the full amount of the unauthorised transaction.</li>
        <li>You should safeguard your unique User ID and Password by keeping it secret and confidential. Never write them down or share these details with anyone. HSBC Insurance will never ask you for your Password, in order to ensure that you are the only person who knows this information. When choosing your unique User ID and Password for the first time, do not create it using easily identifiable information such as your birthday, telephone number or a recognisable part of your name. If you think your User ID and/or password has been disclosed to a third party, is lost or stolen and unauthorised transactions may have been conducted, you are responsible to inform us immediately.</li>
    </ul>

    <h2>Collection of Personal Information</h2>
    <ul>
        <li>Use of "cookies" - Your visit to this site may be recorded for analysis on the number of visitors to the site and general usage patterns. Some of this information will be gathered through the use of "cookies". Cookies are small bits of information that are automatically stored on a person's web browser in their computer that can be retrieved by this site. Should you wish to disable these cookies you may do so by changing the setting on your browser. However, you will be unable to enter certain part(s) of our website, including online@hsbc.</li>
        <li>Marketing Promotions - Occasionally we may collect personal information from visitors to this site and those individuals that participate in a contest or promotion (online or over the telephone,or at one of our branches). Such information is only collected from individuals who voluntarily provide us with their personal information. We may use this information to advise them of products, services and other marketing materials, which we think, may be of interest to them. We may also invite visitors to this site to participate in market research and surveys and other similar activities.</li>
    </ul>
</section>

<div class="space60"></div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>

<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC:PrivacyPolicy",
        "page_url"      : window.location.href,
        "page_type"     : "Information Pages",
        "page_language" : "en",
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>