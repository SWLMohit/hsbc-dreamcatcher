<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Hyperlink Policy"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="hyperlinkpolicy"></jsp:param>
    <jsp:param name="metadataKeywords" value="third party sites, hsbc third party sites, hsbc hyperlink policy"></jsp:param>
	<jsp:param name="metadataDescription" value="Click here to find out more about the hyperlink policy of the website of HSBC Insurance. Details include our terms for third party sites and more."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/hyperlink-policy/"></jsp:param>
</jsp:include>

<section class="container">
    <h1>HSBC Insurance's Hyperlink Policy</h1>
    <p><strong>Hyperlinks from HSBC Insurance to Websites outside the HSBC Group</strong><br>
    There may be cases on our website when the HSBC Group ("HSBC") provides hyperlinks to other locations or websites on the Internet. These hyperlinks lead to websites published or operated by third parties who are not affiliated with or in any way related to HSBC Insurance. They have been included in our website to enhance your user experience and are presented for information purposes only. We endeavor to select reputable websites and sources of information for your convenience.</p>
    <p>However, by providing hyperlinks to an external website or webpage, HSBC Insurance shall not be deemed to endorse, recommend, approve, guarantee or introduce any third parties or the services/ products they provide on their websites, or to have any form of cooperation with such third parties and websites unless otherwise stated by HSBC Insurance.</p>
    <p>We are not in any way responsible for the content of any externally linked website or webpage. You use or follow these links at your own risk and HSBC Insurance is not responsible for any damages or losses incurred or suffered by you arising out of or in connection with your use of the link. HSBC Insurance is not a party to any contractual arrangements entered into between you and the provider of the external website unless otherwise expressly specified or agreed to by HSBC Insurance.</p>
    <p>Any links to websites that contain downloadable software are provided for your convenience only. Again we are not responsible for any difficulties you may encounter in downloading the software or for any consequences from your doing so. Please remember that the use of any software downloaded from the Internet may be governed by a licence agreement and your failure to observe the terms of such licence agreement may result in an infringement of intellectual property rights of the relevant software provider which we are not in any way responsible.</p>
    <p>Please be mindful that when you click on a link and leave our website you will be subject to the terms of use and privacy policies of the other website that you are going to visit.</p>
    <p><strong>Hyperlinks from HSBC Insurance to Other HSBC Group Websites</strong><br>
    We may also include hyperlinks to other HSBC Group websites for your convenience. The products and services offered on these websites may be limited to persons located or residing in only that particular jurisdiction. In addition, the content on these linked websites may not be intended for persons located or residing in jurisdictions that restrict the distribution of such content. The terms and conditions governing the use of the website of each HSBC Group entity may differ and you should consult and carefully read the applicable terms and conditions before using the website.</p>
    <p><strong>Hyperlinks from External Websites to our HSBC Insurance Website</strong><br>
    You must always obtain the prior written approval of HSBC Insurance before creating a hyperlink in any form from a third party website to any HSBC Insurance website. HSBC Insurance may or may not give such approval at its absolute discretion. In normal circumstances, we may only approve a hyperlink which displays plainly our name or website address. Any use or display of our logos, trade names and trademarks as a hyperlink will not be approved unless in very exceptional circumstances and may be subject to a fee as HSBC Insurance may determine at its absolute discretion.</p>
    <p>HSBC Insurance is not responsible for the setup of any hyperlink from a third party website to any HSBC Insurance website. Any links so set up shall not constitute any form of co-operation with, or endorsement by, HSBC Insurance of such third party website. Any link to our website shall always be an active and direct link to our website and shall be made directly to the home or front page of our website only and that no "framing" or "deep-linking" of our web page or content is allowed.</p>
    <p>The HSBC Group is not liable for any loss or damage incurred or suffered by you or any third party arising out of or in connection with such link. For further trademarks and copyright information, please refer to the paragraph below. HSBC Insurance reserve the right to rescind any approval granted to link through a plain-text link or any other type of link, and to require the removal of any such link to any of the HSBC Group websites, at our discretion at any time.</p>
    <p><strong>Our Trademarks and Copyright</strong><br>
    The content and information contained within our website or delivered to you in connection with your use of our website is the property of HSBC Insurance and any other third party (where applicable). The trademark, trade names and logos (the "Trade Marks") that are used and displayed on our website include registered and unregistered Trade Marks of ourselves and other third parties. Nothing on our website should be construed as granting any licence or right to use any Trade Marks displayed on our website. We retain all proprietary rights on our website. Users are prohibited from using the same without written permission of HSBC Insurance of such or such other parties.</p>
    <p>The materials on this website are protected by copyright and no part of such materials may be modified, reproduced, stored in a retrieval system, transmitted (in any form or by any means), copied, distributed, used for creating derivative works or used in any other way for commercial or public purposes without HSBC Insurance's prior written consent.</p>
    <p><strong>No Warranties</strong><br>
    While every care has been taken in preparing the information and materials contained in this site, such information and materials are provided to you "AS IS" without warranty of any kind either express or implied. In particular, no warranty regarding non-infringement, security, accuracy, fitness for a particular purpose or freedom from computer virus is given in conjunction with such information and materials.</p>
</section>

<div class="space60"></div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="hideImportantNotes" value="true" />
</jsp:include>

<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC:HyperlinkPolicy",
        "page_url"      : window.location.href,
        "page_type"     : "Information Pages",
        "page_language" : "en",
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>