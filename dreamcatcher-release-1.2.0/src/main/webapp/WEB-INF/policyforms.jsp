<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:include page="/WEB-INF/includes/header.jsp" flush="true">
    <jsp:param name="pageTitle" value="Forms and Documents"></jsp:param>
    <jsp:param name="lpSecondaryTag" value="ploicyforms"></jsp:param>
    <jsp:param name="metadataKeywords" value="hsbc insurance forms, insurance documents, online insurance claim forms"></jsp:param>
	<jsp:param name="metadataDescription" value="Find all the HSBC Insurance forms and documents you'll need here. For example, insurance claim forms, policy management forms and product references etc."></jsp:param>
    <jsp:param name="canonicalTag" value="https://insuranceonline.hsbc.com.sg/policy-forms/"></jsp:param>
</jsp:include>

<!-- Header Image-->
<section class="container-fluid">
    <div class="header-page header-policyforms">
        <div class="container">
            <div class="featured form-documents">
                <h1>Forms and documents</h1>
                <p>Find all the essential policy details and forms in one place.</p>
            </div>
        </div>
    </div>
</section>

<section class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-5">
            <h2>Claim form</h2>
            <p>Whichever type of claim you want to make,<br> submit it online to save yourself time and hassle.</p>
            <p><a href="/eclaims/" class="primary-btn-slate" data-event="make-a-claim">Submit</a></p>
        </div>
    </div>
    <div class="space30"></div>
</section>

<div class="container">
    <hr>
</div>
<div class="space30"></div>

<section class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2>Policy management forms</h2>
            <div class="space20"></div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <ul class="downloads-list">
                <li>
                    <a href="/assets/forms/personal-particulars-update-form-v170321.pdf" target="_blank" aria-label="Download Personal Particulars Update Form in PDF format"  data-event="file-download" data-name="personal-particulars-update-form-v170321.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Personal Particulars Update Form (PDF,&nbsp;139KB)</span>
                    </a>
                </li>
                <li>
                    <a href="/assets/forms/form-for-revocable-nomination-v170321.pdf" target="_blank" aria-label="Download Nomination of Beneficiaries in PDF format" data-event="file-download" data-name="form-for-revocable-nomination-v170321.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Nomination of Beneficiaries (PDF,&nbsp;108KB)</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-5">
            <ul class="downloads-list">

                <li>
                    <a href="/assets/forms/giro-form-v170321.pdf" target="_blank" aria-label="Download Inter-Bank GIRO form in PDF format" data-event="file-download" data-name="giro-form-v170321.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Inter-Bank GIRO form (PDF,&nbsp;159KB)</span>
                    </a>
                </li>
                <li>
                    <a href="/assets/forms/surrender-form-v170321.pdf" target="_blank" aria-label="Download Surrender/Termination Form (Non-Investment Linked Policy) in PDF format" data-event="file-download" data-name="surrender-form-v170321.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Surrender/Termination Form (Non-Investment Linked Policy) (PDF,&nbsp;340KB)</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="space60"></div>
</section>

<div class="container">
    <hr>
</div>
<div class="space30"></div>

<section class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2>Product reference materials</h2>
            <div class="space20"></div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <ul class="downloads-list">
                <li>
                    <a href="/assets/downloads/Your_Guide_to_Life_Insurance.pdf" target="_blank" aria-label="Download Your Guide to Life Insurance in PDF format" data-event="file-download" data-name="Your_Guide_to_Life_Insurance.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Your Guide to Life Insurance (PDF,&nbsp;161KB)</span>
                    </a>
                </li>
                <li>
                    <a href="/assets/downloads/Your_Guide_to_the_Nomination_of_Insurance_Nominees_2015.pdf" target="_blank" aria-label="Download Your Guide to the Nomination of Insurance Nominees - 2015 in PDF format" data-event="file-download" data-name="Your_Guide_to_the_Nomination_of_Insurance_Nominees_2015.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Your Guide to the Nomination of Insurance Nominees - 2015 (PDF,&nbsp;7.4MB)</span>
                    </a>
                </li>
                <li>
                    <a href="/assets/downloads/List_of_policies_under_Policy_Owners_Protection_Scheme_PPF_Scheme.pdf" target="_blank" aria-label="Download List of policies under Policy Owners' Protection Scheme (PPF Scheme) in PDF format" data-event="file-download" data-name="List_of_policies_under_Policy_Owners_Protection_Scheme_PPF_Scheme.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>List of policies under Policy Owners' Protection Scheme (PPF Scheme) (PDF,&nbsp;280KB)</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-5">
            <ul class="downloads-list">
                <li>
                    <a href="/assets/downloads/Your_Guide_to_Health_Insurance.pdf" target="_blank" aria-label="Download Your Guide to Health Insurance in PDF format" data-event="file-download" data-name="Your_Guide_to_Health_Insurance.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Your Guide to Health Insurance (PDF,&nbsp;272KB)</span>
                    </a>
                </li>
                <li>
                    <a href="/assets/downloads/Policy_Owners_Protection_Scheme_Life_Insurance_Consumer_Guide.pdf" target="_blank" aria-label="Download Policy Owners' Protection Scheme (Life Insurance) - Consumer Guide in PDF format" data-event="file-download" data-name="Policy_Owners_Protection_Scheme_Life_Insurance_Consumer_Guide.pdf">
                        <i class="icon icon-download" aria-hidden="true"></i>
                        <span>Policy Owners' Protection Scheme (Life Insurance) - Consumer Guide (PDF,&nbsp;974KB)</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="space60"></div>
</section>


<div class="container">
    <hr>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp">
    <jsp:param name="faqGroup" value="forms_docs" />
</jsp:include>


<!-- Tracking tag -->
<script type="text/javascript">
    var utag_data = {
        "page_name"     : "DC:FormsDocuments",
        "page_url"      : window.location.href,
        "page_type"     : "Information Pages",
        "page_language" : "en",
    };
</script>
<!-- /Tracking tag -->
<jsp:include page="/WEB-INF/includes/utag_tracker.jsp"></jsp:include>