<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our plans</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/products/mast.jpg)">
            <div class="masthead__container container">
                <div class="masthead__copy">
                    <h1>HSBC Insurance OnlineInvestor</h1>
                    <p class="emphasize">Predicting market movements can be hard.<br>Fortunately, investing the smart way is not. Now&rsquo;s Good.</p>
                </div>
            </div>
        </section>

        <div class="container">
            <section class="row article__leader">
                <div class="col-md-7 article__leader__main">
                    <h3>Make the ideal life investment</h3>
                    <p>Gain confidence in your future plans by building your wealth with an investment plan that offers access to a wide selection of well-researched funds and guaranteed lifetime protection.</p>
                    <div class="article__leader__main__btnset">
                        <a class="btn btn-default" href="#get-a-quote">See estimated returns</a>
                        <a class="btn btn-white" href="#">Retrieve your form</a>
                    </div>
                </div>
                <aside class="col-md-4 col-md-offset-1 article__leader__aside">
                    <dl>
                        <dt>Protection Cover</dt>
                        <dd>Guaranteed payout of minimum 105% of total regular premiums paid</dd>
                        <dt>Low Charges</dt>
                        <dd>
                            <ul>
                                <li>Product administration fees of just 0.6% per annum on your policy net asset value</li>
                                <li>Fund management charge from 0.4% per annum depending on the fund(s) you choose</li>
                                <li>Early withdrawal fees for only the first four years</li>
                            </ul>
                        </dd>
                        <dt>Fund Switching</dt>
                        <dd>Free and unlimited</dd>
                        <dt>Diversified Fund Range</dt>
                        <dd>Asset classes include Equity, Index, Bond and Balanced Funds</dd>
                    </dl>
                    <small></small>
                </aside>
            </section>

            <hr class="section_divider" />

            <section class="thumbnail_select">
                <h2>How HSBC Insurance OnlineInvestor benefits you</h2>
                <div class="row thumbnail_select__listing">
                    <span class="col-sm-4 thumbnail_select__item">
                        <img src="<%= request.getContextPath() %>/assets/images/products/lifeinsurance1.jpg" alt>
                        <h3>Transparent and simplified product structure</h3>
                        <p>Pay for just two fees unlike similar<br>products in the market. Also, there<br>are no additional charges for<br>insurance coverage.</p>
                    </span>
                    <span class="col-sm-4 thumbnail_select__item">
                        <img src="<%= request.getContextPath() %>/assets/images/products/lifeinsurance2.jpg" alt>
                        <h3>A diverse selection of carefully researched funds</h3>
                        <p>Choose from a range of Equity, Index, Bond and Balanced Funds with diverse risk profiles. What's more, enjoy free and unlimited fund switches.</p>
                    </span>
                    <span class="col-sm-4 thumbnail_select__item">
                        <img src="<%= request.getContextPath() %>/assets/images/products/lifeinsurance3.jpg" alt>
                        <h3>Minimum coverage of 105% of total premiums paid</h3>
                        <p>Receive guaranteed payout of either 105% of your regular premiums paid or the investment amount in your account, whicever is higher. </p>
                    </span>
                </div>
            </section>

            <hr class="section_divider" />

            <section class="products_collapsibles">
                <h2>
                    Funds to choose from
                    <button type="button" class="help-tooltip" tabindex="0" data-container="body" 
                    data-toggle="popover" data-placement="right" label="help" 
                    aria-label="The tables below only provide brief summaries of the funds. For full descriptions of the funds and the associated benefits and risks, please read the Fund Factsheet, Product highlights Sheet and Fund Summary carefully before choosing your fund(s)."
                    data-content="The tables below only provide brief summaries of the funds. For full descriptions of the funds and the associated benefits and risks, please read the Fund Factsheet, Product highlights Sheet and Fund Summary carefully before choosing your fund(s)."><i class="icon icon-circle-help-solid"></i></button>:
                </h2>

                <div class="panel-group products_listing" id="products_listing" role="tablist" aria-multiselectable="true">
                    <div class="panel HSBC-panel">
                      <div class="panel-heading" role="tab" id="product_title1">
                        <h3 class="panel-title">
                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#products_listing" aria-expanded="false"
                           href="#products_listing01"
                           aria-controls="products_listing01">
                            Equity Funds
                          </a>
                        </h3>
                      </div>
                      <div class="panel-collapse collapse" role="tabpanel"
                        id="products_listing01"
                        aria-labelledby="product_title1">

                            <ul class="products_listing__sets">
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance Pacific Equity Fund</h4>
                                        <p>This fund aims to provide medium to long-term capital growth from a diversified portfolio of
                                            Asian-Pacific equities excluding Japanese equities.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>4</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>Asia (Ex. Japan)</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>1.50%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to risks which are involved in investing in
                                            specific regional markets.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance Europe Dynamic Equity Fund</h4>
                                        <p>This fund aims to maximise long-term capital growth by investing primarily
                                        in an aggressively managed portfolio of Europe companies.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>4</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>Europe</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>1.50%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to higher risk equity strategies which are subject
                                            to market risks and higher price volatility.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance Singapore Equity Fund</h4>
                                        <p>This fund aims to provide long-term capital growth primarily through investments
                                        in securities of companies listed on Singapore Exchange Securities Trading Limited.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>4</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>Singapore</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>1.125%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to risks of investing in equities and equity derivatives
                                        which are subject to market risks and higher price volatility than experienced by fixed income securities.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                            </ul>
                        </div><!-- /.products_listing__category -->
                    </div>
                    <div class="panel HSBC-panel">
                      <div class="panel-heading" role="tab" id="product_title2">
                        <h3 class="panel-title">
                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#products_listing" aria-expanded="false"
                           href="#products_listing02"
                           aria-controls="products_listing02">
                            Equity (Index Funds)
                          </a>
                        </h3>
                      </div>
                      <div class="panel-collapse collapse" role="tabpanel"
                        id="products_listing02"
                        aria-labelledby="product_title2">
                            <ul class="products_listing__sets">
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance Global Equity Index Fund</h4>
                                        <p>This fund seeks to track the HSBC Economic Scale Index World by investing in securities in the Global Index.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>4</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>Global</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>0.60%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to market volatility and the risks related to investments in global equities and index tracking.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance US Equity Index Fund</h4>
                                        <p>This fund seeks to track the HSBC Economic Scale Index World by investing in securities in the US Index.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>4</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>US</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>0.40%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to market volatility and the risks related to US equities and index tracking.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                            </ul>
                        </div><!-- /.products_listing__category -->
                    </div>
                    <div class="panel HSBC-panel">
                      <div class="panel-heading" role="tab" id="product_title3">
                        <h3 class="panel-title">
                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#products_listing" aria-expanded="false"
                           href="#products_listing03"
                           aria-controls="products_listing03">
                            Bond Funds
                          </a>
                        </h3>
                      </div>
                      <div class="panel-collapse collapse" role="tabpanel"
                        id="products_listing03"
                        aria-labelledby="product_title3">
                            <ul class="products_listing__sets">
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance Global Emerging Markets Bond Fund</h4>
                                        <p>This fund seeks total return through a diversified portfolio of Investment and
                                            Non-investment Grade fixed income and other similar securities issued by companies
                                            which have their registered offices in Emerging Markets.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>2</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>Emerging Markets</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>1.25%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to risks related to investing in Investment
                                            Grade and Non-investment Grade fixed income securities and risks of investing in Emerging Markets.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance Global High Income Bond Fund</h4>
                                        <p>This fund invests for high income primarily in a diversified portfolio of higher yielding
                                        fixed income bonds and other similar securities from around the world, denominated in a range of
                                        currencies, including Investment Grade bonds, high yield bonds and Asian and Emerging Markets debt instruments.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>2</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>Global</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>1.25%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to risks related to investing in high yield fixed income
                                            securities from around the world.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                            </ul>
                        </div><!-- /.products_listing__category -->
                    </div>
                    <div class="panel HSBC-panel">
                      <div class="panel-heading" role="tab" id="product_title4">
                        <h3 class="panel-title">
                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#products_listing" aria-expanded="false"
                           href="#products_listing04"
                           aria-controls="products_listing04">
                            Balanced Funds
                          </a>
                        </h3>
                      </div>
                      <div class="panel-collapse collapse" role="tabpanel"
                        id="products_listing04"
                        aria-labelledby="product_title4">
                            <ul class="products_listing__sets">
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance Global Multi-Asset Fund</h4>
                                        <p>This fund aims to provide fixed income distribution and capital growth over a market
                                            cycle by investing in a diversified range of assets and markets worldwide.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>2</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>Asia</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>1.25%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to risks of investing in global equities, global fixed income securities
                                            and derivative risks as financial derivative instruments may be used as part of the investment process.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                                <li class="product_listing__item">
                                    <div class="product_listing__item__alpha">
                                        <h4>HSBC Insurance World Selection 3 Fund</h4>
                                        <p>This fund aims to provide long-term total return by investing in a portfolio of equity and fixed income
                                            securities, consistent with a medium risk investment strategy.</p>
                                    </div>
                                    <div class="product_listing__item__beta">
                                        <dl>
                                            <div>
                                                <dt>Risk Rating*</dt>
                                                <dd>3</dd>
                                            </div>
                                            <div>
                                                <dt>Region</dt>
                                                <dd>Global</dd>
                                            </div>
                                            <div>
                                                <dt>Fee</dt>
                                                <dd>1.45%</dd>
                                            </div>
                                            <div>
                                                <dt>Price</dt>
                                                <dd>S$1.00</dd>
                                            </div>
                                        </dl>
                                        <strong>Key Risks</strong>
                                        <p>The fund may be exposed to volatility and associated risks of investments
                                            in equities and bonds around the world.</p>
                                        <ul>
                                            <li><a href="#">Fund Factsheet</a></li>
                                            <li><a href="#">Product Highlights Sheet</a></li>
                                            <li><a href="#">Fund Summary</a></li>
                                        </ul>
                                    </div>
                                </li><!-- /.product_listing__item -->
                            </ul>
                        </div><!-- /.products_listing__category -->
                    </div>
                </div>

                <small id="id_fund_t_c" style="display: none;">*The Fund Risk Level stated in this website is determined according to the historical volatility of the fund or, where appropriate, the historical volatility of a comparable market
index or comparable fund. Please refer to the product summary to know more about the fund risk rating.</small>

            </section>

        </div><!--/ container -->
    </article>




    <section class="getaquote" id="get-a-quote">
        <div class="container">

            <form class="row getaquote__box">
                <div class="col-md-3 getaquote__alpha">
                    <h2>Customer Knowledge Assessment (CKA)</h2>
                </div>
                <div class="col-md-9 getaquote__beta">

                  <div id="getaquote__carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                      <div class="item active">
                        <!-- INITIAL -->
                        <!-- INITIAL -->
                        <fieldset class="getaquote__box__askset step0">
                            <h3>Customer Knowledge Assessment</h3>
                            <p>
                              This Customer Knowledge Assessment (CKA) is intended to help a
                              customer evaluate and thereafter to independently take a decision
                              to purchase Investment-linked Insurance Products (ILP). The CKA
                              will help you assess if you have the relevant knowledge or
                              experience to understand the risks and features of our ILP.
                              If you believe that you may not have the relevant experience
                              or knowledge to transact in ILPs on this self-directed online
                              platform, you must approach a financial consultant for advice.
                            </p>
                            <p>
                              Please go through the questions set out below.
                            </p>
                            <div class="getaquote__box__btnset">
                                <button class="btn btn-red" type="button"
                                        onclick="nextStep(1)">Next</button>
                            </div>
                        </fieldset>
                        <!-- END INITIAL -->
                        <!-- END INITIAL -->
                      </div>
                      <div class="item">
                        <!-- QUESTION 1 -->
                        <!-- QUESTION 1 -->
                        <fieldset class="getaquote__box__askset step1">
                            <h3>1) Do you hold a diploma or higher qualification in the following?</h3>
                            <ul>
                                <li>Accountancy</li>
                                <li>Actuarial Science</li>
                                <li>Business / Business Administration / Business Management / Business Studies</li>
                                <li>Capital Markets </li>
                                <li>Commerce</li>
                                <li>Economics</li>
                                <li>Finance</li>
                                <li>Financial Engineering</li>
                                <li>Financial Planning </li>
                                <li>Computational Finance and / or</li>
                                <li>Insurance</li>
                            </ul>
                            <div class="getaquote__box__btnset">
                                <button class="btn btn-default" type="button" onclick="nextStep(2, 1)">Yes</button>
                                <button class="btn btn-default" type="button" onclick="nextStep(2)">No</button>
                            </div>
                        </fieldset>
                        <!-- END QUESTION 1 -->
                        <!-- END QUESTION 1 -->
                      </div>
                      <div class="item">
                        <!-- QUESTION 2 -->
                        <!-- QUESTION 2 -->
                        <fieldset class="getaquote__box__askset step2">
                            <h3>2) Do you have a professional finance-related qualification such as?</h3>
                            <ul>
                                <li>The Chartered Financial Analyst Examination conducted by the CFA Institute, USA</li>
                                <li>The Association of Chartered Certified Accountants Qualifications</li>
                            </ul>
                            <div class="getaquote__box__btnset">
                                <button class="btn btn-default" type="button" onclick="nextStep(3, 1)">Yes</button>
                                <button class="btn btn-default" type="button" onclick="nextStep(3)">No</button>
                            </div>
                        </fieldset>
                        <!-- END QUESTION 2 -->
                        <!-- END QUESTION 2 -->
                      </div>
                      <div class="item">
                        <!-- QUESTION 3 -->
                        <!-- QUESTION 3 -->
                        <fieldset class="getaquote__box__askset step3">
                            <h3>3) In the past 10 years, do you have a minimum of 3 consecutive years of working experience in?</h3>
                            <ul>
                                <li>The development of investment products</li>
                                <li>The structuring of investment products</li>
                                <li>The management of investment products</li>
                                <li>The sale of investment products</li>
                                <li>The trading of investment products</li>
                                <li>Research and analysis of investment products or</li>
                                <li>The provision of training in investment products</li>
                            </ul>
                            <p>Work experience in accountancy, actuarial science, treasury and financial risk management are considered relevant experience. Work experience would also include the provision of legal advice or possession of legal expertise on the relevant areas listed above.</p>
                            <div class="getaquote__box__btnset">
                                <button class="btn btn-default" type="button" onclick="nextStep(4, 1)">Yes</button>
                                <button class="btn btn-default" type="button" onclick="nextStep(4)">No</button>
                            </div>
                        </fieldset>
                        <!-- END QUESTION 3 -->
                        <!-- END QUESTION 3 -->
                      </div>
                      <div class="item">
                        <!-- QUESTION 4 -->
                        <!-- QUESTION 4 -->
                        <fieldset class="getaquote__box__askset step4">
                            <h3>4) Have you in the last 3 years, invested at least 6 times in?</h3>
                            <ol>
                                <li>Collective Investment Schemes (CISs) such as unit trust or
                                    Investment Linked Policies (ILPs)</li>
                                <li>Any SIPs (e.g. Dual Currency Plus, Structured Notes, Structured Deposits, Market
                                    Access Platform and Equity Linked Notes) which are neither listed nor quoted on a
                                    securities market or a futures market (excluding CISs and ILPs)</li>
                                <li>Any SIPs (e.g. shares listed on the Hong Kong Stock Exchange, shares listed on the
                                    NewYork Stock Exchange) which are not listed on an exchange in Singapore but only
                                    listed on overseas exchanges</li>
                            </ol>
                            <div class="getaquote__box__btnset">
                                <button class="btn btn-default" type="button" onclick="nextStep(5, 1)">Yes</button>
                                <button class="btn btn-default" type="button" onclick="nextStep(5)">No</button>
                            </div>
                        </fieldset>
                        <!-- END QUESTION 4 -->
                        <!-- END QUESTION 4 -->
                      </div>
                      <div class="item">
                        <!-- IF When user passes CKA -->
                        <!-- IF When user passes CKA -->
                        <fieldset class="getaquote__box__askset step5">
                            <h3>Declaration</h3>
                            <p>I understand the purpose of the Customer Knowledge Assessment (CKA) and the outcome of my assessment.</p>

                            <p>I understand that this is a self-directed online platform to purchase Investment-linked Products (ILPs). HSBC Insurance does not provide any investment advice to clients transacting on this platform and I will not be able to rely on section 27 of the Financial Advisers Act to file a civil claim in the event that I suffer a loss. I acknowledge that it is entirely my responsibility to understand the risks, features and suitability of the ILPs should I decide to transact.</p>

                            <p>I declare that the above information provided by me is true and accurate, and any inaccurate or incomplete information provided by me may affect the CKA outcome.   </p>
                            <div class="getaquote__box__btnset">
                                <button class="btn btn-red" type="button" onclick="nextStep(6)">I accept</button>
                                <button class="btn btn-white" type="button" data-toggle="modal" data-target="#getaquote__modal">I decline</button>
                            </div>
                        </fieldset>
                        <!-- ENDIF When user passes CKA -->
                        <!-- ENDIF When user passes CKA -->
                      </div>
                      <div class="item">
                        <!-- QUOTE CALCULTE -->
                        <!-- QUOTE CALCULTE -->
                        <fieldset class="row getaquote__splitter get_a_quote">
                            <div class="col-sm-6">
                                <h3>5) Investment amount</h3>
                                <p>Drag the slider to adjust the monthly investment:</p>
                                <div class="form-group">
                                    <input class="form-control programmatic-range-input" type="programmatic-range-number" value="S$1500">
                                    <input type="programmatic-range" min="200" max="3000" step="50" value="1500">
                                    <div class="clearfix">
                                        <small class="min pull-left">S$200</small>
                                        <small class="max pull-right">S$3,000</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <small>
                                    Get a 10% discount on your premium for the first 3 months by entering the promo code: HSBCONLINE1
                                    <button type="button" class="help-tooltip" tabindex="0" data-container="body" label="help" data-toggle="popover" data-placement="bottom" aria-label="xxxxxxxxxxxxxxxxx" data-content="xxxxxxxxxxxxxxxxx"><i class="icon icon-circle-help-solid"></i></button>:
                                </small>
                                <div class="form-group">
                                    <input class="form-control" type="email">
                                </div>
                                <div class="getaquote__box__btnset">
                                    <button class="btn btn-default getaquote-btn" type="button" onclick="nextStep(7)">See estimated returns</button>
                                </div>
                            </div>
                        </fieldset>
                        <!-- END QUOTE CALCULTE -->
                        <!-- END QUOTE CALCULTE -->
                      </div>
                    </div>
                  </div>

                </div><!-- /.getaquote__beta -->
            </form>

            <!-- SHOW RESULT -->
            <!-- SHOW RESULT -->
            <div class="row getaquote__box getaquote__box--result hide">
                <div class="col-md-3 getaquote__alpha">
                    <%--<h2>Your quote</h2>--%>
                    <p>Based on a monthly investment of <em class="dynamic-bubble-result">S$xxxx</em>, here are estimates of your investment returns.</p>
                </div>
                <div class="col-md-9 getaquote__beta">
                    <div class="row getaquote__splitter">
                        <div class="col-sm-6">
                            <table class="getaquote__table">
                                <thead>
                                <tr>
                                    <th>Investment Period</th>
                                    <th>Projected investment return at 4% per annum (S$)</th>
                                    <th>Projected investment return at 8% per annum (S$)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>5 years</td>
                                    <td>56,000</td>
                                    <td>56,000</td>
                                </tr>
                                <tr>
                                    <td>10 years</td>
                                    <td>67,000</td>
                                    <td>167,000</td>
                                </tr>
                                <tr>
                                    <td>15 years</td>
                                    <td>78,000</td>
                                    <td>178,000</td>
                                </tr>
                                <tr>
                                    <td>20 years</td>
                                    <td>89,000</td>
                                    <td>189,000</td>
                                </tr>
                                </tbody>
                            </table>
                            <p class="promocode_success"><i class="icon icon-circle-confirmation"></i> Promo code successful. You are entitled to 10% off for the first 3 months.</p>
                        </div>
                        <div class="col-sm-6">
                            <p>These figures are only an estimate and is not yet an offer for any insurance product</p>
                            <div class="getaquote__box__btnset">
                                <button class="btn btn-red" onclick="openPage('<%=request.getContextPath()%>/our-plans/onlineinvestor-application/')">Buy Now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SHOW RESULT -->
            <!-- END SHOW RESULT -->

        </div>

    </section>

    <div class="container">

        <aside class="product_reference_materials">
            <h2>Product reference materials</h2>
            <ul>
                <li><a href="#">Product Summary and General Provisions (EN, PDF)</a></li>
            </ul>
        </aside>

        <hr class="section_divider" />

        <section class="customer_feedback">
            <h2>What our customers are saying</h2>
            <ul class="customer_feedback__listing">
                <li>
                    <strong>36 of 38 people found the following review helpful.</strong>
                    <strong>
                        <i class="review_star review_star--100"></i>
                        <i class="review_star review_star--100"></i>
                        <i class="review_star review_star--100"></i>
                        <i class="review_star review_star--100"></i>
                        <i class="review_star review_star--0"></i>
                    </strong>
                    <h3>
                        By <span>Peter Tan</span>
                        <small>Review posted 5 months ago</small>
                    </h3>
                    <p>Fast and easy claim process.
                        Process from submission to approval took only
                        one day!</p>
                    <div class="quickfeedback">
                        Yes, I recommend this product.
                    </div>
                    <div class="review_report">
                        Is this review helpful?
                        <button class="review_report__up" type="button">Yes</button>
                        <button class="review_report__down active" type="button">No</button>
                    </div>
                </li>
                <li>
                    <strong>37 of 38 people found the following review helpful.</strong>
                    <strong>
                        <i class="review_star review_star--100"></i>
                        <i class="review_star review_star--100"></i>
                        <i class="review_star review_star--100"></i>
                        <i class="review_star review_star--100"></i>
                        <i class="review_star review_star--0"></i>
                    </strong>
                    <h3>
                        By <span>Lee Lye Urn</span>
                        <small>Review posted 6 months ago</small>
                    </h3>
                    <p>Buying online is so convenient.</p>
                    <div class="quickfeedback quickfeedback--positive">
                        Yes, I recommend this product.
                    </div>
                    <div class="review_report">
                        Is this review helpful?
                        <button class="review_report__up active" type="button">Yes</button>
                        <button class="review_report__down" type="button">No</button>
                    </div>
                </li>
            </ul>

        </section>

        <hr class="section_divider" />

        <section class="products__trailer">
            <div class="row">
                <div class="col-md-6 faq">
                    <h3>Frequently Asked Questions</h3>
                    <form class="faq__search">
                        <input type="search" placeholder="Ask a question">
                        <button type="submit">Search FAQ</button>
                    </form>
                    <div class="faq-accordion" id="faq__listing">
                        <div class="HSBC-panel">
                            <h4>
                              <a class="" role="button" data-toggle="collapse" data-parent="#faq__listing" href="#faq01">
                                Common questions
                              </a>
                            </h4>
                            <div id="faq01" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading">
                              <ul style="list-style: none;margin: 0; padding: 0;">
                                <li>
                                  <a data-toggle="collapse" href="#faq01-01">What is Online Term plan (OT)?</a>
                                  <div id="faq01-01" class="panel-collapse collapse">
                                    xxxxxxxxxxx
                                  </div>
                                </li>
                                <li>
                                  <a data-toggle="collapse" href="#faq01-02">What does OT cover?</a>
                                  <div id="faq01-02" class="panel-collapse collapse">
                                    xxxxxxxxxxx
                                  </div>
                                </li>
                                <li>
                                  <a data-toggle="collapse" href="#faq01-03">What are the available investment-linked sub-funds that I can choose from?</a>
                                  <div id="faq01-03" class="panel-collapse collapse">
                                    xxxxxxxxxxx
                                  </div>
                                </li>
                                <li>
                                  <a data-toggle="collapse" href="#faq01-04">What are index funds?</a>
                                  <div id="faq01-04" class="panel-collapse collapse">
                                    xxxxxxxxxxx
                                  </div>
                                </li>
                                <li>
                                  <a data-toggle="collapse" href="#faq01-05">Are investment returns from HSBC Insurance OnlineInvestor guaranteed?</a>
                                  <div id="faq01-05" class="panel-collapse collapse">
                                    xxxxxxxxxxx
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 make_a_claim">
                    <h3>Making a Claim</h3>
                    <p>We know it can be a difficult time when you have to make an insurance claim. So, we have made it convenient for you to submit your claim online as soon as possible.</p>
                    <a class="btn btn-default" href="#">Submit</a>
                </div>
            </div>
        </section>

    </div><!-- /.container -->

</div>

<div class="modal fade" id="getaquote__modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>Sorry!</h3>
            </div>
            <div class="modal-body">
              <p>
                You have been assessed to not have the relevant knowledge and/or
                experience to transact in Investment-linked Insurance Products (ILP)
                based on Singapore regulatory standards. As such, you will need to
                seek advice from a qualified financial consultant to help you.
              </p>
              <p>
                We recommend that you visit the nearest HSBC Branch so that our
                financial consultants can provide you with advice, but please note
                that the charges, features and benefits for advised products will be
                different from products offered on this online insurance platform.
              </p>
            </div>
        </div>
    </div>
</div>



<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script>
    $('#getaquote__carousel').carousel({
      interval: false
    });

    /*
    $('#getaquote__carousel .item').each(function() {
      var $divs = $(this).add($(this).prev('.item'));
      var tallestHeight = $divs.map(function(i, el) {
          return $(el).height();
      }).get();
      $divs.css('min-height',(Math.max.apply(this, tallestHeight)));
    });
    */

    $(document).ready(function () {
        $('#getaquote__modal').modal('hide')
        $(".getaquote__box.getaquote__box--result").removeClass("show").addClass("hide");
    });

    var countYes = 0;
    function nextStep(step, yes) {
        if(step >= 0 && step <= 6) {
            if (step == 1) {
                countYes = 0;
                $('#getaquote__modal').modal('hide')
            }
            if (yes == 1) {
                countYes += 1;
            }
            if (step == 5) {
                if (countYes == 0) {
                    $('#getaquote__modal').modal('show')
                    $('#getaquote__carousel').carousel(0);
                    return;
                }
            }
            $('#getaquote__carousel').carousel(step);
        } else if (step == 7) {
            $(".getaquote__box.getaquote__box--result").removeClass("hide").addClass("show");
            $(".getaquote-btn").text('Recalculate');

            var amount = $("input[type='programmatic-range-number']").val();
            $(".dynamic-bubble-result").text(amount);
        }

        if(step == 6 || step == 7){
            $(".getaquote__alpha h2").text('See Estimated Returns');
        }else
        {
            $(".getaquote__alpha h2").text('Customer Knowledge Assessment (CKA)');
        }
    }

    $('#products_listing').on('hidden.bs.collapse', function () {
        var count = $('#products_listing .collapsed').size();
        if (count == 4) {
            $("#id_fund_t_c").hide();
        }
    });

    $('#products_listing').on('show.bs.collapse', function () {
        $("#id_fund_t_c").show();
    })

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }
</script>
