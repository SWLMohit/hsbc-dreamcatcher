<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

  <div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
      <a href="#">Home</a>
      <a href="#">Our Plans</a>
      <a href="#">HSBC Insurance OnlineInvestor</a>
      <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
    </nav>
  </div>

  <article>
    <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
      <div class="masthead__container container">
      </div>
    </section>

    <section class="onlineinvestor">
      <div class="container">
        <h1>HSBC Insurance OnlineInvestor application</h1>


        <div class="onlineinvestor__breadcrumb">
          <a href="#" class="current">Eligibility Check</a>
          <a href="#" class="disabled">Your Plan</a>
          <a href="#" class="disabled">Your details</a>
          <a href="#" class="disabled">Payment</a>
        </div>


        <div class="row">
          <div class="col-md-8">
            <form>
              <div class="onlineinvestor_carousel">
                <div class="form-item item2 active">
                  <fieldset>
                    <legend>Tell us more about yourself before proceeding with your application</legend>
                    <dl class="onlineinvestor__questionnaire">
                      <dt>What is your country of nationality?</dt>
                      <dd class="form-group">
                        <div class="form-select_wrap"><select class="form-control control_country_select">
                          <option>Singapore</option>
                          <option>Australia</option>
                          <option>Austria</option>
                          <option>Bahrain</option>
                          <option>Belgium</option>
                          <option>Bermuda</option>
                          <option>Brunei</option>
                          <option>Canada</option>
                          <option>Hong Kong</option>
                          <option>Denmark</option>
                          <option>Finland</option>
                          <option>France</option>
                          <option>Germany</option>
                          <option>Ireland</option>
                          <option>Italy</option>
                          <option>Japan</option>
                          <option>South Korea</option>
                          <option>Liechtenstein</option>
                          <option>Luxembourg</option>
                          <option>Malaysia</option>
                          <option>Netherlands</option>
                          <option>New Zealand</option>
                          <option>Norway</option>
                          <option>Singapore</option>
                          <option>Spain</option>
                          <option>Sweden</option>
                          <option>Switzerland</option>
                          <option>Taiwan</option>
                          <option>United Arab Emirates</option>
                          <option>United Kingdom</option>
                          <option>United States of America</option>
                          <option>Vatican City</option>
                          <option>China</option>
                          <option>India</option>
                          <option>Indonesia</option>
                          <option>Philippines</option>
                          <option>Thailand</option>
                          <option>Vietnam</option>
                          <option value="none_of_above">None of the above</option>
                        </select></div>
                      </dd>

                      <dt>If you are a national of more than one country, please select addtional country / countries <strong>(optional)</strong></dt>
                      <div class="country-dropdown-container">
                        <dd class="form-group">
                        <p>Country 2:</p>
                        <div class="form-select_wrap">
                          <select class="form-control control_country_select">
                            <option disabled selected>Please select&hellip;</option>
                            <option>Singapore</option>
                            <option>Australia</option>
                            <option>Austria</option>
                            <option>Bahrain</option>
                            <option>Belgium</option>
                            <option>Bermuda</option>
                            <option>Brunei</option>
                            <option>Canada</option>
                            <option>Hong Kong</option>
                            <option>Denmark</option>
                            <option>Finland</option>
                            <option>France</option>
                            <option>Germany</option>
                            <option>Ireland</option>
                            <option>Italy</option>
                            <option>Japan</option>
                            <option>South Korea</option>
                            <option>Liechtenstein</option>
                            <option>Luxembourg</option>
                            <option>Malaysia</option>
                            <option>Netherlands</option>
                            <option>New Zealand</option>
                            <option>Norway</option>
                            <option>Singapore</option>
                            <option>Spain</option>
                            <option>Sweden</option>
                            <option>Switzerland</option>
                            <option>Taiwan</option>
                            <option>United Arab Emirates</option>
                            <option>United Kingdom</option>
                            <option>United States of America</option>
                            <option>Vatican City</option>
                            <option>China</option>
                            <option>India</option>
                            <option>Indonesia</option>
                            <option>Philippines</option>
                            <option>Thailand</option>
                            <option>Vietnam</option>
                            <option value="none_of_above">None of the above</option>
                          </select></div>
                      </dd>
                      </div>
                      <!-- Add More Something Button -->
                      <dt><button type="button" class="blocklink" onclick="addCountry()"><i class="icon icon-add"></i> Add another country</button></dt>
                      <dd>&nbsp;</dd>
                      <!-- Add More Something Button -->

                      <dt>Have you smoked in the past 12 months</dt>
                      <dd class="form-group">
                        <div class="newradio--inline">
                          <input id="id_smoking_option1" type="radio" name="smoking" value="option1">
                          <label for="id_smoking_option1"><span><span></span></span>Yes</label>
                        </div>
                        <div class="newradio--inline">
                          <input id="id_smoking_option2" type="radio" name="smoking" value="option2">
                          <label for="id_smoking_option2"><span><span></span></span>No</label>
                        </div>
                      </dd>

                      <dt>Who are you buying the policy for?</dt>
                      <dd class="form-group">
                        <div class="form-select_wrap">
                          <select class="form-control" id="select_buy_for">
                            <option disabled selected>Please select&hellip;</option>
                            <option value="myself">Myself</option>
                            <option value="your spouse">My spouse</option>
                            <option value="your child">My child</option>
                            <option value="your legal ward">My legal ward</option>
                          </select></div>
                      </dd>
                    </dl>
                  </fieldset>
                  <div class="article__leader__main__btnset">
                    <button type="button" class="btn btn-red" onclick="nextStep(3)">Continue</button>
                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application-step1a/')">Back</button>
                  </div>

                </div><!-- /.item -->
              </div>
            </form>
          </div>
        </div><!-- /.row -->
      </div><!--/ container -->
    </section>

  </article>


  <div class="modal fade" id="onlineinvestormodal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                  onclick="openPage('../onlineinvestor-application/')">
            <span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
        </div>
        <div class="modal-body">
          <p>Based on your response(s) we are unable to proceed with your application.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red" data-dismiss="modal"
                  onclick="openPage('../onlineinvestor-application/')">Back</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="onlineinvestormodal_child">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                  onclick="openPage('../onlineinvestor-application/')">
            <span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
        </div>
        <div class="modal-body">
          <p>Your child or Legal ward has to be 18 years old and below in order to for you to continue with this application form.</p>
          <p>Your child, however, can purchase this product in his/her own name.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red" data-dismiss="modal"
                  onclick="openPage('../onlineinvestor-application/')">Back</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="onlineinvestormodal_warning">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title" id="myModalLabel">Warning!</h2>
        </div>
        <div class="modal-body">
          <p>Your child or Legal ward has to be 17 years old and below in order to for you to continue with this application form.</p>
          <p>Your child, however, can purchase this product in his/her own name.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red" data-dismiss="modal">Continue</button>
        </div>
      </div>
    </div>
  </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">

    $(document).ready(function(){
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);
    });

    function nextStep(step) {

        if (step == 3) {
            var buyFor = $("#select_buy_for").val();
            if (buyFor == 'myself') {
                openPage('../onlineinvestor-application-step1e/');
            }
            else {
                openPage('../onlineinvestor-application-step1c/');

            }
        }
    };

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }

    $("select.control_residency_select").change(function() {
        if ($(this).val() == 'none_of_above') {
            $("#onlineinvestormodal").modal("show");
        }
        if ($(this).val() == 'singaporean' || $(this).val() == 'singaporean_pr') {
            $(".control_singapore").show();
            $(".control_passport_label").html('NRIC');
        } else {
            $(".control_singapore").hide();
            $(".control_passport_label").html('Passport number');
        }
    });

    $('select.control_country_select').on('change', function() {
        if ($(this).val() == 'none_of_above') {
            $("#onlineinvestormodal").modal("show");
        }
    });

    $("#select_buy_for").change(function () {
        $(".control_employment").hide();
        $("#label_buy_for").html($(this).val());
        if ($(this).val() == 'your child' || $(this).val() == 'your legal ward') {
            $("#onlineinvestormodal_warning").modal("show");
            $(".control_option_child").show();
        } else {
            $(".control_option_child").hide();
        }
    });

    $("select#control_buy_for_year").change(function () {
        var buyFor = $("#select_buy_for").val();
        if (buyFor == 'your child' || buyFor == 'your legal ward') {
            if ($(this).val() < '1999') {
                $("#onlineinvestormodal_child").modal("show");
            }
        }
    });

    $("select.control_employment_select").change(function() {
        if ($(this).val() == 'student') {
            $(".control_study").show();
        } else {
            $(".control_study").hide();
        }
        if ($(this).val() == 'student' || $(this).val() == 'homemaker' || $(this).val() == 'retired' || $(this).val() == 'unemployed') {
            $(".control_occupation").hide();
            $(".control_industry").hide();
            $(".control_company").hide();
            $(".control_employment_country").hide();
            $(".control_employment_city").hide();
            $("input.control_salary").val("0");
        } else {
            $(".control_occupation").show();
            $(".control_industry").show();
            $(".control_company").show();
            $(".control_employment_country").show();
            $(".control_employment_city").show();
            $("input.control_salary").val("");
        }
        if ($(this).val() == 'self-employed') {
            $(".control_company_label").html("Company");
        }
        if ($(this).val() == 'salaried') {
            $(".control_company_label").html("Employer")
        }
    })

    var numTaxCountryDropdowns = 2;
    function addCountry(){
        $('.country-dropdown-container dd:nth-of-type(1)').clone(true).appendTo('.country-dropdown-container');
        $('.country-dropdown-container dd:nth-last-of-type(1) p').text('Country ' + ++numTaxCountryDropdowns +':');
    }
</script>
