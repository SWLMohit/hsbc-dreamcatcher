<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our Plans</a>
            <a href="#">HSBC Insurance OnlineInvestor</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
            <div class="masthead__container container">
            </div>
        </section>

        <section class="onlineinvestor">
            <div class="container">
                <h1>HSBC Insurance OnlineInvestor application</h1>


                <div class="onlineinvestor__breadcrumb">
                    <a href="../onlineinvestor-application-step1a/">Eligibility check</a>
                    <a href="../onlineinvestor-application-step2a/">Your plan</a>
                    <a href="#" class="current">Your details</a>
                    <a href="#" class="disabled">Payment</a>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <form>
                          <div class="onlineinvestor_carousel">
                              <div class="form-item item2 active">
                                <fieldset>
                                  <legend>Please provide us with your personal particulars</legend>
                                  <dl class="onlineinvestor__questionnaire">
                                    <dt>What is your country of birth?</dt>
                                    <dd class="form-group">
                                      <div class="form-select_wrap">
                                        <select class="form-control">
                                        <option disabled>Please select&hellip;</option>
                                            <option selected>Singapore</option>
                                            <option>Afghanistan</option>
                                            <option>Albania</option>
                                            <option>Algeria</option>
                                            <option>American Samoa</option>
                                            <option>Andorra</option>
                                            <option>Angola</option>
                                            <option>Anguilla</option>
                                            <option>Antarctica</option>
                                            <option>Antigua and Barbuda</option>
                                            <option>Argentina</option>
                                            <option>Armenia</option>
                                            <option>Aruba</option>
                                            <option>Australia</option>
                                            <option>Austria</option>
                                            <option>Azerbaijan</option>
                                            <option>Bahama</option>
                                            <option>Bahrain</option>
                                            <option>Bangladesh</option>
                                            <option>Barbados</option>
                                            <option>Belarus</option>
                                            <option>Belgium</option>
                                            <option>Belize</option>
                                            <option>Benin</option>
                                            <option>Bermuda</option>
                                            <option>Bhutan</option>
                                            <option>Bolivia</option>
                                            <option>Bosnia and Herzegovina</option>
                                            <option>Botswana</option>
                                            <option>Bouvet Island</option>
                                            <option>Brazil</option>
                                            <option>British Indian Ocean Territory</option>
                                            <option>British Virgin Islands</option>
                                            <option>Brunei Darussalam</option>
                                            <option>Bulgaria</option>
                                            <option>Burkina Faso</option>
                                            <option>Burundi</option>
                                            <option>Cambodia</option>
                                            <option>Cameroon</option>
                                            <option>Canada</option>
                                            <option>Cape Verde</option>
                                            <option>Cayman Islands</option>
                                            <option>Central African Republic</option>
                                            <option>Chad</option>
                                            <option>Chile</option>
                                            <option>China</option>
                                            <option>Christmas Island</option>
                                            <option>Cocos (Keeling) Islands</option>
                                            <option>Colombia</option>
                                            <option>Comoros</option>
                                            <option>Congo</option>
                                            <option>Cook Islands</option>
                                            <option>Costa Rica</option>
                                            <option>Croatia</option>
                                            <option>Cuba</option>
                                            <option>Cyprus</option>
                                            <option>Czech Republic</option>
                                            <option>Denmark</option>
                                            <option>Djibouti</option>
                                            <option>Dominica</option>
                                            <option>Dominican Republic</option>
                                            <option>East Timor</option>
                                            <option>Ecuador</option>
                                            <option>Egypt</option>
                                            <option>El Salvador</option>
                                            <option>Equatorial Guinea</option>
                                            <option>Eritrea</option>
                                            <option>Estonia</option>
                                            <option>Ethiopia</option>
                                            <option>Falkland Islands (Malvinas)</option>
                                            <option>Faroe Islands</option>
                                            <option>Fiji</option>
                                            <option>Finland</option>
                                            <option>France</option>
                                            <option>France, Metropolitan</option>
                                            <option>French Guiana</option>
                                            <option>French Polynesia</option>
                                            <option>French Southern Territories</option>
                                            <option>Gabon</option>
                                            <option>Gambia</option>
                                            <option>Georgia</option>
                                            <option>Germany</option>
                                            <option>Ghana</option>
                                            <option>Gibraltar</option>
                                            <option>Greece</option>
                                            <option>Greenland</option>
                                            <option>Grenada</option>
                                            <option>Guadeloupe</option>
                                            <option>Guam</option>
                                            <option>Guatemala</option>
                                            <option>Guinea</option>
                                            <option>Guinea-Bissau</option>
                                            <option>Guyana</option>
                                            <option>Haiti</option>
                                            <option>Heard Island and McDonald Islands</option>
                                            <option>Honduras</option>
                                            <option>Hong Kong</option>
                                            <option>Hungary</option>
                                            <option>Iceland</option>
                                            <option>India</option>
                                            <option>Indonesia</option>
                                            <option>Islamic Republic of Iran</option>
                                            <option>Iraq</option>
                                            <option>Ireland</option>
                                            <option>Israel</option>
                                            <option>Italy</option>
                                            <option>Jamaica</option>
                                            <option>Japan</option>
                                            <option>Jordan</option>
                                            <option>Kazakhstan</option>
                                            <option>Kenya</option>
                                            <option>Kiribati</option>
                                            <option>Korea, Democratic People's Republic of</option>
                                            <option>Korea, Republic of</option>
                                            <option>Kuwait</option>
                                            <option>Kyrgyzstan</option>
                                            <option>Lao People's Democratic Republic</option>
                                            <option>Latvia</option>
                                            <option>Lebanon</option>
                                            <option>Lesotho</option>
                                            <option>Liberia</option>
                                            <option>Libyan Arab Jamahiriya</option>
                                            <option>Liechtenstein</option>
                                            <option>Lithuania</option>
                                            <option>Luxembourg</option>
                                            <option>Macau</option>
                                            <option>Madagascar</option>
                                            <option>Malawi</option>
                                            <option>Malaysia</option>
                                            <option>Maldives</option>
                                            <option>Mali</option>
                                            <option>Malta</option>
                                            <option>Marshall Islands</option>
                                            <option>Martinique</option>
                                            <option>Mauritania</option>
                                            <option>Mauritius</option>
                                            <option>Mayotte</option>
                                            <option>Mexico</option>
                                            <option>Micronesia</option>
                                            <option>Moldova, Republic of</option>
                                            <option>Monaco</option>
                                            <option>Mongolia</option>
                                            <option>Monserrat</option>
                                            <option>Morocco</option>
                                            <option>Mozambique</option>
                                            <option>Myanmar</option>
                                            <option>Nambia</option>
                                            <option>Nauru</option>
                                            <option>Nepal</option>
                                            <option>Netherlands</option>
                                            <option>Netherlands Antilles</option>
                                            <option>New Caledonia</option>
                                            <option>New Zealand</option>
                                            <option>Nicaragua</option>
                                            <option>Niger</option>
                                            <option>Nigeria</option>
                                            <option>Niue</option>
                                            <option>Norfolk Island</option>
                                            <option>Northern Mariana Islands</option>
                                            <option>Norway</option>
                                            <option>Oman</option>
                                            <option>Pakistan</option>
                                            <option>Palau</option>
                                            <option>Panama</option>
                                            <option>Papua New Guinea</option>
                                            <option>Paraguay</option>
                                            <option>Peru</option>
                                            <option>Philippines</option>
                                            <option>Pitcairn</option>
                                            <option>Poland</option>
                                            <option>Portugal</option>
                                            <option>Puerto Rico</option>
                                            <option>Qatar</option>
                                            <option>Romania</option>
                                            <option>Russian Federation</option>
                                            <option>Rwanda</option>
                                            <option>S. Georgia & the S. Sandwich Islands</option>
                                            <option>Saint Lucia</option>
                                            <option>Samoa</option>
                                            <option>San Marino</option>
                                            <option>Sao Tome and Principe</option>
                                            <option>Saudi Arabia</option>
                                            <option>Senegal</option>
                                            <option>Seychelles</option>
                                            <option>Sierra Leone</option>
                                            <option>Slovakia</option>
                                            <option>Slovenia</option>
                                            <option>Solomon Islands</option>
                                            <option>Somalia</option>
                                            <option>South Africa</option>
                                            <option>Spain</option>
                                            <option>Sri Lanka</option>
                                            <option>St. Helena</option>
                                            <option>St. Kitts and Nevis</option>
                                            <option>St. Pierre and Miquelon</option>
                                            <option>St. Vincent and the Grenadines</option>
                                            <option>Sudan</option>
                                            <option>Suriname</option>
                                            <option>Svalbard and Jan Mayen Islands</option>
                                            <option>Swaziland</option>
                                            <option>Sweden</option>
                                            <option>Switzerland</option>
                                            <option>Syrian Arab Republic</option>
                                            <option>Taiwan, Province of China</option>
                                            <option>Tajikistan</option>
                                            <option>Thailand</option>
                                            <option>Togo</option>
                                            <option>Tokelau</option>
                                            <option>Tonga</option>
                                            <option>Trinidad and Tobago</option>
                                            <option>Tunisia</option>
                                            <option>Turkey</option>
                                            <option>Turkmenistan</option>
                                            <option>Turks and Caicos Islands</option>
                                            <option>Tuvalu</option>
                                            <option>Uganda</option>
                                            <option>Ukraine</option>
                                            <option>United Arab Emirates</option>
                                            <option>United Kingdom (Great Britain)</option>
                                            <option>United Republic of Tanzania</option>                                            
                                            <option>United States of America</option>
                                            <option>United States Minor Outlying Islands</option>
                                            <option>United States Virgin Islands</option>
                                            <option>Uruguay</option>
                                            <option>Uzbekistan</option>
                                            <option>Vanuatu</option>
                                            <option>Vatican City State (Holy See)</option>
                                            <option>Venezuela</option>
                                            <option>Viet Nam</option>
                                            <option>Wallis and Futuna Islands</option>
                                            <option>Western Sahara</option>
                                            <option>Yugoslavia</option>
                                            <option>Zaire</option>
                                            <option>Zambia</option>
                                            <option>Zimbabwe</option>
                                      </select></div>
                                    </dd>
                                    <dt>Please complete the following section indicating
                                    where you are a tax resident <button type="button" class="help-tooltip" tabindex="0" 
                                    data-container="body" data-toggle="popover" data-placement="top" label="help"
                                    aria-label="The tax residency is the country where one is resident or registered for tax purposes."
                                    data-content="The tax residency is the country where one is resident or registered for tax purposes.">
                                    <i class="icon icon-circle-help-solid"></i></button> of and your TIN 
                                    where appropriate:</dt>
                                      <div class="country-dropdown-container">
                                          <dd class="form-group">
                                              <p>Country 1:</p>
                                              <div class="form-select_wrap">
                                                  <select class="form-control tax-country-dropdown">
                                                      <option disabled>Please select&hellip;</option>
                                                      <option selected>Singapore</option>
                                                      <option>Afghanistan</option>
                                                      <option>Albania</option>
                                                      <option>Algeria</option>
                                                      <option>American Samoa</option>
                                                      <option>Andorra</option>
                                                      <option>Angola</option>
                                                      <option>Anguilla</option>
                                                      <option>Antarctica</option>
                                                      <option>Antigua and Barbuda</option>
                                                      <option>Argentina</option>
                                                      <option>Armenia</option>
                                                      <option>Aruba</option>
                                                      <option>Australia</option>
                                                      <option>Austria</option>
                                                      <option>Azerbaijan</option>
                                                      <option>Bahama</option>
                                                      <option>Bahrain</option>
                                                      <option>Bangladesh</option>
                                                      <option>Barbados</option>
                                                      <option>Belarus</option>
                                                      <option>Belgium</option>
                                                      <option>Belize</option>
                                                      <option>Benin</option>
                                                      <option>Bermuda</option>
                                                      <option>Bhutan</option>
                                                      <option>Bolivia</option>
                                                      <option>Bosnia and Herzegovina</option>
                                                      <option>Botswana</option>
                                                      <option>Bouvet Island</option>
                                                      <option>Brazil</option>
                                                      <option>British Indian Ocean Territory</option>
                                                      <option>British Virgin Islands</option>
                                                      <option>Brunei Darussalam</option>
                                                      <option>Bulgaria</option>
                                                      <option>Burkina Faso</option>
                                                      <option>Burundi</option>
                                                      <option>Cambodia</option>
                                                      <option>Cameroon</option>
                                                      <option>Canada</option>
                                                      <option>Cape Verde</option>
                                                      <option>Cayman Islands</option>
                                                      <option>Central African Republic</option>
                                                      <option>Chad</option>
                                                      <option>Chile</option>
                                                      <option>China</option>
                                                      <option>Christmas Island</option>
                                                      <option>Cocos (Keeling) Islands</option>
                                                      <option>Colombia</option>
                                                      <option>Comoros</option>
                                                      <option>Congo</option>
                                                      <option>Cook Islands</option>
                                                      <option>Costa Rica</option>
                                                      <option>Croatia</option>
                                                      <option>Cuba</option>
                                                      <option>Cyprus</option>
                                                      <option>Czech Republic</option>
                                                      <option>Denmark</option>
                                                      <option>Djibouti</option>
                                                      <option>Dominica</option>
                                                      <option>Dominican Republic</option>
                                                      <option>East Timor</option>
                                                      <option>Ecuador</option>
                                                      <option>Egypt</option>
                                                      <option>El Salvador</option>
                                                      <option>Equatorial Guinea</option>
                                                      <option>Eritrea</option>
                                                      <option>Estonia</option>
                                                      <option>Ethiopia</option>
                                                      <option>Falkland Islands (Malvinas)</option>
                                                      <option>Faroe Islands</option>
                                                      <option>Fiji</option>
                                                      <option>Finland</option>
                                                      <option>France</option>
                                                      <option>France, Metropolitan</option>
                                                      <option>French Guiana</option>
                                                      <option>French Polynesia</option>
                                                      <option>French Southern Territories</option>
                                                      <option>Gabon</option>
                                                      <option>Gambia</option>
                                                      <option>Georgia</option>
                                                      <option>Germany</option>
                                                      <option>Ghana</option>
                                                      <option>Gibraltar</option>
                                                      <option>Greece</option>
                                                      <option>Greenland</option>
                                                      <option>Grenada</option>
                                                      <option>Guadeloupe</option>
                                                      <option>Guam</option>
                                                      <option>Guatemala</option>
                                                      <option>Guinea</option>
                                                      <option>Guinea-Bissau</option>
                                                      <option>Guyana</option>
                                                      <option>Haiti</option>
                                                      <option>Heard Island and McDonald Islands</option>
                                                      <option>Honduras</option>
                                                      <option>Hong Kong</option>
                                                      <option>Hungary</option>
                                                      <option>Iceland</option>
                                                      <option>India</option>
                                                      <option>Indonesia</option>
                                                      <option>Islamic Republic of Iran</option>
                                                      <option>Iraq</option>
                                                      <option>Ireland</option>
                                                      <option>Israel</option>
                                                      <option>Italy</option>
                                                      <option>Jamaica</option>
                                                      <option>Japan</option>
                                                      <option>Jordan</option>
                                                      <option>Kazakhstan</option>
                                                      <option>Kenya</option>
                                                      <option>Kiribati</option>
                                                      <option>Korea, Democratic People's Republic of</option>
                                                      <option>Korea, Republic of</option>
                                                      <option>Kuwait</option>
                                                      <option>Kyrgyzstan</option>
                                                      <option>Lao People's Democratic Republic</option>
                                                      <option>Latvia</option>
                                                      <option>Lebanon</option>
                                                      <option>Lesotho</option>
                                                      <option>Liberia</option>
                                                      <option>Libyan Arab Jamahiriya</option>
                                                      <option>Liechtenstein</option>
                                                      <option>Lithuania</option>
                                                      <option>Luxembourg</option>
                                                      <option>Macau</option>
                                                      <option>Madagascar</option>
                                                      <option>Malawi</option>
                                                      <option>Malaysia</option>
                                                      <option>Maldives</option>
                                                      <option>Mali</option>
                                                      <option>Malta</option>
                                                      <option>Marshall Islands</option>
                                                      <option>Martinique</option>
                                                      <option>Mauritania</option>
                                                      <option>Mauritius</option>
                                                      <option>Mayotte</option>
                                                      <option>Mexico</option>
                                                      <option>Micronesia</option>
                                                      <option>Moldova, Republic of</option>
                                                      <option>Monaco</option>
                                                      <option>Mongolia</option>
                                                      <option>Monserrat</option>
                                                      <option>Morocco</option>
                                                      <option>Mozambique</option>
                                                      <option>Myanmar</option>
                                                      <option>Nambia</option>
                                                      <option>Nauru</option>
                                                      <option>Nepal</option>
                                                      <option>Netherlands</option>
                                                      <option>Netherlands Antilles</option>
                                                      <option>New Caledonia</option>
                                                      <option>New Zealand</option>
                                                      <option>Nicaragua</option>
                                                      <option>Niger</option>
                                                      <option>Nigeria</option>
                                                      <option>Niue</option>
                                                      <option>Norfolk Island</option>
                                                      <option>Northern Mariana Islands</option>
                                                      <option>Norway</option>
                                                      <option>Oman</option>
                                                      <option>Pakistan</option>
                                                      <option>Palau</option>
                                                      <option>Panama</option>
                                                      <option>Papua New Guinea</option>
                                                      <option>Paraguay</option>
                                                      <option>Peru</option>
                                                      <option>Philippines</option>
                                                      <option>Pitcairn</option>
                                                      <option>Poland</option>
                                                      <option>Portugal</option>
                                                      <option>Puerto Rico</option>
                                                      <option>Qatar</option>
                                                      <option>Romania</option>
                                                      <option>Russian Federation</option>
                                                      <option>Rwanda</option>
                                                      <option>S. Georgia & the S. Sandwich Islands</option>
                                                      <option>Saint Lucia</option>
                                                      <option>Samoa</option>
                                                      <option>San Marino</option>
                                                      <option>Sao Tome and Principe</option>
                                                      <option>Saudi Arabia</option>
                                                      <option>Senegal</option>
                                                      <option>Seychelles</option>
                                                      <option>Sierra Leone</option>
                                                      <option>Slovakia</option>
                                                      <option>Slovenia</option>
                                                      <option>Solomon Islands</option>
                                                      <option>Somalia</option>
                                                      <option>South Africa</option>
                                                      <option>Spain</option>
                                                      <option>Sri Lanka</option>
                                                      <option>St. Helena</option>
                                                      <option>St. Kitts and Nevis</option>
                                                      <option>St. Pierre and Miquelon</option>
                                                      <option>St. Vincent and the Grenadines</option>
                                                      <option>Sudan</option>
                                                      <option>Suriname</option>
                                                      <option>Svalbard and Jan Mayen Islands</option>
                                                      <option>Swaziland</option>
                                                      <option>Sweden</option>
                                                      <option>Switzerland</option>
                                                      <option>Syrian Arab Republic</option>
                                                      <option>Taiwan, Province of China</option>
                                                      <option>Tajikistan</option>
                                                      <option>Thailand</option>
                                                      <option>Togo</option>
                                                      <option>Tokelau</option>
                                                      <option>Tonga</option>
                                                      <option>Trinidad and Tobago</option>
                                                      <option>Tunisia</option>
                                                      <option>Turkey</option>
                                                      <option>Turkmenistan</option>
                                                      <option>Turks and Caicos Islands</option>
                                                      <option>Tuvalu</option>
                                                      <option>Uganda</option>
                                                      <option>Ukraine</option>
                                                      <option>United Arab Emirates</option>
                                                      <option>United Kingdom (Great Britain)</option>
                                                      <option>United Republic of Tanzania</option>                                                      
                                                      <option>United States of America</option>
                                                      <option>United States Minor Outlying Islands</option>
                                                      <option>United States Virgin Islands</option>
                                                      <option>Uruguay</option>
                                                      <option>Uzbekistan</option>
                                                      <option>Vanuatu</option>
                                                      <option>Vatican City State (Holy See)</option>
                                                      <option>Venezuela</option>
                                                      <option>Viet Nam</option>
                                                      <option>Wallis and Futuna Islands</option>
                                                      <option>Western Sahara</option>
                                                      <option>Yugoslavia</option>
                                                      <option>Zaire</option>
                                                      <option>Zambia</option>
                                                      <option>Zimbabwe</option>
                                                  </select></div>
                                          </dd>
                                          <dd class="form-group">
                                              <p>Country 2:</p>
                                              <div class="form-select_wrap">
                                                  <select class="form-control tax-country-dropdown">
                                                      <option selected>Please select&hellip;</option>
                                                      <option>Singapore</option>
                                                      <option>Afghanistan</option>
                                                      <option>Albania</option>
                                                      <option>Algeria</option>
                                                      <option>American Samoa</option>
                                                      <option>Andorra</option>
                                                      <option>Angola</option>
                                                      <option>Anguilla</option>
                                                      <option>Antarctica</option>
                                                      <option>Antigua and Barbuda</option>
                                                      <option>Argentina</option>
                                                      <option>Armenia</option>
                                                      <option>Aruba</option>
                                                      <option>Australia</option>
                                                      <option>Austria</option>
                                                      <option>Azerbaijan</option>
                                                      <option>Bahama</option>
                                                      <option>Bahrain</option>
                                                      <option>Bangladesh</option>
                                                      <option>Barbados</option>
                                                      <option>Belarus</option>
                                                      <option>Belgium</option>
                                                      <option>Belize</option>
                                                      <option>Benin</option>
                                                      <option>Bermuda</option>
                                                      <option>Bhutan</option>
                                                      <option>Bolivia</option>
                                                      <option>Bosnia and Herzegovina</option>
                                                      <option>Botswana</option>
                                                      <option>Bouvet Island</option>
                                                      <option>Brazil</option>
                                                      <option>British Indian Ocean Territory</option>
                                                      <option>British Virgin Islands</option>
                                                      <option>Brunei Darussalam</option>
                                                      <option>Bulgaria</option>
                                                      <option>Burkina Faso</option>
                                                      <option>Burundi</option>
                                                      <option>Cambodia</option>
                                                      <option>Cameroon</option>
                                                      <option>Canada</option>
                                                      <option>Cape Verde</option>
                                                      <option>Cayman Islands</option>
                                                      <option>Central African Republic</option>
                                                      <option>Chad</option>
                                                      <option>Chile</option>
                                                      <option>China</option>
                                                      <option>Christmas Island</option>
                                                      <option>Cocos (Keeling) Islands</option>
                                                      <option>Colombia</option>
                                                      <option>Comoros</option>
                                                      <option>Congo</option>
                                                      <option>Cook Islands</option>
                                                      <option>Costa Rica</option>
                                                      <option>Croatia</option>
                                                      <option>Cuba</option>
                                                      <option>Cyprus</option>
                                                      <option>Czech Republic</option>
                                                      <option>Denmark</option>
                                                      <option>Djibouti</option>
                                                      <option>Dominica</option>
                                                      <option>Dominican Republic</option>
                                                      <option>East Timor</option>
                                                      <option>Ecuador</option>
                                                      <option>Egypt</option>
                                                      <option>El Salvador</option>
                                                      <option>Equatorial Guinea</option>
                                                      <option>Eritrea</option>
                                                      <option>Estonia</option>
                                                      <option>Ethiopia</option>
                                                      <option>Falkland Islands (Malvinas)</option>
                                                      <option>Faroe Islands</option>
                                                      <option>Fiji</option>
                                                      <option>Finland</option>
                                                      <option>France</option>
                                                      <option>France, Metropolitan</option>
                                                      <option>French Guiana</option>
                                                      <option>French Polynesia</option>
                                                      <option>French Southern Territories</option>
                                                      <option>Gabon</option>
                                                      <option>Gambia</option>
                                                      <option>Georgia</option>
                                                      <option>Germany</option>
                                                      <option>Ghana</option>
                                                      <option>Gibraltar</option>
                                                      <option>Greece</option>
                                                      <option>Greenland</option>
                                                      <option>Grenada</option>
                                                      <option>Guadeloupe</option>
                                                      <option>Guam</option>
                                                      <option>Guatemala</option>
                                                      <option>Guinea</option>
                                                      <option>Guinea-Bissau</option>
                                                      <option>Guyana</option>
                                                      <option>Haiti</option>
                                                      <option>Heard Island and McDonald Islands</option>
                                                      <option>Honduras</option>
                                                      <option>Hong Kong</option>
                                                      <option>Hungary</option>
                                                      <option>Iceland</option>
                                                      <option>India</option>
                                                      <option>Indonesia</option>
                                                      <option>Islamic Republic of Iran</option>
                                                      <option>Iraq</option>
                                                      <option>Ireland</option>
                                                      <option>Israel</option>
                                                      <option>Italy</option>
                                                      <option>Jamaica</option>
                                                      <option>Japan</option>
                                                      <option>Jordan</option>
                                                      <option>Kazakhstan</option>
                                                      <option>Kenya</option>
                                                      <option>Kiribati</option>
                                                      <option>Korea, Democratic People's Republic of</option>
                                                      <option>Korea, Republic of</option>
                                                      <option>Kuwait</option>
                                                      <option>Kyrgyzstan</option>
                                                      <option>Lao People's Democratic Republic</option>
                                                      <option>Latvia</option>
                                                      <option>Lebanon</option>
                                                      <option>Lesotho</option>
                                                      <option>Liberia</option>
                                                      <option>Libyan Arab Jamahiriya</option>
                                                      <option>Liechtenstein</option>
                                                      <option>Lithuania</option>
                                                      <option>Luxembourg</option>
                                                      <option>Macau</option>
                                                      <option>Madagascar</option>
                                                      <option>Malawi</option>
                                                      <option>Malaysia</option>
                                                      <option>Maldives</option>
                                                      <option>Mali</option>
                                                      <option>Malta</option>
                                                      <option>Marshall Islands</option>
                                                      <option>Martinique</option>
                                                      <option>Mauritania</option>
                                                      <option>Mauritius</option>
                                                      <option>Mayotte</option>
                                                      <option>Mexico</option>
                                                      <option>Micronesia</option>
                                                      <option>Moldova, Republic of</option>
                                                      <option>Monaco</option>
                                                      <option>Mongolia</option>
                                                      <option>Monserrat</option>
                                                      <option>Morocco</option>
                                                      <option>Mozambique</option>
                                                      <option>Myanmar</option>
                                                      <option>Nambia</option>
                                                      <option>Nauru</option>
                                                      <option>Nepal</option>
                                                      <option>Netherlands</option>
                                                      <option>Netherlands Antilles</option>
                                                      <option>New Caledonia</option>
                                                      <option>New Zealand</option>
                                                      <option>Nicaragua</option>
                                                      <option>Niger</option>
                                                      <option>Nigeria</option>
                                                      <option>Niue</option>
                                                      <option>Norfolk Island</option>
                                                      <option>Northern Mariana Islands</option>
                                                      <option>Norway</option>
                                                      <option>Oman</option>
                                                      <option>Pakistan</option>
                                                      <option>Palau</option>
                                                      <option>Panama</option>
                                                      <option>Papua New Guinea</option>
                                                      <option>Paraguay</option>
                                                      <option>Peru</option>
                                                      <option>Philippines</option>
                                                      <option>Pitcairn</option>
                                                      <option>Poland</option>
                                                      <option>Portugal</option>
                                                      <option>Puerto Rico</option>
                                                      <option>Qatar</option>
                                                      <option>Romania</option>
                                                      <option>Russian Federation</option>
                                                      <option>Rwanda</option>
                                                      <option>S. Georgia & the S. Sandwich Islands</option>
                                                      <option>Saint Lucia</option>
                                                      <option>Samoa</option>
                                                      <option>San Marino</option>
                                                      <option>Sao Tome and Principe</option>
                                                      <option>Saudi Arabia</option>
                                                      <option>Senegal</option>
                                                      <option>Seychelles</option>
                                                      <option>Sierra Leone</option>
                                                      <option>Slovakia</option>
                                                      <option>Slovenia</option>
                                                      <option>Solomon Islands</option>
                                                      <option>Somalia</option>
                                                      <option>South Africa</option>
                                                      <option>Spain</option>
                                                      <option>Sri Lanka</option>
                                                      <option>St. Helena</option>
                                                      <option>St. Kitts and Nevis</option>
                                                      <option>St. Pierre and Miquelon</option>
                                                      <option>St. Vincent and the Grenadines</option>
                                                      <option>Sudan</option>
                                                      <option>Suriname</option>
                                                      <option>Svalbard and Jan Mayen Islands</option>
                                                      <option>Swaziland</option>
                                                      <option>Sweden</option>
                                                      <option>Switzerland</option>
                                                      <option>Syrian Arab Republic</option>
                                                      <option>Taiwan, Province of China</option>
                                                      <option>Tajikistan</option>
                                                      <option>Thailand</option>
                                                      <option>Togo</option>
                                                      <option>Tokelau</option>
                                                      <option>Tonga</option>
                                                      <option>Trinidad and Tobago</option>
                                                      <option>Tunisia</option>
                                                      <option>Turkey</option>
                                                      <option>Turkmenistan</option>
                                                      <option>Turks and Caicos Islands</option>
                                                      <option>Tuvalu</option>
                                                      <option>Uganda</option>
                                                      <option>Ukraine</option>
                                                      <option>United Arab Emirates</option>
                                                      <option>United Kingdom (Great Britain)</option>
                                                      <option>United Republic of Tanzania</option>                                                      
                                                      <option>United States of America</option>
                                                      <option>United States Minor Outlying Islands</option>
                                                      <option>United States Virgin Islands</option>
                                                      <option>Uruguay</option>
                                                      <option>Uzbekistan</option>
                                                      <option>Vanuatu</option>
                                                      <option>Vatican City State (Holy See)</option>
                                                      <option>Venezuela</option>
                                                      <option>Viet Nam</option>
                                                      <option>Wallis and Futuna Islands</option>
                                                      <option>Western Sahara</option>
                                                      <option>Yugoslavia</option>
                                                      <option>Zaire</option>
                                                      <option>Zambia</option>
                                                      <option>Zimbabwe</option>
                                                  </select></div>
                                          </dd>
                                      </div>

                                      <section class="tin-section">

                                        <dd class="form-group">
                                          <p>TIN</p>
                                          <input type="text" class="form-control">
                                        </dd>
                                        <dt>If a TIN is unavailable, please select the appropriate reason below</dt>
                                        <br>
                                        <dd class="form-group">
                                            <div class="newradio">
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                                <label for="optionsRadios1"><span><span></span></span>A: The country where you are liable to pay tax does not issue TINs to its residents.</label>
                                            </div>
                                            <div class="newradio">
                                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                                <label for="optionsRadios2"><span><span></span></span>B: You are otherwise unable to obtain a TIN or equivalent number. (Please provide an explanation for this if you have selected this reason.)</label>
                                                <div class="explanation-wrapper">
                                                    <br>
                                                    <p>Explanation:</p>
                                                    <textarea class="form-control"></textarea>
                                                    <small>105 characters remaining</small>
                                                </div>
                                            </div>
                                            <div class="newradio">
                                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                                <label for="optionsRadios3"><span><span></span></span>C: No TIN is required. (Only select this reason if the authorities of the country of tax residence do not require the TIN to be disclosed.)</label>
                                            </div>
                                        </dd>

                                        <dt>You have not listed the country of residential / mailing address as the country / one of the countries where you have a tax obligation. Please provide a reason for this:</dt>
                                        <dd class="form-group">
                                          <textarea class="form-control"></textarea>
                                          <small>105 characters remaining</small>
                                        </dd>
                                    </section>
                                  </dl>
                                </fieldset>
                                <fieldset>
                                    <legend>Declaration</legend>
                                    <p>By clicking &lsquo;Continue&rsquo; below, I acknowledge that the information contained in this form and information
                                    regarding me and any Reportable Account(s) may be provided to the tax authorities of the country in which
                                    this account(s) is / are maintained and exchanged with tax authorities of another country or countries in
                                    which I may be tax resident pursuant to intergovernmental agreements to exchange financial account
                                    information.</p>
                                    <p>I undertake to advise HSBC within 30 days of any change in circumstances which affects the tax residency
                                    status of me identified in this form or causes the information contained herein to become incorrect, and to
                                    provide HSBC with a suitably updated Self-Certification Form within 90 days of such change in
                                    circumstances.</p>
                                </fieldset>
                                <div class="article__leader__main__btnset">
                                    <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step3c/')">Continue</button>
                                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application-step3a/')">Back</button>
                                </div>

                              </div><!-- /.item -->
                          </div>

                        </form>
                    </div>
                </div><!-- /.row -->
            </div><!--/ container -->
        </section>

    </article>


    <div class="modal fade" id="onlineinvestormodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
                </div>
                <div class="modal-body">
                    <p>Based on your response(s) we are unable to proceed with your application.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red">Back</button>
                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">
    $(document).ready(function(){
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);
        $('.tin-section').hide();
        $('.explanation-wrapper').hide();
    });

    $('select.tax-country-dropdown').change(function(){

        var hasSgSelected = false;
        $("select.tax-country-dropdown").each(function (i, obj) {
            if($(obj).val() === "Singapore"){
                hasSgSelected = true;
            }
        });

        if(hasSgSelected){
            $('.tin-section').hide();
        }else{
            $('.tin-section').show();
        }
    });

    $('input:radio[name="optionsRadios"]').change(
        function(){
            if ($(this).val() == 'option2') {
                $('.explanation-wrapper').show();
            }
            else {
                $('.explanation-wrapper').hide();
            }
        });

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }

    var numTaxCountryDropdowns = 2;
</script>
