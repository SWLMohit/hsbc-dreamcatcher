<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

  <div class="breadcrumbs">
    <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
      <a href="#">Home</a>
      <a href="#">Our Plans</a>
      <a href="#">HSBC Insurance OnlineInvestor</a>
      <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
    </nav>
  </div>

  <article>
    <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
      <div class="masthead__container container">
      </div>
    </section>

    <section class="onlineinvestor">
      <div class="container">
        <h1>HSBC Insurance OnlineInvestor application</h1>


        <div class="onlineinvestor__breadcrumb">
          <a href="#" class="current">Eligibility Check</a>
          <a href="#" class="disabled">Your Plan</a>
          <a href="#" class="disabled">Your details</a>
          <a href="#" class="disabled">Payment</a>
        </div>


        <div class="row">
          <div class="col-md-8">
            <form>
              <div class="onlineinvestor_carousel">
                <div class="form-item item1 active">
                  <fieldset>
                    <legend>Tell us more about yourself before proceeding with your application</legend>
                    <p>
                      The products and its related features offered on this online platform are available only to customers who are currently residing
                      <button type="button" class="help-tooltip" tabindex="0" data-container="body" 
                              data-toggle="popover" data-placement="top" label="help" 
                              aria-label="Residing in Singapore means anyone who is physically present in Singapore for 183 days or more during the year."
                              data-content="Residing in Singapore means anyone who is physically present in Singapore for 183 days or more during the year.">
  <i class="icon icon-circle-help-solid"></i></button>
                      in Singapore.
                    </p>
                    <dl class="onlineinvestor__questionnaire">
                      <dt>What is your current status of residency in Singapore?</dt>
                      <dd class="form-group">
                        <div class="form-select_wrap"><select class="form-control control_residency_select">
                          <option value="singaporean">Singaporean</option>
                          <option value="singaporean_pr">Singapore PR</option>
                          <option>Employment Pass</option>
                          <option>Skilled Pass</option>
                          <option>Personalised Employment Pass</option>
                          <option>Dependant's Pass</option>
                          <option>Student Pass</option>
                          <option value="none_of_above">None of the above</option>
                        </select>
                        </div>
                      </dd>

                      <dt class="control_singapore">Surname <small>(as appears on NRIC/Passport)</small></dt>
                      <dd class="form-group control_singapore">
                        <input class="form-control" type="text">
                      </dd>

                      <dt class="control_singapore">Given name <small>(as appears on NRIC/Passport)</small></dt>
                      <dd class="form-group control_singapore">
                        <input class="form-control" type="text">
                      </dd>

                      <dt>Date of birth</dt>
                      <dd class="form-group">
                        <div class="form-group__multi form-group__multi--date">
                                        <span>
                                          <input type="text" class="form-control date-day" placeholder="DD" onkeydown="validate(event)" onchange="datecheck(event)">
                                          <%--<div class="form-select_wrap">--%>
                                            <%--<select class="form-control">--%>
                                            <%--<option selected disabled>DD</option>--%>
                                            <%--<option>01</option>--%>
                                            <%--<option>02</option>--%>
                                            <%--<option>03</option>--%>
                                            <%--<option>04</option>--%>
                                            <%--<option>05</option>--%>
                                            <%--<option>06</option>--%>
                                            <%--<option>07</option>--%>
                                            <%--<option>08</option>--%>
                                            <%--<option>09</option>--%>
                                            <%--<option>10</option>--%>
                                            <%--<option>11</option>--%>
                                            <%--<option>12</option>--%>
                                            <%--<option>13</option>--%>
                                            <%--<option>14</option>--%>
                                            <%--<option>15</option>--%>
                                            <%--<option>16</option>--%>
                                            <%--<option>17</option>--%>
                                            <%--<option>18</option>--%>
                                            <%--<option>19</option>--%>
                                            <%--<option>20</option>--%>
                                            <%--<option>21</option>--%>
                                            <%--<option>22</option>--%>
                                            <%--<option>23</option>--%>
                                            <%--<option>24</option>--%>
                                            <%--<option>25</option>--%>
                                            <%--<option>26</option>--%>
                                            <%--<option>27</option>--%>
                                            <%--<option>28</option>--%>
                                            <%--<option>29</option>--%>
                                            <%--<option>30</option>--%>
                                            <%--<option>31</option>--%>
                                          <%--</select>--%>
                                          <%--</div>--%>
                                        </span>
                          <span>
                                          <input type="text" class="form-control date-month" placeholder="MM" onkeydown="validate(event)" onchange="datecheck(event)">
                                          <%--<div class="form-select_wrap">--%>
                                            <%--<select class="form-control">--%>
                                            <%--<option selected disabled>MM</option>--%>
                                            <%--<option>01</option>--%>
                                            <%--<option>02</option>--%>
                                            <%--<option>03</option>--%>
                                            <%--<option>04</option>--%>
                                            <%--<option>05</option>--%>
                                            <%--<option>06</option>--%>
                                            <%--<option>07</option>--%>
                                            <%--<option>08</option>--%>
                                            <%--<option>09</option>--%>
                                            <%--<option>10</option>--%>
                                            <%--<option>11</option>--%>
                                            <%--<option>12</option>--%>
                                          <%--</select>--%>
                                          <%--</div>--%>
                                        </span>
                          <span>
                                          <input type="text" class="form-control date-year" placeholder="YYYY" onkeydown="validate(event)" onchange="datecheck(event)">
                                          <%--<div class="form-select_wrap">--%>
                                            <%--<select class="form-control">--%>
                                            <%--<option selected disabled>YYYY</option>--%>
                                            <%--<option>1998</option>--%>
                                            <%--<option>1997</option>--%>
                                            <%--<option>1996</option>--%>
                                            <%--<option>1995</option>--%>
                                            <%--<option>1994</option>--%>
                                            <%--<option>1993</option>--%>
                                            <%--<option>1992</option>--%>
                                            <%--<option>1991</option>--%>
                                            <%--<option>1990</option>--%>
                                            <%--<option>1989</option>--%>
                                            <%--<option>1988</option>--%>
                                            <%--<option>1987</option>--%>
                                            <%--<option>1986</option>--%>
                                            <%--<option>1985</option>--%>
                                            <%--<option>1984</option>--%>
                                            <%--<option>1983</option>--%>
                                            <%--<option>1982</option>--%>
                                            <%--<option>1981</option>--%>
                                            <%--<option>1980</option>--%>
                                            <%--<option>1979</option>--%>
                                            <%--<option>1978</option>--%>
                                            <%--<option>1977</option>--%>
                                            <%--<option>1976</option>--%>
                                            <%--<option>1975</option>--%>
                                            <%--<option>1974</option>--%>
                                            <%--<option>1973</option>--%>
                                            <%--<option>1972</option>--%>
                                            <%--<option>1971</option>--%>
                                            <%--<option>1970</option>--%>
                                            <%--<option>1969</option>--%>
                                            <%--<option>1968</option>--%>
                                            <%--<option>1967</option>--%>
                                            <%--<option>1966</option>--%>
                                            <%--<option>1965</option>--%>
                                            <%--<option>1964</option>--%>
                                            <%--<option>1963</option>--%>
                                            <%--<option>1962</option>--%>
                                            <%--<option>1961</option>--%>
                                            <%--<option>1960</option>--%>
                                            <%--<option>1959</option>--%>
                                            <%--<option>1958</option>--%>
                                            <%--<option>1957</option>--%>
                                            <%--<option>1956</option>--%>
                                            <%--<option>1955</option>--%>
                                          <%--</select>--%>
                                          <%--</div>--%>
                                        </span>
                        </div>
                        <div class="date-error has-error">
                          <span class="help-block">Invalid date</span>
                        </div>
                      </dd>

                      <dt>Gender</dt>
                      <dd class="form-group">
                        <div class="newradio--inline">
                          <input id="id_gender_option1" type="radio" name="gender" value="option1">
                          <label for="id_gender_option1"><span><span></span></span>Male</label>
                        </div>
                        <div class="newradio--inline">
                          <input id="id_gender_option2" type="radio" name="gender" value="option2">
                          <label for="id_gender_option2"><span><span></span></span>Female</label>
                        </div>
                      </dd>

                      <dt>Mobile number</dt>
                      <dd class="form-group">
                        <div class="form-group__multi form-group__multi--phone">
                          <span><input type="text" class="form-control" placeholder="65"></span>
                          <span><input type="text" class="form-control" placeholder=""></span>
                        </div>
                      </dd>

                      <dt>Email address</dt>
                      <dd class="form-group">
                        <input type="email" class="form-control">
                      </dd>
                    </dl>
                  </fieldset>
                  <div class="article__leader__main__btnset">
                    <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step1b/')">Continue</button>
                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application/')">Back</button>
                  </div>
                </div><!-- /.item -->

              </div>
            </form>
          </div>
        </div><!-- /.row -->
      </div><!--/ container -->
    </section>

  </article>


  <div class="modal fade" id="onlineinvestormodal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                  onclick="openPage('../onlineinvestor-application/')">
            <span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
        </div>
        <div class="modal-body">
          <p>Based on your response(s) we are unable to proceed with your application.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red" data-dismiss="modal"
                  onclick="openPage('../onlineinvestor-application/')">Back</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="onlineinvestormodal_child">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                  onclick="openPage('../onlineinvestor-application/')">
            <span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
        </div>
        <div class="modal-body">
          <p>Your child or Legal ward has to be 18 years old and below in order to for you to continue with this application form.</p>
          <p>Your child, however, can purchase this product in his/her own name.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red" data-dismiss="modal"
                  onclick="openPage('../onlineinvestor-application/')">Back</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="onlineinvestormodal_warning">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title" id="myModalLabel">Warning!</h2>
        </div>
        <div class="modal-body">
          <p>Your child or Legal ward has to be 17 years old and below in order to for you to continue with this application form.</p>
          <p>Your child, however, can purchase this product in his/her own name.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-red" data-dismiss="modal">Continue</button>
        </div>
      </div>
    </div>
  </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">

  $(document).ready(function(){
      $('html, body').stop().animate({
          'scrollTop': $(".onlineinvestor_carousel").offset().top -15
      }, 700);
      $('.date-error').hide();
  });

  function validate(e){
      // Allow: backspace, delete, tab, escape and enter
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
          // Allow: Ctrl/cmd+A
          (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
          // Allow: Ctrl/cmd+C
          (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
          // Allow: Ctrl/cmd+X
          (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
          // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
          // let it happen, don't do anything
          return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }

  }

  function datecheck(event){

      item = event.currentTarget;

      var year = $('.date-year').val();
      var month = $('.date-month').val();
      var day = $('.date-day').val();

      if( !isNaN(parseInt(year)) && !isNaN(parseInt(month)) && !isNaN(parseInt(day))) {

          var timestamp = Date.parse(year + '-' + month + '-' + day);

          if (isNaN(timestamp)) {
              $('input[class*="date-"]').parent('span').addClass("has-error");
              $('.date-error').show();
          } else {
              //date is valid, remove all error indicators
              $('input[class*="date-"]').parent('span').removeClass("has-error");
              $('.date-error').hide();

              //only pad month and day
              $('.date-month').val(pad(month,2));
              $('.date-day').val(pad(day,2));
          }

          var currDate = new Date();
          if(parseInt(year) == currDate.getFullYear()-19) { //1998 @ 2017
              if (parseInt(month) >= currDate.getMonth() + 1){
                  if(parseInt(day) > currDate.getDate()){
                      //do not continue with form
                      $('#onlineinvestormodal').modal({show: false});
                      $('#onlineinvestormodal').modal('show');
                  }
              }
          }else if(parseInt(year) > currDate.getFullYear()-19){ //1999 and above @ 2017
              //do not continue with form
              $('#onlineinvestormodal').modal({show: false});
              $('#onlineinvestormodal').modal('show');
          }
      }

  }

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }

    $("select.control_residency_select").change(function() {
        if ($(this).val() == 'none_of_above') {
            $("#onlineinvestormodal").modal("show");
        }
        if ($(this).val() == 'singaporean' || $(this).val() == 'singaporean_pr') {
            $(".control_singapore").show();
            $(".control_passport_label").html('NRIC');
        } else {
            $(".control_singapore").hide();
            $(".control_passport_label").html('Passport number');
        }
    });

    $('select.control_country_select').on('change', function() {
        if ($(this).val() == 'none_of_above') {
            $("#onlineinvestormodal").modal("show");
        }
    });

    $("#select_buy_for").change(function () {
        $(".control_employment").hide();
        $("#label_buy_for").html($(this).val());
        if ($(this).val() == 'your child' || $(this).val() == 'your legal ward') {
            $("#onlineinvestormodal_warning").modal("show");
            $(".control_option_child").show();
        } else {
            $(".control_option_child").hide();
        }
    });

    $("select#control_buy_for_year").change(function () {
        var buyFor = $("#select_buy_for").val();
        if (buyFor == 'your child' || buyFor == 'your legal ward') {
            if ($(this).val() < '1999') {
                $("#onlineinvestormodal_child").modal("show");
            }
        }
    });

    $("select.control_employment_select").change(function() {
        if ($(this).val() == 'student') {
            $(".control_study").show();
        } else {
            $(".control_study").hide();
        }
        if ($(this).val() == 'student' || $(this).val() == 'homemaker' || $(this).val() == 'retired' || $(this).val() == 'unemployed') {
            $(".control_occupation").hide();
            $(".control_industry").hide();
            $(".control_company").hide();
            $(".control_employment_country").hide();
            $(".control_employment_city").hide();
            $("input.control_salary").val("0");
        } else {
            $(".control_occupation").show();
            $(".control_industry").show();
            $(".control_company").show();
            $(".control_employment_country").show();
            $(".control_employment_city").show();
            $("input.control_salary").val("");
        }
        if ($(this).val() == 'self-employed') {
            $(".control_company_label").html("Company");
        }
        if ($(this).val() == 'salaried') {
            $(".control_company_label").html("Employer")
        }
    })
</script>
