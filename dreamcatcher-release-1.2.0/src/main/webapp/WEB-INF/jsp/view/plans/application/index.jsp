<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our Plans</a>
            <a href="#">HSBC Insurance OnlineInvestor</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
            <div class="masthead__container container">
            </div>
        </section>

        <section class="onlineinvestor">

          <div class="container">
            <div class="carousel slide onlineinvestor_carousel" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <h1>HSBC Insurance OnlineInvestor application</h1>
                  <h3>Personal Data Protection Act (PDPA)</h3>
                  <p>
                    Your privacy is important to us. Our Data Protection Policy outlines
                    how your personal data will be managed in accordance with the Personal
                    Data Protection Act ("PDPA") which strives to protect personal data of
                    individuals. Our Data Protection Policy (and any updates to the same) is
                    published at
                    <a style="text-decoration: underline;" href="http://www.HSBCinsuranceonline.com/privacy-policy">
                    http://www.HSBCinsuranceonline.com/privacy-policy</a>. Subject to your
                    rights at law, the prevailing terms of our Data Protection Policy shall apply.
                  </p>
                  <div class="article__leader__main__btnset">
                      <button type="button" class="btn btn-red" onclick="nextStep(1)">I agree</button>
                      <button type="button" class="btn btn-white" data-toggle="modal" data-target="#onlineinvestormodal">I disagree</button>
                  </div>
                </div><!-- /.item -->
                <div class="item">
                  <h1>HSBC Insurance OnlineInvestor application</h1>
                  <h3>Very Important Notes</h3>
                    </p>
                        Investment-linked policies are not available to US citizens, US Residents, green card holders and US
                        Corporations/partnerships.
                    </p>

                    <p>
                        Due to US insurance regulatory requirements, you are not to enter the US or any territory
                        subject to US jurisdiction during the application process, otherwise the request effected
                        hereunder may be made void.
                    </p>
                    <p>
                        This is a self-directed online platform to purchase insurance products. HSBC Insurance
                        (Singapore) Pte. Limited does not provide any advice to clients transacting on this platform.
                    </p>
                    <p>
                        You agree that the information provided in this online application will form the basis of the
                        contract of insurance between you and HSBC Insurance (Singapore) Pte. Limited.
                    </p>
                    <p>
                        Please note, we are required under the Insurance Act to inform you that you must fully and
                        faithfully disclose all the facts which you know or ought to know (including any existing
                        medical conditions). Otherwise, in accordance with Section 25(5) of the Insurance Act (Cap.
                        142), your policy maybe cancelled and you may receive no benefits.
                    </p>

                    <div class="article__leader__main__btnset">
                      <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step1a/')">I agree</button>
                      <button type="button" class="btn btn-white" data-toggle="modal" data-target="#onlineinvestormodal">I disagree</button>
                  </div>

                </div><!-- /.item -->
              </div>
            </div>

          </div><!--/ container -->

        </section>
    </article>

    <div class="modal fade" id="onlineinvestormodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            onclick="openPage('../onlineinvestor-application/')">
                        <span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
                </div>
                <div class="modal-body">
                    <p>Based on your response(s) we are unable to proceed with your application.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red" data-dismiss="modal"
                            onclick="openPage('../onlineinvestor-application/')">Back</button>
                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $('.onlineinvestor_carousel').carousel({
      interval: false
    });
    function nextStep(step) {
      $('.onlineinvestor_carousel').carousel(step);
      $('html, body').stop().animate({
          'scrollTop': $(".onlineinvestor_carousel").offset().top -15
      }, 700);
    };

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }
</script>
