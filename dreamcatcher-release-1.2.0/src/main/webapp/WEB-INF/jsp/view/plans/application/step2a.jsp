<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our Plans</a>
            <a href="#">HSBC Insurance OnlineInvestor</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
            <div class="masthead__container container">
            </div>
        </section>

        <section class="onlineinvestor">
            <div class="container">
                <h1>HSBC Insurance OnlineInvestor application</h1>


                <div class="onlineinvestor__breadcrumb">
                    <a href="../onlineinvestor-application-step1a/">Eligibility Check</a>
                    <a href="#" class="current">Your plan</a>
                    <a href="#" class="disabled">Your details</a>
                    <a href="#" class="disabled">Payment</a>
                </div>


                <div class="row">
                    <div class="col-md-8">
                        <form>
                          <div class="onlineinvestor_carousel">
                              <div class="form-item item1 active">
                                <fieldset>
                                    <legend>Help us to understand your investment needs</legend>
                                    <p>
                                      Please select a risk profile that you identify with most, so as to determine the acceptable risk level:
                                    </p>

                                    <div class="form-group">
                                      <div class="newradio">
                                        <input id="optionRadio1" type="radio" name="optionsRadios" value="1" onclick="selectRadio(1)" data-show="#radio_hint_1" data-hide=".radio_hint">
                                        <label for="optionRadio1">
                                          <span><span></span></span> Very Cautious
                                        </label>
                                          <div id="radio_hint_1" class="radio_hint hide">
                                              <p>You are a <strong>Very Cautious</strong> investor if:</p>
                                              <ul>
                                                  <li>
                                                      You are generally comfortable with achieving minimal level of return potential on your investment coupled with minimal risks.
                                                  </li>
                                                  <li>
                                                      You are comfortable with minimal fluctuations in your invested amount
                                                      which is expected in the normal market conditions.
                                                      Capital values of products that is potentially suitable for you
                                                      can fluctuate and may fall below your original investment. In normal
                                                      market conditions fluctuation is expected to be minimal (although
                                                      this is not guaranteed), and you are comfortable with this level of
                                                      fluctuation.
                                                  </li>
                                                  <li>
                                                      You are not comfortable if the returns of your investment may fall below your original investment.
                                                  </li>
                                              </ul>
                                              <p>You may consider funds with risk rating 1.</p>
                                          </div>
                                      </div>
                                      <div class="newradio">
                                        <input id="optionRadio2" type="radio" name="optionsRadios" value="2" onclick="selectRadio(2)" data-show="#radio_hint_2" data-hide=".radio_hint">
                                        <label for="optionRadio2">
                                          <span><span></span></span> Cautious
                                        </label>
                                          <div id="radio_hint_2" class="radio_hint hide">
                                              <p>You are a <strong>Cautious</strong> investor if:</p>
                                              <ul>
                                                  <li>
                                                      You are generally comfortable with achieving a potentially low level of
                                                      return on your investment coupled with a low level of risk.
                                                  </li>
                                                  <li>
                                                      You are comfortable if the returns of your investment may fall below your
                                                      original investment.  Capital values of the product that is potentially
                                                      suitable for you can fluctuate and may fall below your original investment.
                                                      In normal market conditions fluctuation is expected to be low (although this
                                                      is not guaranteed), and you are comfortable with this level of fluctuation.
                                                  </li>
                                              </ul>
                                              <p>You may consider funds with risk rating 2 or below.</p>
                                          </div>

                                      </div>
                                      <div class="newradio">
                                        <input id="optionRadio3" type="radio" name="optionsRadios" value="3" onclick="selectRadio(3)" data-show="#radio_hint_3" data-hide=".radio_hint">
                                        <label for="optionRadio3">
                                          <span><span></span></span> Balanced
                                        </label>
                                          <div id="radio_hint_3" class="radio_hint hide">
                                              <p>You are a <strong>Balanced</strong> investor if:</p>
                                              <ul>
                                                  <li>
                                                      You are generally comfortable with achieving potentially moderate level of return on your
                                                      investment coupled with a moderate level of risk.
                                                  </li>
                                                  <li>
                                                      You are comfortable with fluctuations in your invested amount.  Capital values of the product can
                                                      fluctuate and may fall below your original investment. Fluctuation is expected to be higher
                                                      than products that are suitable for investors in lower risk tolerance categories, but now as
                                                      much as for higher risk tolerance categories.
                                                  </li>
                                                  <li>
                                                      You are comfortable if the returns of your investment may fall below your original investment.
                                                  </li>
                                              </ul>
                                              <p>You may consider funds with risk rating 3 or below.</p>
                                          </div>
                                      </div>
                                      <div class="newradio">
                                        <input id="optionRadio4" type="radio" name="optionsRadios"value="4" onclick="selectRadio(4)" data-show="#radio_hint_4" data-hide=".radio_hint">
                                        <label for="optionRadio4">
                                          <span><span></span></span> Adventurous
                                        </label>
                                          <div id="radio_hint_4" class="radio_hint hide">
                                              <p>You are a <strong>Adventurous</strong> investor if:</p>
                                              <ul>
                                                  <li>
                                                      You are generally comfortable with achieving
                                                      potentially high level of return on your investment
                                                      coupled with a high level of risk.
                                                  </li>
                                                  <li>
                                                      You are comfortable with significant fluctuations in your invested amount
                                                      and understand the risk / reward equation.  Capital values of the product
                                                      can fluctuate and may fall below your original investment. Fluctuation is
                                                      expected to be higher than products that are suitable for investors in lower
                                                      risk tolerance categories, but not as much as for higher risk tolerance
                                                      categories.
                                                  </li>
                                                  <li>You are comfortable if the returns of your investment may fall substantially
                                                      below your original investment.</li>
                                              </ul>
                                              <p>You may consider funds with risk rating 4 or below.</p>
                                          </div>
                                      </div>
                                      <div class="newradio">
                                        <input id="optionRadio5" type="radio" name="optionsRadios" value="5" onclick="selectRadio(5)" data-show="#radio_hint_5" data-hide=".radio_hint">
                                        <label for="optionRadio5">
                                          <span><span></span></span> Speculative
                                        </label>
                                          <div id="radio_hint_5" class="radio_hint hide">
                                              <p>You are a <strong>Speculative</strong> investor if:</p>
                                              <ul>
                                                  <li>
                                                      you are generally comfortable with maximizing your level of return on your investment coupled with maximized risk.
                                                  </li>
                                                  <li>
                                                      You are comfortable with significant wide fluctuations in your invested amount and understand the risk / reward equation.
                                                      Capital values of the product can fluctuate widely and may fall substantially below your original investment. You understand the risk/reward equation, and are comfortable with this level of fluctuation.
                                                  </li>
                                                  <li>You are comfortable if the returns of your investment may fall substantially below your original investment.</li>
                                              </ul>
                                              <p>You may consider funds with risk rating 5 or below.</p>
                                          </div>
                                      </div>
                                    </div>
                                </fieldset>
                                <div class="article__leader__main__btnset">
                                    <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step2b/')">Continue</button>
                                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application-step1a/')">Back</button>
                                </div>
                              </div><!-- /.item -->
                          </div>
                        </form>
                    </div>
                </div><!-- /.row -->



            </div><!--/ container -->
        </section>

    </article>


    <div class="modal fade" id="onlineinvestormodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
                </div>
                <div class="modal-body">
                    <p>Based on your response(s) we are unable to proceed with your application.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red">Back</button>
                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">
    $(document).ready(function(){
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);
    });

    $(document).on("click", ".confirm_investment a", function(e){
      $('html, body').stop().animate({
          'scrollTop': $(this).parents(".confirm_investment").offset().top -15
      }, 700);
    });


    $(document).ready(function() {
        deselectAllRadio();
        $("radio_range").show();
        $(".totalbox").value = '0';
    });

    function deselectAllRadio() {
        $(".radio_range .radio_range__item").removeClass("selected");
    }

    function selectRadio(num){
        deselectAllRadio();
        $("#optionsRadios" + num).addClass("selected");
    }

    $(document).on("click", "[data-show]", function(e){
        $($(this).data("hide")).addClass("hide");
        $($(this).data("show")).removeClass("hide");
    });

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }

    $("[name=optionsRadios]").change(function (e) {
        sessionStorage.riskLevel = $(this).val();
        delete sessionStorage.selectedFunds;
    });
</script>
