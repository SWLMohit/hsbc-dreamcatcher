<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our Plans</a>
            <a href="#">HSBC Insurance OnlineInvestor</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
            <div class="masthead__container container">
            </div>
        </section>

        <section class="onlineinvestor">
            <div class="container">
                <h1>HSBC Insurance OnlineInvestor application</h1>


                <div class="onlineinvestor__breadcrumb">
                    <a href="../onlineinvestor-application-step1a/">Eligibility Check</a>
                    <a href="#" class="current">Your plan</a>
                    <a href="#" class="disabled">Your details</a>
                    <a href="#" class="disabled">Payment</a>
                </div>


                <div class="row">
                    <div class="col-md-8">
                        <form>
                          <div class="onlineinvestor_carousel">
                              <div class="form-item item4 active">
                                <fieldset>
                                    <legend>Additional details</legend>
                                    <p><strong>Notice to Client </strong></p>
                                    <p>In proceeding to make this purchase you are confirming that this product is an appropriate solution for your needs.</p>

                                    <p><strong>Acknowledgement by Client </strong></p>
                                    <ul>
                                        <li>
                                            I understand that I am purchasing HSBC Insurance OnlineInvestor plan without seeking advice from any        Financial Advisory representative.
                                        </li>
                                        <li>
                                            I understand that the following resources are available for my assessment before I determine if this Insurance policy meets my financial circumstances and needs:
                                            <ol style="list-style: lower-alpha;">
                                                <li>
                                                    Insurance Estimator: https://www.cpf.gov.sg/eSvc/Web/Schemes/InsuranceEstimator/InsuranceEstimator
                                                    to calculate the amount of life insurance coverage I would need.
                                                </li>
                                                <li>
                                                    Budget Calculator: http://www.moneysense.gov.sg/financial-planning/financial-calculators/budget-	calculator.aspx to check if the premium payable is affordable based on my current income and expenditure.
                                                </li>
                                                <li>
                                                    Comparefirst: http://www.comparefirst.sg to compare the features and premiums of Direct Purchase 	Insurance (DPI) and other types of life policies.
                                                </li>
                                                <li>
                                                    Considered the different types of DPI and other types of life insurance products that are available and 	whether the insurance policy that I intend to purchase is suitable for my financial circumstances and needs.
                                                </li>
                                            </ol>
                                        </li>
                                        <li>
                                            I have read and understood the information in the Product Summary, General Provision and Benefit Illustration.
                                        </li>
                                        <li>
                                            I understand the nature and objective of the products, the benefits, the fees and charges, the exclusions, and
                                            the risks associated with an Investment-linked Policy (ILP).
                                        </li>
                                        <li>
                                            I have read and understood the Product Highlights Sheets and Fund Summaries of the ILP sub-fund(s) of my choice.
                                        </li>
                                        <li>
                                            I understand my premium obligation over the policy term, and that I may lose my protection cover if I do not make payment when due.
                                        </li>
                                        <li>
                                            I acknowledge and understand the following terms and conditions:
                                            <ol style="list-style: lower-alpha;">
                                                <li>
                                                    I have an investment horizon of at least 5 years and confirm that I would like to seek to save for a 10-year  	period or longer to better manage the peaks and troughs of the investment market.
                                                </li>
                                                <li>
                                                    I understand that there is a S$300 early withdrawal fee for each partial withdrawal or full surrender in the 	first four policy years.
                                                </li>
                                                <li>
                                                    I am aware of the two-year rule where I am unable to change premiums in the first two policy years or take
                                                    a premium holiday in that period.
                                                </li>
                                                <li>
                                                    I acknowledge meeting the Customer Knowledge Assessment (CKA) criteria as assessed on DDMMYY.
                                                </li>
                                            </ol>
                                        </li>
                                    </ul>

                                    <aside class="product_reference_materials">
                                        <ul>
                                            <li><a href="#">Customer Knowledge Assessment (EN, PDF)</a></li>
                                        </ul>
                                    </aside>
                                    <div class="onlineinvestor__table__wrap">
                                      <table class="onlineinvestor__table hide">
                                        <thead>
                                        <tr>
                                            <th><strong>Product and Fund </strong></th>
                                            <th colspan="5"><strong>Resources</strong></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="green">
                                            <th></th>
                                            <th>Benefit Illustration</th>
                                            <th>Product Summary and General Provisions</th>
                                            <th>Fund Factsheet</th>
                                            <th>Fund Summary</th>
                                            <th>Product Highlights Sheet</th>
                                        </tr>
                                        <tr class="gray10">
                                            <td>Product Information</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>HSBC Insurance Global High Income Bond Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <tr class="gray10">
                                            <td>HSBC Insurance Global Multi-Asset Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>HSBC Insurance Global Emerging Markets Bond Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <tr class="gray10">
                                            <td>HSBC Insurance World Selection 3 Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>HSBC Insurance Europe Dynamic Equity Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <tr class="gray10">
                                            <td>HSBC Insurance Singapore Equity Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>HSBC Insurance US Equity Index Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <tr class="gray10">
                                            <td>HSBC Insurance Pacific Equity Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>HSBC Insurance Global Equity Index Fund</td>
                                            <td>&mdash;</td>
                                            <td>&mdash;</td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                            <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                      <p>You will also receive a copy of the PDFs listed in the table above via e-mail upon completion of your application.</p>
                                  </div><!-- /.onlineinvestor__table__wrap -->
                                </fieldset>
                                <div class="article__leader__main__btnset">
                                    <button type="button" class="btn btn-white" onclick="openPage('../onlineinvestor-application/')">I decline</button>
                                    <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step3a/')">I accept</button>
                                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application-step2c/')">Back</button>
                                </div>

                              </div><!-- /.item -->
                          </div>
                        </form>
                    </div>
                </div><!-- /.row -->



            </div><!--/ container -->
        </section>

    </article>


    <div class="modal fade" id="onlineinvestormodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
                </div>
                <div class="modal-body">
                    <p>Based on your response(s) we are unable to proceed with your application.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red">Back</button>
                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">
    $(window).load(function () {
        var selectedFundNames = JSON.parse(sessionStorage.selectedFunds).map(function (item) {
            return item.name
        });

        $(".onlineinvestor__table tbody tr:nth-child(n+3)").filter(function () {
            return selectedFundNames.indexOf($("td:nth-child(1)", this).text()) === -1;
        }).remove();

        // Because we don't have a backend to generate just the table we need, we need to dynamically apply
        // the zebra pattern.
        $(".onlineinvestor__table tbody tr:nth-child(2n)")
            .removeClass("gray5")
            .addClass("gray10");
        $(".onlineinvestor__table tbody tr:nth-child(2n+1)")
            .removeClass("gray10")
            .addClass("gray5");

        $(".onlineinvestor__table").removeClass("hide");
    });

    $(document).ready(function(){
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);
    });

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }
</script>
