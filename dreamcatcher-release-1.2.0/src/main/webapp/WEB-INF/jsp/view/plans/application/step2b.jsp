<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our Plans</a>
            <a href="#">HSBC Insurance OnlineInvestor</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
            <div class="masthead__container container">
            </div>
        </section>

        <section class="onlineinvestor">
            <div class="container">
                <h1>HSBC Insurance OnlineInvestor application</h1>


                <div class="onlineinvestor__breadcrumb">
                    <a href="../onlineinvestor-application-step1a/">Eligibility Check</a>
                    <a href="#" class="current">Your plan</a>
                    <a href="#" class="disabled">Your details</a>
                    <a href="#" class="disabled">Payment</a>
                </div>


                <div class="row">
                    <div class="col-md-8">
                        <form>
                          <div class="onlineinvestor_carousel">
                              <div class="form-item item2 active">
                                <fieldset>
                                  <legend>Fund selection</legend>
                                  <p>
Your risk profile is: <strong class="js-risk-level">2</strong>
                                  </p>
                                    <div class="risk_desc">
                                        <div id="radio_hint_1" class="radio_hint hide">
                                            <p>You are a <strong>Very Cautious</strong> investor if:</p>
                                            <ul>
                                                <li>
                                                    You are generally comfortable with achieving minimal level of return potential on your investment coupled with minimal risks.
                                                </li>
                                                <li>
                                                    You are comfortable with minimal fluctuations in your invested amount
                                                    which is expected in the normal market conditions.
                                                    Capital values of products that is potentially suitable for you
                                                    can fluctuate and may fall below your original investment. In normal
                                                    market conditions fluctuation is expected to be minimal (although
                                                    this is not guaranteed), and you are comfortable with this level of
                                                    fluctuation.
                                                </li>
                                                <li>
                                                    You are not comfortable if the returns of your investment may fall below your original investment.
                                                </li>
                                            </ul>
                                            <p>You may consider funds with risk rating 1.</p>
                                        </div>
                                        <div id="radio_hint_2" class="radio_hint hide">
                                            <p>You are a <strong>Moderate</strong> investor if:</p>
                                            <ul>
                                                <li>
                                                    You are generally comfortable with achieving a potentially low level of
                                                    return on your investment coupled with a low level of risk.
                                                </li>
                                                <li>
                                                    You are comfortable if the returns of your investment may fall below your
                                                    original investment.  Capital values of the product that is potentially
                                                    suitable for you can fluctuate and may fall below your original investment.
                                                    In normal market conditions fluctuation is expected to be low (although this
                                                    is not guaranteed), and you are comfortable with this level of fluctuation.
                                                </li>
                                            </ul>
                                            <p>You may consider funds with risk rating 2 or below.</p>
                                        </div>
                                        <div id="radio_hint_3" class="radio_hint hide">
                                            <p>You are a <strong>Balanced</strong> investor if:</p>
                                            <ul>
                                                <li>
                                                    You are generally comfortable with achieving potentially moderate level of return on your
                                                    investment coupled with a moderate level of risk.
                                                </li>
                                                <li>
                                                    You are comfortable with fluctuations in your invested amount.  Capital values of the product can
                                                    fluctuate and may fall below your original investment. Fluctuation is expected to be higher
                                                    than products that are suitable for investors in lower risk tolerance categories, but now as
                                                    much as for higher risk tolerance categories.
                                                </li>
                                                <li>
                                                    You are comfortable if the returns of your investment may fall below your original investment.
                                                </li>
                                            </ul>
                                            <p>You may consider funds with risk rating 3 or below.</p>
                                        </div>
                                        <div id="radio_hint_4" class="radio_hint hide">
                                            <p>You are a <strong>Growth</strong> investor if:</p>
                                            <ul>
                                                <li>
                                                    You are generally comfortable with achieving
                                                    potentially high level of return on your investment
                                                    coupled with a high level of risk.
                                                </li>
                                                <li>
                                                    You are comfortable with significant fluctuations in your invested amount
                                                    and understand the risk / reward equation.  Capital values of the product
                                                    can fluctuate and may fall below your original investment. Fluctuation is
                                                    expected to be higher than products that are suitable for investors in lower
                                                    risk tolerance categories, but not as much as for higher risk tolerance
                                                    categories.
                                                </li>
                                                <li>You are comfortable if the returns of your investment may fall substantially
                                                    below your original investment.</li>
                                            </ul>
                                            <p>You may consider funds with risk rating 4 or below.</p>
                                        </div>
                                        <div id="radio_hint_5" class="radio_hint hide">
                                        <p>You are a <strong>Speculative</strong> investor if:</p>
                                        <ul>
                                            <li>
                                                you are generally comfortable with maximizing your level of return on your investment coupled with maximized risk.
                                            </li>
                                            <li>
                                                You are comfortable with significant wide fluctuations in your invested amount and understand the risk / reward equation.
                                                Capital values of the product can fluctuate widely and may fall substantially below your original investment. You understand the risk/reward equation, and are comfortable with this level of fluctuation.
                                            </li>
                                            <li>You are comfortable if the returns of your investment may fall substantially below your original investment.</li>
                                        </ul>
                                        <p>You may consider funds with risk rating 5 or below.</p>
                                    </div>
                                    </div>
                                  <p><strong>
Please indicate the percentage of the monthly investment that you wish to allocate to the chosen fund(s), in
multiples of 5%. The total allocation must add up to 100%:</strong>
                                  </p>
                                  <div class="onlineinvestor__table__wrap">
                                    <table class="onlineinvestor__table">
                                      <thead>
                                      <tr>
                                          <th><strong>Fund</strong></th>
                                          <th><strong>Risk Rating</strong></th>
                                          <th colspan="4"><strong>Resources</strong></th>
                                          <th><strong>Allocation</strong></th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr class="green" style="font-size: .85em;">
                                          <th colspan="2"></th>
                                          <th>Product Highlights Sheet</th>
                                          <th>Product Summary</th>
                                          <th>Fund Factsheet</th>
                                          <th>Fund Summary</th>
                                          <th>%</th>
                                      </tr>
                                      <tr class="gray10">
                                          <td>HSBC Insurance Global High Income Bond Fund</td>
                                          <td>1</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      <tr class="gray5">
                                          <td>HSBC Insurance Global Multi-Asset Fund</td>
                                          <td>2</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      <tr class="gray10">
                                          <td>HSBC Insurance Global Emerging Markets Bond Fund</td>
                                          <td>2</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      <tr class="gray5">
                                          <td>HSBC Insurance World Selection 3 Fund</td>
                                          <td>3</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      <tr class="gray10">
                                          <td>HSBC Insurance Europe Dynamic Equity Fund</td>
                                          <td>4</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      <tr class="gray5">
                                          <td>HSBC Insurance Singapore Equity Fund</td>
                                          <td>4</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      <tr class="gray10">
                                          <td>HSBC Insurance US Equity Index Fund</td>
                                          <td>4</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      <tr class="gray5">
                                          <td>HSBC Insurance Pacific Equity Fund</td>
                                          <td>4</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      <tr class="gray10">
                                          <td>HSBC Insurance Global Equity Index Fund</td>
                                          <td>4</td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><a href="#" aria-label="Download"><i class="icon icon-download" aria-hidden="true">></i></a></td>
                                          <td><input type="text" class="form-control numbox" value="" onchange="updateTotal()" onkeydown="validate(event)"></td>
                                      </tr>
                                      </tbody>
                                      <tfoot>
                                      <tr class="gray5">
                                          <td colspan="6" style="text-align:right;"><strong>Total:</strong></td>
                                          <td><input type="text" class="form-control totalbox" value="0" disabled></td>
                                      </tr>
                                      </tfoot>
                                  </table>
                                </div><!-- /.onlineinvestor__table__wrap -->

                                </fieldset>
                                <div class="article__leader__main__btnset">
                                    <button type="button" class="btn btn-red" onclick="checkTotal()">Continue</button>
                                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application-step2a/')">Back</button>
                                </div>

                              </div><!-- /.item -->
                          </div>
                        </form>
                    </div>
                </div><!-- /.row -->



            </div><!--/ container -->
        </section>

    </article>


    <div class="modal fade" id="onlineinvestormodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
                </div>
                <div class="modal-body">
                    <p>Based on your response(s) we are unable to proceed with your application.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red">Back</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="onlineinvestormodal_totalincorrect">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Error</h2>
                </div>
                <div class="modal-body">
                    <p>Please ensure that the total allocation adds up to 100%</p>
                    <br>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red" data-dismiss="modal">Go back to application</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="onlineinvestormodal_riskexceed">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Warning</h2>
                </div>
                <div class="modal-body">
                    <p>You have selected investment-linked policy sub-fund(s) with a risk rating higher than your
                        selected risk profile which is <strong class="js-risk-level">2</strong>. This means that you are able to accept a higher level of fund price
                        fluctuations as compared to your chosen risk profile.
                        <br>
                        Would you like to revise your fund selection?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal" onclick="openPage('../onlineinvestor-application-step2c/')">No</button>

                    <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step2a/')">Yes</button>
                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">
    $(document).ready(function(){
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);

        if (sessionStorage.riskLevel) {
            var risklevel = parseInt(sessionStorage.riskLevel);
            $('.radio_hint:nth-of-type(' + risklevel + ')').removeClass('hide');
            $(".radio_hint:not(:nth-of-type(" + risklevel + "))").remove();
        }

        $(".totalbox").value = '0';

    });

    $(document).on("click", "[data-show]", function(e){
        $($(this).data("hide")).addClass("hide");
        $($(this).data("show")).removeClass("hide");
    });

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }

    function checkRiskLevel(){
        var riskLevel = sessionStorage.riskLevel;
        var selectedHigher = false;

        $(".numbox").each(function (i, obj) {
            if($(obj).val()){
                var planLevel = parseInt($(obj).parents('tr').find("td:nth-child(2)").text());
                if (planLevel > parseInt(riskLevel)){
                    selectedHigher = true;
                }
            }
        });

        if(selectedHigher){
            //show warning
            $('#onlineinvestormodal_riskexceed').modal({show: false});
            $('#onlineinvestormodal_riskexceed').modal('show');
        }else{
            openPage('../onlineinvestor-application-step2c/');
        }

    }

    function updateTotal() {
        var total = 0;
        var boxVal = 0;

        $(".numbox").each(function (i, obj) {
            boxVal = parseInt(obj.value, 10) || 0;

            if (boxVal > 100 || boxVal % 5 !== 0) {
                $(this).parent('td').addClass("has-error");
            }else{
                $(this).parent('td').removeClass("has-error");
            }

            total += boxVal;

            if (total > 100) {
                $('.totalbox').parent('td').addClass("has-error");
            } else {
                $('.totalbox').parent('td').removeClass("has-error");
            }
        });
        $(".totalbox").val(total);
    }

    function checkTotal(){
        if($('.totalbox').val() != 100){
            //show warning
            $('#onlineinvestormodal_totalincorrect').modal({show: false});
            $('#onlineinvestormodal_totalincorrect').modal('show');
        }else
        {
            checkRiskLevel();
        }
    }


    function validate(e){
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }

    if (sessionStorage.riskLevel) {

        var txt = '';

        if(parseInt(sessionStorage.riskLevel) == 1){
            txt= '1 Very Cautious';
        }else if(parseInt(sessionStorage.riskLevel) == 2){
            txt= '2 Cautious';
        }else if(parseInt(sessionStorage.riskLevel) == 3){
            txt= '3 Balanced';
        }else if(parseInt(sessionStorage.riskLevel) == 4){
            txt= '4 Adventurous';
        }else if(parseInt(sessionStorage.riskLevel) == 5){
            txt= '5 Speculative';
        }

        $(".js-risk-level").text(txt);
    }

    $(".onlineinvestor__table tbody tr:visible input").on("change", function () {
        var results = $(".onlineinvestor__table tbody tr:visible").filter(function () {
            var value = $("input", this).val();
            return value && parseInt(value, 10) > 0;
        }).map(function () {
            return {
                'name': $("td:first-child", this).text(),
                'allocation': parseInt($("input", this).val(), 10)

            }
        }).toArray();

        sessionStorage.selectedFunds = JSON.stringify(results);
    });
</script>
