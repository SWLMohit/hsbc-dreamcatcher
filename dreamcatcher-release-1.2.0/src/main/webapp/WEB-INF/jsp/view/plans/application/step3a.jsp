<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our Plans</a>
            <a href="#">HSBC Insurance OnlineInvestor</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
            <div class="masthead__container container">
            </div>
        </section>

        <section class="onlineinvestor">
            <div class="container">
                <h1>HSBC Insurance OnlineInvestor application</h1>


                <div class="onlineinvestor__breadcrumb">
                    <a href="../onlineinvestor-application-step1a/">Eligibility check</a>
                    <a href="../onlineinvestor-application-step2a/">Your plan</a>
                    <a href="#" class="current">Your details</a>
                    <a href="#" class="disabled">Payment</a>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <form>
                          <div class="onlineinvestor_carousel">
                              <div class="form-item item1 active">
                                <fieldset>
                                  <legend>Please provide us with your personal particulars</legend>
                                  <dl class="onlineinvestor__questionnaire">
                                    <dt>NRIC / Passport number</dt>
                                    <dd class="form-group">
                                      <input type="text" class="form-control">
                                    </dd>

                                    <dt>Residential and mailing address <small>(in Singapore)</small></dt>
                                    <dd class="form-group">
                                      <div class="form-group__multi form-group__multi--residential_line1">
                                        <span>
                                          <p>Postal code</p>
                                          <input type="text" class="form-control" placeholder="">
                                        </span>
                                        <span>
                                          <p>House / Blk number</p>
                                          <input type="text" class="form-control" placeholder="">
                                        </span>
                                      </div>
                                    </dd>
                                    <dd class="form-group">
                                      <p>Unit number</p>
                                      <div class="form-group__multi form-group__multi--residential_line2">
                                        <span>
                                          <input type="text" class="form-control" placeholder="">
                                        </span>
                                        <span>
                                          <input type="text" class="form-control" placeholder="">
                                        </span>
                                      </div>
                                    </dd>
                                    <dd class="form-group">
                                      <p>Street</p>
                                      <input type="text" class="form-control">
                                    </dd>
                                    <dd class="form-group">
                                      <p>Building name <small>(optional)</small></p>
                                      <input type="text" class="form-control">
                                    </dd>


                                    <dt>Is the above residential address the same as stated on your NRIC?</dt>
                                    <dd class="form-group">
                                      <div class="newradio--inline">
                                        <input id="id_residential_option1" type="radio" name="residential" value="Yes">
                                        <label for="id_residential_option1"><span><span></span></span>Yes</label>
                                      </div>
                                      <div class="newradio--inline">
                                        <input id="id_residential_option2" type="radio" name="residential" value="No">
                                        <label for="id_residential_option2"><span><span></span></span>No</label>
                                      </div>
                                    </dd>

                                    <div class="same-as-stated-wrapper">
                                    <dt>Please upload a copy of any one of the following documents as proof of address 
                                        <button type="button" class="help-tooltip" tabindex="0" data-container="body" 
                                        data-toggle="popover" data-placement="right" label="help"
                                        aria-label="A utility/telco bill, a bank statement or any correspondence from a Singapore government body issued within the last three months."
                                        data-content="A utility/telco bill, a bank statement or any correspondence from a Singapore government body issued within the last three months.">
<i class="icon icon-circle-help-solid"></i></button>:
</dt>
                                    <dd class="form-group">
                                      <button class="btn btn-default" type="button" aria-label="Download">Upload <i class="icon icon-download" aria-hidden="true"></i></button>
                                    </dd>

                                        <dt>Time at Residential Address</dt>
                                    <dd class="form-group">
                                          <div class="form-group__multi form-group__multi--date form-group__multi--date--no-days">
                                            <span>
                                                <input type="text" class="form-control date-year" placeholder="YYYY" onkeydown="validate(event)" onchange="datecheck(event)">
                                            </span>
                                            <span>
                                                <input type="text" class="form-control date-month" placeholder="MM" onkeydown="validate(event)" onchange="datecheck(event)">
                                            </span>
                                          </div>
                                            <div class="date-error has-error">
                                                <span class="help-block">Invalid date</span>
                                            </div>
                                    </dd>

                                    <dt>Please state the country of your previous residential
address as you have not resided at your current address
for five years or more.</dt>
                                    <dd class="form-group">
                                            <div class="form-select_wrap"><select class="form-control control_country_select">
                                                <option selected disabled>Please select...</option>
                                                <option>Singapore</option>
                                                <option>Afghanistan</option>
                                                <option>Albania</option>
                                                <option>Algeria</option>
                                                <option>American Samoa</option>
                                                <option>Andorra</option>
                                                <option>Angola</option>
                                                <option>Anguilla</option>
                                                <option>Antarctica</option>
                                                <option>Antigua and Barbuda</option>
                                                <option>Argentina</option>
                                                <option>Armenia</option>
                                                <option>Aruba</option>
                                                <option>Australia</option>
                                                <option>Austria</option>
                                                <option>Azerbaijan</option>
                                                <option>Bahama</option>
                                                <option>Bahrain</option>
                                                <option>Bangladesh</option>
                                                <option>Barbados</option>
                                                <option>Belarus</option>
                                                <option>Belgium</option>
                                                <option>Belize</option>
                                                <option>Benin</option>
                                                <option>Bermuda</option>
                                                <option>Bhutan</option>
                                                <option>Bolivia</option>
                                                <option>Bosnia and Herzegovina</option>
                                                <option>Botswana</option>
                                                <option>Bouvet Island</option>
                                                <option>Brazil</option>
                                                <option>British Indian Ocean Territory</option>
                                                <option>British Virgin Islands</option>
                                                <option>Brunei Darussalam</option>
                                                <option>Bulgaria</option>
                                                <option>Burkina Faso</option>
                                                <option>Burundi</option>
                                                <option>Cambodia</option>
                                                <option>Cameroon</option>
                                                <option>Canada</option>
                                                <option>Cape Verde</option>
                                                <option>Cayman Islands</option>
                                                <option>Central African Republic</option>
                                                <option>Chad</option>
                                                <option>Chile</option>
                                                <option>China</option>
                                                <option>Christmas Island</option>
                                                <option>Cocos (Keeling) Islands</option>
                                                <option>Colombia</option>
                                                <option>Comoros</option>
                                                <option>Congo</option>
                                                <option>Cook Islands</option>
                                                <option>Costa Rica</option>
                                                <option>Croatia</option>
                                                <option>Cuba</option>
                                                <option>Cyprus</option>
                                                <option>Czech Republic</option>
                                                <option>Denmark</option>
                                                <option>Djibouti</option>
                                                <option>Dominica</option>
                                                <option>Dominican Republic</option>
                                                <option>East Timor</option>
                                                <option>Ecuador</option>
                                                <option>Egypt</option>
                                                <option>El Salvador</option>
                                                <option>Equatorial Guinea</option>
                                                <option>Eritrea</option>
                                                <option>Estonia</option>
                                                <option>Ethiopia</option>
                                                <option>Falkland Islands (Malvinas)</option>
                                                <option>Faroe Islands</option>
                                                <option>Fiji</option>
                                                <option>Finland</option>
                                                <option>France</option>
                                                <option>France, Metropolitan</option>
                                                <option>French Guiana</option>
                                                <option>French Polynesia</option>
                                                <option>French Southern Territories</option>
                                                <option>Gabon</option>
                                                <option>Gambia</option>
                                                <option>Georgia</option>
                                                <option>Germany</option>
                                                <option>Ghana</option>
                                                <option>Gibraltar</option>
                                                <option>Greece</option>
                                                <option>Greenland</option>
                                                <option>Grenada</option>
                                                <option>Guadeloupe</option>
                                                <option>Guam</option>
                                                <option>Guatemala</option>
                                                <option>Guinea</option>
                                                <option>Guinea-Bissau</option>
                                                <option>Guyana</option>
                                                <option>Haiti</option>
                                                <option>Heard Island and McDonald Islands</option>
                                                <option>Honduras</option>
                                                <option>Hong Kong</option>
                                                <option>Hungary</option>
                                                <option>Iceland</option>
                                                <option>India</option>
                                                <option>Indonesia</option>
                                                <option>Islamic Republic of Iran</option>
                                                <option>Iraq</option>
                                                <option>Ireland</option>
                                                <option>Israel</option>
                                                <option>Italy</option>
                                                <option>Jamaica</option>
                                                <option>Japan</option>
                                                <option>Jordan</option>
                                                <option>Kazakhstan</option>
                                                <option>Kenya</option>
                                                <option>Kiribati</option>
                                                <option>Korea, Democratic People's Republic of</option>
                                                <option>Korea, Republic of</option>
                                                <option>Kuwait</option>
                                                <option>Kyrgyzstan</option>
                                                <option>Lao People's Democratic Republic</option>
                                                <option>Latvia</option>
                                                <option>Lebanon</option>
                                                <option>Lesotho</option>
                                                <option>Liberia</option>
                                                <option>Libyan Arab Jamahiriya</option>
                                                <option>Liechtenstein</option>
                                                <option>Lithuania</option>
                                                <option>Luxembourg</option>
                                                <option>Macau</option>
                                                <option>Madagascar</option>
                                                <option>Malawi</option>
                                                <option>Malaysia</option>
                                                <option>Maldives</option>
                                                <option>Mali</option>
                                                <option>Malta</option>
                                                <option>Marshall Islands</option>
                                                <option>Martinique</option>
                                                <option>Mauritania</option>
                                                <option>Mauritius</option>
                                                <option>Mayotte</option>
                                                <option>Mexico</option>
                                                <option>Micronesia</option>
                                                <option>Moldova, Republic of</option>
                                                <option>Monaco</option>
                                                <option>Mongolia</option>
                                                <option>Monserrat</option>
                                                <option>Morocco</option>
                                                <option>Mozambique</option>
                                                <option>Myanmar</option>
                                                <option>Nambia</option>
                                                <option>Nauru</option>
                                                <option>Nepal</option>
                                                <option>Netherlands</option>
                                                <option>Netherlands Antilles</option>
                                                <option>New Caledonia</option>
                                                <option>New Zealand</option>
                                                <option>Nicaragua</option>
                                                <option>Niger</option>
                                                <option>Nigeria</option>
                                                <option>Niue</option>
                                                <option>Norfolk Island</option>
                                                <option>Northern Mariana Islands</option>
                                                <option>Norway</option>
                                                <option>Oman</option>
                                                <option>Pakistan</option>
                                                <option>Palau</option>
                                                <option>Panama</option>
                                                <option>Papua New Guinea</option>
                                                <option>Paraguay</option>
                                                <option>Peru</option>
                                                <option>Philippines</option>
                                                <option>Pitcairn</option>
                                                <option>Poland</option>
                                                <option>Portugal</option>
                                                <option>Puerto Rico</option>
                                                <option>Qatar</option>
                                                <option>Romania</option>
                                                <option>Russian Federation</option>
                                                <option>Rwanda</option>
                                                <option>S. Georgia & the S. Sandwich Islands</option>
                                                <option>Saint Lucia</option>
                                                <option>Samoa</option>
                                                <option>San Marino</option>
                                                <option>Sao Tome and Principe</option>
                                                <option>Saudi Arabia</option>
                                                <option>Senegal</option>
                                                <option>Seychelles</option>
                                                <option>Sierra Leone</option>
                                                <option>Slovakia</option>
                                                <option>Slovenia</option>
                                                <option>Solomon Islands</option>
                                                <option>Somalia</option>
                                                <option>South Africa</option>
                                                <option>Spain</option>
                                                <option>Sri Lanka</option>
                                                <option>St. Helena</option>
                                                <option>St. Kitts and Nevis</option>
                                                <option>St. Pierre and Miquelon</option>
                                                <option>St. Vincent and the Grenadines</option>
                                                <option>Sudan</option>
                                                <option>Suriname</option>
                                                <option>Svalbard and Jan Mayen Islands</option>
                                                <option>Swaziland</option>
                                                <option>Sweden</option>
                                                <option>Switzerland</option>
                                                <option>Syrian Arab Republic</option>
                                                <option>Taiwan, Province of China</option>
                                                <option>Tajikistan</option>
                                                <option>Thailand</option>
                                                <option>Togo</option>
                                                <option>Tokelau</option>
                                                <option>Tonga</option>
                                                <option>Trinidad and Tobago</option>
                                                <option>Tunisia</option>
                                                <option>Turkey</option>
                                                <option>Turkmenistan</option>
                                                <option>Turks and Caicos Islands</option>
                                                <option>Tuvalu</option>
                                                <option>Uganda</option>
                                                <option>Ukraine</option>
                                                <option>United Arab Emirates</option>
                                                <option>United Kingdom (Great Britain)</option>
                                                <option>United Republic of Tanzania</option>                                                
                                                <option>United States of America</option>
                                                <option>United States Minor Outlying Islands</option>
                                                <option>United States Virgin Islands</option>
                                                <option>Uruguay</option>
                                                <option>Uzbekistan</option>
                                                <option>Vanuatu</option>
                                                <option>Vatican City State (Holy See)</option>
                                                <option>Venezuela</option>
                                                <option>Viet Nam</option>
                                                <option>Wallis and Futuna Islands</option>
                                                <option>Western Sahara</option>
                                                <option>Yugoslavia</option>
                                                <option>Zaire</option>
                                                <option>Zambia</option>
                                                <option>Zimbabwe</option>
                                                <option value="none_of_above">None of the above</option>
                                     </select></div>
                                    </dd>

                                        <dt>Is the above residential address also your permanent address?
                                            <button type="button" class="help-tooltip" tabindex="0" data-container="body" 
                                                    data-toggle="popover" data-placement="top" label="help"
                                                    aria-label='"Permanent address" refers to an address where a customer intends to stay permanently.'
                                                    data-content='"Permanent address" refers to an address where a customer intends to stay permanently.'>
                                                <i class="icon icon-circle-help-solid"></i></button></dt>
                                    <dd class="form-group">
                                      <div class="newradio--inline">
                                        <input id="id_residential_again_option1" type="radio" name="residential_again" value="Yes">
                                        <label for="id_residential_again_option1"><span><span></span></span>Yes</label>
                                      </div>
                                      <div class="newradio--inline">
                                        <input id="id_residential_again_option2" type="radio" name="residential_again" value="No">
                                        <label for="id_residential_again_option2"><span><span></span></span>No</label>
                                      </div>
                                    </dd>

                                    <div class="permanent-address-wrapper">
                                        <dt>Permanent address
                                            <button type="button" class="help-tooltip" tabindex="0" data-container="body" 
                                                data-toggle="popover" data-placement="top" label="help"
                                                aria-label='"Permanent address" refers to an address where a customer intends to stay permanently.'
                                                data-content='"Permanent address" refers to an address where a customer intends to stay permanently.'>
                                                <i class="icon icon-circle-help-solid"></i>
                                            </button>
                                            <small>(For foreigners, please indicate your overseas permanent address as the permanent address)</small>
                                        </dt>
                                        <dd class="form-group">
                                            <p>Address Line 1:</p>
                                            <textarea class="form-control"></textarea>
                                            <small>105 characters remaining</small>
                                        </dd>
                                        <dd class="form-group">
                                            <p>Address Line 2:</p>
                                            <textarea class="form-control"></textarea>
                                            <small>105 characters remaining</small>
                                        </dd>
                                        <dd class="form-group">
                                            <p>Postal code</p>
                                            <input type="text" class="form-control">
                                        </dd>
                                        <dd class="form-group">
                                            <p>Country</p>
                                            <div class="form-select_wrap">
                                                <select class="form-control">
                                                    <option selected disabled>Please select...</option>
                                                    <option>Singapore</option>
                                                    <option>Afghanistan</option>
                                                    <option>Albania</option>
                                                    <option>Algeria</option>
                                                    <option>American Samoa</option>
                                                    <option>Andorra</option>
                                                    <option>Angola</option>
                                                    <option>Anguilla</option>
                                                    <option>Antarctica</option>
                                                    <option>Antigua and Barbuda</option>
                                                    <option>Argentina</option>
                                                    <option>Armenia</option>
                                                    <option>Aruba</option>
                                                    <option>Australia</option>
                                                    <option>Austria</option>
                                                    <option>Azerbaijan</option>
                                                    <option>Bahama</option>
                                                    <option>Bahrain</option>
                                                    <option>Bangladesh</option>
                                                    <option>Barbados</option>
                                                    <option>Belarus</option>
                                                    <option>Belgium</option>
                                                    <option>Belize</option>
                                                    <option>Benin</option>
                                                    <option>Bermuda</option>
                                                    <option>Bhutan</option>
                                                    <option>Bolivia</option>
                                                    <option>Bosnia and Herzegovina</option>
                                                    <option>Botswana</option>
                                                    <option>Bouvet Island</option>
                                                    <option>Brazil</option>
                                                    <option>British Indian Ocean Territory</option>
                                                    <option>British Virgin Islands</option>
                                                    <option>Brunei Darussalam</option>
                                                    <option>Bulgaria</option>
                                                    <option>Burkina Faso</option>
                                                    <option>Burundi</option>
                                                    <option>Cambodia</option>
                                                    <option>Cameroon</option>
                                                    <option>Canada</option>
                                                    <option>Cape Verde</option>
                                                    <option>Cayman Islands</option>
                                                    <option>Central African Republic</option>
                                                    <option>Chad</option>
                                                    <option>Chile</option>
                                                    <option>China</option>
                                                    <option>Christmas Island</option>
                                                    <option>Cocos (Keeling) Islands</option>
                                                    <option>Colombia</option>
                                                    <option>Comoros</option>
                                                    <option>Congo</option>
                                                    <option>Cook Islands</option>
                                                    <option>Costa Rica</option>
                                                    <option>Croatia</option>
                                                    <option>Cuba</option>
                                                    <option>Cyprus</option>
                                                    <option>Czech Republic</option>
                                                    <option>Denmark</option>
                                                    <option>Djibouti</option>
                                                    <option>Dominica</option>
                                                    <option>Dominican Republic</option>
                                                    <option>East Timor</option>
                                                    <option>Ecuador</option>
                                                    <option>Egypt</option>
                                                    <option>El Salvador</option>
                                                    <option>Equatorial Guinea</option>
                                                    <option>Eritrea</option>
                                                    <option>Estonia</option>
                                                    <option>Ethiopia</option>
                                                    <option>Falkland Islands (Malvinas)</option>
                                                    <option>Faroe Islands</option>
                                                    <option>Fiji</option>
                                                    <option>Finland</option>
                                                    <option>France</option>
                                                    <option>France, Metropolitan</option>
                                                    <option>French Guiana</option>
                                                    <option>French Polynesia</option>
                                                    <option>French Southern Territories</option>
                                                    <option>Gabon</option>
                                                    <option>Gambia</option>
                                                    <option>Georgia</option>
                                                    <option>Germany</option>
                                                    <option>Ghana</option>
                                                    <option>Gibraltar</option>
                                                    <option>Greece</option>
                                                    <option>Greenland</option>
                                                    <option>Grenada</option>
                                                    <option>Guadeloupe</option>
                                                    <option>Guam</option>
                                                    <option>Guatemala</option>
                                                    <option>Guinea</option>
                                                    <option>Guinea-Bissau</option>
                                                    <option>Guyana</option>
                                                    <option>Haiti</option>
                                                    <option>Heard Island and McDonald Islands</option>
                                                    <option>Honduras</option>
                                                    <option>Hong Kong</option>
                                                    <option>Hungary</option>
                                                    <option>Iceland</option>
                                                    <option>India</option>
                                                    <option>Indonesia</option>
                                                    <option>Islamic Republic of Iran</option>
                                                    <option>Iraq</option>
                                                    <option>Ireland</option>
                                                    <option>Israel</option>
                                                    <option>Italy</option>
                                                    <option>Jamaica</option>
                                                    <option>Japan</option>
                                                    <option>Jordan</option>
                                                    <option>Kazakhstan</option>
                                                    <option>Kenya</option>
                                                    <option>Kiribati</option>
                                                    <option>Korea, Democratic People's Republic of</option>
                                                    <option>Korea, Republic of</option>
                                                    <option>Kuwait</option>
                                                    <option>Kyrgyzstan</option>
                                                    <option>Lao People's Democratic Republic</option>
                                                    <option>Latvia</option>
                                                    <option>Lebanon</option>
                                                    <option>Lesotho</option>
                                                    <option>Liberia</option>
                                                    <option>Libyan Arab Jamahiriya</option>
                                                    <option>Liechtenstein</option>
                                                    <option>Lithuania</option>
                                                    <option>Luxembourg</option>
                                                    <option>Macau</option>
                                                    <option>Madagascar</option>
                                                    <option>Malawi</option>
                                                    <option>Malaysia</option>
                                                    <option>Maldives</option>
                                                    <option>Mali</option>
                                                    <option>Malta</option>
                                                    <option>Marshall Islands</option>
                                                    <option>Martinique</option>
                                                    <option>Mauritania</option>
                                                    <option>Mauritius</option>
                                                    <option>Mayotte</option>
                                                    <option>Mexico</option>
                                                    <option>Micronesia</option>
                                                    <option>Moldova, Republic of</option>
                                                    <option>Monaco</option>
                                                    <option>Mongolia</option>
                                                    <option>Monserrat</option>
                                                    <option>Morocco</option>
                                                    <option>Mozambique</option>
                                                    <option>Myanmar</option>
                                                    <option>Nambia</option>
                                                    <option>Nauru</option>
                                                    <option>Nepal</option>
                                                    <option>Netherlands</option>
                                                    <option>Netherlands Antilles</option>
                                                    <option>New Caledonia</option>
                                                    <option>New Zealand</option>
                                                    <option>Nicaragua</option>
                                                    <option>Niger</option>
                                                    <option>Nigeria</option>
                                                    <option>Niue</option>
                                                    <option>Norfolk Island</option>
                                                    <option>Northern Mariana Islands</option>
                                                    <option>Norway</option>
                                                    <option>Oman</option>
                                                    <option>Pakistan</option>
                                                    <option>Palau</option>
                                                    <option>Panama</option>
                                                    <option>Papua New Guinea</option>
                                                    <option>Paraguay</option>
                                                    <option>Peru</option>
                                                    <option>Philippines</option>
                                                    <option>Pitcairn</option>
                                                    <option>Poland</option>
                                                    <option>Portugal</option>
                                                    <option>Puerto Rico</option>
                                                    <option>Qatar</option>
                                                    <option>Romania</option>
                                                    <option>Russian Federation</option>
                                                    <option>Rwanda</option>
                                                    <option>S. Georgia & the S. Sandwich Islands</option>
                                                    <option>Saint Lucia</option>
                                                    <option>Samoa</option>
                                                    <option>San Marino</option>
                                                    <option>Sao Tome and Principe</option>
                                                    <option>Saudi Arabia</option>
                                                    <option>Senegal</option>
                                                    <option>Seychelles</option>
                                                    <option>Sierra Leone</option>
                                                    <option>Slovakia</option>
                                                    <option>Slovenia</option>
                                                    <option>Solomon Islands</option>
                                                    <option>Somalia</option>
                                                    <option>South Africa</option>
                                                    <option>Spain</option>
                                                    <option>Sri Lanka</option>
                                                    <option>St. Helena</option>
                                                    <option>St. Kitts and Nevis</option>
                                                    <option>St. Pierre and Miquelon</option>
                                                    <option>St. Vincent and the Grenadines</option>
                                                    <option>Sudan</option>
                                                    <option>Suriname</option>
                                                    <option>Svalbard and Jan Mayen Islands</option>
                                                    <option>Swaziland</option>
                                                    <option>Sweden</option>
                                                    <option>Switzerland</option>
                                                    <option>Syrian Arab Republic</option>
                                                    <option>Taiwan, Province of China</option>
                                                    <option>Tajikistan</option>
                                                    <option>Thailand</option>
                                                    <option>Togo</option>
                                                    <option>Tokelau</option>
                                                    <option>Tonga</option>
                                                    <option>Trinidad and Tobago</option>
                                                    <option>Tunisia</option>
                                                    <option>Turkey</option>
                                                    <option>Turkmenistan</option>
                                                    <option>Turks and Caicos Islands</option>
                                                    <option>Tuvalu</option>
                                                    <option>Uganda</option>
                                                    <option>Ukraine</option>
                                                    <option>United Arab Emirates</option>
                                                    <option>United Kingdom (Great Britain)</option>
                                                    <option>United Republic of Tanzania</option>                                                   
                                                    <option>United States of America</option>
                                                    <option>United States Minor Outlying Islands</option>
                                                    <option>United States Virgin Islands</option>
                                                    <option>Uruguay</option>
                                                    <option>Uzbekistan</option>
                                                    <option>Vanuatu</option>
                                                    <option>Vatican City State (Holy See)</option>
                                                    <option>Venezuela</option>
                                                    <option>Viet Nam</option>
                                                    <option>Wallis and Futuna Islands</option>
                                                    <option>Western Sahara</option>
                                                    <option>Yugoslavia</option>
                                                    <option>Zaire</option>
                                                    <option>Zambia</option>
                                                    <option>Zimbabwe</option>
                                                    <option value="none_of_above">None of the above</option>
                                                </select></div>
                                        </dd>
                                    </div>
                                    </div>
                                  </dl>
                                </fieldset>
                                <div class="article__leader__main__btnset">
                                    <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step3b/')">Continue</button>
                                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application-step2a/')">Back</button>
                                </div>
                              </div><!-- /.item -->
                          </div>

                        </form>
                    </div>
                </div><!-- /.row -->
            </div><!--/ container -->
        </section>

    </article>


    <div class="modal fade" id="onlineinvestormodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
                </div>
                <div class="modal-body">
                    <p>Based on your response(s) we are unable to proceed with your application.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red">Back</button>
                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">
    $(document).ready(function(){
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);

        $('.permanent-address-wrapper').hide();
        $('.same-as-stated-wrapper').hide();
        $('.date-error').hide();
    });

    function validate(e){
        // Allow: backspace, delete, tab, escape and enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    }

    function datecheck(event){

        item = event.currentTarget;

        var year = $('.date-year').val();
        var month = $('.date-month').val();
        var day = $('.date-day').val();

        if( !isNaN(parseInt(year)) && !isNaN(parseInt(month)) && !isNaN(parseInt(day))) {
            if (parseInt(year) >= 100 || parseInt(month) > 12 || parseInt(day) > 31) {
                $('input[class*="date-"]').parent('span').addClass("has-error");
                $('.date-error').show();
            } else {
                //date is valid, remove all error indicators
                $('input[class*="date-"]').parent('span').removeClass("has-error");
                $('.date-error').hide();

                $('.date-year').val(pad(year,2));
                $('.date-month').val(pad(month,2));
                $('.date-day').val(pad(day,2));

            }
        }
    }
    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }

    $('input:radio[name="residential_again"]').change(
        function(){
            if ($(this).val() == 'Yes') {
                $('.permanent-address-wrapper').hide();
            }
            else {
                $('.permanent-address-wrapper').show();
            }
        });
    $('input:radio[name="residential"]').change(
        function(){
            if ($(this).val() == 'Yes') {
                $('.same-as-stated-wrapper').hide();
            }
            else {
                $('.same-as-stated-wrapper').show();
            }
        });
</script>
