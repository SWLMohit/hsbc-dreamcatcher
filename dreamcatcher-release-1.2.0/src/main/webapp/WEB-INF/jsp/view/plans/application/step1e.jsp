<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our Plans</a>
            <a href="#">HSBC Insurance OnlineInvestor</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
            <div class="masthead__container container">
            </div>
        </section>

        <section class="onlineinvestor">
            <div class="container">
                <h1>HSBC Insurance OnlineInvestor application</h1>


                <div class="onlineinvestor__breadcrumb">
                    <a href="#" class="current">Eligibility Check</a>
                    <a href="#" class="disabled">Your Plan</a>
                    <a href="#" class="disabled">Your details</a>
                    <a href="#" class="disabled">Payment</a>
                </div>


                <div class="row">
                    <div class="col-md-8">
                        <form>
                          <div class="onlineinvestor_carousel">
                              <div class="form-item item5 active">
                                <fieldset>
                                  <legend>Tell us more about yourself before proceeding with your application</legend>
                                  <dl class="onlineinvestor__questionnaire">
                                    <dt>Are you proficient in the English Language (spoken and written) and have you completed at least a secondary school education or its equivalent?</dt>
                                    <dd class="form-group">
                                      <div class="newradio--inline">
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                        <label for="optionsRadios1"><span><span></span></span>Yes</label>
                                      </div>
                                      <div class="newradio--inline">
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"
                                                 data-toggle="modal" data-target="#onlineinvestormodal">
                                        <label for="optionsRadios2"><span><span></span></span>No</label>
                                      </div>
                                    </dd>
                                    <dt>What is your current employment status?</dt>
                                    <dd class="form-group">
                                      <div class="form-select_wrap">
                                        <select class="form-control control_employment_select">
                                            <option disabled selected>Please select&hellip;</option>
                                        <option value="salaried">Salaried</option>
                                        <option value="self-employed">Self-employed</option>
                                        <option value="homemaker">Homemaker</option>
                                        <option value="retired">Retired</option>
                                        <option value="unemployed">Unemployed</option>
                                        <option value="student">Student</option>
                                      </select></div>
                                    </dd>
                                    <dt class="control_occupation control_employment">What is your occupation?</dt>
                                    <dd class="form-group control_occupation control_employment">
                                      <input type="text" class="form-control">
                                    </dd>
                                    <dt class="control_study control_employment">What is the end date of your studies?</dt>
                                    <dd class="form-group control_study control_employment">
                                      <div class="form-group__multi form-group__multi--date">
                                        <span>
                                          <%--<input type="text" class="form-control" placeholder="MM">--%>
                                          <select class="form-control">
                                            <option selected disabled>MM</option>
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                          </select>
                                        </span>
                                        <span>
                                          <%--<input type="text" class="form-control" placeholder="YYYY">--%>
                                          <select class="form-control" id="control_buy_for_year">
                                            <option selected disabled>YYYY</option>
                                            <option>2017</option>
                                            <option>2018</option>
                                            <option>2019</option>
                                            <option>2020</option>
                                            <option>2021</option>
                                          </select>
                                        </span>
                                      </div>
                                    </dd>
                                    <dt class="control_industry control_employment">Which industry do you work in?</dt>
                                    <dd class="form-group control_industry control_employment">
                                      <div class="form-select_wrap">
                                        <select class="form-control">
                                            <option disabled selected>Please select&hellip;</option>
                                        <option>Advertising/Public Relations</option>
                                        <option>Agriculture/Fishing/Mining</option>
                                        <option>Audit & Accountancy</option>
                                        <option>Aviation & Transport</option>
                                        <option>Banking</option>
                                        <option>Casino & Gambling</option>
                                        <option>Charity/Nonprofit organisation/Non-Government Organisation</option>
                                        <option>Communications</option>
                                        <option>Construction</option>
                                        <option>Construction & Engineering</option>
                                        <option>Consultancy</option>
                                        <option>Defence</option>
                                        <option>Distributors</option>
                                        <option>Education</option>
                                        <option>Entertainment</option>
                                        <option>Financial Services</option>
                                        <option>Food</option>
                                        <option>Gas/Utility/Oil</option>
                                        <option>Government/State-Owned bodies</option>
                                        <option>Hotel/Restaurant/FoodCatering</option>
                                        <option>Import & Export</option>
                                        <option>Information Tech/Communication</option>
                                        <option>Insurance</option>
                                        <option>Iron & Steel</option>
                                        <option>Legal</option>
                                        <option>Library and Museums</option>
                                        <option>Manufacturing</option>
                                        <option>Medical & Health Services</option>
                                        <option>Military product distribution</option>
                                        <option>Military production</option>
                                        <option>Money Changer & Remittance</option>
                                        <option>Money Service Business</option>
                                        <option>Multi-Industry</option>
                                        <option>Non Financial Services</option>
                                        <option>Organisations/Clubs/Societies</option>
                                        <option>Others</option>
                                        <option>Pawn Shop</option>
                                        <option>Police & Civil Defence</option>
                                        <option>Printing & Publishing</option>
                                        <option>Property sales & development</option>
                                        <option>Research</option>
                                        <option>Restaurants</option>
                                        <option>Shipping</option>
                                        <option>Transport-People</option>
                                        <option>Travel & Tourism</option>
                                        <option>Unemployed</option>
                                        <option>Whole Sale/Retail</option>
                                      </select></div>
                                    </dd>
                                    <dt class="control_company control_employment control_company_detail_label"><span class="control_company_label">Employer / Company</span> details</dt>
                                    <dd class="form-group control_company control_employment">
                                      <p><span class="control_company_label">Employer / Company</span> name</p>
                                      <input type="text" class="form-control">
                                    </dd>
                                    <dd class="form-group control_employment control_employment_city">
                                      <p>City</p>
                                      <div class="form-select_wrap control_employment control_employment_city">
                                          <select class="form-control">
                                              <option disabled>Please select&hellip;</option>
                                              <option selected>Singapore</option>
                                              <option>Australia</option>
                                              <option>Austria</option>
                                              <option>Bahrain</option>
                                              <option>Belgium</option>
                                              <option>Bermuda</option>
                                              <option>Brunei</option>
                                              <option>Canada</option>
                                              <option>Hong Kong</option>
                                              <option>Denmark</option>
                                              <option>Finland</option>
                                              <option>France</option>
                                              <option>Germany</option>
                                              <option>Ireland</option>
                                              <option>Italy</option>
                                              <option>Japan</option>
                                              <option>South Korea</option>
                                              <option>Liechtenstein</option>
                                              <option>Luxembourg</option>
                                              <option>Malaysia</option>
                                              <option>Netherlands</option>
                                              <option>New Zealand</option>
                                              <option>Norway</option>
                                              <option>Singapore</option>
                                              <option>Spain</option>
                                              <option>Sweden</option>
                                              <option>Switzerland</option>
                                              <option>Taiwan</option>
                                              <option>United Arab Emirates</option>
                                              <option>United Kingdom</option>
                                              <option>United States of America</option>
                                              <option>Vatican City</option>
                                              <option>China</option>
                                              <option>India</option>
                                              <option>Indonesia</option>
                                              <option>Philippines</option>
                                              <option>Thailand</option>
                                              <option>Vietnam</option>
                                          </select></div>
                                    </dd>
                                    <dd class="form-group control_employment control_employment_country">
                                      <p>Country</p>
                                      <div class="form-select_wrap control_employment control_employment_country">
                                        <select class="form-control">
                                          <option>Singapore</option>
                                          <option>Australia</option>
                                          <option>Austria</option>
                                          <option>Bahrain</option>
                                          <option>Belgium</option>
                                          <option>Bermuda</option>
                                          <option>Brunei</option>
                                          <option>Canada</option>
                                          <option>Hong Kong</option>
                                          <option>Denmark</option>
                                          <option>Finland</option>
                                          <option>France</option>
                                          <option>Germany</option>
                                          <option>Ireland</option>
                                          <option>Italy</option>
                                          <option>Japan</option>
                                          <option>South Korea</option>
                                          <option>Liechtenstein</option>
                                          <option>Luxembourg</option>
                                          <option>Malaysia</option>
                                          <option>Netherlands</option>
                                          <option>New Zealand</option>
                                          <option>Norway</option>
                                          <option>Singapore</option>
                                          <option>Spain</option>
                                          <option>Sweden</option>
                                          <option>Switzerland</option>
                                          <option>Taiwan</option>
                                          <option>United Arab Emirates</option>
                                          <option>United Kingdom</option>
                                          <option>United States of America</option>
                                          <option>Vatican City</option>
                                          <option>China</option>
                                          <option>India</option>
                                          <option>Indonesia</option>
                                          <option>Philippines</option>
                                          <option>Thailand</option>
                                          <option>Vietnam</option>
                                        </select></div>
                                    </dd>
                                    <dt class="control_salary">What is your current annual income?</dt>
                                    <dd class="form-group control_salary">
                                      <input type="text" class="form-control control_salary" value="0" placeholder="S$">
                                    </dd>
                                  </dl>
                                </fieldset>
                                <div class="article__leader__main__btnset">
                                    <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step2a/')">Continue</button>
                                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application-step1c/')">Back</button>
                                </div>

                              </div><!-- /.item -->
                            </div>
                        </form>
                    </div>
                </div><!-- /.row -->
            </div><!--/ container -->
        </section>

    </article>


    <div class="modal fade" id="onlineinvestormodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            onclick="openPage('../onlineinvestor-application/')">
                      <span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
                </div>
                <div class="modal-body">
                    <p>Based on your response(s) we are unable to proceed with your application.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red" data-dismiss="modal"
                            onclick="openPage('../onlineinvestor-application/')">Back</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="onlineinvestormodal_child">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    onclick="openPage('../onlineinvestor-application/')">
              <span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
          </div>
          <div class="modal-body">
            <p>Your child or Legal ward has to be 18 years old and below in order to for you to continue with this application form.</p>
            <p>Your child, however, can purchase this product in his/her own name.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-red" data-dismiss="modal"
                    onclick="openPage('../onlineinvestor-application/')">Back</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="onlineinvestormodal_warning">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="myModalLabel">Warning!</h2>
          </div>
          <div class="modal-body">
            <p>Your child or Legal ward has to be 17 years old and below in order to for you to continue with this application form.</p>
            <p>Your child, however, can purchase this product in his/her own name.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-red" data-dismiss="modal">Continue</button>
          </div>
        </div>
      </div>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">
    $(document).ready(function(){
        formInit();
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);
    });

    function formInit() {
        $(".control_employment").hide();
        $(".control_salary").hide();
        $(".control_occupation").show();
    }

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }

    $("select.control_residency_select").change(function() {
      if ($(this).val() == 'none_of_above') {
        $("#onlineinvestormodal").modal("show");
      }
      if ($(this).val() == 'singaporean' || $(this).val() == 'singaporean_pr') {
        $(".control_singapore").show();
        $(".control_passport_label").html('NRIC');
      } else {
        $(".control_singapore").hide();
        $(".control_passport_label").html('Passport number');
      }
    });

    $('select.control_country_select').on('change', function() {
      if ($(this).val() == 'none_of_above') {
        $("#onlineinvestormodal").modal("show");
      }
    });

    $("#select_buy_for").change(function () {
      $(".control_employment").hide();
      $("#label_buy_for").html($(this).val());
      if ($(this).val() == 'your child' || $(this).val() == 'your legal ward') {
        $("#onlineinvestormodal_warning").modal("show");
        $(".control_option_child").show();
      } else {
        $(".control_option_child").hide();
      }
    });

    $("select#control_buy_for_year").change(function () {
      var buyFor = $("#select_buy_for").val();
      if (buyFor == 'your child' || buyFor == 'your legal ward') {
        if ($(this).val() < '1999') {
          $("#onlineinvestormodal_child").modal("show");
        }
      }
    });

    $("select.control_employment_select").change(function() {
      if ($(this).val() == 'student') {
        $(".control_study").show();
      } else {
        $(".control_study").hide();
      }
      if ($(this).val() == 'student' || $(this).val() == 'homemaker' || $(this).val() == 'retired' || $(this).val() == 'unemployed') {
        $(".control_occupation").hide();
        $(".control_industry").hide();
        $(".control_company").hide();
        $(".control_employment_country").hide();
        $(".control_employment_city").hide();

        $(".control_salary").show();
        $("input.control_salary").val("0");
      } else {
        $(".control_occupation").show();
        $(".control_industry").show();
        $(".control_company").show();
        $(".control_employment_country").show();
        $(".control_employment_city").show();

        $(".control_salary").show();
        $("input.control_salary").val("");
      }
      if ($(this).val() == 'self-employed') {
        $(".control_company_label").html("Company");
      }
      if ($(this).val() == 'salaried') {
        $(".control_company_label").html("Employer")
      }
    })
</script>
