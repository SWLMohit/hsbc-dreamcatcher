<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div class="main">

    <div class="breadcrumbs">
        <nav class="breadcrumbs__inner container" aria-label="Breadcrumb">
            <a href="#">Home</a>
            <a href="#">Our Plans</a>
            <a href="#">HSBC Insurance OnlineInvestor</a>
            <a href="#" style="text-decoration: underline;" aria-current="page">HSBC Insurance OnlineInvestor application</a>
        </nav>
    </div>

    <article>
        <section class="masthead" style="background-image: url(<%= request.getContextPath() %>/assets/images/insurance-onlineinvestor/mast.jpg)">
            <div class="masthead__container container">
            </div>
        </section>

        <section class="onlineinvestor">
            <div class="container">
                <h1>HSBC Insurance OnlineInvestor application</h1>


                <div class="onlineinvestor__breadcrumb">
                    <a href="../onlineinvestor-application-step1a/">Eligibility Check</a>
                    <a href="#" class="current">Your plan</a>
                    <a href="#" class="disabled">Your details</a>
                    <a href="#" class="disabled">Payment</a>
                </div>


                <div class="row">
                    <div class="col-md-8">
                        <form>
                          <div class="onlineinvestor_carousel">
                              <div class="form-item item3 active">
                                <fieldset>
                                  <legend>Help us understand your investment needs</legend>
                                  <p>
                                    Please confirm your monthly investment
                                    <button type="button" class="help-tooltip" tabindex="0" data-container="body"

                                    data-toggle="popover" data-placement="right" label="help"
                                    aria-label="The monthly investment displayed was captured during the &lsquo;Customer Knowledge Assesment&rsquo; process. You can choose to revise it here."
                                    data-content="The monthly investment displayed was captured during the &lsquo;Customer Knowledge Assesment&rsquo; process. You can choose to revise it here.">
                                        <i class="icon icon-circle-help-solid"></i>
                                    </button>:
                                  </p>
                                  <div class="confirm_investment">
                                      <input class="form-control programmatic-range-input" type="programmatic-range-number" value="S$1500">
                                      <input type="programmatic-range" min="200" max="3000" step="50" value="1500">
                                      <div class="clearfix">
                                          <small class="min pull-left">S$200</small>
                                          <small class="max pull-right">S$3,000</small>
                                      </div>
                                      <div class="article__leader__main__btnset">
                                          <a class="btn btn-default" href="#">Recalculate</a>
                                      </div>
                                  </div>
                                </fieldset>

                                <fieldset>
                                    <legend>Snapshot of Benefit Illustration</legend>
                                    <div class="onlineinvestor__table__wrap">
                        <table class="onlineinvestor__table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>5 years</th>
                                            <th>10 years</th>
                                            <th>15 years</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="green">
                                            <th colspan="4"><strong>Death Benefit</strong></th>
                                        </tr>
                                        <tr class="gray50">
                                            <th colspan="4"><strong>Projected at 4.0% investment return</strong></th>
                                        </tr>
                                        <tr class="gray10">
                                            <td>Non-guaranteed (S$)</td>
                                            <td>900</td>
                                            <td>7,800</td>
                                            <td>21,800</td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>Total (S$)</td>
                                            <td>53,400</td>
                                            <td>112,800</td>
                                            <td>21,800</td>
                                        </tr>
                                        <tr class="gray50">
                                            <th colspan="4"><strong>Projected at 8.0% investment return</strong></th>
                                        </tr>
                                        <tr class="gray10">
                                            <td>Non-guaranteed (S$)</td>
                                            <td>7,400</td>
                                            <td>31,600</td>
                                            <td>91,600</td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>Total (S$)</td>
                                            <td>59,900</td>
                                            <td>140,600</td>
                                            <td>241,800</td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr class="green">
                                            <th colspan="4"><strong>Surrender Value</strong></th>
                                        </tr>
                                        <tr class="gray50">
                                            <th colspan="4"><strong>Projected at 4.0% investment return</strong></th>
                                        </tr>
                                        <tr class="gray10">
                                            <td>Non-guaranteed (S$)</td>
                                            <td>900</td>
                                            <td>7,800</td>
                                            <td>21,800</td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>Total (S$)</td>
                                            <td>53,400</td>
                                            <td>112,800</td>
                                            <td>21,800</td>
                                        </tr>
                                        <tr class="gray50">
                                            <th colspan="4"><strong>Projected at 8.0% investment return</strong></th>
                                        </tr>
                                        <tr class="gray10">
                                            <td>Non-guaranteed (S$)</td>
                                            <td>7,400</td>
                                            <td>31,600</td>
                                            <td>91,600</td>
                                        </tr>
                                        <tr class="gray5">
                                            <td>Total (S$)</td>
                                            <td>59,900</td>
                                            <td>140,600</td>
                                            <td>241,800</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </div><!-- /.onlineinvestor__table__wrap -->
                                    <dl class="onlineinvestor__questionnaire">
                                        <dt>
                                          Preferred payment frequency
                                        </dt>
                                        <dd class="form-group">
                                                <div class="form-select_wrap">
                                        <select class="form-control">
                                            <option disabled selected>Please select&hellip;</option>
                                            <option>Monthly</option>
                                            <option>Quarterly</option>
                                            <option>Semi-Annually </option>
                                            <option>Annually</option>
                                      </select></div>
                                        </dd>
                                    </dl>
                                </fieldset>
                                <div class="article__leader__main__btnset">
                                    <button type="button" class="btn btn-red" onclick="openPage('../onlineinvestor-application-step2d/')">Continue</button>
                                    <button type="button" class="back_btn" onclick="openPage('../onlineinvestor-application-step2b/')">Back</button>
                                </div>

                              </div><!-- /.item -->
                          </div>
                        </form>
                    </div>
                </div><!-- /.row -->



            </div><!--/ container -->
        </section>

    </article>


    <div class="modal fade" id="onlineinvestormodal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Sorry!</h2>
                </div>
                <div class="modal-body">
                    <p>Based on your response(s) we are unable to proceed with your application.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red">Back</button>
                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>

<script type="text/javascript">
    $(document).ready(function(){
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);
    });

    $(document).on("click", ".confirm_investment a", function(e){
      $('html, body').stop().animate({
          'scrollTop': $(this).parents(".confirm_investment").offset().top -15
      }, 700);
    });


    $(document).ready(function() {
        deselectAllRadio();
        $("radio_range").show();
        $(".totalbox").value = '0';
    });

    function deselectAllRadio() {
        $(".radio_range .radio_range__item").removeClass("selected");
    }

    function selectRadio(num){
        deselectAllRadio();
        $("#optionsRadios" + num).addClass("selected");
    }

    $(document).on("click", "[data-show]", function(e){
        $($(this).data("hide")).addClass("hide");
        $($(this).data("show")).removeClass("hide");
    });

    function openPage(pageURL)
    {
        window.location.href = pageURL;
    }

    function updateTotal(){
        var total = 0;
//        var oldVal = parseInt($(".totalbox").val()) || 0;
        var boxVal = 0;

        $(".numbox").each(function(i, obj) {
            boxVal =  parseInt(obj.value);

            total =  boxVal + total;

            if (!isNaN(total) && !isNaN(boxVal)){
                if (total > 100) {
                    $(this).parent('td').addClass("has-error");
                    return false;
                }else{
                    if($(this).parent('td').hasClass("has-error")){
                        $(this).parent('td').removeClass("has-error");
                    }
                    $(".totalbox").val(total);
                }
            }
        });
    }

    function validate(e){
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
</script>
