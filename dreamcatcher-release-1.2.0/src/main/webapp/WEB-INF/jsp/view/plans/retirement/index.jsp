<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/includes/header.jsp" flush="true"/>

<div>
    <h1>You want to [Retirement Dream]</h1>
    <p>
        Be a step closer to achieving your aspirations by answering a series of questions.
    </p>
</div>

<form class="retirementForm">
    <h3>Your Info</h3>
    <section>
        <label>How do we address you?</label>
        <input name="name" value="${name}"/>

        <label>How old are you?</label>
        <input name="age" value="${age}"/>

        <label>At what age would you like to retire?</label>
        <input name="retirement_age" value="${retirement_age}"/>

    </section>

    <h3>Income</h3>
    <section>
        <label>What is your monthly income?</label>
        <input name="income" value="${income}"/>

        <label> How much do you spend each month?</label>
        <input name="monthly_spending" value="${monthly_spending}"/>

    </section>

    <h3>Liabilities</h3>
    <section>
        <label>How much are your outstanding liabilities?</label>
        <input name="liabilities" value="${liabilities}"/>

        <label>How much are your existing investments worth?</label>
        <input name="existing_investments" value="${existing_investments}"/>

    </section>

    <h3>Interest</h3>
    <section>
        <label>What is the expected rate of return on your savings?</label>
        <input name="expected_returns" value="${expected_returns}"/> % p.a.

        <label>What is your expected inflation rate?</label>
        <input name="expected_inflation" value="${expected_inflation}"/> % p.a.

    </section>

    <h3>Retirement Details</h3>
    <section>
        <label>How much will your monthly expenses ? be during retirement?</label>
        <input name="retirement_monthly_expenses" value="${retirement_monthly_expenses}"/>

        <label>If you're planning for any big ticket expenses, how much would you spend?</label>
        <input name="expected_big_ticket_expenses" value="${expected_big_ticket_expenses}"/>

    </section>


    <input type="hidden" name="submit_form" value="true"/>
</form>
<div>
    <h1>Did you know?</h1>
    <p>
        A high priority for
    </p>
    <div>
        51%
        <p>
            of Singaporeans in their 30s is having enough money to live comfortably.
        </p>
    </div>
    <div>
        53%
        <p>
            of working adults who are financially informed feel they will enjoy a happy retirement.
        </p>
    </div>
</div>
<div class="resultsView">
    <div>
        <div>
            Your Results

            Careful, <span class="js-name"><c:out value="${name}"/></span>. You're off the mark.

            Enjoy your retirement <span class="js-name"><c:out value="${name}"/></span>"/>!

            You'll have a <span>S$<span class="js-amount"><fmt:formatNumber value="${shortfall}"
                                                                            pattern="#,###,###"/></span></span> in
            covering
            your
            expenses over <span class="js-years-in-retirement"><c:out value="${years_in_retirement}"/></span> years in
            retirement.
        </div>
        <div>
            You are currently a
            <span class="js-age"><c:out value="${age}"/></span>
            year old
        </div>
        <div>
            Your plan to retire when you are <span class="js-retirement-age"><c:out value="${retirement_age}"/></span>
            years
            old
        </div>
        <div>
            You are planning for retirement in
            <span class="js-years-to-retirement"><c:out value="${years_to_retirement}"/></span>
            years time
        </div>
    </div>

    <div>
        Rate of return <span>?</span>
        <div class="js-rate-of-return-control"
             data-min="0"
             data-max="100"
             data-step="1"
             data-initial-value="0"
             data-bind-value=".js-retirement-return">
            <div class="ui-slider-handle"><span class="js-expected-return">${expected_returns}</span>% p.a.</div>
        </div>
    </div>

    <div>
        Planned retirement age
        <div class="js-retirement-age-control"
             data-min="45"
             data-max="83"
             data-initial-value="45"
             data-bind-value=".js-retirement-age">
            <div class="ui-slider-handle"><span class="js-retirement-age">${retirement_age}</span></div>
        </div>
    </div>
    <div>
        Monthly expenses <span>?</span> during retirement
        <div class="js-monthly-expenses-control"
             data-min="0"
             data-max="20000"
             data-step="100"
             bind-value="js-monthly-expenses"
             data-initial-value="0">
            <div class="ui-slider-handle">S$<span class="js-retirement-expenses"><fmt:formatNumber
                    value="${retirement_monthly_expenses}" pattern="#,###,###"/></span></div>
        </div>
    </div>
    <div>
        All statistics retrieved from The Future of Retirement - Generations and journeys, Singapore Reports.
    </div>
    <div>
        Get your results:
        <button>Email</button>
        <form method="post">
            <!-- sample form order -->
            <input type="hidden" name="retirement_age_slider" class="js-retirement-age" value="${retirement_age}">
            <input type="hidden" name="expected_returns_slider" class="js-rate-of-return" value="${expected_returns}">
            <input type="hidden" name="retirement_monthly_expenses_slider" class="js-monthly-expenses"
                   value="${retirement_monthly_expenses}">

            <input type="hidden" name="print" value="false">
            <button>Print</button>
        </form>
    </div>
</div>
<div>
    Explore more:
    <a href="#">Protection Calculator</a>
    <a href="#">Education Calculator</a>
    <a href="#">Savings Calculator</a>
</div>

<jsp:include page="/WEB-INF/includes/footer.jsp"/>
<script>
    $(document).ready(function () {
        $('.resultsView').hide();


        var form = $(".retirementForm").show();
        form.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            onStepChanging: function (e, currentIndex, newIndex) {

                //allow going back without validation
                if (currentIndex > newIndex) {
                    return true;
                }

                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (e, currentIndex) {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (e, currentIndex) {
                var form = $(this);

                // Submit form input
                form.submit();
            }
        }).validate({ // initialize the jquery validation plugin
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            rules: {
                name: {
                    required: true,
                    minWords: 1,
                    humanNames: true
                },
                age: {
                    required: true,
                    maxlength: 2,
                    digits: true,
                    range: [16, 62]
                },
                retirement_age: {
                    required: true,
                    maxlength: 2,
                    digits: true,
                    range: [40, 85]
                },
                income: {
                    required: true,
                    digits: true
                },
                monthly_spending: {
                    required: true,
                    digits: true
                },
                liabilities: {
                    required: true,
                    digits: true
                },
                existing_investments: {
                    required: true,
                    digits: true
                },
                expected_returns: {
                    required: true,
                    digits: true
                },
                expected_inflation: {
                    required: true,
                    digits: true
                },
                retirement_monthly_expenses: {
                    required: true,
                    digits: true
                },
                expected_big_ticket_expenses: {
                    required: true,
                    digits: true
                }
            },
            submitHandler: function (form) {
                //default submit handler will be executed
            }
        });


        $('.retirementForm').submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "",
                data: {
                    "name": $("input[name='name']").val(),
                    "age": $("input[name='age']").val(),
                    "retirement_age": $("input[name='retirement_age']").val(),
                    "income": $("input[name='income']").val(),
                    "monthly_spending": $("input[name='monthly_spending']").val(),
                    "liabilities": $("input[name='liabilities']").val(),
                    "existing_investments": $("input[name='existing_investments']").val(),
                    "expected_returns": $("input[name='expected_returns']").val(),
                    "expected_inflation": $("input[name='expected_inflation']").val(),
                    "retirement_monthly_expenses": $("input[name='retirement_monthly_expenses']").val(),
                    "expected_big_ticket_expenses": $("input[name='expected_big_ticket_expenses']").val(),
                    "submit_form": "true"
                },
                dataType: 'text',
                async: false,
                success: function (resultData) {

                    var json = JSON.parse(resultData);
                    updateResultFields(json);

                }
                ,
                error: function (xhr, status, error) {

                    alert("Something went wrong " + xhr.responseText.toString() + status.toString() + error.toString());
                }
            });
        });

    });

    function updateResultFields(json) {

        var shortfall = json.shortfall;

        //update the page
        $(".js-name").text(json.name);
        $(".js-age").text(json.age);
        $(".js-amount").text(parseFloat(shortfall).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,").toString());
        $(".js-years-in-retirement").text(json.yearsInRetirement);
        $(".js-retirement-age").text(json.retirementAge);
        $(".js-years-to-retirement").text(json.yearsToRetire);

        //update the slider
        $(".js-rate-of-return-control").resultsSlider('value', json.expectedReturns);
        $(".js-retirement-age-control").resultsSlider('value', json.retirementAge);
        $(".js-monthly-expenses-control").resultsSlider('value', json.retirementMonthlyExpenses);
        $(".js-expected-return").text(json.expectedReturns);
        $(".js-retirement-age").text(json.retirementAge);
        $(".js-retirement-expenses").text(json.retirementMonthlyExpenses);

        //update hidden slider values
        $("input[name='retirement_age_slider']").val(json.retirementAge);
        $("input[name='expected_returns_slider']").val(json.expectedReturns); //double
        $("input[name='retirement_monthly_expenses_slider']").val(json.retirementMonthlyExpenses);

        $('.resultsView').show();
        $('.retirementForm').hide();
    }
</script>

