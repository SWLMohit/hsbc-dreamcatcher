// Copyright 2014-2015 Twitter, Inc.
// Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement('style')
    msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'))
    document.querySelector('head').appendChild(msViewportStyle)
}
$(function () {
    var nua = navigator.userAgent
    var isAndroid = (nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1 && nua.indexOf('Chrome') === -1)
    if (isAndroid) {
        $('select.form-control').removeClass('form-control').css('width', '100%')
    }
})

$(document).ready(function() {
    setupMobileNav();
    setupDreamsSearch();
    setupFaqSearch();
    setupContactUsForm();
    setupFormInputGroupFocusUI();
    setupPostalCodeLookup();
    selectBoxAria();
    selectBoxLabels();
});

function mobileMenuKeyTrap(container) {
    
    // Find the modal and its overlay
    var $modal = container;

    // Listen for and trap the keyboard
    $modal.on('keydown', trapTabKey);

    // Find all focusable children
    var focusableElements = $('a', $modal);
    // Convert NodeList to Array
    focusableElements = Array.prototype.slice.call(focusableElements);

    var firstTabStop = focusableElements[0];
    var lastTabStop = focusableElements[focusableElements.length - 1];

    // Focus first child
    setTimeout(function () {            
        firstTabStop.focus();
    }, 100);

    function trapTabKey(e) {
        // Check for TAB key press
        if (e.keyCode === 9) {

            // SHIFT + TAB
            if (e.shiftKey) {
                if (document.activeElement === firstTabStop) {
                  e.preventDefault();
                  lastTabStop.focus();
                }

            // TAB
            } else {
                if (document.activeElement === lastTabStop) {
                    e.preventDefault();
                    firstTabStop.focus();
                }
            }
        }

    }
}

function setupMobileNav() {
    // Hamburger Button
    $('button.navbar-toggle').click(function() {
        $('.nav-overlay').css({'z-index': '200'}).show();
        $('.nav-overlay').after('<div class="navbarrier"></div>');
        $('body').css({'overflow': 'hidden'});

        // Set aria-hidden from 'true' to 'false'
        $('button.navbar-toggle').hide();
        $('.nav-overlay').attr('aria-hidden','false');
        $('body').attr('tabindex','-1');

        $('.nav-overlay .overlay-content-panel:first').attr('tabindex','0');

        mobileMenuKeyTrap($('.nav-overlay .overlay-content-panel:first'));

        $('.navbar, body > section, div.main, .calculator__wrap, div.container, footer.container, .header-block').attr({'aria-hidden':'true', 'tabindex': '-1'});

        return false;
    });

    // Close Button
    $('.overlay-content-panel-header > div:nth-child(2) a').click(function() {
        $('.nav-overlay').css({'z-index': '1'}).hide();
        $('body').css({'overflow': 'initial'});
        $('.navbarrier').remove();
        $('.nav-overlay .overlay-content:first').css({'margin-left': '0'});

        // Set aria-hidden from 'false' to 'true'
        $('button.navbar-toggle').show().focus();
        $('.nav-overlay').attr('aria-hidden','true');
        $('body').attr('tabindex','0');
        $('button.navbar-toggle').focus();

        $('.nav-overlay .overlay-content-panel').attr('tabindex','-1');
        $('.navbar, body > section, div.main, .calculator__wrap, div.container, footer.container, .header-block').removeAttr('aria-hidden tabindex');


        return false;
    });

    // Arrow right/next button
    $('.nav-overlay .menu li a:nth-child(2)').click(function() {
        $('.nav-overlay .overlay-content-panel:not(:first)').hide();
        $('.nav-overlay .overlay-content-panel[data-panel-id=' + $(this).data('panel-id') + ']').show();
        $('.nav-overlay .overlay-content:first').animate({
            'margin-left': '-100vw'
        });

        $('.nav-overlay .overlay-content-panel[data-panel-id=' + $(this).data('panel-id') + ']').attr('tabindex','0');
        $('.nav-overlay .overlay-content-panel:first').attr('tabindex','-1');

        mobileMenuKeyTrap($('.nav-overlay .overlay-content-panel[data-panel-id=' + $(this).data('panel-id') + ']'));
        
        return false;
    });

    // Arrow left/prev button
    $('.nav-overlay .overlay-content-panel:not(:first) .overlay-content-panel-header > div:first-child a').click(function() {
        $('.nav-overlay .overlay-content:first').animate({
            'margin-left': '0'
        });

        $('.nav-overlay .overlay-content-panel:not(:first)').attr('tabindex','-1');
        $('.nav-overlay .overlay-content-panel:first').attr('tabindex','0');

        mobileMenuKeyTrap($('.nav-overlay .overlay-content:first'));
        
        return false;
    });
}


function setupDreamsSearch() {
    $.widget('ui.autocomplete', $.ui.autocomplete, {
        _resizeMenu: function() {
            this.menu.element.outerWidth($('#dreams-search-keywords').outerWidth());
        }
    });

    $('#dreams-search-keywords').autocomplete({
        source: '/your-dreams/search',
        search: function() {
            $('.dream-search-result-actions').removeClass('success');
            $('#dreams-search-keywords').data('selected', false);
        },
        response: function(event, ui) {
            if (ui.content.length > 0) {
                $('.dream-search-result-actions').removeClass('error');
                $('.dream-search-result-actions').siblings('.form-group').find('.input-group').removeClass('error');
            } else {
                $('.dream-search-result-actions').addClass('error');
                $('.dream-search-result-actions').siblings('.form-group').find('.input-group').addClass('error');
            }
        },
        focus: function(event, ui) {
            var tokens = ui.item.value.split(',');
            $(event.target).data('category', tokens[0]);
            $(event.target).data('id', tokens[1]);
            $(event.target).data('image', tokens[2]);
            $(event.target).val(ui.item.label);
            $('.header-page, .dream-search, .dream-search-inner').attr('data-dreams-image-id', tokens[0] + '-' + tokens[1]);
            return false;
        },
        select: function(event, ui) {
            $(event.target).val(ui.item.label);
            $('.dream-search-result-actions').addClass('success').removeClass('error');
            $('.dream-search-result-actions').siblings('.form-group').find('.input-group').removeClass('error');
            $('#dreams-search-keywords').data('selected', true);
            $('#dreams-search-keywords').data('category', $(event.target).data('category'));
            return false;
        },
        close: function() {
            if ($('#dreams-search-keywords').data('selected') != true) {
                $('.dream-search-result-actions').addClass('error').removeClass('success');
                $('.dream-search-result-actions').siblings('.form-group').find('.input-group').addClass('error');
            }
        }
    });
    // $('.dream-suggestion-label a').click(function() {
    //     $('#dreams-search-keywords').val($(this).data('term')).autocomplete('search');
    //     return false;
    // }); 
    function submitCalculatorForm(category,searchTextValue){
	//location.href = '/yourdreams/' + category + 'calculator';
        var form = document.createElement('form');
        form.method="POST";
        form.action='/your-dreams/' + category + '-calculator/';
        var inputSearch = document.createElement('input');
        inputSearch.type="hidden";
        inputSearch.name="searchText";
        inputSearch.value = searchTextValue;
        $(form).append(inputSearch);
        $(document.getElementsByTagName('body')[0]).append(form);
        form.submit();
    }
	
	var flagYouMean = false;
    $('[data-action=goto-calculator-btn]').click(function(event){
		var domEle = event.target;
		var category = domEle.getAttribute("data-category");
		var searchTextValue = domEle.getAttribute("data-term");
		if (!flagYouMean) {
			flagYouMean = true;
			TMS.trackEvent({
				'event_category': 'Whats Your Dream Widget',
				'event_action': 'Lets Begin',
				'event_content': searchTextValue
			});
			setTimeout(function () {
				submitCalculatorForm(category,searchTextValue);
				flagYouMean = false;
			}, 2000);
		}
	});

	var flagWhatIsYourDream = false;
    $('[data-action=goto-calculator]').click(function() {
		var category = $('#dreams-search-keywords').data('category');
		var text = $('#dreams-search-keywords').val();
		if (!flagWhatIsYourDream) {
			flagWhatIsYourDream = true;
			TMS.trackEvent({
				'event_category': 'Whats Your Dream Widget',
				'event_action': 'Lets Begin',
				'event_content': text
			});
			setTimeout(function () {
				if (category !== undefined) {
					var searchTextValue = document.getElementById("dreams-search-keywords").value;
					submitCalculatorForm(category,searchTextValue);
				} else {
					$('.dream-search-result-actions').addClass('error');
					$('.dream-search-result-actions').siblings('.form-group').find('.input-group').addClass('error');
				}
				flagWhatIsYourDream = false;
			}, 1500);
		}
        
        return false;
    });


    $('footer a').click(function() {
        TMS.trackEvent({
            "event_type": "Footer Navigation",
            "event_category" : "click",
            "event_action" : "link-click"
        });
    });

    $('.footer-utility a[href="/eclaims"]').click(function() {
        TMS.trackEvent({
            "event_type": "Claim",
            "event_category" : "Click",
            "event_action" : "Claim click"
        });
    });

    $('.home-special a').click(function() {
        var productName = $(this).data('product-name');
        TMS.trackEvent({
            "event_category": "Sub-Banners:" + productName,
            "event_action" : "CTR",
            "event_content": "FindOutMore"
        });
    });

    $('.dream-search-result-actions .primary-btn-slate').click(function() {
        var kw = $('#dreams-search-keywords').val();
        TMS.trackEvent({
            "event_category": "widget",
            "event_action" : "Let’s Begin",
            "event_content": kw
        });
    });

    $('.life-insurance-container a').click(function() {
        var linkName = $(this).data('link-name');
        var productName = $(this).data('product-name');
        TMS.trackEvent({
            "event_category": "widget",
            "event_action" : linkName,
            "event_content": productName
        });
    });
}

function setupFaqSearch() {
    $('form#faq-search').submit(function() {
        $.ajax({
            data: $(this).serialize(),
            url: '/faq/search',
            success: function(html) {
                if (html.trim().length > 0) {
                    $('.faq-search-results').html(html);
                    $('.question-result').show();
                    $('#faq-search .error-message').hide();
                    $('#faq-search .input-group').removeClass('error');
                    if (!$('.question-result div.collapse').hasClass('in')) {
                        $('.question-result [data-toggle=collapse]').click();
                    }
                    if ($('.question-result > a').hasClass('collapsed')) {
                        $(this).attr('aria-label', 'open');
                    } else {
                        $(this).attr('aria-label', 'close');
                    }
                } else {
                    $('#faq-search .error-message').show();
                    $('#faq-search .input-group').addClass('error');
                }
            }
        });
        return false;
    });
    $('form#faq-search .input-group .input-group-addon').click(function() {
        $('form#faq-search').submit();
    });
    $('.faq-question').click(function() {
        $('.faq-question').not($(this)).parent().addClass('collapsed');
        $(this).parent().toggleClass('collapsed');

        if ($(this).parent().hasClass('collapsed')) {
            $(this).find('a').attr({'aria-label': 'open', 'aria-expanded': 'false'});
        } else {
            $(this).find('a').attr({'aria-label': 'close', 'aria-expanded': 'true'});
        }

        return false;
    });
    $('.faq-search-results > li.collapsed').each(function() {
      var btnFAQ = $(this).children('.faq-question');

      btnFAQ.click(function() {
        if ( btnFAQ.parent().attr('class') === '' ) {
          $('.faq-question').attr('aria-expanded','true');
          $('.faq-question').parent().siblings('li.collapsed').children('.faq-question').attr('aria-expanded','false');
        }
        else if ( btnFAQ.parent().attr('class') === 'collapsed' ) {
          btnFAQ.attr('aria-expanded','false');
        }
      });
    });
}

function countCharacters(form, charsRemaining, commentsCharLimit) {

    if (charsRemaining < 0) {
        $('#form-comments').val($('#form-comments').val().substring(0, commentsCharLimit));
        charsRemaining = 0;
    }
    form.find('.comments-char-count').text(charsRemaining + (charsRemaining == 1 ? ' character' : ' characters') + ' remaining');
    form.find('.comments-char-count').toggleClass('error', (charsRemaining < 0));
}

function setupContactUsForm() {
    if ($('#contact-us-form').length) {
        var form = $('#contact-us-form');
        var commentsCharLimit = parseInt(form.find('[name=comments]').data('char-limit'));
        var charsRemaining = commentsCharLimit - $('textarea[name=comments]').val().length;


        if ( $('[name=comments]').val() < 1 ) {
            form.find('.comments-char-count').text(commentsCharLimit + ' characters remaining');

        } else {
            countCharacters(form, charsRemaining, commentsCharLimit);
        }

        form.find('textarea[name=comments]').keyup(function() {
            var charsRemaining = commentsCharLimit - $(this).val().length;
            countCharacters(form, charsRemaining, commentsCharLimit);
        });

        form.find('a[data-action=submit]').click(function() {
            setCookie('contactus', 'true');
            form.submit();
            return false;
        });
    }
}

function setupFormInputGroupFocusUI() {
    $('.form-control').focus(function() {
        $(this).siblings('.input-group-addon').addClass('focus');
    });
    $('.form-control').blur(function() {
        $(this).siblings('.input-group-addon').removeClass('focus');
    });

    if ( $(window).outerWidth() > 600 ) {
        $('.form-control').hover(function() {
            $(this).siblings('.input-group-addon').addClass('hover');
        }, function() {
            $(this).siblings('.input-group-addon').removeClass('hover');
        });
    }

    $('label').focus(function() {
        $(this).parent().next().find('.input-group-addon').addClass('focus');
    });
    $('label').blur(function() {
        $(this).parent().next().find('.input-group-addon').removeClass('focus');
    });

    if ( $(window).outerWidth() > 600 ) {
        $('label').hover(function() {
            $(this).parent().next().find('.input-group-addon').addClass('hover');
        }, function() {
            $(this).parent().next().find('.input-group-addon').removeClass('hover');
        });
    }
}

var cachedPostal = '';
function setupPostalCodeLookup() {
    $('input[data-lookup="postalCode"]').on('keyup blur change', function(e) {
        if ($(this).val().length == 6) {
            var $this = $(this);
            var curPostal = $this.val();


            // only do post to retrieve address if current postal code is
            // not the same as the previously entered postal code
            if (curPostal.length == 6 && cachedPostal !== curPostal) {
              cachedPostal = this.value;

                $.post('/lookup', { postalCode: $this.val() }, function(data) {
                    if (data.buildingNumber) {
                        $this.closest('.row').find('input[data-lookup="buildingNumber"]').val(data.buildingNumber);
                    }
                    if (data.buildingName) {
                        $this.closest('.row').find('input[data-lookup="buildingName"]').val(data.buildingName);
                    }
                    if (data.streetName) {
                        $this.closest('.row').find('input[data-lookup="streetName"]').val(data.streetName);
                    }
                });
            }
        }
    });
}

function setCookie(key, value, second) {
	if (second === undefined) {
		second = 60;
	}
	var expires = new Date();
	expires.setTime(expires.getTime() + (second * 1000));
	document.cookie = key + '=' + value + ';path=/' + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
	var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
	return keyValue ? keyValue[2] : null;
}

function deleteCookie(key) {
	setCookie(key, '', -1);
}


function selectBoxAria() {

    var listID = 0;

    // HSBCIDCU-732
  $('.selectBox-dropdown-menu').each(function() {
    var _this = $(this),
        option = _this.find('li'),
        optionID = 0,
        desc = null;


    $('li', _this).each(function() {
        $(this).attr('id', listID + '_' + optionID);
        optionID++;
    });


    if ($('li.selectBox-selected').length ) {
        desc = $('li.selectBox-selected', _this).attr('id');
    } else {
        desc = _this.find('li').first().attr('id');
    }


    // Adding role and tabindex
    _this.attr({
      'role':'listbox',
      'tabindex': 0,
      'aria-activedescendant' : desc
    });

    // Adding role on list/option
    option.attr('role','option');

    listID++;

  });

   $(document).on('keydown', 'a.selectBox', function(){

        var getUl = $('.selectBox-dropdown-menu.selectBox-options-bottom');

        if ($('li.selectBox-selected', getUl).length < 1 ) {
            $('li:first', getUl).addClass('selectBox-hover');
        }
    });
}

function selectBoxLabels() {

    //added selectbox aria-labelledby as per HSBCIDCU-742
    setTimeout(function() {
            

        $('a.selectBox-dropdown').each(function() {
            var $elem = $(this);
            if ($('[data-current-step="11.0"]').length) {
                var getLabel = $elem.closest('.tax-country-wrapper').find('label').attr('id');
            } else {
                var getLabel = $elem.closest('.row').find('label').attr('id');
            }

            if (getLabel) {
                $elem.attr({
                    'aria-labelledby' : getLabel,
                    'role' : "button"
                });
            }
        });

    }, 3000);

}