
$(document).ready(function(){
    $('.help-block-secondary').hide();
    if ($(".onlineinvestor_carousel").length) {
        $('html, body').stop().animate({
            'scrollTop': $(".onlineinvestor_carousel").offset().top -15
        }, 700);
    }
});

function displayErrorMessage() {
    setTimeout(function () {

        var errorCount = $('.help-block:visible').length;

        if (errorCount > 0 && $('.buynow-form').length) {

            if ( $('.aria-error-message').length ) {
                $('.aria-error-message').removeAttr('aria-live').text( + errorCount + ' errors found');
            } else {
                $('.help-block:visible').closest('form').prepend('<div class="visuallyhidden focusable aria-error-message" aria-atomic="true" tabindex="-1">' + errorCount + ' errors found</div>');
            }


            

            setTimeout(function () {
                $(document).find('.aria-error-message').attr('aria-live', 'assertive').focus();
            }, 400);
        }

    }, 500);
}

var formEclaims = $('[data-form-name="eclaims"]'),
    typeEclaims = formEclaims.data('form-type') ? formEclaims.data('form-type') : 'Online insurance claims',
    dataEclaims = formEclaims.find('input, select, textarea, .form-select_wrap, .selectBox'),
    startEclaims = false;

if (formEclaims.length > 0) {
    $('.form-select_wrap').click(function () {
        if (!startEclaims) {
            TMS.trackEvent({
                'event_category': 'Claim Form',
                'event_action':   'Start',
                'event_form':     'Form Start',
                'event_content':  typeEclaims
            });
            startEclaims = true;
        }
    });
    dataEclaims.focus(function () {
        if (!startEclaims) {
            TMS.trackEvent({
                'event_category': 'Claim Form',
                'event_action':   'Start',
                'event_form':     'Form Start',
                'event_content':  typeEclaims
            });
            startEclaims = true;
        }
    });
}

$(document).on('click', 'a[data-file-input]', function() {
    $(this).parent().find('input[type=file]').click();
    return false;
});


$(document).on('change', 'input[type=file]', function() {
    var filename = $(this).val().replace(/^.*[\\\/]/, '');
    $('a[data-file-input="' + $(this).attr('name') + '"]').text('File Selected');// 798 Since eclaims has uppercase S

    if(this.files[0].size > 1000000){
        alert("The file is over 1MB. Please upload a smaller file.");
        this.value = "";
        $('a[data-file-input="' + $(this).attr('name') + '"]').text('Upload');// 798 making text "Upload" again as file is invalid
    }
});


function submitForm() {
    TMS.trackEvent({
        'event_category': 'Claim Form',
        'event_action':   'Finish',
        'event_form':     'Form Finish',
        'event_content':  typeEclaims
    });
    setTimeout(function () {
        $('form.buynow-form').submit();
    }, 2000);
}


// Turn simple date (DD/MM/YYYY) into Javascript Date (Tue Mar 20 2018 15:51:58 GMT+0800 (SGT))
function process(date){
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

// Compare two dates, true if time1 is later
function dateCompare(time1, time2) {
    return new Date(time1) > new Date(time2);
}

// Convert letter based month name into two digits
function getMonthFromString(mon){
    var d = Date.parse(mon + "1, 2012");
    if(!isNaN(d)){
        return new Date(d).getMonth() + 1;
    }
    return -1;
}


//compare last worked and return to work dates against each other
$('input#date-return-work').on('change', function () {
    var date1 = $(this).val();
    var date2 = $('input#date-last-work').val();
    var $parent = $(this).closest('.form-group');
    var compare = dateCompare(process(date1), process(date2));
    var bDayParse = date1.split('/');

    if ( bDayParse[2]==0 || bDayParse[1]==0 || bDayParse[0]==0 ) {
        return;
    }

    if (!compare) {
        $parent.find('.help-block').hide();
        $parent.find('.secondary-error').show();
        $parent.addClass('has-error');
    } else {  
        $parent.find('.secondary-error').hide();
        $parent.removeClass('has-error');
    }

});

//compare date against birthday, also converts Java date into something more useable
$('input[name=accident_date_day], input#date-last-work, input#date-symptoms').on('change', function() {
    var accidentDate = $(this).val();
    var getBday = $('input[name=policy_owner_birthday]').val();
    var $parent = $(this).closest('.form-group');
    var getBdayArray = getBday.split(' ');
    var bDayTranslated = getBdayArray[2] + "/" + "0" + getMonthFromString(getBdayArray[1]) + "/" + getBdayArray[5];
    var compare = dateCompare(process(accidentDate), process(bDayTranslated));

    if ( getBdayArray[2]==0 || getBdayArray[1]==0 || getBdayArray[5]==0 ) {
        return;
    }

    if (!compare) {
        $parent.find('.help-block').hide();
        $parent.find('.secondary-error').show();
        $parent.addClass('has-error');

    } else {
        $parent.find('.secondary-error').hide();
        $parent.removeClass('has-error');
    }

});


function validateThenOpenPage(destPage){
    var hasErrors = false;
    var requiredInputs = $('input[type="text"]:not(.optional), input[type="email"]:not(.optional), input[type="file"]:not(.optional)');
    if (requiredInputs.length){
        requiredInputs.each(function () {
            var $input = $(this);


            // empty input validation
            if($input.attr("type")=='file'){
	            if(!(($input.val() === ''&&$input.attr("value")!='')|| ($input.attr("value")===''&&$input.val() != ''))){
	                showErrorMessageOn($input);
	                hasErrors = true;
	            } else {
                    hideErrorMessageOn($(this));
                }
            }else {
                if ($input.hasClass('date-validate')){

                	if($input.val() === ''){
                		showErrorMessageOn($input);
     	                hasErrors = true;
                	} 
                    
                } else {
                    if($input.val() === ''){
                        showErrorMessageOn($input);
                        hasErrors = true;
                    } else {
                        hideErrorMessageOn($(this));
                    }
                }
            }
            if($input.attr('type')=='file'&&$input.val()!='') {

                var $testVal = $input.val().toLowerCase();

                if(!($testVal.indexOf('.pdf')!=-1||$testVal.indexOf('.jpg')!=-1||$testVal.indexOf('.jpeg')!=-1||$testVal.indexOf('.png')!=-1)){
                    showErrorMessageOn($input);
                    hasErrors = true;
                    $input.val('');
                } else {
                    hideErrorMessageOn($(this));
                }
            }

            if($input.closest('.form-group').hasClass('has-error')){
                hasErrors = true;
            }
        });

        $('.name:visible').each(function(e){

            var val1 = $(this).find('input').first().val();
            var val2 = $(this).find('input').last().val();

            if( (!validateName(val1) && val1.length > 0) || (!validateName(val2) && val2.length > 0)  ){
                showSecondaryErrorMessageOn($(this));
                hasErrors = true;

            } else if (val1.length == 0 || val2.length == 0){
                showErrorMessageOn($(this));
                hasErrors = true;
            }
        });
    }

    $('input[type="file"].optional').each(function(){
        if($(this).val().trim()!=''){
        	var image=$(this).val().toLowerCase();
            if(!(image.indexOf('.pdf')!=-1||image.indexOf('.jpg')!=-1||image.indexOf('.jpeg')!=-1||image.indexOf('.png')!=-1)){
                var formGroup = $(this).closest(".form-group");
                formGroup.addClass("has-error");
                var len=formGroup.find(".help-block").length;
                if(len==0){
                    formGroup.append("<div class='help-block'><small>Please upload a file</small></div>");
                }else{
                    formGroup.find('.help-block:not(.secondary-error)').show();
                }
                $(this).val("");
                hasErrors = true;

            }
        }
    });


    if($('input[type="text"].optional').length){
        $('input[type="text"].optional').each(function () {

            // HSBCIDCU-798:
            // breaks validation as ignores prior required fields validation
            if($(this).val() === ""){
                hideErrorMessageOn($(this));
            //     hasErrors = false;
            }

            if($(this).closest('.form-group').hasClass('has-error')){
                hasErrors = true;
            }

        });
    }



    function radioChecker($this){
        var radioChecked = false;

        if($this.find('input[type="radio"]').hasClass('optional')){
            hideErrorMessageOn($this.find('input[type="radio"]:not(.optional)'));
            return;
        }
        $this.find('input[type="radio"]:not(.optional)').each(function(){
            if($(this).is(':checked')){
                radioChecked = true;
            }

        });
        if(!radioChecked){
            showErrorMessageOn($this.find('input[type="radio"]:not(.optional)'));
            hasErrors = true;
        }else{
            hideErrorMessageOn($this.find('input[type="radio"]:not(.optional)'));
        }

        if($this.parent().hasClass('has-error')){
            hasErrors = true;
        }
    }

    if($('.newradio--inline input[type="radio"]:not(.optional)').length){
        $('.form-group').has('.newradio--inline').each(function () {

            radioChecker($(this));
        });
    }

    if($('.newradio input[type="radio"]:not(.optional)').length){
        $('.form-group').has('.newradio').each(function () {
            radioChecker($(this));
        });
    }

    if($('select:not(.optional)').selectBox().length){
        $('select:not(.optional)').selectBox().each(function(){
            if($(this).prop('selectedIndex') === 0){
                showErrorMessageOn($(this));
                hasErrors = true;
            } else {
                hideErrorMessageOn($(this));
            }

            if($(this).closest('.form-group').hasClass('has-error')){
                hasErrors = true;
            }
        });
    }

    var values = [];
    $('input[id^=id_policy]').each(function () {
        if ($.inArray(this.value, values) >= 0) {
            showSecondaryErrorMessageOn($(this));
            hasErrors = true;
        }else{
            values.push(this.value);
        }
    });

    if(!hasErrors){
        $('option:disabled').attr("disabled", false);
		// $('select').selectBox('refresh');
		console.log('finish');
		TMS.trackEvent({
			'event_category': 'Claim Form',
			'event_action':   'Finish',
			'event_form':     'Form Finish',
			'event_content':  typeEclaims
		});
		setTimeout(function () {
			$('form.buynow-form').submit();
		}, 2000);
	} else {
        displayErrorMessage();
		$('html, body').animate({
            scrollTop: $(".has-error:first").offset().top - 50
        }, 1000);
    }

    setTimeout(function () {
        $('.has-error').each(function () {
            TMS.trackEvent({
                'event_category': 'Claim Form',
                'event_action':   'Error',
                'event_error':    $(this).data('field-name'),
                'event_content':  typeEclaims
            });
        });
    }, 2000);

}

$('.name input').on('blur',function(e){

    var val1 = $(this).closest('.form-group').find('input').first().val();
    var val2 = $(this).closest('.form-group').find('input').last().val();

    if( (!validateName(val1) && val1.length > 0) || (!validateName(val2) && val2.length > 0)  ){
        showSecondaryErrorMessageOn($(this));
    } else if ( (validateName(val1) && val2.length == 0) || (validateName(val2) && val1.length == 0) ) {
        hideSecondaryErrorMessageOn($(this));

    } else {
        hideSecondaryErrorMessageOn($(this));
    }
});

$('input[type="file"]').change(function () {
    if($(this).val() !== ""){
        hideErrorMessageOn($(this));
    }
});

/* eclaims 1 index */
var numAddPolicy = 1;
function addPolicyNum(){

    $('.policy-number-container .policy-num').first().clone(true).appendTo('.policy-number-container').find("input[type='text']").val("");
    $('.policy-number-container .policy-num').last().find('input').addClass("optional").attr('id','id_policy_' + ++numAddPolicy);
    $('.policy-number-container .policy-num').last().removeClass('col-md-offset-1').addClass('col-md-offset-6');
    $('.policy-number-container .policy-num').last().append('<button type="button" class="blocklink remove-policy" style="font-size: 12px;float:right;">Remove this policy</button>' );
    $('.policy-number-container .policy-num').last().find('input').each(function () {
        $(this).val("");
        hideErrorMessageOn($(this));
    });
    $('.policy-number-container .policy-number-field select').each(function () {
        $(this).val("");
        hideErrorMessageOn($(this));
    });
    $('.policy-number-container .policy-num').last().find('input').focus();
}

$(document).on('click', '.remove-policy', function() {
    $(this).parent().remove();
});


$('input:radio[name="policyowner_life_insured"]').change(
    function(){
        if ($(this).val() == 'true') {
            $('.not_policy_owner_wrapper').hide();
            $('.not_policy_owner_wrapper input').addClass("optional");
            hideErrorMessageOn($('.not_policy_owner_wrapper input'));
        }
        else {
            $('.not_policy_owner_wrapper').show();
            $('.not_policy_owner_wrapper input').removeClass("optional");
        }
        hideErrorMessageOn($('.newradio--inline input'));
    });

/* Added function to hide the validation on input */
$('.policy-number-container input').change(function () {
    var val = $(this).val();

    if (val.length > 7) {
        $(this).val(val.substr(0, 8));
        hideErrorMessageOn($(this));
    }else if(val.length != 8){
    	hideSecondaryErrorMessageOn($(this)); // HSBCIDCU-1031
        showErrorMessageOn($(this));
       
    }
});
$('.policy-number-container input').on('input',function () {
    var val = $(this).val();

    if (val.length > 7) {
        $(this).val(val.substr(0, 8));
    }
});

$('.email input').change(function () {
    var $parent = $(this).closest('.form-group');
    var email_value = $(this).val();

    if(email_value == null || email_value == ''){
        $parent.find('.help-block-secondary').hide();

    }
    if(!validateEmail($(this).val()) && email_value !=''){
        $parent.find('.help-block-secondary').show();
        $parent.addClass('has-error');
        $parent.find('.help-block').hide();
    }
    else
    {
        $parent.find('.help-block-secondary').hide();
        $parent.removeClass('has-error');
    }
});

$('.email input').on("focusin",function(){
    $(".email input").closest('.form-group').find('.help-block-secondary').hide();

})

$('.nric input#nric').change(function(){
    if(!validateNRIC($(this).val())){
        showErrorMessageOn($(this));
    }
});

$('.confirm-email input').change(function (e) {
    if($(this).val() !== $('.email input').val()){
        showErrorMessageOn($(this));
    }
});


$('.onlineinvestor .form-group__multi--phone span:first-child input').change(function () {
    var formGroup = $(this).closest('.form-group');
    var countryCode = /^\+(?!0{2})[0-9]{2,3}$/;

    if ($(this).val().length > 4 || $(this).val().length < 2 ) {

        if ($(this).val().length < 2 ) {
            formGroup.find('.help-block-multi.error-1 small').html('Please enter your country code');
        }

        formGroup.addClass('has-error-1');
        formGroup.find('.help-block-multi.error-1').show();
    }
    else if(!$(this).val().match(countryCode)){

        formGroup.find('.help-block-multi.error-1 small').html('Your country code is invalid');
        formGroup.addClass('has-error-1');
        formGroup.find('.help-block-multi.error-1').show();
    }

    else {
        formGroup.removeClass('has-error-1');
        formGroup.find('.help-block-multi.error-1').hide();

    }
});


$('.onlineinvestor .form-group__multi--phone span:last-child input').change(function () {
    var formGroup = $(this).closest(".form-group");
    var phoneNo = /^(?!0{10})[0-9]{8,10}$/;

    if( $(this).val()==null|| $(this).val()==""){
        formGroup.find('.help-block-multi.error-2 small').html('Please enter your phone number');
        formGroup.addClass("has-error-2");
        formGroup.find('.help-block-multi.error-2').show();
    }
    else if(!$(this).val().match(phoneNo)){
        formGroup.find('.help-block-multi.error-2 small').html('Your phone number is invalid');
        formGroup.addClass("has-error-2");
        formGroup.find('.help-block-multi.error-2').show();
    }

    else {
        formGroup.removeClass("has-error-2");
        formGroup.find('.help-block-multi.error-2').hide();

    }
});

//addNewDocument('claimant_relationship_document-container1','claimant_relationship_document')

var maxDocQty = 100;
var claimantNewDocumentNo=1;
function addClaimantNewDocument(containerName,name){
    var containerNameCss="";
    if(containerName.length>0){
        containerNameCss="."+containerName;
    }
    var container=containerNameCss+".document-number-container"+" "+".document-num";
    // clone from the root div "claimant_relationship_document-container1 document-number-container"
    var noOfDocsLocation=containerNameCss+".document-number-container"+" ."+name.slice(0,-2)+"_docs";
    var lastId=$(container).last().attr("id");
    var claimantNum = name.slice(-1);
    claimantNewDocumentNo = parseInt(lastId.replace('document_no_'+claimantNum+'_', ''));
    var lengthOfDoc=$(container).length
    if(lengthOfDoc < maxDocQty){
        $(container).first().clone(true).appendTo(containerNameCss).find("input[type='file']").val("");
        $(container).last().find("input[type='file']")
            .attr("name",name+"_"+ (claimantNewDocumentNo + 1))
            .addClass('optional');
        $(container).last().find("input[type='file']").attr("value","");
        $(container).last().find(".btn-file-upload")
            .text("Upload")
            .attr("data-file-input",name+"_"+ (claimantNewDocumentNo + 1));
        $(container).last()
            .attr("id","document_no_"+ claimantNum + "_"+ (claimantNewDocumentNo + 1))
            .addClass('multiple')
            .append('<button type="button" class="blocklink remove-document" style="font-size: 12px;">Remove above document</button>' );
    }

    // hide addButton if max documents added is reached
    if (lengthOfDoc === (maxDocQty - 1)) {
        // add document button is on the next row following the file upload container
        var $btnContainer = $(containerNameCss+'.document-number-container').closest('.row').next();
        var $addBtn = $btnContainer.find('button.add-document');
        $addBtn.hide();
    }
    $(noOfDocsLocation).val(claimantNewDocumentNo);
}

var documentNo=1;
function addNewDocument(containerName,name){
    var containerNameCss="";
    if(containerName.length>0){
        containerNameCss="."+containerName;
    }
    var container=containerNameCss+".document-number-container"+" "+".document-num";
    var noOfDocsLocation=containerNameCss+".document-number-container"+" ."+name+"_docs";
    var lastId=$(container).last().attr("id");
    documentNo = parseInt(lastId.replace('document_no_', ''));
    var lengthOfDoc=$(container).length
    if (lengthOfDoc < maxDocQty){
        $(container).first().clone(true).appendTo(containerNameCss).find("input[type='file']").val("");
        $(container).last().find("input[type='file']")
            .attr("name", name+"_"+ (documentNo + 1))
            .addClass('optional');
        $(container).last().find("input[type='file']").attr("value","");
        $(container).last().find(".btn-file-upload")
            .text("Upload")
            .attr("data-file-input", name+"_"+ (documentNo + 1))
            .data("file-input", name+"_"+ (documentNo) + 1);
        $(container).last()
            .attr("id", "document_no_"+ (documentNo + 1))
            .addClass('multiple')
            .append('<button type="button" class="blocklink remove-document" style="font-size: 12px;">Remove above document</button>' );
    }

    // hide addButton if max documents added is reached
    if (lengthOfDoc === (maxDocQty - 1)) {
        // add document button is on the next row following the file upload container
        var $btnContainer = $(containerNameCss+'.document-number-container').closest('.document-number-container.row').next();
        var $addBtn = $btnContainer.find('button.add-document');
        $addBtn.hide();
    }
    $(noOfDocsLocation).val(documentNo+1);

}

$(document).on('click', '.remove-document', function() {
    var $docContainer = $(this).closest('.document-number-container'); // cache reference to container before remove, changes this after removal
    var $toRemove = $(this).parent(); // element to remove
    $toRemove.remove();

    var $fileGroups = $docContainer.find('.document-num'); // find all remaining file uploads in group

    // update link ids with new order
    $fileGroups.each(function(i){
        var idx = i + 1; // add 1 because array starts at 0
        var $container = $(this);
        var $input = $container.find('[type="file"]');
        var $link = $container.find('a.btn-file-upload');


        var newId = $container.attr('id').slice(0, -1) + idx;
        $container.attr({
            'id': newId
        });

        var newName = $input.attr('name').slice(0, -1) + idx
        $input.attr({
            'name': newName
        });

        var newDataAttr = $link.attr('data-file-input').slice(0, -1) + idx
        $link.attr({
            'data-file-input': newDataAttr
        });
    });

    // update count of documents in hidden input
    $docContainer.find('input[type=hidden]').val($fileGroups.length);

    // display add documents button if total file upload fields is less than max qty
    if ($fileGroups.length < maxDocQty) {
        var $btnContainer = $docContainer.closest('.row').next();
        var $addBtn = $btnContainer.find('button.add-document');
        $addBtn.css('display','');
    }
});


/* eclaims 1 index */
/* eclaims 1 and 2 step 1 */
var claimantNewNum = 1;
function addNewClaimant(){
    $('.claimant-container .claimant:nth-last-of-type(1)').clone(true).appendTo('.claimant-container').find("input[type='text']").val("");

    // retrieve id of previous claimant container
    var lastId = $('.claimant-container .claimant').last().attr('id');
    claimantNewNum = parseInt(lastId.replace('id_claimant_', ''));

    // increment id of newly created claimant container
    $('.claimant-container .claimant:nth-last-of-type(1)').attr('id','id_claimant_' + ++claimantNewNum);

    // update claimant container title with new id
    $('#id_claimant_' + claimantNewNum + ' .row:nth-of-type(1) fieldset>div:nth-of-type(1)').text("Name of Claimant " + claimantNewNum );

    // append 'remove above claimant' button if not found
    if($('.claimant-container .claimant').last().find(".remove-claimant").length==0){
        $('.claimant-container .claimant:nth-last-of-type(1)').append('<div style="overflow:hidden;margin-bottom:80px;margin-top:-30px;"><button type="button" class="blocklink remove-claimant" style="font-size: 12px;float:right;">Remove above claimant</button></div>' );
    }

    // clear populated values on cloned claimant container
    // clear error messages on cloned claimant container
    $('.claimant-container .claimant:nth-last-of-type(1) input').each(function () {
        if ($(this).attr('type') !== 'hidden') {
            $(this).val("");
            $(this).attr("value","");
        } else {
            var curId = parseInt($(this).val());
            $(this).val(curId + 1);
        }
        hideErrorMessageOn($(this));
    });

    $('.claimant-container .claimant:nth-last-of-type(1) .claimant_address_document-back')
        .attr({
            'onclick': 'addClaimantNewDocument("claimant_address_document-container'+claimantNewNum+'","claimant_address_document_'+claimantNewNum+'")'
        });

    $('.claimant-container .claimant:nth-last-of-type(1) .claimant_relationship_document-back')
        .attr({
            'onclick': 'addClaimantNewDocument("claimant_relationship_document-container'+claimantNewNum+'","claimant_relationship_document_'+claimantNewNum+'")'
        });

    $('.claimant-container .claimant:nth-last-of-type(1) .document-number-container').each(function() {
        // find classname to increment for relationship input
        if (this.className.indexOf('relationship') > -1) {
            var $classArr = $(this.classList).toArray();
            var classMatch;

            for (var i=0,len=$classArr.length; i<len; i++) {
                if ($classArr[i].match(/relationship/)) {
                    classMatch = $classArr[i];
                }
            }
        }

        // find classname to increment for address input
        if (this.className.indexOf('address') > -1) {
            var $classArr = $(this.classList).toArray();
            var classMatch;

            for (var i=0,len=$classArr.length; i<len; i++) {
                if ($classArr[i].match(/address/)) {
                    classMatch = $classArr[i];
                }
            }
        }

        // remove old class and add new incremenet class
        $(this)
            .removeClass(classMatch)
            .addClass(classMatch.slice(0, -1) + claimantNewNum);
    });

    $('.claimant-container .claimant:nth-last-of-type(1) input[type="file"]').each(function () {
        var inputName = $(this).attr('name');
        var idName = $(this).attr('id');

        var inputNameArr = inputName.split('_');

        var trimName = inputNameArr.slice(0, -2).join('_') + '_';
        var trimIdx = inputNameArr[inputNameArr.length - 2];
        var fieldIdx = parseInt(inputNameArr[inputNameArr.length - 1]);


        if ($(this).closest('.document-num').hasClass('multiple')) {
            $(this).closest('.document-num').remove();
        } else {
            $(this).attr('name', trimName + claimantNewNum + '_'+ 1);
            $(this).attr('id', trimName + claimantNewNum + '_'+ 1);
            $(this).closest('.row').find('label').attr('for', trimName + claimantNewNum + '_'+ 1);
            $(this).siblings('a').attr('data-file-input', trimName + claimantNewNum + '_'+ 1);

            $(this).siblings('a').text('Upload');
            // $(this).removeClass("optional");

            // update document num ids
            var docNum = $(this).closest('.document-num');
            var docNumId = docNum.attr('id').slice(0, -3);

            docNum.attr('id', docNumId + claimantNewNum + '_'+ 1);
        }
    });
    
    $('.claimant-container .claimant:nth-last-of-type(1) .add-document').css('display', '');

    $('.claimant-container a.control_relationship_select').last().remove();
    $('.claimant-container a.control_country_select').last().remove();

    $('.claimant-container select.control_relationship_select').last().remove();
    $('.claimant-container select.control_country_select').last().remove();

    $('.claimant-container .claimant:nth-of-type(1) select.control_relationship_select').clone().appendTo($('.claimant:nth-last-of-type(1) .form-select_wrap:nth-of-type(1)').first());
    $('.claimant-container .claimant:nth-of-type(1) select.control_country_select').clone().appendTo($('.claimant:nth-last-of-type(1) .form-select_wrap:nth-of-type(1)').last());

    $('.claimant-container select.control_relationship_select').last().selectBox('create');
    $('.claimant-container select.control_country_select').last().selectBox('create');

    $('.claimant-container .claimant:nth-last-of-type(1) select').selectBox('value','');
}

var claimantNum = 1;
function addClaimant(){
    $('.claimant-container .claimant:nth-last-of-type(1)').clone(true).appendTo('.claimant-container').find("input[type='text']").val("");

    var lastId = $('.claimant-container .claimant').last().attr('id');
    claimantNum = parseInt(lastId.replace('id_claimant_', ''));

    $('.claimant-container .claimant:nth-last-of-type(1)').attr('id','id_claimant_' + ++claimantNum);

    $('#id_claimant_' + claimantNum + ' .row:nth-of-type(1) fieldset>div:nth-of-type(1)').text("Name of Claimant " + claimantNum );

    if($('.claimant-container .claimant').last().find(".remove-claimant").length==0){
        $('.claimant-container .claimant:nth-last-of-type(1)').append('<div style="overflow:hidden;margin-bottom:80px;margin-top:-30px;"><button type="button" class="blocklink remove-claimant" style="font-size: 12px;float:right;">Remove above claimant</button></div>' );
    }

    $('.claimant-container .claimant:nth-last-of-type(1) input').each(function () {
        $(this).val("");
        hideErrorMessageOn($(this));
    });

    $('.claimant-container .claimant:nth-last-of-type(1) input[type="file"]').each(function () {

        var inputName = $(this).attr('name');
        var idName = $(this).attr('id');
        var checkName = inputName.substr(inputName.length - 1);

        if ( isNaN(checkName) ) {
            $(this).attr('name',inputName + '_' +  claimantNum);
            $(this).attr('id',idName + '_' +  claimantNum);
            $(this).closest('.row').find('label').attr('for',idName + '_' +  claimantNum);
            $(this).siblings('a').attr('data-file-input',inputName + '_' +  claimantNum);
        } else {
            var trimName = inputName.slice(0, -1);
            $(this).attr('name',trimName + claimantNum);
            $(this).attr('id',idName.slice(0, -1)  +  claimantNum);
            $(this).closest('.row').find('label').attr('for',idName.slice(0, -1)  +  claimantNum);
            $(this).siblings('a').attr('data-file-input',trimName + claimantNum);
        }

        $(this).siblings('a').text('Upload');
        $(this).removeClass("optional");
    });

    $('.claimant-container a.control_relationship_select').last().remove();
    $('.claimant-container a.control_country_select').last().remove();

    $('.claimant-container select.control_relationship_select').last().remove();
    $('.claimant-container select.control_country_select').last().remove();

    $('.claimant-container .claimant:nth-of-type(1) select.control_relationship_select').clone().appendTo($('.claimant:nth-last-of-type(1) .form-select_wrap:nth-of-type(1)').first());
    $('.claimant-container .claimant:nth-of-type(1) select.control_country_select').clone().appendTo($('.claimant:nth-last-of-type(1) .form-select_wrap:nth-of-type(1)').last());

    $('.claimant-container select.control_relationship_select').last().selectBox('create');
    $('.claimant-container select.control_country_select').last().selectBox('create');

    $('.claimant-container .claimant:nth-last-of-type(1) select').selectBox('value','');
}
$(document).on('click', '.remove-claimant', function() {
    $(this).parent().parent().remove();
});

$(document).on('change', 'select.control_relationship_select', function(){
    if($(this).prop('selectedIndex') !== 0){
        hideErrorMessageOn($(this));
    }
});

$(document).on('change', 'select.control_country_select', function(){
    if($(this).prop('selectedIndex') !== 0){
        hideErrorMessageOn($(this));
    }
});

/* eclaims 1 and 2 step 1 */
/* eclaims 1 step 2b and eclaims 2 step 2 */

$('input:radio[name="post_mortem"]').change(
    function(){
        hideErrorMessageOn($(this));
        if ($(this).val() == 'false') {
            $('.post_mortem_wrapper').hide();
            if(!$('.post_mortem_wrapper').find('input').hasClass('optional')){
                $('.post_mortem_wrapper').find('input').addClass('optional');
            }
        }
        else {
            $('.post_mortem_wrapper').show();
            if($('.post_mortem_wrapper').find('input').hasClass('optional')){
                $('.post_mortem_wrapper').find('input').removeClass('optional');
            }
        }
    });
$('input:radio[name="suicide"]').change(function () {
    hideErrorMessageOn($(this));
});

$('input:radio[name="left_a_will"]').change(
    function(){
        hideErrorMessageOn($(this));
        if ($(this).val() == 'false') {
            $('.will_wrapper').hide();
            if(!$('.will_wrapper').find('input').hasClass('optional')){
                $('.will_wrapper').find('input').addClass('optional');
            }
        }
        else {
            $('.will_wrapper').show();
            if($('.will_wrapper').find('input').hasClass('optional')){
                $('.will_wrapper').find('input').removeClass('optional');
            }
        }
    });
$('input:radio[name="occur_in_singapore"]').change(
    function(){
        hideErrorMessageOn($(this));
        if ($(this).val() == 'true') {
            $('.local_death_wrapper').hide();
            if(!$('.local_death_wrapper').find('input').hasClass('optional')){
                $('.local_death_wrapper').find('input').addClass('optional');
            }
        }
        else {
            $('.local_death_wrapper').show();
            if($('.local_death_wrapper').find('input').hasClass('optional')){
                $('.local_death_wrapper').find('input').removeClass('optional');
            }
        }
    });

/* eclaims 1 step 2b and eclaims 2 step 2 */
/* eclaims 1, 2 step 3 eclaims 3, 4, 5, 6 step 2 */

var bankruptNum = 1;
function addBankrupt(){
    $('.bankrupt-container .bankrupt:nth-last-of-type(1)').clone(true).appendTo('.bankrupt-container').find("input[type='text']").val("");

    var lastId = $('.bankrupt-container .bankrupt').last().attr('id');
    bankruptNum = parseInt(lastId.replace('id_bankrupt_', ''));

    $('.bankrupt-container .bankrupt:nth-last-of-type(1)').attr('id','id_bankrupt_' + ++bankruptNum);

    $('#id_bankrupt_' + bankruptNum + ' div.col-md-5:nth-of-type(1):eq(0)').html("Name of bankrupt person " + bankruptNum );

    if($('.bankrupt-container .bankrupt').last().find(".remove-bank").length==0){
        $('.bankrupt-container .bankrupt:nth-last-of-type(1)').append('<div style="overflow:hidden;margin-bottom:80px;margin-top:-30px;"><button type="button" class="blocklink remove-bank" style="font-size: 12px;float:right;width:50%;display:initial;text-align:right;">Remove the details of above bankrupt person,<br>country / state that issued the bankrpt order</button></div>' );
    }
    $('.bankrupt-container .bankrupt:nth-last-of-type(1) input').each(function () {
        $(this).val("");
        hideErrorMessageOn($(this));
    });
    $('.bankrupt-container .bankrupt:nth-last-of-type(1) input[type="radio"]').removeAttr("checked");
    $('.bankrupt-container .bankrupt:nth-last-of-type(1) select').each(function () {
        $(this).val("");
        hideErrorMessageOn($(this));
    });

    /* for unknown reasons this instance of reinit selectbox doesn't work like the rest */
    $('.bankrupt-container a.control_country_select').last().remove();
    $('.bankrupt-container select.control_country_select').last().remove();
    $('.bankrupt-container .bankrupt').first().find('select.control_country_select').clone().appendTo($('.bankrupt:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(0)'));
    $('.bankrupt-container select.control_country_select').last().selectBox('create');

    $('.bankrupt-container a.control_day_select').last().remove();
    $('.bankrupt-container select.control_day_select').last().remove();
    $('.bankrupt-container .bankrupt').first().find('select.control_day_select').clone().appendTo($('.bankrupt:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(1)'));
    $('.bankrupt-container select.control_day_select').last().selectBox('create');

    $('.bankrupt-container a.control_month_select').last().remove();
    $('.bankrupt-container select.control_month_select').last().remove();
    $('.bankrupt-container .bankrupt').first().find('select.control_month_select').clone().appendTo($('.bankrupt:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(2)'));
    $('.bankrupt-container select.control_month_select').last().selectBox('create');

    $('.bankrupt-container a.control_year_select').last().remove();
    $('.bankrupt-container select.control_year_select').last().remove();
    $('.bankrupt-container .bankrupt').first().find('select.control_year_select').clone().appendTo($('.bankrupt:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(3)'));
    $('.bankrupt-container select.control_year_select').last().selectBox('create');

    $('.bankrupt-container .bankrupt:nth-last-of-type(1) select').selectBox('value','');
}
$(document).on('click', '.remove-bank', function() {
    $(this).parent().parent().remove();
});

var insurerNum = 1;
function addInsurer(){
    var yes = $('.insurer-container .insurer:nth-last-of-type(1) .admitted_option_yes').prop('checked');
    var no = $('.insurer-container .insurer:nth-last-of-type(1) .admitted_option_no').prop('checked');

    $('.insurer-container .insurer:nth-last-of-type(1)').clone(true).appendTo('.insurer-container').find("input[type='text']").val("");

    if(yes){
        $('.insurer-container .insurer:nth-last-of-type(2) .admitted_option_yes').prop('checked', true);
    }else if(no){
        $('.insurer-container .insurer:nth-last-of-type(2) .admitted_option_no').prop('checked', true);
    }

    var lastId = $('.insurer-container .insurer').last().attr('id');
    insurerNum = parseInt(lastId.replace('id_insurer_', ''));

    $('.insurer-container .insurer:nth-last-of-type(1)').attr('id','id_insurer_' + ++insurerNum);

    $('#id_insurer_' + insurerNum + ' .row:nth-of-type(1) div:nth-of-type(1)').text("Name of Insurance Company " + insurerNum );
    if($('.insurer-container .insurer').last().find(".remove-insurer").length==0){
        $('.insurer-container .insurer:nth-last-of-type(1)').append('<div style="overflow:hidden;margin-bottom:80px;margin-top:-30px;"><button type="button" class="blocklink remove-insurer" style="font-size: 12px;float:right;width:50%;display:initial;text-align:right;">Remove the details of above insurance company</button></div>' );
    }

    $('.insurer-container .insurer:nth-last-of-type(1) input').each(function () {
        hideErrorMessageOn($(this));
    });
    $('.insurer-container .insurer:nth-last-of-type(1) input:not([type="radio"])').each(function () {
        $(this).val("");
    });
    $('.insurer-container .insurer:nth-last-of-type(1) input[type="radio"]').removeAttr("checked");
    $('.insurer-container .insurer:nth-last-of-type(1) select').each(function () {
        $(this).val("");
        hideErrorMessageOn($(this));
    });

    $('.insurer-container .insurer:nth-last-of-type(1) .admitted_option_yes').each(function () {
        $(this).attr('id', 'id_admitted_option1_' + insurerNum);
        $(this).attr('name', 'other_insured_claim_admitted_' + insurerNum);
    });
    $('.insurer-container .insurer:nth-last-of-type(1) .admitted_option_no').each(function () {
        $(this).attr('id', 'id_admitted_option2_' + insurerNum);
        $(this).attr('name', 'other_insured_claim_admitted_' + insurerNum);
    });
    $('.insurer-container .insurer:nth-last-of-type(1) .admitted_option_label_yes').each(function () {
        $(this).attr('for', 'id_admitted_option1_' + insurerNum);
    });
    $('.insurer-container .insurer:nth-last-of-type(1) .admitted_option_label_no').each(function () {
        $(this).attr('for', 'id_admitted_option2_' + insurerNum);
    });

    $('.insurer-container a.control_day_select').last().remove();
    $('.insurer-container select.control_day_select').last().remove();
    $('.insurer-container .insurer').first().find('select.control_day_select').clone().appendTo($('.insurer:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(0)'));
    $('.insurer-container select.control_day_select').last().selectBox('create');

    $('.insurer-container a.control_month_select').last().remove();
    $('.insurer-container select.control_month_select').last().remove();
    $('.insurer-container .insurer').first().find('select.control_month_select').clone().appendTo($('.insurer:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(1)'));
    $('.insurer-container select.control_month_select').last().selectBox('create');

    $('.insurer-container a.control_year_select').last().remove();
    $('.insurer-container select.control_year_select').last().remove();
    $('.insurer-container .insurer').first().find('select.control_year_select').clone().appendTo($('.insurer:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(2)'));
    $('.insurer-container select.control_year_select').last().selectBox('create');

    $('.insurer-container .insurer:nth-last-of-type(1) select').selectBox('value','');

}
$(document).on('click', '.remove-insurer', function() {
    $(this).parent().parent().remove();
});

$('input:radio[name="bankrupt"]').change(
    function(){
        hideErrorMessageOn($(this));
        if ($(this).val() == 'false') {
            $('.bankrupt_wrapper').hide();
            if(!$('.bankrupt_wrapper').find('input').hasClass('optional')){
                $('.bankrupt_wrapper').find('input').addClass('optional');
            }
            if(!$('.bankrupt_wrapper').find('select').hasClass('optional')){
                $('.bankrupt_wrapper').find('select').addClass('optional');
            }
        }
        else {
            $('.bankrupt_wrapper').show();
            if($('.bankrupt_wrapper').find('input').hasClass('optional')){
                $('.bankrupt_wrapper').find('input').removeClass('optional');
            }
            if($('.bankrupt_wrapper').find('select').hasClass('optional')){
                $('.bankrupt_wrapper').find('select').removeClass('optional');
            }
        }
    });

$('input:radio[name="other_insured"]').change(
    function(){
        hideErrorMessageOn($(this));
        if ($(this).val() == 'false') {
            $('.other_insured_wrapper').hide();
            if(!$('.other_insured_wrapper').find('input').hasClass('optional')){
                $('.other_insured_wrapper').find('input').addClass('optional');
            }
            if(!$('.other_insured_wrapper').find('select').hasClass('optional')){
                $('.other_insured_wrapper').find('select').addClass('optional');
            }
        }
        else {
            $('.other_insured_wrapper').show();
            if($('.other_insured_wrapper').find('input').hasClass('optional')){
                $('.other_insured_wrapper').find('input').removeClass('optional');
            }
            if($('.other_insured_wrapper').find('select').hasClass('optional')){
                $('.other_insured_wrapper').find('select').removeClass('optional');
            }
        }
    });
$('.bankrupt_wrapper').change(
    function () {
        hideErrorMessageOn($(this));
    });

/* eclaims 1, 2 step 3 eclaims 3, 4, 5, 6 step 2 */
/* eclaims 1, 2 step 4a */

function choosePage(claim) {
    var id = parseInt($('input[name="paymentRadios"]:checked').attr("value"));

    switch (id) {
        case 1:
            //skip to declaration
            openPage('../'+ claim + '/step5a');
            break;
        case 2:
            //skip to declaration
            openPage('../'+ claim + '/step5a');
            break;
        case 3:
            openPage('../'+ claim + '/step4b');
            break;
        default:
            showErrorMessageOn($('input[name="paymentRadios"]'));
            break;
    }
}
$('input[name="payment"]').change(function () {
    hideErrorMessageOn($(this));
});
/* eclaims 1, 2 step 4a */
/* eclaims 3, 4, 5 step 3a */

function choosePage34(claim) {
    var id = parseInt($('input[name="paymentRadios"]:checked').attr("value"));

    switch (id) {
        case 1:
            //skip to declaration
            openPage('../'+ claim + '/step4a');
            break;
        case 2:
            //skip to declaration
            openPage('../'+ claim + '/step4a');
            break;
        case 3:
            openPage('../'+ claim + '/step3b');
            break;
        default:
            showErrorMessageOn($('input[name="paymentRadios"]'));
            break;
    }
}
$('input[name="payment"]').change(function () {
    hideErrorMessageOn($(this));
});
/* eclaims 3, 4, 5 step 3a */
/* eclaims step 4b */

$('select.bank_country_select, select.bank_city_select').change(
    function () {
        if($(this).prop('selectedIndex') !== 0){
            hideErrorMessageOn($(this));
        }
    }
);
$('select.bank_country_select').change(
    function(){
        if ($(this).val() == 'Singapore') {
            $('.bank_address_wrapper').hide();
            $('.bank_address_wrapper input').each(function(){
                hideErrorMessageOn($(this));
                if(!$(this).hasClass('optional')){
                    $(this).addClass('optional');
                }
            });

            if(!$('.bank_address_wrapper').find('select').hasClass('optional')){
                $('.bank_address_wrapper').find('select').addClass('optional');
                hideErrorMessageOn($('.bank_address_wrapper select'));
            }
        }
        else {
            $('.bank_address_wrapper').show();
            $('.bank_address_wrapper input').each(function(){
                if($(this).hasClass('optional')){
                    $(this).removeClass('optional');
                }
            });
            if($('.bank_address_wrapper').find('select').hasClass('optional')){
                $('.bank_address_wrapper').find('select').removeClass('optional');
            }
            $('.bank_address_wrapper .unit-number input').addClass('optional');
            $('.bank_address_wrapper .street').last().find('input').addClass('optional');
            $('.bank_address_wrapper .building-name input').addClass('optional');

        }
    });
/* eclaims step 4b */
/* eclaims 2 & 3 step 2, eclaims 5 step 1c*/
var numAddAttended = 1;
function addAttended(){
    var lastId = $('.attended-container .attended').last().attr('id');
    numAddAttended = parseInt(lastId.replace('id_attended_', ''));
    $('.attended-container .attended:nth-of-type(1)').clone(true,true).appendTo('.attended-container').find("input[type='text']").val("");
    $('.attended-container .attended:nth-last-of-type(1)').attr('id','id_attended_' + ++numAddAttended);
    $('#id_attended_' + numAddAttended + ' .row:nth-of-type(1) legend').html("Consultation " + numAddAttended + " Period"+"<small>(optional)</small>");
    $('#id_attended_' + numAddAttended + ' .row:nth-of-type(2)>div:nth-of-type(1)').html("Name of Doctor / Specialist " + numAddAttended+"<small>(optional)</small>");
    $('#id_attended_' + numAddAttended + ' .row:nth-of-type(3)>div:nth-of-type(1)').html("Name of Hospital / Clinic  " + numAddAttended+"<small>(optional)</small>");
    $('#id_attended_' + numAddAttended + ' .row:nth-of-type(4)>div:nth-of-type(1)').html("Address of Hospital / Clinic  " + numAddAttended+"<small>(optional)</small>");

    $('.attended-container a.control_country_select').last().remove();
    $('.attended-container select.control_country_select').last().remove();
    $('.attended-container .attended:nth-of-type(1) select.control_country_select').first().clone().appendTo($('.attended:nth-last-of-type(1) .city_dropdown .form-select_wrap:nth-of-type(1)').last());

    /* for unknown reasons this instance of reinit selectbox doesn't work like the rest */
    $('.attended-container a.control_month_select_from').last().remove();
    $('.attended-container a.control_month_select_to').last().remove();
    $('.attended-container select.control_month_select_from').last().remove();
    $('.attended-container select.control_month_select_to').last().remove();
    $('.attended-container .attended:nth-of-type(1) select.control_month_select_from').first().clone().appendTo($('.attended:nth-last-of-type(1) .date_dropdown_from .form-select_wrap:nth-of-type(1)').first());
    $('.attended-container .attended:nth-of-type(1) select.control_month_select_to').first().clone().appendTo($('.attended:nth-last-of-type(1) .date_dropdown_to .form-select_wrap:nth-of-type(1)').first());

    $('.attended-container a.control_year_select_from').last().remove();
    $('.attended-container a.control_year_select_to').last().remove();
    $('.attended-container select.control_year_select_from').last().remove();
    $('.attended-container select.control_year_select_to').last().remove();
    $('.attended-container .attended:nth-of-type(1) select.control_year_select_from').first().clone().appendTo($('.attended:nth-last-of-type(1) .date_dropdown_from .form-select_wrap:nth-of-type(1)').last());
    $('.attended-container .attended:nth-of-type(1) select.control_year_select_to').first().clone().appendTo($('.attended:nth-last-of-type(1) .date_dropdown_to .form-select_wrap:nth-of-type(1)').last());
    $('.attended-container .attended:nth-last-of-type(1) select').selectBox('create');
    if($('.attended-container .attended').last().find(".remove-consultant").length==0){
        $('.attended-container .attended:nth-last-of-type(1)').append('<div style="overflow:hidden;margin-bottom:80px;"><button type="button" class="blocklink remove-consultant" style="font-size: 12px;float:right;width:50%;display:initial;text-align:right;">Remove the details of above doctor,hospital / clinic</button></div>' );
    }
    $('.attended-container .attended:nth-last-of-type(1) input').each(function(){
        $(this).val("");
        if(!$(this).hasClass("optional")){
            $(this).addClass("optional");
        }
        hideErrorMessageOn($(this));
    });
    $('.attended-container .attended:nth-last-of-type(1) select').each(function(){
        $(this).val("");
        // $(this).selectBox('destroy');
        if(!$(this).hasClass("optional")){
            $(this).addClass("optional");
        }
        hideErrorMessageOn($(this));
    });

    $('.attended-container .attended:nth-last-of-type(1) select').selectBox('value','');

}
$(document).on('click', '.remove-consultant', function() {
    $(this).parent().parent().remove();
});

$('select.control_day_select_to, select.control_month_select_to, select.control_year_select_to,select.control_day_select_from, select.control_month_select_from, select.control_year_select_from').change(
    function (){
        var fromDay = parseInt($(this).closest('.onlineinvestor__questionnaire').find('.date_dropdown_from .control_day_select_from').selectBox('value'));
        var fromMonth = parseInt($(this).closest('.onlineinvestor__questionnaire').find('.date_dropdown_from .control_month_select_from').selectBox('value'));
        var fromYear = parseInt($(this).closest('.onlineinvestor__questionnaire').find('.date_dropdown_from .control_year_select_from').selectBox('value'));
        var toDay = parseInt($(this).closest('.onlineinvestor__questionnaire').find('.date_dropdown_to .control_day_select_to').selectBox('value'));
        var toMonth = parseInt($(this).closest('.onlineinvestor__questionnaire').find('.date_dropdown_to .control_month_select_to').selectBox('value'));
        var toYear = parseInt($(this).closest('.onlineinvestor__questionnaire').find('.date_dropdown_to .control_year_select_to').selectBox('value'));

        if( fromMonth != null &&
            fromYear != null &&
            toMonth != null &&
            toYear != null){


            if(fromYear === toYear){
                if(fromMonth > toMonth){
                    showSecondaryErrorMessageOn($('.date_dropdown_from select').closest('.onlineinvestor__questionnaire').find('.date_dropdown_to select'));
                }else if(fromMonth == toMonth){
                    if (fromDay != null && toDay != null){
                        if(fromDay > toDay) {
                            showSecondaryErrorMessageOn($('.date_dropdown_from select').closest('.onlineinvestor__questionnaire').find('.date_dropdown_to select'));
                        }else{
                            hideSecondaryErrorMessageOn($('.date_dropdown_from select').closest('.onlineinvestor__questionnaire').find('.date_dropdown_to select'));
                        }
                    }
                }else {
                    hideSecondaryErrorMessageOn($('.date_dropdown_from select').closest('.onlineinvestor__questionnaire').find('.date_dropdown_to select'));
                }
            }else if(fromYear > toYear){
                showSecondaryErrorMessageOn($('.date_dropdown_from select').closest('.onlineinvestor__questionnaire').find('.date_dropdown_to select'));
            }else{
                hideSecondaryErrorMessageOn($('.date_dropdown_from select').closest('.onlineinvestor__questionnaire').find('.date_dropdown_to select'));
            }
        }
    }
);

/* eclaims 2 & 3 step 2, eclaims 5 step 1c */
/* eclaims 3 ,4 step 1 */
$('input:radio[name="illness_previously_suffered"]').change(
    function(){
        hideErrorMessageOn($(this));
        if ($(this).val() == 'false') {
            $('.previous_suffered_container').hide();
            if(!$('.previous_suffered_container').find('input').hasClass('optional')){
                $('.previous_suffered_container').find('input').addClass('optional');
                hideErrorMessageOn($('.previous_suffered_container').find('input'));
            }
        }
        else {
            $('.previous_suffered_container').show();
            if($('.previous_suffered_container').find('input').hasClass('optional')){
                $('.previous_suffered_container').find('input').removeClass('optional');
            }
        }
    });
/* eclaims 3, 4 step 1 */
/* eclaims 3, 4 step 2*/
$('input:radio[name="relationship_illness"]').change(
    function(){
        hideErrorMessageOn($(this));
        if ($(this).val() == 'false') {
            $('.sibling_wrapper').hide();
            if(!$('.sibling_wrapper').find('input').hasClass('optional')){
                $('.sibling_wrapper').find('input').addClass('optional');
                hideErrorMessageOn($('.sibling_wrapper').find('input'));
            }
            if(!$('.sibling_wrapper').find('select').hasClass('optional')){
                $('.sibling_wrapper').find('select').addClass('optional');
                hideErrorMessageOn($('.sibling_wrapper').find('select'));
            }
        }
        else {
            $('.sibling_wrapper').show();
            if($('.sibling_wrapper').find('input').hasClass('optional')){
                $('.sibling_wrapper').find('input').removeClass('optional');
            }
            if($('.sibling_wrapper').find('select').hasClass('optional')){
                $('.sibling_wrapper').find('select').removeClass('optional');
            }
        }
    });

var siblingNum = 1;
function addSibling(){
    var lastId = $('.sibling-container .sibling').last().attr('id');
    siblingNum = parseInt(lastId.replace('id_sibling_', ''));
    $('.sibling-container .sibling:nth-of-type(1)').clone(true).appendTo('.sibling-container').find("input[type='text']").val("");
    $('.sibling-container .sibling:nth-last-of-type(1)').attr('id','id_sibling_' + ++siblingNum);

    $('#id_sibling_' + siblingNum +' div.row:nth-of-type(1)>div:nth-of-type(1)').text("Relationship "+siblingNum);
    $('#id_sibling_' + siblingNum +' div.row:nth-of-type(2)>div:nth-of-type(1)').text("Nature of illness "+siblingNum);
    $('#id_sibling_' + siblingNum +' div.row:nth-of-type(3)>div:nth-of-type(1)').text("Diagnosed date "+siblingNum);

    $('.sibling-container .sibling:nth-last-of-type(1) input').each(function () {
        $(this).val("");
        hideErrorMessageOn($(this));
    });
    $('.sibling-container .sibling:nth-last-of-type(1) select').each(function () {
        $(this).val("");
        hideErrorMessageOn($(this));
    });

    /* for unknown reasons this instance of reinit selectbox doesn't work like the rest */
    $('.sibling-container a.control_relationship_select').last().remove();
    $('.sibling-container select.control_relationship_select').last().remove();
    $('.sibling-container .sibling:nth-of-type(1) select.control_relationship_select').clone().appendTo($('.sibling:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(0)'));
    $('.sibling-container select.control_relationship_select').last().selectBox('create');

    $('.sibling-container a.control_day_select').last().remove();
    $('.sibling-container select.control_day_select').last().remove();
    $('.sibling-container .sibling:nth-of-type(1) select.control_day_select').clone().appendTo($('.sibling:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(1)'));
    $('.sibling-container select.control_day_select').last().selectBox('create');

    $('.sibling-container a.control_month_select').last().remove();
    $('.sibling-container select.control_month_select').last().remove();
    $('.sibling-container .sibling:nth-of-type(1) select.control_month_select').clone().appendTo($('.sibling:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(2)'));
    $('.sibling-container select.control_month_select').last().selectBox('create');

    $('.sibling-container a.control_year_select').last().remove();
    $('.sibling-container select.control_year_select').last().remove();
    $('.sibling-container .sibling:nth-of-type(1) select.control_year_select').clone().appendTo($('.sibling:nth-last-of-type(1) .form-select_wrap:nth-of-type(1):eq(3)'));
    $('.sibling-container select.control_year_select').last().selectBox('create');
    if($('.sibling-container .sibling').last().find(".remove-sibling").length==0){
        $('.sibling-container .sibling:nth-last-of-type(1)').append('<div style="overflow:hidden;margin-bottom:80px;"><button type="button" class="blocklink remove-sibling" style="font-size: 12px;float:right;width:50%;display:initial;text-align:right;">Remove the details of above relationship, nature of illness</button></div>' );
    }

}
$(document).on('click', '.remove-sibling', function() {
    $(this).parent().parent().remove();
});
