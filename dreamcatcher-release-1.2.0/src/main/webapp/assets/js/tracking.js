$(document).ready(function () {
	
    $('[data-event="sub-banners"]').on('click', function(evt) {
		evt.preventDefault();
		var link = $(this).attr('href');
		TMS.trackEvent({
            "event_category": "Sub-Banners:" + $(this).data('product-name'),
            "event_action" : "CTR",
            "event_content": "FindOutMore"
        });
		setTimeout(function () {
			location.href = link;
		}, 2000);
    });

    $('[data-event="make-a-claim"]').on('click', function (evt) {
		evt.preventDefault();
		var link = $(this).attr('href');
		TMS.trackEvent({
            "event_category": "Make a Claim",
            "event_action" : "Make a Claim Submit Button Click",
            "event_content": 'Make a Claim Submit'
		});
		setTimeout(function () {
			location.href = link;
		}, 2000);
    });
    
    $('[data-event="ask-a-question"]').on('click', function () {
    		TMS.trackEvent({
            "event_category": "AskQuestion",
            "event_action" : "Click",
            "event_content": $('#faq-search-keywords').val()
        });
    });
    
    $('[data-event="common-questions"]').on('click', function () {
        TMS.trackEvent({
            "event_category": "CommonQuestions",
            "event_action" : "Click",
            "event_content": 'Questions Click'
        });
    });

   $('[data-event="footer-naviation"]').on('click', function (evt) {
		evt.preventDefault();
		var link = $(this).attr('href');
        TMS.trackEvent({
            "event_category": "Footer Navigation",
            "event_action" : "Click",
            "event_content": link
        });
		setTimeout(function () {
			location.href = link;
		}, 2000);
    });

    $('[data-event="our-plans"]').on('click', function () {
        TMS.trackEvent({
            "event_category": "Product Selection",
            "event_action" : "Click",
            "event_content": "Our Plans"
        });
    });

    $('[data-event="faq-interaction"]').on('click', function () {
        TMS.trackEvent({
            "event_category": "FAQ",
            "event_action" : "FAQ Click",
            "event_content": $(this).find('span').text()
        });
    });
    
    $('[data-event="calculate-your-needs"]').on('click', function (evt) {
		evt.preventDefault();
		var link = $(this).attr('href');
    	TMS.trackEvent({
            "event_category": "Click to Action",
            "event_action" : "Click",
            "event_content": link
        });
		
		setTimeout(function () {
			location.href = link;
		}, 2000);
    });
    
    $('[data-event="quote-retireve"]').on('click', function (evt) {
		evt.preventDefault();
		var link = $(this).attr('href');
    	TMS.trackEvent({
            "event_category": "Get a Quote - " + $(this).data('name'),
            "event_action" : "Get a Quote Button Click",
            "event_content": "Get a Quote"
		});
		setTimeout(function () {
			location.href = link;
		}, 2000);
	});

	$('[data-event="find-out-more"]').on('click', function (evt) {
		evt.preventDefault();
		var link = $(this).attr('href');
    	TMS.trackEvent({
            "event_category": "Find out more - " + $(this).data('name'),
            "event_action" : "Find out more Button Click",
            "event_content": "Find out more"
		});
		setTimeout(function () {
			location.href = link;
		}, 2000);
	});

	$('[data-event="retrieve-your-form"]').on('click', function (evt) {
		evt.preventDefault();
		var link = $(this).attr('href');
    	TMS.trackEvent({
            "event_category": "Retrieve Your Form - " + $(this).data('name'),
            "event_action" : "Retrieve Your Form Button Click",
            "event_content": $(this).data('name') + " - Form Retreive"
		});
		setTimeout(function () {
			location.href = link;
		}, 2000);
	});
    
    $('[data-event="call-us"]').on('click', function () {
    		TMS.trackEvent({
            "event_category": "lead",
            "event_action" : "contact",
            "event_content": "contact HSBC - " + $(this).text()
        });
    });
    
    $('[data-event="email-us"]').on('click', function () {
    		TMS.trackEvent({
            "event_category": "lead",
            "event_action" : "contact",
            "event_content": "contact HSBC - " + $(this).text()
        });
    });
    
    $('[data-event="file-download"]').on('click', function(e){
    		trackingLibrary.fileDownloads({
            filename: $(this).data('name')
        });
    });
    
    $('[data-event="submit-a-claim"]').on('click', function () {
    		TMS.trackEvent({
            "event_category": "click",
            "event_action" : "submit",
            "event_content": 'SubmitClaim'
        });
    });
	
	var formContactUs = $('[data-form-name="contactus"]'),
		dataContactUs= formContactUs.find('input, select, textarea, .selectBox'),
		startContactUs = false;
	
	if (formContactUs.length > 0) {
		dataContactUs.focus(function () {
			if (!startContactUs) {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Start',
					'event_form':     'Form Start',
					'event_content':  'FeedbackForm'
				});
				startContactUs = true;
			}
		});
		
		setTimeout(function () {
			$('.error').each(function () {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Error',
					'event_error':    $(this).data('field-name'),
					'event_content':  'FeedbackForm'
				});
			});
		}, 2000);
	}
	
	if (getCookie('contactus')) {
		setTimeout(function () {
			if ($('.error').length === 0) {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Finish',
					'event_form':     'Form Finish',
					'event_content':  'FeedbackForm'
				});
				deleteCookie('contactus');
			}
		}, 2000);
	}
	
	var formOnlineprotectorRetrieve = $('[data-form-name="onlineprotector-retrieve"]'),
		dataOnlineprotectorRetrieve = formOnlineprotectorRetrieve.find('input, select, textarea, .selectBox'),
		startFormOnlineprotectorRetrieve = false;

	if (formOnlineprotectorRetrieve.length > 0) {
		dataOnlineprotectorRetrieve.focus(function () {
			if (!startFormOnlineprotectorRetrieve) {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Start',
					'event_form':     'Form Start',
					'event_content':  'Online Protector Calculator Retrieve'
				});
				startFormOnlineprotectorRetrieve = true;
			}
		});
		
		setTimeout(function () {
			$('.error').each(function () {
					TMS.trackEvent({
						'event_category': 'Form Interaction',
						'event_action':   'Error',
						'event_error':    $(this).data('field-name'),
						'event_content':  'Online Protector Calculator Retrieve'
					});
			});
		},2000);
	}
	
	if (getCookie('onlineprotector-retrieve')) {
		setTimeout(function () {
			if ($('.error').length === 0 && $('[data-form-name="onlineprotector-retrieve"] .error-message').length === 0) {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Finish',
					'event_form':     'Form Finish',
					'event_content':  'Online Protector Retrieve'
				});
				deleteCookie('onlineprotector-retrieve');
			}
		}, 2000);
	}
	
	var formDirectvaluetermRetrieve = $('[data-form-name="directvalueterm-retrieve"]'),
		dataDirectvaluetermRetrieve = formDirectvaluetermRetrieve.find('input, select, textarea, .selectBox'),
		startFormDirectvaluetermRetrieve = false;

	if (formDirectvaluetermRetrieve.length > 0) {
		dataDirectvaluetermRetrieve.focus(function () {
			if (!startFormDirectvaluetermRetrieve) {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Start',
					'event_form':     'Form Start',
					'event_content':  'Directvalueterm Calculator Retrieve'
				});
				startFormDirectvaluetermRetrieve = true;
			}
		});
		
		setTimeout(function () {
			$('.error').each(function () {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Error',
					'event_error':    $(this).data('field-name'),
					'event_content':  'Directvalueterm Calculator Retrieve'
				});
			});
		}, 2000);
	}
	
	if (getCookie('directvalueterm-retrieve')) {
		setTimeout(function () {
			if ($('.error').length === 0 && $('[data-form-name="directvalueterm-retrieve"] .error-message').length === 0) {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Finish',
					'event_form':     'Form Finish',
					'event_content':  'Online Protector Retrieve'
				});
				deleteCookie('directvalueterm-retrieve');
			}
		}, 2000);
	}
    
    if ($('.calculators__landing').length > 0) {
		var formStarted = false;
		$('[data-event="calculator-start"]').on('click', function() {
			if(!formStarted) {
				trackingLibrary.toolInteractions({
					action: "Click",
					field: "Start",
					content: $(this).data('name')
				});
				formStarted = true;
			}
		});

		$('[data-event="calculator-finish"]').on('click', function() {
			if ($('.has-error').length === 0) {
				trackingLibrary.toolInteractions({
					action: "Click",
					field: "Finish",
					content: $(this).data('name')
				});
			}
		});
		
		$('[data-event="yourdreams"]').on('click', function () {
			trackingLibrary.toolInteractions({
				action: "Start",
				field: "Form Start",
				content: $(this).data('name')
			});
		});
	}	
});