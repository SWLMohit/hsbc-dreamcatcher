(function (window, document) {


    var logoutDisplayTimeout = null;
    var logoutTimeout = null;
    var logoutDisplayDelay = 30 * 6000;
    var logoutDelay = 2 * 6000;
    var isIE11 = /Trident.*rv[ :]*11\./.test(navigator.userAgent);
    var ciRiderFlag = true;
    var errorCount = null;

    
    /*
     * Makes a ajax call to the backend
     * to get the latest discount rates for
     * premium term 5,20 and 65. 
     * */
    function ajaxPaymentCall(sumAssuredMax){
    	 var term = $('select[name=term_duration] :selected').val();
         var paymentFrequency=$("[name=payment_mode]").val();
         
         $.ajax({
         	type: 'POST',
             url: "/our-plans/direct-value-term/calculation",
             data: {
                
                 "sum_assured": sumAssuredMax
                 
             },
             success: function (resultData) {
            	 var actualdiscount5=resultData["actualDiscount5"];
            	 var actualdiscount20=resultData["actualDiscount20"];
            	 var actualdiscount65=resultData["actualDiscount65"];
             	$("#premium-table .premium-table").attr("actualdiscount5",actualdiscount5);
             	$("#premium-table .premium-table").attr("actualdiscount20",actualdiscount20);
             	$("#premium-table .premium-table").attr("actualdiscount65",actualdiscount65);
             	
             	$("[name=actualdiscount5]").val(actualdiscount5);
             	$("[name=actualdiscount20]").val(actualdiscount20);
             	$("[name=actualdiscount65]").val(actualdiscount65);
             	
             	paymentChange(sumAssuredMax);
             	
             }
         	
         })
    }
    /*
	 * Calculates the latest premium values
	 * based on the input term ,sum assured,payment frequecy and 
	 * ci rider selected
	 */
    function paymentChange(sumAssuredMax){
    	var term = $('select[name=term_duration] :selected').val();
    	var premiumRate=parseFloat($("#premium-table .premium-table").attr("premiumrate"));
    	var premiumRateUnitAmount=parseFloat($("#premium-table .premium-table").attr("premiumrateunitamount"));
    
    	var actualdiscount=parseFloat($("#premium-table .premium-table").attr("actualdiscount5"));
    	
    	var ciadjustedPremium=parseFloat($("#premium-table .premium-table").attr("ciadjustedPremium"));
    	if(term.trim()=="@65"){
    		premiumRate=parseFloat($("#premium-table .premium-table").attr("premiumrate65"));
    		premiumRateUnitAmount=parseFloat($("#premium-table .premium-table").attr("premiumrateunitamount65"));
    		actualdiscount=parseFloat($("#premium-table .premium-table").attr("actualdiscount65"));
    		ciadjustedPremium=parseFloat($("#premium-table .premium-table").attr("ciadjustedPremium65"));
    	}else if(term.trim()=="20Y"){
    		premiumRate=parseFloat($("#premium-table .premium-table").attr("premiumrate20"));
    		premiumRateUnitAmount=parseFloat($("#premium-table .premium-table").attr("premiumrateunitamount20"));
    		actualdiscount=parseFloat($("#premium-table .premium-table").attr("actualdiscount20"));
    		ciadjustedPremium=parseFloat($("#premium-table .premium-table").attr("ciadjustedPremium20"));
    	}
    	
    	
    	if(sumAssuredMax==undefined||sumAssuredMax==null){
        	sumAssuredMax=parseInt($("[name=sum_assured]").val().replace(/,/g,''));
        	if(isNaN(sumAssuredMax)){
            	  sumAssuredMax=0;
              }
    	}
          var paymentFrequency=$("[name=payment_mode]").val();
          var modalFactor=getModalFactor(paymentFrequency);
          
          	var adjustedPremium=((premiumRate / premiumRateUnitAmount) - (actualdiscount));
			var underwriting=parseFloat($("#premium-table .premium-table").attr("underwritingclassFactor"));
			adjustedPremium=adjustedPremium*underwriting*sumAssuredMax;
			var newBasic = Math.round(adjustedPremium * modalFactor * 100) / 100;
			var basicValue=parseFloat(newBasic.toFixed(2));
			$("#basic_mode").text("S$"+newBasic.toFixed(2));
			var riderHidden=$("#cirider-row").is(":hidden");
			var total=basicValue;
			
				
				var riderSumAssuredMax = parseInt($("[name=rider_sum_assured]").val().replace(/,/g,''));
				
				adjustedPremium=ciadjustedPremium*riderSumAssuredMax;
				var newCiRider = adjustedPremium* modalFactor;
				newCiRider=Math.round(newCiRider * 100) / 100;
				var ciValue=parseFloat(newCiRider.toFixed(2));
				$("#ci_mode").text("S$"+newCiRider.toFixed(2));
				
				if(!riderHidden){
				if(!isNaN(ciValue)){
					total=ciValue+total;
				}
				
			}
			
			
          $("#total").text("S$"+total.toFixed(2));
          $("[name=basic]").val(basicValue);
          $("[name=ci]").val(ciValue);
          $("#premium-table table.data-table tbody td").css("visibility","visible");
          $("#premium-table #basic_mode").parent().parent().find("td").first().text("Basic Plan");
          $("#premium-table #basic_mode").parent().parent().find("td").first().css("background","#EDEDED");
          $("#premium-table tbody").css("background","#EDEDED");
    	
    }
    $(document).ready(function () {
        //
        // early out if we're not in the directvalueterm page
        //
    	
        if (!$('section.directvalueterm').length) {
            return;
        }


        setTimeout(function () {

            var errorCount = $('.error-message').length;

            if (errorCount > 0 && !$('#quick_quote.directvalueterm').length && $('.buynow-form').length) {
                $('.error-message').closest('form').prepend('<div class="visuallyhidden focusable aria-error-message" aria-atomic="true" tabindex="-1">' + errorCount + ' errors found</div>');

                setTimeout(function () {
                    $(document).find('.aria-error-message').attr('aria-live', 'assertive').focus();
                }, 400);
            }

        }, 500);
        

        function displayErrorMessageQQ() {

            setTimeout(function () {
                var errorCount = $('.form-group.error').length;

                if (errorCount > 0) {

                    if ( $('.aria-error-message').length ) {
                        $('.aria-error-message').removeAttr('aria-live').text( + errorCount + ' errors found');
                    } else {
                        $('.form-group.error').closest('form').prepend('<div class="visuallyhidden focusable aria-error-message" aria-atomic="true" tabindex="-1">' + errorCount + ' errors found</div>');                    
                    }

                    setTimeout(function () {
                        $(document).find('.aria-error-message').attr('aria-live', 'assertive').focus();
                    }, 400);
                }
            }, 500);
            
        }

        $('form [data-action=quote]').click(function () {
            displayErrorMessageQQ();
        });


        if ( $('section.verification').hasClass('formnotfound')) {
            $('#modalformnotfound').modal('show');
        }

        if ( $('section.verification').hasClass('formnotfoundsimple')) {
            $('#modalformnotfoundsimple').modal('show');
        }

        if ( $('section.verification').hasClass('modalcodesent')) {
            $('#modalcodesent').modal('show');
        }

        if ( $('section.verification').hasClass('modalcodesentnew')) {
            $('#modalcodesentnew').modal('show');
        }

        if ( $('section.verification').hasClass('modalformsent')) {
            $('#modalformsent').modal('show');
        }

        if ( $('section.verification').hasClass('modalinvalidcode')) {
            $('#modalinvalidcode').modal('show');
        }
       
        //allow enter or space keys to toggle pretty checkboxes
        $(document).on('keypress', 'label.checkbox', function(e) {
            if(e.keyCode == 13 || e.keyCode == 32) {
                e.preventDefault();
                $(this).find('input[type=checkbox]').trigger('click');
            }
        });

        $('form [data-form-action="submit"]').click(function (evt) {
			evt.preventDefault();
			var obj = $(this);

			setCookie('directvalueterm-retrieve', 'true');

			$('input').attr('placeholder', '');
			obj.parents('form').submit();
			window.onbeforeunload = null;
		});

		$('form.buynow-form [data-form-action="accept"]').click(function (evt) {
			evt.preventDefault();
			var obj = $(this);
			TMS.trackEvent({
				'event_category': 'Form Interaction',
				'event_action':   'Start',
				'event_form':     'Form Start',
				'event_content':  'Direct Value Term Application Form (I Agree)'
			});
			if ($('[data-current-step="7.0"]').length && $('.ci-rider-addon').is(':visible') && $('input[name=include_ci_rider]').is(':unchecked') && ciRiderFlag) {
                $('#ridermodal').modal('show');
            } else {
				setTimeout(function () {
					$(this).attr("disabled", "disabled");
					window.onbeforeunload = null;
					$('form.buynow-form').submit();
					return false;
				}, 2000);
            }
		});

		var formDirectvaluetermStep = $('[data-form-name="directvalueterm-step"]'),
			dataDirectvaluetermStep = formDirectvaluetermStep.find('input, select, textarea, .selectBox'),
			startDirectvaluetermStep = false;

		if (formDirectvaluetermStep.length > 0) {
			dataDirectvaluetermStep.focus(function () {
				if (!startDirectvaluetermStep) {
					TMS.trackEvent({
						'event_category': 'Form Interaction',
						'event_action':   'Start',
						'event_form':     'Form Start',
						'event_content':  'Direct Value Term Application Form (By Now)'
					});
					startDirectvaluetermStep = true;
				}
			});

			setTimeout(function () {
				$('.error').each(function () {
					TMS.trackEvent({
						'event_category': 'Form Interaction',
						'event_action':   'Error',
						'event_error':    $(this).data('field-name'),
						'event_content':  'Direct Value Term Application Form (By Now)'
					});
				});
			}, 2000);
		}

		if (getCookie('directvalueterm-step')) {
			setTimeout(function () {
				if ($('.error').length === 0) {
					TMS.trackEvent({
						'event_category': 'Form Interaction',
						'event_action':   'Finish',
						'event_form':     'Form Finish',
						'event_content':  'Online Protector Retrieve'
					});
					deleteCookie('directvalueterm-step');
				}
			}, 2000);
		}

        $('form.buynow-form [data-form-action=next]').click(function () {

            if ($('[data-current-step="7.0"]').length && $('.ci-rider-addon').is(':visible') && $('input[name=include_ci_rider]').is(':unchecked') && ciRiderFlag) {
                $('#ridermodal').modal('show');

            } else {
				setCookie('directvalueterm-step', 'true');
                $(this).attr("disabled", "disabled");
                window.onbeforeunload = null;
                $('form.buynow-form').submit();
                return false;
            }
        });

        
        $('form.buynow-form [data-form-action="prev"]').click(function () {
			window.location = $(this).data('url');
			window.onbeforeunload = null;
			return false;
		});

		$('form.buynow-form [data-form-action="reject"]').click(function () {
			TMS.trackEvent({
				'event_category': 'Form Interaction',
				'event_action':   'Finish',
				'event_form':     'Form Finish',
				'event_content':  'Direct Value Term Application Form (I Disagree)'
			});
			displayDropoutModal();
			window.onbeforeunload = null;
			return false;
		});

        $('input[type=file]').on('change', function() {
            if(this.files[0].size > 1000000){
               alert("The file is over 1MB. Please upload a smaller file.");
               this.value = "";
            }
        });
        
        if ($('.slider-coverage').length) {
            var coverageSlider = $('.slider-coverage')[0];
            noUiSlider.create(coverageSlider, {
                start: $('input[name=sum_assured]').val() || 0,
                step: 100,
                connect: [true, false],
                padding: 100,
                tooltips: [
                    wNumb({
                        decimals: 0,
                        prefix: 'S$',
                        thousand: ','
                    })
                ],
                range: {
                    min: $(coverageSlider).data('min') - 100,
                    max: $(coverageSlider).data('max') + 100
                },
                pips: {
                    mode: 'values',
                    values: [$(coverageSlider).data('min'), $(coverageSlider).data('max')],
                    density: 20,
                    format: wNumb({
                        decimals: 0,
                        prefix: 'S$',
                        thousand: ','
                    })
                },
                format: wNumb({
                    decimals: 0,
                    prefix: '',
                    thousand: ','
                })
            });
            var sliderTouched=false;
            coverageSlider.noUiSlider.on('set', function (values, handle) {
            	console.log("the end is called")
				if(sliderTouched){
            	sliderTouched=false;
            		setTimeout(function(){
            			if(!sliderTouched){
							var sumAssuredMax=parseInt($("[name=sum_assured]").val().replace(/,/g,''));
            				sumAssured=parseInt(values[handle].replace(/,/g, ''));
							if(sumAssured==sumAssuredMax){
            				  ajaxPaymentCall(sumAssured);
							}
            			}
            		}, 2000);
				}
            	
            });
            coverageSlider.noUiSlider.on('update', function (values, handle) {
                $('input[name=sum_assured]').val(values[handle] || 0);

                $('input[name=rider_sum_assured]').val(values[handle] || 0);
                sliderTouched=true;
                $("table.data-table tbody td").css("visibility","hidden");
    			$("#basic_mode").parent().parent().find("td").first().text("");
    			$("#basic_mode").parent().parent().find("td").first().css("visibility","visible")
    			$("#basic_mode").parent().parent().find("td").first().css("background","url('/assets/images/HSBC_loadingspinner.gif') no-repeat 70%");
    			$("#basic_mode").parent().parent().find("td").first().css("background-size","10% 100%");


            });


            if (isIE11) {
                $('input[name=sum_assured]').on('mouseenter mouseleave', function () {
                    coverageSlider.noUiSlider.set($(this).val());
                });

                $('input[name=sum_assured]').on('keyup', function () {
                    if (event.keyCode === 13) {
                        coverageSlider.noUiSlider.set($(this).val());
                    }
                });
            }
            else {
                $('input[name=sum_assured]').on('change', function () {
                    coverageSlider.noUiSlider.set($(this).val() || 0);
                });
            }

            $('.noUi-handle', this).removeAttr('tabindex');

        }

        if ($('.slider-rider-coverage').length) {
            var riderCoverageSlider = $('.slider-rider-coverage')[0];
            noUiSlider.create(riderCoverageSlider, {
                start: $('input[name=rider_sum_assured]').val() || 0,
                step: 100,
                connect: [true, false],
                padding: 100,
                tooltips: [
                    wNumb({
                        decimals: 0,
                        prefix: 'S$',
                        thousand: ','
                    })
                ],
                range: {
                    min: $(riderCoverageSlider).data('min') - 100,
                    max: $(riderCoverageSlider).data('max') + 100
                },
                pips: {
                    mode: 'values',
                    values: [$(riderCoverageSlider).data('min'), $(riderCoverageSlider).data('max')],
                    density: 20,
                    format: wNumb({
                        decimals: 0,
                        prefix: 'S$',
                        thousand: ','
                    })
                },
                format: wNumb({
                    decimals: 0,
                    prefix: '',
                    thousand: ','
                })
            });
            riderCoverageSlider.noUiSlider.on('update', function (values, handle) {
                $('input[name=rider_sum_assured]').val(values[handle] || 0);
            });

            if (isIE11) {
                $('input[name=rider_sum_assured]').on('mouseenter mouseleave', function () {
                    riderCoverageSlider.noUiSlider.set($(this).val());
                });

                $('input[name=rider_sum_assured]').on('keyup', function () {
                    if (event.keyCode === 13) {
                        riderCoverageSlider.noUiSlider.set($(this).val());
                    }
                });
            }
            else {
                $('input[name=rider_sum_assured]').on('change', function(event) {
                    riderCoverageSlider.noUiSlider.set($(this).val() || 0);

                });
            }

            $('.noUi-handle', this).removeAttr('tabindex');
        }

        if ($('.directvalueterm__breadcrumb').data('otp-sent') == '0' && parseInt($('.directvalueterm__breadcrumb').data('max-step')) > 4 && parseInt($('.directvalueterm__breadcrumb').data('max-step')) <= 14) {
            $('#buynow-timeout-modal').on('hidden.bs.modal', resetLogoutTimeout);
            $('body').click(resetLogoutTimeout);
            $('body').keyup(resetLogoutTimeout);
            logoutDisplayTimeout = setTimeout(displayLogoutWarning, logoutDisplayDelay);
        }

        $("#premium-table table.data-table tbody td").css("visibility","visible");
        $("#premium-table #basic_mode").parent().parent().find("td").first().text("Basic Plan");
        $("#premium-table #basic_mode").parent().parent().find("td").first().css("background","#EDEDED");
        $("#premium-table tbody").css("background","#EDEDED")
        


        if ($('input[type=checkbox]').length) checkBoxAria();
        if ($('#quick_quote.directvalueterm').length) quickQuote();
        if ($('[data-current-step="3.0"]').length) step3();
        if ($('[data-current-step="4.0"]').length) step4();
        if ($('[data-current-step="4.1"]').length) step4a();
        if ($('[data-current-step="7.0"]').length) step7();
        if ($('[data-current-step="8.0"]').length) step8();
        if ($('[data-current-step="10.0"]').length) step10();
        if ($('[data-current-step="11.0"]').length) step11();
        if ($('[data-current-step="12.0"]').length) step12();
        if ($('[data-current-step="14.0"]').length) step14();
        if ($('[data-current-step="15.0"]').length) step15();

        if ($('form[data-form-dropout="1"]').length) {
            displayDropoutModal();
        }

        $('#buynow-dropout-modal .close').click(function () {
            location.href = '/your-dreams/';
        });

        $('a[data-file-input]').click(function () {
            $('input#' + $(this).data('file-input')).click();
            return false;
        });

        $('input[type=file]').change(function () {
            var filename = $(this).val().replace(/^.*[\\\/]/, '');
            $('a[data-file-input="' + $(this).attr('id') + '"]').text('File selected');
        });

        /**
         * Tagging event functions
         */
        var formStarted = false;
        $('[data-tag-quick-quote] input').add('[data-tag-quick-quote] select').change(function(e){
            if(!formStarted){
                trackingLibrary.toolInteractions({
                    action: "Start",
                    field: "Form Start",
                    content: $(this).attr('name')
                });
                formStarted = true;
            }
        });
        $('[data-tag-quick-quote] a[data-action=quote]').focus(function(e){
            trackingLibrary.toolInteractions({
                action: "Finish",
                field: "Form Finish",
                content: '' //todo what value? <GetQuoteCalculator>
            });
        });

        $('[data-tag-file-download]').click(function(e){
            trackingLibrary.fileDownloads({
                // filename: $(this).data('tagFileDownload')
                filename: $(this).attr('href')
            });
        });

        $('[data-tag-feedback-interaction]').click(function(e){
            trackingLibrary.feedbackInteractions({
                isPositive: $(this).data('tagFeedbackInteraction')
            });
        });

       var maxStep=parseInt($('.onlineprotector__breadcrumb').data('max-step'));
         if ( maxStep>= 3&&maxStep<=14) {
          window.onbeforeunload = function() {
            return 'Are you sure you want to exit?';
          }
        }

        function checkAge() {

            $('input.date-validate, select[name=term_duration], [name=include_ci_rider]').on('change', function () {
            // $('input.date-validate').on('change', function () {
      

                var dobInput = $('input.date-validate').val();
                var bDayParse = dobInput.split('/');
                if ( bDayParse[2]==0 || bDayParse[1]==0 || bDayParse[0]==0 ) {
                   return;
                }
                var bDay = new Date(bDayParse[2], bDayParse[1] - 1, bDayParse[0]);
                var age = getAge(bDay);
                var today = new Date();
                var riderCoverageSlider = $('.slider-rider-coverage');
                var sumAssured = parseFloat( $("input[name='sum_assured']").val().replace(/,/g,'') );
                var term = $('select[name=term_duration] :selected').val();
                var ci_rider = $('[name=include_ci_rider]');

                if ($('input.date-validate').length) {

                    if (bDay > today) {
                      $('#age-validation-modal').modal('show');

                    } else {
                      

                        //Basic
                        //1) Level Term up till Age 65: Max entry age is 60
                        //2) 5 year renewable Term and 20 non-renewable year Term: Max entry age is 65 but 62 for online
                        //Rider
                        // 1) Level Term up till Age 65 and 5 year renewable Term: Max entry age is 60
                        // 2) 20 non-renewable year Term: Max entry age is 45
                        if (age > 60) {
                            $('.ci-rider-addon').hide();
                            $("#include_ci_rider").prop("checked", false);

                            if (age <= 62 )  {
                                $('[data-action=quote]').addClass('primary-btn-slate').removeClass('secondary-btn-slate').css('pointer-events','auto'); //enable the Get a quote
                            }

                            if (term === "@65" || age > 62) { // show error and hide the quote
                                $('#eligibility-validation-modal').modal('show');
                                $('[data-action=quote]').removeClass('primary-btn-slate').addClass('secondary-btn-slate').css('pointer-events','none');
                            }

                        } else if (age > 45) {
                            $('[data-action=quote]').addClass('primary-btn-slate').removeClass('secondary-btn-slate').css('pointer-events','auto');

                            if  ( age > 45 && term === "20Y" ) {
                                $('.ci-rider-addon').hide();
                                $("#include_ci_rider").prop("checked", false);
                            } else {
                                 $('.ci-rider-addon').show();
                            }
                        } else if (age < 19 ) {
                            $('[data-form-action=next]').addClass('btn-red').removeClass('btn-grey').css('pointer-events','auto');

                        } else {// show the rider and enable the quote button
                            $('[data-action=quote]').addClass('primary-btn-slate').removeClass('secondary-btn-slate').css('pointer-events','auto');
                            $('.ci-rider-addon').show();
                        }
                    }
                 }
            });
        }

        function quickQuote() {
            checkAge();

            $('[data-action=quote]').on('click', function() {
                setTimeout(function() {
                    if ($(".estimate-form-result").length){

                        $('html, body').animate({
                            scrollTop: $(".estimate-form-result").offset().top -60
                        }, 1000);
                    }
                },500);
            });

            $('#promocode').keyup(function() {
                promo = $(this).val();

                $(this).val($(this).val().replace(/ +?/g, ''));

                if(!validateAlphaNum(promo) && $(this).val() !== ""){
                   showErrorMessageOn($(this));
                }

            });
            
        }
    });


    function checkBoxAria() {

        $('input[type=checkbox]').each(function(){
            if ($(this).is(':checked')) {
                $(this).closest('label').attr('aria-checked','true').toggleClass('checked');
            }
        });

        /**
        * Quick Quote: checkbox aria-checked
        */
        $('#quick_quote.directvalueterm input[type=checkbox]').on('change', function() {
            $(this).closest('label').toggleClass('checked');

            if ( $(this).closest('label').hasClass('checked') ) {
                $(this).closest('label').attr('aria-checked','true');
            } else {
                $(this).closest('label').attr('aria-checked','false');
            }
        });
    }


    /**
     * Returns age from date of birth
     *
     * @param {Date} date
     * @returns {Number}
     */
    function getAge(date) {
        var now = new Date();
        var age = now.getFullYear() - date.getFullYear();
        if (((now.getMonth()) * 100 + now.getDate() - (date.getMonth()) * 100 - date.getDate()) > 0) {
            age = age + 1;
        }
        return age;
    };

    function step3() {
        $('body').addClass('step-3');
        $('select[name="applicant_residency"]').change(function () {
            if ($(this).val() != '') {
                if ($(this).val() == 'Singaporean' || $(this).val() == 'Singapore PR') {
                    $('[data-dynamic-label="applicant-nric-passport-label"]').text('NRIC');
                } else {
                    $('[data-dynamic-label="applicant-nric-passport-label"]').text('Passport');
                }
            }
        });
        $('select[name="insured_residency"]').change(function () {
            if ($(this).val() != '') {
                if ($(this).val() == 'Singaporean' || $(this).val() == 'Singapore PR') {
                    $('[data-dynamic-label="insured-nric-passport-label"]').text('NRIC');
                } else {
                    $('[data-dynamic-label="insured-nric-passport-label"]').text('Passport');
                }
            }
        });
        $('input[name="life_insured"]').change(function () {
            $('[data-question-group=spouse]').toggle($(this).val() == 'spouse');
        });
        if ($('#id_insured_option2').is(':checked')) {
            $('[data-question-group=spouse]').show();
        }
        $('#buynow-dropout-modal .add1').show();
        $('select[name="applicant_residency"], select[name="insured_residency"]').change();
    }

    function step4() {
        $('input[name="applicant_multiple_nationalities"]').change(function () {
            $('[data-question-group=multiple_nationalities]').toggle($(this).val() == 'Y');
        });
        if ($('#id_multiple_nationalities_option1').is(':checked')) {
            $('[data-question-group=multiple_nationalities]').show();
        }

        if ($('select[name=applicant_nationality]').prop('selectedIndex') !== 0) {
            var nationality = $('select[name=applicant_nationality]').val(); 

            $('select[name=applicant_birth_country]').selectBox('value', nationality);
        }

        $('select[name=applicant_nationality]').change(function() {
            var nationality = $(this).val();

            $('select[name=applicant_birth_country]').selectBox('value', nationality);
        });

        $('#buynow-dropout-modal .add1').show();
    }

    function step7() {
		var sumAssuredJs=parseInt($("[name=sum_assured]").val().replace(/,/g,''));
		var sumAssuredJsp=parseInt($("[name=sum_assured]").attr("value"));
		if(sumAssuredJs!=sumAssuredJsp){
			$("[name=sum_assured]").val(sumAssuredJsp);
			var coverageSlider = $('.slider-coverage')[0];
			coverageSlider.noUiSlider.set(sumAssuredJsp);
		}
        $('input[name="include_ci_rider"]').change(function () {
            $('[data-question-group=ci_rider]').toggle($('input[name=include_ci_rider]').is(':checked'));
            var basicValue=parseFloat($("#basic_mode").text().replace('S$', ''));
            var ci= parseFloat($("#ci_mode").text().replace('S$', ''));
            var total=0;
            if($(this).is(":checked")){
            	$("#cirider-row").show();
            	total=basicValue+ci;
            }else{
            	total=basicValue;
            	$("#cirider-row").hide();
            }
            $("#total").text("S$"+total.toFixed(2));
        });
        if ($('input[name=include_ci_rider]').is(':checked')){
        	 $("#cirider-row").show();
            $('[data-question-group=ci_rider]').show();
            var basicValue=parseFloat($("#basic_mode").text().replace('S$', ''));
            var ci= parseFloat($("#ci_mode").text().replace('S$', ''));
            var total=0;
        	total=basicValue+ci;
        	$("#total").text("S$"+total.toFixed(2));
        }

        function checkEligibility() {

            var dob = $('section.directvalueterm').data('dob');
            var dobArr = dob.split("-");
            var term = $('select[name=term_duration] :selected').val();
            var Bday = new Date(dobArr[0], dobArr[1] - 1, dobArr[2]);
            console.log('Bday:' + Bday);
            var age = getAge(Bday);
            console.log('age:' + age + ' term:' + term);
            if(term=="20Y"||term=="@65"){
            	paymentChange();
            }
            if (age > 60) {
                $('.ci-rider-addon').hide();

            } else if (age > 45 && term === "20Y") {
                $('.ci-rider-addon').hide();

            } else {
                $('.ci-rider-addon').show();
            }

            if (age > 60) {
                $('[data-form-action=next]').addClass('btn-red').removeClass('btn-grey').css('pointer-events', 'auto');
                if (age > 62 || term === "@65" && age > 60) {
                    $('#eligibility-validation-modal').modal('show');
                    $('[data-form-action=next]').removeClass('btn-red').addClass('btn-grey').css('pointer-events', 'none');
                }
            }
        }

        checkEligibility();
        
        $('select[name=term_duration]').on('change', function () {
            checkEligibility();
            paymentChange();
        });

        $('#ridermodal .btn-white').click(function(){
            ciRiderFlag = false;
            $('[data-form-action=next]').trigger('click');
        });
        if($("[name=payment_mode]").val().length>0){
     	   var paymentFrequency=$("[name=payment_mode]").val();
     	   var frequencyName=getPaymentName(paymentFrequency);
     	   $("#payment-method").text(frequencyName);
     	   paymentChange();
         }
     
        $("[name=payment_mode]").on("change",function(){
        	var paymentFrequency=$("[name=payment_mode]").val();
        	var frequencyName=getPaymentName(paymentFrequency);
            $("#payment-method").text(frequencyName);
            paymentChange();
               
      });
    }
    function getModalFactor(paymentMode){
    	var factor;
    	 switch (paymentMode) {
         case "12":
             factor = 0.085;
             break;
         case "4":
             factor = 0.255;
             break;
         case "2":
             factor = 0.51;
             break;
         case "1":
        	 factor = 1;
             break;
         default:
             factor = 0.085;
             break;
     }
    return factor;
    }
    function getPaymentName(paymentMode){
    	var payment;
    	 switch (paymentMode) {
         case "12":
        	 payment = "Monthly";
             break;
         case "4":
        	 payment = "Quarterly";
             break;
         case "2":
        	 payment = "Semi-Annually";
             break;
         case "1":
        	 payment = "Annually";
             break;
         default:
        	 payment = "Monthly";
             break;
     }
    return payment;
    }
    function step8() {
        $('input[name="uw_lifestyle_questions_optin"]').change(function () {
            $('[data-question-group=uw_lifestyle]').toggle($(this).val() == 'Y');
        });
        if ($('#id_uw_lifestyle_questions_optin_option1').is(':checked')) {
            $('[data-question-group=uw_lifestyle]').show();
        }
    }

    function step10() {
        var workingStatus = ['Salaried', 'Self-employed'];
        var studentStatus = ['Student'];
        var employment = $('[name="applicant_employment"]');

        employment.change(function () {

            $('[data-question-group=working_applicant]').toggle(workingStatus.indexOf($(this).val()) != -1);
            $('[data-question-group=student_applicant]').toggle(studentStatus.indexOf($(this).val()) != -1);
            if (studentStatus.indexOf($(this).val()) != -1 && !$('[name="applicant_income"]').val()) {
                $('[name="applicant_income"]').val(0);
            }

            if ($('input[name=applicant_income]').val().trim().length == 0 || $('input[name=applicant_income]').val().trim() == '0') {
                if ($(this).val() != 'Salaried' && $(this).val() != 'Self-employed') {
                    $('input[name=applicant_income]').val(0);
                } else {
                    $('input[name=applicant_income]').val(null);
                }
            }

            if ($(this).val() === 'Salaried') {
                $('[data-label-group="work-salaried"]').show();
                $('[data-label-group="work-self"]').hide();
            } else {
                $('[data-label-group="work-salaried"]').hide();
                $('[data-label-group="work-self"]').show();
            }
        });

        $('[data-question-group=working_applicant]').toggle(workingStatus.indexOf(employment.val()) != -1);
        $('[data-question-group=student_applicant]').toggle(studentStatus.indexOf(employment.val()) != -1);
    }

    function step11() {
        var totalMonths = -1;
        var years = parseInt($('input[name="applicant_address_period_stay_years"]').val());
        var months = parseInt($('input[name="applicant_address_period_stay_months"]').val());
        years = (isNaN(years) ? -1 : years);
        months = (isNaN(months) ? -1 : months);
        totalMonths = (years * 12) + months;
        if (totalMonths >= 0 && totalMonths < 60) {
            $('[data-question-group=address_less_than_5yrs]').show();
        }
        $('input[name="applicant_address_period_stay_years"], input[name="applicant_address_period_stay_months"]').change(function () {
            years = parseInt($('input[name="applicant_address_period_stay_years"]').val());
            months = parseInt($('input[name="applicant_address_period_stay_months"]').val());
            years = (isNaN(years) ? 0 : years);
            months = (isNaN(months) ? 0 : months);
            totalMonths = (years * 12) + months;
            $('[data-question-group=address_less_than_5yrs]').toggle(totalMonths < 60);
        });

        $('input[name="applicant_address_permanent"]').change(function () {
            $('[data-question-group=permanent_address]').toggle($(this).val() == 'N');
        });
        if ($('#id_applicant_address_permanent_option2').is(':checked')) {
            $('[data-question-group=permanent_address]').show();
        }

        var residentialAddressIsTaxCountry = false;
        $('.tax-country-wrapper').each(function () {
             $(this).find('.tax-tin').show();
             if ($(this).find('.tax-tin [type=radio]:checked').val() == 'B') {
                $(this).find('.tax-tin .tax-tin-reason').show();
             }

            if ($(this).find('select').val() == 'Singapore') {
                residentialAddressIsTaxCountry = true;
            }
        });
        $('[data-question-group=no_tax_country]').toggle(!residentialAddressIsTaxCountry);

        $('.tax-country select').change(function () {
            //if ($(this).val() != '' && $(this).val() != 'Singapore') {
            if ($(this).val() != '') {
                $(this).parents('.tax-country-wrapper').find('.tax-tin').show();
            } else {
                $(this).parents('.tax-country-wrapper').find('.tax-tin').hide();
            }

            residentialAddressIsTaxCountry = false;
            $('.tax-country select').each(function (index, el) {
                if ($(el).val() == 'Singapore') {
                    residentialAddressIsTaxCountry = true;
                }
            });
            $('[data-question-group=no_tax_country]').toggle(!residentialAddressIsTaxCountry);
        });
        $('.tax-tin [type=radio]').change(function () {
            if ($(this).val() == 'B') {
                $(this).parents('.tax-tin').find('.tax-tin-reason').show();
            } else {
                $(this).parents('.tax-tin').find('.tax-tin-reason').hide();
            }
        });

        $('input[name="applicant_bo"]').change(function () {
            $('[data-question-group=bo_details]').toggle($(this).val() == 'N');
        });
        if ($('#id_applicant_bo_option2').is(':checked')) {
            $('[data-question-group=bo_details]').show();
        }

        // Toggle proof-of-address upload based on "residential same as on NRIC?"
        // no need to check if Singaporean, as this is handled on backend
        $('[name=applicant_address_registered]').change(function () {
            var isSameAddressAsNric = $('[name=applicant_address_registered]:checked').val() === 'Y';
            var $hiddenSection = $('[data-question-group=proof-of-address]');

            if (!isSameAddressAsNric) {
                $hiddenSection.show();
            } else {
                $hiddenSection.hide();
            }
        });
    }

    function step12() {
        $('input[name="declare_accept5"]').change(function () {

            if ($('input[name="declare_accept5"]').is(':unchecked')) {
                $('#marketingmodal').modal('show');
            }

        });

        $('#marketingmodal .marketingmodal-yes, #marketingmodal .close').click(function () {
            $('#marketingmodal').modal('hide');
        });

        $('#marketingmodal .marketingmodal-no').click(function () {
            $('input[name="declare_accept5"]').prop('checked', true);
            $('#marketingmodal').modal('show');
        });
    }

    function step14() {
        $('input[type="file"]').checkFileType({
            allowedExtensions: ['jpg', 'jpeg'],

            error: function() {
                alert('Only jpg/jpeg files allowed');
                $(this).text('Upload');
            }
        });
        
    }

    function step15() {
        $('input[name="feedback_recommend"]').change(function () {
            $('[data-question-group=feedback_norecommend]').toggle($(this).val() == 'N');
        });
        if ($('#id_feedback_recommend_option2').is(':checked')) {
            $('[data-question-group=feedback_norecommend]').show();
        }

        $('.rating span').click(function () {
            $(this).siblings().removeClass('hover');
            $(this).addClass('hover');
            $(this).parent().siblings('input[type=hidden]').val(5 - $(this).index());
        });
    }

    function resetLogoutTimeout() {
        clearTimeout(logoutDisplayTimeout);
        clearTimeout(logoutTimeout);
        logoutDisplayTimeout = setTimeout(displayLogoutWarning, logoutDisplayDelay);
    }

    function displayLogoutWarning() {
        clearTimeout(logoutDisplayTimeout);
        $('#buynow-timeout-modal').modal('show');
        logoutTimeout = setTimeout(function () {
            $.post('/our-plans/direct-value-term/dropout', function () {
                location.href = '/our-plans/direct-value-term/';
            });
        }, logoutDelay);
    }

    function displayDropoutModal() {
        $('#buynow-dropout-modal').modal('show');
    }

    (function($) {
        $.fn.checkFileType = function(options) {
            var defaults = {
                allowedExtensions: [],
                success: function() {},
                error: function() {}
            };
            options = $.extend(defaults, options);

            return this.each(function() {

                $(this).on('change', function() {
                    var value = $(this).val(),
                        file = value.toLowerCase(),
                        extension = file.substring(file.lastIndexOf('.') + 1);

                    if ($.inArray(extension, options.allowedExtensions) == -1) {
                        options.error();
                        $(this).focus();
                    } else {
                        options.success();

                    }

                });

            });
        };

    })(jQuery);

}(window, document));
