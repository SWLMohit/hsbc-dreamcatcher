/**
 *
 * A common place for all of the tracking convenience functions
 *
 * These methods are called by event handlers
 *
 */


(function(window, document) {

    'use strict';

    var library,
        TMS = window.TMS;

    if (!TMS) {
        console.warn('TMS Tracking library was not found.');

        /**
         * Stub out a fake TMS so that registered events can be viewed by developers
         */
        TMS = {
            trackEvent: function(opts) {
                console.warn('trackEvent:', opts);
            }
        };
    }

    function trackEvent(opts) {
        // Wrap the main TMS handler to avoid exceptions interfering with our application
        try {
            TMS.trackEvent(opts);
        } catch (e) {
            console.warn('TMS.trackEvent() threw an exception', e);
            console.log('Options used', opts);
        }
    }

    function makeFormLifetimeHandler(category) {
        return function toolInteractions(opts) {
            var options = {
                event_category: category,
                event_action: opts.action,  // Start/Finish/Error
                event_content: opts.content,  // specs would indicate perhaps values from the quick quote?

			};

            if (opts.action === 'error') {
                options.event_error = opts.field;
            } else {
                options.event_form = opts.field;
            }

            trackEvent(options);
        };
    }

    library = window.trackingLibrary = {
        /**
         *
         * DIRECT-ValueTerm Quick Quote
         *
         */
        bannerInteraction: function(opts) {
            // Get a quote, Retrieve a form, Make a claim
            trackEvent({
                event_category: 'Click to Action',
                event_action: ' Click',
                event_content: opts.linkName
            });
        },
        fileDownloads: function(opts) {
            trackEvent({
                event_category: 'click',
                event_action: 'download',
                event_content: opts.filename
            });
        },
        feedbackInteractions: function(opts) {
            trackEvent({
               event_category: 'click',
               event_action: 'Feedback',
               event_content: opts.isPositive ? 'Useful' : 'NotUseful'
            });
        },

        /**
         *
         * DIRECT-ValueTerm Form
         *
         */
        applicationProgress: makeFormLifetimeHandler('Application Form'),

        feedbackPage: makeFormLifetimeHandler('Form Feedback'),


        /**
         * EClaims Form
         */

        eclaimsProgress: makeFormLifetimeHandler('Claim Form'),

        /**
         * Quotes and Calculators
         */
        toolInteractions: makeFormLifetimeHandler('Tool Interaction'),
        
        // Contact Us
        formInteractions: makeFormLifetimeHandler('Form Interaction'),
        
        // Retrieve
        formRetrieve: makeFormLifetimeHandler('Form')

    };

}(this, this.document));







