var logoutDisplayTimeout = null;
var logoutTimeout = null;
var logoutDisplayDelay = 30 * 60000;
var logoutDelay = 2 * 60000;
// var logoutDisplayDelay = 10000;
// var logoutDelay = 5000;
var shouldWarnFormExit = true;
var isIE11 = /Trident.*rv[ :]*11\./.test(navigator.userAgent);
var ciRiderFlag = true;
var errorCount = null;

			/*payment frequency change method
			 * Called when the mode of payment is changed*/
			function paymentFrequencyChange(){
				var premiumRate=parseFloat($("#premium-table .premium-table").attr("premiumrate"));
				var premiumRateUnitAmount=parseFloat($("#premium-table .premium-table").attr("premiumrateunitamount"));
				var actualdiscount=parseFloat($("#premium-table .premium-table").attr("actualdiscount"));
			      /*calculation on the basis of payment frequency*/
			      var sumAssuredMax=parseInt($("[name=sum_assured]").val().replace(/,/g,''));
			      if(!isNaN()){
			    	  sumAssuredMax=0;
			      }
			      var paymentFrequency=$("[name=payment_mode]").val();
			      var modalFactor=getModalFactor(paymentFrequency);
			      var frequencyName=getPaymentName(paymentFrequency);
			      $("#payment-method").text(frequencyName);
			      
			      var adjustedPremium=((premiumRate / premiumRateUnitAmount) - (actualdiscount));
					var underwriting=parseFloat($("#premium-table .premium-table").attr("underwritingclassFactor"));
					adjustedPremium=adjustedPremium*underwriting*sumAssuredMax;
					var newBasic = Math.round(adjustedPremium * modalFactor * 100) / 100;
					var basicValue=parseFloat(newBasic.toFixed(2));
					$("#basic_mode").text("S$"+newBasic.toFixed(2));
					$("[name=basic]").val(basicValue);
					var riderHidden=$("#cirider-row").is(":hidden");
					var total=basicValue;
					var ropHidden=$("#roprider-row").is(":hidden");
					if(!riderHidden){
						
						var riderSumAssuredMax = parseInt($("[name=rider_sum_assured]").val().replace(/,/g,''));
						var ciadjustedPremium=parseFloat($("#premium-table .premium-table").attr("ciadjustedPremium"));
						adjustedPremium=ciadjustedPremium*riderSumAssuredMax;
						var newCiRider = adjustedPremium* modalFactor;
						newCiRider=Math.round(newCiRider * 100) / 100;
						var ciValue=parseFloat(newCiRider.toFixed(2));
						$("#ci_mode").text("S$"+newCiRider.toFixed(2));
						
						
						if(!isNaN(ciValue)){
							total=ciValue+total;
						}
						
					}
					if(!ropHidden){
						var rop= parseFloat($("#rop_mode").text().replace('S$', ''));
						
							total=total+rop;
						
					}
					
			      $("#total").text("S$"+total.toFixed(2));
				}

$(document).ready(function() {
	
				
				/*
			     * Makes a ajax call to the backend
			     * to get the latest discount rates 
			     * */
				function ajaxPaymentCall(sumAssuredMax){
					 $.ajax({
			            	type: 'POST',
			                url: "/our-plans/online-protector/calculation",
			                data: {
			                    "sum_assured": sumAssuredMax
			                    
			                },
			                success: function (resultData) {
			                	var actualdiscount=resultData["actualDiscount"];             
			                	$("#premium-table .premium-table").attr("actualdiscount",actualdiscount);
			                	var premiumRate=parseFloat($("#premium-table .premium-table").attr("premiumrate"));
			                	var premiumRateUnitAmount=parseFloat($("#premium-table .premium-table").attr("premiumrateunitamount"));
			                	var adjustedPremium=((premiumRate / premiumRateUnitAmount) - (actualdiscount));
			                	$("#premium-table .premium-table").attr("adjustedPremium",adjustedPremium);
			                	$("[name=actualdiscount]").val(actualdiscount);
			                	paymentChange(sumAssuredMax);
			                	
			                }
			            	
			            })
					
				}
				/*
				 * Calculates the latest premium values
				 * based on the input term ,sum assured,payment frequecy and 
				 * ci rider selected
				 */
				function paymentChange(sumAssuredMax){
					var paymentFrequency=$("[name=payment_mode]").val();
				    var modalFactor=getModalFactor(paymentFrequency);
					var adjustedPremium=parseFloat($("#premium-table .premium-table").attr("adjustedPremium"));
					var underwriting=parseFloat($("#premium-table .premium-table").attr("underwritingclassFactor"));
					adjustedPremium=adjustedPremium*underwriting*sumAssuredMax;
					var newBasic = Math.round(adjustedPremium * modalFactor * 100) / 100;
					var basicValue=parseFloat(newBasic.toFixed(2));
					$("#basic_mode").text("S$"+newBasic.toFixed(2));
					var riderHidden=$("#cirider-row").is(":hidden");
					var total=basicValue;
					var ropHidden=$("#roprider-row").is(":hidden");
					if(!riderHidden){
						var ci= parseFloat($("#ci_mode").text().replace('S$', ''));
						
							total=ci+basicValue;
						
					}
					if(!ropHidden){
						var rop= parseFloat($("#rop_mode").text().replace('S$', ''));
						
							total=total+rop;
						
					}
					
				    $("#total").text("S$"+total.toFixed(2));
				    $("[name=basic]").val(basicValue);
				    $("#premium-table table.data-table tbody td").css("visibility","visible");
				    $("#premium-table #basic_mode").parent().parent().find("td").first().text("Basic Plan");
				   $("#premium-table #basic_mode").parent().parent().find("td").first().css("background","#EDEDED");
				   $("#premium-table tbody").css("background","#EDEDED");
				}
    if (!$('section.onlineprotector').length) {
        return;
    }

    setTimeout(function () {

        var errorCount = $('.error-message').length;

        if (errorCount > 0 && !$('#quick_quote.onlineprotector').length && $('.buynow-form').length) {
            $('.error-message').closest('form').prepend('<div class="visuallyhidden focusable aria-error-message" aria-atomic="true" tabindex="-1">' + errorCount + ' errors found</div>');

            setTimeout(function () {
                $(document).find('.aria-error-message').attr('aria-live', 'assertive').focus();
            }, 400);
        }

    }, 500);
    

    function displayErrorMessageQQ() {

        setTimeout(function () {
            var errorCount = $('.form-group.error').length;

            if (errorCount > 0) {

                if ( $('.aria-error-message').length ) {
                    $('.aria-error-message').removeAttr('aria-live').text( + errorCount + ' errors found');
                } else {
                    $('.form-group.error').closest('form').prepend('<div class="visuallyhidden focusable aria-error-message" aria-atomic="true" tabindex="-1">' + errorCount + ' errors found</div>');                    
                }

                setTimeout(function () {
                    $(document).find('.aria-error-message').attr('aria-live', 'assertive').focus();
                }, 400);
            }
        }, 500);
        
    }

    $('form [data-action=quote]').click(function () {
        displayErrorMessageQQ();
    });


    //allow enter or space keys to toggle pretty checkboxes
    $(document).on('keypress', 'label.checkbox', function(e) {
        if(e.keyCode == 13 || e.keyCode == 32) {
            e.preventDefault();
            $(this).find('input[type=checkbox]').trigger('click');

        }
    });
	
    $('form [data-form-action=submit]').click(function (evt) {
        evt.preventDefault();
		var obj = $(this);
		
		setCookie('onlineprotector-retrieve', 'true');
		
		obj.parents('form').submit();
		shouldWarnFormExit = false;
		window.onbeforeunload = null;
		flagOnlineprotectorSubmit = false;
    });

    if ( $('section.verification').hasClass('formnotfound')) {
        $('#modalformnotfound').modal('show');
    }

    if ( $('section.verification').hasClass('formnotfoundsimple')) {
        $('#modalformnotfoundsimple').modal('show');
    }

    if ( $('section.verification').hasClass('modalcodesent')) {
        $('#modalcodesent').modal('show');
    }

    if ( $('section.verification').hasClass('modalcodesentnew')) {
        $('#modalcodesentnew').modal('show');
    }

    if ( $('section.verification').hasClass('modalformsent')) {
        $('#modalformsent').modal('show');
    }

    if ( $('section.verification').hasClass('modalinvalidcode')) {
        $('#modalinvalidcode').modal('show');
    }

	$('form.buynow-form [data-form-action="accept"]').click(function(evt) {
		evt.preventDefault();
		var obj = $(this);

		TMS.trackEvent({
			'event_category': 'Form Interaction',
			'event_action':   'Start',
			'event_form':     'Form Start',
			'event_content':  'Online Protector Application Form (I Agree)'
		});

		if ($('[data-current-step="7.0"]').length && $('input[name=include_ci_rider]').is(':unchecked') && ciRiderFlag) {
            $('#ridermodal').modal('show');
        } else {
			setTimeout(function (evt) {
				obj.attr("disabled", "disabled");
				window.onbeforeunload = null;
				$('form.buynow-form').submit();
				shouldWarnFormExit = false;
			}, 2000);
		}
    });
	
	var formOnlineprotectorStep = $('[data-form-name="onlineprotector-step"]'),
		dataOnlineprotectorStep = formOnlineprotectorStep.find('input, select, textarea, .selectBox'),
		startOnlineprotectorStep = false;
	
	if (formOnlineprotectorStep.length > 0) {
		dataOnlineprotectorStep.focus(function () {
			if (!startOnlineprotectorStep) {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Start',
					'event_form':     'Form Start',
					'event_content':  'Online Protector Application Form (By Now)'
				});
				startOnlineprotectorStep = true;
			}
		});
		
		setTimeout(function () {
			$('.error').each(function () {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Error',
					'event_error':    $(this).data('field-name'),
					'event_content':  'Online Protector Application Form (By Now)'
				});
			});
		}, 2000);
	}
	
	$('form.buynow-form [data-form-action=next]').click(function() {


        if ($('[data-current-step="7.0"]').length && $('input[name=include_ci_rider]').is(':unchecked') && ciRiderFlag) {
            $('#ridermodal').modal('show');

        } else {
			setCookie('onlineprotector-form', 'true');
            $(this).attr("disabled", "disabled");
            window.onbeforeunload = null;
            $('form.buynow-form').submit();
            shouldWarnFormExit = false;
            return false;
		}
	});
	
	if (getCookie('onlineprotector-form')) {
		setTimeout(function () {
			if ($('.error').length === 0) {
				TMS.trackEvent({
					'event_category': 'Form Interaction',
					'event_action':   'Finish',
					'event_form':     'Form Finish',
					'event_content':  'Online Protector Retrieve'
				});
				deleteCookie('onlineprotector-form');
			}
		}, 2000);
	}
	
    $('form.buynow-form [data-form-action="prev"]').click(function() {
        window.location = $(this).data('url');
        window.onbeforeunload = null;
        shouldWarnFormExit = false;
        return false;
    });
    $('form.buynow-form [data-form-action="reject"]').click(function() {
		TMS.trackEvent({
			'event_category': 'Form Interaction',
			'event_action':   'Finish',
			'event_form':     'Form Finish',
			'event_content':  'Online Protector Application Form (I Disagree)'
		});
        shouldWarnFormExit = false;
        window.onbeforeunload = null;
        displayDropoutModal();
        return false;
    });


    $('input[type=file]').on('change', function() {
        if(this.files[0].size > 1000000){
           alert("The file is over 1MB. Please upload a smaller file.");
           this.value = "";
        }
    });


    if ($('.slider-coverage').length) {
        var coverageSlider = $('.slider-coverage')[0];
        noUiSlider.create(coverageSlider, {
            start: $('input[name=sum_assured]').val(),
            step: 100,
            connect: [true, false],
            padding: 100,
            tooltips: [
                wNumb({
                    decimals: 0,
                    prefix: 'S$',
                    thousand: ','
                })
            ],
            range: {
                min: $(coverageSlider).data('min') - 100,
                max: $(coverageSlider).data('max') + 100
            },
            pips: {
                mode: 'values',
                values: [$(coverageSlider).data('min'), $(coverageSlider).data('max')],
                density: 20,
                format: wNumb({
                    decimals: 0,
                    prefix: 'S$',
                    thousand: ','
                })
            },
            format: wNumb({
                decimals: 0,
                prefix: '',
                thousand: ','
            })
        });
        var sliderTouched=false;
        coverageSlider.noUiSlider.on('set', function (values, handle) {
        	console.log("the end is called")
			if(sliderTouched){
        	sliderTouched=false;
        		setTimeout(function(){
        			if(!sliderTouched){
						var sumAssuredMax=parseInt($("[name=sum_assured]").val().replace(/,/g,''));
        				sumAssured=parseInt(values[handle].replace(/,/g, ''));
						if(sumAssured==sumAssuredMax){
        				  ajaxPaymentCall(sumAssured);
						}
        			}
        		}, 2000);
			}
        	
        });
        coverageSlider.noUiSlider.on('update', function (values, handle) {
            $('input[name=sum_assured]').val(values[handle]);
			var sumAssuredMax = parseInt(values[handle].replace(/,/g, ''));
			sliderTouched=true;
			
			$("table.data-table tbody td").css("visibility","hidden");
			$("#basic_mode").parent().parent().find("td").first().text("");
			$("#basic_mode").parent().parent().find("td").first().css("visibility","visible")
			$("#basic_mode").parent().parent().find("td").first().css("background","url('/assets/images/HSBC_loadingspinner.gif') no-repeat 70%");
			$("#basic_mode").parent().parent().find("td").first().css("background-size","10% 100%");
			
            var ciSumAssuredMax = $(riderCoverageSlider).data('max');
            sumAssuredMax = (sumAssuredMax > ciSumAssuredMax) ? ciSumAssuredMax : sumAssuredMax;
            if (typeof riderCoverageSlider != 'undefined') {
                riderCoverageSlider.noUiSlider.updateOptions({
                    range: {
                        'min': $(riderCoverageSlider).data('min') - 100,
                        'max': sumAssuredMax + 100
                    }
                });
                riderCoverageSlider.noUiSlider.pips({
                    mode: 'values',
                    values: [$(riderCoverageSlider).data('min'), sumAssuredMax],
                    density: 20,
                    format: wNumb({
                        decimals: 0,
                        prefix: 'S$',
                        thousand: ','
                    })
                });
            }
        });

        coverageSlider.noUiSlider.on('update', function(values, handle) {
        //    $('input[name=rider_sum_assured]').val(values[handle]);
            var sumAssuredMax = parseInt(values[handle].replace(/,/g, ''));
            var riderSumAssured = $('input[name=rider_sum_assured]').val();
            var riderSumAssuredClean = parseInt(riderSumAssured.replace(/,/g, ''));
            var ciSumAssuredMax = $(riderCoverageSlider).data('max');
            var moneyFormat = wNumb({
                thousand: ','
            });
            sumAssuredMax = (sumAssuredMax > ciSumAssuredMax) ? ciSumAssuredMax : sumAssuredMax;


        });


        $('input[name=sum_assured].sumAssured').on('change', function () {
       	   var sumAssuredMax=parseInt($("[name=sum_assured]").val().replace(/,/g,''));
            var paymentFrequency=$("[name=payment_mode]").val();
           
           
       });


        if (isIE11) {
            $('input[name=sum_assured]').on('mouseenter mouseleave', function () {
                coverageSlider.noUiSlider.set($(this).val());
            });

            $('input[name=sum_assured]').on('keyup', function () {
                if (event.keyCode === 13) {
                    coverageSlider.noUiSlider.set($(this).val());
                }
            });
        }
        else {
            $('input[name=sum_assured]').on('change', function () {
                coverageSlider.noUiSlider.set($(this).val());
            });
        }

        $('.noUi-handle', this).removeAttr('tabindex');
    }

    $("#premium-table table.data-table tbody td").css("visibility","visible");
    $("#premium-table #basic_mode").parent().parent().find("td").first().text("Basic Plan");
   $("#premium-table #basic_mode").parent().parent().find("td").first().css("background","#EDEDED");
   $("#premium-table tbody").css("background","#EDEDED");

    if ($('.slider-rider-coverage').length) {
        var riderCoverageSlider = $('.slider-rider-coverage')[0];

        var maxRiderValue=parseFloat($(riderCoverageSlider).data('max')) ;
        var coverageText=$('input[name=sum_assured]').val();
        var coverageSliderVal = parseInt($('input[name=sum_assured]').val().replace(/,/g, ''));

        if(coverageSliderVal<maxRiderValue){
            maxRiderValue=coverageSliderVal;
            maxRiderText=coverageText;
        }else{
            maxRiderText=maxRiderValue+"";
        if(maxRiderText.length>3){
            maxRiderText=maxRiderText.substr(0,3)+","+maxRiderText.substr(3);
            }
        }
        noUiSlider.create(riderCoverageSlider, {
            start: $('input[name=rider_sum_assured]').val(),
            step: 100,
            connect: [true, false],
            padding: 100,
            tooltips: [
                wNumb({
                    decimals: 0,
                    prefix: 'S$',
                    thousand: ','
                })
            ],
            range: {
                min: $(riderCoverageSlider).data('min') - 100,
                max: maxRiderValue + 100
            },
            pips: {
                mode: 'values',
                values: [$(riderCoverageSlider).data('min'), coverageSliderVal],
                density: 20,
                format: wNumb({
                    decimals: 0,
                    prefix: 'S$',
                    thousand: ','
                })
            },
            format: wNumb({
                decimals: 0,
                prefix: '',
                thousand: ','
            })
        });

        var initialValue=$(".slider-rider-coverage .noUi-pips .noUi-value")[1];

        if(initialValue!=undefined){
            $(initialValue).removeClass("noUi-value").css("display","none");
            $(initialValue).removeClass("noUi-value")
        }

        $('.slider-rider-coverage .noUi-pips').append('<div class="noUi-value noUi-value-horizontal noUi-value-large new-value" style="left: 99.9833%;">S$'+maxRiderText+'</div>');

        riderCoverageSlider.noUiSlider.on('update', function(values, handle) {
            $('input[name=rider_sum_assured]').val(values[handle]);
            
            var riderSumAssuredMax = parseInt(values[handle].replace(/,/g, ''));
            var paymentFrequency=$("[name=payment_mode]").val();
	        var modalFactor=getModalFactor(paymentFrequency);
			var ciadjustedPremium=parseFloat($("#premium-table .premium-table").attr("ciadjustedPremium"));
			adjustedPremium=ciadjustedPremium*riderSumAssuredMax;
			var newBasic = adjustedPremium* modalFactor;
			newBasic=Math.round(newBasic * 100) / 100;
			var basicValue=parseFloat(newBasic.toFixed(2));
			$("#ci_mode").text("S$"+newBasic.toFixed(2));
			var riderHidden=$("#cirider-row").is(":hidden");
			var total=parseFloat($("#basic_mode").text().replace('S$', ''));
			var ropHidden=$("#roprider-row").is(":hidden");
			if(!riderHidden){
				
				
					total=total+basicValue;
				
			}
			if(!ropHidden){
				var rop= parseFloat($("#rop_mode").text().replace('S$', ''));
				
					total=total+rop;
				
			}
			
            $("#total").text("S$"+total.toFixed(2));
        });

        var reset=true;
        $('a[data-action=quote]').on("click",function(){

            reset=false;

        });

        if (isIE11) {
            $('input[name=rider_sum_assured]').on('mouseenter mouseleave', function () {
                riderCoverageSlider.noUiSlider.set($(this).val());
            });

            $('input[name=rider_sum_assured]').on('keyup', function () {
                if (event.keyCode === 13) {
                    riderCoverageSlider.noUiSlider.set($(this).val());
                }
            });
        }
        else {
            $('input[name=rider_sum_assured]').on('change', function(event) {
                riderCoverageSlider.noUiSlider.set($(this).val());

            });
        }

        $('.noUi-handle', this).removeAttr('tabindex');
    }

    $('input[name="include_ci_rider"]').change(function() {
        var getVal = parseInt($('input[name=sum_assured]').val().replace(/,/g, ''));
        var getMax = $('.slider-rider-coverage').data('max');
        var updateVal = (getVal > getMax) ? getMax : getVal;
        var moneyFormat = wNumb({
                thousand: ','
            });

    });

    if ($('.onlineprotector__breadcrumb').data('otp-sent') == '0' && parseInt($('.onlineprotector__breadcrumb').data('max-step')) > 4 && parseInt($('.onlineprotector__breadcrumb').data('max-step')) <= 14) {
        $('#buynow-timeout-modal').on('hidden.bs.modal', resetLogoutTimeout);
        $('body').click(resetLogoutTimeout);
        $('body').keyup(resetLogoutTimeout);
        logoutDisplayTimeout = setTimeout(displayLogoutWarning, logoutDisplayDelay);
    }

    if ($('input[type=checkbox]').length) checkBoxAria();
    if ($('#quick_quote.onlineprotector').length) quickQuote();
    if ($('[data-current-step="3.0"]').length) step3();
    if ($('[data-current-step="4.0"]').length) step4();
    if ($('[data-current-step="4.1"]').length) step4a();
    if ($('[data-current-step="5.0"]').length) step5();
    if ($('[data-current-step="7.0"]').length) step7();
    if ($('[data-current-step="8.0"]').length) step8();
    if ($('[data-current-step="11.0"]').length) step11();
    if ($('[data-current-step="12.0"]').length) step12();
    if ($('[data-current-step="14.0"]').length) step14();
    if ($('[data-current-step="15.0"]').length) step15();

    if ($('form[data-form-dropout="1"]').length) {
        displayDropoutModal();
    }

    $('#buynow-dropout-modal .close').click(function() {
        location.href = '/your-dreams/';
    });

    $('a[data-file-input]').click(function() {
        $('input#' + $(this).data('file-input')).click();
        return false;
    });

    $('input[type=file]').change(function() {
        var filename = $(this).val().replace(/^.*[\\\/]/, '');
        $('a[data-file-input="' + $(this).attr('id') + '"]').text('File selected');
    });

    /**
     * Tagging event functions
     */
    $('[data-tag-feedback-interaction]').click(function(e){
        trackingLibrary.feedbackInteractions({
            isPositive: $(this).data('tagFeedbackInteraction')
        });
    });

    /**
     * Exit button and form exit
     */

    $('#buynow-exit-modal').on('shown.bs.modal', function() {
        shouldWarnFormExit = false;
    });

    $('#buynow-exit-modal').on('hidden.bs.modal', function() {
        shouldWarnFormExit = true;
    });

    var maxStep=parseInt($('.onlineprotector__breadcrumb').data('max-step'));
    if (maxStep >= 4 && maxStep<=14) {
        window.onbeforeunload = function () {
            return 'Are you sure you want to exit?';
        }
    }

    function updateSliderRange ( min, max ) {

        riderCoverageSlider.noUiSlider.updateOptions({
            range: {
                'min': $(riderCoverageSlider).data('min'),
                'max': max + 100
            }
        });

        riderCoverageSlider.noUiSlider.pips({
            mode: 'values',
            values: [$(riderCoverageSlider).data('min'), max],
            density: 20,
            format: wNumb({
                decimals: 0,
                prefix: 'S$',
                thousand: ','
            })
        });
    }

    function checkAge() {

        $('input.date-validate').blur(function(){
      
            var bDayParse = this.value.split('/');
            if ( bDayParse[2]==0 || bDayParse[1]==0 || bDayParse[0]==0 ) {
               return;
            }
            var bDay = new Date(bDayParse[2], bDayParse[1] - 1, bDayParse[0]);
            var age = getAge(bDay);
            var today = new Date();
            var riderCoverageSlider = $('.slider-rider-coverage');
            var sumAssured = parseFloat( $("input[name='sum_assured']").val().replace(/,/g,'') );

            if (bDay > today) {
              $('#age-validation-modal').modal('show');

            } else {
              if (age > 60) {
                $('#eligibility-validation-modal').modal('show');
                $('[data-action=quote]').removeClass('primary-btn-slate').addClass('secondary-btn-slate').css('pointer-events','none');

              } else {
                $('[data-action=quote]').addClass('primary-btn-slate').removeClass('secondary-btn-slate').css('pointer-events','auto');

                if ( age >= 50 ) {
                  $('.slider-rider-coverage').data('data-max', 500000);
                  $('.slider-rider-coverage').data('max', 500000);
                  age == 50 ? $('.pr_rider_option').show() : $('.pr_rider_option').hide();
                  $("#include_pr_rider").prop("checked", false);

                   if ( sumAssured > 500000 ) {

                      var min = riderCoverageSlider.data('min') - 100;
                      var max = 500000;
                      var initialValue = $(".slider-rider-coverage .noUi-pips .noUi-value")[1];

                      updateSliderRange(min, max);

                      if ( initialValue != undefined ) {

                          $(initialValue).removeClass("noUi-value").css("display","none");
                          $(initialValue).removeClass("noUi-value")
                      }
                   }
                } else if ( age >= 19 && age <= 49 ) {
                  $('.slider-rider-coverage').data('max', 650000);
                  $('.pr_rider_option').show();

                  if ( sumAssured > 650000 ) {
                    var min = riderCoverageSlider.data('min') - 100;
                    var max = 650000;
                    var initialValue = $(".slider-rider-coverage .noUi-pips .noUi-value")[1];

                    updateSliderRange(min, max);

                    if ( initialValue != undefined ) {
                      $(initialValue).removeClass("noUi-value").css("display","none");
                      $(initialValue).removeClass("noUi-value")
                    }
                  }
                }

              }
            }
        });
    }

    function quickQuote() {
        checkAge();

        $('[data-action=quote]').on('click', function() {
            setTimeout(function() {
                if ($(".estimate-form-result").length){
                    
                    $('html, body').animate({
                        scrollTop: $(".estimate-form-result").offset().top -60
                    }, 1000);
                }
            },500);
        });

        $('#promocode').keyup(function() {
            promo = $(this).val();

            $(this).val($(this).val().replace(/ +?/g, ''));

            if(!validateAlphaNum(promo) && $(this).val() !== ""){
               showErrorMessageOn($(this));
            }

        });
    }
});


function checkBoxAria() {
  /**
    * Quick Quote: checkbox aria-checked
  */

  $('input[type=checkbox]').each(function(){
    if ($(this).is(':checked')) {
        $(this).closest('label').attr('aria-checked','true').toggleClass('checked');
      }
  });

  
  $('input[type=checkbox]').on('change', function() {
    $(this).closest('label').toggleClass('checked');

    if ( $(this).closest('label').hasClass('checked') ) {
      $(this).closest('label').attr('aria-checked','true');
    } else {
      $(this).closest('label').attr('aria-checked','false');
    }
  });
}


/**
 * Returns age from date of birth
 *
 * @param {Date} date
 * @returns {Number}
 */
function getAge(date) {
    var now = new Date();
    var age = now.getFullYear() - date.getFullYear();
    if (((now.getMonth())*100 + now.getDate() - date.getMonth()*100 - date.getDate())>0) {
        age = age+1;
    }
    return age;
};

function step3() {
    $('body').addClass('step-3');
    $('select[name="applicant_residency"]').change(function() {
        if ($(this).val() != '') {
            if ($(this).val() == 'Singaporean' || $(this).val() == 'Singapore PR') {
                $('[data-dynamic-label="applicant-nric-passport-label"]').text('NRIC');
            } else {
                $('[data-dynamic-label="applicant-nric-passport-label"]').text('Passport');
            }
        }
    });
    $('select[name="insured_residency"]').change(function() {
        if ($(this).val() != '') {
            if ($(this).val() == 'Singaporean' || $(this).val() == 'Singapore PR') {
                $('[data-dynamic-label="insured-nric-passport-label"]').text('NRIC');
            } else {
                $('[data-dynamic-label="insured-nric-passport-label"]').text('Passport');
            }
        }
    });
    $('input[name="life_insured"]').change(function() {
        $('[data-question-group=spouse]').toggle($(this).val() == 'spouse');
    });
    if ($('#id_insured_option2').is(':checked')) {
        $('[data-question-group=spouse]').show();
    }
    $('#buynow-dropout-modal .add1').show();
    $('select[name="applicant_residency"], select[name="insured_residency"]').change();
}

function step4() {
    $('input[name="applicant_multiple_nationalities"]').change(function() {
        $('[data-question-group=multiple_nationalities]').toggle($(this).val() == 'Y');
    });
    if ($('#id_multiple_nationalities_option1').is(':checked')) {
        $('[data-question-group=multiple_nationalities]').show();
    }

    if ($('select[name=applicant_nationality]').prop('selectedIndex') !== 0) {
        var nationality = $('select[name=applicant_nationality]').val(); 

        $('select[name=applicant_birth_country]').selectBox('value', nationality);
    }

    $('select[name=applicant_nationality]').change(function() {
        var nationality = $(this).val();

        $('select[name=applicant_birth_country]').selectBox('value', nationality);
    });

    $('#buynow-dropout-modal .add1').show();
}

function step4a() {
    $('input[name="insured_multiple_nationalities"]').change(function() {
        $('[data-question-group=multiple_nationalities]').toggle($(this).val() == 'Y');
    });
    if ($('#id_multiple_nationalities_option1').is(':checked')) {
        $('[data-question-group=multiple_nationalities]').show();
    }
    $('#buynow-dropout-modal .add1').show();
}

function step5() {
    $('select[name=applicant_employment]').change(function() {
        if ($('input[name=applicant_income]').val().trim().length == 0 || $('input[name=applicant_income]').val().trim() == '0') {
            if ($(this).val() != 'Salaried' && $(this).val() != 'Self-employed') {
                $('input[name=applicant_income]').val(0);
            } else {
                $('input[name=applicant_income]').val(null);
            }
        }
    });

    $('select[name=insured_employment]').change(function() {
        if ($('input[name=insured_income]').val().trim().length == 0 || $('input[name=insured_income]').val().trim() == '0') {
            if ($(this).val() != 'Salaried' && $(this).val() != 'Self-employed') {
                $('input[name=insured_income]').val(0);
            } else {
                $('input[name=insured_income]').val(null);
            }
        }
    });
}

function step7() {
	var sumAssuredJs=parseInt($("[name=sum_assured]").val().replace(/,/g,''));
	var sumAssuredJsp=parseInt($("[name=sum_assured]").attr("value").replace(/,/g,''));
	if(sumAssuredJs!=sumAssuredJsp){
		
		$("[name=sum_assured]").val(sumAssuredJsp);
		var coverageSlider = $('.slider-coverage')[0];
		coverageSlider.noUiSlider.set(sumAssuredJsp);
	}
    $('input[name="include_ci_rider"]').change(function() {
        $('[data-question-group=ci_rider]').toggle($('input[name=include_ci_rider]').is(':checked'));
        var basicValue=parseFloat($("#basic_mode").text().replace('S$', ''));
        var ropHidden=$("#roprider-row").is(":hidden");
        var ci= parseFloat($("#ci_mode").text().replace('S$', ''));
        var rop= parseFloat($("#rop_mode").text().replace('S$', ''));
        var total=0;
        if($(this).is(":checked")){
        	$("#cirider-row").show();
        	total=basicValue+ci;
        	if(!ropHidden){
					total=total+rop;	
			}
        }else{
        	total=basicValue;
        	if(!ropHidden){
				total=total-rop;	
		    }
        	$("#cirider-row").hide();
        }
        $("#total").text("S$"+total.toFixed(2));
    });
    if ($('input[name=include_ci_rider]').is(':checked')) {
        $('[data-question-group=ci_rider]').show();
        $("#cirider-row").show();
        
        var basicValue=parseFloat($("#basic_mode").text().replace('S$', ''));
        var ropHidden=$("#roprider-row").is(":hidden");
        var ci= parseFloat($("#ci_mode").text().replace('S$', ''));
        var rop= parseFloat($("#rop_mode").text().replace('S$', ''));
        var total=0;
    	total=basicValue+ci;
    	if(!ropHidden){
				total=total+rop;	
		}
    	$("#total").text("S$"+total.toFixed(2));
    }
    $('#ridermodal .btn-white').click(function(){
        ciRiderFlag = false;
        $('[data-form-action=next]').trigger('click');
    });

    
   if($("[name=payment_mode]").val().length>0){
	   var paymentFrequency=$("[name=payment_mode]").val();
	   var frequencyName=getPaymentName(paymentFrequency);
	   $("#payment-method").text(frequencyName);
	   paymentFrequencyChange();
    }
    
    $("[name=payment_mode]").on("change",function(){
         
    	paymentFrequencyChange();
          
    });
}
function getModalFactor(paymentMode){
	var factor;
	 switch (paymentMode) {
     case "12":
         factor = 0.085;
         break;
     case "4":
         factor = 0.255;
         break;
     case "2":
         factor = 0.51;
         break;
     case "1":
    	 factor = 1;
         break;
     default:
         factor = 0.085;
         break;
 }
return factor;
}
function getPaymentName(paymentMode){
	var payment;
	 switch (paymentMode) {
     case "12":
    	 payment = "Monthly";
         break;
     case "4":
    	 payment = "Quarterly";
         break;
     case "2":
    	 payment = "Semi-Annually";
         break;
     case "1":
    	 payment = "Annually";
         break;
     default:
    	 payment = "Monthly";
         break;
 }
return payment;

}
function step8() {
	/*handled on change event for the change in lifestyle values entered*/
	$("[name=uw_lifestyle_mortgage],[name=uw_lifestyle_profile],[name=uw_lifestyle_height],[name=uw_lifestyle_weight],[name=uw_lifestyle_exercise]").on("change",function(){
		console.log("the value is now changed");console.log($(this).val());console.log("all the values obtained are "+$("[name=uw_lifestyle_mortgage]").val()+"  "+$("[name=uw_lifestyle_profile]").val()+"  "+$("[name=uw_lifestyle_height]").val()+" "+$("[name=uw_lifestyle_exercise]").val())

		var  lifestyleMortgage=$("[name=uw_lifestyle_mortgage]").val();
		var profile=$("[name=uw_lifestyle_profile]").val();
		var height=$("[name=uw_lifestyle_height]").val();
		var exersice=$("[name=uw_lifestyle_exercise]").val();
		var weight=$("[name=uw_lifestyle_weight]").val();

		if(lifestyleMortgage.trim().length>0&&profile.trim().length>0&&height.trim().length>0&&weight.trim().length>0&&exersice.trim().length>0){
			$.ajax({
		        type: 'POST',
		        url: "/our-plans/online-protector/recalculate",
		        data: {
		            "uw_lifestyle_questions_optin": "Y",
		            "uw_lifestyle_mortgage": lifestyleMortgage,
		            "uw_lifestyle_profile": profile,
		            "uw_lifestyle_height": height,
		            "uw_lifestyle_weight":weight,
		            "uw_lifestyle_exercise":exersice
		        },

		        success: function (resultData) {

				   var mode=parseInt($("#pay-table").attr("payment-mode"));
				   var modal=0;
				   var annual=0;
				   var cimodal=0;
				   var ciannual=0;
				   var ropmodal=0;
				   var ropannual=0;
				   if(resultData.modalPremium!=undefined){
					   modal=parseFloat(resultData.modalPremium);
					   modal=Math.round(modal*100)/100
					  $("#basic_mode").text("S$"+modal) ;

					  if(!isNaN(mode)){
						  var annual=parseFloat(mode*modal) ;
						  annual=Math.round(annual*100)/100;
						  $("#basic_annual").text("S$"+annual);
					  }
				   }
				   var ciIncluded=$("#pay-table").attr("ci");
				   if(ciIncluded=="true"){
					   if(resultData.ciRiderPremiumModal!=undefined){
					   cimodal=parseFloat(resultData.ciRiderPremiumModal);
					   cimodal=Math.round(cimodal*100)/100;
					   $("#ci_mode").text("S$"+cimodal) ;
					   if(!isNaN(mode)){
						  ciannual=parseFloat(resultData.ciRiderPremiumAnnual) ;
						  ciannual=Math.round(ciannual*100)/100;
						  $("#ci_annual").text("S$"+ciannual);
					  }
					   }
				   }
				   var ropIncluded=$("#pay-table").attr("rop");
				   if(ropIncluded=="true"){
					  if(resultData.ropRiderPremiumModal!=undefined){
					   ropmodal=parseFloat(resultData.ropRiderPremiumModal);
					   ropmodal=Math.round(ropmodal*100)/100;
					   $("#rop_mode").text("S$"+ropmodal) ;
					   if(!isNaN(mode)){
						  ropannual=parseFloat(resultData.ropRiderPremiumAnnual) ;
						  ropannual=Math.round(ropannual*100)/100;
						  $("#rop_annual").text("S$"+ropannual);
					  }
				   }
				   }
				   var totalModal=parseFloat(parseFloat(modal)+parseFloat(cimodal)+parseFloat(ropmodal));
				   var totalAnnual=parseFloat(parseFloat(annual)+parseFloat(ciannual)+parseFloat(ropannual));
					$("#summary_mode").text("S$"+totalModal);
					$("#summary_total").text("S$"+totalAnnual);
		        }
		    });
			console.log("the ajax call is now being made");

		}



		})



	$('input[name="uw_lifestyle_questions_optin"]').change(function() {
        $('[data-question-group=uw_lifestyle]').toggle($(this).val() == 'Y');
        console.log($(this).val())
		if($(this).val() == 'Y'){
         $('[name="summary"]').show();
		}else{
         $('[name="summary"]').hide();
		}
    });
    if ($('#id_uw_lifestyle_questions_optin_option1').is(':checked')) {
        $('[data-question-group=uw_lifestyle]').show();
       }else{
           $('[name="summary"]').hide();
  		}
}

function step11() {
    var totalMonths = -1;
    var years = parseInt($('input[name="applicant_address_period_stay_years"]').val());
    var months = parseInt($('input[name="applicant_address_period_stay_months"]').val());
    years = (isNaN(years) ? -1 : years);
    months = (isNaN(months) ? -1 : months);
    totalMonths = (years * 12) + months;
    if (totalMonths >= 0 && totalMonths < 60) {
        $('[data-question-group=address_less_than_5yrs]').show();
    }
    $('input[name="applicant_address_period_stay_years"], input[name="applicant_address_period_stay_months"]').change(function() {
        years = parseInt($('input[name="applicant_address_period_stay_years"]').val());
        months = parseInt($('input[name="applicant_address_period_stay_months"]').val());
        years = (isNaN(years) ? 0 : years);
        months = (isNaN(months) ? 0 : months);
        totalMonths = (years * 12) + months;
        $('[data-question-group=address_less_than_5yrs]').toggle(totalMonths < 60);
    });

    $('input[name="applicant_address_permanent"]').change(function() {
        $('[data-question-group=permanent_address]').toggle($(this).val() == 'N');
    });
    if ($('#id_applicant_address_permanent_option2').is(':checked')) {
        $('[data-question-group=permanent_address]').show();
    }

    var residentialAddressIsTaxCountry = false;
    $('.tax-country-wrapper').each(function() {
        $(this).find('.tax-tin').show();
        if ($(this).find('.tax-tin [type=radio]:checked').val() == 'B') {
            $(this).find('.tax-tin .tax-tin-reason').show();
        }
        if ($(this).find('select').val() == 'Singapore') {
            residentialAddressIsTaxCountry = true;
        }
    });
    $('[data-question-group=no_tax_country]').toggle(!residentialAddressIsTaxCountry);

    $('.tax-country select').change(function() {
        //if ($(this).val() != '' && $(this).val() != 'Singapore') {
        if ($(this).val() != '') {
            $(this).parents('.tax-country-wrapper').find('.tax-tin').show();
        } else {
            $(this).parents('.tax-country-wrapper').find('.tax-tin').hide();
        }

        residentialAddressIsTaxCountry = false;
        $('.tax-country select').each(function(index, el) {
            if ($(el).val() == 'Singapore') {
                residentialAddressIsTaxCountry = true;
            }
        });
        $('[data-question-group=no_tax_country]').toggle(!residentialAddressIsTaxCountry);
    });
    $('.tax-tin [type=radio]').change(function() {
        if ($(this).val() == 'B') {
            $(this).parents('.tax-tin').find('.tax-tin-reason').show();
        } else {
            $(this).parents('.tax-tin').find('.tax-tin-reason').hide();
        }
    });

    $('input[name="applicant_bo"]').change(function() {
        $('[data-question-group=bo_details]').toggle($(this).val() == 'N');
    });
    if ($('#id_applicant_bo_option2').is(':checked')) {
        $('[data-question-group=bo_details]').show();
    }
}

function step12() {
    $('input[name="declare_accept5"]').change(function() {

        if ($('input[name="declare_accept5"]').is(':unchecked') ) {
            $('#marketingmodal').modal('show');
        }

    });

    $('#marketingmodal .marketingmodal-yes, #marketingmodal .close').click(function() {
        $('#marketingmodal').modal('hide');
    });

    $('#marketingmodal .marketingmodal-no').click(function() {
        $('input[name="declare_accept5"]').prop('checked', true);
        $('#marketingmodal').modal('show');
    });
}


function step14() {
   $('input[type="file"]').checkFileType({
        allowedExtensions: ['jpg', 'jpeg'],

        error: function() {
            alert('Only jpg/jpeg files allowed');
            $(this).text('Upload');
        }
    });
}


function step15() {
    $('input[name="feedback_recommend"]').change(function() {
        $('[data-question-group=feedback_norecommend]').toggle($(this).val() == 'N');
    });
    if ($('#id_feedback_recommend_option2').is(':checked')) {
        $('[data-question-group=feedback_norecommend]').show();
    }

    $('.rating span').click(function() {
        $(this).siblings().removeClass('hover');
        $(this).addClass('hover');
        $(this).parent().siblings('input[type=hidden]').val(5 - $(this).index());
    });
}

function resetLogoutTimeout() {
    clearTimeout(logoutDisplayTimeout);
    clearTimeout(logoutTimeout);
    logoutDisplayTimeout = setTimeout(displayLogoutWarning, logoutDisplayDelay);
}

function displayLogoutWarning() {
    clearTimeout(logoutDisplayTimeout);
    $('#buynow-timeout-modal').modal('show');
    console.log("the dropout function is now called ");

    logoutTimeout = setTimeout(function () {
        $.post('/our-plans/online-protector/dropout', function() {
            location.href = '/our-plans/online-protector/';
        });
    }, logoutDelay);
}

function displayDropoutModal() {
    $('#buynow-dropout-modal').on('shown.bs.modal', function() {
        shouldWarnFormExit = false;
    });
    $('#buynow-dropout-modal').on('hidden.bs.modal', function() {
    	shouldWarnFormExit = true;
    });
    $('#buynow-dropout-modal').modal('show');
}

(function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);
