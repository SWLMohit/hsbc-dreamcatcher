var currentStep = 1;

(function () {
    $.widget("custom.resultsSlider", $.ui.slider, {
        _create: function () {
            this.options.value = $(this.element).data("initial-value");
            this.options.min = $(this.element).data("min");
            this.options.max = $(this.element).data("max");
            this.options.step = $(this.element).data("step") || 1;
            this._super();

            this.element.on("resultssliderslide", function (event, ui) {
                $("span", ui.handle).text(ui.value);
                $($(this).data("bind-value")).text(ui.value);
            });

        }
    });

    $(".js-rate-of-return-control").resultsSlider({
        change: function (event, ui) {
            $("input[name='expected_returns_slider']").val(ui.value);
            updateUserInput();
        }
    });

    $(".js-retirement-age-control").resultsSlider({
        change: function (event, ui) {
            $("input[name='retirement_age_slider']").val(ui.value);
            updateUserInput();
        }
    });

    $(".js-monthly-expenses-control").resultsSlider({
        slide: function (event, ui) {
            var thousandsFormatted = ui.value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            $(".js-retirement-expenses").text(thousandsFormatted);
        },
        change: function (event, ui) {
            $("input[name='retirement_monthly_expenses_slider']").val(ui.value);
            updateUserInput();
        }
    });

    if (currentStep == 1) {
        $('.calculators__btnset .back_btn').hide();
    }
    $('.help-block').hide();

    get_short_url(function(short_url) {
        if (typeof short_url != "undefined") {
            $(".calculator__share.em").attr("href", $(".calculator__share.em").attr("href") + " " + short_url);
        }
    });

    if(window.innerWidth <= 991) {
        $('.help-tooltip').attr('data-placement', 'top');
    }
    
    /* JIRA 945 */
    	
	if(sessionStorage.getItem("name")!=null){// Name will be fetched from session
		$("#id_name").val(sessionStorage.getItem("name"));
	}
		
	if(sessionStorage.getItem("age")!=null){// Age will be fetched from session
		$("#id_age").val(sessionStorage.getItem("age"));
	}

    $("#id_name").blur(function(){//Saving name in session
    	sessionStorage.setItem("name", this.value);
    });

    $("#id_age").blur(function(){//Saving age in session
    	sessionStorage.setItem("age", this.value);
    });
    
})();

function displayErrorMessageCalc() {
    setTimeout(function () {

        var errorCount = $('.help-block:visible').length;

        if (errorCount > 0) {
            if ( $('.aria-error-message').length ) {
                $('.aria-error-message').removeAttr('aria-live').text( + errorCount + ' errors found');
            } else {
                $('.help-block:visible').closest('.splitter__alpha').prepend('<div class="visuallyhidden aria-error-message" aria-atomic="true" tabindex="-1">' + errorCount + ' errors found</div>');
            }

            setTimeout(function () {
                $(document).find('.aria-error-message').attr('aria-live', 'assertive').focus();
            }, 400);
        }

    }, 500);
}


function updateUserInput() {

    //get vars
    var retirementAge = parseFloat($("input[name='retirement_age_slider']").val());
    var expectedReturns = parseFloat($("input[name='expected_returns_slider']").val()); //double
    var retiredMonthlyExpenses = parseFloat($("input[name='retirement_monthly_expenses_slider']").val());

    //ajax to connect to calc
    var saveData = $.ajax({
        type: 'POST',
        url: "",
        data: {
            "rateOfReturn": expectedReturns,
            "retirementAge": retirementAge,
            "retiredMonthlyExpense": retiredMonthlyExpenses,
            "update": "true"
        },
        dataType: 'text',
        success: function (resultData) {

            var json = JSON.parse(resultData);
            var shortfall = json.shortfall;

            //update the shortfall amount
            $(".js-amount").text(parseFloat(shortfall).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,").toString());
            $(".js-years-in-retirement").text(json.yearsInRetirement);
            $(".js-retirement-age").text(json.retirementAge);
            $(".js-years-to-retirement").text(json.yearsToRetire);

        }
    });
}

function updatePage() {
    if ($("[data-protection-calculator-value]").length) {

        if (protectionCalculator.coverageRequired !== undefined) {
            if (protectionCalculator.coverageRequired <= 0) {
                $('.protection-gap').hide();
                $('.protection-surplus').show();
            } else {
                $('.protection-gap').show();
                $('.protection-surplus').hide();
            }
        }

        $("[data-protection-calculator-value]").each(function () {
            var field = $(this).data("protection-calculator-value");
            var value = protectionCalculator[field];
            $(this).text(value);
        });

    }else if($("[data-savings-calculator-value]").length){
        $("[data-savings-calculator-value]").each(function () {
            var field = $(this).data("savings-calculator-value");
            var value = savingsCalculator[field];
            $(this).text(value);
        });
    }else if($("[data-education-calculator-value]").length) {

        if(educationCalculator.result !== undefined){
            if(educationCalculator.result > 0){
                $('.education-surplus').hide();
                $('.education-gap').show();
            }else{
                $('.education-surplus').show();
                $('.education-gap').hide();
            }
        }

        $("[data-education-calculator-value]").each(function () {
            var field = $(this).data("education-calculator-value");
            var value = educationCalculator[field];
            $(this).text(value);
        });
    }else if($("[data-retirement-calculator-value]").length) {

        if(retirementCalculator.result !== undefined){
            if(purifyNum(retirementCalculator.result) < 0){
                $('.retirement-surplus').hide();
                $('.retirement-gap').show();
            }else{
                $('.retirement-surplus').show();
                $('.retirement-gap').hide();
            }
        }

        $("[data-retirement-calculator-value]").each(function () {
            var field = $(this).data("retirement-calculator-value");
            var value = retirementCalculator[field];
            $(this).text(value);
        });

    }
}

function switchPage($this){

    var step = parseFloat($this.data("activate-step"), 10);
    history.replaceState({}, document.title, $this.data('navigate'));

    $($this.data("hide")).addClass("hide");
    $($this.data("show")).removeClass("hide");

    $("[data-step]").removeClass("active");
    $("[data-step=" + step + "]").addClass("active print-visible");


    $("[data-step=" + step + "]").find("input").first().focus();

    setTimeout(function() {
        $("[data-step=" + step + "]").attr('aria-live', 'polite');
    }, 500);


    updatePage();

    if(step < currentStep){
        $(".calculator_breadcrumb button")
            .slice(step)
            .removeClass("current")
            .attr("aria-disabled", "true")
            .removeAttr("aria-current");
    }else{
        $(".calculator_breadcrumb button").attr("aria-disabled", "true").removeAttr("aria-current");
        var $crumbs = $(".calculator_breadcrumb button").slice(0, step);
            $crumbs.addClass("current");
            $crumbs.removeClass("disabled");

            $(".calculator_breadcrumb button.current:nth-of-type("+ step + ")").removeAttr("aria-disabled").attr("aria-current", "true");
    }

    /* update mobile bread crumb */
    $(".calculator_breadcrumb button.current").removeClass("live");
    $(".calculator_breadcrumb button.current:nth-of-type("+ step + ")").addClass("live");

    if(step != 1){
        $('.back_btn').show();
    }
    currentStep = step;

    if(window.innerWidth <= 991){
        if ($(".splitter__alpha").length) {
            $('html, body').stop().animate({
                'scrollTop': $('.splitter__alpha:visible').offset().top -15
            }, 700);
        }
    }
}


function pluraliseYears() {
    if($('.calculator_step.active').children().hasClass('calculator__results')){
        
        $('.calculator__results .years').each(function(index) {
            $instance = $(this);
            years = $instance.closest('li').find('strong');
            yearsVal = years.text();

            if (yearsVal < 2) {
                $instance.text('Year');

            } else {
                if ($instance.hasClass('quote')) {
                    $instance.text("Years'");

                } else {
                    $instance.text('Years');

                }
                
            }
        });
    }
}


function purifyNum(obj){
    var result = obj.replace(/[^0-9\.\-]/g,"");
    return isNaN(parseFloat(result)) ? '' : parseFloat(result);
}

/* year range slider start */

var $yearslider = $('input[data-type="year-programmatic-range"]');
var $year = $('input[data-type="year-programmatic-range-number"]');

if($yearslider.length != 0){
    $yearslider.rangeslider({
        polyfill: false,
        onInit: function () {

        },
        onSlide: function (pos, value) {
            $year[0].value = value;
            hideErrorMessageOn($year);
        }
    });
}

$year.on('input', function () {

    var amt = parseFloat(this.value);

    if (!isNaN(parseFloat(amt))) {
        var maxAmt = parseFloat(purifyNum($(".year-range").parent().find(".max").text()),10);
        var minAmt = parseFloat(purifyNum($(".year-range").parent().find(".min").text()),10);
        if (amt >= minAmt && amt <= maxAmt) {
            hideErrorMessageOn($(this));

            $yearslider.val(amt).change();
        } else {
            showErrorMessageOn($(this));
        }
    }

});

/* year range slider end */

/* currency range slider start */

var $currencyslider = $('input[data-type="currency-programmatic-range"]');
var $currency = $('input[data-type="currency-programmatic-range-number"]');

if($currencyslider.length != 0){
    $currencyslider.rangeslider({
        polyfill: false,
        onInit: function () {

        },
        onSlide: function (pos, value) {
            $currency[0].value = value.toLocaleString();
            hideErrorMessageOn($currency);
        }
    });
}

$currency.on('input', function () {

    var amt = parseFloat(purifyNum($(this).val()));

    if (!isNaN(parseFloat(amt))) {
        var maxAmt = parseFloat(purifyNum($(".currency-range").parent().find(".max").text()),10);
        var minAmt = parseFloat(purifyNum($(".currency-range").parent().find(".min").text()),10);
        if (amt >= minAmt && amt <= maxAmt) {
            hideErrorMessageOn($(this));

            var stepSize = $currencyslider.attr('step');
            if (amt < stepSize && amt > 0){
                $currencyslider.val(stepSize).change();

            }else{
                $currencyslider.val(amt).change();
            }
            $(this).val(amt);
        }
    }

});

/* currency range slider end */
/* percentage range slider start */

var $percentageslider = $('input[data-type="percentage-programmatic-range"]');
var $percentage = $('input[data-type="percentage-programmatic-range-number"]');

if($percentageslider.length != 0){
    $percentageslider.rangeslider({
        polyfill: false,
        onInit: function () {

        },
        onSlide: function (pos, value) {
            $percentage[0].value = value;
            hideErrorMessageOn($percentage);
        }
    });
}

$percentage.on('keyup', function (e) {

    /* don't do anything when decimals are input*/
    if(e.keyCode == 110 || e.keyCode == 190){
        return;
    }
    var amt = parseFloat(this.value);

    if (!isNaN(parseFloat(amt))) {
        var maxAmt = parseFloat(purifyNum($(".percentage-range").parent().find(".max").text()));
        var minAmt = parseFloat(purifyNum($(".percentage-range").parent().find(".min").text()));
        if (amt >= minAmt && amt <= maxAmt) {
            hideErrorMessageOn($(this));

            var stepSize = $percentageslider.attr('step');
            if (amt < stepSize && amt > 0){
                $percentageslider.val(stepSize).change();

            }else{
                $percentageslider.val(amt).change();
            }
        }
    }

});

/* percentage range slider end */

$(".js-next").click(function (e) {
    e.preventDefault();
    $(this).closest(".calculator_step").removeClass("primed");
    if(window.innerWidth <= 991) {
        $('.js-continue').removeClass('btn-white');
        $('.js-continue').addClass('btn-red');
    }
    switchPage($(this));
    if($('.calculator_step.active').attr("data-step") == "1"){
        $('.back_btn').hide();
    }else{
        $('.back_btn').show();
    }

    pluraliseYears();

    $('.aria-error-message').remove();
});

$(".calculator_breadcrumb button").click(function (e) {
    e.preventDefault();
    if(!$(this).hasClass('disabled')){
        switchPage($(this));
    }
});


$(".js-continue").click(function (e) {
    e.preventDefault();
    var $formControls = $(this).closest("form").find(".form-group");

    var validate = inputFilled($formControls);
    displayErrorMessageCalc();

    if(!validate){
        return false;
    }
    if($("#id_duration").length){
        protectionCalculator.name = $("#id_name").val();
        protectionCalculator.age = parseFloat($("#id_age").val(), 10);
        protectionCalculator.income = parseFloat(purifyNum($("#id_income").val()), 10);
        protectionCalculator.liabilities = parseFloat(purifyNum($("#id_liabilities").val()), 10);
        protectionCalculator.insuranceCoverage = parseFloat(purifyNum($("#id_insurance_coverage").val()), 10);
        protectionCalculator.duration = parseFloat($("#id_duration").val(), 10);
    }else if($("#id_goal").length){
        savingsCalculator.name = $("#id_name").val();
        savingsCalculator.age = parseFloat($("#id_age").val(), 10);
        savingsCalculator.lumpSum = parseFloat(purifyNum($("#id_goal").val()), 10);
        savingsCalculator.duration = parseFloat($("#id_length").val(), 10);
        savingsCalculator.rateOfReturn = parseFloat($("#id_rate_of_return").val());
        savingsCalculator.inflation = parseFloat($("#id_inflation").val());
    }else if($("#id_course_fees").length){
        educationCalculator.name = $("#id_name").val();
        educationCalculator.age = parseFloat($("#id_age").val(), 10);
        educationCalculator.childName = $("#id_child_name").val();
        educationCalculator.childAge = parseFloat($("#id_child_age").val(), 10);
        educationCalculator.childFutureAge = parseFloat($("#id_child_age_funds_needed").val(), 10);
        educationCalculator.courseFees = parseFloat(purifyNum($("#id_course_fees").val()), 10);
        educationCalculator.liabilities = parseFloat(purifyNum($("#id_liabilities").val()), 10);
        educationCalculator.rateOfReturn = parseFloat($("#id_rate_of_return").val());
        educationCalculator.inflation = parseFloat($("#id_inflation").val());
        educationCalculator.savings = parseFloat(purifyNum($("#id_savings_edu").val()), 10);
    }else if($("#id_monthly_expenses").length){
        retirementCalculator.name = $("#id_name").val();
        retirementCalculator.age = parseFloat($("#id_age").val(), 10);
        retirementCalculator.retirementAge = parseFloat($("#id_retirement_age").val(), 10);
        retirementCalculator.monthlyIncome = parseFloat(purifyNum($("#id_income").val()), 10);
        retirementCalculator.monthlyExpenditure = parseFloat(purifyNum($("#id_spending").val()), 10);
        retirementCalculator.liabilities = parseFloat(purifyNum($("#id_liabilities").val()), 10);
        retirementCalculator.existingAssets = parseFloat(purifyNum($("#id_investment_worth").val()), 10);
        retirementCalculator.rateOfReturn = parseFloat($("#id_rate_of_return").val());
        retirementCalculator.inflation = parseFloat($("#id_inflation").val());
        retirementCalculator.retiredMonthlyExpenses = parseFloat(purifyNum($("#id_monthly_expenses").val()), 10);
        retirementCalculator.lumpsum = parseFloat(purifyNum($("#id_expenses").val()), 10);
    }

    updatePage();

    $(this).closest(".calculator_step").addClass("primed");

    if(window.innerWidth <= 991){
        $(this).removeClass('btn-red');
        $(this).addClass('btn-white');

        if ($(".calculator__form__beta").length) {
            $('html, body').stop().animate({
                'scrollTop': $(this).closest('.calculator__box').find('.calculator__form__beta').offset().top -15
            }, 700);
        }
    }
});

$(".js-print").click(function(){
    window.print();
});

$('.js-facebook-share').click(function (e) {
    e.preventDefault();
    var url = location.href;
    url = url.substring(0, url.indexOf('#'));
    window.open("https://www.facebook.com/sharer.php?u=" + url, 'Share on Facebook', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
});

$('.js-tw-share').click(function (e) {
    e.preventDefault();
    var url = location.href;
    url = url.substring(0, url.indexOf('#'));
    var text = encodeURIComponent($(this).attr("data-sharing-message"));
    window.open("https://twitter.com/share?url=" + url + "&text=" + text, 'Share on Twitter', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
});

$(".js-recalculate").click(function () {
    var $formControls = $(this).closest(".calculator__results__beta_wrap").find(".form-group");
    var validate = inputFilled($formControls);

    if ($("#id_duration").length) {
        protectionCalculator.duration = parseFloat($("#id_duration").val(), 10);
    } else if ($("#id_goal").length) {
        savingsCalculator.duration = parseFloat($("#id_years").val(), 10);
        savingsCalculator.lumpSum = parseFloat(purifyNum($("#id_savings").val()), 10);
    } else if ($("#id_course_fees").length) {
        educationCalculator.courseFees = parseFloat(purifyNum($("#id_cost").val()), 10);
    } else if ($("#id_monthly_expenses").length) {
        retirementCalculator.rateOfReturn = parseFloat($("#id_rate_of_return_slider").val());
        retirementCalculator.retiredMonthlyExpenses = parseFloat(purifyNum($("#id_monthly_expenses_slider").val()), 10);
        retirementCalculator.retirementAge = parseFloat($("#id_retirement_age_slider").val(), 10);
    }

    if (validate) {
        updatePage();
        pluraliseYears();

        if(window.innerWidth <= 991){
            if ($(".splitter__alpha").length) {
                $('html, body').stop().animate({
                    'scrollTop': $('.splitter__alpha:visible').offset().top -15
                }, 700);
            }
        }
    }
    
});


$("input.form-control").on('input',function () {
    if ($(this).val()) {
        var formGroup = $(this).closest(".form-group");
        if(!formGroup.find('.secondary-error:visible').length){
        hideErrorMessageOn($(this));
        }
    }
});

$(".currency-text-box .number-only-input").on('input', function () {
    var position = this.selectionStart;
    var oldValue = $(this).val();

    if (oldValue.length && !Number.isNaN(oldValue)) {
        var val = parseFloat(purifyNum(oldValue), 10).toLocaleString();
        $(this).val(val);

        setTimeout(function () {
            this.selectionStart = this.selectionEnd = position + (val.length > oldValue.length ? 1 : 0);
        }.bind(this), 10);
    }
});

$(".two-digit-text-box").on('input',function (e) {
    var val = $(this).val();
    if (val.length >= 2) {
        $(this).val(purifyNum($(this).val()).toString().substr(0, 2));
    }
});


$(".two-decimal-number-input").on('input',function () {
    var newValue = $(this).val();
    var previousValue = $(this).data('old_value') || '';
    if((newValue.length - previousValue.length) > 1) { //check for copy paste

        $(this).val(previousValue);
        return;
    }

    if (!validateNDecimal(newValue, 2)) { //normal checks
        newValue = sanitizeNDecimal(newValue, 2);
    }

    $(this).val(newValue);
    $(this).data('old_value', newValue);
});

$(".one-decimal-number-input").on('input',function () {
    var newValue = $(this).val();
    var previousValue = $(this).data('old_value') || '';
    if((newValue.length - previousValue.length) > 1) { //check for copy paste

        $(this).val(previousValue);
        return;
    }

    if (!validateNDecimal(newValue, 1)) { //normal checks
        newValue = sanitizeNDecimal(newValue, 1);
    }

    $(this).val(newValue);
    $(this).data('old_value', newValue);
});

$("#id_name, #id_child_name, #id_country").on('input',function () {

    var val = $(this).val();
    if(!validateName(val)){
       $(this).val(sanitizeName(val));
   }
});

$("#id_name, #id_child_name, #id_country").on('input',function () {
    var val = $(this).val();
    if(!(/^([A-Za-z '\-])+$/.test(val))){
        showErrorMessageOn($(this));
    }
});

$("input").on('input',function (e) {
    var val = $(this).val();
    if (val.length > 99) {
        e.preventDefault();
        $(this).val($(this).val().substr(0, 99));
    }
});

$("#id_rate_of_return_slider, #id_rate_of_return").on('input',function () {
    var val = $(this).val();
    var formGroup = $(this).closest(".form-group");
    hideErrorMessageOn($(this));
    if(val.length){
        if (val > 20){
            formGroup.addClass("has-error");
            formGroup.find('.rate-over-20').show();
        }
    }else{
        showErrorMessageOn($(this));
    }
});

$("#id_age:not(.savings_age), #id_retirement_age_slider").on('input',function(e){
    var val = $(this).val();
    var formGroup = $(this).closest(".form-group");
    hideErrorMessageOn($(this));
    if(val.length){

        if(val >= 83){
            formGroup.addClass("has-error");
            formGroup.find('.age-out-of-range').show();
        }else{
        	var age = $('#id_age').val();
            var retirementAge = val;

            if(age.length){
                if( age != '' && retirementAge != ''){
                    if(parseInt(age, 10) > parseInt(retirementAge, 10)){
                        formGroup.addClass("has-error");
                        formGroup.find('.help-block.age-smaller-than').show();
                    }else
                    {
                        formGroup.removeClass("has-error");
                        formGroup.find('.help-block').hide();
                    }
                }
            }
        }
    }
});

/* START PROTECTION CALCULATOR SPECIFIC */
var protectionCalculator = {
    duration: 25,
    age: 0,
    liabilities: 0,
    insuranceCoverage: 0,
    income: 0,
    get coverageToFund() {
        return this.duration * this.income;
    },
    get coverageRequired() {
        if (this.duration === 0) {
            return 0;
        }
        var required = this.coverageToFund + this.liabilities - this.insuranceCoverage;

        return required;
    },
    get endingAge() {
        return this.age + this.duration
    },
    get absCoverageRequired(){
        return Math.abs(Math.round(parseFloat(this.coverageRequired.toFixed(2)))).toLocaleString();
    }
};

$("#id_dependants").change(function () {
    $('input[data-type="year-programmatic-range"]').val($(this).val()).change();
    $('#id_duration.slider-text-box').val($(this).val());
});

$("#id_dependants, #id_duration").on('input',function(e){
    var val = $(this).val();
    var formGroup = $(this).closest(".form-group");
    hideErrorMessageOn($(this));
    if(val.length){

        if(val > 50){
            formGroup.addClass("has-error");
            formGroup.find('.dependants-out-of-range').show();
        }
    }
});
/* END PROTECTION CALCULATOR SPECIFIC */

/* START SAVINGS CALCULATOR SPECIFIC */

var savingsCalculator = {
    duration: 10,
    age: 0,
    lumpSum: 0,
    rateOfReturn: 0,
    inflation: 0.02,
    get normalisedInflation(){
        return this.inflation /100;
    },
    get monthlyGrowthRate(){
        var normalisedRate = this.rateOfReturn/100;
        if(this.rateOfReturn == normalisedRate)
        {
            return 0;
        }
        return ((normalisedRate - this.normalisedInflation) / 12);
    },
    get totalMonths(){
        return this.duration * 12;
    },
    get annuityFactor(){
        if (this.monthlyGrowthRate === 0){
            //cant divide 0 by 12. server will explode
            return 144;
        }
        return (1-( Math.pow((1 + this.monthlyGrowthRate),-(this.totalMonths)) )) / this.monthlyGrowthRate;
    },
    get monthlyCommitment(){
        if (this.duration === 0) {
            return this.lumpSum;
        }
        var monthly = this.lumpSum / this.annuityFactor;
        if (monthly <= 0) {
            return 0
        }
        return Math.round(parseFloat(monthly.toFixed(2))).toLocaleString();
    },
    get endingAge() {
        return this.age + this.duration;
    }
};

$("#id_length").change(function () {
    $('input[data-type="year-programmatic-range"]').val($(this).val()).change();
    $('#id_years.slider-text-box').val($(this).val());
});

$('#id_course_fees, #id_goal, #id_monthly_expenses').on('focusout', function () {
    $(this).trigger('change');
});

$(document).on('change','#id_goal', function () {
    var val = purifyNum($(this).val());
    $('input[data-type="currency-programmatic-range"]').val(val).change();
    $('#id_savings.slider-text-box').val(val.toLocaleString());
});

// $("#id_years, #id_length").on('input',function(e){
//     var val = $(this).val();
//     var formGroup = $(this).closest(".form-group");
//     hideErrorMessageOn($(this));
//     if(val.length){
//
//         if(val >= 99){
//             formGroup.addClass("has-error");
//             formGroup.find('.savings-years-out-of-range').show();
//         }
//     }
// });
$("#id_savings, #id_goal").on('input',function(e){
    if($(this).val() !== ''){
        var val = purifyNum($(this).val());
        var formGroup = $(this).closest(".form-group");
        hideErrorMessageOn($(this));

        if(val == 0 || val > 5000000){
            formGroup.addClass("has-error");
            formGroup.find('.savings-out-of-range').show();
        }
    }
});
/* END SAVINGS CALCULATOR SPECIFIC */

/* START EDUCATION CALCULATOR SPECIFIC */
var educationCalculator = {
    age: 0,
    childAge: 0,
    childFutureAge: 0,
    courseFees: 0,
    liabilities: 0,
    savings: 0,
    inflation: 0.02,
    get normalisedInflation(){
        return this.inflation /100;
    },
    rateOfReturn: 0,
    get normalisedRateOfReturn(){
      return this.rateOfReturn/100;
    },
    get yearsToFunding(){
        return parseFloat(this.childFutureAge - this.childAge).toFixed(1);
    },

    get fvCourseCost(){
        var res = (this.courseFees * Math.pow((1+this.normalisedInflation),this.yearsToFunding));
        return res;
    },
    get fvCurrentFunds(){
        var res = this.savings * Math.pow((1+this.normalisedRateOfReturn),this.yearsToFunding);
        return res;
    },
    get fvLiabilities(){
        var res = this.liabilities * Math.pow((1+this.normalisedRateOfReturn),this.yearsToFunding);
        return res;
    },
    get totalFV(){
        var res = this.fvLiabilities + this.fvCourseCost - this.fvCurrentFunds;
        return res;
    },
    get result(){
        // var res = (this.liabilities - this.savings) + (this.fvCourseCost/(Math.pow((1 + this.normalisedInflation), this.yearsToFunding)));
        var res = this.totalFV/(Math.pow(1+this.normalisedRateOfReturn,this.yearsToFunding));
        return Math.round(res);
    },
    get absResult(){
        return Math.abs(parseFloat(this.result.toFixed(2))).toLocaleString();
    }
};

$("#id_course_fees").change( function () {
    var val = purifyNum($(this).val());
    $('input[data-type="currency-programmatic-range"]').val(val).change();
    $("#id_cost.slider-text-box").val(val.toLocaleString());
    if(val != ""){
	    var val1 = val +"";
		var val1Length = val1.length;
		if(val1Length <= 7 && val1 <= 1000000){
        hideErrorMessageOn($(this));
        }
    }
});

$('.calculator__fields select.control_country_select').on('change', function () {
    var country = $(this).val();
    var target = $("#id_course_fees");
    $("#id_country").hide();
    hideErrorMessageOn($(this));
    if(country === "Singapore"){
        target.val("218,009").change();
        $('.input-group-addon').text("S$");
    }else if(country === "Australia"){
        target.val("227,259").change();
        $('.input-group-addon').text("S$");
    }else if(country === "UK"){
        target.val("241,922").change();
        $('.input-group-addon').text("S$");
    }else if(country === "US"){
        target.val("253,157").change();
        $('.input-group-addon').text("S$");
    }else{
        $("#id_country").show();
        target.val("Nil").change();
        console.log(this)
        $(this).closest('.calculator__fields').find('.input-group-addon').text("");
        	
    }
});
$("#id_course_fees, #id_cost").on('input',function(e){
    if($(this).val() !== ''){
        var val = purifyNum($(this).val());
        var formGroup = $(this).closest(".form-group");
        hideErrorMessageOn($(this));

        if(val > 1000000){
            formGroup.addClass("has-error");
            formGroup.find('.cost-out-of-range').show();
        }
    }
});

$("#id_child_age, #id_child_age_funds_needed").on('input', function (e) {
    var childAge = $('#id_child_age').val();
    var childAgeNeeded = $('#id_child_age_funds_needed').val();
    if( childAge != '' && childAgeNeeded != ''){
       if(parseFloat(childAge) > parseFloat(childAgeNeeded)){
           showSecondaryErrorMessageOn($('#id_child_age_funds_needed'));
       }else
       {
           hideSecondaryErrorMessageOn($('#id_child_age_funds_needed'));
       }
    }
});
/* END EDUCATION CALCULATOR SPECIFIC */

/* START RETIREMENT CALCULATOR SPECIFIC */

var retirementCalculator = {
    age: 0,
    retirementAge: 0,
    get yearsToRetire(){

        return parseFloat(this.retirementAge - this.age).toFixed(1);
    },
    endOfRetirement: 83,
    get yearsInRetirement(){
        return this.endOfRetirement - this.retirementAge;
    },
    monthlyIncome: 0,
    get monthlyIncome60(){
        return (this.monthlyIncome * 0.6).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    },
    get monthlyIncome70(){
        return (this.monthlyIncome * 0.7).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    },

    get annualisedIncome(){
        return this.monthlyIncome * 12;
    },
    monthlyExpenditure: 0,
    get monthlySavings(){
        return this.monthlyIncome - this. monthlyExpenditure;
    },
    get annualisedSavings(){
        return this.monthlySavings * 12;
    },
    liabilities: 0,
    existingAssets: 0,
    rateOfReturn: 0,
    inflation: 0.02,
    get normalisedInflation(){
        return this.inflation /100;
    },
    retiredMonthlyExpenses: 0,
    lumpsum: 0,
    get estimatedNetWorth(){
        var fv = FV(this.rateOfReturn/100,this.yearsToRetire,this.annualisedSavings,(this.existingAssets - this.liabilities),false) * -1;
        return fv;
    },
    get requiredMonthlyIncome(){
        return this.retiredMonthlyExpenses * (Math.pow((1+this.normalisedInflation),this.yearsToRetire));
    },
    get lumpSumRequired(){
        return this.lumpsum * (Math.pow((1+this.normalisedInflation),this.yearsToRetire));

    },
    get presentValue(){
        var pv = (PV(((1+(this.rateOfReturn/100))/(1+this.normalisedInflation)-1),this.yearsInRetirement - 1,(this.requiredMonthlyIncome * 12), 1, false) * -1) + (this.requiredMonthlyIncome *12);
        return pv;
    },
    get result(){
        var calculate1 = parseFloat((this.estimatedNetWorth - (this.lumpSumRequired + this.presentValue)).toFixed(2));
        var calculate2 = 0.0;
        if (calculate1>=0) {
          calculate2 = Math.round(calculate1);
        } else {
          calculate2 = Math.ceil(calculate1);
        }
        return calculate2.toLocaleString();
    },
    get absResult(){
        return Math.abs(purifyNum(this.result)).toLocaleString();
    }

};

/* converted from org.apache.poi.ss.formula.functions.FinanceLib java library*/
function FV(r, n, y, p, t) {
    var retval = 0.0;
    if (r == 0.0) {
        retval = -1.0 * (p + n * y);
    } else {
        var r1 = r + 1.0;
        retval = (1.0 - Math.pow(r1, n)) * (t ? r1 : 1.0) * y / r - p * Math.pow(r1, n);
    }

    return retval;
}

function PV(r, n, y, f, t) {
    var retval = 0.0;
    if (r == 0.0) {
        retval = -1.0 * (n * y + f);
    } else {
        var r1 = r + 1.0;
        retval = ((1.0 - Math.pow(r1, n)) / r * (t ? r1 : 1.0) * y - f) / Math.pow(r1, n);
    }
    return retval;
}

$("#id_rate_of_return").change( function () {
    var val = $(this).val();
    $('input[data-type="percentage-programmatic-range"]').val(val).change();
    $("#id_rate_of_return_slider.slider-text-box").val(val);
});
$("#id_retirement_age").change( function () {
    var val = $(this).val();
    $('input[data-type="year-programmatic-range"]').val(val).change();
    $("#id_retirement_age_slider.slider-text-box").val(val);
});
$("#id_monthly_expenses").change( function () {
    var val = purifyNum($(this).val());
    $('input[data-type="currency-programmatic-range"]').val(val).change();
    $("#id_monthly_expenses_slider.slider-text-box").val(val.toLocaleString());
});
$(".retirement__calc #id_retirement_age, .retirement__calc #id_age").change(function(){
    var val = $(this).val();
    var formGroup = $(this).closest(".form-group");
    hideErrorMessageOn($(this));
    if(val.length){

        if(val >= 83){
            formGroup.addClass("has-error");
            formGroup.find('.age-out-of-range').show();
        }else{
            var age = $('#id_age').val();
            var retirementAge = $('#id_retirement_age').val();

            var formGroupRetirement = $('#id_retirement_age').closest(".form-group");

            if(age.length){
                if( age != '' && retirementAge != ''){
                    if(age > retirementAge){
                        formGroupRetirement.addClass("has-error");
                        formGroupRetirement.find('.help-block.age-smaller-than').show();
                    }else
                    {
                        formGroupRetirement.removeClass("has-error");
                        formGroupRetirement.find('.help-block').hide();
                    }
                }
            }
        }
    }
});
$('#id_inflation').change(function () {
    $('.retirement-surplus button').attr('aria-label','Based on an inflation rate of '+ $(this).val() +'%.');
    $('.retirement-surplus button').attr('data-content','Based on an inflation rate of '+ $(this).val() +'%.');
    $('.retirement-gap button').attr('aria-label','Based on an inflation rate of '+ $(this).val() +'%.');
    $('.retirement-gap button').attr('data-content','Based on an inflation rate of '+ $(this).val() +'%.');
});

$("#id_monthly_expenses, #id_monthly_expenses_slider").on('input',function(e){
    if($(this).val() !== ''){
        var val = purifyNum($(this).val());
        var formGroup = $(this).closest(".form-group");
        hideErrorMessageOn($(this));

        if(val > 500000){
            formGroup.addClass("has-error");
            formGroup.find('.mth-spending-out-of-range').show();
        }
    }
});
/* END RETIREMENT CALCULATOR SPECIFIC */


$('[data-toggle="popover"]').focus(function () {
    $(this).popover("show");
}).blur(function () {
    $(this).popover("hide");
});


function get_short_url(func)
{
    $.getJSON(
        "http://api.bitly.com/v3/shorten?callback=?",
        {
            "format": "json",
            "apiKey": "R_eb75601c81e24c02a987f97f48b81e7e",
            "login": "tran342",
            "longUrl": location.href
        },
        function(response)
        {
            func(response.data.url);
        }
    );
}

/* START CONTACT FORM*/
$('document').ready(function(){
    initToggleComplaintFields();
});

function initToggleComplaintFields() {
    var form = $('#contact-us-form');
    var optionFields = form.find('.radio input');
    var complaintFields =  form.find('.complaint-related');

    // hide complaint fields on load
    complaintFields.each(function(i,el){
        $(el).hide();

    });

    // event handler on clicking option fields
    // toggle display of complaint fields
    optionFields.on('click.toggleComplaint', function(e){
        e.stopPropagation();
        console.log(this.value);

        if (this.value === 'complaint') {
            console.log('complaint field');
            $('.contact-us-form-fields').find('.primary-btn-slate').hide();
            // todo: toggle open complaint box
            complaintFields.each(function(i,el) {
                $(el).slideDown();
            });
        } else {
            // todo: collapse complaint box
            $('.contact-us-form-fields').find('.primary-btn-slate').show();
            complaintFields.each(function(i,el) {
                $(el).slideUp();
            });
        }
    });
}
/* END CONTACT FORM*/

/* START: Click button and focus updated element for screen reader */
$('document').ready(function(){

  // Step 1 button
  $('[data-step=1] .calculators__btnset > button[type=submit]').click(function() {
    setTimeout(function() {
      $('.calculator__form__beta__inner').focus();
    }, 1000);
  });

  // Step 2 button
  $('[data-step=2] .calculators__btnset > button[type=submit]').click(function() {
    setTimeout(function() {
      $('.calculator__form__beta__inner').focus();
    }, 1000);
  });

  // Step 3 button
  $('[data-step=3] .calculators__btnset > button[type=submit]').click(function() {
    setTimeout(function() {
      $('.calculator__form__beta__inner').focus();
    }, 1000);
  });
});
/* END: Click button and focus updated element for screen reader */


var formStarted = false;
$('[data-tracking-start]').click(function(e){
    if(!formStarted){
        trackingLibrary.toolInteractions({
            action: "Start",
            field: "Form Start",
            content: $(this).attr('name')
        });
        formStarted = true;
    }
});

$('button.js-finish').click(function(e){
    trackingLibrary.toolInteractions({
        action: "Finish",
        field: "Form Finish",
        content: $(this).attr('name')
    });
});

$(document).ready(function () {

    if ($('.rangeslider__handle').length) {

        $('.rangeslider__handle').closest('.form-group').attr({'tabindex': '-1', 'aria-hidden' : 'true'});

    } else {
        setTimeout(function() {
            $('.rangeslider__handle').closest('.form-group').attr({'tabindex': '-1', 'aria-hidden' : 'true'});
        },500);
    }
});
