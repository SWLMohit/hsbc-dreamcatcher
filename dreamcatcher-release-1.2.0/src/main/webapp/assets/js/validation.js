
function validateModifiers(e){
    // Allow: backspace, delete, tab, escape and enter
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, up, down
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return true;
    }
    return false;
}
function validateNumbers(e){
    // Ensure that it is a number and stop the keypress
    // if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    //     return false;
    // }
    //
    // if(e.originalEvent.data != null){
    //     if(/\D/.test(e.originalEvent.data)){ //for mobile
    //         return false;
    //     }
    // }
    //
    // return true;

    return /^\d+$/.test(e);
}

function validateArrows(e){
    if(e.keyCode < 37 || e.keyCode > 40) {
        return false;
    }
    return true;
}

function validateNDecimal(val, n){

    var re = new RegExp("^(\\d{1,2})(?:(\\.)(\\d{0,"+ n +"}))?$");

    return re.test(val);
};

function sanitizeNDecimal(val, n){
    val = purifyNum(val);
    var reg = new RegExp("^(\\d{1,2})(?:\\d)*(?:(\\.)(\\d{0,"+ n +"})(?:\\d)*)?$");

    return val.toString().replace(reg,"$1$2$3");
}

function validateName(val){
    // Ensure that it is a letter or dash or space or '
    // if ((
    //         (e.keyCode < 65 || e.keyCode > 90) &&
    //         e.keyCode !== 189 && //dash
    //         e.keyCode !== 173 && //dash
    //         e.keyCode !== 32,
    //         e.keyCode !== 222 // '
    //      ) ||
    //      (e.keyCode === 189 && (e.shiftKey === true || e.metaKey === true)) ||
    //      (e.keyCode === 173 && (e.shiftKey === true || e.metaKey === true)) ||
    //      (e.keyCode === 222 && (e.shiftKey === true || e.metaKey === true))
    // ){
    //     return false;
    // }
    // return true;

    return /^([a-zA-Z]+)([ ][a-zA-Z]*)*$/.test(val);
}

function sanitizeName(val){
    return val.replace(/([^a-zA-Z ]+)/g,"");
}

function validateAlphaNum(val){
    //numbers or letters
    // if((
    //         (e.keyCode < 48 || e.keyCode > 57) &&
    //         (e.keyCode < 65 || e.keyCode > 90) &&
    //         (e.keyCode < 96 || e.keyCode > 105)
    //     ) ||
    //     ((e.keyCode < 65 || e.keyCode > 90) && e.shiftKey === true)
    // ){
    //     return false;
    // }
    // return true;
    return /^([a-zA-Z0-9]+)([ ][a-zA-Z0-9]*)*$/.test(val);
}

function sanitizeAlphaNum(val){
    return val.replace(/([^a-zA-Z0-9 ]+)/g,"");
}
function validateCountryCode(val){

    // var plus = input.val().split("+").length;
    //
    // if( (e.keyCode === 107) || //numpad +
    //     (e.keyCode === 187 && e.shiftKey === true) ||
    //     (e.keyCode === 61 && e.shiftKey === true)
    // ){
    //     return plus <= 1;
    // }
    //
    // if(validateNumbers(e)) {
    //     if(plus<=1){
    //         input.val("+" + input.val());
    //     }
    //     if(input.val().length > 4){
    //         return false
    //     }
    //     return true;
    // }
    // return false;

    return /^(\+)([0-9]{0,4})$/.test(val);
}

function sanitizeCountryCode(val){
    var reg = new RegExp("^(?:.)*(\\+)(?:\\D)*([0-9]{0,4})(?:.)*$");
    return val.replace(reg,"$1$2");
}

function validateEmail(val){
    var regex = new RegExp("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    return regex.test(val);
    }

function validateNRIC(val){
    return /^[STFGstfg]\d{7}[A-Za-z]$/.test(val);
}


function validateDate(event){

    var $formGroup = $(event.currentTarget).closest('.form-group__multi');

    var year = $formGroup.find('.date-year').val();
    var month = $formGroup.find('.date-month').val();
    var day = $formGroup.find('.date-day').val();

    if (day === undefined){
        day = 1;
    }

    if( !isNaN(parseInt(year)) && !isNaN(parseInt(month)) && !isNaN(parseInt(day))) {

        var timestamp = Date.parse(year + '-' + month + '-' + day);

        if (isNaN(timestamp)) {
            return false;
        } else {
            //date is valid, remove all error indicators
            hideErrorMessageOn($formGroup.find('.date-day'));

            //only pad month and day
            $formGroup.find('.date-month').val(pad(month,2));
            $formGroup.find('.date-day').val(pad(day,2));
        }

        var currDate = new Date();
        if(parseInt(year) > currDate.getFullYear()) { //year greater than current
            //do not continue with form
            return false;
        }
        if (parseInt(year) == currDate.getFullYear() && parseInt(month) > currDate.getMonth() + 1){ //month greater than current, get month returns 0-11
            //do not continue with form
            return false;
        }
        if(parseInt(year) >= currDate.getFullYear() && parseInt(month) == currDate.getMonth() + 1 && parseInt(day) > currDate.getDate()){ //date greater than current
            //do not continue with form
            return false;
        }
        // else if(parseInt(year) > currDate.getFullYear()-19){ //1999 and above @ 2017
        //     //do not continue with form
        //     $('#onlineinvestormodal').modal({show: false});
        //     $('#onlineinvestormodal').modal('show');
        // }
        return true;
    }

    return false;
}
function validateDateDropdown(event){

    var $formGroup = $(event.currentTarget).closest('.form-group__multi');

    var year = $formGroup.find('.control_year_select').val();
    var month = $formGroup.find('.control_month_select').val();
    var day = $formGroup.find('.control_day_select').val();

    if (day === undefined){
        day = 1;
    }

    if( !isNaN(parseInt(year)) && !isNaN(parseInt(month)) && !isNaN(parseInt(day))) {

        var timestamp = Date.parse(year + '-' + month + '-' + day);

        if (isNaN(timestamp)) {
            return false;
        } else {
            //date is valid, remove all error indicators
            hideErrorMessageOn($formGroup.find(".control_day_select"));
        }

        var currDate = new Date();
        if(parseInt(year) > currDate.getFullYear()) { //year greater than current
            //do not continue with form
            return false;
        }
        if (parseInt(year) == currDate.getFullYear() && parseInt(month) > currDate.getMonth() + 1){ //month greater than current, get month returns 0-11
            //do not continue with form
            return false;
        }
        if(parseInt(year) >= currDate.getFullYear() && parseInt(month) == currDate.getMonth() + 1 && parseInt(day) > currDate.getDate()){ //date greater than current
            //do not continue with form
            return false;
        }
        // else if(parseInt(year) > currDate.getFullYear()-19){ //1999 and above @ 2017
        //     //do not continue with form
        //     $('#onlineinvestormodal').modal({show: false});
        //     $('#onlineinvestormodal').modal('show');
        // }
        return true;
    }

    return false;
}

function inputFilled($formGroup){

    $formGroup.each(function () {
        if ($("select", this).length) {
            if ($("select", this).val() == "Other" && $(this).find("input").val() == "") {
                showErrorMessageOn($(this).find("input"));
            }
        }else
        if ($("input", this).length) {
            if (!$("input", this).val()) {
                showErrorMessageOn($(this));
            }
        }
    });

    if ($formGroup.filter(".has-error").length) {

        //send tracking error events
    	$('.has-error').each(function() {
		var objError = $(this),
			type = objError.data('name'),
			formError = objError.find('input'),
			formNameError = formError.attr('id');

		trackingLibrary.toolInteractions({
		    action: "error",
		    field: formNameError,
		    content: type
		});
	});

        return false;
    }
    return true;
}

$(".number-only-input:not([name='policyowner_contact_country'])").on('input',function (e) {
    if(!validateNumbers(e)){
        if(!validateModifiers(e) ){
            e.preventDefault(); //ignored by mobile

            var sanitizedVal = $(this).val().replace(/\D/g, "");
            $(this).val(sanitizedVal);
        }
    }
});

$("input.number-only-input").on('input', function (e) {
    var position = this.selectionStart;

    if (!validateModifiers(e)) {
        var val = $(this).val();
        if (val.length >= 13) {
            var start = val.substr(0, position - 1);
            var end = val.substr(position, val.length);

            $(this).val(start + end);

            setTimeout(function () {
                this.selectionStart = this.selectionEnd = position;
            }.bind(this), 10);
        }
    }
});

$("input.number-only-input").on('input',function (e) {
    if (!validateModifiers(e)) {
        var val = $(this).val();
        if (val.length >= 13) {
            e.preventDefault();
            $(this).val($(this).val().substr(0, 13));
        }
    }
});


$(".date-day, .date-month, .date-year").not('.date-credit-card').change(function (e){
    var $group = $(this).closest('.form-group__multi');
    if( $group.find(".date-month").val().length && $group.find(".date-year").val().length){
       if(!validateDate(e)){
           showErrorMessageOn($(this));
       }
   }
});
$("select.control_day_select, select.control_month_select, select.control_year_select").change(function (e){
    var $group = $(this).closest('.form-group__multi');
    if($group.find(".control_month_select").val() !== null && $group.find(".control_year_select").val() !== null){
       if(!validateDateDropdown(e)){
           showErrorMessageOn($(this));
       }
    }
});
$(".date-day, .date-month").on('input',function () {
    var val = $(this).val();
    if (val.length >= 2) {
        $(this).val(val.substr(0, 2));
    }
});
$(".date-year").on('input',function () {
    var val = $(this).val();
    if (val.length >= 4) {
        $(this).val(val.substr(0, 4));

    }
});

/* country code */
$(".form-group__multi--phone span:nth-of-type(1) input").on('input', function (e) {
    var position = this.selectionStart;
    var val = $(this).val();

    if (!validateCountryCode(val)) {
        val = "+" + val;
        $(this).val(sanitizeCountryCode(val));
    }
    if (val.length > 5) {
        var start = val.substr(0, position - 1);
        var end = val.substr(position - 1, val.length);

        $(this).val(sanitizeCountryCode(start + end));

        setTimeout(function () {
            this.selectionStart = this.selectionEnd = position;
        }.bind(this), 10);
    }
});

/* phone number */
$(".form-group__multi--phone span:nth-last-of-type(1) input").on('input', function (e) {
    var position = this.selectionStart;
    var val = $(this).val();
    if(validateNumbers(val)) {
        if (val.length > 10) {
            var start = val.substr(0, position - 1);
            var end = val.substr(position, val.length);

            $(this).val(start + end);

            setTimeout(function () {
                this.selectionStart = this.selectionEnd = position;
            }.bind(this), 10);
        }
    }else{
        $(this).val()
    }
});

$('.alphanum-input').on('input',function () {
    var val = $(this).val();
    if(!validateAlphaNum(val)){
        $(this).val(sanitizeAlphaNum(val));
    }
});


$('select.control_country_select').on('change', function () {
    if($(this).val() !== "Choose one..."){
        hideErrorMessageOn($(this));
    }
});



$('input.date-validate-month').keyup(function(e){
    var key = e.charCode || e.keyCode || 0;
    $date = $(this);

    if (key !== 8) {
        if ($date.val().length === 0) {
            $date.val($date.val() + '');
        }
        if ($date.val().length === 2) {
            $date.val($date.val() + '/');
        }
    }
});

$('input.date-validate-month').blur(function(e){

    var dateSplit = this.value.split('/');

    if (dateSplit[0] > 12 ||  dateSplit[0] < 1 ) {
        showErrorMessageOn($(this));

    } else if ( dateSplit[1] && dateSplit[1].length < 4  ||  dateSplit[1] && dateSplit[1] == "0000") {
        showErrorMessageOn($(this));

    } else {
        hideErrorMessageOn($(this));
    }
});


$('input.date-clone').each(function() {

    $(this).blur(function(e){

        var dateSplit = this.value.split('/');

        if (dateSplit[0] > 31 ||  dateSplit[0] < 1 ) {
            showErrorMessageOn($(this));

        } else if (
                dateSplit[0] > 30 &&  dateSplit[1] == 2 ||
                dateSplit[0] > 30 &&  dateSplit[1] == 4 ||
                dateSplit[0] > 30 &&  dateSplit[1] == 6 ||
                dateSplit[0] > 30 &&  dateSplit[1] == 9 ||
                dateSplit[0] > 30 &&  dateSplit[1] == 11 ) {

            showErrorMessageOn($(this));

        } else if (dateSplit[0] > 28 && dateSplit[1] == 2 ) {
            showErrorMessageOn($(this));

        } else if (dateSplit[1] > 12 ||  dateSplit[1] < 1 ) {
            showErrorMessageOn($(this));

        } else if ( dateSplit[2] && dateSplit[2].length < 4 ||  dateSplit[2] && dateSplit[2] == "0000") {
            showErrorMessageOn($(this));

        } else {
            hideErrorMessageOn($(this));
        }
    });
});


$('input.date-validate').keyup(function(e){

    var key = e.charCode || e.keyCode || 0;
    $date = $(this);

    if (key !== 8) {
        if ($date.val().length === 0) {
            $date.val($date.val() + '');
        }
        if ($date.val().length === 2) {
            $date.val($date.val() + '/');
        }
        if ($date.val().length === 5) {
            $date.val($date.val() + '/');
        }
    }
});


$('input.date-validate').on('change', function() {
    dateFieldValidation();
})

$('input[name=date]').on('change', function() {
    dateFieldValidationQQ();
})


function dateFieldValidationQQ() {
    $('#quick-quote-form').validate({
        rules : {
            'applicant_dob' : {
                required : false,
                dateITA : true
            }
        },
        messages : {
            date : "Please enter a valid date in the format DD/MM/YYYY"
        }
    });
}

function dateFieldValidation() {
    $('#quick-quote-form, .buynow-form').validate({
        rules : {
            date : {
                required : false,
                dateITA : true
            },
            'applicant_dob' : {
                required : false,
                dateITA : false
            },
            'insured_dob' : {
                required : false,
                dateITA : false
            },
            "illness_doctor_consult_date_day" : {
                required : false,
                dateITA : true
            },
            "policy_owner_birth_day" : {
                required : false,
                dateITA : true
            },
            "illness_symptoms_start_date_day" : {
                required : false,
                dateITA : true
            },
            "disability_last_work_date_day" : {
                required : false,
                dateITA : true
            },
            "accident_date_day" : {
                required : false,
                dateITA : true
            },
            "deceased_symptoms_present_date_day" : {
                required : false,
                dateITA : true
            },
            "illness_doctor_consult_date_day" : {
                required : false,
                dateITA : true
            }
        },
        messages : {
            date : "Please enter a valid date in the format DD/MM/YYYY",
            "applicant_dob" : "Please enter a valid date in the format DD/MM/YYYY",
            "insured_dob" : "Please enter a valid date in the format DD/MM/YYYY",
            "illness_doctor_consult_date_day" : "Please enter a valid date in the format DD/MM/YYYY",
            "policy_owner_birth_day" : "Please enter a valid date in the format DD/MM/YYYY",
            "illness_symptoms_start_date_day" : "Please enter a valid date in the format DD/MM/YYYY",
            "disability_last_work_date_day" : "Please enter a valid date in the format DD/MM/YYYY",
            "accident_date_day" : "Please enter a valid date in the format DD/MM/YYYY",
            "deceased_symptoms_present_date_day" : "Please enter a valid date in the format DD/MM/YYYY",
            "illness_doctor_consult_date_day" : "Please enter a valid date in the format DD/MM/YYYY"
        }

    });
}


function showErrorMessageOn($this) {
    var formGroup = $this.closest(".form-group");
    formGroup.addClass("has-error");
    formGroup.find('.help-block:not(.secondary-error)').show();

    if (!$('.calculator_step').length) {
        formGroup.find('.help-block:not(.secondary-error)').attr('aria-live', 'assertive');
    }
}
function hideErrorMessageOn($this) {
    var formGroup = $this.closest(".form-group");
    formGroup.removeClass("has-error");
    formGroup.find('.help-block').hide().removeAttr('aria-live');
}
function showSecondaryErrorMessageOn($this) {
    var formGroup = $this.closest(".form-group");
    formGroup.addClass("has-error");
    formGroup.find('.help-block.secondary-error').show();

    if (!$('.calculator_step').length) {
        formGroup.find('.help-block.secondary-error').attr('aria-live', 'assertive');
    }
}
function hideSecondaryErrorMessageOn($this) {
    var formGroup = $this.closest(".form-group");
    formGroup.removeClass("has-error");
    formGroup.find('.help-block.secondary-error').hide().removeAttr('aria-live');
}