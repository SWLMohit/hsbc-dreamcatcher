$(function(){
	console.log("document is now ready")
	$('.buynow-exit-button').click(function() {
		console.log("the exit now clicked")
	    $('#buynow-exit-modal').modal('show');


		var currentLocation=location.pathname;
		if( currentLocation.indexOf('eclaims') >= 0 ){
			$(".modal-body").css("display", "none");
		}

		if(currentLocation.indexOf('your-dreams') >= 0){
			$(".modal-body").css("display", "none");

			location.href = '/our-plans/';
		}

	    return false;
	});
	$('#buynow-exit-modal #exit-sure').on("click",function(){
		var currentLocation=location.pathname;
		if(currentLocation.indexOf("eclaims")==-1){
			window.onbeforeunload = null;
			var steps=currentLocation.split("/");
			var step=parseInt(steps[steps.length-1]);
			var applicationType=steps[steps.length-2];
			var maxStep=0;
			if(applicationType=="online-protector"){
				maxStep=4;
				url="/our-plans/online-protector/dropout";
			}else if(applicationType=="direct-value-term"){
				maxStep=3;
				url="/our-plans/direct-value-term/dropout";
			}else { //eclaims
				url="/";
			}
			var isFirefox=navigator.userAgent.indexOf("Firefox");
			var asyncVar=true;
			if(isFirefox!=-1){
				$(".modal-footer a,.modal-footer button").attr("disabled","disabled");
				$(".modal-footer a,.modal-footer button").css("cursor","not-allowed");
				asyncVar=false;
			}
			if(step>maxStep){
			
				$.ajax({type:'POST',url:url,async:asyncVar,sucess: function() {
			           
				 }});
				 location.href = '/';
			}else{
				location.href = '/';
			}
		}else{
			  location.href = '/';
		}
	})
});


