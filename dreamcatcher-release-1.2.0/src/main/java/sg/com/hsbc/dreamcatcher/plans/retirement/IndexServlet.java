package sg.com.hsbc.dreamcatcher.plans.retirement;

import com.google.gson.Gson;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.poi.ss.formula.functions.FinanceLib;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


@WebServlet(urlPatterns = "/our-plans/retirement/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/jsp/view/plans/retirement/index.jsp")
})
public class IndexServlet extends BaseFormServlet {

    private static Gson gson;

    @Override
    public void requestContext(HttpServletRequest request) {
        gson = new Gson();
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RetirementData data = RetirementData.getInstance();

        if (request.getParameter("submit_form") != null) {


            String name = request.getParameter("name");
            String age = request.getParameter("age");
            String retirementAge = request.getParameter("retirement_age");
            String income = request.getParameter("income");
            String monthlySpend = request.getParameter("monthly_spending");
            String liabilities = request.getParameter("liabilities");
            String existing_Investments = request.getParameter("existing_investments");
            String expected_returns = request.getParameter("expected_returns");
            String expected_inflation = request.getParameter("expected_inflation");
            String retirement_monthly_expenses = request.getParameter("retirement_monthly_expenses");
            String expected_big_ticket_expenses = request.getParameter("expected_big_ticket_expenses");

            data.setAge(Integer.parseInt(age));
            data.setRetirementAge(Integer.parseInt(retirementAge));
            data.setMonthlyIncome(Integer.parseInt(income));
            data.setMonthlySpending(Integer.parseInt(monthlySpend));
            data.setLiabilities(Integer.parseInt(liabilities));
            data.setExistingInvestments(Integer.parseInt(existing_Investments));
            data.setExpectedReturns(Double.parseDouble(expected_returns));
            data.setExpectedInflation(Double.parseDouble(expected_inflation));
            data.setRetirementMonthlyExpenses(Integer.parseInt(retirement_monthly_expenses));
            data.setExpectedBigTicketPurchases(Integer.parseInt(expected_big_ticket_expenses));
            data.setName(name);

            calculateAdditionalResults(data);

            String json = gson.toJson(data);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8"); //for IE compatibility
            response.getWriter().write(json);

            request.setAttribute("data", data);

        } else if (request.getParameter("print") != null) {

            ByteArrayOutputStream output = createPDF(request);

            response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename=retirement.pdf");
            response.getOutputStream().write(output.toByteArray());
            response.getOutputStream().flush();



        } else if (request.getParameter("update").equals("true")) {

            updateUi(request,data);

            request.setAttribute("data", data);
            String json = gson.toJson(data);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8"); //for IE compatibility
            response.getWriter().write(json);


        }
    }

    private void calculateAdditionalResults(RetirementData data) {
        int expectedEndRetirement = 83;
        int yearsToRetire = data.getRetirementAge() - data.getAge();
        int yearsInRetirement = expectedEndRetirement - data.getRetirementAge();
        int annualIncome = data.getMonthlyIncome() * 12;
        int monthlySavings = data.getMonthlyIncome() - data.getMonthlySpending();
        int annualSavings = monthlySavings * 12;

        data.setYearsToRetire(yearsToRetire);
        data.setYearsInRetirement(yearsInRetirement);
        data.setAnnualIncome(annualIncome);
        data.setMonthlySavings(monthlySavings);
        data.setAnnualSavings(annualSavings);

        double returnRate = data.getExpectedReturns() / 100;
        double yearsToRetirement = data.getRetirementAge() - data.getAge();
        double existingInvestment = data.getExistingInvestments();
        double liabilities = data.getLiabilities();
        double yearsIn = data.getYearsInRetirement();
        double retiredMonthlyExpenses = data.getRetirementMonthlyExpenses();
        double inflation = data.getExpectedInflation() / 100;

        double projectedRequiredMonthlyIncome = retiredMonthlyExpenses * (Math.pow((1 + inflation), yearsToRetirement));
        data.setProjectedMontlyIncome((int) projectedRequiredMonthlyIncome);

        double futureValue = FinanceLib.fv(returnRate, yearsToRetirement, annualSavings, (existingInvestment - liabilities), true) * -1;
        data.setFutureValue((int) futureValue);

        double presentValue = FinanceLib.pv(((1 + returnRate) / (1 + inflation) - 1), yearsIn, (projectedRequiredMonthlyIncome * 12), 1, false) * -1;
        data.setPresentValue((int) presentValue);

        int aspirationLumpSum = 2000000;
        data.setAspirationalLumpSum(aspirationLumpSum);

        double requiredLumpSum = aspirationLumpSum * (Math.pow(((1 + inflation)), yearsToRetirement));
        data.setRequiredLumpSum((int) requiredLumpSum);

        double finalResult = futureValue - (requiredLumpSum + presentValue);
        data.setShortfall((int) finalResult);

    }

    private void updateUi(HttpServletRequest request, RetirementData data) {

        double rateOfReturn = Double.parseDouble(request.getParameter("rateOfReturn"));
        data.setExpectedReturns(rateOfReturn);

        int retirementAge = Integer.parseInt(request.getParameter("retirementAge"));
        data.setRetirementAge(retirementAge);

        int retiredMonthlyExpense = Integer.parseInt(request.getParameter("retiredMonthlyExpense"));
        data.setRetirementMonthlyExpenses(retiredMonthlyExpense);

        calculateAdditionalResults(data);

    }

    public ByteArrayOutputStream createPDF(HttpServletRequest request) throws IOException {

        String[][] content = getContent(RetirementData.getInstance());

        PDDocument document;
        PDPage page;
        PDFont font;
        PDPageContentStream contentStream;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();


        // Creating Document
        document = new PDDocument();

        // Creating Pages
        page = new PDPage();
        page.setMediaBox(PDRectangle.A4); //A4 page

        // Adding page to document
        document.addPage(page);

        // Next we start a new content stream which will "hold" the to be created content.
        contentStream = new PDPageContentStream(document, page);

        float[] colWidth = {400, 0};
        drawTable(page, contentStream, 800, 20, content, colWidth);

        // Let's close the content stream
        contentStream.close();

        // Finally Let's save the PDF
        document.save(baos);
        document.close();
        return baos;
    }

    public String[][] getContent(RetirementData data) {

        //grab all the data required for pdf generation
        //retrieve all vars into a String for text writing
        String name = data.getName();
        String age = String.valueOf(data.getAge());

        String retirementAge = String.valueOf(data.getRetirementAge()); //modifiable
        String yearsToRetirement = String.valueOf(data.getYearsToRetire()); //recalc'd
        String expectedEndRetirement = String.valueOf(data.getExpectedEndRetirement()); //should be 83
        String yearsInRetirement = String.valueOf(data.getYearsInRetirement()); //recalc'd

        String income = String.valueOf(data.getMonthlyIncome());
        String annualIncome = String.valueOf(data.getAnnualIncome());
        String monthlySpending = String.valueOf(data.getMonthlySpending());
        String monthlySavings = String.valueOf(data.getMonthlySavings());
        String annualSavings = String.valueOf(data.getAnnualSavings());

        String liabilities = String.valueOf(data.getLiabilities());
        String existingInvestments = String.valueOf(data.getExistingInvestments());
        String rateOfReturn = String.valueOf(data.getExpectedReturns());
        String expectedInflation = String.valueOf(data.getExpectedInflation());

        String retirementMonthlyExpenses = String.valueOf(data.getRetirementMonthlyExpenses());

        String aspirationalLumpSum = String.valueOf(data.getAspirationalLumpSum());
        String futureValue = String.valueOf(data.getFutureValue());
        String projectedMonthlyIncome = String.valueOf(data.getProjectedMontlyIncome());
        String requiredLumpSum = String.valueOf(data.getRequiredLumpSum());
        String presentValue = String.valueOf(data.getPresentValue());
        String shortfall = String.valueOf(data.getShortfall());

        String[][] newContent = {
                {"Name", name},
                {"Age", age},
                {,},
                {"Retirement Age", retirementAge},
                {"Years to retirement", yearsToRetirement},
                {"Expected end of retirement", expectedEndRetirement},
                {"Years in retirement", yearsInRetirement},
                {,},
                {"Monthly income", income},
                {"Annualised income", annualIncome},
                {"Monthly expenditure", monthlySpending},
                {"Monthly savings", monthlySavings},
                {"Annual savings", annualSavings},
                {,},
                {"Outstanding liabilities", liabilities},
                {"Existing investments", existingInvestments},
                {"Rate of return", rateOfReturn},
                {"Expected inflation p.a.", expectedInflation},
                {,},
                {"Monthly expenses during retirement", retirementMonthlyExpenses},
                {,},
                {"Aspiration lump sum", aspirationalLumpSum},
                {,},
                {"Estimated net worth at retirement age", futureValue},
                {"Monthly income you will need due to inflation", projectedMonthlyIncome},
                {,},
                {"Lump sum required due to inflation", requiredLumpSum},
                {"Total present value of monthly income at the time of retirement", presentValue},
                {,},
                {"Retirement Surplus (Gap) at the time of retirement", shortfall}
        };
        return newContent;
    }

    public static void drawTable(PDPage page, PDPageContentStream contentStream, float y, float margin, String[][] content, float[] colWidths) throws IOException {

        final int rows = content.length;
        final int cols = content[0].length;
        final float rowHeight = 25f;
        final float tableWidth = page.getMediaBox().getWidth() - margin - margin;
        final float tableHeight = rowHeight * rows;
        final float colWidth = tableWidth / (float) cols;
        final float cellMargin = 5f;

        //draw the rows
        float nexty = y;
        for (int i = 0; i <= rows; i++) {
            contentStream.drawLine(margin, nexty, margin + tableWidth, nexty);
            nexty -= rowHeight;
        }

        //draw the columns
        float nextx = margin;
        for (int i = 0; i < cols; i++) {
            contentStream.drawLine(nextx, y, nextx, y - tableHeight);
//            nextx += (colWidths != null) ? colWidths[i] : colWidth;
//            nextx = (colWidths != null && i == 0) ? nextx + colWidths[i] : colWidths[i];

            if (colWidths != null && i == 0) {
                nextx += colWidths[i];
            } else {
                nextx = colWidths[i];
            }
//            nextx += colWidths[i];
        }

        PDFont font = PDType1Font.TIMES_ROMAN;

        float textx = margin + cellMargin;
        float texty = y - 15;
        for (int i = 0; i < content.length; i++) {
            for (int j = 0; j < content[i].length; j++) {
                String text = content[i][j];

                contentStream.setFont(font, 10);
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx, texty);

                if (text.contains("\n")) {
                    String[] lines = text.split("\n");
                    contentStream.appendRawCommands(10 + " TL\n");
                    for (int k = 0; k < lines.length; k++) {
                        contentStream.drawString(lines[k]);
                        if (k < lines.length - 1) {
                            contentStream.appendRawCommands("T*\n");
                        }
                    }
                } else {
                    contentStream.drawString(text);
                }
                contentStream.endText();
                textx += (colWidths != null) ? colWidths[j] : colWidth;
            }
            texty -= rowHeight;
            textx = margin + cellMargin;
        }
    }
}
