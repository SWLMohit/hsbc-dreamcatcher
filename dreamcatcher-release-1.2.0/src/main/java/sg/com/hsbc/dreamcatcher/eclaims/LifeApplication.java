package sg.com.hsbc.dreamcatcher.eclaims;

import be.quodlibet.boxable.BaseTable;

import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDPage;
import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

import static sg.com.hsbc.dreamcatcher.helpers.PDFGenerator.PAGE_MARGIN_BOTTOM;
import static sg.com.hsbc.dreamcatcher.helpers.PDFGenerator.PAGE_MARGIN_LEFT;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class LifeApplication extends EClaimApplication{

    protected String policyownerCity;
    protected String policyownerPostalCode;
    protected String policyownerBuildingNumber;
    protected String policyownerUnitNumber1;
    protected String policyownerUnitNumber2;
    protected String policyownerStreet1;
    protected String policyownerStreet2;
    protected String policyownerBuildingName;

    public boolean save() {
        if (this.getMaxStep() == (float)4.5) {
            this.setStatus(STATUS_COMPLETE);
        }
        return super.save();
    }

    public String getPolicyownerCity() {
        return policyownerCity;
    }

    public void setPolicyownerCity(String policyownerCity) {
        this.policyownerCity = policyownerCity;
    }

    public String getPolicyownerPostalCode() {
        return policyownerPostalCode;
    }

    public void setPolicyownerPostalCode(String policyownerPostalCode) {
        this.policyownerPostalCode = policyownerPostalCode;
    }

    public String getPolicyownerBuildingNumber() {
        return policyownerBuildingNumber;
    }

    public void setPolicyownerBuildingNumber(String policyownerBuildingNumber) {
        this.policyownerBuildingNumber = policyownerBuildingNumber;
    }

    public String getPolicyownerUnitNumber1() {
        return policyownerUnitNumber1;
    }

    public void setPolicyownerUnitNumber1(String policyownerUnitNumber1) {
        this.policyownerUnitNumber1 = policyownerUnitNumber1;
    }

    public String getPolicyownerUnitNumber2() {
        return policyownerUnitNumber2;
    }

    public void setPolicyownerUnitNumber2(String policyownerUnitNumber2) {
        this.policyownerUnitNumber2 = policyownerUnitNumber2;
    }

    public String getPolicyownerStreet1() {
        return policyownerStreet1;
    }

    public void setPolicyownerStreet1(String policyownerStreet1) {
        this.policyownerStreet1 = policyownerStreet1;
    }

    public String getPolicyownerStreet2() {
        return policyownerStreet2;
    }

    public void setPolicyownerStreet2(String policyownerStreet2) {
        this.policyownerStreet2 = policyownerStreet2;
    }

    public String getPolicyownerBuildingName() {
        return policyownerBuildingName;
    }

    public void setPolicyownerBuildingName(String policyownerBuildingName) {
        this.policyownerBuildingName = policyownerBuildingName;
    }
    
    /**
     * Overrided this function for non death eclaim types
     * PDFGenerator pd prints the pdf
     * ystart           position where table is drawn
     */
    public float writeBasicInformation(PDFGenerator pd, float yStart) throws IOException {
        PDPage page = pd.getCurrentPage();
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        float tableWidth = getTableWidth(pd);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PAGE_MARGIN_BOTTOM, tableWidth, PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);


        List<String[]> rows = new ArrayList<>();
        rows.add(new String[]{"Policy number(s)", String.join(" , ", getPolicyNumberList())});
        rows.add(new String[]{
                "Name of Policyowner",


                getPolicyownerFirstName() + ", " + getPolicyownerLastName() 
        });
        List<String> passportDocuments=getPolicyownerPassport();
        
        if (passportDocuments != null) {
            Collections.sort(passportDocuments);
            for (int i=0; i<passportDocuments.size();i++) {
              String doc=passportDocuments.get(i);
              if (!uploadedDocuments.contains(doc)) {
                uploadedDocuments.add(doc);
              }
            }

          }
        rows.add(new String[]{"NRIC of Claimant",  getPolicyownerNRIC()});
        rows.add(new String[]{"Date of Birth of Claimant",  formatDate(getPolicyownerBirthday())});
        rows.add(new String[]{"NRIC/Passport of Claimant",  "Number of documents uploaded - 1"});
        rows.add(new String[]{
                "Contact no of Claimant",
                getPolicyownerContactCountry() + " - " + getPolicyownerContactNumber()
        });


        rows.add(new String[]{"Email address of Claimant", getPolicyownerEmail()});
        rows.add(new String[]{"Confirm email address", getPolicyownerEmail()});
        String claimantAddress=getMailingAddress();
        rows.add(new String[]{
                "Mailing Address of Claimant",
                claimantAddress
        });
       
        rows.add(new String[]{"Is the Policyowner the Life Insured?", isPolicyownerLifeInsured() ? "Yes": "No"});

        
        
        
        List<String> insuredPassportDocuments = getPolicyownerLifeInsuredPassport();
        if (!isPolicyownerLifeInsured()) {
            rows.add(new String[]{"Name of Life Insured", getPolicyownerLifeInsuredFirstName() + ", " + getPolicyownerLifeInsuredLastName()});
            rows.add(new String[]{"NRIC/Passport of the Life Insured",  "Number of documents uploaded - 1"});
            Collections.sort(insuredPassportDocuments);
            for(int i=0;i<insuredPassportDocuments.size();i++){
                String doc=insuredPassportDocuments.get(i);
                if(!uploadedDocuments.contains(doc)){
                    uploadedDocuments.add(doc);
                }
            }
         }
        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
        }

        return table.draw() - 10;
    }
    /**
     * Checks if the mailing address is null or empty
     * If null or empty doesnt add to the mail String
     * @return Non empty/null String generated
     */
    private String getMailingAddress() {
		// TODO Auto-generated method stub
    	 StringBuffer claimantAdd=new StringBuffer();
         if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getPolicyownerBuildingNumber()), "null"))&&StringUtils.isNotBlank(getPolicyownerBuildingNumber())){
         	claimantAdd.append(getPolicyownerBuildingNumber()+" ");
         }
         if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getPolicyownerStreet1()), "null"))&&StringUtils.isNotBlank(getPolicyownerStreet1())){
         	claimantAdd.append(getPolicyownerStreet1());
         }
         if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getPolicyownerStreet2()), "null"))&&StringUtils.isNotBlank(getPolicyownerStreet2())){
         	claimantAdd.append("<br>"+getPolicyownerStreet2() +" ");
         }
         if(!(StringUtils.equalsIgnoreCase(getPolicyownerUnitNumber1(), "null"))&&StringUtils.isNotBlank(getPolicyownerUnitNumber1())){
         	claimantAdd.append("<br>"+getPolicyownerUnitNumber1() +" -");
         }
         if(!(StringUtils.equalsIgnoreCase(getPolicyownerUnitNumber2(), "null"))&&StringUtils.isNotBlank(getPolicyownerUnitNumber2())){
         	claimantAdd.append(getPolicyownerUnitNumber2() +"");
         }
         if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getPolicyownerBuildingName()), "null")&&StringUtils.isNotBlank(getPolicyownerBuildingName())){
         	claimantAdd.append("<br>"+getPolicyownerBuildingName()+"");
         }
         if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getPolicyownerCity()), "null")&&StringUtils.isNotBlank(getPolicyownerCity())){
         	claimantAdd.append("<br>"+getPolicyownerCity()+"");
         }
         if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getPolicyownerPostalCode()), "null"))&&StringUtils.isNotBlank(getPolicyownerPostalCode())){
         	claimantAdd.append(" "+getPolicyownerPostalCode()+"");
         }
         return claimantAdd.toString();
	}

	
    /**
     * Write Critical Illness/Terminal Illness/Unemployment Claimant information
     * @param pd
     * @param yStart
     * @return
     * @throws IOException
     */
    public float writeClaimantInformation(PDFGenerator pd, float yStart) throws IOException {
        PDPage page = pd.getCurrentPage();
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        List<String[]> rows = new ArrayList<>();

        pd.addTableHeader(table,  "Claimant information");


        String claimantAddress=getMailingAddress();
        rows.add(new String[]{"Name of Claimant", getPolicyownerFirstName() + ", " + getPolicyownerLastName()});


        rows.add(new String[]{
                "Mailing Address of Claimant",
                claimantAddress
        });


        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
        }
        yStart = table.draw() - 10;
        return yStart;
    }
}
