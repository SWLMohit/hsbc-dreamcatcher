package sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

@WebServlet(urlPatterns = "/eclaims/1-death-due-to-accident/2a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/1_death_due_to_accident/step2b.jsp")
})

@MultipartConfig
public class Step2bServlet extends BaseServlet {
	private final static Logger logger = Logger.getLogger(Step2bServlet.class);
    public float getCurrentStep() {
        return (float)2.5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizeDeathCertificate(request, sanitized);
        sanitized = sanitizePoliceInvestigation (request, sanitized);
        sanitized = sanitizeYNQuestions(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizeDeathCertificate(HttpServletRequest request, Map<String, Object> sanitized) {

	  sanitizeUploadedDocument(request, sanitized, "death_certificate", "DeathCertificate");

        return sanitized;
    }

    private Map<String, Object> sanitizePoliceInvestigation(HttpServletRequest request, Map<String, Object> sanitized) {

      sanitizeUploadedDocument(request, sanitized, "police_investigation", "PoliceInvestigationDocument");

        return sanitized;
    }

    private Map<String, Object> sanitizeYNQuestions(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("post_mortem");
        sanitized.put("post_mortem", Boolean.parseBoolean(input));
        if (Boolean.parseBoolean(input)) {
          sanitizeUploadedDocument(request, sanitized, "post_mortem_document", "PostMortemDocument");
        }
        input = request.getParameter("suicide");
        sanitized.put("suicide", Boolean.parseBoolean(input));
        input = request.getParameter("left_a_will");
        sanitized.put("left_a_will", Boolean.parseBoolean(input));
        if (Boolean.parseBoolean(input)) {
          sanitizeUploadedDocument(request, sanitized, "left_a_will_document", "LeftAWillDocument");
        }
        input = request.getParameter("occur_in_singapore");
        sanitized.put("occur_in_singapore", Boolean.parseBoolean(input));
        if (!Boolean.parseBoolean(input)) {
          sanitizeUploadedDocument(request, sanitized, "occur_in_singapore_document", "OccurInSingaporeDocument");
        }

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        DeathDueToAccidentApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");
       
        application.setDeathCertificate(docList(request, data, "death_certificate", "DeathCertificate"));
        application.setPoliceInvestigation(docList(request, data, "police_investigation", "PoliceInvestigationDocument"));
        
        
        application.setPostMortem((boolean)data.get("post_mortem"));
        if (application.getPostMortem()) {
          
         application.setPostMortemDocument(docList(request, data, "post_mortem_document", "PostMortemDocument"));
            
        } else {
            application.setPostMortemDocument(null);
        }
        application.setSuicide((boolean)data.get("suicide"));
        application.setLeftAWill((boolean)data.get("left_a_will"));
        if (application.getLeftAWill()) {

          application.setLeftAWillDocument(docList(request, data, "left_a_will_document", "LeftAWillDocument"));

        } else {
            application.setLeftAWillDocument(null);
        }
        application.setOccurInSingapore((boolean)data.get("occur_in_singapore"));
        if (!application.getOccurInSingapore()) {
         
          application.setOccurInSingaporeDocument(docList(request, data, "occur_in_singapore_document", "OccurInSingaporeDocument"));
        
        } else {
            application.setOccurInSingaporeDocument(null);
        }

        /*10 MB File size validation implementation starts here */
        if(application.getFileInfo()!=null){
          Map<String, Long>updatedFileInfo=application.getFileInfo();
          if(data.get("fileInfo")!=null) {
          updatedFileInfo.putAll((Map<String,Long>)data.get("fileInfo"));
          application.setFileInfo(updatedFileInfo);
          }
        }
        else {
          application.setFileInfo((Map<String,Long>)data.get("fileInfo")); 
        }
        /*10 MB File size validation implementation ends here */
        application.setMaxStep(getCurrentStep() + (float)0.5);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DeathDueToAccidentApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("death_certificate", application.getDeathCertificate());
        data.put("police_investigation", application.getPoliceInvestigation());
        data.put("post_mortem", application.getPostMortem());
        if (application.getPostMortem() != null && application.getPostMortem()) {
            data.put("post_mortem_document", application.getPostMortemDocument());
        }
        data.put("suicide", application.getSuicide());
        data.put("left_a_will", application.getLeftAWill());
        if (application.getLeftAWill() != null && application.getLeftAWill()) {
            data.put("left_a_will_document", application.getLeftAWillDocument());
        }
        data.put("occur_in_singapore", application.getOccurInSingapore());
        if (application.getOccurInSingapore() != null && !application.getOccurInSingapore()) {
            data.put("occur_in_singapore_document", application.getOccurInSingaporeDocument());
        }
        request.setAttribute("data", data);
    }

    protected float getStep() {
        return (float)0.5;
    }
}
