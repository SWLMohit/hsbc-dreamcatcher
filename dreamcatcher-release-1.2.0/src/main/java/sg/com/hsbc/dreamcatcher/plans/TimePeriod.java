package sg.com.hsbc.dreamcatcher.plans;

public enum TimePeriod {
    SEVENPM_NINEAM("19:00:01","09:00:00"),
    NINEAM_ELEVENAM("09:00:01","11:00:00"),
    ELEVENAM_ONETHIRTYPM("11:00:01","13:30:00"),
    ONETHIRTYPM_FOURPM("13:30:01","16:00:00"),
    FOURPM_SEVENPM("16:00:01","19:00:00"),
    SEVENPM_SEVENPM("19:00:01","19:00:00");

    private final String timeFrom;
    private final String timeTo;
    TimePeriod(String timeFrom, String timeTo){
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }

    public String getTimeFrom(){return this.timeFrom;}
    public String getTimeTo(){return this.timeTo;}

    public static String getTimePeriodDescription(TimePeriod timePeriod) {
        String description = "";
        switch (timePeriod) {
            case SEVENPM_NINEAM:
                description = "Previous day 7.01pm to 9am";
                break;
            case NINEAM_ELEVENAM:
                description = "9am to 11am";
                break;
            case ELEVENAM_ONETHIRTYPM:
                description = "11.01am to 1.30pm";
                break;
            case ONETHIRTYPM_FOURPM:
                description = "1.31pm to 4pm";
                break;
            case FOURPM_SEVENPM:
                description = "4.01pm to 7pm";
                break;
            case SEVENPM_SEVENPM:
                description = "Previous day 7:01pm to 7pm of current day";
                break;
        }

        return description;
    }


}
