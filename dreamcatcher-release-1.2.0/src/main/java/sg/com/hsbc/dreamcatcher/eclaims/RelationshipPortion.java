package sg.com.hsbc.dreamcatcher.eclaims;

import java.util.Date;

/**
 * Created by KhoaTran on 11/21/2017.
 */
public class RelationshipPortion {
    protected String relationship;
    protected String illnessNature;
    protected Date diagnosedDate;

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getIllnessNature() {
        return illnessNature;
    }

    public void setIllnessNature(String illnessNature) {
        this.illnessNature = illnessNature;
    }

    public Date getDiagnosedDate() {
        return diagnosedDate;
    }

    public void setDiagnosedDate(Date diagnosedDate) {
        this.diagnosedDate = diagnosedDate;
    }
}
