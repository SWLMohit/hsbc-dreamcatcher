package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoillness;

import sg.com.hsbc.dreamcatcher.eclaims.LifeIndexProcessor;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/5-tpd-due-to-illness", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/5_tpd_due_to_illness/index.jsp")
})
@MultipartConfig
public class IndexServlet extends BaseServlet {
    private LifeIndexProcessor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new LifeIndexProcessor(this);
    }

    public float getCurrentStep() {
        return (float)0;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        return processor.process(request);
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processor.handleSuccess(request, response);
    }

    public void populateFormData(HttpServletRequest request) {
        processor.populateFormData(request);

        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("country_list", this.getCountryList());

        request.setAttribute("formData", data);
    }
}
