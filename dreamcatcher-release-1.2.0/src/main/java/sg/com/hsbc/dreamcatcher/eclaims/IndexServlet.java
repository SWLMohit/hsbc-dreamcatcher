package sg.com.hsbc.dreamcatcher.eclaims;

import sg.com.hsbc.dreamcatcher.BaseServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = "/eclaims/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/index.jsp")
})
public class IndexServlet extends BaseServlet {


}
