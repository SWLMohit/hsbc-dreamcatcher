package sg.com.hsbc.dreamcatcher.admin.repository;

import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.helpers.Database;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.plans.onlineprotector.OnlineProtectorApplication;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ApplicationsRepository {

    String sql = null;
    Database db = new Database();
    Connection conn = null;
    PreparedStatement stmt = null;
    ResultSet rs = null;

    /**
     *  Get the application base on Mobile number and email
     * @param type DVT or OP
     * @param status      0 for incomplete, 1 for complete
     * @param countryCode mobile no country code
     * @param mobileNo
     * @param email
     * @return
     */
    public List<Applications> findByMobileNoAndEmail (String type, int status, String countryCode, String mobileNo, String email) {

        if (type == null || mobileNo == null || email == null) {
            return null;
        }
        Connection conn = db.getConnection();
        List<Applications> applicationsList = new ArrayList<>();
        Applications applications;
        try {
            sql = "select * from applications where status = ? and product_type = ? and applicant_mobile_country_code = ? and applicant_mobile_num = ? and applicant_email = ? order by id desc";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, status);
            stmt.setString(2, type);
            stmt.setString(3, countryCode==null?"+65":countryCode);
            stmt.setString(4, mobileNo);
            stmt.setString(5, email);
            rs = stmt.executeQuery();
            while (rs.next()) {
                applications = new Applications();
                applications.setId(rs.getString("id"));
                applications.setReferenceNum(rs.getString("reference_num"));

                applications.setApplicantEmail(rs.getString("applicant_email"));
                applications.setApplicantMobileCountryCode(rs.getString("applicant_mobile_country_code"));
                applications.setApplicantMobileNum(rs.getString("applicant_mobile_num"));
                applications.setApplicantFirstName(rs.getString("applicant_first_name"));
                applications.setApplicantLastName(rs.getString("applicant_last_name"));
                applications.setInsuredFirstName(rs.getString("insured_first_name"));
                applications.setInsuredLastName(rs.getString("insured_last_name"));
                applicationsList.add(applications);
            }
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
            return null;
        }finally {
            db.closeAll(conn,stmt,rs);
        }
        return applicationsList;
    }

    /**
     *  Get the application base on Mobile number
     * @param type DVT or OP
     * @param countryCode mobile no country code
     * @param mobileNo
     * @return
     */
    public List<Applications> findByMobileNo (String type, String countryCode, String mobileNo, int status) {
        if (type == null || mobileNo == null) {
            return null;
        }
        Connection conn = db.getConnection();
        List<Applications> applicationsList = new ArrayList<>();
        Applications applications;
        try {
            sql = "select * from applications where product_type = ? and applicant_mobile_country_code = ? and applicant_mobile_num = ? and status = ? order by id desc";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, type);
            stmt.setString(2, countryCode==null?"+65":countryCode);
            stmt.setString(3, mobileNo);
            stmt.setInt(4, status);
            rs = stmt.executeQuery();
            while (rs.next()) {
                applications = new Applications();
                applications.setId(rs.getString("id"));
                applications.setReferenceNum(rs.getString("reference_num"));
                applications.setApplicantEmail(rs.getString("applicant_email"));
                applications.setApplicantMobileCountryCode(rs.getString("applicant_mobile_country_code"));
                applications.setApplicantMobileNum(rs.getString("applicant_mobile_num"));
                applicationsList.add(applications);
            }
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
            return null;
        }finally {
            db.closeAll(conn,stmt,rs);
        }
        return applicationsList;
    }

    /**
     *  Get the application base on email
     * @param type DVT or OP
     * @param email
     * @return
     */
    public List<Applications> findByEmail (String type, String email,int status ) {
        if (type == null || email == null) {
            return null;
        }
        Connection conn = db.getConnection();
        List<Applications> applicationsList = new ArrayList<>();
        Applications applications;
        try {
            sql = "select * from applications where product_type = ? and applicant_email = ? and status = ? order by id desc";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, type);
            stmt.setString(2, email);
            stmt.setInt(3, status);
            rs = stmt.executeQuery();
            while (rs.next()) {
                applications = new Applications();
                applications.setId(rs.getString("id"));
                applications.setReferenceNum(rs.getString("reference_num"));
                applications.setApplicantEmail(rs.getString("applicant_email"));
                applications.setApplicantMobileCountryCode(rs.getString("applicant_mobile_country_code"));
                applications.setApplicantMobileNum(rs.getString("applicant_mobile_num"));
                applicationsList.add(applications);
            }
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
            return null;
        }finally {
            db.closeAll(conn,stmt,rs);
        }
        return applicationsList;
    }

    /**
     *  Get the application base on email
     * @param applications Application data
     * @return String
     */
    public String findByVerificationCode (Applications applications) {
        if (applications == null || applications.getApplicantEmail() == null || applications.getDropoutPassword() == null) {
            return null;
        }
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            sql = "SELECT id FROM applications WHERE product_type = ? AND applicant_email = ? AND BINARY dropout_password = ? AND status = ? ORDER BY date_created DESC limit 1";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, applications.getProductType());
            stmt.setString(2, applications.getApplicantEmail());
            stmt.setString(3, applications.getDropoutPassword());
            stmt.setInt(4, OnlineProtectorApplication.STATUS_INCOMPLETE);
            rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getString("id");
            }
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }
        return null;
    }

    /**
     *  Get the application base on email
     * @param productType DVT or OP
     * @param email
     * @return status
     */
    public boolean findByVerificationCode (String productType, String email) {
        if (email == null) {
            return false;
        }
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            sql = "SELECT id FROM applications WHERE product_type = ? AND applicant_email = ? AND status = ? ORDER BY date_created DESC limit 1";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, productType);
            stmt.setString(2, email);
            stmt.setInt(3, OnlineProtectorApplication.STATUS_INCOMPLETE);
            rs = stmt.executeQuery();
            if (rs.next()) {
               return true;
            }
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }
        return false;
    }

    /**
     * After re-generate the VerficiationCode then need to update on applications table
     * @param applications
     * @param code
     */
    public void updateVerficiationCode(Applications applications, String code) {
        if (applications == null || code == null) {
            throw new IllegalArgumentException("Argument cannot null");
        }
        Connection conn = db.getConnection();
        try {
            sql = "Update applications SET dropout_password = ?,status=0 where id=?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, code);
            stmt.setString(2, applications.getId());
            stmt.setMaxRows(1);
            stmt.executeUpdate();
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
        }finally {
            db.closeAll(conn,stmt);
        }
    }

    /**
     *
     * @param applicationId
     * @return Applications
     */

    public Applications findApplicationById (String applicationId) {
        if (applicationId == null) {
            return null;
        }

        Applications applications = new Applications();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            sql = "SELECT * FROM applications WHERE id = ? ORDER BY date_created DESC limit 1";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, applicationId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                applications.setId(rs.getString("id"));
                applications.setReferenceNum(rs.getString("reference_num"));
                applications.setApplicantMobileCountryCode(rs.getString(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE));
                applications.setApplicantMobileNum(rs.getString(FieldConstants.APPLICANT_MOBILE_NUM));
                applications.setApplicantEmail(rs.getString(FieldConstants.APPLICANT_EMAIL));
            }
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }
        return applications;
    }

}
