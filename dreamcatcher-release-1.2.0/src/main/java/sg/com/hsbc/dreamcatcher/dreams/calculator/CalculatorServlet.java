package sg.com.hsbc.dreamcatcher.dreams.calculator;

import sg.com.hsbc.dreamcatcher.BaseServlet;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 
 * This class is used to handle the servlet call for the 
 * savings, retirement, education and protection calculator.
 *
 */

public abstract class CalculatorServlet extends BaseServlet {
  private final static Logger logger = Logger.getLogger(CalculatorServlet.class);
  /**
   * This method will match the words present in the String searchText 
   * with keys and replace them with the values of the of the HashMap textAlterations  
   * 
   * @param searchText  String of text searched on the front end
   * @return            Updated String searchText
   */
  
  private String textToDisplay(String searchText) {
    Map<String, String>textAlterations= new HashMap<>();
   
    textAlterations.put("I", "you");
    textAlterations.put("my", "your");
    
    for (Map.Entry<String, String> entry : textAlterations.entrySet())
    {
        searchText=searchText.replaceAll("\\b"+entry.getKey()+"\\b", entry.getValue());
    }
    return searchText;
  }
  
  /**
   * This method converts utf-8 to java internal string format
   * 
   * @param utf8String  String containing utf-8 encoding
   * @return            converted String or null
   */
  public static String convertFromUTF8(String utf8String) {
    String output = null;
    try {
      output = new String(utf8String.getBytes("ISO-8859-1"), "UTF-8");
    } catch (java.io.UnsupportedEncodingException e) {
      logger.error("Exception occurred while converting from utf-8"+e);
        return null;
    }
    return output;
}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String searchText = req.getParameter("searchText");
		
		String convertedString=convertFromUTF8(searchText);
		if(StringUtils.isNotBlank(convertedString)) {
		  searchText=textToDisplay(convertedString);
		}
		req.setAttribute("searchText", searchText);
		render(req, resp);
	}
}
