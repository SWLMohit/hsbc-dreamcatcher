package sg.com.hsbc.dreamcatcher.eclaims;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;


public class EclaimIllnessStep1bProcessor {
    private EClaimBaseServlet servletInstance;
    private final static Logger logger = Logger.getLogger(EclaimIllnessStep1bProcessor.class);
    public EclaimIllnessStep1bProcessor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeDoctor(request, sanitized);
        sanitized = sanitizeRelationship(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizeDoctor(HttpServletRequest request, Map<String, Object> sanitized) {
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        String[] fromDate = request.getParameterValues("doctor_from_date");
        String[] toDate = request.getParameterValues("doctor_to_date");
        
        String[] doctorNames = request.getParameterValues("doctor_name");
        String[] doctorHospitals = request.getParameterValues("doctor_hospital");
        String[] doctorHospitalStates = request.getParameterValues("doctor_hospital_state");
        String[] doctorHospitalStreet1s = request.getParameterValues("doctor_hospital_street1");
        String[] doctorHospitalStreet2s = request.getParameterValues("doctor_hospital_street2");

        for (int i = 0; i < doctorNames.length; i++) {
            Map<String, Object> subData = new HashMap<>();
            subData.put("doctor_from_date", fromDate[i]);
            subData.put("doctor_to_date", toDate[i]);
            subData.put("doctor_name", doctorNames[i]);
            subData.put("doctor_hospital", doctorHospitals[i]);
            subData.put("doctor_hospital_state", doctorHospitalStates[i]);
            subData.put("doctor_hospital_street1", doctorHospitalStreet1s[i]);
            subData.put("doctor_hospital_street2", doctorHospitalStreet2s[i]);

            list.add(subData);
        }

        sanitized.put("doctor_list", list.toArray());

        return sanitized;
    }

    private Map<String, Object> sanitizeRelationship(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("relationship_illness");
        sanitized.put("relationship_illness", Boolean.parseBoolean(input));
        servletInstance.handleUploadFile(request, sanitized, "medical_report", "MedicalReport");
        servletInstance.handleUploadFile(request, sanitized, "clinical_abstract_form", "ClinicalAbstractForm");
     
        if (Boolean.parseBoolean(input)) {
            ArrayList<Map<String, Object>> list = new ArrayList<>();
            String[] relationshipRelationships = request.getParameterValues("relationship_relationship");
            String[] relationshipIllnessNatures = request.getParameterValues("relationship_illness_nature");
            String[] relationshipDiagnosedDateDays = request.getParameterValues("relationship_diagnosed_date_day");

            for (int i = 0; i < relationshipRelationships.length; i++) {
                Map<String, Object> subData = new HashMap<>();
                subData.put("relationship_relationship", relationshipRelationships[i]);
                subData.put("relationship_illness_nature", relationshipIllnessNatures[i]);
                subData.put("relationship_diagnosed_date_day", relationshipDiagnosedDateDays[i]);

                list.add(subData);
            }

            sanitized.put("relationship_list", list.toArray());

            
        }

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        IllnessApplication application = (IllnessApplication)servletInstance.getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        ArrayList<DoctorPortion> list = new ArrayList<>();
        DoctorPortion[] oldList = application.getDoctorList();
        int i = 0;
        for (Object subDataItem : (Object[]) data.get("doctor_list")) {
            DoctorPortion portion;
            if (i < oldList.length) {
                portion = oldList[i];
            } else {
                portion = new DoctorPortion();
            }
            Map<String, Object> subData = (Map<String, Object>) subDataItem;

            try {
                Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s/%s", "01", subData.get("doctor_from_date")));
                portion.setFromDate(date);
            } catch (ParseException e) {
                ErrorHandler.handleError(application, e);
                return false;
            }
            try {
                Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s/%s", "01", subData.get("doctor_to_date")));
                portion.setToDate(date);
            } catch (ParseException e) {
                ErrorHandler.handleError(application, e);
                return false;
            }

            portion.setName(subData.get("doctor_name").toString());
            portion.setHospital(subData.get("doctor_hospital").toString());
            portion.setHospitalCity(subData.get("doctor_hospital_state").toString());
            portion.setHospitalStreet1(subData.get("doctor_hospital_street1").toString());
            portion.setHospitalStreet2(subData.get("doctor_hospital_street2").toString());

            list.add(portion);
            i++;
        }
        application.setDoctorList(list.toArray(new DoctorPortion[list.size()]));

        application.setRelationshipIllness((boolean)data.get("relationship_illness"));
        
        if (!data.get("medical_report").toString().equals("")) {
            application.setMedicalReport(data.get("medical_report").toString());
        }
        if (!data.get("clinical_abstract_form").toString().equals("")) {
            application.setClinicalAbtractForm(data.get("clinical_abstract_form").toString());
        }
        
        /** 10MB File size implementation starts here **/ 
        if(application.getFileInfo()!=null){
          Map<String, Long>updatedFileInfo=application.getFileInfo();
          if(data.get("fileInfo")!=null) {
          updatedFileInfo.putAll((Map<String,Long>)data.get("fileInfo"));
          application.setFileInfo(updatedFileInfo);
          }
        }
        else {
          application.setFileInfo((Map<String,Long>)data.get("fileInfo")); 
        }
        
        /** 10MB File size implementation ends here **/ 
        if (application.getRelationshipIllness()) {
            ArrayList<RelationshipPortion> relList = new ArrayList<>();
            RelationshipPortion[] relOldList = application.getRelationshipList();
            i = 0;
            for (Object subDataItem : (Object[]) data.get("relationship_list")) {
                RelationshipPortion portion;
                if (i < relOldList.length) {
                    portion = relOldList[i];
                } else {
                    portion = new RelationshipPortion();
                }
                Map<String, Object> subData = (Map<String, Object>) subDataItem;
                portion.setRelationship(subData.get("relationship_relationship").toString());
                portion.setIllnessNature(subData.get("relationship_illness_nature").toString());

                try {
                    Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", subData.get("relationship_diagnosed_date_day")));
                    portion.setDiagnosedDate(date);
                } catch (ParseException e) {
                    ErrorHandler.handleError(application, e);
                    return false;
                }

                relList.add(portion);
                i++;
            }
            application.setRelationshipList(relList.toArray(new RelationshipPortion[relList.size()]));

            
        } else {
            application.setRelationshipList(null);
        }

        application.setMaxStep(servletInstance.getCurrentStep() + servletInstance.getStep());
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        IllnessApplication application = (IllnessApplication)servletInstance.getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (DoctorPortion portion : application.getDoctorList()) {
            Map<String, Object> subData = new HashMap<>();

            Date date = portion.getFromDate();
            if (date != null) {
                subData.put("doctor_from_date", (new SimpleDateFormat("MM/y")).format(date));
            }
            date = portion.getToDate();
            if (date != null) {
                subData.put("doctor_to_date", (new SimpleDateFormat("MM/y")).format(date));
            }

            subData.put("doctor_name", portion.getName());
            subData.put("doctor_hospital", portion.getHospital());
            subData.put("doctor_hospital_state", portion.getHospitalCity());
            subData.put("doctor_hospital_street1", portion.getHospitalStreet1());
            subData.put("doctor_hospital_street2", portion.getHospitalStreet2());

            list.add(subData);
        }
        data.put("doctor_list", list.toArray());

        data.put("relationship_illness", application.getRelationshipIllness());

        ArrayList<Map<String, Object>> relList = new ArrayList<>();
        if (application.getRelationshipList() == null || application.getRelationshipList().length == 0) {
            application.setRelationshipList(new RelationshipPortion[1]);
            application.getRelationshipList()[0] = new RelationshipPortion();
        }
        for (RelationshipPortion portion : application.getRelationshipList()) {
            Map<String, Object> subData = new HashMap<>();

            subData.put("relationship_relationship", portion.getRelationship());
            subData.put("relationship_illness_nature", portion.getIllnessNature());

            Date date = portion.getDiagnosedDate();
            if (date != null) {
                subData.put("relationship_diagnosed_date_day", (new SimpleDateFormat("dd/MM/y")).format(date));
            }

            relList.add(subData);
        }
        data.put("relationship_list", relList.toArray());

        
            data.put("medical_report", application.getMedicalReport());
            data.put("clinical_abstract_form", application.getClinicalAbtractForm());
       

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("country_list", servletInstance.getCountryList());
        data.put("day_list", servletInstance.getDayList());
        data.put("month_list", servletInstance.getMonthList());
        data.put("year_list", servletInstance.getYearList());

        request.setAttribute("formData", data);
    }
}
