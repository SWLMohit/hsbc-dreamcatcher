package sg.com.hsbc.dreamcatcher.jobs;

import sg.com.hsbc.dreamcatcher.helpers.Config;
import sg.com.hsbc.dreamcatcher.helpers.Database;
import sg.com.hsbc.dreamcatcher.helpers.EmailSender;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PurgeExpiredApplicationsJob implements Runnable {

    @Override
    public void run() {
        boolean hasError = false;
        /*
         * Null and purge incomplete applications (14 days retention)
         */
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
       try {
           try {
                stmt = conn.prepareStatement("UPDATE applications SET reference_num = NULL, product_type = NULL, max_step = 0, agree_privacy = NULL, agree_important_notes = NULL, life_insured = NULL, dropout_password = NULL, notify_status = NULL, mail_option = NULL, campaign_code = NULL, applicant_residency = NULL, applicant_first_name = NULL, applicant_last_name = NULL, applicant_dob = NULL, applicant_gender = NULL, applicant_mobile_country_code = NULL, applicant_mobile_num = NULL, applicant_email = NULL, applicant_nationality = NULL, applicant_nationality2 = NULL, applicant_nationality3 = NULL, applicant_multiple_nationalities = NULL, applicant_birth_country = NULL, applicant_nric = NULL, applicant_en_proficient = NULL, applicant_employment = NULL, applicant_income = NULL, applicant_smoker = NULL, applicant_occupation = NULL, applicant_industry = NULL, applicant_company_name = NULL, applicant_company_city = NULL, applicant_company_country = NULL, applicant_studies_end_date = NULL, applicant_address_postal_code = NULL, applicant_address_house = NULL, applicant_address_unit = NULL, applicant_address_street = NULL, applicant_address_building = NULL, applicant_address_registered = NULL, applicant_address_period_stay = NULL, applicant_previous_address_country = NULL, applicant_address_permanent = NULL, applicant_permanent_address_line1 = NULL, applicant_permanent_address_line2 = NULL, applicant_permanent_address_postal_code = NULL, applicant_permanent_address_country = NULL, applicant_tax_country1 = NULL, applicant_tax_country1_tin = NULL, applicant_tax_country1_tin_reason = NULL, applicant_tax_country1_tin_reason_custom = NULL, applicant_tax_country2 = NULL, applicant_tax_country2_tin = NULL, applicant_tax_country2_tin_reason = NULL, applicant_tax_country2_tin_reason_custom = NULL, applicant_tax_country3 = NULL, applicant_tax_country3_tin = NULL, applicant_tax_country3_tin_reason = NULL, applicant_tax_country3_tin_reason_custom = NULL, applicant_address_no_tax_reason = NULL, applicant_hsbc_customer = NULL, applicant_pep = NULL, applicant_bo = NULL, insured_residency = NULL, insured_first_name = NULL, insured_last_name = NULL, insured_dob = NULL, insured_gender = NULL, insured_nationality = NULL, insured_nationality2 = NULL, insured_nationality3 = NULL, insured_multiple_nationalities = NULL, insured_nric = NULL, insured_employment = NULL, insured_income = NULL, insured_smoker = NULL, insured_medical1 = NULL, insured_medical2 = NULL, insured_medical3 = NULL, insured_medical4 = NULL, insured_occupation = NULL, insured_industry = NULL, insured_company_name = NULL, insured_studies_end_date = NULL, bo_first_name = NULL, bo_last_name = NULL, bo_nric = NULL, bo_relation = NULL, payment_mode = NULL, sum_assured = NULL, rider_sum_assured = NULL, include_ci_rider = NULL, include_pr_rider = NULL, uw_lifestyle_questions_optin = NULL, uw_lifestyle_mortgage = NULL, uw_lifestyle_profile = NULL, uw_lifestyle_height = NULL, uw_lifestyle_weight = NULL, uw_lifestyle_exercise = NULL, cc_type = NULL, cc_name = NULL, cc_num = NULL, cc_expiry = NULL, product_code = NULL, term_duration = NULL, term_years = NULL WHERE status = 0 AND DATE(date_updated) < CURDATE() - INTERVAL 14 DAY");
                stmt.executeUpdate();
            }finally {
                db.closeAll(conn,stmt);
            }

            try {
                conn = db.getConnection();
                stmt = conn.prepareStatement("DELETE FROM applications WHERE status = 0 AND DATE(date_updated) < CURDATE() - INTERVAL 14 DAY");
                stmt.executeUpdate();
            }finally {
                db.closeAll(conn,stmt);
            }

        /*
         * Null and purge complete applications (30 days retention)
         */
            try {
                conn = db.getConnection();
                stmt = conn.prepareStatement("UPDATE applications SET reference_num = NULL, product_type = NULL, max_step = 0, agree_privacy = NULL, agree_important_notes = NULL, life_insured = NULL, dropout_password = NULL, notify_status = NULL, mail_option = NULL, campaign_code = NULL, applicant_residency = NULL, applicant_first_name = NULL, applicant_last_name = NULL, applicant_dob = NULL, applicant_gender = NULL, applicant_mobile_country_code = NULL, applicant_mobile_num = NULL, applicant_email = NULL, applicant_nationality = NULL, applicant_nationality2 = NULL, applicant_nationality3 = NULL, applicant_multiple_nationalities = NULL, applicant_birth_country = NULL, applicant_nric = NULL, applicant_en_proficient = NULL, applicant_employment = NULL, applicant_income = NULL, applicant_smoker = NULL, applicant_occupation = NULL, applicant_industry = NULL, applicant_company_name = NULL, applicant_company_city = NULL, applicant_company_country = NULL, applicant_studies_end_date = NULL, applicant_address_postal_code = NULL, applicant_address_house = NULL, applicant_address_unit = NULL, applicant_address_street = NULL, applicant_address_building = NULL, applicant_address_registered = NULL, applicant_address_period_stay = NULL, applicant_previous_address_country = NULL, applicant_address_permanent = NULL, applicant_permanent_address_line1 = NULL, applicant_permanent_address_line2 = NULL, applicant_permanent_address_postal_code = NULL, applicant_permanent_address_country = NULL, applicant_tax_country1 = NULL, applicant_tax_country1_tin = NULL, applicant_tax_country1_tin_reason = NULL, applicant_tax_country1_tin_reason_custom = NULL, applicant_tax_country2 = NULL, applicant_tax_country2_tin = NULL, applicant_tax_country2_tin_reason = NULL, applicant_tax_country2_tin_reason_custom = NULL, applicant_tax_country3 = NULL, applicant_tax_country3_tin = NULL, applicant_tax_country3_tin_reason = NULL, applicant_tax_country3_tin_reason_custom = NULL, applicant_address_no_tax_reason = NULL, applicant_hsbc_customer = NULL, applicant_pep = NULL, applicant_bo = NULL, insured_residency = NULL, insured_first_name = NULL, insured_last_name = NULL, insured_dob = NULL, insured_gender = NULL, insured_nationality = NULL, insured_nationality2 = NULL, insured_nationality3 = NULL, insured_multiple_nationalities = NULL, insured_nric = NULL, insured_employment = NULL, insured_income = NULL, insured_smoker = NULL, insured_medical1 = NULL, insured_medical2 = NULL, insured_medical3 = NULL, insured_medical4 = NULL, insured_occupation = NULL, insured_industry = NULL, insured_company_name = NULL, insured_studies_end_date = NULL, bo_first_name = NULL, bo_last_name = NULL, bo_nric = NULL, bo_relation = NULL, payment_mode = NULL, sum_assured = NULL, rider_sum_assured = NULL, include_ci_rider = NULL, include_pr_rider = NULL, uw_lifestyle_questions_optin = NULL, uw_lifestyle_mortgage = NULL, uw_lifestyle_profile = NULL, uw_lifestyle_height = NULL, uw_lifestyle_weight = NULL, uw_lifestyle_exercise = NULL, cc_type = NULL, cc_name = NULL, cc_num = NULL, cc_expiry = NULL, product_code = NULL, term_duration = NULL, term_years = NULL WHERE status = 1 AND DATE(date_updated) < CURDATE() - INTERVAL 30 DAY");
                stmt.executeUpdate();
            }finally {
                db.closeAll(conn,stmt);
            }

            try{
                conn = db.getConnection();
                stmt = conn.prepareStatement("DELETE FROM applications WHERE status = 1 AND DATE(date_updated) < CURDATE() - INTERVAL 30 DAY");
                stmt.executeUpdate();
            }   finally {
                db.closeAll(conn,stmt);
            }

        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
            hasError = true;
        }
        if (hasError) {
            try {
                EmailSender.sendEmail(Config.getString("dreamcatcher.email.support"), "Failure to purge expired data", "Failure to purge expired data", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
