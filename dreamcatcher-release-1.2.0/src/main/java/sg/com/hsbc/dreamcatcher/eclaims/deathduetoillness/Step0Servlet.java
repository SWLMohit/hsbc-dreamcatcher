package sg.com.hsbc.dreamcatcher.eclaims.deathduetoillness;

import org.apache.commons.io.FileUtils;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/2-death-due-to-illness/0", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/2_death_due_to_illness/step0.jsp")
})
public class Step0Servlet extends BaseServlet {
    public float getCurrentStep() {
        return (float)0;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");
        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");
        request.setAttribute("data", data);
    }

    protected float getStep() {
        return (float)1;
    }
}
