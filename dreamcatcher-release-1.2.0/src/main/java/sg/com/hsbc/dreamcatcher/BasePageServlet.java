package sg.com.hsbc.dreamcatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BasePageServlet extends BaseServlet {
    public abstract String getFormTitle();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("formTitle", this.getFormTitle());
        render(request, response);
    }
}
