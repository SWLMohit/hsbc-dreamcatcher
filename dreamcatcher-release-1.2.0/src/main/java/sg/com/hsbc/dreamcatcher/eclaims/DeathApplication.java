package sg.com.hsbc.dreamcatcher.eclaims;

public abstract class DeathApplication extends EClaimApplication {
    public boolean save() {
        if (this.getMaxStep() == (float) 5.5) {
            this.setStatus(STATUS_COMPLETE);
        }
        return super.save();
    }
}
