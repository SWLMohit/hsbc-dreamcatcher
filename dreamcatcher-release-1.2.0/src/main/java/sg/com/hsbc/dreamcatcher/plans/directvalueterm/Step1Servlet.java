package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.helpers.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/1", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step1.jsp")
})
public class Step1Servlet extends BaseServlet {

	private final static Logger logger = Logger.getLogger(Step1Servlet.class);

    public float getCurrentStep() {
        return 1;
    }

    public void init(HttpServletRequest request) {
        super.init(request);
        
        //JIRA HSBCICDU-592 if dvt already completed, remove from session and
        //create a new session object
        DirectValueTermApplication application = getApplication(request);
        if (application.getStatus() == ApplicationForm.STATUS_COMPLETE) {
            request.getSession().setAttribute(this.getApplicationSessionAttributeName(), UUID.randomUUID());
            application = application.getById(request.getSession().getAttribute(this.getApplicationSessionAttributeName()).toString());
            request.setAttribute("maxStep", application.getMaxStep());
            float currentStep = getCurrentStep();
            request.setAttribute("currentStep", currentStep);
            request.setAttribute("application", application);
            logger.info("the application id for the current session is:"+request.getSession().getAttribute(this.getApplicationSessionAttributeName()).toString());
        }

        String sql = "Update applications SET dropout_password = NULL,status=NULL  where id=?" ;
        String id=null;
        if(request.getSession().getAttribute(this.getApplicationSessionAttributeName())!=null){
           id=request.getSession().getAttribute(this.getApplicationSessionAttributeName()).toString();
           if(StringUtils.isNotBlank(id)){
               Database db = new Database();
               Connection conn = db.getConnection();
               PreparedStatement stmt = null;
        	  try{
        		  System.out.println("the id to be reset is "+id);
        	   stmt = conn.prepareStatement(sql);
        	   stmt.setString(1, id);
        	   stmt.setMaxRows(1);
        	   stmt.executeUpdate();
        	  }catch(Exception e){
        		  System.out.println("error is thrown in updating the dropout password");
        	  } finally {
                  db.closeAll(conn,stmt);
              }
        	   
           }
        }
        boolean isDirty = false;


        if (request.getParameter("applicant_gender") != null) {
            application.setApplicantGender(request.getParameter("applicant_gender"));
            isDirty = true;
        }

        if (request.getParameter("applicant_smoker") != null) {
            application.setApplicantSmoker(request.getParameter("applicant_smoker").equalsIgnoreCase("Y"));
            isDirty = true;
        }

        if (request.getParameter("applicant_smoker") != null) {
            application.setApplicantSmoker(request.getParameter("applicant_smoker").equalsIgnoreCase("Y"));
            isDirty = true;
        }

        if (request.getParameter("sum_assured") != null) {
            application.setSumAssured(Float.parseFloat(request.getParameter("sum_assured").replace(",", "")));
            isDirty = true;
        }

        if (request.getParameter("include_ci_rider") != null) {
            application.setIncludeCriticalIllnessRider(true);
            isDirty = true;
        }else{
        	application.setIncludeCriticalIllnessRider(false);
        }

        if (request.getParameter("rider_sum_assured") != null) {
            application.setRiderSumAssured(Float.parseFloat(request.getParameter("rider_sum_assured").replace(",", "")));
            isDirty = true;
        }

        if (request.getParameter("campaign_code") != null) {
            application.setCampaignCode(request.getParameter("campaign_code"));
            isDirty = true;
        }

        if (request.getParameter("applicant_dob") != null) {
            try {
                application.setApplicantDob((new SimpleDateFormat("d/M/y")).parse(String.format("%s", request.getParameter("applicant_dob"))));
                isDirty = true;
            } catch (Exception e) {
                ErrorHandler.handleError(application, e);
            }
        }

        if (request.getParameter("term_duration") != null) {
            application.setTermDuration(request.getParameter("term_duration"));
            isDirty = true;
        }

        if (isDirty) {
            application.save();
        }
    }

    public boolean validate(HttpServletRequest request) {
        return true;
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        application.setAgreePrivacy(true);
        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

}
