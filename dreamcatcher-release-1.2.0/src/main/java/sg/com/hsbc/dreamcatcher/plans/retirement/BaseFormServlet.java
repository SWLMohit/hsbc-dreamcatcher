package sg.com.hsbc.dreamcatcher.plans.retirement;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseFormServlet extends BaseServlet {
    /**
     * Add context for the template.
     *
     * @param request The http request
     */
    public void requestContext(HttpServletRequest request) {
        if (getSuccessUrl() != null) {
            request.setAttribute("success_url", getSuccessUrl());
        }
        if (getPreviousUrl() != null) {
            request.setAttribute("previous_url", getPreviousUrl());
        }
        super.requestContext(request);
    }
}
