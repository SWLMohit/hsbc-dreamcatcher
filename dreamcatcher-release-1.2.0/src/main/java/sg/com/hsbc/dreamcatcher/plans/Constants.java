package sg.com.hsbc.dreamcatcher.plans;

public class Constants  {
    public static final String DVT_5_YEARS_PRODUCT_CODE = "VT73";
    public static final String DVT_20_YEARS_PRODUCT_CODE = "VT83";
    public static final String DVT_AGE_65_PRODUCT_CODE = "VT93";
    public static final String DECLARATION_HEADER_OP_DVT="Declarations and Acknowledgements";
}
