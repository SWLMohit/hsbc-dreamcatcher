package sg.com.hsbc.dreamcatcher.helpers;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Properties;

public class EmailSenderThread implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(EmailSenderThread.class);
    private String toAddresses;
    private String subject;
    private String body;
    private String attachmentPath;

    public EmailSenderThread(String toAddresses, String subject, String body, String attachmentPath) {
        this.toAddresses = toAddresses;
        this.subject = subject;
        this.body = body;
        this.attachmentPath = attachmentPath;
    }

    @Override
    public void run() {
        try {
            // Send the notifications to the user, and log that we have
            String host = Config.getString("dreamcatcher.smtp.host");
            String port = Config.getString("dreamcatcher.smtp.port");
            String userName = Config.getString("dreamcatcher.smtp.username");
            String password = Config.getString("dreamcatcher.smtp.password");
            String fromAddress = Config.getString("dreamcatcher.email.from");

            // sets SMTP server properties
            Properties properties = new Properties();
            properties.put("mail.smtp.host", host);
            properties.put("mail.smtp.port", port);
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");

            // creates a new session with an authenticator
            Authenticator auth = new Authenticator() {
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userName, password);
                }
            };

            Session session = Session.getInstance(properties, auth);
            Message message = new MimeMessage(session);
            Address[] to = InternetAddress.parse(toAddresses);
            message.setFrom(new InternetAddress(fromAddress));
            message.setRecipients(Message.RecipientType.TO, to);
            message.setSubject(subject);
            message.setSentDate(new Date());

            if (attachmentPath != null) {
                MimeMultipart multipart = new MimeMultipart("mixed");

                if(StringUtils.isNotBlank(attachmentPath)){
                    BodyPart attachmentBodyPart = new MimeBodyPart();
                    DataSource file = new FileDataSource(attachmentPath);
                    attachmentBodyPart.setDataHandler(new DataHandler(file));
                    attachmentBodyPart.setFileName(file.getName());
                    multipart.addBodyPart(attachmentBodyPart);
                }

                BodyPart htmlBodyPart = new MimeBodyPart();
                htmlBodyPart.setContent(body , "text/html");
                multipart.addBodyPart(htmlBodyPart);

                message.setContent(multipart);
            } else {
                // NB text/plain
                message.setText(body);
            }
            Transport.send(message);
            logger.info("Sent email to user for {} ", subject);
        } catch (MessagingException me) {
            ErrorHandler.handleError(subject, me, "Failed to send notifications");
        }
    }
}
