package sg.com.hsbc.dreamcatcher.admin.service;

public interface ValidationService {
    public boolean validateMobileNo (String type, String countryCode, String mobileNo, int status) ;
    public boolean validateEmail (String type, String email, int status);
    public boolean validateMobileNoAndEmail(String type, String countryCode, String mobileNo, String email);
}
