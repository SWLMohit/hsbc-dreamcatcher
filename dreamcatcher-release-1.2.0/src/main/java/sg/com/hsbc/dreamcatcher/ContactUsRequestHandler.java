package sg.com.hsbc.dreamcatcher;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;

import sg.com.hsbc.dreamcatcher.helpers.Config;
import sg.com.hsbc.dreamcatcher.helpers.EmailSender;

public class ContactUsRequestHandler {

	private HttpServletRequest request;
	private HttpServletResponse response;

	private final static Logger logger = Logger.getLogger(ContactUsRequestHandler.class);
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String PHONE_COUNTRY_CODE_PATTERN = "^[\\+0-9]{2,3}$";
	public static final String PHONE_NUMBER_PATTERN = "^[0-9]+$";
	private Map<String, String> sanitized;
	private Map<String, String> errors;
	public static final String EMAIL_CONTENT = "Dear Customer,<br>" + "<br>"
			+ "Thank you for your feedback. We will reply to you within 3 working days.<br>" + "<br>"
			+ "For urgent matters, please call our HSBC Insurance Customer Service Hotline at (65) 6225 6111 (Monday to Fridays from 9am-5pm).";
	public static final String EMAIL_SUBJECT = "Thank you for visiting HSBC Insurance";
	public static final String BO_EMAIL_SUBJECT = "Subject";
	public static final String BO_EMAIL_COMMENTS = "Comments / Questions";
	public static final String BO_EMAIL_TITLE = "Title";
	public static final String BO_EMAIL_FIRST_NAME = "First Name";
	public static final String BO_EMAIL_SURNAME = "Surname";
	public static final String BO_EMAIL_ID = "E-mail";
	public static final String BO_EMAIL_NRIC = "NRIC/Passport no";
	public static final String BO_EMAIL_COUNTRY = "Country";
	public static final String BO_EMAIL_CONTACT_NO = "Contact no";
	public static final String ADDITIONAL_FIELD_CONDITION = "Complaint";

	public boolean checkForComplaintFields = false;

	public ContactUsRequestHandler(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	public boolean handlerPost() throws IOException {
		this.sanitized = new HashMap<>();
		this.errors = new HashMap<>();
		if (request.getParameter("subject").equalsIgnoreCase(ADDITIONAL_FIELD_CONDITION)) {
			checkForComplaintFields = true;
		} else {
			checkForComplaintFields = false;
		}
		if (this.validate(request, checkForComplaintFields)) {
			sendContactUsEmail(checkForComplaintFields);
			return true;
		}

		Iterator it = errors.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry me = (Map.Entry) it.next();
		}

		request.setAttribute("sanitized", this.sanitized);
		request.setAttribute("errors", this.errors);

		return false;

	}

	private void sendContactUsEmail(boolean checkForComplaintFields) {
		
		final String TR_START = "<tr>";
		final String TR_END = "</tr>";
		final String TD_START = "<td>";
		final String TD_END = "</td>";
		
		StringBuffer tableBuffer = new StringBuffer();
		
		tableBuffer
			.append("<table style='width: 450px;'>")
				.append("<tbody>")
					.append(TR_START)
						.append(TD_START).append(BO_EMAIL_SUBJECT).append(TD_END)
						.append(TD_START).append(sanitized.get("subject")).append(TD_END)
					.append(TR_END)
					.append(TR_START)
						.append(TD_START).append(BO_EMAIL_COMMENTS).append(TD_END)
						.append(TD_START).append(sanitized.get("comments")).append(TD_END)
					.append(TR_END)
					.append(TR_START)
						.append(TD_START).append(BO_EMAIL_TITLE).append(TD_END)
						.append(TD_START).append(sanitized.get("title")).append(TD_END)
					.append(TR_END)
					.append(TR_START)
						.append(TD_START).append(BO_EMAIL_FIRST_NAME).append(TD_END)
						.append(TD_START).append(sanitized.get("first_name")).append(TD_END)
					.append(TR_END)
					.append(TR_START)
						.append(TD_START).append(BO_EMAIL_SURNAME).append(TD_END)
						.append(TD_START).append(sanitized.get("surname")).append(TD_END)
					.append(TR_END);
		if (checkForComplaintFields) {
			tableBuffer
					.append(TR_START)
						.append(TD_START).append(BO_EMAIL_NRIC).append(TD_END)
						.append(TD_START).append(sanitized.get("nric")).append(TD_END)
					.append(TR_END)
					.append(TR_START)
						.append(TD_START).append(BO_EMAIL_COUNTRY).append(TD_END)
						.append(TD_START).append(sanitized.get("country")).append(TD_END)
					.append(TR_END)
					.append(TR_START)
						.append(TD_START).append(BO_EMAIL_CONTACT_NO).append(TD_END)
						.append(TD_START).append(sanitized.get("phone_number")).append(TD_END)
					.append(TR_END);
		}		
		tableBuffer
				.append(TR_START)
					.append(TD_START).append(BO_EMAIL_ID).append(TD_END)
					.append(TD_START).append("<a href=\\\"mailto:").append(sanitized.get("email")).append("\">").append(TD_END)
				.append(TR_END)
			.append("</tbody>")
		.append("</table>");
		try {
			String BOEmailID = Config.getString("dreamcatcher.email.contactus");
			EmailSender.sendEmail(sanitized.get("email"), EMAIL_SUBJECT, EMAIL_CONTENT, "");
			EmailSender.sendEmail(BOEmailID, EMAIL_SUBJECT, tableBuffer.toString(), "");
		} catch (MessagingException e) {
			logger.error("Error While sending Contact Us mail" + e);

		}
	}
	protected boolean validate(HttpServletRequest request, boolean checkForComplaintFields) {
		this.sanitize(request, checkForComplaintFields);
		this.validateSubject();
		this.validateComments();
		this.validateTitle();
		this.validateSurname();
		this.validateFirstName();
		if (checkForComplaintFields) {
			this.validateNric();
			this.validateCountry();
			this.validatePhone();
		}
		this.validateEmail();

		return (this.errors.size() == 0);
	}

	protected void sanitize(HttpServletRequest request, boolean checkForComplaintFields) {

		this.sanitized.put("subject", WordUtils.capitalize(request.getParameter("subject")));
		this.sanitized.put("comments", request.getParameter("comments").trim());
		this.sanitized.put("title", request.getParameter("title"));
		this.sanitized.put("surname", request.getParameter("surname").trim());
		this.sanitized.put("first_name", request.getParameter("first_name").trim());
		if (checkForComplaintFields) {
			this.sanitized.put("nric", request.getParameter("nric").trim());
			this.sanitized.put("country", request.getParameter("country"));
			this.sanitized.put("phone_country_code", request.getParameter("phone_country_code").trim());
			this.sanitized.put("phone_number", request.getParameter("phone_number").trim());

		}
		this.sanitized.put("email", request.getParameter("email").trim());
	}

	protected void validateSubject() {
		this.validateStringInput("subject", "subject");
	}

	protected void validateComments() {
		this.validateStringInput("comments", "comments", 175);
	}

	protected void validateTitle() {
		this.validateStringInput("title", "title");
	}

	protected void validateSurname() {
		this.validateStringInput("surname", "surname", 100);
	}

	protected void validateFirstName() {
		this.validateStringInput("first_name", "first name", 100);
	}

	protected void validateNric() {
		this.validateStringInput("nric", "NRIC", 10);
	}

	protected void validateCountry() {
		this.validateStringInput("country", "country");
	}

	protected void validatePhone() {
		this.validateStringInput("phone_country_code", "country code", 3);
		this.validateStringInput("phone_number", "phone number", 20);

		if (this.errors.get("phone_country_code") == null) {
			Matcher matcher = Pattern.compile(PHONE_COUNTRY_CODE_PATTERN)
					.matcher(this.sanitized.get("phone_country_code"));
			if (!matcher.matches()) {
				this.errors.put("phone_country_code", "Phone country code is not valid.");
			}
		}

		if (this.errors.get("phone_number") == null) {
			Matcher matcher = Pattern.compile(PHONE_NUMBER_PATTERN).matcher(this.sanitized.get("phone_number"));
			if (!matcher.matches()) {
				this.errors.put("phone_number", "Phone number is not valid.");
			}
		}
	}

	protected void validateEmail() {
		this.validateStringInput("email", "email", 255);
		if (this.errors.get("email") == null) {
			Matcher matcher = Pattern.compile(EMAIL_PATTERN).matcher(this.sanitized.get("email"));
			if (!matcher.matches()) {
				this.errors.put("email", "Email address is not valid.");
			}
		}
	}

	protected void validateStringInput(String paramKey, String paramName) {
		this.validateStringInput(paramKey, paramName, 0);
	}

	protected void validateStringInput(String paramKey, String paramName, int maxCharLength) {
		String input = this.sanitized.get(paramKey);

		if (input == null || input.isEmpty()) {
			this.errors.put(paramKey, StringUtils.capitalize(paramName) + " is missing.");
		} else {
			if (maxCharLength > 0 && input.length() > maxCharLength) {
				this.errors.put(paramKey, StringUtils.capitalize(paramName) + " is over the limit of " + maxCharLength
						+ " characters allowed.");
			} else {
				if (paramKey.equalsIgnoreCase("first_name") || paramKey.equalsIgnoreCase("surname")) {
					Matcher matcher = Pattern.compile("[A-Za-z0-9]+").matcher(input);
					if (!matcher.matches()) {
						this.errors.put(paramKey, StringUtils.capitalize(paramName) + " contains invalid characters.");
					}
				}
			}
		}
	}
}
