package sg.com.hsbc.dreamcatcher.eclaims;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class EclaimStep3Processor {
    private EClaimBaseServlet servletInstance;

    public EclaimStep3Processor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizeBankrupt(request, sanitized);
        sanitized = sanitizeOtherInsured(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizeBankrupt(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("bankrupt");
        sanitized.put("bankrupt", Boolean.parseBoolean(input));
        if (Boolean.parseBoolean(input)) {
            ArrayList<Map<String, Object>> list = new ArrayList<>();
            String[] firstNames = request.getParameterValues("bankrupt_first_name");
            String[] lastNames = request.getParameterValues("bankrupt_last_name");
            String[] countries = request.getParameterValues("bankrupt_country");
            String[] states = request.getParameterValues("bankrupt_state");
            String[] dateDays = request.getParameterValues("bankrupt_bankruptcy_date_day");

            for (int i = 0; i < firstNames.length; i++) {
                Map<String, Object> subData = new HashMap<>();
                subData.put("bankrupt_first_name", firstNames[i]);
                subData.put("bankrupt_last_name", lastNames[i]);
                subData.put("bankrupt_country", countries[i]);
                subData.put("bankrupt_state", states[i]);
                subData.put("bankrupt_bankruptcy_date_day", dateDays[i]);

                list.add(subData);
            }

            sanitized.put("bankrupt_list", list.toArray());
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeOtherInsured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("other_insured");
        sanitized.put("other_insured", Boolean.parseBoolean(input));
        if (Boolean.parseBoolean(input)) {
            ArrayList<Map<String, Object>> list = new ArrayList<>();
            String[] companyNames = request.getParameterValues("other_insured_company_name");
            String[] dateDays = request.getParameterValues("other_insured_policy_issue_date_day");
            String[] planTypes = request.getParameterValues("other_insured_plan_type");
            String[] claim_amounts = request.getParameterValues("other_insured_claim_amount");

            for (int i = 0; i < companyNames.length; i++) {
                Map<String, Object> subData = new HashMap<>();
                subData.put("other_insured_company_name", companyNames[i]);
                subData.put("other_insured_policy_issue_date_day", dateDays[i]);
                subData.put("other_insured_plan_type", planTypes[i]);

                try {
                    subData.put("other_insured_claim_amount", BigDecimal.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).parse(claim_amounts[i]).doubleValue()));
                } catch (ParseException e) {
                    ErrorHandler.handleError(this, e);
                }

                String claim_admitted = request.getParameter("other_insured_claim_admitted_" + (i+1));
                subData.put("other_insured_claim_admitted", Boolean.parseBoolean(claim_admitted));

                list.add(subData);
            }

            sanitized.put("other_insured_list", list.toArray());
        }

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setBankrupt((boolean)data.get("bankrupt"));
        if (application.getBankrupt()) {
            ArrayList<DeathBankruptPortion> list = new ArrayList<>();
            DeathBankruptPortion[] oldList = application.getBankruptList();
            int i = 0;
            for (Object subDataItem : (Object[]) data.get("bankrupt_list")) {
                DeathBankruptPortion portion;
                if (i < oldList.length) {
                    portion = oldList[i];
                } else {
                    portion = new DeathBankruptPortion();
                }
                Map<String, Object> subData = (Map<String, Object>) subDataItem;
                portion.setFirstName(subData.get("bankrupt_first_name").toString());
                portion.setLastName(subData.get("bankrupt_last_name").toString());
                portion.setCountry(subData.get("bankrupt_country").toString());
                portion.setState(subData.get("bankrupt_state").toString());

                try {
                    Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", subData.get("bankrupt_bankruptcy_date_day")));
                    portion.setBankruptcyDate(date);
                } catch (ParseException e) {
                    ErrorHandler.handleError(application, e);
                    return false;
                }


                list.add(portion);
                i++;
            }
            application.setBankruptList(list.toArray(new DeathBankruptPortion[list.size()]));
        } else {
            application.setBankruptList(null);
        }

        application.setOtherInsured((boolean)data.get("other_insured"));
        if (application.getOtherInsured()) {
            ArrayList<DeathOtherInsuredPortion> list = new ArrayList<>();
            DeathOtherInsuredPortion[] oldList = application.getOtherInsuredList();
            int i = 0;
            for (Object subDataItem : (Object[]) data.get("other_insured_list")) {
                DeathOtherInsuredPortion portion;
                if (i < oldList.length) {
                    portion = oldList[i];
                } else {
                    portion = new DeathOtherInsuredPortion();
                }
                Map<String, Object> subData = (Map<String, Object>) subDataItem;
                portion.setCompanyName(subData.get("other_insured_company_name").toString());
                try {
                    Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", subData.get("other_insured_policy_issue_date_day")));
                    portion.setPolicyIssueDate(date);
                } catch (ParseException e) {
                    ErrorHandler.handleError(application, e);
                    return false;
                }
                portion.setPlanType(subData.get("other_insured_plan_type").toString());
                portion.setClaimAmount((BigDecimal) subData.get("other_insured_claim_amount"));
                portion.setClaimAdmitted((Boolean) subData.get("other_insured_claim_admitted"));

                list.add(portion);
                i++;
            }
            application.setOtherInsuredList(list.toArray(new DeathOtherInsuredPortion[list.size()]));
        } else {
            application.setOtherInsuredList(null);
        }

        application.setMaxStep(servletInstance.getCurrentStep() + (float)1);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("bankrupt", application.getBankrupt());

        ArrayList<Map<String, Object>> list = new ArrayList<>();
        if (application.getBankruptList() == null || application.getBankruptList().length == 0) {
            application.setBankruptList(new DeathBankruptPortion[1]);
            application.getBankruptList()[0] = new DeathBankruptPortion();
        }
        for (DeathBankruptPortion portion : application.getBankruptList()) {
            Map<String, Object> subData = new HashMap<>();

            subData.put("bankrupt_first_name", portion.getFirstName());
            subData.put("bankrupt_last_name", portion.getLastName());
            subData.put("bankrupt_country", portion.getCountry());
            subData.put("bankrupt_state", portion.getState());

            Date date = portion.getBankruptcyDate();
            if (date != null) {
                subData.put("bankrupt_bankruptcy_date_day", (new SimpleDateFormat("dd/MM/y")).format(date));
            }

            list.add(subData);
        }
        data.put("bankrupt_list", list.toArray());


        data.put("other_insured", application.getOtherInsured());
        list = new ArrayList<>();
        if (application.getOtherInsuredList() == null || application.getOtherInsuredList().length == 0) {
            application.setOtherInsuredList(new DeathOtherInsuredPortion[1]);
            application.getOtherInsuredList()[0] = new DeathOtherInsuredPortion();
        }
        for (DeathOtherInsuredPortion portion : application.getOtherInsuredList()) {
            Map<String, Object> subData = new HashMap<>();

            subData.put("other_insured_company_name", portion.getCompanyName());
            Date date = portion.getPolicyIssueDate();
            if (date != null) {
                subData.put("other_insured_policy_issue_date_day", (new SimpleDateFormat("dd/MM/y")).format(date));
            }
            subData.put("other_insured_plan_type", portion.getPlanType());
            subData.put("other_insured_claim_amount", portion.getClaimAmount());
            subData.put("other_insured_claim_admitted", portion.getClaimAdmitted());

            list.add(subData);

            data.put("other_insured_list", list.toArray());
        }

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("country_list", servletInstance.getCountryList());
        data.put("day_list", servletInstance.getDayList());
        data.put("month_list", servletInstance.getMonthList());
        data.put("year_list", servletInstance.getYearList());

        request.setAttribute("formData", data);
    }
}
