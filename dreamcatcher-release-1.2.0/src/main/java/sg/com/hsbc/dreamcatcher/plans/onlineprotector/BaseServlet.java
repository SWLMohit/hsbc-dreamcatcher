package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.BaseFormServlet;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseServlet extends BaseFormServlet {
    static String FORM_TITLE = "HSBC Insurance OnlineProtector";
    static String APPLICATION_SESSION_ATTRIBUTE_NAME = "OnlineProtectorApplication";

    public String getFormTitle() {
        return BaseServlet.FORM_TITLE;
    }

    public String getApplicationSessionAttributeName() {
        return BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME;
    }

    public String getBaseFormUrl() {
        return "/our-plans/online-protector/";
    }

    public ApplicationForm getById(String id) {
        return OnlineProtectorApplication.getById(id);
    }

    public OnlineProtectorApplication getApplication(HttpServletRequest request) {
        return (OnlineProtectorApplication)super.getApplication(request);
    }
}
