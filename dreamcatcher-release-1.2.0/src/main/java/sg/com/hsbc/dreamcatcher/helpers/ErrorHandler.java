package sg.com.hsbc.dreamcatcher.helpers;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Static methods for logging and reporting application errors.
 */
public class ErrorHandler {

    /**
     * Handle an error.
     * We will attempt to send an email with the exception to support.
     * Also write a log entry.
     * @param source the source of the error (for determining logging
     * module)
     * @param e the exception
     */
    public static void handleError(Object source, Exception e) {
        handleError(source, e, (e == null) ? null : e.getMessage());
    }

    /**
     * Handle an error.
     * We will attempt to send an email with the exception to support.
     * Also write a log entry.
     * @param source the source of the error (for determining logging
     * module)
     * @param e the exception
     * @param message message about what operation the error occurred in
     */
    public static void handleError(Object source, Exception e, String message) {
        String className = (source instanceof Class) ? ((Class) source).getName() : source.getClass().getName();
        Logger logger = Logger.getLogger(className);
        logger.log(Level.SEVERE, message, e);
        /*String address = Config.getString("dreamcatcher.email.support");
        try {
            StringWriter buf = new StringWriter();
            buf.write(message);
            buf.write("\n\n");
            buf.write(source.toString());
            if (e != null) {
                buf.write("\n\n");
                e.printStackTrace(new PrintWriter(buf, true));
            }
            String subject = "Dreamcatcher: error in module " + className;
            EmailSender.sendEmail(address, subject, buf.toString(), null);
        } catch (Exception e2) {
            logger.log(Level.SEVERE, "Failed to send email to "+address, e);
        }*/

    }

}