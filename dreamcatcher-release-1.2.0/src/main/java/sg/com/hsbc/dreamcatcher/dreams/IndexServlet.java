package sg.com.hsbc.dreamcatcher.dreams;

import sg.com.hsbc.dreamcatcher.BaseServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = "/your-dreams/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/yourdreams/index.jsp")
})
public class IndexServlet extends BaseServlet {


}
