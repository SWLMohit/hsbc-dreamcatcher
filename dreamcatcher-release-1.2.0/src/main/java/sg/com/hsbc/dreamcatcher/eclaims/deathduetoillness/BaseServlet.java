package sg.com.hsbc.dreamcatcher.eclaims.deathduetoillness;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimBaseServlet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;

public abstract class BaseServlet extends EClaimBaseServlet {
    static String FORM_TITLE = "eClaims - Death Due to Illness - Make a Claim";
    public static String APPLICATION_SESSION_ATTRIBUTE_NAME = "EClaimDeathDueToIllness";

    public String getFormTitle() {
        return BaseServlet.FORM_TITLE;
    }

    public String getApplicationSessionAttributeName() {
        return BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME;
    }

    public String getBaseFormUrl() {
        return "/eclaims/2-death-due-to-illness";
    }

    public ApplicationForm getById(String id) throws SQLException {
        return DeathDueToIllnessApplication.getById(id);
    }

    public DeathDueToIllnessApplication getApplication(HttpServletRequest request) {
        return (DeathDueToIllnessApplication)super.getApplication(request);
    }
}
