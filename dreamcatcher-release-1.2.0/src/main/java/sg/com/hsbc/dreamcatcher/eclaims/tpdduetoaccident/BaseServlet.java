package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoaccident;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimBaseServlet;
import sg.com.hsbc.dreamcatcher.eclaims.unemployment.UnemploymentApplication;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;

public abstract class BaseServlet extends EClaimBaseServlet {
    static String FORM_TITLE = "eClaims - TPD Due to Accident - Make a Claim";
    public static String APPLICATION_SESSION_ATTRIBUTE_NAME = "EClaimTPDDueToAccident";

    public String getFormTitle() {
        return BaseServlet.FORM_TITLE;
    }

    public String getApplicationSessionAttributeName() {
        return BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME;
    }

    public String getBaseFormUrl() { return "/eclaims/6-tpd-due-to-accident"; }

    public ApplicationForm getById(String id) throws SQLException { return TpdDueToAccidentApplication.getById(id); }

    public TpdDueToAccidentApplication getApplication(HttpServletRequest request) {
        return (TpdDueToAccidentApplication)super.getApplication(request);
    }
}
