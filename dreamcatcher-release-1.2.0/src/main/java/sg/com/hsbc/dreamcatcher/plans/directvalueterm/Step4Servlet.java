package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import org.apache.commons.lang.StringUtils;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/4", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step4.jsp")
})
public class Step4Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 4;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeApplicantNationality(request, sanitized);
        sanitized = sanitizeApplicantMultipleNationalities(request, sanitized);
        sanitized = sanitizeApplicantNationality2(request, sanitized);
        sanitized = sanitizeApplicantNationality3(request, sanitized);
        sanitized = sanitizeApplicantBirthCountry(request, sanitized);
        sanitized = sanitizeApplicantNric(request, sanitized);
        sanitized = sanitizeApplicantEnProficient(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
    	DirectValueTermApplication application = getApplication(request);
    	Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();

        errors = validateApplicantNationality(data, errors);
        errors = validateApplicantMultipleNationalities(data, errors);

        if (data.get(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES) != null && ((boolean) data.get(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES))) {
            errors = validateApplicantNationality2(data, errors);
            errors = validateApplicantNationality3(data, errors);
        }
		if (application.applicantIsSingaporean()|| application.applicantIsPermanentResident()) {
			errors = validateApplicantNric(data, errors);
		} else {
			errors = validateApplicantPassport(data, errors);
		}
		errors = validateApplicantBirthCountry(data, errors);
		//errors = validateApplicantNric(data, errors);
		errors = validateApplicantEnProficient(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setApplicantNationality(data.get(FieldConstants.APPLICANT_NATIONALITY).toString());
        application.setApplicantNationality2(data.get(FieldConstants.APPLICANT_NATIONALITY_2).toString());
        application.setApplicantNationality3(data.get(FieldConstants.APPLICANT_NATIONALITY_3).toString());
        application.setApplicantBirthCountry(data.get(FieldConstants.APPLICANT_BIRTH_COUNTRY).toString());
        application.setApplicantNric(data.get(FieldConstants.APPLICANT_NRIC).toString());

        application.setMaxStep(getCurrentStep() + 1);

        if (data.get(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES) != null) {
            application.setApplicantMultipleNationalities((Boolean) data.get(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES));
        }

        if (data.get(FieldConstants.APPLICANT_ENGLISH_PROFICIENT) != null) {
            application.setApplicantEnProficient((Boolean) data.get(FieldConstants.APPLICANT_ENGLISH_PROFICIENT));
        }

        return application.save();
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("/our-plans/direct-value-term/5");
    }

    private Map<String, String> validateApplicantNationality(Map<String, Object> data, Map<String, String> errors) {
        String input = data.get(FieldConstants.APPLICANT_NATIONALITY).toString();
        boolean hasError = false;

        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_NATIONALITY, "Your nationality is missing or invalid.");
                break;
        }

        if (!hasError) {
            if (input.equalsIgnoreCase("NA")) {
                errors.put(FieldConstants.APPLICANT_NATIONALITY, "Your nationality is missing or invalid.");
                errors.put("dropout", "1");
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantMultipleNationalities(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES, "Please indicate if you have multiple nationalities.");
                break;
        }

        return errors;
    }

    /**
     * This method checks the request object (sanitized hashmap) for element 'APPLICANT_NATIONALITY_2' to be not empty and
     * to be different from element 'APPLICANT_NATIONALITY'.
     * if not, then error messages will added to the error hashmap.
     * 
     * @param data    Hashmap of santized http request data. 
     * @param errors  Hashmap of error messages to be displayed to the end user.
     * @return        Updated Hashmap of error messages to be displayed to the end user.   
     */
    
    private Map<String, String> validateApplicantNationality2(Map<String, Object> data, Map<String, String> errors) {
        String input = data.get(FieldConstants.APPLICANT_NATIONALITY_2).toString();
        boolean hasError = false;

        if (data.get(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES).toString().equalsIgnoreCase("true")) {
            switch (Validator.validateString(input)) {
                case Validator.ERROR_STRING_EMPTY:
                    errors.put(FieldConstants.APPLICANT_NATIONALITY_2, "Your nationality is missing or invalid.");
                    hasError = true;
                    break;
            }
            String mainNationality = data.get(FieldConstants.APPLICANT_NATIONALITY).toString();// checking for duplicate nationality JIRA HSBCIDCU-963
            String thirdNationality = data.get(FieldConstants.APPLICANT_NATIONALITY_3).toString();
            if (StringUtils.isNotBlank(mainNationality)&& StringUtils.isNotBlank(input)
                &&Validator.isUniqueNationality(mainNationality, input)){
                errors.put(FieldConstants.APPLICANT_NATIONALITY_2,
                    "Your nationality selected cannot be same as above.");
                hasError = true;
            }
            if (!hasError) {
                if (input.equalsIgnoreCase("NA")) {
                    errors.put(FieldConstants.APPLICANT_NATIONALITY_2, "We are unable to proceed with your online application.");
                    errors.put("dropout", "1");
                }
            }
        }

        return errors;
    }

    /**
     * This method checks the request object (sanitized hashmap) for element 'APPLICANT_NATIONALITY_3' to be not empty and
     * to be different from element 'APPLICANT_NATIONALITY'and 'APPLICANT_NATIONALITY_2'..
     * if not, then error messages will added to the error hashmap.
     * 
     * @param data    Hashmap of santized http request data. 
     * @param errors  Hashmap of error messages to be displayed to the end user.
     * @return        Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantNationality3(Map<String, Object> data, Map<String, String> errors) {
      String input = data.get(FieldConstants.APPLICANT_NATIONALITY_3).toString();
      if (data.get(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES).toString().equalsIgnoreCase("true")) {
      String mainNationality = data.get(FieldConstants.APPLICANT_NATIONALITY).toString();// checking for duplicate nationality JIRA HSBCIDCU-963
      String secondNationality = data.get(FieldConstants.APPLICANT_NATIONALITY_2).toString();
      if ((StringUtils.isNotBlank(mainNationality)&& StringUtils.isNotBlank(input)
          &&(Validator.isUniqueNationality(mainNationality, input)))
          ||(StringUtils.isNotBlank(secondNationality) && StringUtils.isNotBlank(input)
              && Validator.isUniqueNationality(secondNationality, input))){
          errors.put(FieldConstants.APPLICANT_NATIONALITY_3,
              "Your nationality selected cannot be same as above.");   
      }
     }
        return errors;
    }

    private Map<String, String> validateApplicantBirthCountry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_BIRTH_COUNTRY))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_BIRTH_COUNTRY, "Your country of birth is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantNric(Map<String, Object> data, Map<String, String> errors) { //JIRA 625
        switch (Validator.validateNric(data.get(FieldConstants.APPLICANT_NRIC))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_NRIC, "Your NRIC number is missing.");
                break;
            case Validator.ERROR_NRIC_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_NRIC, "Please enter a valid NRIC number.");
                break;
        }

        return errors;
    }
   
    private Map<String, String> validateApplicantPassport(Map<String, Object> data, Map<String, String> errors) { //JIRA 625
        switch (Validator.validatePassport(data.get(FieldConstants.APPLICANT_NRIC))) {
        
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_PASSPORT, "Your passport number is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.APPLICANT_PASSPORT, "Please enter a valid Passport number.");
                break;
            case Validator.ERROR_PASSPORT_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_PASSPORT, "Please enter a valid Passport number.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantEnProficient(Map<String, Object> data, Map<String, String> errors) {
        String input = (data.get(FieldConstants.APPLICANT_ENGLISH_PROFICIENT) != null ? data.get(FieldConstants.APPLICANT_ENGLISH_PROFICIENT).toString() : "");
        boolean hasError = false;

        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_ENGLISH_PROFICIENT, "Please indicate if you are proficient in English.");
                hasError = true;
                break;
        }

        if (!hasError) {
            if (!input.equalsIgnoreCase("true")) {
                errors.put(FieldConstants.APPLICANT_ENGLISH_PROFICIENT, "We are unable to proceed with your online application.");
                errors.put("dropout", "1");
            }
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantNationality(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_NATIONALITY);

        sanitized.put(FieldConstants.APPLICANT_NATIONALITY, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantMultipleNationalities(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES);
        if (input != null) {
            sanitized.put(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantNationality2(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_NATIONALITY_2);

        sanitized.put(FieldConstants.APPLICANT_NATIONALITY_2, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantNationality3(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_NATIONALITY_3);

        sanitized.put(FieldConstants.APPLICANT_NATIONALITY_3, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantBirthCountry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_BIRTH_COUNTRY);

        sanitized.put(FieldConstants.APPLICANT_BIRTH_COUNTRY, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantNric(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_NRIC);

        sanitized.put(FieldConstants.APPLICANT_NRIC, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantEnProficient(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_ENGLISH_PROFICIENT);
        if (input != null) {
            sanitized.put(FieldConstants.APPLICANT_ENGLISH_PROFICIENT, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.APPLICANT_ENGLISH_PROFICIENT, null);
        }

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        data.put(FieldConstants.APPLICANT_MULTIPLE_NATIONALITIES, application.getApplicantMultipleNationalities());
        if (application.applicantIsSingaporean() && StringUtils.isEmpty(application.getApplicantNationality())) {
            data.put(FieldConstants.APPLICANT_NATIONALITY, "Singapore");
        } else {
            data.put(FieldConstants.APPLICANT_NATIONALITY, application.getApplicantNationality());
        }
        data.put(FieldConstants.APPLICANT_NATIONALITY_2, application.getApplicantNationality2());
        data.put(FieldConstants.APPLICANT_NATIONALITY_3, application.getApplicantNationality3());
        data.put(FieldConstants.APPLICANT_BIRTH_COUNTRY, application.getApplicantBirthCountry());
        data.put(FieldConstants.APPLICANT_NRIC, application.getApplicantNric());
        data.put(FieldConstants.APPLICANT_ENGLISH_PROFICIENT, application.getApplicantEnProficient());

        if (data.get(FieldConstants.APPLICANT_BIRTH_COUNTRY) == null) {
            data.put(FieldConstants.APPLICANT_BIRTH_COUNTRY, application.getApplicantNationality());
        }

        request.setAttribute("data", data);
    }

}