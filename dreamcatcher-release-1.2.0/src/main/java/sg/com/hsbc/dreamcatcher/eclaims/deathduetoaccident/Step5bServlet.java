package sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplicationJob;

@WebServlet(urlPatterns = "/eclaims/1-death-due-to-accident/5a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/1_death_due_to_accident/step5b.jsp"),
        @WebInitParam(name = "threadpoolsize", value = "20")
}, asyncSupported = true)
public class Step5bServlet extends BaseServlet {
    public float getCurrentStep() {
        return (float) 5.5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        return true;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DeathDueToAccidentApplication application = getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        data.put("referenceNumber", application.getReferenceNumber());
        data.put("fileSizeError", application.fileSizeExceeded());
        request.setAttribute("data", data);

        removeFromSession(request);

        // NB see comments in EClaimApplicationJob
        ExecutorService threadPool = (ExecutorService) getServletConfig().getServletContext().getAttribute("threadPool");
        threadPool.execute(new EClaimApplicationJob(application));
    }
}

