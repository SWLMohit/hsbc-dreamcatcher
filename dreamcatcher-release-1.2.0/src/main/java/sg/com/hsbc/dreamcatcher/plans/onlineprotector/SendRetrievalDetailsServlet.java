package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import sg.com.hsbc.dreamcatcher.helpers.Config;
import sg.com.hsbc.dreamcatcher.helpers.*;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

@WebServlet(urlPatterns = "/our-plans/online-protector/dropout")
public class SendRetrievalDetailsServlet extends HttpServlet {
  private final static Logger logger = Logger.getLogger(SendRetrievalDetailsServlet.class);
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String id = request.getSession().getAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME).toString();
       try{
        OnlineProtectorApplication application = OnlineProtectorApplication.getById(id);
        if (application.getDropoutPassword() == null) {
            application.setDropoutPassword(UUID.randomUUID().toString().split("-")[0].toLowerCase());
            if (application.save()) {
                try {
                    sendEmail(application);
                    sendSMS(application);

                    request.getSession().setAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME, null);
                } catch (MessagingException e) {
                    ErrorHandler.handleError(application, e, "Error sending messages for application");
                }
            }
        }
       }catch(Exception e){
         logger.error("Exception occurred while creating a drop out "+e);
       }
    }

    private void sendEmail(OnlineProtectorApplication application) throws MessagingException {
        String subject = "How to retrieve your HSBC Insurance application";
        String message = "Dear " + application.getApplicantFirstName() + ",\n\n" +
                "Thank you for your interest in HSBC Insurance! We have sent you a verification code via SMS to retrieve your saved application within the next 30 days.\n\n" +
                "To retrieve your application, simply:\n" +
                "1. Visit HSBC Insurance Online\n" +
                "2. Click on 'Our Plans'\n" +
                "3. Click on the 'Retrieve Your Form' button\n" +
                "4. Enter your email address and verification code\n\n" +
                "We look forward to receiving your completed application.\n\n\n" +
                "Sincerely,\n" +
                "HSBC Insurance Online Team";
        String toEmail = String.format("%s <%s>", application.getApplicantFullName(), application.getApplicantEmail());
        EmailSender.sendEmail(toEmail, subject, message, null);
    }

    private void sendSMS(OnlineProtectorApplication application) throws MessagingException {
        AmazonSNS sns = AmazonSNSClient.builder().withRegion(Regions.AP_SOUTHEAST_1).build();
        String message = String.format("Your verification code is: %s. Please use this code to retrieve your application within the next 30 days.", application.getDropoutPassword());
        String phoneNumber = application.getApplicantFullMobileNum();
        Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
        try{
            smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                    .withStringValue("HSBCInsure") //The sender ID shown on the device.
                    .withDataType("String"));
                PublishResult result = sns.publish(new PublishRequest()
                        .withMessage(message)
                        .withPhoneNumber(phoneNumber)
                        .withMessageAttributes(smsAttributes));
        }catch(Exception e){
          logger.error("Exception occurred while sending sms "+e);
        }
    }

}
