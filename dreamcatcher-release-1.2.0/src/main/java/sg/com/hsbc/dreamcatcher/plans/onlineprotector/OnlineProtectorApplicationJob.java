package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.*;

import java.io.IOException;
import java.sql.SQLException;
import javax.mail.MessagingException;
import java.util.logging.Logger;

/**
 * A job to send the notifications for an application.
 *
 * Rather than firing off this in a separate thread, it would be better
 * to invoke it as an async job via AJAX, so that the user can be
 * informed about the progress of the PDF generation and the rest of the
 * process, and whether it was successful or not. Here is an example:
 * https://stackoverflow.com/questions/10587771/call-asynchronous-servlet-from-ajax
 * If so it would be better to use XML messages rather than JSON as in
 * this one, for security reasons (temptation of the JavaScript
 * developer to eval() the response).
 */

public class OnlineProtectorApplicationJob implements Runnable {

    private final OnlineProtectorApplication application;

    public OnlineProtectorApplicationJob(OnlineProtectorApplication application) {
        this.application = application;
    }

    public void run() {
        Logger logger = Logger.getLogger(getClass().getName());
        try {
            // Send the notifications to the user, and log that we have done
            // so
            application.sendNotifications();
            logger.info("Sent notifications to user for application " + application.getId());
        /*} catch (MessagingException e) {
            ErrorHandler.handleError(application, e, "Messaging error sending notifications");
        } catch (IOException e) {
            ErrorHandler.handleError(application, e, "I/O error sending notifications");
        } catch (SQLException e) {
            ErrorHandler.handleError(application, e, "SQL error sending notifications");*/
        }   catch (Exception e) {
            ErrorHandler.handleError(application, e, "error sending notifications");
        }
    }
}
