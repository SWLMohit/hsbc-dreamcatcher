package sg.com.hsbc.dreamcatcher;

import sg.com.hsbc.dreamcatcher.jobs.PurgeExpiredApplicationsJob;
import sg.com.hsbc.dreamcatcher.jobs.SendApplicationNotificationsJob;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@WebListener
public class ApplicationStartUpListener implements ServletContextListener {

    /**
     * The maximum number of threads that will be assigned for creating PDFs
     * and emailing them out.
     * */
    static final int MAX_THREADS = 32;

    private ScheduledExecutorService scheduler1;
    private ScheduledExecutorService scheduler2;
    private ExecutorService threadPool;
    public static String rootPath;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        scheduler1 = Executors.newSingleThreadScheduledExecutor();
        scheduler1.scheduleAtFixedRate(new SendApplicationNotificationsJob(), 0, 60, TimeUnit.SECONDS);

        scheduler2 = Executors.newSingleThreadScheduledExecutor();
        scheduler2.scheduleAtFixedRate(new PurgeExpiredApplicationsJob(), 0, 1, TimeUnit.DAYS);

        rootPath = event.getServletContext().getRealPath("/");

        // Create a thread pool that will be used to service requests for
        // long-running application jobs. Save it as a context attribute so
        // the servlets have access to it.
        threadPool = Executors.newFixedThreadPool(MAX_THREADS);
        ServletContext ctx = event.getServletContext();
        ctx.setAttribute("threadPool", threadPool);

    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        scheduler1.shutdownNow();
        scheduler2.shutdownNow();
        threadPool.shutdown();
    }
}
