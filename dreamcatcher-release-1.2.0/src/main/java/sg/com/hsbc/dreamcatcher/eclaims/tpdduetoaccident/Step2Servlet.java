package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoaccident;

import sg.com.hsbc.dreamcatcher.eclaims.EclaimStep3Processor;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/6-tpd-due-to-accident/2", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/6_tpd_due_to_accident/step2.jsp")
})
public class Step2Servlet extends BaseServlet {
    private EclaimStep3Processor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new EclaimStep3Processor(this);
    }

    public float getCurrentStep() {
        return (float)2;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        return processor.process(request);
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void populateFormData(HttpServletRequest request) {
        processor.populateFormData(request);
    }

    protected float getStep() {
        return (float)1;
    }
}
