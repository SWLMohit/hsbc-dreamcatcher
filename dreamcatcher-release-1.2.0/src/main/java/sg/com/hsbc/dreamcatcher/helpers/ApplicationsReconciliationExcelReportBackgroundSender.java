package sg.com.hsbc.dreamcatcher.helpers;

import sg.com.hsbc.dreamcatcher.plans.TimePeriod;

import javax.mail.MessagingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@WebListener
public class ApplicationsReconciliationExcelReportBackgroundSender implements ServletContextListener {
	
	private final static Logger logger = LoggerFactory.getLogger(ApplicationsReconciliationExcelReportBackgroundSender.class);

    private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

    @Override
    public void contextInitialized(ServletContextEvent event) {

        class nineAMRunnable implements Runnable {
            @Override
            public void run() {
                excelGeneratorWrapper(TimePeriod.SEVENPM_NINEAM);
            }
        }
        class elevenAMRunnable implements Runnable {
            @Override
            public void run() {
                excelGeneratorWrapper(TimePeriod.NINEAM_ELEVENAM);
            }
        }
        class oneThirtyPMRunnable implements Runnable {
            @Override
            public void run() {
                excelGeneratorWrapper(TimePeriod.ELEVENAM_ONETHIRTYPM);
            }
        }
        class fourPMRunnable implements Runnable {
            @Override
            public void run() {
                excelGeneratorWrapper(TimePeriod.ONETHIRTYPM_FOURPM);
            }
        }
        class sevenPMRunnable implements Runnable {
            @Override
            public void run() {
                excelGeneratorWrapper(TimePeriod.FOURPM_SEVENPM);
            }
        }

        try {
            String enabledJob=Config.getString("dreamcatcher.recon.job.enabled");
            if (!"true".equalsIgnoreCase(enabledJob)) {
                return;
            }

            List<Integer> job1Schedule = readConfig("dreamcatcher.recon.applications.job1");
	        Long nineAM = calculateDelay(job1Schedule.get(0),job1Schedule.get(1),job1Schedule.get(2));
	        logger.info("nineAM:" + nineAM);
	        executorService.scheduleAtFixedRate(new nineAMRunnable(), nineAM, job1Schedule.get(2), TimeUnit.MINUTES);

	        List<Integer> job2Schedule = readConfig("dreamcatcher.recon.applications.job2");
            Long elevenAM = calculateDelay(job2Schedule.get(0),job2Schedule.get(1),job2Schedule.get(2));
	        logger.info("elevenAM:" + elevenAM);
	        executorService.scheduleAtFixedRate(new elevenAMRunnable(), elevenAM, job2Schedule.get(2), TimeUnit.MINUTES);

	        List<Integer> job3Schedule = readConfig("dreamcatcher.recon.applications.job3");
            Long oneThirtyPM = calculateDelay(job3Schedule.get(0),job3Schedule.get(1),job3Schedule.get(2));
	        logger.info("oneThirtyPM:" + oneThirtyPM);
	        executorService.scheduleAtFixedRate(new oneThirtyPMRunnable(), oneThirtyPM, job3Schedule.get(2), TimeUnit.MINUTES);

	        List<Integer> job4Schedule = readConfig("dreamcatcher.recon.applications.job4");
	        Long fourPM = calculateDelay(job4Schedule.get(0),job4Schedule.get(1),job4Schedule.get(2));
	        logger.info("fourPM:" + fourPM);
	        executorService.scheduleAtFixedRate(new fourPMRunnable(), fourPM, job4Schedule.get(2), TimeUnit.MINUTES);

            List<Integer> job5Schedule = readConfig("dreamcatcher.recon.applications.job5");
	        Long sevenPM = calculateDelay(job5Schedule.get(0),job5Schedule.get(1),job5Schedule.get(2));
	        logger.info("sevenPM:" + sevenPM);
	        executorService.scheduleAtFixedRate(new sevenPMRunnable(), sevenPM, job5Schedule.get(2), TimeUnit.MINUTES);
        } catch (Exception e) {
        	logger.error("contextInitialized",e);
        }
    }

    /**
     * This method reads the app.properties file for the job start time
     * @param config    The configuration in property file
     * @return List of integer values containing the start hour, minute and interval
     */
    private List<Integer> readConfig(String config) {
        String job1Time = Config.getString(config);
        String[] job1Schedule = job1Time.split(",");
        int hour = Integer.parseInt(job1Schedule[0]);
        int minute = Integer.parseInt(job1Schedule[1]);
        int interval = Integer.parseInt(job1Schedule[2]);
        ArrayList<Integer> jobSchedule = new ArrayList<Integer>();
        jobSchedule.add(hour);
        jobSchedule.add(minute);
        jobSchedule.add(interval);
        return jobSchedule;
    }
    
    /**
     * This method calculate the delay used in scheduler
     * @param hour      the start hour
     * @param minute    the start minute
     * @param interval  the interval
     * @return          the delay value
     */
    private Long calculateDelay(int hour, int minute, int interval)  {
        Long delay = LocalDateTime.now().withSecond(0).withNano(0).until(LocalDate.now(ZoneId.of("+8")).atTime(hour,minute,0),ChronoUnit.MINUTES);
        logger.info("delay:" + delay);
        if (delay<0) {
        	//if it's in the past, then delay till the next interval. This prevent job from starting for each tomcat restart
        	int i= 0;
        	do {
        		i++;
                int nextInterval = interval * i;
                logger.info("nextInterval:" + nextInterval);
                delay = LocalDateTime.now().withSecond(0).withNano(0).until(LocalDate.now(ZoneId.of("+8")).atTime(hour,minute,0).plusMinutes(nextInterval),ChronoUnit.MINUTES);
        	} while (delay<0);
        	logger.info("delay-new:" + delay);
        }
        return delay;
    }
    
    /**
     * Return a new calendar after adjusting by an interval
     * @param hour
     * @param minute
     * @param second
     * @param ms
     * @param endInterval
     * @return calendar
     */
	private Calendar getAdjustedCalendar(int hour, int minute, int second, int ms, int endInterval) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		calendar.set(Calendar.MILLISECOND, ms);
		calendar.add(Calendar.MINUTE, endInterval);
		return calendar;
	}

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        executorService.shutdownNow();
    }

    private void excelGeneratorWrapper(TimePeriod timePeriod){
    	try {
	        LocalDate date = LocalDate.now();
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        String formattedDate = date.format(formatter);
	
	        String time = timePeriod.getTimeTo().replace(':','-');
	
	        String filename = String.format("%s/%s_%s_%s_HSBC.xlsx", System.getProperty("java.io.tmpdir"), formattedDate, time , "ApplicationsReport");
	
	        ApplicationsReconciliationExcelGenerator gen = new ApplicationsReconciliationExcelGenerator(filename,1);
	        
	        logger.info("Applications reconciliation batch job running started at {}", LocalDateTime.now());
	        
	        //get data
	        gen.createSheet("Report");
	        gen.addReportHeader("simple", timePeriod, true);
	        gen.addSubmissions(timePeriod, "simple");
	        gen.addBlankRow();
	        gen.addReportHeader("complex", timePeriod, false);
	        gen.addSubmissions(timePeriod, "complex");
	        gen.addBlankRow();
	        gen.addSubtotalSection("For time period",timePeriod);
	        gen.addSubtotalSection("Cumulative",TimePeriod.SEVENPM_SEVENPM);
	        gen.save();
	
	        //send the excel
	        sendEmail(Config.getString("dreamcatcher.email.admin.recon.applications"), formattedDate, timePeriod.getTimeTo(), filename);
	        
	        logger.info("Applications reconciliation batch job running completed at " + LocalDateTime.now());
    	} catch (Exception e) {
    		logger.error("",e);
    	}
    }

    public void sendEmail(String toAddress, String formattedDate, String time, String attachmentPath) {
        String subject = String.format("HSBC Insurance Online Singapore Reconciliation (Completed Applications) Report for %s at %s", formattedDate, time);
        String message = "This is a system generated message. Please do not reply to this email directly.";
        try {
            logger.info("Attempt to send out email");
            EmailSender.sendEmail(toAddress, subject, message, attachmentPath);
            logger.info("Email successfully sent out");
        } catch (MessagingException e) {
            ErrorHandler.handleError(this, e);
            int attempts = 0;
            while (attempts++ < 3) {
                try {
                    logger.info("we failed to send out email, will try again in 30 seconds");
                    Thread.sleep(30000);
                    EmailSender.sendEmail(toAddress, subject, message, attachmentPath);
                    logger.info("Email successfully sent out");
                    break; //stops the loop
                } catch (MessagingException e1) {
                    ErrorHandler.handleError(this, e1);
                } catch (InterruptedException e2) {
                    ErrorHandler.handleError(this, e2);
                    break; //stops the loop
                }
            }
        }
    }
}
