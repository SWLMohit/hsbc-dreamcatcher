package sg.com.hsbc.dreamcatcher.admin.api.dvt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sg.com.hsbc.dreamcatcher.admin.AdminUtil;
import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendHistory;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendStatus;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendType;
import sg.com.hsbc.dreamcatcher.admin.repository.ApplicationsRepository;
import sg.com.hsbc.dreamcatcher.admin.repository.ResendHistoryRepository;
import sg.com.hsbc.dreamcatcher.admin.service.AdminService;
import sg.com.hsbc.dreamcatcher.admin.service.impl.AdminDVTServiceImpl;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.DirectValueTermApplication;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/retrieve-pass-code/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/retrieve_pass_code.jsp")
})
public class DVTRetrievePassCodeApi extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(DVTRetrievePassCodeApi.class);

    private AdminService adminDVTService;
    private Applications applications;
    private ResendHistoryRepository resendHistoryRepository;
    private ResendHistory resendHistory;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("direct-value-term retrieve pass code page");
        render(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> data = new HashMap<>();
        Map<String, String> errors;

        errors = validate(request);
        if (!errors.isEmpty()) {
            render(request, response);
            return;
        }

        adminDVTService = new AdminDVTServiceImpl();
        applications = new Applications();
        applications.setProductType(FieldConstants.OP_PRODUCT_TYPE);
        applications.setApplicantEmail(request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL));
        applications.setApplicantMobileCountryCode(request.getParameter(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE));
        applications.setApplicantMobileNum(request.getParameter(FieldConstants.APPLICANT_MOBILE_NUM));

        try{
        	ApplicationsRepository applicationsRepository = new ApplicationsRepository();
            List<Applications> applicationList = applicationsRepository.findByMobileNoAndEmail(DirectValueTermApplication.PRODUCT_TYPE, 1,
                    applications.getApplicantMobileCountryCode(),applications.getApplicantMobileNum(),applications.getApplicantEmail());
            if ( applicationList==null||applicationList.size()<=0) {
                boolean emailExist = adminDVTService.isImcompleteEmailExisted(applications.getApplicantEmail());
                boolean mobileExist = adminDVTService.isImcompleteMobileExisted(applications.getApplicantMobileCountryCode(),applications.getApplicantMobileNum());

                errors = AdminUtil.validateEmailAndMobileNotMatch(emailExist, mobileExist);
            } else {
                if (applicationList.size() > 1) {
                    // multiple policy
                    String policyList = AdminUtil.convertListToString(applicationList);
                    logger.info("Policy list ", policyList);

                    request.getSession().setAttribute("PolicyList", policyList);
                    request.getSession().setAttribute(FieldConstants.RETRIEVE_TYPE, FieldConstants.RETRIEVE_PASS_CODE);
                    response.sendRedirect("/our-plans/direct-value-term/resend-form-multi/");
                    return;
                } else {
                    applications = applicationList.get(0);
                    //single policy
                    //Send pass code via sms
                    adminDVTService.resendSMS(applications);
                    request.getSession().setAttribute(FieldConstants.RETRIEVE_TYPE, FieldConstants.RETRIEVE_PASS_CODE);
                    logger.info("Send SMS successful");

                    //send sms success and save to db
                    resendHistoryRepository = new ResendHistoryRepository();
                    resendHistory = AdminUtil.convertApplicationsToResendHistory(
                            applications, ResendType.DVT_RESEND_VERIFICATION_CODE, ResendStatus.SUCCESS,new Date());
                    resendHistoryRepository.save(resendHistory);
                    logger.info("{} save to db success", ResendType.DVT_RESEND_VERIFICATION_CODE);

                    errors.put("success", "success");
                }
            }
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
            errors.put(FieldConstants.RETRIEVAL_FORM_EMAIL, "Sorry, either your email address or phone number is incorrect. Please try again.");
            throw e;
        }
        
        request.setAttribute("errors", errors);
        render(request, response);
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }
    

    
    /**
     * This is the main validation function for the retrieval servlet. It will call individual function to validate each field.
     * @param request
     * @return errors
     */
    public Map<String, String> validate(HttpServletRequest request) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();

        String inputEmail = request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL);
        data.put(FieldConstants.RETRIEVAL_FORM_EMAIL, inputEmail);

        String applicantCountryCode = request.getParameter(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE);
        data.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, applicantCountryCode);

        String applicantPhoneNum = request.getParameter(FieldConstants.APPLICANT_MOBILE_NUM);
        data.put(FieldConstants.APPLICANT_MOBILE_NUM, applicantPhoneNum);

        errors = validateApplicantEmail(data, errors);
        errors = AdminUtil.validateApplicantCountryCode(data, errors);
        errors = validateApplicantPhoneNum(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return errors;
    }

    /**
     * This function validates the email field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantEmail(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.RETRIEVAL_FORM_EMAIL))) {
	        case Validator.ERROR_STRING_EMPTY:
	            errors.put(FieldConstants.RETRIEVAL_FORM_EMAIL, "Your email address is missing.");
	            break;
	    }
	    
	    switch (Validator.validateEmail(data.get(FieldConstants.RETRIEVAL_FORM_EMAIL))) {
	        case Validator.ERROR_EMAIL_FORMAT_INVALID:
	            errors.put(FieldConstants.RETRIEVAL_FORM_EMAIL, "Your email address is invalid.");
	            break;
	    }

        return errors;
    }

    /**
     * This function validates the phone number field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantPhoneNum(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_MOBILE_NUM))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_MOBILE_NUM, "Your phone number is missing.");
                break;
        }

        return errors;
    }

}
