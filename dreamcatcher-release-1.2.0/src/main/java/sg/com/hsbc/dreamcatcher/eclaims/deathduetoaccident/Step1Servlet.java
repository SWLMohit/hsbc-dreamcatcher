package sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimBaseServlet;
import sg.com.hsbc.dreamcatcher.eclaims.EclaimStep1Processor;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/1-death-due-to-accident/1", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/1_death_due_to_accident/step1.jsp")
})
@MultipartConfig
public class Step1Servlet extends BaseServlet {
  private final static Logger logger = Logger.getLogger(Step1Servlet.class);
    private EclaimStep1Processor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new EclaimStep1Processor(this);
    }

    public float getCurrentStep() {
        return (float)1;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        boolean result = false;
        try {
            result = processor.process(request);
        }
        catch(Exception e) {
          logger.error("Exception occurred in EclaimStep1Processor process method "+e);
            processor.populateFormData(request);
        }
        return result;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void populateFormData(HttpServletRequest request) {
        processor.populateFormData(request);
    }

    protected float getStep() {
        return (float)1;
    }
}
