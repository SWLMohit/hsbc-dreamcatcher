package sg.com.hsbc.dreamcatcher.plans.investment;

import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = "/our-plans/investment-linked-plan/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/jsp/view/plans/investment/index.jsp")
})
public class IndexServlet extends BaseFormServlet {
}
