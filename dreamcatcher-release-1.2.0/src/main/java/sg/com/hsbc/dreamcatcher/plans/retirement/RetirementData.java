package sg.com.hsbc.dreamcatcher.plans.retirement;

/**
 * This class serves to hold the data for a session in the retirement calculator.
 * To create a instance of this class use {@link #getInstance()}
 *
 */
public class RetirementData {

    private static RetirementData instance = null;

    private String name;
    private int age;
    private int retirementAge;
    private int expectedEndRetirement = 83;


    private int monthlyIncome;
    private int monthlySpending;
    private int monthlySavings;
    private int annualIncome;
    private int annualSavings;

    private int liabilities;
    private int existingInvestments;
    private double expectedReturns;
    private double expectedInflation;

    private int retirementMonthlyExpenses;
    private int expectedBigTicketPurchases;
    private int aspirationalLumpSum;

    //calculated vars
    private int yearsToRetire;
    private int yearsInRetirement;

    private int futureValue;
    private int projectedMontlyIncome;
    private int requiredLumpSum;
    private int presentValue;

    private int shortfall;

    protected RetirementData() {
        // Exists only to defeat instantiation.
    }
    public static RetirementData getInstance() {
        if(instance == null) {
            instance = new RetirementData();
        }
        return instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getRetirementAge() {
        return retirementAge;
    }

    public void setRetirementAge(int retirementAge) {
        this.retirementAge = retirementAge;
    }

    public int getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(int monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public int getMonthlySpending() {
        return monthlySpending;
    }

    public void setMonthlySpending(int monthlySpending) {
        this.monthlySpending = monthlySpending;
    }

    public int getMonthlySavings() {
        return monthlySavings;
    }

    public void setMonthlySavings(int monthlySavings) {
        this.monthlySavings = monthlySavings;
    }

    public int getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(int annualIncome) {
        this.annualIncome = annualIncome;
    }

    public int getAnnualSavings() {
        return annualSavings;
    }

    public void setAnnualSavings(int annualSavings) {
        this.annualSavings = annualSavings;
    }

    public int getLiabilities() {
        return liabilities;
    }

    public void setLiabilities(int liabilities) {
        this.liabilities = liabilities;
    }

    public int getExistingInvestments() {
        return existingInvestments;
    }

    public void setExistingInvestments(int existingInvestments) {
        this.existingInvestments = existingInvestments;
    }

    public double getExpectedReturns() {
        return expectedReturns;
    }

    public void setExpectedReturns(double expectedReturns) {
        this.expectedReturns = expectedReturns;
    }

    public double getExpectedInflation() {
        return expectedInflation;
    }

    public void setExpectedInflation(double expectedInflation) {
        this.expectedInflation = expectedInflation;
    }

    public int getRetirementMonthlyExpenses() {
        return retirementMonthlyExpenses;
    }

    public void setRetirementMonthlyExpenses(int retirementMonthlyExpenses) {
        this.retirementMonthlyExpenses = retirementMonthlyExpenses;
    }

    public int getExpectedBigTicketPurchases() {
        return expectedBigTicketPurchases;
    }

    public void setExpectedBigTicketPurchases(int expectedBigTicketPurchases) {
        this.expectedBigTicketPurchases = expectedBigTicketPurchases;
    }

    public int getAspirationalLumpSum() {
        return aspirationalLumpSum;
    }

    public void setAspirationalLumpSum(int aspirationalLumpSum) {
        this.aspirationalLumpSum = aspirationalLumpSum;
    }

    public int getYearsToRetire() {
        return yearsToRetire;
    }

    public void setYearsToRetire(int yearsToRetire) {
        this.yearsToRetire = yearsToRetire;
    }

    public int getYearsInRetirement() {
        return yearsInRetirement;
    }

    public void setYearsInRetirement(int yearsInRetirement) {
        this.yearsInRetirement = yearsInRetirement;
    }

    public int getFutureValue() {
        return futureValue;
    }

    public void setFutureValue(int futureValue) {
        this.futureValue = futureValue;
    }

    public int getProjectedMontlyIncome() {
        return projectedMontlyIncome;
    }

    public void setProjectedMontlyIncome(int projectedMontlyIncome) {
        this.projectedMontlyIncome = projectedMontlyIncome;
    }

    public int getRequiredLumpSum() {
        return requiredLumpSum;
    }

    public void setRequiredLumpSum(int requiredLumpSum) {
        this.requiredLumpSum = requiredLumpSum;
    }

    public int getPresentValue() {
        return presentValue;
    }

    public void setPresentValue(int presentValue) {
        this.presentValue = presentValue;
    }

    public int getShortfall() {
        return shortfall;
    }

    public void setShortfall(int shortfall) {
        this.shortfall = shortfall;
    }

    public int getExpectedEndRetirement() {
        return expectedEndRetirement;
    }
}
