package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import java.io.IOException;
import javax.mail.MessagingException;
import java.util.logging.Logger;

/**
 * A job to generate the PDFs for an application and send them to the
 * recipients.
 *
 * Rather than firing off this in a separate thread, it would be better
 * to invoke it as an async job via AJAX, so that the user can be
 * informed about the progress of the PDF generation and the rest of the
 * process, and whether it was successful or not. Here is an example:
 * https://stackoverflow.com/questions/10587771/call-asynchronous-servlet-from-ajax
 * If so it would be better to use XML messages rather than JSON as in
 * this one, for security reasons (temptation of the JavaScript
 * developer to eval() the response).
 */

public class DirectValueTermApplicationJob implements Runnable {

    private final DirectValueTermApplication application;

    public DirectValueTermApplicationJob(DirectValueTermApplication application) {
        this.application = application;
    }

    public void run() {
        Logger logger = Logger.getLogger(getClass().getName());
        try {
            // Generate the PDFs
            application.generatePDFs();
            try {
                // Send the notifications to the user, and log that we have
                // done so
                application.sendNotifications();
                logger.info("Sent notifications to user for application " + application.getId());
            } catch (MessagingException e2) {
                ErrorHandler.handleError(application, e2, "Failed to send notifications");
            }
        } catch (IOException e) {
            ErrorHandler.handleError(application, e, "Failed to generate PDFs");
        }
    }

}
