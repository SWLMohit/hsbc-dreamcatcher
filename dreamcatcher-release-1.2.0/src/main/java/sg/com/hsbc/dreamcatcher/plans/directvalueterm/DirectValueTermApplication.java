package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import be.quodlibet.boxable.*;

import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.poi.util.StringUtil;
import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.ApplicationStartUpListener;
import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.Constants;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.ProductCalculation;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.activation.FileDataSource;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static sg.com.hsbc.dreamcatcher.plans.Constants.DVT_20_YEARS_PRODUCT_CODE;
import static sg.com.hsbc.dreamcatcher.plans.Constants.DVT_5_YEARS_PRODUCT_CODE;
import static sg.com.hsbc.dreamcatcher.plans.Constants.DVT_AGE_65_PRODUCT_CODE;

public class DirectValueTermApplication extends ApplicationForm {

    public static final int PAYMENT_MODE_ANNUALLY = 1;
    public static final int PAYMENT_MODE_SEMI_ANNUALLY = 2;
    public static final int PAYMENT_MODE_QUARTERLY = 4;
    public static final int PAYMENT_MODE_MONTHLY = 12;
    public static final int UNDERWRITING_CLASS_PREFERRED = 0;
    public static final int UNDERWRITING_CLASS_STANDARD = 1;
    public static final int UNDERWRITING_CLASS_PREFERRED_PLUS = 2;
    public static final int UNDERWRITING_CLASS_STANDARD_PLUS = 3;
    public static final int NOTIFY_STATUS_NOT_SENT = 0;
    public static final int NOTIFY_STATUS_PROCESSING = 99;
    public static final int NOTIFY_STATUS_SENT = 1;
    public static final int MAIL_OPTION_EMAIL = 1;
    public static final int MAIL_OPTION_MAILOUT = 0;
    public static final int PDF_TYPE_BENEFIT_ILLUSTRATION = 1;
    public static final int PDF_TYPE_APPLICATION = 2;

    public static final int UNDERWRITING_LIFESTYLE_MORTGAGE_NA = 0;
    public static final int UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_12_MONTHS = 1;
    public static final int UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_24_MONTHS = 2;
    public static final int UNDERWRITING_LIFESTYLE_MORTGAGE_MORE_THAN_24_MONTHS = 3;

    public static final int UNDERWRITING_LIFESTYLE_PROFILE_NON_HSBC = 0;
    public static final int UNDERWRITING_LIFESTYLE_PROFILE_HSBC_JADE = 1;
    public static final int UNDERWRITING_LIFESTYLE_PROFILE_HSBC_PREMIER = 2;
    public static final int UNDERWRITING_LIFESTYLE_PROFILE_HSBC_ADVANCE = 3;
    public static final int UNDERWRITING_LIFESTYLE_PROFILE_HSBC_OTHERS = 4;

    public static final int UNDERWRITING_LIFESTYLE_EXERCISE_NA = 0;
    public static final int UNDERWRITING_LIFESTYLE_EXERCISE_DAILY = 1;
    public static final int UNDERWRITING_LIFESTYLE_EXERCISE_3_TIMES_WEEKLY = 2;
    public static final int UNDERWRITING_LIFESTYLE_EXERCISE_WEEKEND = 3;

    public static final String PRODUCT_TYPE = "directvalueterm";
    //JIRA HSBCIDCU-917
    public static final String SMS_SIMPLE_CASE="Thank you for your application. Please allow 7 working days to process the first premium payment and complete internal checks.";
    public static final String SMS_COMPLEX_CASE="Thank you for your application. We will be contacting you shortly on the requirements to process your application.";

    // TODO: remove - this is redundant since product codes are not consistent
    // JIRA HSBCIDCU-632 - Use correct product name
    public static final String PRODUCT_NAME = "DIRECT - ValueTerm";

    public static final Set<String> COMPLEX_NATIONALITIES = new HashSet<String>(Arrays.asList(
            "Canada",
            "Japan",
            "Vatican City",
            "United States of America",
            "United Kingdom" //JIRA 457
    ));

    public static final Set<String> COMPLEX_INDUSTRIES = new HashSet<String>(Arrays.asList(
            "Casino & Gambling",
            "Charity/Nonprofit organisation",
            "Defence",
            "Government/State-Owned bodies",
            "Military product/Military production distributors",
            "Money Service Business/Money Changer/Money Remittance",
            "Oil & Gas/Petrolium /Oil Rigs",
            "Others"
    ));

    static final String[] VALID_PASS_TYPES = new String[] {
            "Employment Pass",
            "Skilled Pass",
            "Personalised Employment Pass",
            "Dependent Pass",
            "Student Pass"
    };

    protected DecimalFormat df = new DecimalFormat("#,###");
    protected DecimalFormat df2 = new DecimalFormat("#,##0.00");

    private final static Logger logger = Logger.getLogger(DirectValueTermApplication.class);

    protected String id;
    protected String referenceNum;
    protected String campaignCode;
    protected Boolean agreePrivacy;
    protected Boolean agreeImportantNotes;
    protected String lifeInsured;
    protected int status;
    protected String dropoutPassword;
    protected int notifyStatus;
    protected int mailOption;

    protected String applicantResidency;
    protected String applicantFirstName;
    protected String applicantLastName;
    protected Date applicantDob;
    protected String applicantGender;
    protected String applicantMobileCountryCode;
    protected String applicantMobileNum;
    protected String applicantEmail;
    protected String applicantNationality;
    protected Boolean applicantMultipleNationalities;
    protected String applicantNationality2;
    protected String applicantNationality3;
    protected String applicantBirthCountry;
    protected String applicantNric;
    protected Boolean applicantEnProficient;
    protected String applicantEmployment;
    protected Integer applicantIncome;
    protected Boolean applicantSmoker;
    protected String applicantOccupation;
    protected String applicantIndustry;
    protected String applicantCompanyName;
    protected String applicantCompanyCity;
    protected String applicantCompanyCountry;
    protected Date applicantStudiesEndDate;
    protected String applicantAddressPostalCode;
    protected String applicantAddressHouse;
    protected String applicantAddressUnit;
    protected String applicantAddressStreet;
    protected String applicantAddressBuilding;
    protected Boolean applicantAddressIsRegistered;
    protected Integer applicantAddressPeriodOfStay;
    protected String applicantPreviousAddressCountry;
    protected Boolean applicantAddressIsPermanent;
    protected String applicantPermanentAddressLine1;
    protected String applicantPermanentAddressLine2;
    protected String applicantPermanentAddressPostalCode;
    protected String applicantPermanentAddressCountry;

    protected String applicantTaxCountry1;
    protected String applicantTaxCountry1Tin;
    protected String applicantTaxCountry1NoTinReason;
    protected String applicantTaxCountry1NoTinReasonCustom;
    protected String applicantTaxCountry2;
    protected String applicantTaxCountry2Tin;
    protected String applicantTaxCountry2NoTinReason;
    protected String applicantTaxCountry2NoTinReasonCustom;
    protected String applicantTaxCountry3;
    protected String applicantTaxCountry3Tin;
    protected String applicantTaxCountry3NoTinReason;
    protected String applicantTaxCountry3NoTinReasonCustom;

    protected String applicantAddressNoTaxReason;
    protected Boolean applicantIsHsbcCustomer;
    protected Boolean applicantIsPep;
    protected Boolean applicantIsBeneficalOwner;

    protected String insuredResidency;
    protected String insuredFirstName;
    protected String insuredLastName;
    protected Date insuredDob;
    protected String insuredGender;
    protected String insuredNationality;
    protected Boolean insuredMultipleNationalities;
    protected String insuredNationality2;
    protected String insuredNationality3;
    protected String insuredNric;
    protected String insuredEmployment;
    protected Integer insuredIncome;
    protected Boolean insuredSmoker;
    protected float existingLifeInsuranceCoverage;
    protected Boolean insuredMedical1;
    protected Boolean insuredMedical2;
    protected Boolean insuredMedical3;
    protected Boolean insuredMedical4;
    protected String insuredOccupation;
    protected String insuredIndustry;
    protected String insuredCompanyName;
    protected Date insuredStudiesEndDate;

    protected String beneficialOwnerFirstName;
    protected String beneficialOwnerLastName;
    protected String beneficialOwnerNric;
    protected String beneficialOwnerRelation;

    protected String termDuration;
    protected String productCode;

    protected Float sumAssured;
    protected Float riderSumAssured;
    protected int paymentMode;
    protected Boolean includeCriticalIllnessRider;
    protected Boolean includeRefundRider;
    protected Boolean underwritingLifestyleQuestionsOptin;
    protected Integer underwritingLifestyleMortgage;
    protected Integer underwritingLifestyleProfile;
    protected Integer underwritingLifestyleHeight;
    protected Integer underwritingLifestyleWeight;
    protected Integer underwritingLifestyleExercise;

    protected String creditCardType;
    protected String creditCardName;
    protected String creditCardNum;
    protected String creditCardExpiry;
    protected Date dateCreated;
    protected Date dateUpdated;
    protected float maxStep;

    protected Integer feedbackExperience;
    protected Integer feedbackEase;
    protected Boolean feedbackRecommend;
    protected String feedbackNoRecommendReason;
    protected String feedbackImprove;
    protected String feedbackAdditional;
    protected Boolean marketingConsent=true;
    protected Date emailSentDate;

    private String EMAIL_ADMIN_SUBJECT = "HSBC Insurance Online Singapore DIRECT ValueTerm Application (%s)";
    private String EMAIL_ADMIN_CONTENT = "This is a system generated message. Please do not reply to this email directly\n\n" +
            "Hi, \n\n" +
            "%s has created a new application for DIRECT ValueTerm.";

    private String EMAIL_SUBJECT = "HSBC Insurance Online Singapore (%s)";
   
    public static DirectValueTermApplication getById(String id) {
        DirectValueTermApplication application = new DirectValueTermApplication();
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM applications WHERE id = ?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                application = populate(application, rs);
            } else {
                try (
                        PreparedStatement stmt2 = conn.prepareStatement("INSERT INTO applications (id, product_type, date_created, date_updated, max_step, status, notify_status) VALUES (?, ?, NOW(), NOW(), 0, ?, ?)")
                ) {
                    stmt2.setString(1, id);
                    stmt2.setString(2, PRODUCT_TYPE);
                    stmt2.setInt(3, STATUS_INCOMPLETE);
                    stmt2.setInt(4, NOTIFY_STATUS_NOT_SENT);
                    stmt2.executeUpdate();
                    application = getById(id);
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            ErrorHandler.handleError(application, e, "SQL error retrieving application");
            return null;
        }finally {
            db.closeAll(conn,stmt, rs);
        }

        return application;
    }

    public void generatePDFs() throws IOException {
        generateBIPDF();
        generateAdminBIPDF();
        generateApplicationPDF();
    }

    public void sendNotifications() throws IOException, MessagingException {
        sendNotifications(true);
    }
    public void sendNotifications(boolean isNeedSendToBO) throws IOException, MessagingException {

        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", Config.getString("dreamcatcher.smtp.port"));
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        String DefaultCharSet = MimeUtility.getDefaultJavaCharset();
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        Transport transport = null;

        String toEmail = String.format("%s <%s>", getApplicantFullName(), getApplicantEmail());
        String fromEmail = Config.getString("dreamcatcher.email.from");

        /**
         * CUSTOMER EMAIL NOTIFICATION
         */

        String zipFilename = generateZipFile();
       
        try {
            message.setSubject(String.format(EMAIL_SUBJECT, getReferenceNum()), "UTF-8");
            message.setFrom(new InternetAddress(fromEmail));
            message.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(toEmail));

            MimeMultipart msg_body = new MimeMultipart("alternative");
            MimeBodyPart wrap = new MimeBodyPart();
            MimeBodyPart textPart = new MimeBodyPart();
           // JIRA HSBCIDCU-917 adding underwriting class dyanamically
            String underwritingClassLabel = "";
            switch (getUnderwritingClass()) {
              case UNDERWRITING_CLASS_PREFERRED:
                underwritingClassLabel = "preferred rates";
                  break;
              case UNDERWRITING_CLASS_STANDARD:
                underwritingClassLabel = "standard rates";
                  break;
              case UNDERWRITING_CLASS_PREFERRED_PLUS:
                underwritingClassLabel = "preferred plus";
                  break;
              case UNDERWRITING_CLASS_STANDARD_PLUS:
                underwritingClassLabel = "standard plus";
                  break;
            }
            
            if(getReferenceNumComplexityFlag().equals("C")) {
              String complexCaseEmailContent = "This is a system generated message. Please do not reply to this email directly\n\n" +
                  "Dear %s,\n\n" +
                  "Thank you for your application for a HSBC insurance policy. " +
                  "We will be writing to you shortly on the requirements to process application.\n\n"+
                  "Meanwhile, we enclose a copy of your application form for your reference. If the information given in the application form is not correct or should you have any questions pertaining to your application, please do not hesitate to contact us at (65) 62256111 (Mondays to Fridays, 9am to 5pm) or via email at e-surance@hsbc.com.sg by quoting the application number given in this email.\n\n\n" +
                  "Sincerely,\n" +
                  "HSBC Insurance Online Team";
              textPart.setContent(MimeUtility.encodeText(String.format(complexCaseEmailContent, getApplicantFullName()), DefaultCharSet,"B"), "text/plain; charset=UTF-8");
              
            }
            else {
              String simpleCaseEmailContent = "This is a system generated message. Please do not reply to this email directly\n\n" +
                  "Dear %s,\n\n" +
                  "Thank you for your application for a HSBC insurance policy.\n\n" +
                  "We are pleased to inform you that we have accepted your application at "+underwritingClassLabel+" and insurance coverage will commence immediately upon our receipt of the first premium payment and the completion of internal administrative checks, such as customer due diligence.  We will send you the policy documents upon issuance of the policy within 7 working days.\n\n"+
                  "Meanwhile, we enclose a copy of your application form for your reference. If the information given in the application form is not correct or should you have any questions pertaining to your application, please do not hesitate to contact us at (65) 62256111 (Mondays to Fridays, 9am to 5pm) or via email at e-surance@hsbc.com.sg by quoting the application number given in this email.\n\n\n" +
                  "Sincerely,\n" +
                  "HSBC Insurance Online Team";
              textPart.setContent(MimeUtility.encodeText(String.format(simpleCaseEmailContent, getApplicantFullName()), DefaultCharSet,"B"), "text/plain; charset=UTF-8");
              
            }
            textPart.setHeader("Content-Transfer-Encoding", "base64");
            msg_body.addBodyPart(textPart);
            wrap.setContent(msg_body);
            MimeMultipart msg = new MimeMultipart("mixed");
            message.setContent(msg);
            msg.addBodyPart(wrap);

            MimeBodyPart att = new MimeBodyPart();
            DataSource fds = new FileDataSource(zipFilename);
            att.setDataHandler(new DataHandler(fds));
            att.setFileName(fds.getName());

            msg.addBodyPart(att);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        try {
            transport = session.getTransport(); 
            transport.connect(Config.getString("dreamcatcher.smtp.host"), Config.getString("dreamcatcher.smtp.username"), Config.getString("dreamcatcher.smtp.password"));
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        /**
         * CUSTOMER SMS NOTIFICATION
         */

        try {
            AmazonSNS sns = AmazonSNSClient.builder().withRegion(Regions.AP_SOUTHEAST_1).build();
           //JIRA HSBCIDCU-917 
            String smsMessage=SMS_SIMPLE_CASE; 
            if(getReferenceNumComplexityFlag().equals("C")) { //HSBCIDCU-917 
            smsMessage=SMS_COMPLEX_CASE;
            }
            smsMessage += String.format("Your PDF password is: %s.", getPDFPassword(PDF_TYPE_BENEFIT_ILLUSTRATION));
            String smsPhoneNumber = getApplicantFullMobileNum();
            Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
            smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                    .withStringValue("HSBCInsure") //The sender ID shown on the device.
                    .withDataType("String"));

            PublishResult result = sns.publish(new PublishRequest()
                    .withMessage(smsMessage)
                    .withPhoneNumber(smsPhoneNumber)
                    .withMessageAttributes(smsAttributes));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }


        /**
         * ADMIN EMAIL NOTIFICATION
         */
        if (isNeedSendToBO) {
            zipFilename = generateAdminZipFile();
            toEmail = Config.getString("dreamcatcher.email.admin.applications");
            if (isNationalitySpecialCase()) {
                toEmail += "," + Config.getString("dreamcatcher.email.admin.applications.nationality");
            }
            if (isIndustrySpecialCase()) {
                toEmail += "," + Config.getString("dreamcatcher.email.admin.applications.industry");
            }

            try {
                message = new MimeMessage(session);
                message.setSubject(String.format(EMAIL_ADMIN_SUBJECT, getReferenceNum()), "UTF-8");
                message.setFrom(new InternetAddress(fromEmail));
                message.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(toEmail));

                MimeMultipart msg_body = new MimeMultipart("alternative");
                MimeBodyPart wrap = new MimeBodyPart();
                MimeBodyPart textPart = new MimeBodyPart();
                textPart.setContent(MimeUtility.encodeText(String.format(EMAIL_ADMIN_CONTENT, getApplicantFullName()), DefaultCharSet, "B"), "text/plain; charset=UTF-8");
                textPart.setHeader("Content-Transfer-Encoding", "base64");
                msg_body.addBodyPart(textPart);
                wrap.setContent(msg_body);
                MimeMultipart msg = new MimeMultipart("mixed");
                message.setContent(msg);
                msg.addBodyPart(wrap);

                MimeBodyPart att = new MimeBodyPart();
                DataSource fds = new FileDataSource(zipFilename);
                att.setDataHandler(new DataHandler(fds));
                att.setFileName(fds.getName());

                msg.addBodyPart(att);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            try {
                transport = session.getTransport();
                transport.connect(Config.getString("dreamcatcher.smtp.host"), Config.getString("dreamcatcher.smtp.username"), Config.getString("dreamcatcher.smtp.password"));
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

    }

    public String getPDFPassword(int type) {
        String password = null;
        switch (type) {
            case PDF_TYPE_BENEFIT_ILLUSTRATION:
                String nric = getApplicantNric();
                password = String.format("%s%s", (new SimpleDateFormat("ddMMM")).format(getApplicantDob()).toUpperCase(), nric.substring(nric.length() - 6, nric.length() - 1));
                break;
            case PDF_TYPE_APPLICATION:
                break;
        }

        return password;
    }

    public String getPDFFilename(int type) {
        return getPDFFilename(type, false);
    }

    public String getPDFFilename(int type, boolean forAdmin) {
        String filename = null;
        switch (type) {
            case PDF_TYPE_BENEFIT_ILLUSTRATION:
                if (forAdmin) {
                    filename = String.format("%s/%s-BenefitIllustration-HSBC.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum());
                } else {
                    filename = String.format("%s/%s-BenefitIllustration.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum());
                }
                break;
            case PDF_TYPE_APPLICATION:
                filename = String.format("%s/%s-ApplicationForm.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum());
                break;
        }

        return filename;
    }

    public String generateZipFile() {
        String filename = String.format("%s/%s.zip", System.getProperty("java.io.tmpdir"), getReferenceNum());
        String pdfFilename = String.format("%s/%s.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum());
        PDFMergerUtility pdfMerger = new PDFMergerUtility();

        try {
            pdfMerger.addSource(new File(getPDFFilename(PDF_TYPE_APPLICATION)));
            pdfMerger.addSource(new File(getPDFFilename(PDF_TYPE_BENEFIT_ILLUSTRATION)));
            int termYears = getTermYears();
            //if (termYears != 5 && termYears != 20) termYears = 5;
            //pdfMerger.addSource(new File(String.format("%s/assets/downloads/GPPSDirectValueTerm%d.pdf", ApplicationStartUpListener.rootPath, termYears)));
            pdfMerger.addSource(new File(String.format("%s/assets/downloads/GPPSDirectValueTerm%s.pdf", ApplicationStartUpListener.rootPath, getTermYears() == 5 ? "5" : "20Age65")));
            if (getIncludeCriticalIllnessRider()) {
                pdfMerger.addSource(new File(String.format("%s/assets/downloads/AXPSDirectCI.pdf", ApplicationStartUpListener.rootPath)));
            }
            pdfMerger.setDestinationFileName(pdfFilename);
            pdfMerger.mergeDocuments(MemoryUsageSetting.setupMixed(0, 1000000000000l));
            String password = getPDFPassword(PDF_TYPE_BENEFIT_ILLUSTRATION);
            PDDocument doc = PDDocument.load(new File(pdfMerger.getDestinationFileName()), MemoryUsageSetting.setupTempFileOnly());
            AccessPermission ap = new AccessPermission();
            StandardProtectionPolicy spp = new StandardProtectionPolicy(password, password, ap);
            spp.setEncryptionKeyLength(128);
            spp.setPermissions(ap);
            doc.protect(spp);
            doc.save(pdfFilename);
            doc.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ZipOutputStream zos = new ZipOutputStream(fos);

            ArrayList<String> files = new ArrayList<>(Arrays.asList(
//                    getPDFFilename(PDF_TYPE_BENEFIT_ILLUSTRATION),
//                    getPDFFilename(PDF_TYPE_APPLICATION),
                    pdfFilename,
                    String.format("%s/%s-ApplicantIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantAddress", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-BeneficalOwnerIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantNationalityTwoIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantNationalityThreeIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantPermanentAddress", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantIDVBackNric", System.getProperty("java.io.tmpdir"), getId()), //HSBCICDU-950 back of nric
                    String.format("%s/%s-ApplicantIDV2", System.getProperty("java.io.tmpdir"), getId()),//JIRA 661 relevant pass file addition
                    String.format("%s/%s-ApplicantRelevantPassBack", System.getProperty("java.io.tmpdir"), getId())
                   
//                    String.format("%s/assets/downloads/Basic_Plan_Product_Summary_General_Provisions.pdf", ApplicationStartUpListener.rootPath)
            ));



            for (Iterator<String> iterator = files.iterator(); iterator.hasNext();) {
            	 String f = iterator.next();
             	if(!StringUtils.contains(f, ".pdf")){
                  String fileToCheck=f+".jpg";
                  File file = new File(fileToCheck);
                  if (file != null && file.exists()) {
                  	readFileIntoZip(zos,file);
                  }else{
                  	fileToCheck=f+".jpeg";
                  	file = new File(fileToCheck);
                  	if (file != null && file.exists()) {
                  		readFileIntoZip(zos,file);
                  	}else{
                  		fileToCheck=f+".png";
                      	file = new File(fileToCheck);
                      	if (file != null && file.exists()) {
                      		readFileIntoZip(zos,file);
                      	}else{
                      		fileToCheck=f+".pdf";
                          	file = new File(fileToCheck);
                          	if (file != null && file.exists()) {
                          		readFileIntoZip(zos,file);
                          	}
                      	}
                  	}
                  }
             }else{
             	File file = new File(f);
             	readFileIntoZip(zos,file);
             }
            }
            zos.close();
            fos.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return filename;
    }
    public void readFileIntoZip(ZipOutputStream zos,File file){
    	try{  
    		FileInputStream fis = new FileInputStream(file) ;
            ZipEntry zipEntry = new ZipEntry(file.getName());
            zos.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zos.write(bytes, 0, length);
            }
            zos.closeEntry();
            fis.close();
        }catch(Exception e){
        	
        }
    }
    public String generateAdminZipFile() {
        String filename = String.format("%s/%s-HSBC.zip", System.getProperty("java.io.tmpdir"), getReferenceNum());

        PDFMergerUtility pdfMerger = new PDFMergerUtility();
        try {
            pdfMerger.addSource(new File(getPDFFilename(PDF_TYPE_APPLICATION)));
            pdfMerger.addSource(new File(getPDFFilename(PDF_TYPE_BENEFIT_ILLUSTRATION)));
            pdfMerger.addSource(new File(String.format("%s/assets/downloads/GPPSDirectValueTerm%s.pdf", ApplicationStartUpListener.rootPath, getTermYears() == 5 ? "5" : "20Age65")));
            if (getIncludeCriticalIllnessRider()) {
                pdfMerger.addSource(new File(String.format("%s/assets/downloads/AXPSOnlineCI.pdf", ApplicationStartUpListener.rootPath)));
            }
            pdfMerger.setDestinationFileName(String.format("%s/%s.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum()));
            pdfMerger.mergeDocuments(MemoryUsageSetting.setupMixed(0, 1000000000000l));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        try (FileOutputStream fos = new FileOutputStream(filename);
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            ArrayList<String> files = new ArrayList<>(Arrays.asList(
//                    getPDFFilename(PDF_TYPE_BENEFIT_ILLUSTRATION, true),
//                    getPDFFilename(PDF_TYPE_APPLICATION),

                    String.format("%s/%s.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum()),
                    String.format("%s/%s-ApplicantIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantAddress", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-BeneficalOwnerIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantNationalityTwoIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantNationalityThreeIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantPermanentAddress", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantIDV2", System.getProperty("java.io.tmpdir"), getId()),//JIRA 661 relevant pass file addition
                    String.format("%s/%s-ApplicantIDVBackNric", System.getProperty("java.io.tmpdir"), getId()), //HSBCICDU-950 back of nric
                    String.format("%s/%s-ApplicantRelevantPassBack", System.getProperty("java.io.tmpdir"), getId())
            ));

            for (Iterator<String> iterator = files.iterator(); iterator.hasNext(); ) {
            	 String f = iterator.next();
             	if(!StringUtils.contains(f, ".pdf")){
                  String fileToCheck=f+".jpg";
                  File file = new File(fileToCheck);
                  if (file != null && file.exists()) {
                  	readFileIntoZip(zos,file);
                  }else{
                  	fileToCheck=f+".jpeg";
                  	file = new File(fileToCheck);
                  	if (file != null && file.exists()) {
                  		readFileIntoZip(zos,file);
                  	}else{
                  		fileToCheck=f+".png";
                      	file = new File(fileToCheck);
                      	if (file != null && file.exists()) {
                      		readFileIntoZip(zos,file);
                      	}else{
                      		fileToCheck=f+".pdf";
                          	file = new File(fileToCheck);
                          	if (file != null && file.exists()) {
                          		readFileIntoZip(zos,file);
                          	}
                      	}
                  	}
                  }
             }else{
             	File file = new File(f);
             	readFileIntoZip(zos,file);
             }
            }
            zos.close();
            fos.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return filename;
    }

    public void generateApplicationPDF() {
        generatePDF(PDF_TYPE_APPLICATION, false);
    }

    public void generateBIPDF() {
        generatePDF(PDF_TYPE_BENEFIT_ILLUSTRATION, false);
    }

    public void generateAdminBIPDF() {
        generatePDF(PDF_TYPE_BENEFIT_ILLUSTRATION, true);
    }

    public void generatePDF(int type, boolean makePublic) {
        PDFGenerator pd = null;
        try {
            switch (type) {
                case PDF_TYPE_BENEFIT_ILLUSTRATION:
                    if (makePublic) {
                        pd = new PDFGenerator(getPDFFilename(type, true), PDFGenerator.PDF_TYPE_BENEFIT_ILLUSTRATION);
                    } else {
                        pd = new PDFGenerator(getPDFFilename(type), PDFGenerator.PDF_TYPE_BENEFIT_ILLUSTRATION);
                    }
                    //fixme doesnt seem to be required in the docs
//                generateBIPDFPage1(pd);
                    generateBIPDFPage2(pd);
                    generateBIPDFPage3(pd);
                    if (getIncludeCriticalIllnessRider()
                            && ((DVT_5_YEARS_PRODUCT_CODE.equals(getProductCode()) && getApplicantAge()<=60)
                            ||  (DVT_AGE_65_PRODUCT_CODE.equals(getProductCode()) && getApplicantAge()<=60)
                            ||  (DVT_20_YEARS_PRODUCT_CODE.equals(getProductCode()) && getApplicantAge()<=45))) {
                        generateBIPDFPage4(pd);
                    }
                    PDPage page;
                    for (int i = 0; i < pd.getDocument().getNumberOfPages(); i++) {
                        page = pd.getDocument().getPage(i);
                        pd.applyPageHeader(page);
                        pd.applyPageFooter(page, i+1);
                    }
                    break;
                case PDF_TYPE_APPLICATION:
                    pd = new PDFGenerator(getPDFFilename(type), PDFGenerator.PDF_TYPE_APPLICATION);
                    generateApplicationPDFPages(pd);
                    break;
            }

            if (pd != null) {
                try {
                    String password = getPDFPassword(type);
                    if (!makePublic && password != null && !password.isEmpty()) {
//                    pd.protect(password);
                    }

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());

        } finally {
            if (pd != null) {
                try {
                    pd.save();
                    pd.close();
                } catch (IOException ioe) {
                    logger.error(ioe.getMessage());
                }
            }
        }
    }

    /**
     * Returns Age at end of policy term
     * @return int age
     */
    public int getAgeAtTermEnd(){
        return getApplicantAge() + getTermYears();

    }

    private void generateApplicationPDFPages(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);
        String[] arrayDeclaration = {};
        String joinedDeclarations="";
        PDFont font = PDType1Font.TIMES_ITALIC;
        PDFont boldFontArial=PDType1Font.HELVETICA_BOLD;
        try {

            pd.applyPageHeader(page, pd.getDocument());
            //Pdf header
            float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
            

            PDFLayout.myFont=12;
            float yStartHead=PDFLayout.mainTitleListRight(
                    new String[]{
                            "<b>Proposal for DIRECT - ValueTerm</b>", // JIRA HSBCIDCU-632 - Use correct product name

                    }, page.getMediaBox().getHeight() - PDFGenerator.PAGE_MARGIN_TOP, page.getMediaBox().getHeight() - PDFGenerator.PAGE_MARGIN_TOP, tableWidth, page, pd, 15);
            PDFLayout.myFont=0;
            pd.setFont(PDFGenerator.FONT_NORMAL, 9);
            pd.setBuiltInFont(null, 9);
            yStartHead = PDFLayout.mainTitleListLeft(

                    new String[]{
                            "<b>HSBC Insurance (Singapore) Pte. Limited </b>(Reg. No. 195400150N)",
                            "21 Collyer Quay #02-01 Singapore 049320, Monday to Friday 9.30am to 5pm. insuranceonline.hsbc.com.sg",
                            "Customer Care Hotline: (65) 6225 6111 Fax: (65) 6221 2188",
                            "Mailing address: Robinson Road Post Office P.O. BOX 1538 Singapore 903038"
                    }, yStartHead, yStartHead, tableWidth-20, page, pd, 0);
            try{
            	
                pd.setRedColor(255);
                pd.drawRectangle(page,PDFGenerator.PAGE_MARGIN_LEFT ,yStartHead-250, 230, tableWidth);


//JIRA HSBCICDU-632 Update important notes
                String guidelines = "VERY IMPORTANT NOTES\n\nDUE TO US INSURANCE REGULATORY REQUIREMENTS, YOU ARE NOT TO ENTER THE US OR ANY TERRITORY \nSUBJECT TO US JURISDICTION DURING THE APPLICATION PROCESS, OTHERWISE THE REQUEST EFFECTED \nHEREUNDER MAY BE MADE VOID.\n\n"+
                        "THIS IS A SELF-DIRECTED ONLINE PLATFORM TO PURCHASE INSURANCE PRODUCTS. "
                        + "HSBC INSURANCE\n(SINGAPORE) PTE. LIMITED (\"HSBC INSURANCE\") DOES NOT PROVIDE ANY ADVICE TO CLIENTS TRANSACTING ON\nTHIS PLATFORM.\n\n" +
                        "YOU AGREE THAT THE INFORMATION PROVIDED IN THIS ONLINE APPLICATION FORM, SUCH OTHER FORMS, \nDOCUMENTS AND QUESTIONNAIRES PROVIDED BY YOU IN RELATION TO THIS APPLICATION WILL FORM THE BASIS \nOF THE CONTRACT OF INSURANCE BETWEEN YOU AND HSBC INSURANCE.\n\n"+
                        "PLEASE NOTE, WE ARE REQUIRED UNDER THE INSURANCE ACT TO INFORM YOU THAT YOU MUST FULLY AND \nFAITHFULLY DISCLOSE ALL THE FACTS WHICH YOU KNOW OR OUGHT TO KNOW (INCLUDING ANY EXISTING \nMEDICAL CONDITIONS). OTHERWISE, IN ACCORDANCE WITH SECTION 25(5) OF THE INSURANCE ACT (CAP. 142), \nYOUR POLICY MAY BE CANCELLED AND YOU MAY RECEIVE NO BENEFITS.";

                pd.setBuiltInFont(PDType1Font.HELVETICA_BOLD, 8);

                page = pd.writeText(page, guidelines, PDFGenerator.PAGE_MARGIN_LEFT+10,page.getMediaBox().getHeight()-3*PDFGenerator.PAGE_MARGIN_TOP+5);

                pd.setRedColor(0);
                String note = "The products and its related features offered on this online platform are available only to customers who are currently \nresiding in Singapore";


                pd.setBuiltInFont(PDType1Font.HELVETICA,9);
                page = pd.writeText(page, note, 60,page.getMediaBox().getHeight()-7*PDFGenerator.PAGE_MARGIN_TOP+5);
                pd.setBuiltInFont(null, 9);
                pd.setFont(PDFGenerator.FONT_NORMAL, 9);
            }catch(Exception e){

                e.printStackTrace();
            }
        float yStartNewPage = page.getMediaBox().getHeight() - (7*PDFGenerator.PAGE_MARGIN_TOP+40);
        float yStart = yStartNewPage;

      
      
        List<List> dataDec = new ArrayList();

            try{
                File fileDeclaration1=null;
                File fileDeclaration2=null;

                File fileDeclaration0=new File(String.format("%sWEB-INF/ourplans/directvalueterm/step1.jsp", ApplicationStartUpListener.rootPath));
                File fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/directvalueterm/includes/ack2_plan.jsp", ApplicationStartUpListener.rootPath));
                if(getIncludeCriticalIllnessRider()){
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/directvalueterm/includes/declaration_plan_ci.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration2=new File(String.format("%s/WEB-INF/ourplans/directvalueterm/includes/ack1_plan_ci.jsp", ApplicationStartUpListener.rootPath));

                }else{
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/directvalueterm/includes/declaration_plan.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration2=new File(String.format("%s/WEB-INF/ourplans/directvalueterm/includes/ack1_plan.jsp", ApplicationStartUpListener.rootPath));

                }
                dataDec.add(new ArrayList<>(Arrays.asList("")));
                dataDec.add(new ArrayList<>(Arrays.asList("")));
                dataDec=parsingHtmlToText(fileDeclaration0,dataDec);
                dataDec= parsingHtmlToText(fileDeclaration1,dataDec);
                //JIRA HSBCIDCU-632 fix missing i accept
                String iAccept = "<img-check><b>I " + getApplicantFullName() + " agree with the above statements.</b>";
                dataDec.add(new ArrayList<>(Arrays.asList(iAccept)));
                dataDec= parsingHtmlToText(fileDeclaration2,dataDec);
                
                dataDec=parsingHtmlToText(fileDeclaration3,dataDec);
                dataDec.add(new ArrayList<>(Arrays.asList("<img-check><b>By submitting this application, I " + getApplicantFullName() + " acknowledge that I have read, understood and agree with the statements above.</b>")));
                dataDec.add(new ArrayList<>(Arrays.asList("<img-check><b>I "+ getApplicantFullName() + " agree with the statement below")));
                dataDec.add(new ArrayList<>(Arrays.asList("","If I am a customer of, or become a customer of, HSBC Bank (Singapore) Limited (\"HSBC Bank\"), I agree that HSBC Insurance may disclose my personal data provided in this form to HSBC Bank, to enable HSBC Bank to provide me with a consolidated view of all the products I may hold with HSBC Bank, HSBC Insurance and (where relevant) any other member of the HSBC Group.")));
                dataDec.add(new ArrayList<>(Arrays.asList("")));
                String checkbox="<img-uncheck>";
                
                if(getMarketingConsent()){
                	checkbox="<img-check>";
                }
                dataDec.add(new ArrayList<>(Arrays.asList(checkbox+"<b>I "+ getApplicantFullName() + " agree with the statement below")));
                dataDec.add(new ArrayList<>(Arrays.asList("","I consent to the use and disclosure of my personal data provided in this form to the HSBC Group and its respective agents, authorised service parties and relevant third parties for the purpose of sending me news and alerts on new products and special promotions offered by HSBC Insurance and its group companies.")));
                dataDec.add(new ArrayList<>(Arrays.asList("")));
            }
            catch(Exception e){
            	logger.error("error in parsing jsp"+e);
            }

            yStart = PDFLayout.table("Additional Details", Arrays.asList(
                    Arrays.asList("", ""),
                    Arrays.asList(String.format("Application Date:<br>%s", (new Date())), String.format("Reference Number:<br>%s", StringUtils.isBlank(getReferenceNum())?"NA":getReferenceNum())),
                    Arrays.asList(String.format("Agent Code:<br>%s", "14000"), String.format("Campaign Code:<br>%s", StringUtils.isBlank(getCampaignCode())?"NA":getCampaignCode()))
            ), false, yStart, yStartNewPage, tableWidth, page, pd, 20);

            // My Details
            ArrayList<List> myDetailsData = new ArrayList<>(Arrays.asList(
                    Arrays.asList("", ""),
                    Arrays.asList(String.format("Last Name/Surname:<br>%s", StringUtils.isBlank(getApplicantLastName())?"NA":getApplicantLastName()), String.format("First/Given Name:<br>%s", StringUtils.isBlank(getApplicantFirstName())?"NA":getApplicantFirstName())),

                    Arrays.asList(String.format("Date of Birth:<br>%s", getApplicantDob()==null?"NA":new SimpleDateFormat("dd-MM-yyyy").format(getApplicantDob())), String.format("Gender:<br>%s", StringUtils.isBlank(getGenderDescription())?"NA":getGenderDescription())),
                    Arrays.asList(String.format("Country of Birth:<br>%s", StringUtils.isBlank(getApplicantBirthCountry())?"NA":getApplicantBirthCountry()), String.format(((applicantIsSingaporean()) || (applicantIsPermanentResident()) ? "NRIC No.:<br>%s" : "Passport No.:<br>%s"), StringUtils.isBlank(getApplicantNric())?"NA":getApplicantNric()))

            ));
            String nationalities = StringUtils.isBlank(getApplicantNationality())?"NA":getApplicantNationality();
            if (!getApplicantNationality2().isEmpty())
                nationalities += "<br>" + getApplicantNationality2();
            if (!getApplicantNationality3().isEmpty())
                nationalities += "<br>" + getApplicantNationality3();

           myDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Nationalities: (please list all)<br>%s", nationalities), String.format("Residency Status:<br>%s", StringUtils.isBlank(getApplicantResidency())?"NA":getApplicantResidency()))));
           myDetailsData.add(new ArrayList<>(Arrays.asList(
                    "Are you proficient in the English Language (spoken and written) and have you completed at least a secondary school education or its equivalent?<br>Yes"
                    )));
           myDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Are you an HSBC customer?<br>%s", (getApplicantIsHsbcCustomer()==null?"No":getApplicantIsHsbcCustomer() ? "Yes" : "No")))));
           yStartNewPage = page.getMediaBox().getHeight() - (PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP / 2);
           BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
           PlanDataTable t = new PlanDataTable(dataTable, page);
           t.addListToTable(myDetailsData, PlanDataTable.NOHEADER,  "My Details",HorizontalAlignment.LEFT);
           yStart = dataTable.draw() - 20;
          


            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);

            pd.setCurrentPage(page);
            try{

            // My Contact Details and Address
            List<List> contactDetailsData = new ArrayList();
            contactDetailsData.add(new ArrayList<>(Arrays.asList("")));
            StringBuffer address=new StringBuffer();
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressHouse()), "null")){
            	address.append(getApplicantAddressHouse()+" ");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressStreet()), "null")){
            	address.append(getApplicantAddressStreet()+" ");
            }
            if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressUnit()), "null")||StringUtils.containsIgnoreCase(StringUtils.trim(getApplicantAddressUnit()), "null"))){
            	address.append("<br>"+getApplicantAddressUnit()+" ");
            }else{
            	address.append("<br>");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressBuilding()), "null")){
            	address.append(getApplicantAddressBuilding()+" ");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressPostalCode()), "null")){
            	address.append("<br>Postal Code: "+getApplicantAddressPostalCode()+" ");
            }
            
           
            contactDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Mobile No.:<br>%s", StringUtils.isBlank(getApplicantFullMobileNum())?"NA":getApplicantFullMobileNum()), String.format("Residential/Mailing Address:<br>%s", StringUtils.isBlank(address.toString())?"NA":address))));
          try {
        	 if(applicantIsSingaporean() || applicantIsPermanentResident()){
        	  if(getApplicantAddressPeriodOfStay() % 12==1) {  
            contactDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Is the above residential address the same as stated on your NRIC?:<br>%s", (getApplicantAddressIsRegistered()==null?"No":getApplicantAddressIsRegistered() ? "Yes" : "No")), String.format("How long have you been living at the residential address that you provided above?:<br>%s years %s month", Math.round((getApplicantAddressPeriodOfStay() / 12)), ((getApplicantAddressPeriodOfStay() % 12))))));
        	  }else{
        		  contactDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Is the above residential address the same as stated on your NRIC?:<br>%s", (getApplicantAddressIsRegistered()==null?"No":getApplicantAddressIsRegistered() ? "Yes" : "No")), String.format("How long have you been living at the residential address that you provided above?:<br>%s years %s months", Math.round((getApplicantAddressPeriodOfStay() / 12)), ((getApplicantAddressPeriodOfStay() % 12))))));  
        	  }
        	}else{
        		if(getApplicantAddressPeriodOfStay() % 12==1) { 
        		  contactDetailsData.add(new ArrayList<>(Arrays.asList(String.format("How long have you been living at the residential address that you provided above?:<br>%s years %s month", Math.round((getApplicantAddressPeriodOfStay() / 12)), ((getApplicantAddressPeriodOfStay() % 12))))));
        		}else{
        			contactDetailsData.add(new ArrayList<>(Arrays.asList(String.format("How long have you been living at the residential address that you provided above?:<br>%s years %s months", Math.round((getApplicantAddressPeriodOfStay() / 12)), ((getApplicantAddressPeriodOfStay() % 12))))));
        		}
        	}
          }catch(Exception e){
        	 
          }
            String permanentAddress = "";
            if (!getApplicantAddressIsPermanent()) {
                permanentAddress += (getApplicantPermanentAddressLine1() != null ? getApplicantPermanentAddressLine1()+" " : "");
                permanentAddress += (getApplicantPermanentAddressLine2() != null ? "<br>" + getApplicantPermanentAddressLine2()+" " : "");
                permanentAddress+=(getApplicantPermanentAddressPostalCode()!=null?"<br>Postal Code: "+getApplicantPermanentAddressPostalCode()+" ":"");
                permanentAddress+=(getApplicantPermanentAddressCountry()!=null?"<br>Country: "+getApplicantPermanentAddressCountry():"");
            }
            //JIRA HSBCIDCU-632 Show NA in permanent address for Singaporean
           
            if(applicantIsPermanentResident()){
            	contactDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Is the above residential address also your Permanent Address?:<br>%s", (getApplicantAddressIsPermanent()==null?"No":getApplicantAddressIsPermanent() ? "Yes" : "No")), String.format("Permanent Address:<br>%s", StringUtils.isBlank(permanentAddress)?"NA":permanentAddress))));
            }else if(applicantIsPassHolder()){
            	contactDetailsData.add(new ArrayList<>(Arrays.asList(  String.format("Permanent Address:<br>%s", StringUtils.isBlank(permanentAddress)?"NA":permanentAddress))));
            }
         
            if(StringUtils.isNotBlank(getApplicantPreviousAddressCountry())){
            	contactDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Previous Address:<br>%s", (StringUtils.isNotBlank(getApplicantPreviousAddressCountry())? getApplicantPreviousAddressCountry() : "NA")), String.format("Email Address:<br>%s", StringUtils.isBlank(getApplicantEmail())?"NA":getApplicantEmail()))));
               }else{
            	   contactDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Email Address:<br>%s", StringUtils.isBlank(getApplicantEmail())?"NA":getApplicantEmail()))));
               }
            
              yStart = PDFLayout.table("My Contact Details and Address", contactDetailsData, false, yStart, yStartNewPage, tableWidth, page, pd, 20);
            }catch(Exception e){
            	
            }
           page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);


                  

            pd.setCurrentPage(page);
            // My Occupation

          try{
            ArrayList<List> myOccupationData = new ArrayList<>(Arrays.asList(
                    Arrays.asList("", ""),
                    Arrays.asList(String.format("Employment Status:<br>%s", StringUtils.isBlank(getApplicantEmployment())?"NA":getApplicantEmployment()), String.format("Industry:<br>%s", StringUtils.isBlank(getApplicantIndustry())?"NA":getApplicantIndustry())),
                    Arrays.asList(String.format("Occupation:<br>%s", StringUtils.isBlank(getApplicantOccupation())?"NA":getApplicantOccupation()), String.format("Current Annual Income:<br>S$%s", df.format(getApplicantIncome()))),
                    Arrays.asList(String.format("Employer's Name:<br>%s", StringUtils.isBlank(getApplicantCompanyName())?"NA":getApplicantCompanyName()), (StringUtils.isNotBlank(getApplicantCompanyCity()) && StringUtils.isNotBlank(getApplicantCompanyCountry())) ? String.format("City / Country:<br>%s / %s", getApplicantCompanyCity(), getApplicantCompanyCountry()) : "City / Country:<br>NA")
                     
                    
            ));
         
            if (applicantIsStudying()) {

                myOccupationData.add(new ArrayList<>(Arrays.asList(String.format("Student Course End Date:<br>%s", getApplicantStudiesEndDate()==null?"NA":new SimpleDateFormat("MM-yyyy").format(getApplicantStudiesEndDate())), "")));

            }
            yStart = PDFLayout.table("My Occupation", myOccupationData, false, yStart, yStartNewPage, tableWidth, page, pd, 20);
           
            }catch(Exception e){
        	  
          } 

            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
            // My Insurance Details

           try{
            List<List> insuranceDetailsData = new ArrayList();
            insuranceDetailsData.add(new ArrayList<>(Arrays.asList("", "")));
            //JIRA HSBCIDCU-632: The premium should be different for payment frequency (annually,semi-annually,quarterly,monthly)
            String premium = df2.format(((double) ProductCalculation.getPremiumAtAge(calculateAgeNextBirthday(getApplicantDob()), this).get("modalPremium")));
            String riders = "";
            int termDuration=getTermYears();
            String riderMonthlyPremium="";
            if (getIncludeCriticalIllnessRider()) {
                riders += String.format("<br>Online Critical Illness Rider: S$%s", df.format(getRiderSumAssured()));
                riderMonthlyPremium="<br>S$"+df2.format((double) ProductCalculation.getCriticalIllnessRiderPremiumAtAge(getApplicantAge(), this).get("modalPremium"));
            }
            logger.info("the existing coverage amount is "+getExistingLifeInsuranceCoverage());
            //Change existing coverage amount to two decimal point
            insuranceDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Plan Name:<br>%s", StringUtils.isBlank(PRODUCT_NAME)?"NA":PRODUCT_NAME),String.format("Premium Amount:<br>S$%s", StringUtils.isBlank(premium)?"NA":premium ))));
            
            insuranceDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Coverage:<br>S$%s", df.format(getSumAssured())),String.format("Existing Coverage Amount:<br>S$%s", df2.format(getExistingLifeInsuranceCoverage())))));
            insuranceDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Riders: (if applicable) %s", StringUtils.isBlank(riders)?"NA":riders),String.format("Rider Premium Amount:%s", StringUtils.isBlank(riderMonthlyPremium)?"NA":riderMonthlyPremium))));
            insuranceDetailsData.add(new ArrayList<>(Arrays.asList(String.format("Payment Frequency:<br>%s", StringUtils.isBlank(getPaymentModeLabelPdf())?"NA":getPaymentModeLabelPdf()),String.format("Policy Term:<br>%s", termDuration+" years"))));
            //JIRA HSBCIDCU-632 Rename table to insurance details
            yStart = PDFLayout.table("Insurance Details", insuranceDetailsData, false, yStart, yStartNewPage, tableWidth, page, pd, 20);
           }catch(Exception e){
        	   
           }

            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);

            // Underwriting Questions
            try{

            List<List> uwQuestionsData = new ArrayList();
            uwQuestionsData.add(new ArrayList<>(Arrays.asList("")));
            String medical = "";
            uwQuestionsData.add(new ArrayList<>(Arrays.asList(String.format("Have you smoked in the last 12 months?:<br>%s", (getApplicantSmoker() ? "Yes" : "No")))));
            medical += "1. Have you ever been diagnosed with cancer, heart disease or stroke?<br>";
            medical += "%s<br><br>";
            medical += "2. In the last 5 years, have you been diagnosed with or suffered from diabetes , HIV/AIDS or any medical condition affecting your brain, blood, heart, lungs, liver, kidneys for which you were required to undergo medical treatment for more than 14 days?<br>";
            medical += "%s<br><br>";
            medical += "3. In the last 12 months, have you had any symptom such as unexplained bleeding, weight loss, lump or growth, blood in the stool, chest pain or weakness of limbs for which you are still under investigation or have not yet sought medical advice?<br>";
            medical += "%s<br><br>";
            
           
            if(getIncludeCriticalIllnessRider()){
            	medical += "4. Have any of your parents or siblings ever suffered from cancer, heart disease, stroke or any hereditary condition before age 60?<br>";
                medical += "%s";
            }
            medical = String.format(medical,
                    ((getInsuredMedical1()==null)?"No":(getInsuredMedical1() ? "Yes" : "No")),
                    ((getInsuredMedical2()==null)?"No": (getInsuredMedical2()? "Yes" : "No")),
                    ((getInsuredMedical3()==null)?"No": (getInsuredMedical3()? "Yes" : "No")),
                    ((getInsuredMedical4()==null)?"No": (getInsuredMedical4()? "Yes" : "No")));
            uwQuestionsData.add(new ArrayList<>(Arrays.asList(medical)));
            //JIRA HSBCIDCU-632 change title to Life Insured's Health Detail
            yStart = PDFLayout.table("Life Insured's Health Details", uwQuestionsData, false, yStart, yStartNewPage, tableWidth, page, pd, 20);

            }catch(Exception e){
                logger.error(e.getMessage());
            }
           
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);

            pd.setCurrentPage(page);

            // Country of Residence for Tax Purposes


            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
      
            // My Payment Details
            //Temp data


            try {
                List<List> data = new ArrayList();
                data.add(new ArrayList<>(Arrays.asList("Country of Tax Residence", "TIN", "If no TIN available Reasons as stated")));
                if (getApplicantTaxCountry1() != null) {
                    String reason = "NA";
                    if (getApplicantTaxCountry1NoTinReason() != null) {
                        reason = getApplicantTaxCountryNoTinReasonLabel(getApplicantTaxCountry1NoTinReason());
                        if (getApplicantTaxCountry1NoTinReasonCustom() != null) {
                            reason += "<br>" + getApplicantTaxCountry1NoTinReasonCustom();
                        }
                    }
                    data.add(new ArrayList<>(Arrays.asList(getApplicantTaxCountry1(), StringUtils.isBlank(getApplicantTaxCountry1Tin())?"NA":getApplicantTaxCountry1Tin(), reason)));
                }
                if (getApplicantTaxCountry2() != null) {
                    String reason = "NA";
                    if (getApplicantTaxCountry2NoTinReason() != null) {
                        reason = getApplicantTaxCountryNoTinReasonLabel(getApplicantTaxCountry2NoTinReason());
                        if (getApplicantTaxCountry2NoTinReasonCustom() != null) {
                            reason += "<br>" + getApplicantTaxCountry2NoTinReasonCustom();
                        }
                    }
                    data.add(new ArrayList<>(Arrays.asList(getApplicantTaxCountry2(), StringUtils.isBlank(getApplicantTaxCountry2Tin())?"NA":getApplicantTaxCountry2Tin(), reason)));
                }
                if (getApplicantTaxCountry3() != null) {
                    String reason = "NA";
                    if (getApplicantTaxCountry3NoTinReason() != null) {
                        reason = getApplicantTaxCountryNoTinReasonLabel(getApplicantTaxCountry3NoTinReason());
                        if (getApplicantTaxCountry3NoTinReasonCustom() != null) {
                            reason += "<br>" + getApplicantTaxCountry3NoTinReasonCustom();
                        }
                    }
                    data.add(new ArrayList<>(Arrays.asList(getApplicantTaxCountry3(), StringUtils.isBlank(getApplicantTaxCountry3Tin())?"NA":getApplicantTaxCountry3Tin(), reason)));
                }


                BaseTable dataTable1 = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
                PlanDataTable t1 = new PlanDataTable(dataTable1, page);
                t1.addListToTable(data, PlanDataTable.HASHEADER, "Country of Residence for Tax Purposes and related Taxpayer Identification Number or equivalent number (\"TIN\")",HorizontalAlignment.LEFT);
                yStart = dataTable1.draw() - 20;
            } catch (Exception e) {

            }
            if (getApplicantAddressNoTaxReason() != null) {
                yStart += 21;
                try {
                    yStart = PDFLayout.table("", Arrays.asList(

                            Arrays.asList(String.format("Reason if Residential/Mailing Address does not match Country of Tax Residence:<br>%s" , StringUtils.isBlank(getApplicantAddressNoTaxReason())?"NA":getApplicantAddressNoTaxReason()))

                    ), false, yStart, yStartNewPage, tableWidth, page, pd, 20);
                } catch (Exception e) {
                    logger.error(e.getStackTrace().toString());
                }
            }
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
            //temp data
            try{
                yStart = PDFLayout.table("My Payment Details", Arrays.asList(
                        Arrays.asList(""),
                        Arrays.asList(String.format("Name on Card:<br>%s", StringUtils.isBlank(getCreditCardName())?"NA":getCreditCardName()), String.format("Credit Card Number:<br>%s", StringUtils.isBlank(getCreditCardNum())?"NA":getCreditCardNum())),
                        Arrays.asList(String.format(String.format("Expiry Date:<br>%s", StringUtils.isBlank(getCreditCardExpiry())?"NA":getCreditCardExpiry())), String.format("Payment Type:<br>%s", StringUtils.isBlank(getCreditCardTypeLabel())?"NA":getCreditCardTypeLabel())))
                        , false, yStart, yStartNewPage, tableWidth, page, pd, 20);

                // Prominent Public Position* Declaration
                yStart = PDFLayout.table("Prominent Public Position* Declaration", Arrays.asList(
                        Arrays.asList(""),
                        Arrays.asList(String.format("Have you or any person in connection with this policy* ever been a Politically Exposed Person (PEP), a family member of a PEP or close associate of a PEP?<br>%s", (getApplicantIsPep()==null?"No":getApplicantIsPep() ? "Yes" : "No")))
                ), false, yStart, yStartNewPage, tableWidth, page, pd, 10);

                //    String policy="*This includes policy owner, life insured, beneficial owner(s) and beneficiary(ies). A PEP is an individual\n who is or has been entrusted with prominent public positions in Singapore, a foreign country or an\n international organisation, which includes a current or former senior official in the executive\n, legislative, administrative, military or judicial branches of a government or a government agency, a\n member of a ruling royal family, a senior official of a political party, a senior executive of a government-\nowned or government-funded corporation, institution or charity and a senior executive of an\n international organisation.";
            }catch(Exception e){

            }
            pd.setFont(PDFGenerator.FONT_NORMAL, 8);
            yStart = PDFLayout.mainTitleListLeft(new String[]{
                    "*This includes policy owner, life insured, beneficial owner(s) and beneficiary(ies). A PEP is an individual who is or has been entrusted with prominent public positions in Singapore, a foreign country or an international organisation, which includes a current or former senior official in the executive, legislative, administrative, military or judicial branches of a government or a government agency, a member of a ruling royal family, a senior official of a political party, a senior executive of a government-owned or government-funded corporation, institution or charity and a senior executive of an international organisation.",
            }, yStart, yStartNewPage, tableWidth, page, pd, 20);
            pd.setFont(PDFGenerator.FONT_NORMAL, 9);
            // Declaration of Beneficial Ownership
            List<List> boDeclarationData = new ArrayList();
            boDeclarationData.add(new ArrayList<>(Arrays.asList("", "")));
            String boDetails = "";
            if (!getApplicantIsBeneficalOwner()) {
                boDetails += String.format("Surname: %s<br>", getBeneficialOwnerLastName());
                boDetails += String.format("Given Name: %s<br>", getBeneficialOwnerFirstName());
                boDetails += String.format("NRIC/Passport Number: %s<br>", getBeneficialOwnerNric());
                boDetails += String.format("Relationship to Benefical Owner: %s", getBeneficialOwnerRelation());
            }

            boDeclarationData.add(new ArrayList<>(Arrays.asList(String.format("Are you the Beneficial Owner* of the Policy?:<br>%s", (getApplicantIsBeneficalOwner() ? "Yes" : "No")), String.format("If you are not the Beneficial Owner we are required by regulation to ask for the Name, nature of relationship and Identity documents of the Beneficial Owner.<br><br>%s", StringUtils.isBlank(boDetails)?"NA":boDetails))));
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
            yStart = PDFLayout.table("Declaration of Beneficial Ownership", boDeclarationData, false, yStart, yStartNewPage, tableWidth, page, pd, 10);
            String beni= "*You are the Beneficial Owner if you are the individual who either controls the applicant or on whose behalf the policy was purchased. However it does not constitute nominating a Beneficiary who receives the policy proceeds";

            pd.setFont(PDFGenerator.FONT_NORMAL, 8);
            yStart = PDFLayout.mainTitleListLeft(new String[]{
                    beni
            }, yStart, yStartNewPage, tableWidth, page, pd, 20);
            pd.setFont(PDFGenerator.FONT_NORMAL, 9);
            page = pd.createPage();
            pd.addPage(page);
            yStart=yStartNewPage;

            PDFLayout.border=5;
            /*added the declarations in the list of list to use data table to display the declarations*/
            BaseTable dataTable1 = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t1 = new PlanDataTable(dataTable1, page);
            t1.addListToTable(dataDec, PlanDataTable.NOHEADER,"Declarations and Acknowledgements",HorizontalAlignment.LEFT);
            yStart = dataTable1.draw() - 20;
            PDFLayout.border=0;
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);


            // Acknowledgements footer
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
            yStart = PDFLayout.table("", Arrays.asList(
                    Arrays.asList(String.format("Acknowledgement Date: %s", (new Date())))
            ), false, yStart, yStartNewPage, tableWidth, page, pd, 20);

            for (int jj = 0; jj < pd.getDocument().getNumberOfPages(); jj++) {
                page = pd.getDocument().getPage(jj);

                //  PDFLayout.mainTitleList(new String[]{"Page " + jj + " of " + (pd.getDocument().getNumberOfPages())}, 20, 20, tableWidth, page, pd, 3);
                pd.applyPageFooter(page, jj + 1);
                pd.setRotationFactor(90);
                pd.setBuiltInFont(font, 7);
                pd.writeText(page, "SG HSBCONLINE JAN 19 2018",page.getMediaBox().getWidth()-140 ,20);
                pd.setBuiltInFont(null, 7);
                pd.setRotationFactor(0);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());

        }
    }
    /*Logic to parse the file with declarations
     * into array which is used to be written
     * on the pdf 
     * 
     * filedeclaration0 The file to be parsed from html to text
     * dataDec          The list of list used by the datatable to populate 
     *                  on the pdf
     *  Return          The declarations to be displayed in the pdf
     *  List<List>
     * 
     * */
    private List<List> parsingHtmlToText(File fileDeclaration1, List<List> dataDec) {

        final String JAPAN_NATIONAL = "I am agreeable to complete the Declaration of Non-Japanese Residency in the Supplementary Proposal Form as part of my application for this policy";
        final String BOLD1 = "I understand the nature, objective and benefits of the Product and have read and acknowledged the information";
        final String BOLD2 = "I have initiated the purchase of this product and the entire application is made while I am a resident in Singapore";
        final String BOLD3 = "I understand that I have a 30-day free-look period to cancel a policy after HSBC Insurance confirms acceptance of my";
        final String BOLD4 = "I understand the nature, objective and benefits of the Product and the CI Rider and have read and acknowledged following information";
        final String IGNORE1 = "in the DIRECT-ValueTerm Fact Sheet";
        final String IGNORE2 = "Your Guide to Health Insurance (if applicable)";
        final String IGNORE3 = "in the Product Summary and Annexure related to DIRECT-Critical Illness Rider";
        final String IGNORE4 = "If I am a customer of, or become a customer of, HSBC Bank (Singapore) Limited (\"HSBC Bank\"), I agree that";
        final String IGNORE5 = "I consent to the use and disclosure of my personal data provided in this form to the HSBC Group and its respective agents";
        final String RED1 = "the information given by me to HSBC Insurance in this application form or in any form or document relating to this";
        final String RED2 = "I understand that the commencement of risk coverage under the policy is subject to the receipt by HSBC Insurance of";
        final String RED3 = "in the Benefit Illustration";
        final String RED4 = "in the Product Summary and General Provision";
        final String RED5 = "in the DIRECT-ValueTerm Fact Sheet";
        final String RED6 = "in the Product Summary and Annexure related to DIRECT-Critical Illness Rider";
        final String PDPA = "Your privacy is important to us.";
        final String DVT5YEAR="whilst the premiums for the Product in the first 5 years";
        final String titleText="In proceeding to make this purchase";
        //Adds the file parsing logic and putting the string in a array
        try{
            String fileReceived=FileUtils.readFileToString(fileDeclaration1,"utf-8");
            
            int jspScript =StringUtils.lastIndexOf(fileReceived, "%>");
            fileReceived=StringUtils.substring(fileReceived, jspScript+2);

            int indexChoose=StringUtils.indexOf(fileReceived, "<c:choose>");
            int indexEndChoose=StringUtils.indexOf(fileReceived, "</c:choose>");

            String prev=StringUtils.substring(fileReceived, 0, indexChoose);
            String next=StringUtils.substring(fileReceived,indexEndChoose+11);
            if(indexChoose!=-1){
                fileReceived=prev+" "+next;
            }
            Document actualTags=Jsoup.parse(fileReceived);

            Elements formTag=actualTags.children();
            int liElements=0;
            for(int k=0;k<formTag.size();k++){
                Element n=formTag.get(k);
                Elements children=n.getAllElements();
                for(int i=0;i<children.size();i++){
                    String tagNameRetrieved=children.get(i).tagName();
                    Element elementencountered=children.get(i);
                    if(StringUtils.equalsIgnoreCase(tagNameRetrieved, "ol")){
                        Elements childOrdered=elementencountered.children();
                        for(int o=0;o<childOrdered.size();o++){
                            //Change to bullet point HSBCIDCU-630
                            dataDec.add(new ArrayList<>(Arrays.asList("\u25E6",childOrdered.get(o).text())));
                            int childOfolLinks=childOrdered.get(o).children().size();
                            if(childOfolLinks>0){
                                i=i+childOfolLinks;
                            }
                            if(o==childOrdered.size()-1){
                                i=i+childOrdered.size();
                            }
                        }

                    }else if(StringUtils.equalsIgnoreCase(tagNameRetrieved, "ul")){ //added this section from OP
                        Elements parentsOrdered=elementencountered.parents();
                        for(int p=0;p<parentsOrdered.size();p++){
                            Element parent=parentsOrdered.get(p);
                            if(StringUtils.equalsIgnoreCase(parent.tagName(),"ul")){
                                Elements innerUlChild= elementencountered.children();


                                for(int innerChild=0;innerChild<innerUlChild.size();innerChild++){
                                    //Change to bullet point HSBCIDCU-630
                                	
                                    if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),RED3) ||
	                                            StringUtils.startsWithIgnoreCase(elementencountered.text(),RED4) ||
	                                            StringUtils.startsWithIgnoreCase(elementencountered.text(),RED5) ||
	                                            StringUtils.startsWithIgnoreCase(elementencountered.text(),RED6) ) {
	                                        dataDec.add(new ArrayList<>(Arrays.asList("\u25E6", "<red>"+ innerUlChild.get(innerChild).text())));
                                    }else if(StringUtils.startsWithIgnoreCase(innerUlChild.get(innerChild).text(),DVT5YEAR)){
                                    	if(getTermYears()==5){
                                    		dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u25E6"), String.format(innerUlChild.get(innerChild).text()))));
                                    	}
                                    }else{
		                                    String html=innerUlChild.get(innerChild).html();
		          	  						String before=StringUtils.substringBefore(html, "<br>");
		          	  						before=StringUtils.replace(before, "<li>", "");
		          	  						String text=innerUlChild.get(innerChild).text();
		          	  						if(StringUtils.contains(html, "<br>")){
		          	  							text=before+"<br>  "+StringUtils.substringAfter(text, before);
		          	  						}
		          	  					if(innerUlChild.get(innerChild).hasAttr("inner-intended")){
		          	  					 dataDec.add(new ArrayList<>(Arrays.asList(String.format("<inner-intend>\u25E6"), String.format(text))));
		          	  					}else{
		          							 dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u25E6"), String.format(text))));
		          	  					}
                                    }
                                    int childOfolLinks=innerUlChild.get(innerChild).children().size();
                                    if(childOfolLinks>0){
                                        i=i+childOfolLinks;
                                    }
                                    if(innerChild==innerUlChild.size()-1){

                                        i=i+innerUlChild.size();
                                    }
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            }
                        }

                    }else if(StringUtils.equalsIgnoreCase(tagNameRetrieved, "li")){
                        liElements=liElements+1;
                        boolean ownText=false;
                        Elements child=elementencountered.children();
                        for(int l=0;l<child.size();l++){
                            String tag=child.get(l).tagName();
                            if(StringUtils.equalsIgnoreCase(tag, "ol")||StringUtils.equalsIgnoreCase(tag, "ul")){
                                ownText=true;
                            }
                        }
                        if(ownText){
                            if ( StringUtils.startsWithIgnoreCase(elementencountered.ownText(),JAPAN_NATIONAL) ) {
                                if ( getApplicantNationality().equals("Japan") || getApplicantNationality2().equals("Japan") || getApplicantNationality3().equals("Japan") ) {
                                    dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"), String.format(elementencountered.ownText()))));
                                }
                            } else {
                            	if(elementencountered.hasAttr("intended")){
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(elementencountered.ownText()))));
                            	}else{
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format(elementencountered.ownText()))));
                            	}
                            }
                        }else{
                            //Change to bullet point HSBCIDCU-630
                            //Print this sentence "Applicable only to Japanese Nationals residing ins Japan..." in pdf if nationality is japanese only
                            if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),JAPAN_NATIONAL) ) {
                                if ( getApplicantNationality().equals("Japan") || getApplicantNationality2().equals("Japan") || getApplicantNationality3().equals("Japan") ) {
                                    dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format( elementencountered.text()))));
                                    dataDec.add(new ArrayList<>(Arrays.asList("")));
                                }
                                
                            } else if (StringUtils.startsWithIgnoreCase(elementencountered.text(),IGNORE1) ||
                                    StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE2) ||
                                    StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE3) ) {
                                logger.info("not taking duplicate:" + IGNORE1 + "," + IGNORE2 + "," + IGNORE3);
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),BOLD1) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD2) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD3) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD4) ) {
                            	if(StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD2)){
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format("<b>"+ elementencountered.text()+"</b>"))));
                            	}else{
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format("<b>"+ elementencountered.text()+"</b>"))));
                            	}
                                
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),RED1) ||
                                    StringUtils.startsWithIgnoreCase(elementencountered.text(),RED2) ) {
                            	if(StringUtils.startsWithIgnoreCase(elementencountered.text(),RED1)){
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format("<red>"+ elementencountered.text()))));
                            	}else{
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format("<red>"+ elementencountered.text()))));
                            	}
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else {
                            	if(elementencountered.hasAttr("intended")){
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(elementencountered.text()))));
                            	}else if(elementencountered.hasAttr("inner-intended")){
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<inner-intend>\u2022"),String.format(elementencountered.text()))));
                            	}else{
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format(elementencountered.text()))));
                            	}
                                
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            }
                        }

                    }else if(StringUtils.equalsIgnoreCase(tagNameRetrieved, "h3")||StringUtils.equalsIgnoreCase(tagNameRetrieved, "p")){

                        //this fixes blank signatures in pdf ...HSBCIDCU-632
                        if ( StringUtils.equals(elementencountered.text(),"I accept ()")||StringUtils.equals(elementencountered.text(),"checked> I accept ()") ) {
                            String iAccept = "<img-check>I accept (" + getApplicantFullName() + ")";
                            dataDec.add(new ArrayList<>(Arrays.asList(iAccept)));
                            dataDec.add(new ArrayList<>(Arrays.asList("")));
                        } else if ( StringUtils.equals(elementencountered.text(),"We accept ( & )")||StringUtils.equals(elementencountered.text(),"checked> We accept ( & )") ) {
                            String weAccept = "<img-check>We accept (" + getApplicantFullName() + " & " + getInsuredFullName() + ")";
                            dataDec.add(new ArrayList<>(Arrays.asList(weAccept)));
                            dataDec.add(new ArrayList<>(Arrays.asList("")));
                        } else if ( StringUtils.equals(elementencountered.text(),"I agree with the statement below") || StringUtils.equals(elementencountered.text(),"checked> I agree with the statement below") ) {
                        	logger.info("Ignore these:I agree with the statement below");
                        } else if ( StringUtils.startsWith(elementencountered.text(), "checked>") ) {
                            String checked = StringUtils.removeStart(elementencountered.text(), "checked> ");
                            checked="<img-check>"+checked;
                            dataDec.add(new ArrayList<>(Arrays.asList(checked)));
                        } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(), PDPA) ) {
                            dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format(elementencountered.text()))));
                            dataDec.add(new ArrayList<>(Arrays.asList("")));
                        } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE4) ||
                                StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE5) ) {
                           logger.info("Ignore these:" + IGNORE4);
                        } else{
                        	if(StringUtils.isNotBlank(elementencountered.text())){
                        		if(StringUtils.startsWithIgnoreCase(elementencountered.text(), titleText)){
                        			dataDec.add(new ArrayList<>(Arrays.asList(elementencountered.text())));	
                        		}else{
	                              dataDec.add(new ArrayList<>(Arrays.asList("para",elementencountered.text())));
                        		}
	                            dataDec.add(new ArrayList<>(Arrays.asList("")));
                        	}
                        }

                    }

                }
            }

        }catch(Exception e){
           logger.error("error is thrown in parsing the variables "+e);
        }
return dataDec;
    }

    private void generateBIPDFPage1(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);

        try {
            pd.setFont(PDFGenerator.FONT_BOLD, 9);
            BaseTable table = new BaseTable(yStartNewPage, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
            Row r1 = table.createRow(9);
            String title = "Consolidated Summary for Benefit Illustration (DIRECT - ValueTerm)";
            if (getIncludeCriticalIllnessRider()) {
                title = "Consolidated Summary for Benefit Illustration (DIRECT - ValueTerm with Riders)";
            }
            Cell c1 = r1.createCell(100, title, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(9);
            c1.setBottomPadding(10);
            c1.setTopPadding(0);
            c1.setLeftPadding(0);
            Row r2 = table.createRow(9);
            Cell c2 = r2.createCell(100, "Policyowner and Plan Details", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c2.setFont(pd.getFont());
            c2.setFontSize(9);
            c2.setBottomPadding(0);
            c2.setTopPadding(0);
            c2.setLeftPadding(0);
            yStart = table.draw() - 20;

            table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);

            List<String[]> rows = new ArrayList<>();
            rows.add(new String[]{"Applicant Name", getApplicantFullName()});
            rows.add(new String[]{"", ""});
            rows.add(new String[]{"Proposed Life Insured", getApplicantFullName()});
            rows.add(new String[]{"Age Next Birthday", Integer.toString(getApplicantAge())});
            rows.add(new String[]{"Sex", getApplicantGender()});
            rows.add(new String[]{"Smoker / Non Smoker", (getApplicantSmoker() ? "Y" : "N")});
            rows.add(new String[]{"", ""});
            rows.add(new String[]{String.format("%s Premium", getPaymentModeLabel()), "S$" + df.format((double) ProductCalculation.getPremiumAtAge(getApplicantAge(), this).get("premium"))});
            rows.add(new String[]{"", ""});
            rows.add(new String[]{"Plan", "DIRECT - ValueTerm"});
            rows.add(new String[]{"Policy Term", String.format("%s Years", getTermYears())});
            rows.add(new String[]{"Premium Payment Term", String.format("%s Years", getTermYears())});
            rows.add(new String[]{"Basic Sum Insured", "S$" + df.format(getSumAssured())});

            if (getIncludeCriticalIllnessRider()) {
                rows.add(new String[]{"", ""});
                rows.add(new String[]{"Riders Attached", "Riders", "Sum Insured", "Expiry Age", "Annual Premium (S$)"});
                double ciRider=(double) ProductCalculation.getCriticalIllnessRiderPremiumAtAge(getApplicantAge(), this).get("premium");
 
           	 double ciRiderTrunc=(Math.round(ciRider * 100.0) / 100.0);
                rows.add(new String[]{"HSBC Insurance Online Critical Illness Rider", "S$" + df.format(getRiderSumAssured()), df.format(getAgeAtTermEnd()), "S$" + df.format(ciRiderTrunc)});
            }

            for (int i = 0; i < rows.size(); i++) {
                if (i == 14) {
                    pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY3);
                } else {
                    if (i == 15 || i == 16) {
                        pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY4);
                    } else {
                        pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY2);
                    }
                }

            }

            yStart = table.draw() - 10;
        } catch (Exception e) {

            logger.error(e.getMessage());
        }

        try {
            BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
            Row r1 = table.createRow(9);
            Cell c1 = r1.createCell(100, "<b>Below is the summary of what I intend to purchase:</b>", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(9);
            c1.setBottomPadding(0);
            c1.setTopPadding(0);
            yStart = table.draw() - 10;
        } catch (Exception e) {

            logger.error(e.getMessage());
        }

        try {
            ArrayList<Map<String, Object>> bi = ProductCalculation.getBenefitIllustration(this);
            ArrayList<Map<String, Object>> cibi = getIncludeCriticalIllnessRider() ? ProductCalculation.getCriticalIllnessRiderBenefitIllustration(this) : null;
            ArrayList<Map<String, Object>> ropbi = null;
            List<List> data = new ArrayList();

            ArrayList tuple = new ArrayList<>(Arrays.asList("", String.format("The Base Product<br>%s", getIncludeCriticalIllnessRider()? "(For comparison)" : "(Your selected plan)")));
            if (getIncludeCriticalIllnessRider()) {
                tuple.add(1, "The Base Product and the CI Rider<br>(Your selected plan)");
            }
            data.add(tuple);

            double value = (double)bi.get(0).get("premium");
            tuple = new ArrayList<>(Arrays.asList("How much you pay in a year", String.format("S$%s annually for " + getTermYears() + " years", df.format(value))));
            if (getIncludeCriticalIllnessRider()) {
                value += getIncludeCriticalIllnessRider() ? Math.round((double) cibi.get(0).get("premium")) : 0;
                tuple.add(1, String.format("S$%s annually for %s years", df.format(value), getTermYears()));
            }
            data.add(tuple);

            value = (float)bi.get(9).get("premiumPaid");
            //fixme not 10 years
            tuple = new ArrayList<>(Arrays.asList("How much you pay for the first 10 years", String.format("S$%s in total premium", df.format(value))));
            if (getIncludeCriticalIllnessRider()) {
                value += getIncludeCriticalIllnessRider() ? (float)cibi.get(9).get("premiumPaid") : 0;
                tuple.add(1, String.format("S$%s in total premium", df.format(value)));
            }
            data.add(tuple);

            tuple = new ArrayList<>(Arrays.asList("What will be my Sum Insured payout", String.format("S$%s",  df.format(getSumAssured()))));
            if (getIncludeCriticalIllnessRider()) {
                if (getIncludeCriticalIllnessRider()) {
                    tuple.add(1, String.format("S$%s for Base Product<br><br>S$%s for CI Rider", df.format(getSumAssured()), df.format(getRiderSumAssured())));
                } else {
                    tuple.add(1, String.format("S$%s for Base Product", df.format(getSumAssured())));
                }
            }
            data.add(tuple);

            tuple = new ArrayList<>(Arrays.asList("What will you get back at the end of the 10th year if there is no claims", "Nil"));
            if (getIncludeCriticalIllnessRider()) {
                tuple.add(1, "Nil");
            }
            data.add(tuple);

            tuple = new ArrayList<>(Arrays.asList("Renewability", "Yes. The Product 10 plan is renewable at the end of each 10-year policy term, subject to maximum renewal age of 80 next birthday. The policy will renew for 10 years at each renewal.\\n\\nThe premiums at point of renewal will change according to your age and the prevailing rates applicable at point of renewal."));
            if (getIncludeCriticalIllnessRider()) {
                tuple.add(1, "<p>Yes. The Base Product and CI Rider are renewable at the end of each 10 year policy term subject maximum renewal ages as follow:</p><ol>" +
                        "<li>Base Product: age 80 next birthday</li>" +
                        "<li>CI Rider: age 80 next birthday</li>" +
                        "</ol><p>The policy will renew for 10 years at each renewal.</p>" +
                        "<p>The premiums for the CI rider is not guaranteed during the policy term, and the premiums at point of renewal will change according to your age and the rates applicable at point of renewal.</p>");
            }
            data.add(tuple);

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, (tableWidth * (getIncludeCriticalIllnessRider()? 1 : 0.75f)), PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            String title = "HSBC Insurance DIRECT - ValueTerm (\"the Base Product\")";
            if (getIncludeCriticalIllnessRider()) {
                title = "HSBC Insurance DIRECT - ValueTerm (\"the Base Product\") and the HSBC Insurance Online Critical Illness Rider (\"the CI Rider\")";
            }
            t.addListToTable(data, PlanDataTable.HASHEADER, title);
            dataTable.draw();
        } catch (Exception e) {
            logger.error(e.getStackTrace().toString());

        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
    }

    private void generateBIPDFPage2(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);

        try{
            yStart = PDFLayout.mainTitleList(new String[]{
                    "Benefit Illustration",
                    "DIRECT - ValueTerm (" + getProductCode() + ")"
            },yStart, yStartNewPage, tableWidth, page, pd, 20);

            yStart = PDFLayout.titledParagraphList("Introduction",
                    new String[]{
                            "HSBC Insurance (Singapore) Pte. Limited believes that is important that you fully appreciate the benefits of your policy. You should also understand how the cost of your insurance cover and the expenses of administration and sales affect the benefits that you will receive.",
                            "The illustration that follows shows how the value of your policy progresses over time and the sum(s) that would be payable. The methods used to derive the values shown follow guidelines established by the Life Insurance Association, Singapore, to ensure that a fair and consistent approach is used in preparing this illustration.",
                            "Buying a life insurance policy can be a long-term commitment. An early termination of the policy usually involves high costs and the surrender value payable may be less than the total premiums paid.",
                            "If you need clarification, please do not hesitate to contact HSBC Insurance Customer Service at (65) 6225 6111 or email us at e-surance@hsbc.com.sg."
                    }, yStart, yStartNewPage, tableWidth, page, pd, 20);

            yStart = PDFLayout.titledParagraphList("Important notes",
                    new String[]{
                            "This illustration assumes you are a standard life and are accepted on standard terms. This is not a contract of assurance. The precise benefits, terms and conditions will be provided in the Policy Contract.",
                            "The figures shown in the illustration and the actual value (premium and benefit amount) shown in the Policy Contract / Schedule may differ due to rounding difference. In the event of inconsistency, the amount shown in the Policy Contract / Schedule shall prevail.",
                            "We will deduct outstanding premium and any amount owing from the benefits payable.",
                    }, yStart, yStartNewPage, tableWidth, page, pd, 20);

            yStart = PDFLayout.boldParagraphList(
                    new String[]{
                            "There is no distribution cost for this product as the policy is sold without financial advice. Distribution costs include commissions and other benefits paid to the sales representative.",
                            "This Benefit Illustration is computer-generated and does not require a physical signature. This Benefit Illustration is generated based on the information you have provided."
                    }, yStart, yStartNewPage, tableWidth, page, pd, 20);

        } catch (Exception e) {
            logger.error(e.getStackTrace().toString());

        }
    }

    private void generateBIPDFPage3(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);

        try {
            yStart = PDFLayout.title("Policyowner and Plan Details", yStart, yStartNewPage, tableWidth, page, pd, 20);

            yStart = PDFLayout.labelValueList(Arrays.asList(
                    new String[]{"Applicant Name", getApplicantFullName()},
                    new String[]{"", ""},
                    new String[]{"Proposed Life Insured", getApplicantFullName()},
                    new String[]{"Age Next Birthday", String.valueOf(calculateAgeNextBirthday(getApplicantDob()))},
                    new String[]{"Sex", getGenderDescription()},
                    new String[]{"Smoker / Non Smoker", (getApplicantSmoker() ? "Smoker" : "Non-Smoker")},
                    new String[]{"", ""},
                    new String[]{"", ""},
                    new String[]{"Plan", "DIRECT - ValueTerm (" + getProductCode() + ")"},
                    new String[]{"Sum Insured", String.format("S$%s", df.format(getSumAssured()))},
                    new String[]{String.format("%s Premium", getPaymentModeLabel()), String.format("S$%s", df2.format((double) ProductCalculation.getPremiumAtAge(getApplicantAge(), this).get("modalPremium")))},
                    new String[]{"Policy Term", String.format("%s Years", getTermYears())},
                    new String[]{"Premium Payment Term", String.format("%s Years", getTermYears())},
                    new String[]{"", ""}
            ), yStart, yStartNewPage, tableWidth, page, pd, 20);


            Map<String, Double> premiums = new HashMap<>();
            premiums.put("annually", ((double)ProductCalculation.getPremiumAtAge(calculateAgeNextBirthday(getApplicantDob()), PAYMENT_MODE_ANNUALLY, this).get("modalPremium")));
            premiums.put("semi_annually", ((double)ProductCalculation.getPremiumAtAge(calculateAgeNextBirthday(getApplicantDob()), PAYMENT_MODE_SEMI_ANNUALLY, this).get("modalPremium")));
            premiums.put("quarterly", ((double)ProductCalculation.getPremiumAtAge(calculateAgeNextBirthday(getApplicantDob()), PAYMENT_MODE_QUARTERLY, this).get("modalPremium")));
            premiums.put("monthly", ((double)ProductCalculation.getPremiumAtAge(calculateAgeNextBirthday(getApplicantDob()), PAYMENT_MODE_MONTHLY, this).get("modalPremium")));
            yStart = PDFLayout.tableAlignCentre(null, Arrays.asList(
                    Arrays.asList("", "Annually<br>(S$)", "Semi Annually<br>(S$)", "Quarterly<br>(S$)", "Monthly<br>(S$)"),
                    Arrays.asList("Basic Premium", df2.format(premiums.get("annually")), df2.format(premiums.get("semi_annually")), df2.format(premiums.get("quarterly")), df2.format(premiums.get("monthly")))
            ), true, yStart, yStartNewPage, tableWidth, page, pd, 30);

            pd.setFont(PDFGenerator.FONT_BOLD, 10);
            pd.writeText(page, "Benefit Illustration for Basic Plan", yStart);
            yStart -= 10;

            // below page need to differentiate the 5 years or 20 years(65 years)
            ArrayList<List> biData = new ArrayList<>();
            ArrayList<List> biFinalData = new ArrayList<>();

            if (DVT_5_YEARS_PRODUCT_CODE.equals(getProductCode())) {
                biData.add(Arrays.asList("End of Policy Year / Age", "Annual Premium<br>(S$)", "Total Premiums Paid To-Date<br>(S$)", "Death Benefit Guaranteed<br>(S$)", "Surrender Value Guaranteed<br>(S$)"));
                biFinalData.add(Arrays.asList("End of Policy Year / Age", "Annual Premium<br>(S$)", "Total Premiums Paid To-Date<br>(S$)", "Death Benefit Guaranteed<br>(S$)", "Surrender Value Guaranteed<br>(S$)"));
                for (Map<String, Object> bi : ProductCalculation.getBenefitIllustration(this)) {
                    if ((int) bi.get("year") > 85) { break; }
                    if ((double) bi.get("premium") <= 0) { continue; }
                    if ((int) bi.get("year") <= 10 || ((int) bi.get("year") % 5 == 0)) {
                        biData.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", bi.get("year"), bi.get("age")),
                                df.format((double) bi.get("premium")),
                                df.format((float) bi.get("premiumPaid")),
                                df.format(getSumAssured()),
                                df.format(0))));
                    }

                    if ((int) bi.get("age") == 55 || (int) bi.get("age") == 60 || (int) bi.get("age") == 65 || (int) bi.get("age") == 85) {
                        biFinalData.add(new ArrayList<>(Arrays.asList(String.format("Age %s", bi.get("age")), (double) bi.get("premium") <= 0 ? 0 : df.format((double) bi.get("premium")), df.format((float) bi.get("premiumPaid")), df.format(getSumAssured()), df.format(0))));
                    }
                }
            } else {
                biData.add(Arrays.asList("End of Policy Year / Age", "Total Premiums Paid To-Date<br>(S$)", "Death Benefit Guaranteed<br>(S$)", "Surrender Value Guaranteed<br>(S$)"));

                boolean isDisplayAgeTable = true;
                if (DVT_20_YEARS_PRODUCT_CODE.equals(getProductCode())) {
                    for (Map<String, Object> bi : ProductCalculation.getBenefitIllustration(this)) {
                        if ((int) bi.get("age") > 85) { break; }
                        if ((int) bi.get("year") <= 10 || (int) bi.get("year") % 5 == 0 || (int) bi.get("age") == 85) {
                            biData.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", bi.get("year"), bi.get("age")), df.format((float) bi.get("premiumPaid")), df.format(getSumAssured()), df.format(0))));
                        }
                        if ((int) bi.get("age") == 55 || (int) bi.get("age") == 60 || (int) bi.get("age") == 65 || (int) bi.get("age") == 85) {
                            if (isDisplayAgeTable) {
                                biFinalData.add(Arrays.asList("End of Policy Year / Age", "Total Premiums Paid To-Date<br>(S$)", "Death Benefit Guaranteed<br>(S$)", "Surrender Value Guaranteed<br>(S$)"));
                                isDisplayAgeTable = false;
                            }
                            biFinalData.add(new ArrayList<>(Arrays.asList(String.format("Age %s", bi.get("age")), df.format((float) bi.get("premiumPaid")), df.format(getSumAssured()), df.format(0))));
                        }
                    }
                } else {
                    biFinalData.add(Arrays.asList("End of Policy Year / Age", "Total Premiums Paid To-Date<br>(S$)", "Death Benefit Guaranteed<br>(S$)", "Surrender Value Guaranteed<br>(S$)"));
                    for (Map<String, Object> bi : ProductCalculation.getBenefitIllustration(this)) {
                        if ((int) bi.get("age") > 65) {
                            break;
                        }
                        if ((int) bi.get("year") <= 10 || (int) bi.get("year") % 5 == 0 || (int) bi.get("age") == 65) {
                            biData.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", bi.get("year"), bi.get("age")), df.format((float) bi.get("premiumPaid")), df.format(getSumAssured()), df.format(0))));
                        }
                        if ((int) bi.get("age") == 55 || (int) bi.get("age") == 60 || (int) bi.get("age") == 65) {
                            biFinalData.add(new ArrayList<>(Arrays.asList(String.format("Age %s", bi.get("age")), df.format((float) bi.get("premiumPaid")), df.format(getSumAssured()), df.format(0))));
                        }
                    }
                }
            }
            //biData.addAll(biFinalData);
            yStart = PDFLayout.tableAlignCentre(null, biData, true, yStart, yStartNewPage, tableWidth, page, pd, 20);

            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);

            yStart = PDFLayout.tableAlignCentre(null, biFinalData, true, yStart, yStartNewPage, tableWidth, page, pd, 10);

            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);

            if (DVT_5_YEARS_PRODUCT_CODE.equals(getProductCode())) {
                yStart = PDFLayout.paragraphList(new String[]{
                        "Please note that at each renewal, the premium payable will be based on the prevailing premium rates at the Life Insured's age next birthday and will stay level throughout the renewed term.",
                        "At the expiry of each policy term, the Policy will be automatically renewed for an additional 5 years at the prevailing Sum Insured and on the same terms and conditions without evidence of insurability. Refer to Product Summary and Policy Contract for terms and condiitons of renewability.",
                }, yStart, yStartNewPage, tableWidth, page, pd, 20);
            }

        } catch (Exception e) {
            logger.error("Error message: " + e.getMessage());
        }
    }

    private void generateBIPDFPage4(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT)-50;

        try {

            yStart = PDFLayout.titledParagraphList("Supplementary Illustration (Supplementary Benefits)",
                    new String[] {
                            "This is only a supplementary illustration and must be read in conjunction with the main illustration.",
                            "Supplementary benefits are optional insurance benefits payable in addition to the basic insurance benefit. The benefit terminates once a claim is fully paid. You may continue the policy and its other benefits after a claim for a chosen benefit has been admitted, unless we state otherwise.",
                    }, yStart, yStartNewPage, tableWidth, page, pd, 10);

            Map<Integer, Float> ciPremiums = new HashMap<>();
            int anb = calculateAgeNextBirthday(getApplicantDob());
            /*
             * Added truncations to 2 decimal places before the monthly ,
             * yearly premiums are added for basic and ci components
             */
            double basicMonthlyPremium=((double)ProductCalculation.getPremiumAtAge(anb, PAYMENT_MODE_MONTHLY, this).get("modalPremium"));

            double basicMonthlyPremiumAdd=(Math.round(basicMonthlyPremium * 100.0) / 100.0);
            
            double basicAnnualPremium=((double)ProductCalculation.getPremiumAtAge(anb, PAYMENT_MODE_ANNUALLY, this).get("modalPremium"));

            double basicAnnualPremiumAdd = (Math.round(basicAnnualPremium * 100.0) / 100.0);
            
            double basicSemiAnnualPremium=((double)ProductCalculation.getPremiumAtAge(anb,PAYMENT_MODE_SEMI_ANNUALLY, this).get("modalPremium"));

            double basicSemiAnnualPremiumAdd=(Math.round(basicSemiAnnualPremium * 100.0) / 100.0);
            
            double basicQuarterlyPremium=((double)ProductCalculation.getPremiumAtAge(anb, PAYMENT_MODE_QUARTERLY, this).get("modalPremium"));

            double basicQuarterlyPremiumAdd=(Math.round(basicQuarterlyPremium * 100.0) / 100.0);

            double ciMonthlyPremium=(((double) ProductCalculation.getCriticalIllnessRiderPremiumAtAge(getApplicantAge(), PAYMENT_MODE_MONTHLY, this).get("modalPremium")));

            double ciMonthlyPremiumAdd=(Math.round(ciMonthlyPremium * 100.0) / 100.0);
            
            double ciAnnualPremium=(((double) ProductCalculation.getCriticalIllnessRiderPremiumAtAge(getApplicantAge(), PAYMENT_MODE_ANNUALLY, this).get("modalPremium")));

            double ciAnnualPremiumAdd=(Math.round(ciAnnualPremium * 100.0) / 100.0);
            
            double ciSemiAnnualPremium=(((double) ProductCalculation.getCriticalIllnessRiderPremiumAtAge(getApplicantAge(), PAYMENT_MODE_SEMI_ANNUALLY, this).get("modalPremium")));

            double ciSemiAnnualPremiumAdd=(Math.round(ciSemiAnnualPremium * 100.0) / 100.0);
            
            double ciSemiQuarterlyPremium=(((double) ProductCalculation.getCriticalIllnessRiderPremiumAtAge(getApplicantAge(), PAYMENT_MODE_QUARTERLY, this).get("modalPremium")));

            double ciSemiQuarterlyPremiumAdd=(Math.round(ciSemiQuarterlyPremium * 100.0) / 100.0);
            

            ciPremiums.put(PAYMENT_MODE_ANNUALLY,(float) (basicAnnualPremiumAdd+ciAnnualPremiumAdd));
            ciPremiums.put(PAYMENT_MODE_SEMI_ANNUALLY,(float) (basicSemiAnnualPremiumAdd+ciSemiAnnualPremiumAdd));
            ciPremiums.put(PAYMENT_MODE_QUARTERLY,(float) (basicQuarterlyPremiumAdd+ciSemiQuarterlyPremiumAdd));
            ciPremiums.put(PAYMENT_MODE_MONTHLY,(float) (basicMonthlyPremiumAdd+ciMonthlyPremiumAdd));


            Map<Integer, Double> ciPremiumsOnly = new HashMap<>();
            ciPremiumsOnly.put(PAYMENT_MODE_ANNUALLY,(ciAnnualPremium));
            ciPremiumsOnly.put(PAYMENT_MODE_SEMI_ANNUALLY,(ciSemiAnnualPremium));
            ciPremiumsOnly.put(PAYMENT_MODE_QUARTERLY,(ciSemiQuarterlyPremium));
            ciPremiumsOnly.put(PAYMENT_MODE_MONTHLY,(ciMonthlyPremium));

            //Ticket 590 to remove this table and add like as OP
            try {
                pd.setFont(PDFGenerator.FONT_NORMAL, 10);
                BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
                Row r = table.createRow(9);

                String text = "<b>Supplementary Benefits</b><br>Total & Permanent Disability" + (getIncludeCriticalIllnessRider()?"<br>Critical Illness": "");
                Cell c1 = r.createCell(35, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                c1.setFont(pd.getFont());
                c1.setFontSize(8);
                c1.setBottomPadding(0);
                c1.setTopPadding(0);
                c1.setLeftPadding(0);

                text = String.format("<b>Sum Insured (S$)</b><br>%s%s", df.format(getSumAssured()), (getIncludeCriticalIllnessRider()?"<br>"+df.format(getRiderSumAssured()): "")) ;
                Cell c2 = r.createCell(20, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                c2.setFont(pd.getFont());
                c2.setFontSize(8);
                c2.setBottomPadding(0);
                c2.setTopPadding(0);
                c2.setLeftPadding(0);

                text = String.format("<b>Expiry Age</b><br>%s%s", getAgeAtTermEnd()+1, (getIncludeCriticalIllnessRider()?"<br>"+(getAgeAtTermEnd()+1): ""));
                Cell c3 = r.createCell(15, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                c3.setFont(pd.getFont());
                c3.setFontSize(8);
                c3.setBottomPadding(0);
                c3.setTopPadding(0);
                c3.setLeftPadding(0);

                text = String.format("<b>%s Premium (S$)</b><br>%s%s", getPaymentModeLabel(), "-", (getIncludeCriticalIllnessRider()?"<br>"+df2.format(ciPremiumsOnly.get(getPaymentMode())): ""));
                Cell c4 = r.createCell(30, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                c4.setFont(pd.getFont());
                c4.setFontSize(8);
                c4.setBottomPadding(0);
                c4.setTopPadding(0);
                c4.setLeftPadding(0);

                yStart = table.draw() - 15;
            } catch (Exception e) {
                logger.error("Error Message "+e);
            }



            yStart = PDFLayout.tableAlignCentre(null, Arrays.asList(
                    Arrays.asList("", "Annually<br>(S$)", "Semi Annually<br>(S$)", "Quarterly<br>(S$)", "Monthly<br>(S$)"),
                    Arrays.asList("Total Premium (inclusive of supplementary benefits)", df2.format(ciPremiums.get(PAYMENT_MODE_ANNUALLY)), df2.format(ciPremiums.get(PAYMENT_MODE_SEMI_ANNUALLY)), df2.format(ciPremiums.get(PAYMENT_MODE_QUARTERLY)), df2.format(ciPremiums.get(PAYMENT_MODE_MONTHLY)))
            ), true, yStart, yStartNewPage, tableWidth, page, pd, 30);

            ArrayList<Map<String, Object>> biList = ProductCalculation.getCriticalIllnessRiderBenefitIllustration(this);
            List<List> biData = new ArrayList();
            List<List> biFinalData = new ArrayList();
            biData.add(new ArrayList<>(Arrays.asList("End of Policy Year / Age", "Rider Annual Premium<br>(S$)", "Total Premiums Paid To-Date<br>(S$)")));

            // if 20 year term, only print 20 years
            if (DVT_20_YEARS_PRODUCT_CODE.equals(getProductCode())) {
                boolean isDisplayAgeTable = true;
                for (Map<String, Object> bi : biList) {
                    if ((int) bi.get("age") > 65) { break; }
                    if ((double) bi.get("premium") <= 0) { continue; }
                    if ((int) bi.get("year") <= 10 || ((int) bi.get("year") % 5 == 0) && (int) bi.get("year") <= 20 || (int)bi.get("age") == 65) {
                        biData.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", bi.get("year"), bi.get("age")), (double)bi.get("premium")<=0?0:df.format((double) bi.get("premium")), df.format((float) bi.get("premiumPaid")))));
                    }
                    if ((int) bi.get("age") == 55 || (int) bi.get("age") == 60 || (int) bi.get("age") == 65) {
                        if (isDisplayAgeTable) {
                            biFinalData.add(new ArrayList<>(Arrays.asList("End of Policy Year / Age", "Rider Annual Premium<br>(S$)", "Total Premiums Paid To-Date<br>(S$)")));
                            isDisplayAgeTable = false;
                        }
                        biFinalData.add(new ArrayList<>(Arrays.asList(String.format("Age %s", bi.get("age")), (double)bi.get("premium")<=0?0:df.format((double) bi.get("premium")), df.format((float) bi.get("premiumPaid")))));
                    }
                }
            } else {
                // else print up to 65
                biFinalData.add(new ArrayList<>(Arrays.asList("End of Policy Year / Age", "Rider Annual Premium<br>(S$)", "Total Premiums Paid To-Date<br>(S$)")));
                for (Map<String, Object> bi : biList) {
                    if ((int) bi.get("age") > 65) { break; }
                    if ((double) bi.get("premium") <= 0) { continue; }
                    if ((int) bi.get("year") <= 10 || (int) bi.get("year") % 5 == 0 ||  (DVT_AGE_65_PRODUCT_CODE.equals(getProductCode()) && (int) bi.get("age") == 65)) {
                        biData.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", bi.get("year"), bi.get("age")), (double)bi.get("premium")<=0?0:df.format((double) bi.get("premium")), df.format((float) bi.get("premiumPaid")))));
                    }
                    if ((int) bi.get("age") == 55 || (int) bi.get("age") == 60 || (int) bi.get("age") == 65) {
                        biFinalData.add(new ArrayList<>(Arrays.asList(String.format("Age %s", bi.get("age")), (double)bi.get("premium")<=0?0:df.format((double) bi.get("premium")), df.format((float) bi.get("premiumPaid")))));
                    }
                }
            }

            yStart = PDFLayout.tableAlignCentre(null, biData, true, yStart, yStartNewPage, tableWidth, page, pd, 10);

            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);

            yStart = PDFLayout.tableAlignCentre(null, biFinalData, true, yStart, yStartNewPage, tableWidth, page, pd, 10);

            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
            if (DVT_5_YEARS_PRODUCT_CODE.equals(getProductCode())) {
                yStart = PDFLayout.paragraphList(new String[]{
                        "At the expiry of each policy term, the Policy will be automatically renewed for an additional 5 years at the prevailing Sum Insured and on the same terms and conditions without evidence of insurability. Refer to Product Summary and Policy Contract for terms and conditions of renewability.",
                        "Please note that at each renewal, the premium payable will be based on the prevailing premium rates at the Life Insured's age next birthday and will stay level throughout the renewed term.",
                }, yStart, yStartNewPage, tableWidth, page, pd, 20);
            }


        } catch (Exception e) {
            logger.error(e.getMessage());

        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
    }

    /**
     * Convert product code to the term duration shorthand used in forms
     *
     * @param productCode
     * @return
     */
    public String getTermDurationCodeFromProductCode(String productCode) {
        if (productCode == null) return null;

        switch (productCode.toUpperCase()) {
            case "VT73":
                return "5Y";
            case "VT83":
                return "20Y";
            case "VT93":
                return "@65";
        }

        return null;
    }

    /**
     * Convert term duration shorthand to actual product code
     *
     * @param durationCode
     * @return
     */
    public String getProductCodeFromDurationCode(String durationCode) {
        if (durationCode == null) return null;

        switch (durationCode.toUpperCase()) {
            case "5Y":
                return DVT_5_YEARS_PRODUCT_CODE;
            case "20Y":
                return DVT_20_YEARS_PRODUCT_CODE;
            case "@65":
                return DVT_AGE_65_PRODUCT_CODE;
        }

        return null;
    }

    public String getProductCode() {
        if (productCode != null) {
            return productCode;
        }

        // TODO: this assumes the termDuration has been set properly, and that all requirements are valid
        return getProductCodeFromDurationCode(getTermDuration());
    }

    protected void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getRiderProductCode() {
        switch (getProductCode()){
            case "VT73":
                return "CD69";
            case "VT83":
            case "VT93":
                return "CD79";
        }
        return "CD79";
    }

    protected static DirectValueTermApplication populate(DirectValueTermApplication application, ResultSet rs) {
        try {
            application.setId(rs.getString("id"));
            application.setReferenceNum(rs.getString("reference_num"));
            application.setAgreePrivacy(getBooleanFromResultSet(rs, "agree_privacy"));
            application.setAgreeImportantNotes(getBooleanFromResultSet(rs, "agree_important_notes"));
            application.setLifeInsured(rs.getString("life_insured"));
            application.setStatus(rs.getInt("status"));
            application.setDropoutPassword(rs.getString("dropout_password"));
            application.setNotifyStatus(rs.getInt("notify_status"));
            application.setCampaignCode(rs.getString("campaign_code"));

            application.setApplicantResidency(rs.getString("applicant_residency"));
            application.setApplicantFirstName(rs.getString("applicant_first_name"));
            application.setApplicantLastName(rs.getString("applicant_last_name"));
            application.setApplicantDob(rs.getDate("applicant_dob"));
            application.setApplicantGender(rs.getString("applicant_gender"));
            application.setApplicantMobileCountryCode(rs.getString("applicant_mobile_country_code"));
            application.setApplicantMobileNum(rs.getString("applicant_mobile_num"));
            application.setApplicantEmail(rs.getString("applicant_email"));
            application.setApplicantNationality(rs.getString("applicant_nationality"));
            application.setApplicantMultipleNationalities(getBooleanFromResultSet(rs, "applicant_multiple_nationalities"));
            application.setApplicantNationality2(rs.getString("applicant_nationality2"));
            application.setApplicantNationality3(rs.getString("applicant_nationality3"));
            application.setApplicantBirthCountry(rs.getString("applicant_birth_country"));
            application.setApplicantNric(rs.getString("applicant_nric"));
            application.setApplicantEnProficient(getBooleanFromResultSet(rs, "applicant_en_proficient"));
            application.setApplicantEmployment(rs.getString("applicant_employment"));
            application.setApplicantIncome(getIntegerFromResultSet(rs, "applicant_income"));
            application.setApplicantSmoker(getBooleanFromResultSet(rs, "applicant_smoker"));
            application.setApplicantOccupation(rs.getString("applicant_occupation"));
            application.setApplicantIndustry(rs.getString("applicant_industry"));
            application.setApplicantCompanyName(rs.getString("applicant_company_name"));
            application.setApplicantCompanyCity(rs.getString("applicant_company_city"));
            application.setApplicantCompanyCountry(rs.getString("applicant_company_country"));
            application.setApplicantStudiesEndDate(rs.getDate("applicant_studies_end_date"));
            application.setApplicantAddressPostalCode(rs.getString("applicant_address_postal_code"));
            application.setApplicantAddressHouse(rs.getString("applicant_address_house"));
            application.setApplicantAddressUnit(rs.getString("applicant_address_unit"));
            application.setApplicantAddressStreet(rs.getString("applicant_address_street"));
            application.setApplicantAddressBuilding(rs.getString("applicant_address_building"));
            application.setApplicantAddressIsRegistered(getBooleanFromResultSet(rs, "applicant_address_registered"));
            application.setApplicantAddressPeriodOfStay(getIntegerFromResultSet(rs, "applicant_address_period_stay"));
            application.setApplicantPreviousAddressCountry(rs.getString("applicant_previous_address_country"));
            application.setApplicantAddressIsPermanent(getBooleanFromResultSet(rs, "applicant_address_permanent"));
            application.setApplicantPermanentAddressLine1(rs.getString("applicant_permanent_address_line1"));
            application.setApplicantPermanentAddressLine2(rs.getString("applicant_permanent_address_line2"));
            application.setApplicantPermanentAddressPostalCode(rs.getString("applicant_permanent_address_postal_code"));
            application.setApplicantPermanentAddressCountry(rs.getString("applicant_permanent_address_country"));

            application.setApplicantTaxCountry1(rs.getString("applicant_tax_country1"));
            application.setApplicantTaxCountry1Tin(rs.getString("applicant_tax_country1_tin"));
            application.setApplicantTaxCountry1NoTinReason(rs.getString("applicant_tax_country1_tin_reason"));
            application.setApplicantTaxCountry1NoTinReasonCustom(rs.getString("applicant_tax_country1_tin_reason_custom"));
            application.setApplicantTaxCountry2(rs.getString("applicant_tax_country2"));
            application.setApplicantTaxCountry2Tin(rs.getString("applicant_tax_country2_tin"));
            application.setApplicantTaxCountry2NoTinReason(rs.getString("applicant_tax_country2_tin_reason"));
            application.setApplicantTaxCountry2NoTinReasonCustom(rs.getString("applicant_tax_country2_tin_reason_custom"));
            application.setApplicantTaxCountry3(rs.getString("applicant_tax_country3"));
            application.setApplicantTaxCountry3Tin(rs.getString("applicant_tax_country3_tin"));
            application.setApplicantTaxCountry3NoTinReason(rs.getString("applicant_tax_country3_tin_reason"));
            application.setApplicantTaxCountry3NoTinReasonCustom(rs.getString("applicant_tax_country3_tin_reason_custom"));

            application.setApplicantAddressNoTaxReason(rs.getString("applicant_address_no_tax_reason"));
            application.setApplicantIsHsbcCustomer(getBooleanFromResultSet(rs, "applicant_hsbc_customer"));
            application.setApplicantIsPep(getBooleanFromResultSet(rs, "applicant_pep"));
            application.setApplicantIsBeneficalOwner(getBooleanFromResultSet(rs, "applicant_bo"));

            application.setInsuredResidency(rs.getString("insured_residency"));
            application.setInsuredFirstName(rs.getString("insured_first_name"));
            application.setInsuredLastName(rs.getString("insured_last_name"));
            application.setInsuredDob(rs.getDate("insured_dob"));
            application.setInsuredGender(rs.getString("insured_gender"));
            application.setInsuredNationality(rs.getString("insured_nationality"));
            application.setInsuredMultipleNationalities(getBooleanFromResultSet(rs, "insured_multiple_nationalities"));
            application.setInsuredNationality2(rs.getString("insured_nationality2"));
            application.setInsuredNationality3(rs.getString("insured_nationality3"));
            application.setInsuredNric(rs.getString("insured_nric"));
            application.setInsuredEmployment(rs.getString("insured_employment"));
            application.setInsuredIncome(getIntegerFromResultSet(rs, "insured_income"));
            application.setInsuredSmoker(getBooleanFromResultSet(rs, "insured_smoker"));
            application.setInsuredMedical1(getBooleanFromResultSet(rs, "insured_medical1"));
            application.setInsuredMedical2(getBooleanFromResultSet(rs, "insured_medical2"));
            application.setInsuredMedical3(getBooleanFromResultSet(rs, "insured_medical3"));
            application.setInsuredMedical4(getBooleanFromResultSet(rs, "insured_medical4"));
            application.setInsuredOccupation(rs.getString("insured_occupation"));
            application.setInsuredIndustry(rs.getString("insured_industry"));
            application.setInsuredCompanyName(rs.getString("insured_company_name"));
            application.setInsuredStudiesEndDate(rs.getDate("insured_studies_end_date"));

            application.setBeneficialOwnerFirstName(rs.getString("bo_first_name"));
            application.setBeneficialOwnerLastName(rs.getString("bo_last_name"));
            application.setBeneficialOwnerNric(rs.getString("bo_nric"));
            application.setBeneficialOwnerRelation(rs.getString("bo_relation"));

            application.setPaymentMode(rs.getInt("payment_mode"));
            application.setSumAssured(getFloatFromResultSet(rs, "sum_assured"));
            application.setRiderSumAssured(getFloatFromResultSet(rs, "rider_sum_assured"));
            application.setIncludeCriticalIllnessRider(getBooleanFromResultSet(rs, "include_ci_rider"));
            application.setUnderwritingLifestyleQuestionsOptin(getBooleanFromResultSet(rs, "uw_lifestyle_questions_optin"));
            application.setUnderwritingLifestyleMortgage(getIntegerFromResultSet(rs, "uw_lifestyle_mortgage"));
            application.setUnderwritingLifestyleProfile(getIntegerFromResultSet(rs, "uw_lifestyle_profile"));
            application.setUnderwritingLifestyleHeight(getIntegerFromResultSet(rs, "uw_lifestyle_height"));
            application.setUnderwritingLifestyleWeight(getIntegerFromResultSet(rs, "uw_lifestyle_weight"));
            application.setUnderwritingLifestyleExercise(getIntegerFromResultSet(rs, "uw_lifestyle_exercise"));
            application.setTermDuration(rs.getString("term_duration"));

            application.setCreditCardType(rs.getString("cc_type"));
            application.setCreditCardName((new TextEncryptor()).decrypt(rs.getString("cc_name")));
            application.setCreditCardNum((new TextEncryptor()).decrypt(rs.getString("cc_num")));
            application.setCreditCardExpiry((new TextEncryptor()).decrypt(rs.getString("cc_expiry")));

            application.setMaxStep(rs.getFloat("max_step"));
            // JIRA HSBCIDCU-632 - Fix existing insurance coverage amount issue
            application.setExistingLifeInsuranceCoverage(getFloatFromResultSet(rs,"existing_life_insurance_coverage"));
            //HSBCIDCU-592 - Wrong mail option saved to db
            application.setMailOption(rs.getInt("mail_option"));
            application.setEmailSentDate(rs.getTimestamp("email_sent_date"));
        } catch (Exception e) {
            logger.error("Error Msg "+e);
        }

        return application;
    }

    public boolean complete() {
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs;
        try {
            stmt = conn.prepareStatement("INSERT INTO application_reference_nums (application_id) VALUES(?)");
            stmt.setString(1, getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
            return false;
        } finally {
            db.closeAll(conn,stmt);
        }
        String referenceNum = ApplicationUtils.getNextAppGlobalRefNum(getReferenceNumComplexityFlag(), getReferenceNumMailOptionFlag());
        setReferenceNum(referenceNum);

        setStatus(STATUS_COMPLETE);

        //Save email sent date in UTC to be consistent with db
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -8);
        Date utc = cal.getTime();
        logger.info("Reference number:" + getReferenceNum() + ",Email sent (UTC):" + utc.toString());
        setEmailSentDate(utc);

        return save();

    }

    @Override
    public boolean save() {
        Database db = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        // prepare the update sql
        String sql = "UPDATE applications SET ";
        try {
            Object[][] columns = new Object[][] {
                    new Object[] { "reference_num", "string", (getReferenceNum()) },
                    new Object[] { "agree_privacy", "boolean", (getAgreePrivacy()) },
                    new Object[] { "agree_important_notes", "boolean", (getAgreeImportantNotes()) },
                    new Object[] { "max_step", "float", getMaxStep() },
                    new Object[] { "life_insured", "string", getLifeInsured() },
                    new Object[] { "status", "string", getStatus() },
                    new Object[] { "dropout_password", "string", getDropoutPassword() },
                    new Object[] { "notify_status", "string", getNotifyStatus() },
                    new Object[] { "mail_option", "string", getMailOption() },
                    new Object[] { "campaign_code", "string", getCampaignCode() },

                    new Object[] { "applicant_residency", "string", getApplicantResidency() },
                    new Object[] { "applicant_first_name", "string", getApplicantFirstName() },
                    new Object[] { "applicant_last_name", "string", getApplicantLastName() },
                    new Object[] { "applicant_dob", "date", getApplicantDob() },
                    new Object[] { "applicant_gender", "string", getApplicantGender() },
                    new Object[] { "applicant_mobile_country_code", "string", getApplicantMobileCountryCode() },
                    new Object[] { "applicant_mobile_num", "string", getApplicantMobileNum() },
                    new Object[] { "applicant_email", "string", getApplicantEmail() },
                    new Object[] { "applicant_nationality", "string", getApplicantNationality() },
                    new Object[] { "applicant_multiple_nationalities", "boolean", getApplicantMultipleNationalities() },
                    new Object[] { "applicant_nationality2", "string", getApplicantNationality2() },
                    new Object[] { "applicant_nationality3", "string", getApplicantNationality3() },
                    new Object[] { "applicant_birth_country", "string", getApplicantBirthCountry() },
                    new Object[] { "applicant_nric", "string", getApplicantNric() },
                    new Object[] { "applicant_en_proficient", "boolean", getApplicantEnProficient() },
                    new Object[] { "applicant_employment", "string", getApplicantEmployment() },
                    new Object[] { "applicant_income", "string", getApplicantIncome() },
                    new Object[] { "applicant_smoker", "boolean", getApplicantSmoker() },
                    new Object[] { "applicant_occupation", "string", getApplicantOccupation() },
                    new Object[] { "applicant_industry", "string", getApplicantIndustry() },
                    new Object[] { "applicant_company_name", "string", getApplicantCompanyName() },
                    new Object[] { "applicant_company_city", "string", getApplicantCompanyCity() },
                    new Object[] { "applicant_company_country", "string", getApplicantCompanyCountry() },
                    new Object[] { "applicant_studies_end_date", "date", getApplicantStudiesEndDate() },
                    new Object[] { "applicant_address_postal_code", "string", getApplicantAddressPostalCode() },
                    new Object[] { "applicant_address_house", "string", getApplicantAddressHouse() },
                    new Object[] { "applicant_address_unit", "string", getApplicantAddressUnit() },
                    new Object[] { "applicant_address_street", "string", getApplicantAddressStreet() },
                    new Object[] { "applicant_address_building", "string", getApplicantAddressBuilding() },
                    new Object[] { "applicant_address_registered", "boolean", getApplicantAddressIsRegistered() },
                    new Object[] { "applicant_address_period_stay", "string", getApplicantAddressPeriodOfStay() },
                    new Object[] { "applicant_previous_address_country", "string", getApplicantPreviousAddressCountry() },
                    new Object[] { "applicant_address_permanent", "boolean", getApplicantAddressIsPermanent() },
                    new Object[] { "applicant_permanent_address_line1", "string", getApplicantPermanentAddressLine1() },
                    new Object[] { "applicant_permanent_address_line2", "string", getApplicantPermanentAddressLine2() },
                    new Object[] { "applicant_permanent_address_postal_code", "string", getApplicantPermanentAddressPostalCode() },
                    new Object[] { "applicant_permanent_address_country", "string", getApplicantPermanentAddressCountry() },
                    new Object[] { "applicant_tax_country1", "string", getApplicantTaxCountry1() },
                    new Object[] { "applicant_tax_country1_tin", "string", getApplicantTaxCountry1Tin() },
                    new Object[] { "applicant_tax_country1_tin_reason", "string", getApplicantTaxCountry1NoTinReason() },
                    new Object[] { "applicant_tax_country1_tin_reason_custom", "string", getApplicantTaxCountry1NoTinReasonCustom() },
                    new Object[] { "applicant_tax_country2", "string", getApplicantTaxCountry2() },
                    new Object[] { "applicant_tax_country2_tin", "string", getApplicantTaxCountry2Tin() },
                    new Object[] { "applicant_tax_country2_tin_reason", "string", getApplicantTaxCountry2NoTinReason() },
                    new Object[] { "applicant_tax_country2_tin_reason_custom", "string", getApplicantTaxCountry2NoTinReasonCustom() },
                    new Object[] { "applicant_tax_country3", "string", getApplicantTaxCountry3() },
                    new Object[] { "applicant_tax_country3_tin", "string", getApplicantTaxCountry3Tin() },
                    new Object[] { "applicant_tax_country3_tin_reason", "string", getApplicantTaxCountry3NoTinReason() },
                    new Object[] { "applicant_tax_country3_tin_reason_custom", "string", getApplicantTaxCountry3NoTinReasonCustom() },
                    new Object[] { "applicant_address_no_tax_reason", "string", getApplicantAddressNoTaxReason() },
                    new Object[] { "applicant_hsbc_customer", "boolean", getApplicantIsHsbcCustomer() },
                    new Object[] { "applicant_pep", "boolean", getApplicantIsPep() },
                    new Object[] { "applicant_bo", "boolean", getApplicantIsBeneficalOwner() },

                    new Object[] { "insured_residency", "string", getInsuredResidency() },
                    new Object[] { "insured_first_name", "string", getInsuredFirstName() },
                    new Object[] { "insured_last_name", "string", getInsuredLastName() },
                    new Object[] { "insured_dob", "date", getInsuredDob() },
                    new Object[] { "insured_gender", "string", getInsuredGender() },
                    new Object[] { "insured_nationality", "string", getInsuredNationality() },
                    new Object[] { "insured_multiple_nationalities", "boolean", getInsuredMultipleNationalities() },
                    new Object[] { "insured_nationality2", "string", getInsuredNationality2() },
                    new Object[] { "insured_nationality3", "string", getInsuredNationality3() },
                    new Object[] { "insured_nric", "string", getInsuredNric() },
                    new Object[] { "insured_employment", "string", getInsuredEmployment() },
                    new Object[] { "insured_income", "string", getInsuredIncome() },
                    new Object[] { "insured_smoker", "boolean", getInsuredSmoker() },
                    new Object[] { "existing_life_insurance_coverage", "float", getExistingLifeInsuranceCoverage() },
                    new Object[] { "insured_medical1", "boolean", getInsuredMedical1() },
                    new Object[] { "insured_medical2", "boolean", getInsuredMedical2() },
                    new Object[] { "insured_medical3", "boolean", getInsuredMedical3() },
                    new Object[] { "insured_medical4", "boolean", getInsuredMedical4() },
                    new Object[] { "insured_occupation", "string", getInsuredOccupation() },
                    new Object[] { "insured_industry", "string", getInsuredIndustry() },
                    new Object[] { "insured_company_name", "string", getInsuredCompanyName() },
                    new Object[] { "insured_studies_end_date", "date", getInsuredStudiesEndDate() },

                    new Object[] { "bo_first_name", "string", getBeneficialOwnerFirstName() },
                    new Object[] { "bo_last_name", "string", getBeneficialOwnerLastName() },
                    new Object[] { "bo_nric", "string", getBeneficialOwnerNric() },
                    new Object[] { "bo_relation", "string", getBeneficialOwnerRelation() },

                    new Object[] { "payment_mode", "string", getPaymentMode() },
                    new Object[] { "sum_assured", "float", getSumAssured() },
                    new Object[] { "rider_sum_assured", "float", getRiderSumAssured() },
                    new Object[] { "include_ci_rider", "boolean", getIncludeCriticalIllnessRider() },
                    new Object[] { "include_pr_rider", "boolean", null },
                    new Object[] { "uw_lifestyle_questions_optin", "boolean", getUnderwritingLifestyleQuestionsOptin() },
                    new Object[] { "uw_lifestyle_mortgage", "string", getUnderwritingLifestyleMortgage() },
                    new Object[] { "uw_lifestyle_profile", "string", getUnderwritingLifestyleProfile() },
                    new Object[] { "uw_lifestyle_height", "string", getUnderwritingLifestyleHeight() },
                    new Object[] { "uw_lifestyle_weight", "string", getUnderwritingLifestyleWeight() },
                    new Object[] { "uw_lifestyle_exercise", "string", getUnderwritingLifestyleExercise() },

                    new Object[] { "cc_type", "string", getCreditCardType() },
                    new Object[] { "cc_name", "string", (new TextEncryptor()).encrypt(getCreditCardName()) },
                    new Object[] { "cc_num", "string", (new TextEncryptor()).encrypt(getCreditCardNum()) },
                    new Object[] { "cc_expiry", "string", (new TextEncryptor()).encrypt(getCreditCardExpiry()) },

                    new Object[] { "term_years", "int", getTermYears() },
                    new Object[] { "term_duration", "string", getTermDuration() },
                    new Object[] { "product_code", "string", getProductCode() },
                    new Object[] { "email_sent_date", "timestamp", getEmailSentDate() }
            };
            for (int i = 0; i < columns.length; i++) {
                sql += columns[i][0] + " = ?, ";
            }
            sql += "date_updated = NOW() WHERE id = ?";

            db = new Database();
            conn = db.getConnection();
            stmt = conn.prepareStatement(sql);
            for (int i = 0; i < columns.length; i++) {
                switch (columns[i][1].toString()) {
                    case "string":
                        stmt.setObject((i + 1), columns[i][2]);
                        break;
                    case "boolean":
                        stmt.setObject((i + 1), columns[i][2]);
                        break;
                    case "float":
                        stmt.setObject((i + 1), columns[i][2]);
                        break;
                    case "int":
                        stmt.setObject((i + 1), columns[i][2]);
                        break;
                    case "date":
                        stmt.setDate((i + 1), (columns[i][2] != null ? new java.sql.Date(((Date)columns[i][2]).getTime()) : null));
                        break;
                    case "timestamp":
                        stmt.setTimestamp((i + 1), (columns[i][2] != null ? new java.sql.Timestamp(((Date)columns[i][2]).getTime()) : null));
                        break;
                }
            }
            stmt.setString((columns.length + 1), id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e, "Error saving application");
            return false;
        } catch (Exception e) {
            ErrorHandler.handleError(this, e, "Error saving application");
            return false;
        }finally {
            db.closeAll(conn,stmt);
        }
            return true;
        }

    /**
     * Update the Feedback table
     * @return
     */
    public boolean saveFeedback() {
        Database db = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        if (getFeedbackExperience() != null || getFeedbackEase() != null || getFeedbackRecommend() != null || getFeedbackNoRecommendReason() != null || getFeedbackImprove() != null || getFeedbackAdditional() != null) {
            try{
                db = new Database();
                conn = db.getConnection();
                String sql = "INSERT INTO feedback (application_id, feedback_experience, feedback_ease, feedback_recommend, feedback_norecommend_reason, feedback_improve, feedback_additional) VALUES (?, ?, ?, ?, ?, ?, ?) ";
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, id);
                stmt.setInt(2, getFeedbackExperience());
                stmt.setInt(3, getFeedbackEase());
                if (getFeedbackRecommend()!=null)
                    stmt.setBoolean(4, getFeedbackRecommend());
                else
                    stmt.setString(4, null);
                stmt.setString(5, getFeedbackNoRecommendReason());
                stmt.setString(6, getFeedbackImprove());
                stmt.setString(7, getFeedbackAdditional());
                stmt.executeUpdate();
            } catch (SQLException e){
                ErrorHandler.handleError(this, e, "Error saving feedback for application");
                return false;
            } finally {
                db.closeAll(conn,stmt);
            }
        }
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceNum() {
        return referenceNum;
    }

    public void setReferenceNum(String referenceNum) {
        this.referenceNum = referenceNum;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public Boolean getAgreePrivacy() {
        return agreePrivacy;
    }

    public void setAgreePrivacy(Boolean agreePrivacy) {
        this.agreePrivacy = agreePrivacy;
    }

    public Boolean getAgreeImportantNotes() {
        return agreeImportantNotes;
    }

    public void setAgreeImportantNotes(Boolean agreeImportantNotes) {
        this.agreeImportantNotes = agreeImportantNotes;
    }

    public String getLifeInsured() {
        return lifeInsured;
    }

    public void setLifeInsured(String lifeInsured) {
        this.lifeInsured = lifeInsured;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDropoutPassword() {
        return dropoutPassword;
    }

    public void setDropoutPassword(String dropoutPassword) {
        this.dropoutPassword = dropoutPassword;
    }

    public int getNotifyStatus() {
        return notifyStatus;
    }

    public void setNotifyStatus(int notifyStatus) {
        this.notifyStatus = notifyStatus;
    }

    public int getMailOption() {
        return mailOption;
    }

    public void setMailOption(int mailOption) {
        this.mailOption = mailOption;
    }

    public String getApplicantResidency() {
        return applicantResidency;
    }

    public void setApplicantResidency(String applicantResidency) {
        this.applicantResidency = applicantResidency;
    }

    public String getApplicantFirstName() {
        return applicantFirstName;
    }

    public void setApplicantFirstName(String applicantFirstName) {
        this.applicantFirstName = applicantFirstName;
    }

    public String getApplicantLastName() {
        return applicantLastName;
    }

    public void setApplicantLastName(String applicantLastName) {
        this.applicantLastName = applicantLastName;
    }

    public Date getApplicantDob() {
        return applicantDob;
    }

    public void setApplicantDob(Date applicantDob) {
        this.applicantDob = applicantDob;
    }

    public String getApplicantGender() {
        return applicantGender;
    }

    public void setApplicantGender(String applicantGender) {
        this.applicantGender = applicantGender;
    }

    public String getApplicantMobileCountryCode() {
        return applicantMobileCountryCode;
    }

    public void setApplicantMobileCountryCode(String applicantMobileCountryCode) {
        this.applicantMobileCountryCode = applicantMobileCountryCode;
    }

    public String getApplicantMobileNum() {
        return applicantMobileNum;
    }

    public void setApplicantMobileNum(String applicantMobileNum) {
        this.applicantMobileNum = applicantMobileNum;
    }

    public String getApplicantEmail() {
        return applicantEmail;
    }

    public void setApplicantEmail(String applicantEmail) {
        this.applicantEmail = applicantEmail;
    }

    public String getApplicantNationality() {
        return applicantNationality;
    }

    public void setApplicantNationality(String applicantNationality) {
        this.applicantNationality = applicantNationality;
    }

    public Boolean getApplicantMultipleNationalities() {
        return applicantMultipleNationalities;
    }

    public void setApplicantMultipleNationalities(Boolean applicantMultipleNationalities) {
        this.applicantMultipleNationalities = applicantMultipleNationalities;
    }

    public String getApplicantNationality2() {
        return applicantNationality2;
    }

    public void setApplicantNationality2(String applicantNationality2) {
        this.applicantNationality2 = applicantNationality2;
    }

    public String getApplicantNationality3() {
        return applicantNationality3;
    }

    public void setApplicantNationality3(String applicantNationality3) {
        this.applicantNationality3 = applicantNationality3;
    }

    public String getApplicantBirthCountry() {
        return applicantBirthCountry;
    }

    public void setApplicantBirthCountry(String applicantBirthCountry) {
        this.applicantBirthCountry = applicantBirthCountry;
    }

    public String getApplicantNric() {
        return applicantNric;
    }

    public void setApplicantNric(String applicantNric) {
        this.applicantNric = applicantNric;
    }

    public Boolean getApplicantEnProficient() {
        return applicantEnProficient;
    }

    public void setApplicantEnProficient(Boolean applicantEnProficient) {
        this.applicantEnProficient = applicantEnProficient;
    }

    public String getApplicantEmployment() {
        return applicantEmployment;
    }

    public void setApplicantEmployment(String applicantEmployment) {
        this.applicantEmployment = applicantEmployment;
    }

    public Integer getApplicantIncome() {
        return applicantIncome;
    }

    public void setApplicantIncome(Integer applicantIncome) {
        this.applicantIncome = applicantIncome;
    }

    public Boolean getApplicantSmoker() {
        return applicantSmoker;
    }

    public void setApplicantSmoker(Boolean applicantSmoker) {
        this.applicantSmoker = applicantSmoker;
    }

    public String getApplicantOccupation() {
        return applicantOccupation;
    }

    public void setApplicantOccupation(String applicantOccupation) {
        this.applicantOccupation = applicantOccupation;
    }

    public String getApplicantIndustry() {
        return applicantIndustry;
    }

    public void setApplicantIndustry(String applicantIndustry) {
        this.applicantIndustry = applicantIndustry;
    }

    public String getApplicantCompanyName() {
        return applicantCompanyName;
    }

    public void setApplicantCompanyName(String applicantCompanyName) {
        this.applicantCompanyName = applicantCompanyName;
    }

    public String getApplicantCompanyCity() {
        return applicantCompanyCity;
    }

    public void setApplicantCompanyCity(String applicantCompanyCity) {
        this.applicantCompanyCity = applicantCompanyCity;
    }

    public String getApplicantCompanyCountry() {
        return applicantCompanyCountry;
    }

    public void setApplicantCompanyCountry(String applicantCompanyCountry) {
        this.applicantCompanyCountry = applicantCompanyCountry;
    }

    public Date getApplicantStudiesEndDate() {
        return applicantStudiesEndDate;
    }

    public void setApplicantStudiesEndDate(Date applicantStudiesEndDate) {
        this.applicantStudiesEndDate = applicantStudiesEndDate;
    }

    public String getApplicantAddressPostalCode() {
        return applicantAddressPostalCode;
    }

    public void setApplicantAddressPostalCode(String applicantAddressPostalCode) {
        this.applicantAddressPostalCode = applicantAddressPostalCode;
    }

    public String getApplicantAddressHouse() {
        return applicantAddressHouse;
    }

    public void setApplicantAddressHouse(String applicantAddressHouse) {
        this.applicantAddressHouse = applicantAddressHouse;
    }

    public String getApplicantAddressUnit() {
        return applicantAddressUnit;
    }

    public void setApplicantAddressUnit(String applicantAddressUnit) {
        this.applicantAddressUnit = applicantAddressUnit;
    }

    public String getApplicantAddressStreet() {
        return applicantAddressStreet;
    }

    public void setApplicantAddressStreet(String applicantAddressStreet) {
        this.applicantAddressStreet = applicantAddressStreet;
    }

    public String getApplicantAddressBuilding() {
        return applicantAddressBuilding;
    }

    public void setApplicantAddressBuilding(String applicantAddressBuilding) {
        this.applicantAddressBuilding = applicantAddressBuilding;
    }

    public Boolean getApplicantAddressIsRegistered() {
        return applicantAddressIsRegistered;
    }

    public void setApplicantAddressIsRegistered(Boolean applicantAddressRegistered) {
        this.applicantAddressIsRegistered = applicantAddressRegistered;
    }

    public Integer getApplicantAddressPeriodOfStay() {
        return applicantAddressPeriodOfStay;
    }

    public void setApplicantAddressPeriodOfStay(Integer applicantAddressPeriodOfStay) {
        this.applicantAddressPeriodOfStay = applicantAddressPeriodOfStay;
    }

    public String getApplicantPreviousAddressCountry() {
        return applicantPreviousAddressCountry;
    }

    public void setApplicantPreviousAddressCountry(String applicantPreviousAddressCountry) {
        this.applicantPreviousAddressCountry = applicantPreviousAddressCountry;
    }

    public Boolean getApplicantAddressIsPermanent() {
        return applicantAddressIsPermanent;
    }

    public void setApplicantAddressIsPermanent(Boolean applicantAddressIsPermanent) {
        this.applicantAddressIsPermanent = applicantAddressIsPermanent;
    }

    public String getApplicantPermanentAddressLine1() {
        return applicantPermanentAddressLine1;
    }

    public void setApplicantPermanentAddressLine1(String applicantPermanentAddressLine1) {
        this.applicantPermanentAddressLine1 = applicantPermanentAddressLine1;
    }

    public String getApplicantPermanentAddressLine2() {
        return applicantPermanentAddressLine2;
    }

    public void setApplicantPermanentAddressLine2(String applicantPermanentAddressLine2) {
        this.applicantPermanentAddressLine2 = applicantPermanentAddressLine2;
    }

    public String getApplicantPermanentAddressPostalCode() {
        return applicantPermanentAddressPostalCode;
    }

    public void setApplicantPermanentAddressPostalCode(String applicantPermanentAddressPostalCode) {
        this.applicantPermanentAddressPostalCode = applicantPermanentAddressPostalCode;
    }

    public String getApplicantPermanentAddressCountry() {
        return applicantPermanentAddressCountry;
    }

    public void setApplicantPermanentAddressCountry(String applicantPermanentAddressCountry) {
        this.applicantPermanentAddressCountry = applicantPermanentAddressCountry;
    }

    public String getApplicantTaxCountry1() {
        return applicantTaxCountry1;
    }

    public void setApplicantTaxCountry1(String applicantTaxCountry1) {
        this.applicantTaxCountry1 = applicantTaxCountry1;
    }

    public String getApplicantTaxCountry1Tin() {
        return applicantTaxCountry1Tin;
    }

    public void setApplicantTaxCountry1Tin(String applicantTaxCountry1Tin) {
        this.applicantTaxCountry1Tin = applicantTaxCountry1Tin;
    }

    public String getApplicantTaxCountry1NoTinReason() {
        return applicantTaxCountry1NoTinReason;
    }

    public void setApplicantTaxCountry1NoTinReason(String applicantTaxCountry1NoTinReason) {
        this.applicantTaxCountry1NoTinReason = applicantTaxCountry1NoTinReason;
    }

    public String getApplicantTaxCountry1NoTinReasonCustom() {
        return applicantTaxCountry1NoTinReasonCustom;
    }

    public void setApplicantTaxCountry1NoTinReasonCustom(String applicantTaxCountry1NoTinReasonCustom) {
        this.applicantTaxCountry1NoTinReasonCustom = applicantTaxCountry1NoTinReasonCustom;
    }

    public String getApplicantTaxCountry2() {
        return applicantTaxCountry2;
    }

    public void setApplicantTaxCountry2(String applicantTaxCountry2) {
        this.applicantTaxCountry2 = applicantTaxCountry2;
    }

    public String getApplicantTaxCountry2Tin() {
        return applicantTaxCountry2Tin;
    }

    public void setApplicantTaxCountry2Tin(String applicantTaxCountry2Tin) {
        this.applicantTaxCountry2Tin = applicantTaxCountry2Tin;
    }

    public String getApplicantTaxCountry2NoTinReason() {
        return applicantTaxCountry2NoTinReason;
    }

    public void setApplicantTaxCountry2NoTinReason(String applicantTaxCountry2NoTinReason) {
        this.applicantTaxCountry2NoTinReason = applicantTaxCountry2NoTinReason;
    }

    public String getApplicantTaxCountry2NoTinReasonCustom() {
        return applicantTaxCountry2NoTinReasonCustom;
    }

    public void setApplicantTaxCountry2NoTinReasonCustom(String applicantTaxCountry2NoTinReasonCustom) {
        this.applicantTaxCountry2NoTinReasonCustom = applicantTaxCountry2NoTinReasonCustom;
    }

    public String getApplicantTaxCountry3() {
        return applicantTaxCountry3;
    }

    public void setApplicantTaxCountry3(String applicantTaxCountry3) {
        this.applicantTaxCountry3 = applicantTaxCountry3;
    }

    public String getApplicantTaxCountry3Tin() {
        return applicantTaxCountry3Tin;
    }

    public void setApplicantTaxCountry3Tin(String applicantTaxCountry3Tin) {
        this.applicantTaxCountry3Tin = applicantTaxCountry3Tin;
    }

    public String getApplicantTaxCountry3NoTinReason() {
        return applicantTaxCountry3NoTinReason;
    }

    public void setApplicantTaxCountry3NoTinReason(String applicantTaxCountry3NoTinReason) {
        this.applicantTaxCountry3NoTinReason = applicantTaxCountry3NoTinReason;
    }

    public String getApplicantTaxCountry3NoTinReasonCustom() {
        return applicantTaxCountry3NoTinReasonCustom;
    }

    public void setApplicantTaxCountry3NoTinReasonCustom(String applicantTaxCountry3NoTinReasonCustom) {
        this.applicantTaxCountry3NoTinReasonCustom = applicantTaxCountry3NoTinReasonCustom;
    }

    public String getApplicantAddressNoTaxReason() {
        return applicantAddressNoTaxReason;
    }

    public void setApplicantAddressNoTaxReason(String applicantAddressNoTaxReason) {
        this.applicantAddressNoTaxReason = applicantAddressNoTaxReason;
    }

    public Boolean getApplicantIsHsbcCustomer() {
        return applicantIsHsbcCustomer;
    }

    public void setApplicantIsHsbcCustomer(Boolean applicantIsHsbcCustomer) {
        this.applicantIsHsbcCustomer = applicantIsHsbcCustomer;
    }

    public Boolean getApplicantIsPep() {
        return applicantIsPep;
    }

    public void setApplicantIsPep(Boolean applicantIsPep) {
        this.applicantIsPep = applicantIsPep;
    }

    public Boolean getApplicantIsBeneficalOwner() {
        return applicantIsBeneficalOwner;
    }

    public void setApplicantIsBeneficalOwner(Boolean applicantIsBeneficalOwner) {
        this.applicantIsBeneficalOwner = applicantIsBeneficalOwner;
    }

    public String getApplicantFullName() {
        return String.format("%s %s", getApplicantFirstName(), getApplicantLastName());
    }

    public int getApplicantAge() {
        Date dob = getApplicantDob();

        if (dob == null) {
            logger.error("getApplicantAge() called without applicant dob in form data");

            return 0;
        }
        return Period.between((new java.sql.Date(dob.getTime())).toLocalDate(), (new java.sql.Date((new Date()).getTime())).toLocalDate()).getYears();
    }

    public String getApplicantFullMobileNum() {
        return String.format("%s%s", getApplicantMobileCountryCode(), getApplicantMobileNum());
    }

    public String getInsuredResidency() {
        return insuredResidency;
    }

    public void setInsuredResidency(String insuredResidency) {
        this.insuredResidency = insuredResidency;
    }

    public String getInsuredFirstName() {
        return insuredFirstName;
    }

    public void setInsuredFirstName(String insuredFirstName) {
        this.insuredFirstName = insuredFirstName;
    }

    public String getInsuredLastName() {
        return insuredLastName;
    }

    public void setInsuredLastName(String insuredLastName) {
        this.insuredLastName = insuredLastName;
    }

    public Date getInsuredDob() {
        return insuredDob;
    }

    public void setInsuredDob(Date insuredDob) {
        this.insuredDob = insuredDob;
    }

    public String getInsuredGender() {
        return insuredGender;
    }

    public void setInsuredGender(String insuredGender) {
        this.insuredGender = insuredGender;
    }

    public String getInsuredNationality() {
        return insuredNationality;
    }

    public void setInsuredNationality(String insuredNationality) {
        this.insuredNationality = insuredNationality;
    }

    public Boolean getInsuredMultipleNationalities() {
        return insuredMultipleNationalities;
    }

    public void setInsuredMultipleNationalities(Boolean insuredMultipleNationalities) {
        this.insuredMultipleNationalities = insuredMultipleNationalities;
    }

    public String getInsuredNationality2() {
        return insuredNationality2;
    }

    public void setInsuredNationality2(String insuredNationality2) {
        this.insuredNationality2 = insuredNationality2;
    }

    public String getInsuredNationality3() {
        return insuredNationality3;
    }

    public void setInsuredNationality3(String insuredNationality3) {
        this.insuredNationality3 = insuredNationality3;
    }

    public String getInsuredNric() {
        return insuredNric;
    }

    public void setInsuredNric(String insuredNric) {
        this.insuredNric = insuredNric;
    }

    public String getInsuredEmployment() {
        return insuredEmployment;
    }

    public void setInsuredEmployment(String insuredEmployment) {
        this.insuredEmployment = insuredEmployment;
    }

    public Integer getInsuredIncome() {
        return insuredIncome;
    }

    public void setInsuredIncome(Integer insuredIncome) {
        this.insuredIncome = insuredIncome;
    }

    public Boolean getInsuredSmoker() {
        return insuredSmoker;
    }

    public void setInsuredSmoker(Boolean insuredSmoker) {
        this.insuredSmoker = insuredSmoker;
    }

    public float getExistingLifeInsuranceCoverage() {
        return existingLifeInsuranceCoverage;
    }

    public void setExistingLifeInsuranceCoverage(float existingLifeInsuranceCoverage) {
        this.existingLifeInsuranceCoverage = existingLifeInsuranceCoverage;
    }

    public Boolean getInsuredMedical1() {
        return insuredMedical1;
    }

    public void setInsuredMedical1(Boolean insuredMedical1) {
        this.insuredMedical1 = insuredMedical1;
    }

    public Boolean getInsuredMedical2() {
        return insuredMedical2;
    }

    public void setInsuredMedical2(Boolean insuredMedical2) {
        this.insuredMedical2 = insuredMedical2;
    }

    public Boolean getInsuredMedical3() {
        return insuredMedical3;
    }

    public void setInsuredMedical3(Boolean insuredMedical3) {
        this.insuredMedical3 = insuredMedical3;
    }

    public Boolean getInsuredMedical4() {
        return insuredMedical4;
    }

    public void setInsuredMedical4(Boolean insuredMedical4) {
        this.insuredMedical4 = insuredMedical4;
    }

    public String getInsuredOccupation() {
        return insuredOccupation;
    }

    public void setInsuredOccupation(String insuredOccupation) {
        this.insuredOccupation = insuredOccupation;
    }

    public String getInsuredIndustry() {
        return insuredIndustry;
    }

    public void setInsuredIndustry(String insuredIndustry) {
        this.insuredIndustry = insuredIndustry;
    }

    public String getInsuredCompanyName() {
        return insuredCompanyName;
    }

    public void setInsuredCompanyName(String insuredCompanyName) {
        this.insuredCompanyName = insuredCompanyName;
    }

    public Date getInsuredStudiesEndDate() {
        return insuredStudiesEndDate;
    }

    public void setInsuredStudiesEndDate(Date insuredStudiesEndDate) {
        this.insuredStudiesEndDate = insuredStudiesEndDate;
    }

    public String getInsuredFullName() {
        return String.format("%s %s", getInsuredFirstName(), getInsuredLastName());
    }

    public int getInsuredAge() {
        Date dob = getInsuredDob();

        return Period.between((new java.sql.Date(dob.getTime())).toLocalDate(), (new java.sql.Date((new Date()).getTime())).toLocalDate()).getYears();
    }

    public String getBeneficialOwnerFirstName() {
        return beneficialOwnerFirstName;
    }

    public void setBeneficialOwnerFirstName(String beneficialOwnerFirstName) {
        this.beneficialOwnerFirstName = beneficialOwnerFirstName;
    }

    public String getBeneficialOwnerLastName() {
        return beneficialOwnerLastName;
    }

    public void setBeneficialOwnerLastName(String beneficialOwnerLastName) {
        this.beneficialOwnerLastName = beneficialOwnerLastName;
    }

    public String getBeneficialOwnerNric() {
        return beneficialOwnerNric;
    }

    public void setBeneficialOwnerNric(String beneficialOwnerNric) {
        this.beneficialOwnerNric = beneficialOwnerNric;
    }

    public String getBeneficialOwnerRelation() {
        return beneficialOwnerRelation;
    }

    public void setBeneficialOwnerRelation(String beneficialOwnerRelation) {
        this.beneficialOwnerRelation = beneficialOwnerRelation;
    }

    public int getTermYears() {
        if (getApplicantDob() != null && getTermDuration() != null)
            switch (termDuration.toUpperCase()) {
                case "5Y": return 5;
                case "20Y": return 20;
                case "@65":
                    if (getApplicantAge() < 65)
                        return 65 - calculateAgeNextBirthday(getApplicantDob());
            }
        return ProductCalculation.MAXIMUM_TERM_YEARS;
    }

    public Float getSumAssured() {
        return sumAssured;
    }

    public void setSumAssured(Float sumAssured) {
        this.sumAssured = sumAssured;
    }

    public Float getRiderSumAssured() {
        return riderSumAssured;
    }

    public void setRiderSumAssured(Float riderSumAssured) {
        this.riderSumAssured = riderSumAssured;
    }

    public String getTermDuration() {
        return termDuration;
    }

    public void setTermDuration(String termDuration) {
        this.termDuration = termDuration;

        setProductCode(getProductCodeFromDurationCode(termDuration));

    }

    public int getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(int paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Boolean getIncludeCriticalIllnessRider() {
        return includeCriticalIllnessRider;
    }

    public void setIncludeCriticalIllnessRider(Boolean includeCriticalIllnessRider) {
        this.includeCriticalIllnessRider = includeCriticalIllnessRider;
    }

    public String getPaymentModeLabel() {
        return getPaymentModeLabel(getPaymentMode());
    }
    public String getPaymentModeLabelPdf() {
        String label = null;
        switch (getPaymentMode()) {
            case PAYMENT_MODE_ANNUALLY:
                label = "Annually";
                break;
            case PAYMENT_MODE_SEMI_ANNUALLY:
                label = "Semi-Annually";
                break;
            case PAYMENT_MODE_QUARTERLY:
                label = "Quarterly";
                break;
            case PAYMENT_MODE_MONTHLY:
                label = "Monthly";
                break;
        }

        return label;
    }


    public String getPaymentModeLabel(int paymentMode) {
        String label = null;
        switch (paymentMode) {
            case PAYMENT_MODE_ANNUALLY:
                label = "Annual";
                break;
            case PAYMENT_MODE_SEMI_ANNUALLY:
                label = "Semi-Annual";
                break;
            case PAYMENT_MODE_QUARTERLY:
                label = "Quarterly";
                break;
            case PAYMENT_MODE_MONTHLY:
                label = "Monthly";
                break;
        }

        return label;
    }

    public Boolean getUnderwritingLifestyleQuestionsOptin() {
        return underwritingLifestyleQuestionsOptin;
    }

    public void setUnderwritingLifestyleQuestionsOptin(Boolean underwritingLifestyleQuestionsOptin) {
        this.underwritingLifestyleQuestionsOptin = underwritingLifestyleQuestionsOptin;
    }

    public Integer getUnderwritingLifestyleMortgage() {
        return underwritingLifestyleMortgage;
    }

    public void setUnderwritingLifestyleMortgage(Integer underwritingLifestyleMortgage) {
        this.underwritingLifestyleMortgage = underwritingLifestyleMortgage;
    }

    public Integer getUnderwritingLifestyleProfile() {
        return underwritingLifestyleProfile;
    }

    public void setUnderwritingLifestyleProfile(Integer underwritingLifestyleProfile) {
        this.underwritingLifestyleProfile = underwritingLifestyleProfile;
    }

    public Integer getUnderwritingLifestyleHeight() {
        return underwritingLifestyleHeight;
    }

    public void setUnderwritingLifestyleHeight(Integer underwritingLifestyleHeight) {
        this.underwritingLifestyleHeight = underwritingLifestyleHeight;
    }

    public Integer getUnderwritingLifestyleWeight() {
        return underwritingLifestyleWeight;
    }

    public void setUnderwritingLifestyleWeight(Integer underwritingLifestyleWeight) {
        this.underwritingLifestyleWeight = underwritingLifestyleWeight;
    }

    public Integer getUnderwritingLifestyleExercise() {
        return underwritingLifestyleExercise;
    }

    public void setUnderwritingLifestyleExercise(Integer underwritingLifestyleExercise) {
        this.underwritingLifestyleExercise = underwritingLifestyleExercise;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getCreditCardName() {
        return creditCardName;
    }

    public void setCreditCardName(String creditCardName) {
        this.creditCardName = creditCardName;
    }

    public String getCreditCardNum() {
        return creditCardNum;
    }

    public void setCreditCardNum(String creditCardNum) {
        this.creditCardNum = creditCardNum;
    }

    public String getCreditCardExpiry() {
        return creditCardExpiry;
    }

    public void setCreditCardExpiry(String creditCardExpiry) {
        this.creditCardExpiry = creditCardExpiry;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public float getMaxStep() {
        return maxStep;
    }

    public void setMaxStep(float maxStep) {
        if (this.maxStep < maxStep) {
            this.maxStep = maxStep;
        }
    }

    public Integer getFeedbackExperience() {
        return feedbackExperience;
    }

    public void setFeedbackExperience(Integer feedbackExperience) {
        this.feedbackExperience = feedbackExperience;
    }

    public Integer getFeedbackEase() {
        return feedbackEase;
    }

    public void setFeedbackEase(Integer feedbackEase) {
        this.feedbackEase = feedbackEase;
    }

    public Boolean getFeedbackRecommend() {
        return feedbackRecommend;
    }

    public void setFeedbackRecommend(Boolean feedbackRecommend) {
        this.feedbackRecommend = feedbackRecommend;
    }

    public String getFeedbackNoRecommendReason() {
        return feedbackNoRecommendReason;
    }

    public void setFeedbackNoRecommendReason(String feedbackNoRecommendReason) {
        this.feedbackNoRecommendReason = feedbackNoRecommendReason;
    }

    public String getFeedbackImprove() {
        return feedbackImprove;
    }

    public void setFeedbackImprove(String feedbackImprove) {
        this.feedbackImprove = feedbackImprove;
    }

    public String getFeedbackAdditional() {
        return feedbackAdditional;
    }

    public void setFeedbackAdditional(String feedbackAdditional) {
        this.feedbackAdditional = feedbackAdditional;
    }



    public Boolean applicantIsWorking() {
        if (getApplicantEmployment() == null) {
            return false;
        }

        return (getApplicantEmployment().equals("Salaried") || getApplicantEmployment().equals("Self-employed"));
    }

    public Boolean applicantIsSingaporean() {
        if (getApplicantResidency() == null) {
            return false;
        }

        return (getApplicantResidency().equals("Singaporean"));
    }

    public Boolean applicantIsPermanentResident() {
        if (getApplicantResidency() == null) {
            return false;
        }

        return getApplicantResidency().equals("Singapore PR");
    }

    public Boolean applicantIsPassHolder() {
        if (getApplicantResidency() == null) {
            return false;
        }

        return Arrays.asList(VALID_PASS_TYPES).contains(getApplicantResidency());
    }

    public Boolean applicantIsStudying() {
        return ("Student".equals(getApplicantEmployment()));
    }

    public Boolean applicantIsSelfEmployed() {
        return ("Self-employed".equals(getApplicantEmployment()));
    }

    private static Boolean getBooleanFromResultSet(ResultSet rs, String columnLabel) {
        Boolean bool;
        try {
            bool = rs.getBoolean(columnLabel);
            if (rs.wasNull()) {
                bool = null;
            } else {
            }
        } catch (SQLException e) {
            bool = null;
        }

        return bool;
    }

    public int getUnderwritingClassPreferentialPoints() {
        int points = 0;

        if (isEligiblePreferentialRates() && getUnderwritingLifestyleQuestionsOptin() != null && getUnderwritingLifestyleQuestionsOptin()) {
            if (!getApplicantSmoker()) {
                points += 1;
            }

            float income = getApplicantIncome();
            if (income > 250000) {
                points += 2;
            } else {
                if (income > 150000) {
                    points += 1;
                }
            }

            if (getUnderwritingLifestyleMortgage()!= null) {
                switch (getUnderwritingLifestyleMortgage()) {
                    case UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_12_MONTHS:
                        points += 2;
                        break;
                    case UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_24_MONTHS:
                        points += 1;
                        break;
                }
            }

            if (getUnderwritingLifestyleProfile() != null) {
                switch (getUnderwritingLifestyleProfile()) {
                    case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_JADE:
                        points += 2;
                        break;
                    case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_PREMIER:
                        points += 1;
                        break;
                }
            }

            if (getUnderwritingLifestyleWeight() != null && getUnderwritingLifestyleHeight() != null) {
                int weight = getUnderwritingLifestyleWeight();
                int height = getUnderwritingLifestyleHeight();
                float bmi = weight * weight / height;
                if (bmi < 25) {
                    points += 2;
                } else {
                    if (bmi < 30) {
                        points += 1;
                    }
                }
            }

            if (getUnderwritingLifestyleExercise() != null) {
                switch (getUnderwritingLifestyleExercise()) {
                    case UNDERWRITING_LIFESTYLE_EXERCISE_DAILY:
                        points += 2;
                        break;
                    case UNDERWRITING_LIFESTYLE_EXERCISE_3_TIMES_WEEKLY:
                        points += 1;
                        break;
                }
            }
        }

        return points;
    }

    public int getUnderwritingClass() {
        int points = getUnderwritingClassPreferentialPoints();
        int underwritingClass = UNDERWRITING_CLASS_STANDARD;

        if (points >= 1 && points <= 6) {
            underwritingClass = UNDERWRITING_CLASS_STANDARD_PLUS;
        }
        //fixme >7 and smoker goes to standard class
        if (points >= 7 && points <= 9 && !getApplicantSmoker()) {
            underwritingClass = UNDERWRITING_CLASS_PREFERRED;
        }
        if (points >= 10 && !getApplicantSmoker()) {
            underwritingClass = UNDERWRITING_CLASS_PREFERRED_PLUS;
        }

        return underwritingClass;
    }

    public String getUnderwritingClassLabel() {
        String label = null;
        switch (getUnderwritingClass()) {
            case UNDERWRITING_CLASS_PREFERRED:
                label = "Preferred";
                break;
            case UNDERWRITING_CLASS_STANDARD:
                label = "Standard";
                break;
            case UNDERWRITING_CLASS_PREFERRED_PLUS:
                label = "Preferred Plus";
                break;
            case UNDERWRITING_CLASS_STANDARD_PLUS:
                label = "Standard Plus";
                break;
        }

        return label;
    }

    public String getReferenceNumComplexityFlag() {
        String flag = "S";

        if (isNationalitySpecialCase() ||
                isIndustrySpecialCase() ||
                Boolean.TRUE.equals(getInsuredMedical1()) ||
                Boolean.TRUE.equals(getInsuredMedical2()) ||
                Boolean.TRUE.equals(getInsuredMedical3()) ||
                Boolean.TRUE.equals(getInsuredMedical4()) ||
                (getApplicantIsPep() != null && getApplicantIsPep())) {
            flag = "C";
        }

        return flag;
    }

    public String getReferenceNumMailOptionFlag() {
        String flag = "E";

        if (getMailOption() == MAIL_OPTION_MAILOUT) {
            flag = "M";
        }

        return flag;
    }
    
    public String getGenderDescription(){
        if("M".equals(getApplicantGender()))
            return "Male";
        else if("F".equals(getApplicantGender()))
            return "Female";
        else
            return getApplicantGender();
    }

    private static Integer getIntegerFromResultSet(ResultSet rs, String columnLabel) {
        Integer integer;
        try {
            integer = rs.getInt(columnLabel);
            if (rs.wasNull()) {
                integer = null;
            } else {
            }
        } catch (SQLException e) {
            integer = null;
        }

        return integer;
    }

    private static Float getFloatFromResultSet(ResultSet rs, String columnLabel) {
        Float num;
        try {
            num = rs.getFloat(columnLabel);
            if (rs.wasNull()) {
                num = null;
            } else {
            }
        } catch (SQLException e) {
            num = null;
        }

        return num;
    }

    public String getInsuredPossessiveLabel(boolean useName) {
        String label = null;

        if (useName) {
            label = String.format("%s's", getInsuredFirstName());
        }

        if (label == null) {
            switch (getInsuredGender()) {
                case "M":
                    label = "his";
                    break;
                case "F":
                    label = "her";
                    break;
            }
        }

        return label;
    }

    public String getInsuredPronounLabel() {
        String label = null;

        switch (getInsuredGender()) {
            case "M":
                label = "he";
                break;
            case "F":
                label = "she";
                break;
        }

        return label;
    }

    public String getUnderwritingLifestyleMortgageLabel() {
        String label = null;

        switch (getUnderwritingLifestyleMortgage()) {
            case UNDERWRITING_LIFESTYLE_MORTGAGE_NA:
                label = "Not applicable";
                break;
            case UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_12_MONTHS:
                label = "In the last 12 months";
                break;
            case UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_24_MONTHS:
                label = "In the last 24 months";
                break;
            case UNDERWRITING_LIFESTYLE_MORTGAGE_MORE_THAN_24_MONTHS:
                label = "More than 24 months ago";
                break;
        }

        return label;
    }

    public String getUnderwritingLifestyleProfileLabel() {
        String label = null;

        switch (getUnderwritingLifestyleProfile()) {
            case UNDERWRITING_LIFESTYLE_PROFILE_NON_HSBC:
                label = "Non-HSBC";
                break;
            case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_JADE:
                label = "HSBC Jade or Private Banking";
                break;
            case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_PREMIER:
                label = "HSBC Premier";
                break;
            case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_ADVANCE:
                label = "HSBC Advance";
                break;
            case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_OTHERS:
                label = "HSBC Others";
                break;
        }

        return label;
    }

    public String getUnderwritingLifestyleExerciseLabel() {
        String label = null;

        switch (getUnderwritingLifestyleExercise()) {
            case UNDERWRITING_LIFESTYLE_EXERCISE_NA:
                label = "Not applicable";
                break;
            case UNDERWRITING_LIFESTYLE_EXERCISE_DAILY:
                label = "Daily (for at least 30 minutes each time)";
                break;
            case UNDERWRITING_LIFESTYLE_EXERCISE_3_TIMES_WEEKLY:
                label = "3 times a week";
                break;
            case UNDERWRITING_LIFESTYLE_EXERCISE_WEEKEND:
                label = "Every weekend";
                break;
        }

        return label;
    }

    public int getLastAgeBaseCover(){
        return getLastAgeBaseCover(getProductCode());
    }

    /**
     * Get the max age of product coverage based on product code
     * @param productCode
     * @return max age
     */
    public int getLastAgeBaseCover(String productCode){
        return getLastAgeCover(productCode);
    }

    public int getLastAgeRiderCover(){
        return getLastAgeRiderCover(getProductCode());
    }

    /**
     * Get the max age of product coverage based on rider code
     * @param productCode
     * @return max age
     */
    public int getLastAgeRiderCover(String productCode){
        return getLastAgeCover(productCode);
    }

    /**
     * Get the max age of rider coverage
     * @return max age
     */
    public int getLastAgeCover(String productCode){
        if (productCode != null) {
            switch (productCode){
                case DVT_5_YEARS_PRODUCT_CODE:
                    return 85;
                case DVT_20_YEARS_PRODUCT_CODE:
                    return calculateAgeNextBirthday(getApplicantDob())+20;
                case DVT_AGE_65_PRODUCT_CODE://Age 65
                    return 65;
            }
        }
        return 85;
    }

    public String getCreditCardTypeLabel() {
        String label = "";

        switch (getCreditCardType()) {
            case "V":
                label = "VISA";
                break;
            case "M":
                label = "MasterCard";
                break;
        }

        return label;
    }

    public String getApplicantTaxCountryNoTinReasonLabel(String reasonType) {
        String label = "";

        switch (reasonType) {
            case "A":
                label = "A: The country where you are liable to pay tax does not issue TINs to its residents";
                break;
            case "B":
                label = "B: You are otherwise unable to obtain a TIN or equivalent number.";
                break;
            case "C":
                label = "C: No TIN is required.";
                break;
        }

        return label;
    }

    public static int calculateAgeNextBirthday(Date dob) {
        int age = 0;

        if (dob == null) return age;

        Calendar dobCal = Calendar.getInstance(Locale.US);
        dobCal.setTime(dob);

        Date now = new Date();
        Calendar nowCal = Calendar.getInstance(Locale.US);
        nowCal.setTime(now);
        nowCal.set(Calendar.HOUR_OF_DAY, 0);
        nowCal.set(Calendar.MINUTE, 0);
        nowCal.set(Calendar.SECOND, 0);
        nowCal.set(Calendar.MILLISECOND, 0);
        now = nowCal.getTime();

        Date dobThisYear = null;
        String dateString = String.format("%s/%s/%s", dobCal.get(Calendar.DAY_OF_MONTH), dobCal.get(Calendar.MONTH) + 1, nowCal.get(Calendar.YEAR));
        DateFormat df = new SimpleDateFormat("d/M/y");
        try {
            dobThisYear = df.parse(dateString);
        } catch (Exception e) {
        }

        if (now.after(dobThisYear)) {
            age = nowCal.get(Calendar.YEAR) - dobCal.get(Calendar.YEAR) + 1;
        } else {
            age = nowCal.get(Calendar.YEAR) - dobCal.get(Calendar.YEAR);
        }

        return age;
    }

    public static float getMinSumAssured() {
        return 50000f;
    }

    public static float getMaxSumAssured() {
        return 400000f;
    }

    public static float getMinRiderSumAssured() {
        return 50000f;
    }

    public static float getMaxRiderSumAssured() {
        return 400000f;
    }

    public boolean isEligiblePreferentialRates() {
        return  !(
                Boolean.TRUE.equals(getInsuredMedical1())
                        || Boolean.TRUE.equals(getInsuredMedical2())
                        || Boolean.TRUE.equals(getInsuredMedical3())
                        || Boolean.TRUE.equals(getInsuredMedical4())
        );
//        return (getInsuredMedical1() != null && !(getInsuredMedical1() || getInsuredMedical2() || getInsuredMedical3() || getInsuredMedical4()));
    }

    public boolean isNationalitySpecialCase() {
        boolean isSpecialCase = false;

        for (String nationality: COMPLEX_NATIONALITIES) {
            if(nationality.equalsIgnoreCase(getApplicantNationality())
                    || nationality.equalsIgnoreCase(getApplicantNationality2())
                    || nationality.equalsIgnoreCase(getApplicantNationality3()))
                isSpecialCase = true;
        }

        return isSpecialCase;
    }

    public boolean isIndustrySpecialCase() {
        boolean isSpecialCase = false;

        for (String industry: COMPLEX_INDUSTRIES) {
            if(industry.equalsIgnoreCase(getApplicantIndustry()))
                isSpecialCase = true;
        }

        return isSpecialCase;
    }
    
    /**
     * Getter method to return the email sent date
     * @return email sent date
     */
    public Date getEmailSentDate() {
		return emailSentDate;
	}

    /**
     * Setter method to set the email sent date
     * @param emailSentDate date which email was sent
     */
      public void setEmailSentDate(Date emailSentDate) {
        this.emailSentDate = emailSentDate;
      }

      
    /**
     * This is to indicate whether feedback has been done.
     * @return true if user has done the feedback, else false
     */
    public boolean isFeedbackCompleted() {
        boolean isFeedback = false;

        getFeedback();
        
        if (getFeedbackExperience() != null && getFeedbackEase() != null && getFeedbackRecommend() != null && 
        		StringUtils.isNotBlank(getFeedbackImprove()) && StringUtils.isNotBlank(getFeedbackAdditional())) {
        	isFeedback = true;
        }

        return isFeedback;
    }
    
    /**
     * This function will retrieve the feedback data from db and populate the feedback portion of the application object
     * @return void
     */
    private void getFeedback() {
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM feedback WHERE application_id = ?");
            stmt.setString(1, getId());
            rs = stmt.executeQuery();
            if (rs.next()) {
            	setFeedbackExperience(rs.getInt("feedback_experience"));
            	setFeedbackEase(rs.getInt("feedback_ease"));
            	setFeedbackRecommend(getBooleanFromResultSet(rs, "feedback_recommend"));
            	setFeedbackNoRecommendReason(rs.getString("feedback_norecommend_reason"));
            	setFeedbackImprove(rs.getString("feedback_improve"));
            	setFeedbackAdditional(rs.getString("feedback_additional"));
            }
        } catch (Exception e) {
             logger.error(e.getMessage());
        }finally {           
			try {
				if (rs != null) {
	                rs.close();
	                rs = null;
	            }
	            if (stmt != null) {
	                stmt.close();
	                stmt = null;
	            }
	            if (conn != null) {
	                conn.close();
	                conn = null;
	            }
			} catch (Exception e) {
				logger.error("getFeedBack:",e);
			}         
        }
    }
    

    @Override
    public String toString() {
        return "DirectValueTermApplication{" +
                "df=" + df +
                ", id='" + id + '\'' +
                ", referenceNum='" + referenceNum + '\'' +
                ", campaignCode='" + campaignCode + '\'' +
                ", agreePrivacy=" + agreePrivacy +
                ", agreeImportantNotes=" + agreeImportantNotes +
                ", lifeInsured='" + lifeInsured + '\'' +
                ", status=" + status +
                ", dropoutPassword='" + dropoutPassword + '\'' +
                ", notifyStatus=" + notifyStatus +
                ", mailOption=" + mailOption +
                ", applicantResidency='" + applicantResidency + '\'' +
                ", applicantFirstName='" + applicantFirstName + '\'' +
                ", applicantLastName='" + applicantLastName + '\'' +
                ", applicantDob=" + applicantDob +
                ", applicantGender='" + applicantGender + '\'' +
                ", applicantMobileCountryCode='" + applicantMobileCountryCode + '\'' +
                ", applicantMobileNum='" + applicantMobileNum + '\'' +
                ", applicantEmail='" + applicantEmail + '\'' +
                ", applicantNationality='" + applicantNationality + '\'' +
                ", applicantMultipleNationalities=" + applicantMultipleNationalities +
                ", applicantNationality2='" + applicantNationality2 + '\'' +
                ", applicantNationality3='" + applicantNationality3 + '\'' +
                ", applicantBirthCountry='" + applicantBirthCountry + '\'' +
                ", applicantNric='" + applicantNric + '\'' +
                ", applicantEnProficient=" + applicantEnProficient +
                ", applicantEmployment='" + applicantEmployment + '\'' +
                ", applicantIncome=" + applicantIncome +
                ", applicantSmoker=" + applicantSmoker +
                ", applicantOccupation='" + applicantOccupation + '\'' +
                ", applicantIndustry='" + applicantIndustry + '\'' +
                ", applicantCompanyName='" + applicantCompanyName + '\'' +
                ", applicantCompanyCity='" + applicantCompanyCity + '\'' +
                ", applicantCompanyCountry='" + applicantCompanyCountry + '\'' +
                ", applicantStudiesEndDate=" + applicantStudiesEndDate +
                ", applicantAddressPostalCode='" + applicantAddressPostalCode + '\'' +
                ", applicantAddressHouse='" + applicantAddressHouse + '\'' +
                ", applicantAddressUnit='" + applicantAddressUnit + '\'' +
                ", applicantAddressStreet='" + applicantAddressStreet + '\'' +
                ", applicantAddressBuilding='" + applicantAddressBuilding + '\'' +
                ", applicantAddressIsRegistered=" + applicantAddressIsRegistered +
                ", applicantAddressPeriodOfStay=" + applicantAddressPeriodOfStay +
                ", applicantPreviousAddressCountry='" + applicantPreviousAddressCountry + '\'' +
                ", applicantAddressIsPermanent=" + applicantAddressIsPermanent +
                ", applicantPermanentAddressLine1='" + applicantPermanentAddressLine1 + '\'' +
                ", applicantPermanentAddressLine2='" + applicantPermanentAddressLine2 + '\'' +
                ", applicantPermanentAddressPostalCode='" + applicantPermanentAddressPostalCode + '\'' +
                ", applicantPermanentAddressCountry='" + applicantPermanentAddressCountry + '\'' +
                ", applicantTaxCountry1='" + applicantTaxCountry1 + '\'' +
                ", applicantTaxCountry1Tin='" + applicantTaxCountry1Tin + '\'' +
                ", applicantTaxCountry1NoTinReason='" + applicantTaxCountry1NoTinReason + '\'' +
                ", applicantTaxCountry1NoTinReasonCustom='" + applicantTaxCountry1NoTinReasonCustom + '\'' +
                ", applicantTaxCountry2='" + applicantTaxCountry2 + '\'' +
                ", applicantTaxCountry2Tin='" + applicantTaxCountry2Tin + '\'' +
                ", applicantTaxCountry2NoTinReason='" + applicantTaxCountry2NoTinReason + '\'' +
                ", applicantTaxCountry2NoTinReasonCustom='" + applicantTaxCountry2NoTinReasonCustom + '\'' +
                ", applicantTaxCountry3='" + applicantTaxCountry3 + '\'' +
                ", applicantTaxCountry3Tin='" + applicantTaxCountry3Tin + '\'' +
                ", applicantTaxCountry3NoTinReason='" + applicantTaxCountry3NoTinReason + '\'' +
                ", applicantTaxCountry3NoTinReasonCustom='" + applicantTaxCountry3NoTinReasonCustom + '\'' +
                ", applicantAddressNoTaxReason='" + applicantAddressNoTaxReason + '\'' +
                ", applicantIsHsbcCustomer=" + applicantIsHsbcCustomer +
                ", applicantIsPep=" + applicantIsPep +
                ", applicantIsBeneficalOwner=" + applicantIsBeneficalOwner +
                ", insuredResidency='" + insuredResidency + '\'' +
                ", insuredFirstName='" + insuredFirstName + '\'' +
                ", insuredLastName='" + insuredLastName + '\'' +
                ", insuredDob=" + insuredDob +
                ", insuredGender='" + insuredGender + '\'' +
                ", insuredNationality='" + insuredNationality + '\'' +
                ", insuredMultipleNationalities=" + insuredMultipleNationalities +
                ", insuredNationality2='" + insuredNationality2 + '\'' +
                ", insuredNationality3='" + insuredNationality3 + '\'' +
                ", insuredNric='" + insuredNric + '\'' +
                ", insuredEmployment='" + insuredEmployment + '\'' +
                ", insuredIncome=" + insuredIncome +
                ", insuredSmoker=" + insuredSmoker +
                ", existingLifeInsuranceCoverage=" + existingLifeInsuranceCoverage +
                ", insuredMedical1=" + insuredMedical1 +
                ", insuredMedical2=" + insuredMedical2 +
                ", insuredMedical3=" + insuredMedical3 +
                ", insuredMedical4=" + insuredMedical4 +
                ", insuredOccupation='" + insuredOccupation + '\'' +
                ", insuredIndustry='" + insuredIndustry + '\'' +
                ", insuredCompanyName='" + insuredCompanyName + '\'' +
                ", insuredStudiesEndDate=" + insuredStudiesEndDate +
                ", beneficialOwnerFirstName='" + beneficialOwnerFirstName + '\'' +
                ", beneficialOwnerLastName='" + beneficialOwnerLastName + '\'' +
                ", beneficialOwnerNric='" + beneficialOwnerNric + '\'' +
                ", beneficialOwnerRelation='" + beneficialOwnerRelation + '\'' +
                ", termDuration='" + termDuration + '\'' +
                ", productCode='" + productCode + '\'' +
                ", sumAssured=" + sumAssured +
                ", riderSumAssured=" + riderSumAssured +
                ", paymentMode=" + paymentMode +
                ", includeCriticalIllnessRider=" + includeCriticalIllnessRider +
                ", includeRefundRider=" + includeRefundRider +
                ", underwritingLifestyleQuestionsOptin=" + underwritingLifestyleQuestionsOptin +
                ", underwritingLifestyleMortgage=" + underwritingLifestyleMortgage +
                ", underwritingLifestyleProfile=" + underwritingLifestyleProfile +
                ", underwritingLifestyleHeight=" + underwritingLifestyleHeight +
                ", underwritingLifestyleWeight=" + underwritingLifestyleWeight +
                ", underwritingLifestyleExercise=" + underwritingLifestyleExercise +
                ", creditCardType='" + creditCardType + '\'' +
                ", creditCardName='" + creditCardName + '\'' +
                ", creditCardNum='" + creditCardNum + '\'' +
                ", creditCardExpiry='" + creditCardExpiry + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateUpdated=" + dateUpdated +
                ", maxStep=" + maxStep +
                ", feedbackExperience=" + feedbackExperience +
                ", feedbackEase=" + feedbackEase +
                ", feedbackRecommend=" + feedbackRecommend +
                ", feedbackNoRecommendReason='" + feedbackNoRecommendReason + '\'' +
                ", feedbackImprove='" + feedbackImprove + '\'' +
                ", feedbackAdditional='" + feedbackAdditional + '\'' +
                '}';
    }

	/**
	 * Setting the value for marketing consent
	 * 
	 * @param b True if user has opted for news feeds
	 */
	public void setMarketingConsent(boolean b) {
		this.marketingConsent=b;
		
	}
	/**
	 * @return Boolean the marketing consent opted 
	 */
	public Boolean getMarketingConsent(){
		return this.marketingConsent;
	}
}
