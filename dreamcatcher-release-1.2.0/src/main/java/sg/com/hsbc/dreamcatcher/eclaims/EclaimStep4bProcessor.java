package sg.com.hsbc.dreamcatcher.eclaims;

import sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident.Step4bServlet;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


public class EclaimStep4bProcessor {
    private final static Logger LOGGER = Logger.getLogger(Step4bServlet.class.getName());

    private EClaimBaseServlet servletInstance;

    public EclaimStep4bProcessor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizePayment(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizePayment(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("bank_name");
        sanitized.put("bank_name", input);
        input = request.getParameter("account_number");
        sanitized.put("account_number", input);
        input = request.getParameter("bank_country");
        sanitized.put("bank_country", input);
        if (!input.equals("Singapore")) {
            sanitized = this.sanitizeBankAddress(request, sanitized);
        }

        servletInstance.sanitizeUploadedDocument(request, sanitized, "account_document", "AccountDocument");

        return sanitized;
    }

    private Map<String, Object> sanitizeBankAddress(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("bank_city");
        sanitized.put("bank_city", input);
        input = request.getParameter("bank_postal_code");
        sanitized.put("bank_postal_code", input);
        input = request.getParameter("bank_building_number");
        sanitized.put("bank_building_number", input);
        input = request.getParameter("bank_unit_number_1");
        sanitized.put("bank_unit_number_1", input);
        input = request.getParameter("bank_unit_number_2");
        sanitized.put("bank_unit_number_2", input);
        input = request.getParameter("bank_street_1");
        sanitized.put("bank_street_1", input);
        input = request.getParameter("bank_street_2");
        sanitized.put("bank_street_2", input);
        input = request.getParameter("bank_building_name");
        sanitized.put("bank_building_name", input);
        input = request.getParameter("bank_swift_code");
        sanitized.put("bank_swift_code", input);

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");
        application.setBankName(data.get("bank_name").toString());
        application.setAccountNumber(data.get("account_number").toString());
        application.setBankCountry(data.get("bank_country").toString());
        if (!application.getBankCountry().equals("Singapore")) {
            application.setBankCity(data.get("bank_city").toString());
            application.setBankPostalCode(data.get("bank_postal_code").toString());
            application.setBankBuildingNumber(data.get("bank_building_number").toString());
            application.setBankUnitNumber1(data.get("bank_unit_number_1").toString());
            application.setBankUnitNumber2(data.get("bank_unit_number_2").toString());
            application.setBankStreet1(data.get("bank_street_1").toString());
            application.setBankStreet2(data.get("bank_street_2").toString());
            application.setBankBuildingName(data.get("bank_building_name").toString());
            application.setBankSwiftCode(data.get("bank_swift_code").toString());
        }
        application.setAccountDocument(servletInstance.docList(request, data, "account_document", "AccountDocument"));
       
        /** 10MB File size implementation starts here **/ 
        if(application.getFileInfo()!=null){
          Map<String, Long>updatedFileInfo=application.getFileInfo();
          if(data.get("fileInfo")!=null) {
          updatedFileInfo.putAll((Map<String,Long>)data.get("fileInfo"));
          application.setFileInfo(updatedFileInfo);
          }
        }
        else {
          application.setFileInfo((Map<String,Long>)data.get("fileInfo")); 
        }
        /** 10MB File size implementation ends here **/ 
        
        application.setMaxStep(servletInstance.getCurrentStep() + (float)0.5);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("bank_name", application.getBankName());
        data.put("account_number", application.getAccountNumber());
        data.put("bank_country", application.getBankCountry());
        data.put("account_document", application.getAccountDocument());
        if (application.getBankCountry() != null && !application.getBankCountry().equals("Singapore")) {
            data.put("bank_city", application.getBankCity());
            data.put("bank_postal_code", application.getBankPostalCode());
            data.put("bank_building_number", application.getBankBuildingNumber());
            data.put("bank_unit_number_1", application.getBankUnitNumber1());
            data.put("bank_unit_number_2", application.getBankUnitNumber2());
            data.put("bank_street_1", application.getBankStreet1());
            data.put("bank_street_2", application.getBankStreet2());
            data.put("bank_building_name", application.getBankBuildingName());
            data.put("bank_swift_code", application.getBankSwiftCode());
        }

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("country_list", servletInstance.getCountryList());

        request.setAttribute("formData", data);
    }
}
