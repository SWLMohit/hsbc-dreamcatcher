package sg.com.hsbc.dreamcatcher.eclaims.deathduetoillness;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplicationJob;

@WebServlet(urlPatterns = "/eclaims/2-death-due-to-illness/5a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/2_death_due_to_illness/step5b.jsp")
}, asyncSupported = true)
public class Step5bServlet extends BaseServlet {
	 private final static Logger logger = Logger.getLogger(Step5bServlet.class);
    public float getCurrentStep() {
        return (float) 5.5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        return true;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DeathDueToIllnessApplication application = getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        data.put("referenceNumber", application.getReferenceNumber());
        data.put("fileSizeError", application.fileSizeExceeded()); //10MB message
        request.setAttribute("data", data);

        removeFromSession(request);
        // NB see comments in EClaimApplicationJob
        ExecutorService threadPool = (ExecutorService) getServletConfig().getServletContext().getAttribute("threadPool");
        threadPool.execute(new EClaimApplicationJob(application));

    }
}
