package sg.com.hsbc.dreamcatcher.helpers;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.EncryptRequest;
import com.amazonaws.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextEncryptor {

    private static final String EMPTY_STRING = "";
    static private Logger logger = LoggerFactory.getLogger(TextEncryptor.class);
    private final AWSKMS kms;
    private final String kmsKeyId;

    public  TextEncryptor() {
        this.kms = AWSKMSClientBuilder.standard().withRegion(Regions.AP_SOUTHEAST_1).build();
        this.kmsKeyId = Config.getString("dreamcatcher.kms.key");
    }

    public  String encrypt(final String text) {
        //TODO fix get the kms from AWS error
        if (text == null || text.isEmpty()) {
            return EMPTY_STRING;
        } else {
            try {
                final EncryptRequest encryptRequest =
                        new EncryptRequest().withKeyId(kmsKeyId) //
                                .withPlaintext(ByteBuffer.wrap(text.getBytes()));

                final ByteBuffer encryptedBytes = kms.encrypt(encryptRequest).getCiphertextBlob();
                return extractString(ByteBuffer.wrap(Base64.encode(encryptedBytes.array())));
            } catch (Exception ex) {
                logger.error("use kms to encrypt {} got error {}", text, ex.getMessage());
                return EMPTY_STRING;
            }
        }
    }

    public  String decrypt(final String encryptedText) {
        if (encryptedText == null || encryptedText.isEmpty()) {
            return EMPTY_STRING;
        } else {

            // Extract the encryption context and the remaining part
            final Map<String, String> encryptionContext = extractEncryptionContext(encryptedText);
            final String encryptedValue = extractEncryptedValue(encryptedText);

            // Assuming the encryptedText is encoded in Base64
            final ByteBuffer encryptedBytes =
                    ByteBuffer.wrap(Base64.decode(encryptedValue.getBytes()));

            final DecryptRequest decryptRequest = new DecryptRequest()
                    .withCiphertextBlob(encryptedBytes)
                    .withEncryptionContext(encryptionContext);

            return extractString(kms.decrypt(decryptRequest).getPlaintext());
        }
    }

    public static Map<String, String> extractEncryptionContext(final String encryptedText) {
        final int lower = encryptedText.indexOf('(');
        final int upper= encryptedText.indexOf(')');
        if (lower != 0 || upper < 0) {
            return Collections.emptyMap();
        } else {
            final Map<String, String> encryptionContext = new HashMap<>();
            final String encryptionContextText = encryptedText.substring(lower+1, upper);
            final String[] pairs = encryptionContextText.split(",");
            for (String pair : pairs) {
                // we must not use simply split("="), as = is a pad symbol in base64, and would be cut out...
                String[] keyValue = pair.split("=", 2);
                if (keyValue.length == 1) {
                    encryptionContext.put(keyValue[0], "");
                } else if (keyValue.length == 2) {
                    encryptionContext.put(keyValue[0], new String(Base64.decode(keyValue[1].trim().getBytes())));
                }
            }
            return encryptionContext;
        }
    }

    public static  String extractEncryptedValue(final String encryptedText) {
        final int index = encryptedText.lastIndexOf(')');
        if (index > 0) {
            return encryptedText.substring(index + 1);
        }
        return encryptedText;
    }

    private static  String extractString(final ByteBuffer bb) {
        if (bb.hasRemaining()) {
            final byte[] bytes = new byte[bb.remaining()];
            bb.get(bytes, bb.arrayOffset(), bb.remaining());
            return new String(bytes);
        } else {
            return EMPTY_STRING;
        }
    }
}
