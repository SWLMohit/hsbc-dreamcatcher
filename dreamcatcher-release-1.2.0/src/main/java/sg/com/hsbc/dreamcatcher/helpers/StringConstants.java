package sg.com.hsbc.dreamcatcher.helpers;

/**
 * Created by IntelliJ IDEA.
 * User: keilin.olsen@heathwallace.com
 * Date: 19/11/2017
 * Time: 3:38 PM
 * Project: hsbc-dreamcatcher-jsp
 **/
public class StringConstants {
    public static final String UNABLE_TO_PROCEED_WITH_ONLINE_APPLICATION = "We are unable to proceed with your online application.";
    public static final String DATE_OF_BIRTH_INVALID = "Your date of birth is invalid.";
    public static final String DATE_OF_BIRTH_MISSING = "Your date of birth is missing.";
    public static final String CURRENT_RESIDENCY_STATUS_MISSING_OR_INVALID = "Your current residency status is missing or invalid.";
    public static final String CURRENT_RESIDENCY_STATUS_INVALID = "Your current residency status is invalid.";
    public static final String GIVEN_NAME_MISSING = "Your given name is missing.";
    public static final String GIVEN_NAME_TOO_LONG = "Your given name is too long.";
    public static final String GIVEN_NAME_CONTAINS_INVALID_CHARACTERS = "Your given name contains invalid characters.";
    public static final String SURNAME_MISSING = "Your surname is missing.";
    public static final String SURNAME_TOO_LONG = "Your surname is too long.";
    public static final String SURNAME_CONTAINS_INVALID_CHARACTERS = "Your surname contains invalid characters.";
    public static final String GENDER_MISSING_OR_INVALID = "Your gender is missing or invalid.";
    public static final String OP_VALID_AGE = "Eligible for customers between age next birthday 19 to 60";
    public static final String DVT_VALID_AGE = "Eligible for customers between age next birthday 19 to 62";
    public static final String EMPTY_DATE = "Please enter a valid date";
}
