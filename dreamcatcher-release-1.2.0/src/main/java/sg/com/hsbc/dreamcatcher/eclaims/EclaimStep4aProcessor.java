package sg.com.hsbc.dreamcatcher.eclaims;

import sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident.DeathDueToAccidentApplication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;


public class EclaimStep4aProcessor {
    private EClaimBaseServlet servletInstance;

    public EclaimStep4aProcessor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizePayment(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizePayment(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("payment");
        sanitized.put("payment", Integer.parseInt(input));

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");
        application.setPayment((int)data.get("payment"));

        if (application.getPayment() == DeathDueToAccidentApplication.PAYMENT_TRANSFER_TO_BANK_ACCOUNT) {
            application.setMaxStep(servletInstance.getCurrentStep() + (float)0.5);
        } else {
            application.setMaxStep(servletInstance.getCurrentStep() + (float)1);
        }
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("payment", application.getPayment());

        request.setAttribute("data", data);
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        EClaimApplication application = servletInstance.getApplication(request);
        if (application.getPayment() == DeathDueToAccidentApplication.PAYMENT_TRANSFER_TO_BANK_ACCOUNT) {
            response.sendRedirect(servletInstance.getBaseFormUrl() + "/4a");
        } else {
            response.sendRedirect(servletInstance.getBaseFormUrl() + "/5");
        }
    }
}
