package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoillness;

import be.quodlibet.boxable.BaseTable;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;
import sg.com.hsbc.dreamcatcher.eclaims.DoctorPortion;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.eclaims.TpdApplication;
import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

import java.io.IOException;
import java.text.DecimalFormat;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TpdDueToIllnessApplication extends TpdApplication {
	private final static Logger logger = Logger.getLogger(TpdDueToIllnessApplication.class);
    protected Date illnessSymptomsStart;
    protected DoctorPortion[] doctorList;
    protected String medicalReport;
    protected String clinicalAbtractForm;

    public TpdDueToIllnessApplication() {
        claimsType = "T";
        claimsTitle = "TPD Due to Illness";
    }

    public static TpdDueToIllnessApplication getById(String id) throws SQLException {
        TpdDueToIllnessApplication application = new TpdDueToIllnessApplication();
        application.load(id);

        return application;
    }

    protected void initializeListField() {
        super.initializeListField();

        this.doctorList = new DoctorPortion[1];
        this.doctorList[0] = new DoctorPortion();
    }

    public Date getIllnessSymptomsStart() {
        return illnessSymptomsStart;
    }

    public void setIllnessSymptomsStart(Date illnessSymptomsStart) {
        this.illnessSymptomsStart = illnessSymptomsStart;
    }

    public DoctorPortion[] getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(DoctorPortion[] doctorList) {
        this.doctorList = doctorList;
    }

    public String getMedicalReport() {
        return medicalReport;
    }

    public void setMedicalReport(String medicalReport) {
        this.medicalReport = medicalReport;
    }

    public String getClinicalAbtractForm() {
        return clinicalAbtractForm;
    }

    public void setClinicalAbtractForm(String clinicalAbtractForm) {
        this.clinicalAbtractForm = clinicalAbtractForm;
    }

    @Override
    public void generatePages(PDFGenerator pd) {

    	try{
	    	float yStart=0;
	        yStart=generatePage1(pd);
	        PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
	        pd.setCurrentPage(page);
	        
	        yStart=bankruptcyDetailsAndDeclaration(page,pd,yStart);
	       // generatePage3(pd);
	        writeFileList(pd, yStart);
	        for(int i=1;i<pd.getDocument().getNumberOfPages();i++){
	        	pd.applyPageHeader(pd.getDocument().getPage(i),pd.getDocument());
	        }
    	}catch(Exception e){
    		logger.error("error"+e);
    	}
       // generatePage3(pd);

    }

    @Override
    public float writeClaimInformation(PDFGenerator pd, float yStart) throws IOException {
        float tableWidth = getTableWidth(pd);
        PDPage page = pd.getCurrentPage();

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        List<String[]> rows = new ArrayList<>();

        pd.addTableHeader(table, "Claim information");
        rows.add(new String[]{"Before Disability"});
        rows.add(new String[]{"<no-border>Occupation", getBeforeOccupation()});
        rows.add(new String[]{"<no-border>Name of Employer", getBeforeEmployerName()});
        StringBuffer beforeAdd=new StringBuffer();
        if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBeforeEmployerBuildingNumber()), "null"))&&StringUtils.isNotBlank(getBeforeEmployerBuildingNumber())){
        	beforeAdd.append(getBeforeEmployerBuildingNumber()+" ");
        }
        if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBeforeEmployerStreet1()), "null"))&&StringUtils.isNotBlank(getBeforeEmployerStreet1())){
        	beforeAdd.append(getBeforeEmployerStreet1());
        }
        if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBeforeEmployerStreet2()), "null"))&&StringUtils.isNotBlank(getBeforeEmployerStreet2())){
        	beforeAdd.append("<br>"+getBeforeEmployerStreet2() +" ");
        }
        if(!(StringUtils.equalsIgnoreCase(getBeforeEmployerUnitNumber1(), "null"))&&StringUtils.isNotBlank(getBeforeEmployerUnitNumber1())){
        	beforeAdd.append("<br>"+getBeforeEmployerUnitNumber1() +" -");
        }
        if(!(StringUtils.equalsIgnoreCase(getBeforeEmployerUnitNumber2(), "null"))&&StringUtils.isNotBlank(getBeforeEmployerUnitNumber2())){
        	beforeAdd.append(getBeforeEmployerUnitNumber2() +"");
        }
        if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBeforeEmployerBuildingName()), "null"))&&StringUtils.isNotBlank(getBeforeEmployerBuildingName())){
        	beforeAdd.append("<br>"+getBeforeEmployerBuildingName()+"");
        }
        if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBeforeEmployerCity()), "null"))&&StringUtils.isNotBlank(getBeforeEmployerCity())){
        	beforeAdd.append("<br>"+getBeforeEmployerCity()+"");
        }
        if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBeforeEmployerPostalCode()), "null"))&&StringUtils.isNotBlank(getBeforeEmployerPostalCode())){
        	beforeAdd.append(" "+getBeforeEmployerPostalCode()+"");
        }
        String beforeAddress=beforeAdd.toString();

        rows.add(new String[]{
                "<no-border>Address of Employer",
                beforeAddress
        });

       

        DecimalFormat df1 = new DecimalFormat("#,###.00");
        rows.add(new String[]{"<no-border>Average monthly salary", "S$"+df1.format(getBeforeSalary())});
        rows.add(new String[]{"<no-border-last>Exact duties performed at work", getBeforeWorkDuty()});
        rows.add(new String[]{"After Disability"});
        if(StringUtils.isNotBlank(getAfterOccupation())){
         rows.add(new String[]{"<no-border>Occupation", getAfterOccupation()});
        }else{
        	rows.add(new String[]{"<no-border>Occupation","N/A"});
        }
        if(StringUtils.isNotBlank(getAfterEmployerName())){
         rows.add(new String[]{"<no-border>Name of Employer", getAfterEmployerName()});
        }else{
        	rows.add(new String[]{"<no-border>Name of Employer","N/A"});
        }
        if(StringUtils.isNotBlank(getAfterEmployerBuildingNumber())||StringUtils.isNotBlank(getAfterEmployerStreet1())||StringUtils.isNotBlank(getAfterEmployerBuildingName())
        		||StringUtils.isNotBlank(getAfterEmployerCity())||StringUtils.isNotBlank(getAfterEmployerPostalCode())
        		||StringUtils.isNotBlank(getAfterEmployerStreet2())||StringUtils.isNotBlank(getAfterEmployerUnitNumber1())||StringUtils.isNotBlank(getAfterEmployerUnitNumber2())){
        	
        	StringBuffer address=new StringBuffer();
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getAfterEmployerBuildingNumber()), "null")&&StringUtils.isNotBlank(getAfterEmployerBuildingNumber())){
            	address.append(getAfterEmployerBuildingNumber()+" ");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getAfterEmployerStreet1()), "null")&&StringUtils.isNotBlank(getAfterEmployerStreet1())){
            	address.append(getAfterEmployerStreet1());
            }
            if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getAfterEmployerStreet2()), "null")&&StringUtils.isNotBlank(getAfterEmployerStreet2()))){
            	address.append("<br>"+getAfterEmployerStreet2() +" ");
            }
            if(!(StringUtils.equalsIgnoreCase(getAfterEmployerUnitNumber1(), "null"))&&StringUtils.isNotBlank(getAfterEmployerUnitNumber1())){
            	address.append("<br>"+getAfterEmployerUnitNumber1() +" -");
            }
            if(!(StringUtils.equalsIgnoreCase(getAfterEmployerUnitNumber2(), "null"))&&StringUtils.isNotBlank(getAfterEmployerUnitNumber2())){
            	address.append(getAfterEmployerUnitNumber2() +"");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getAfterEmployerBuildingName()), "null")&&StringUtils.isNotBlank(getAfterEmployerBuildingName())){
            	address.append("<br>"+getAfterEmployerBuildingName()+"");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getAfterEmployerCity()), "null")&&StringUtils.isNotBlank(getAfterEmployerCity())){
            	address.append("<br>"+getAfterEmployerCity()+"");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getAfterEmployerPostalCode()), "null")&&StringUtils.isNotBlank(getAfterEmployerPostalCode())){
            	address.append(" "+getAfterEmployerPostalCode()+"");
            }
        	
        	String add=address.toString();

        rows.add(new String[]{
                "<no-border>Address of Employer",
                add
        });
        }else{
        	rows.add(new String[]{
                    "<no-border>Address of Employer",
                    "N/A"
            });
        }
        String nullableSalary = "";
        if(getAfterSalary() != null){
        	
            nullableSalary = "S$"+df1.format(getAfterSalary());

        }
        if(StringUtils.isNotBlank(nullableSalary)){
         rows.add(new String[]{"<no-border>Average monthly salary", nullableSalary});
        }else{
        	rows.add(new String[]{"<no-border>Average monthly salary","N/A"});
        }
        if(StringUtils.isNotBlank(getAfterWorkDuty())){
         rows.add(new String[]{"<no-border-last>Exact duties performed at work", getAfterWorkDuty()});
        }else{
        	rows.add(new String[]{"<no-border-last>Exact duties performed at work","N/A"});
        }
        rows.add(new String[]{"When did the symptoms start?", formatDate(getIllnessSymptomsStart())});
        rows.add(new String[]{"Please describe in detail all symptoms and / or nature of injuries / disability suffered", getIllnessSuffered()});
        rows.add(new String[]{"Date on which you last worked prior to your disability", formatDate(getDisabilityLastWorkDate())});
        if(getDisabilityReturnWorkDate()!=null){
         rows.add(new String[]{"Date on which you returned or expect to return to work", formatDate(getDisabilityReturnWorkDate())});
        }
        int doctorNo=1;
        for (DoctorPortion doctor : getDoctorList()) {
        	String consultationPeriod="Consultation Period";
        	String nameofDoctor="Name of Doctor / Specialist";
        	String nameOfHospital="Name of Hospital / Clinic";
        	String addressOfHospital="Address of Hospital / Clinic";
        	if(doctorNo>1){
        		consultationPeriod="Consultation "+doctorNo+" Period";
        		nameofDoctor+=" "+doctorNo;
        		nameOfHospital+=" "+doctorNo;
        		addressOfHospital+=" "+doctorNo;
        	}
            rows.add(new String[]{
            		"<no-border>"+consultationPeriod,
                    formatDateMonthYear(doctor.getFromDate()) + " - " + formatDateMonthYear(doctor.getToDate())
            });
            rows.add(new String[]{"<no-border>"+nameofDoctor, doctor.getName()});
            rows.add(new String[]{"<no-border>"+nameOfHospital, doctor.getHospital()});
            rows.add(new String[]{"<no-border-last>"+addressOfHospital,
                    doctor.getHospitalCity() + "<br>" +
                            doctor.getHospitalStreet1() + "<br>" +
                            doctor.getHospitalStreet2()
            });

            doctorNo++;
        }


        rows.add(new String[]{"Please upload a copy of your medical reports from your attending Doctor(s)","Number of documents uploaded - 1"});

        if(!uploadedDocuments.contains(getMedicalReport())){
          uploadedDocuments.add(getMedicalReport());
        }
        rows.add(new String[]{"Please upload a duly signed Clinical Abstract Application form","Number of documents uploaded - 1"});

        if(!uploadedDocuments.contains(getClinicalAbtractForm())){
         uploadedDocuments.add(getClinicalAbtractForm());
        }

        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
        }

        return table.draw() - 10;
    }
}
