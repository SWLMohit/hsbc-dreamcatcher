package sg.com.hsbc.dreamcatcher;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import sg.com.hsbc.dreamcatcher.helpers.BuildDetails;
import sg.com.hsbc.dreamcatcher.helpers.Config;
import sg.com.hsbc.dreamcatcher.helpers.*;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet(urlPatterns = "/debug", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/debug.jsp")
})
public class DebugServlet extends BaseServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");

        try {
            if (email != null && !email.isEmpty()) {
                sendEmail(email);
            }

            if (mobile != null && !mobile.isEmpty()) {
                sendSMS(mobile);
            }
            render(request, response);
        } catch (MessagingException e) {
            IOException ioe = new IOException();
            ioe.initCause(e);
            throw ioe;
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // git details
        request.setAttribute("buildDetails", BuildDetails.getMap());

        render(request, response);
    }

    public void sendEmail(String email) throws MessagingException {
        String subject = "Test email from debugger";
        String content = "This is a test email from debugger";
        String toEmail = email;
        EmailSender.sendEmail(toEmail, subject, content, null);
    }

    public void sendSMS(String mobile) throws MessagingException {
        AmazonSNS sns = AmazonSNSClient.builder().withRegion(Regions.AP_SOUTHEAST_1).build();
        String smsMessage = "This is a test SMS from debugger";
        String smsPhoneNumber = mobile;
        EmailSender.sendSMS(smsMessage, smsPhoneNumber, false);
    }
}
