package sg.com.hsbc.dreamcatcher.eclaims.unemployment;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/7-unemployment/1", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/7_unemployment/step1a.jsp")
})
public class Step1aServlet extends BaseServlet {

    public float getCurrentStep() {
        return (float)1;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        application.setMaxStep(getCurrentStep() + (float)0.5);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

    protected float getStep() {
        return (float)0.5;
    }

}
