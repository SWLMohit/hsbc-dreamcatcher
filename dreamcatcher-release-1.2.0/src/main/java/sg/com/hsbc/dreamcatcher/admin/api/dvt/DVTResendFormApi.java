package sg.com.hsbc.dreamcatcher.admin.api.dvt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.com.hsbc.dreamcatcher.admin.AdminUtil;
import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendHistory;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendStatus;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendType;
import sg.com.hsbc.dreamcatcher.admin.repository.ResendHistoryRepository;
import sg.com.hsbc.dreamcatcher.admin.service.AdminService;
import sg.com.hsbc.dreamcatcher.admin.service.impl.AdminOPServiceImpl;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/resend-form/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/resend_form.jsp")
})
public class DVTResendFormApi extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(DVTResendFormApi.class);

    private AdminService adminDVTService;
    private Applications applications;
    private ResendHistoryRepository resendHistoryRepository;
    private ResendHistory resendHistory;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("DVT resend form");
        render(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> errors;
        errors = new AdminUtil().validateEmailAndMobileNo(request);

        if (!errors.isEmpty()) {
            render(request, response);
            return;
        }
        adminDVTService = new AdminOPServiceImpl();
        resendHistory = new ResendHistory();

        applications = new Applications();
        applications.setProductType(FieldConstants.DVT_PRODUCT_TYPE);
        applications.setApplicantEmail(request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL));
        applications.setApplicantMobileCountryCode(request.getParameter(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE));
        applications.setApplicantMobileNum(request.getParameter(FieldConstants.APPLICANT_MOBILE_NUM));
        logger.info("product type:{}, email: {}, mobile: {},{}",
                applications.getProductType(), applications.getApplicantEmail(),applications.getApplicantMobileCountryCode(),applications.getApplicantMobileNum());

        List<Applications> applicationsList = adminDVTService.listAllPolicy(applications);

        if (applicationsList != null && applicationsList.size() > 0) {
            if (applicationsList.size() > 1) {
                // multiple policy
                String policyList = AdminUtil.convertListToString(applicationsList);
                logger.info("Policy list ", policyList);
                request.getSession().setAttribute("PolicyList", policyList);
                request.getSession().setAttribute(FieldConstants.RETRIEVE_TYPE, FieldConstants.RETRIEVE_FORM);
                response.sendRedirect("/our-plans/direct-value-term/resend-form-multi/");
                return;
            } else {
                applications = applicationsList.get(0);
                //single policy
                adminDVTService.resendEmail(applications);
                errors.put("success", "success");
                logger.info("send email successful");
                resendHistoryRepository = new ResendHistoryRepository();
                resendHistory = AdminUtil.convertApplicationsToResendHistory(
                        applications, ResendType.DVT_RESEND_EMAIL_AND_PASS_CODE, ResendStatus.SUCCESS,new Date());
                resendHistoryRepository.save(resendHistory);
                logger.info("{} save to db success", ResendType.DVT_RESEND_EMAIL_AND_PASS_CODE);
            }
        } else {
            boolean emailExist = adminDVTService.isCompleteEmailExisted(applications.getApplicantEmail());
            boolean mobileExist = adminDVTService.isCompleteMobileExisted(applications.getApplicantMobileCountryCode(), applications.getApplicantMobileNum());

            errors = AdminUtil.validateEmailAndMobileNotMatch(emailExist, mobileExist);
        }
        request.setAttribute("errors", errors);

        render(request, response);
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }
}
