package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;

import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.AgeUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;


@WebServlet(urlPatterns = "/our-plans/online-protector/14", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step14.jsp")
})
@MultipartConfig
public class Step14Servlet extends BaseServlet {

	private final static Logger logger = Logger.getLogger(Step14Servlet.class);
    public float getCurrentStep() {
        return 14;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        sanitized = sanitizeApplicantIdvUpload(request, sanitized);
        
        if ((!application.applicantIsSingaporean() && !application.applicantIsPermanentResident())) { // JIRA 661
            sanitized = sanitizeApplicantSecondIdvUpload(request, sanitized); 
            /*sanitizing the relevant back upload JIRA 984*/
            sanitized = sanitizeDocumentUpload(request, sanitized,
                FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD,
                FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD_DOCUMENT_TAG);
           
			
		}
        if (!application.insuredIsSelf()) {
            sanitized = sanitizeInsuredIdvUpload(request, sanitized);
           
            if(application.getInsuredMultipleNationalities()&&!application.getInsuredNationality2().isEmpty()) {
              sanitized = sanitizeInsuredNationalityTwoUpload(request, sanitized);
            }
            if(application.getInsuredMultipleNationalities()&&!application.getInsuredNationality3().isEmpty()) {
              sanitized = sanitizeInsuredNationalityThreeUpload(request, sanitized);
            }
            if(!application.insuredIsSingaporean() && !application.insuredIsPermanentResident()) {
            	  sanitized = sanitizeInsuredSecondIdvUpload(request, sanitized);	// JIRA 661
            	  /*sanitizing the insured relevant back upload JIRA 984*/
            	  sanitized = sanitizeDocumentUpload(request, sanitized,
                    FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD,
                    FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD_DOCUMENT);
            }
           
        }
        sanitized = sanitizeApplicantAddressUpload(request, sanitized);
        sanitized = sanitizeCcType(request, sanitized);
        sanitized = sanitizeCcName(request, sanitized);
        sanitized = sanitizeCcNum(request, sanitized);
        sanitized = sanitizeCcExpiry(request, sanitized);
        sanitized = sanitizeMailOption(request, sanitized);

        
        if(!application.getApplicantIsBeneficalOwner()) { //added beneficial owner documents as a part of HSBCIDCU-820
          sanitized = sanitizeBeneficalIdvUpload(request, sanitized);
        }
        if (application.getApplicantMultipleNationalities()&&!application.getApplicantNationality2().isEmpty()) { //added second nationality documents as a part of HSBCIDCU-820
          sanitized = sanitizeApplicantNationalityTwoUpload(request, sanitized);
        }
        if (application.getApplicantMultipleNationalities()&&!application.getApplicantNationality3().isEmpty()) { //added third nationality documents as a part of HSBCIDCU-820
          sanitized = sanitizeApplicantNationalityThreeUpload(request, sanitized);
        }
        
        if(!application.applicantIsSingaporean() && !(application.applicantIsPermanentResident()
            &&application.getApplicantAddressIsPermanent()) ) {
          sanitized = sanitizeApplicantPermanentAddressUpload(request, sanitized);
        }

        if (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) { // JIRA HSBCIDCU-950
        	sanitized = sanitizeApplicantBackNricUpload(request, sanitized);
        }
        if (!application.insuredIsSelf()) { // JIRA HSBCIDCU-950
            if (application.insuredIsSingaporean() || application.insuredIsPermanentResident()) { 
        	  sanitized = sanitizeApplicantBackNricInsuredUpload(request, sanitized);
            }

        }
        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        Map<String,String> docUpload=new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        errors = validateCcType(data, errors);
        errors = validateCcName(data, errors);
        errors = validateCcNum(data, errors);
        errors = validateCcExpiry(data, errors);
        errors = validateApplicantIdvUpload(data, errors);


        if(errors.containsKey("applicant_idv_previous_upload")){
        	docUpload.put("applicant_idv_previous_upload", "true");
        	errors.remove("applicant_idv_previous_upload");
        }
        if ((!application.applicantIsSingaporean() && !application.applicantIsPermanentResident())) { // JIRA 661 validation for relevant pass field for self insured scenario
         	 errors = validateApplicantSecondIdvUpload(data, errors);
         	if(errors.containsKey("applicant_idv_previous_upload_2")){
           	docUpload.put("applicant_idv_previous_upload_2", "true");
           	errors.remove("applicant_idv_previous_upload_2");
           }
         	
         	errors = validateDocumentUpload(data, errors,
                    FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD,
                    FieldConstants.EMPTY_FILE_UPLOAD_MESSAGE);
         	
         	if(errors.containsKey(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous")){
               	docUpload.put(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous", "true");
               	errors.remove(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous");
               }
         	
         }


        if (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) { // JIRA HSBCIDCU-950
          errors = validateApplicantIdvUploadBack(data, errors);
           if(data.containsKey("applicant_idv_back_nric_previous_upload")){
             	docUpload.put("applicant_idv_back_nric_previous_upload", "true");
             	errors.remove("applicant_idv_back_nric_previous_upload");
             }
        }

        if (!application.insuredIsSelf()) {
            errors = validateInsuredIdvUpload(data, errors);


            if(errors.containsKey("insured_idv_previous_upload")){
            	docUpload.put("insured_idv_previous_upload", "true");
            	errors.remove("insured_idv_previous_upload");
            }

            if(application.getInsuredMultipleNationalities()&& !application.getInsuredNationality2().isEmpty()) {
              errors = validateInsuredNationalityTwoIdvUpload(data, errors);
              if(errors.containsKey("insured_nationality_two_idv_previous_upload")){
              	docUpload.put("insured_nationality_two_idv_previous_upload", "true");
              	errors.remove("insured_nationality_two_idv_previous_upload");
              }
            }
            if(application.getInsuredMultipleNationalities()&& !application.getInsuredNationality3().isEmpty()) {
              errors = validateInsuredNationalityThreeIdvUpload(data, errors);
              if(errors.containsKey("insured_nationality_three_idv_previous_upload")){
                	docUpload.put("insured_nationality_three_idv_previous_upload", "true");
                	errors.remove("insured_nationality_three_idv_previous_upload");
                }
            }
            if(application.insuredIsSingaporean()||application.insuredIsPermanentResident()) {
            errors = validateInsuredIdvUploadBack(data, errors);
             if(data.containsKey("insured_idv_back_nric_previous_upload")){
               	docUpload.put("insured_idv_back_nric_previous_upload", "true");
               	errors.remove("insured_idv_back_nric_previous_upload");
               }
            }
           
            if(!application.insuredIsSingaporean() && !application.insuredIsPermanentResident()) { // JIRA 661 validation for relevant pass field for spouse insured scenario
                
            	errors = validateInsuredSecondIdvUpload(data, errors);
                if (errors.containsKey("insured_idv_previous_upload_2")) {
                    docUpload.put("insured_idv_previous_upload_2", "true");
                    errors.remove("insured_idv_previous_upload_2");
                }
                errors = validateDocumentUpload(data, errors,
                        FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD,
                        FieldConstants.EMPTY_FILE_UPLOAD_MESSAGE);
                if (errors.containsKey(FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD+"_previous")) {
                    docUpload.put(FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD+"_previous", "true");
                    errors.remove(FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD+"_previous");
                }
                
            }
        }
       
        
        errors = validateApplicantAddressUpload(data, errors,application);
        
        if(errors.containsKey("applicant_address_previous_upload")){
        	docUpload.put("applicant_address_previous_upload", "true");
        	errors.remove("applicant_address_previous_upload");
        }
        


        /* validating beneficial owner documents as a part of HSBCIDCU-820 */
        if(!application.getApplicantIsBeneficalOwner()) { 
          errors = validateBeneficalIdvUpload(data, errors);
          if(errors.containsKey("beneficial_idv_previous_upload")){
          	docUpload.put("beneficial_idv_previous_upload", "true");
          	errors.remove("beneficial_idv_previous_upload");
          }
        }
        
        if (application.getApplicantMultipleNationalities()&& !application.getApplicantNationality2().isEmpty()) { // validating applicant nationality 2 documents as a part of HSBCIDCU-820
          errors =validateApplicantNationalityTwoIdvUpload(data, errors);
          if(errors.containsKey("applicant_nationality_two_idv_previous_upload")){
            	docUpload.put("applicant_nationality_two_idv_previous_upload", "true");
            	errors.remove("applicant_nationality_two_idv_previous_upload");
            }
        }
        
        if (application.getApplicantMultipleNationalities()&& !application.getApplicantNationality3().isEmpty()) { // validating applicant nationality 3 documents as a part of HSBCIDCU-820
          errors =validateApplicantNationalityThreeIdvUpload(data, errors);
          if(errors.containsKey("applicant_nationality_three_idv_previous_upload")){
          	docUpload.put("applicant_nationality_three_idv_previous_upload", "true");
          	errors.remove("applicant_nationality_three_idv_previous_upload");
          }
        }
        if(!application.applicantIsSingaporean() && !(application.applicantIsPermanentResident()
            &&application.getApplicantAddressIsPermanent()) ) {
          errors =validatePermanentApplicantAddressUpload(data, errors);
          
          if(errors.containsKey("applicant_permanent_address_previous_upload")){
            	docUpload.put("applicant_permanent_address_previous_upload", "true");
            	errors.remove("applicant_permanent_address_previous_upload");
            }
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);
        request.setAttribute("docUpload", docUpload);
        if (errors.size() > 0) {
            try {
            	 Map premiums = application.getPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
                 double premiumModal = (double)premiums.get("modalPremium");
                 double premiumAnnual = premiumModal * application.getPaymentMode();

                 double ciRiderPremiumModal = 0.0;
                 double ciRiderPremiumAnnual = 0.0;
                 if (application.getIncludeCriticalIllnessRider()) {
                     premiums = application.getCriticalIllnessRiderPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
                     ciRiderPremiumModal = (double)premiums.get("modalPremium");
                     ciRiderPremiumAnnual = ciRiderPremiumModal * application.getPaymentMode();
                 }

                 float ropRiderPremiumModal = 0;
                 float ropRiderPremiumAnnual = 0;
                 if (application.getIncludeRefundRider()) {
                     premiums = application.getRefundRiderPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
                     ropRiderPremiumModal = (float)premiums.get("modalPremium");
                     ropRiderPremiumAnnual = ropRiderPremiumModal * application.getPaymentMode();
                 }

                 DecimalFormat df = new DecimalFormat("S$#,###.00");
                 DecimalFormat df1 = new DecimalFormat("####.00");
                 String premiumModalString=df1.format(premiumModal);
                 float premiumModalTwo=Float.parseFloat(premiumModalString);
                 data.put("summary_modal", df.format(premiumModal));
                 
                 String premiumAnnualString=df1.format(premiumAnnual);
                 float premiumAnnualTwo=Float.parseFloat(premiumAnnualString);
                 
                 data.put("summary_annually", df.format(premiumAnnual));
                 
                 String ciRiderPremiumModalString=df1.format(ciRiderPremiumModal);
                 float ciRiderPremiumModalTwo=Float.parseFloat(ciRiderPremiumModalString);
                 
                 data.put("summary_ci_modal", df.format(ciRiderPremiumModal));
                 
                 String ciRiderPremiumAnnualString=df1.format(ciRiderPremiumAnnual);
                 float ciRiderPremiumAnnualTwo=Float.parseFloat(ciRiderPremiumAnnualString);
                 
                 data.put("summary_ci_annually", df.format(ciRiderPremiumAnnual));
                 
                 String ropRiderPremiumModalString=df1.format(ropRiderPremiumModal);
                 float ropRiderPremiumModalTwo=Float.parseFloat(ropRiderPremiumModalString);
                 
                 data.put("summary_rop_modal", df.format(ropRiderPremiumModal));
                 
                 String ropRiderPremiumAnnualString=df1.format(ropRiderPremiumAnnual);
                 float ropRiderPremiumAnnualTwo=Float.parseFloat(ropRiderPremiumAnnualString);
                 data.put("summary_rop_annually", df.format(ropRiderPremiumAnnual));
                 
                 data.put("summary_total_modal", df.format(premiumModalTwo + ciRiderPremiumModalTwo + ropRiderPremiumModalTwo));
                 
                 data.put("summary_total_annually",df.format( premiumAnnualTwo + ciRiderPremiumAnnualTwo + ropRiderPremiumAnnualTwo));

                request.setAttribute("data", data);
            } catch (Exception e) {
                ErrorHandler.handleError(application, e);
                return false;
            }
        }
        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setCreditCardType(data.get("cc_type").toString());
        application.setCreditCardName(data.get("cc_name").toString());
        application.setCreditCardNum(data.get("cc_num").toString());
        application.setCreditCardExpiry(String.format("%s/%s", data.get("cc_expiry_month").toString(), data.get("cc_expiry_year").toString()));
        application.setMaxStep(getCurrentStep() + 1);
        application.setMailOption(Integer.parseInt(data.get("mail_option").toString()));
        if(request.getSession().getAttribute("marketingConsentOP")!=null){
        	boolean marketing=(boolean) request.getSession().getAttribute("marketingConsentOP");
        	application.setMarketingConsent( marketing );
        }
        if (application.complete()) {
            ExecutorService threadPool = (ExecutorService) getServletConfig().getServletContext().getAttribute("threadPool");
            threadPool.execute(new OnlineProtectorApplicationJob(application));

            return true;

        }
        return false;
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("/our-plans/online-protector/complete");
    }

    private Map<String, String> validateCcType(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("cc_type"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("cc_type", "Your payment type is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateCcName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("cc_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("cc_name", "Your name is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("cc_name", "Your name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateCcNum(Map<String, Object> data, Map<String, String> errors) {
        boolean hasError = false;
        switch (Validator.validateString(data.get("cc_num"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("cc_num", "Your credit card number is missing.");
                hasError = true;
                break;
        }

        if (!hasError) {
            switch (Validator.validateSingaporeCreditCardNum(data.get("cc_num"))) {
                case Validator.ERROR_CC_NUM_INVALID:
                    errors.put("cc_num", "Your credit card number is invalid.");
                    break;
                case Validator.ERROR_CC_NUM_NOT_LOCAL:
                    errors.put("cc_num", "Only locally issued credit cards or debit cards are allowed.");
                    break;
            }
        }

        return errors;
    }

    private Map<String, String> validateCcExpiry(Map<String, Object> data, Map<String, String> errors) {
        if (data.get("cc_expiry_month") == null && data.get("cc_expiry_year") == null) {
            errors.put("cc_expiry", "Expiry date is missing.");
        } else {
            if (data.get("cc_expiry_month") == null || data.get("cc_expiry_year") == null) {
                errors.put("cc_expiry", "Expiry date is invalid.");
            } else {
                switch (Validator.validateDateExpiry(data.get("cc_expiry_month"), data.get("cc_expiry_year"))) {
                    case Validator.ERROR_DATE_EMPTY:
                        errors.put("cc_expiry", "Expiry date is missing.");
                        break;
                    case Validator.ERROR_DATE_INVALID:
                        errors.put("cc_expiry", "Expiry date is invalid.");
                        break;
                }
            }
        }

        return errors;
    }
   
    
    /**
     * This method checks the http request object (sanitized hashmap) for the
     * existence of the element given by the identifier string. if the element does
     * not exist, an error given by errorMessage string will be added to a map of
     * error messages. JIRA HSBCIDCU-984
     * 
     * @param data          Hashmap of santized http request data. 
     * @param errors        Hashmap of error messages to be displayed to the end user.
     * @param identifier    String of element identifier.
     * @param errorMessage  String of error message.
     * @return              Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateDocumentUpload(Map<String, Object> data,
        Map<String, String> errors, String identifier, String errorMessage) {
      if (StringUtils.isNotBlank(identifier)) {
        if (!(boolean) data.get(identifier)) {
          errors.put(identifier, errorMessage);
        }else if(data.containsKey(identifier+"_previous")&&(boolean)data.get(identifier+"_previous")){
        	errors.put(identifier+"_previous","true");
        }
      }
      return errors;
    }

    private Map<String, String> validateApplicantIdvUpload(Map<String, Object> data, Map<String, String> errors) {
        if (!(boolean)data.get("applicant_idv_upload")) {
            errors.put("applicant_idv_upload", "Please upload a file.");
        }else if(data.containsKey("applicant_idv_previous_upload")&&(boolean)data.get("applicant_idv_previous_upload")){
        	errors.put("applicant_idv_previous_upload","true");
        }

        return errors;
    }
    
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element benefical_idv_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateBeneficalIdvUpload(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get("beneficial_idv_upload")) {
          errors.put("beneficial_idv_upload", "Please upload a file.");
      }else if(data.containsKey("beneficial_idv_previous_upload")&&(boolean)data.get("beneficial_idv_previous_upload")){
      	errors.put("beneficial_idv_previous_upload","true");
      }

      return errors;
  }
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element applicant_nationality_two_idv_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantNationalityTwoIdvUpload(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get("applicant_nationality_two_idv_upload")) {
          errors.put("applicant_nationality_two_idv_upload", "Please upload a file.");
      }else if(data.containsKey("applicant_nationality_two_idv_previous_upload")&&(boolean)data.get("applicant_nationality_two_idv_previous_upload")){
        	errors.put("applicant_nationality_two_idv_previous_upload","true");
        }

      return errors;
  }
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element applicant_nationality_three_idv_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantNationalityThreeIdvUpload(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get("applicant_nationality_three_idv_upload")) {
          errors.put("applicant_nationality_three_idv_upload", "Please upload a file.");
      }else if(data.containsKey("applicant_nationality_three_idv_previous_upload")&&(boolean)data.get("applicant_nationality_three_idv_previous_upload")){
      	errors.put("applicant_nationality_three_idv_previous_upload","true");
      }

      return errors;
  }
    /**
    * This method checks the http request object (sanitized hashmap) for the existence of the element applicant_nationality_two_idv_upload.
    * if the element does not exist, an error will be added to a map of error messages.
    *      
    * @param data     Hashmap of santized http request data. 
    * @param errors   Hashmap of error messages to be displayed to the end user.
    * @return         Updated Hashmap of error messages to be displayed to the end user.   
    */
   private Map<String, String> validateInsuredNationalityTwoIdvUpload(Map<String, Object> insured_nationality_two_idv_upload, Map<String, String> errors) {
     if (!(boolean)insured_nationality_two_idv_upload.get("insured_nationality_two_idv_upload")) {
         errors.put("insured_nationality_two_idv_upload", "Please upload a file.");
     }else if(insured_nationality_two_idv_upload.containsKey("insured_nationality_two_idv_previous_upload")&&(boolean)insured_nationality_two_idv_upload.get("insured_nationality_two_idv_previous_upload")){
       	errors.put("insured_nationality_two_idv_previous_upload","true");
       }

     return errors;
 }
    
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element insured_nationality_three_idv_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateInsuredNationalityThreeIdvUpload(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get("insured_nationality_three_idv_upload")) {
          errors.put("insured_nationality_three_idv_upload", "Please upload a file.");
      }else if(data.containsKey("insured_nationality_three_idv_previous_upload")&&(boolean)data.get("insured_nationality_three_idv_previous_upload")){
        	errors.put("insured_nationality_three_idv_previous_upload","true");
        }

      return errors;
  }


    /** This method checks the http request object (sanitized hashmap) for the existence of the element APPLICANT_IDV_BACK_NRIC_UPLOAD.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    
    private Map<String, String> validateApplicantIdvUploadBack(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD)) {
          errors.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, "Please upload a file.");
      }else if(data.containsKey("applicant_idv_back_nric_previous_upload")&&(boolean)data.get("applicant_idv_back_nric_previous_upload")){
      	errors.put("applicant_idv_back_nric_previous_upload","true");
      }

      return errors;
  }
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateInsuredIdvUploadBack(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get(FieldConstants.INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD)) {
          errors.put(FieldConstants.INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD, "Please upload a file.");
      }else if(data.containsKey("insured_idv_back_nric_previous_upload")&&(boolean)data.get("insured_idv_back_nric_previous_upload")){
        	errors.put("insured_idv_back_nric_previous_upload","true");
        }

      return errors;
  }
    private Map<String, String> validateInsuredIdvUpload(Map<String, Object> data, Map<String, String> errors) {
        if (!(boolean)data.get("insured_idv_upload")) {
            errors.put("insured_idv_upload", "Please upload a file.");
        }else if(data.containsKey("insured_idv_previous_upload")&&(boolean)data.get("insured_idv_previous_upload")){
        	errors.put("insured_idv_previous_upload","true");
        }

        return errors;
    }
   
    
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element insured_idv_two_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateInsuredSecondIdvUpload(Map<String, Object> data, Map<String, String> errors) {//JIRA 661 revelant pass validation for spouse scenario
        if (!(boolean)data.get("insured_idv_two_upload")) {
            errors.put("insured_idv_two_upload", "Please upload a file.");
        }else if(data.containsKey("insured_idv_previous_upload_2")&&(boolean)data.get("insured_idv_previous_upload_2")){
        	errors.put("insured_idv_previous_upload_2","true");
        }
        return errors;
    }
    private Map<String, String> validateApplicantAddressUpload(Map<String, Object> data, Map<String, String> errors, OnlineProtectorApplication application) {
        if ((!(boolean)data.get("applicant_address_upload"))) {
            errors.put("applicant_address_upload", "Please upload a file.");
        }else if(data.containsKey("applicant_address_previous_upload")&&(boolean)data.get("applicant_address_previous_upload")){
        	errors.put("applicant_address_previous_upload","true");
        }

        return errors;
    }
    
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element applicant_idv_two_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantSecondIdvUpload(Map<String, Object> data, Map<String, String> errors) {//JIRA 661
        if (!(boolean)data.get("applicant_idv_two_upload")) {
            errors.put("applicant_idv_two_upload", "Please upload a file.");
        } else if(data.containsKey("applicant_idv_previous_upload_2")&&(boolean)data.get("applicant_idv_previous_upload_2")){
            	errors.put("applicant_idv_previous_upload_2","true");
            }
    

        return errors;
    }



    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element applicant_permanent_address_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validatePermanentApplicantAddressUpload(Map<String, Object> data, Map<String, String> errors) {
      if ((!(boolean)data.get("applicant_permanent_address_upload"))) {
          errors.put("applicant_permanent_address_upload", "Please upload a file.");
      }else if(data.containsKey("applicant_permanent_address_previous_upload")&&(boolean)data.get("applicant_permanent_address_previous_upload")){
      	errors.put("applicant_permanent_address_previous_upload","true");
      }

      return errors;
  }
    private Map<String, Object> sanitizeCcType(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("cc_type");
        sanitized.put("cc_type", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeCcName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("cc_name");
        sanitized.put("cc_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeCcNum(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("cc_num");
        sanitized.put("cc_num", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeCcExpiry(HttpServletRequest request, Map<String, Object> sanitized) {
        String month = request.getParameter("cc_expiry_month");
        String year = request.getParameter("cc_expiry_year");

        sanitized.put("cc_expiry_month", month);
        sanitized.put("cc_expiry_year", year);

        return sanitized;
    }
    private Map<String, Object> sanitizeMailOption(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("mail_option");
        if(input != null){
        sanitized.put("mail_option", input);
        }
        else
        {
          sanitized.put("mail_option", 0);
        }
        return sanitized;
    }
  
    /**
     * This method checks the existence of element given by identifier , validates 
     * the format to be JPEG and  saves it to the temp folder along with appending 
     * a text for identifying the document given in document tag.
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * JIRA HSBCIDCU-984
     * 
     * 
     * @param request       Hashmap of http request data.
     * @param sanitized     Hashmap of santized request data.
     * @param identifier    String identifier of element.
     * @param documentTag   String value of trailing text in file name. 
     * @return              Hashmap of updated sanitized request data
     */
      private Map<String, Object> sanitizeDocumentUpload(HttpServletRequest request,
          Map<String, Object> sanitized, String identifier, String documentTag) {
        sanitized.put(identifier, false);
        InputStream is = null;
        OutputStream os = null;
 
        try {
          if (StringUtils.isNotBlank(identifier) && StringUtils.isNotBlank(documentTag)) {
            Part part = request.getPart(identifier);
            if (part != null && part.getSize() > 0) {
              is = part.getInputStream();
				String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
				if ( !Validator.isJPEG(is,contentType) ) {
				    is.close();
				    return sanitized;
              }
              // close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              os = new FileOutputStream(String.format("%s/%s-%s."+contentType,
                  System.getProperty("java.io.tmpdir"), getApplication(request).getId(), documentTag));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put(identifier, true);
              sanitized.put(identifier+"_previous",true);
            }else{
          	  
            	
                	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
              	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
              		
                  		   String idvField=String.format("%s/%s-%s."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId(), documentTag);
                           File idvFile=new File(idvField);
                           if(idvFile.exists()){
                        	   sanitized.put(identifier, true);
                       		   sanitized.put(identifier+"_previous",true);
                               break;
                           }
              		
              	   }

            }     
          }
        } catch (Exception e) {
          logger.error("Exception occurred while closing sanitizeDocumentUpload inputstream " + e);
        } finally {
          if (is != null) {
            try {
              is.close();
            } catch (IOException e) {
              logger.error("Exception occurred while closing sanitizeDocumentUpload inputstream " + e);
            }
          }
          if (os != null) {
            try {
              os.close();
            } catch (IOException e) {
              logger.error("Exception occurred while closing sanitizeDocumentUpload outputstream " + e);
            }
          }
        }
 
        return sanitized;
      }
 
    


    /**
     * This method checks the existence of element applicant_idv_upload,validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
  private Map<String, Object> sanitizeApplicantIdvUpload(HttpServletRequest request,
      Map<String, Object> sanitized) {
    sanitized.put(FieldConstants.APPLICANT_IDV_UPLOAD, false);
    InputStream is = null;
    OutputStream os = null;
    try {
      Part part = request.getPart(FieldConstants.APPLICANT_IDV_UPLOAD);
      if (part != null && part.getSize() > 0) {
        is = part.getInputStream();
        String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
        if (!Validator.isJPEG(is,contentType)) {
          is.close();
          return sanitized;
        }
        // close and reset the input stream, so that the stream start from the beginning
        is.close();
        is = part.getInputStream();
        os = new FileOutputStream(String.format("%s/%s-ApplicantIDV."+contentType,
            System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId()));
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = is.read(buffer)) != -1) {
          os.write(buffer, 0, bytesRead);
        }
        is.close();
        os.flush();
        os.close();
        sanitized.put(FieldConstants.APPLICANT_IDV_UPLOAD, true);
        sanitized.put("applicant_idv_previous_upload",true);
      }else{
    	  
          	 ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
       	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
       		
           		   String idvField=String.format("%s/%s-ApplicantIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                    File idvFile=new File(idvField);
                    if(idvFile.exists()){
                    	sanitized.put(FieldConstants.APPLICANT_IDV_UPLOAD, true);
                		sanitized.put("applicant_idv_previous_upload",true);
                        break;
                    }
       		
       	   }

      }     
    } catch (Exception e) {
      logger.error(
          "Exception occurred in Step14Servlet sanitizeApplicantIdvUpload method " + e);
    } finally {
      try {
        if (is != null) {
          is.close();
        }

      } catch (IOException e) {
        logger.error(
            "Exception occurred while closing the inputstream in Step14Servlet sanitizeApplicantIdvUpload method"
                + e);
      }
      try {
        if (os != null) {
          os.close();
        }
      } catch (IOException e) {
        logger.error(
            "Exception occurred while closing the outputstream in Step14Servlet sanitizeApplicantIdvUpload method"
                + e);
      }
    }
    return sanitized;
  }
   
    /**
     * This method checks the existence of element APPLICANT_SECOND_IDV_UPLOAD, validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantSecondIdvUpload(HttpServletRequest request, Map<String, Object> sanitized) { //JIRA 661 relevant pass for self insured scenario
        sanitized.put(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD, false);
        InputStream is = null;
        OutputStream os = null;
        try {
            Part part = request.getPart(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD);
            if (part != null && part.getSize() > 0) {
                is = part.getInputStream();
                String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
                if (!Validator.isJPEG(is,contentType)) {
                  is.close();
                  return sanitized;
                }
                // close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                os = new FileOutputStream(String.format("%s/%s-ApplicantIDV2."+contentType, System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId()));
                byte[] buffer = new byte[1024];
                int bytesRead;
                while((bytesRead = is.read(buffer)) !=-1){
                    os.write(buffer, 0, bytesRead);
                }
                is.close();
                os.flush();
                os.close();
                sanitized.put(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD, true);
                sanitized.put("applicant_idv_previous_upload_2",true);
            }else{

            	
            	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
          	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
          		
              		   String idvField=String.format("%s/%s-ApplicantIDV2."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                       File idvFile=new File(idvField);
                       if(idvFile.exists()){
                    	   sanitized.put(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD, true);
                   		   sanitized.put("applicant_idv_previous_upload_2",true);
                           break;
                       }
          		
          	   }
            }
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
        } finally {
          try {
            if (is != null) {
              is.close();
            }
          } catch (IOException e) {
              logger.error("Exception occurred while closing the inputstream in Step14Servlet sanitizeApplicantSecondIdvUpload method"+ e);
              ErrorHandler.handleError(this, e);
          }
          try {
            if (os != null) {
              os.close();
            }
          } catch (IOException e) {
              logger.error("Exception occurred while closing the outputstream in Step14Servlet sanitizeApplicantSecondIdvUpload method"+ e);
              ErrorHandler.handleError(this, e);
          }
        }
        return sanitized;
    }
    
    /**
     * This method checks the existence of element "benefical_idv_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeBeneficalIdvUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("beneficial_idv_upload", false);
      try {
          Part part = request.getPart("beneficial_idv_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-BeneficalOwnerIDV."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while((bytesRead = is.read(buffer)) !=-1){
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("beneficial_idv_upload", true);
              sanitized.put("beneficial_idv_previous_upload",true);
          }else{

              	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
         	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
         		
             		   String idvField=String.format("%s/%s-BeneficalOwnerIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                      File idvFile=new File(idvField);
                      if(idvFile.exists()){
                    	  sanitized.put("beneficial_idv_upload", true);
                    		sanitized.put("beneficial_idv_previous_upload",true);
                          break;
                      }
         		
         	   }

          }     
      } catch (Exception e) {
          logger.error("Exception occurred in while uploading Benefical documents"+e);
      }

      return sanitized;
  }

    /**
     * This method checks the existence of element "applicant_nationality_two_idv_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantNationalityTwoUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("applicant_nationality_two_idv_upload", false);
      try {
          Part part = request.getPart("applicant_nationality_two_idv_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-ApplicantNationalityTwoIDV."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while((bytesRead = is.read(buffer)) !=-1){
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("applicant_nationality_two_idv_upload", true);
              sanitized.put("applicant_nationality_two_idv_previous_upload",true);
          }else{
        	  

              	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
        	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
        		
            		   String idvField=String.format("%s/%s-ApplicantNationalityTwoIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                     File idvFile=new File(idvField);
                     if(idvFile.exists()){
                    	 sanitized.put("applicant_nationality_two_idv_upload", true);
                   		 sanitized.put("applicant_nationality_two_idv_previous_upload",true);
                         break;
                     }
        		
        	   }

          }     
      } catch (Exception e) {
          logger.error("Exception occurred in while uploading second nationality documents"+e);
      }

      return sanitized;
  }
    /**
     * This method checks the existence of element "applicant_nationality_three_idv_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantNationalityThreeUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("applicant_nationality_three_idv_upload", false);
      try {
          Part part = request.getPart("applicant_nationality_three_idv_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-ApplicantNationalityThreeIDV."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while((bytesRead = is.read(buffer)) !=-1){
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("applicant_nationality_three_idv_upload", true);
              sanitized.put("applicant_nationality_three_idv_previous_upload",true);
          }else{
        	  

              	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
         	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
         		
             		   String idvField=String.format("%s/%s-ApplicantNationalityThreeIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                      File idvFile=new File(idvField);
                      if(idvFile.exists()){
                    	  sanitized.put("applicant_nationality_three_idv_upload", true);
                    	  sanitized.put("applicant_nationality_three_idv_previous_upload",true);
                          break;
                      }
         		
         	   }

          }     
      } catch (Exception e) {
          logger.error("Exception occurred in while uploading applicant's third nationality documents"+e);
      }

      return sanitized;
  }
    /**
     * This method checks the existence of element "insured_nationality_two_idv_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeInsuredNationalityTwoUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("insured_nationality_two_idv_upload", false);
      try {
          Part part = request.getPart("insured_nationality_two_idv_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-InsuredNationalityTwoIDV."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while((bytesRead = is.read(buffer)) !=-1){
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("insured_nationality_two_idv_upload", true);
              sanitized.put("insured_nationality_two_idv_previous_upload",true);
          }else{

              	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
        	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
        		
            		   String idvField=String.format("%s/%s-InsuredNationalityTwoIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                     File idvFile=new File(idvField);
                     if(idvFile.exists()){
                    	 sanitized.put("insured_nationality_two_idv_upload", true);
                   		sanitized.put("insured_nationality_two_idv_previous_upload",true);
                         break;
                     }
        		
        	   }

          }     
      } catch (Exception e) {
          logger.error("Exception occurred in while uploading insured second nationality documents"+e);
      }

      return sanitized;
  }
    /**
     * This method checks the existence of element "insured_nationality_three_idv_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeInsuredNationalityThreeUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("insured_nationality_three_idv_upload", false);
      try {
          Part part = request.getPart("insured_nationality_three_idv_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-InsuredNationalityThreeIDV."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while((bytesRead = is.read(buffer)) !=-1){
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("insured_nationality_three_idv_upload", true);
              sanitized.put("insured_nationality_three_idv_previous_upload",true);
          }else{
        	  
              	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
       	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
       		
           		   String idvField=String.format("%s/%s-InsuredNationalityThreeIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                    File idvFile=new File(idvField);
                    if(idvFile.exists()){
                    	sanitized.put("insured_nationality_three_idv_upload", true);
                  		sanitized.put("insured_nationality_three_idv_previous_upload",true);
                        break;
                    }
       		
       	   }
              	

          }     
      } catch (Exception e) {
          logger.error("Exception occurred in while uploading applicant's third nationality documents"+e);
      }

      return sanitized;
  }
     /**
     * This method checks the existence of element "INSURED_IDV_UPLOAD", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */

    private Map<String, Object> sanitizeInsuredIdvUpload(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized.put(FieldConstants.INSURED_IDV_UPLOAD, false);
        InputStream is = null;
        OutputStream os = null;
        try {
            Part part = request.getPart(FieldConstants.INSURED_IDV_UPLOAD);
            if (part != null && part.getSize() > 0) {
                is = part.getInputStream();
                String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
                if ( !Validator.isJPEG(is,contentType) ) {
                    is.close();
                    return sanitized;
                }
                //close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                os = new FileOutputStream(String.format("%s/%s-InsuredIDV."+contentType, System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId()));
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                is.close();
                os.flush();
                os.close();
                sanitized.put("insured_idv_upload", true);
                sanitized.put("insured_idv_previous_upload",true);
            }else{

            	
            	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
          	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
          		
              		   String idvField=String.format("%s/%s-InsuredIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                       File idvFile=new File(idvField);
                       if(idvFile.exists()){
                    	   sanitized.put("insured_idv_upload", true);
                   		   sanitized.put("insured_idv_previous_upload",true);
                           break;
                       }
          		
          	   }
                 	
            	

            }
        } catch (Exception e) {
            logger.error("Exception occurred in Step14Servlet sanitizeInsuredIdvUpload method " + e);
            ErrorHandler.handleError(this, e);
        } finally {
          try {
            if (is != null) {
              is.close();
            }
          } catch (IOException e) {
            logger.error(
                "Exception occurred while closing the inputstream in Step14Servlet sanitizeInsuredIdvUpload method"
                    + e);
          }
          try {
            if (os != null) {
              os.close();
            }
          } catch (IOException e) {
            logger.error(
                "Exception occurred while closing the outputstream in Step14Servlet sanitizeInsuredIdvUpload method"
                    + e);
          }
        }
        return sanitized;
    }
   
    
    /**
     * This method checks the existence of element "INSURED_SECOND_IDV_UPLOAD", validates 
     * the format to be JPEG and saves it to the temp folder.
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
  private Map<String, Object> sanitizeInsuredSecondIdvUpload(HttpServletRequest request,
      Map<String, Object> sanitized) { // JIRA 661 relevant pass for spouse insured scenario
    sanitized.put(FieldConstants.INSURED_SECOND_IDV_UPLOAD, false);
    InputStream is = null;
    OutputStream os = null;
    try {
      Part part = request.getPart(FieldConstants.INSURED_SECOND_IDV_UPLOAD);
      if (part != null && part.getSize() > 0) {
        is = part.getInputStream();
        String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
        if (!Validator.isJPEG(is,contentType)) {
          is.close();
          return sanitized;
        }
        // close and reset the input stream, so that the stream start from the beginning
        is.close();
        is = part.getInputStream();
        os = new FileOutputStream(String.format("%s/%s-InsuredIDV2."+contentType,
            System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId()));
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = is.read(buffer)) != -1) {
          os.write(buffer, 0, bytesRead);
        }
        is.close();
        os.flush();
        os.close();
        sanitized.put(FieldConstants.INSURED_SECOND_IDV_UPLOAD, true);
        sanitized.put("insured_idv_previous_upload_2",true);
      }else{

      	
      	
      	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
 	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
 		
     		   String idvField=String.format("%s/%s-InsuredIDV2."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
              File idvFile=new File(idvField);
              if(idvFile.exists()){
            	  sanitized.put(FieldConstants.INSURED_SECOND_IDV_UPLOAD, true);
            		sanitized.put("insured_idv_previous_upload_2",true);
                  break;
              }
 		
 	   }

      }
    } catch (Exception e) {
      logger.error(
          "Exception occurred in Step14Servlet sanitizeInsuredSecondIdvUpload method " + e);
    } finally {
      try {
        if (is != null) {
          is.close();
        }

      } catch (IOException e) {
        logger.error(
            "Exception occurred while closing the inputstream in Step14Servlet sanitizeInsuredSecondIdvUpload method"
                + e);
      }
      try {
        if (os != null) {
          os.close();
        }
      } catch (IOException e) {
        logger.error(
            "Exception occurred while closing the outputstream in Step14Servlet sanitizeInsuredSecondIdvUpload method"
                + e);
      }
    }
    return sanitized;
  }
  /**
   * This method checks the existence of element "APPLICANT_ADDRESS_UPLOAD", validates 
   * the format to be JPEG and saves it to the temp folder.
   * It will update the hashmap(santized) with true when the element is saved. 
   * 
   * @param request    Hashmap of http request data.    
   * @param sanitized  Hashmap of santized request data
   * @return           Hashmap of updated sanitized request data
   */
    private Map<String, Object> sanitizeApplicantAddressUpload(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_UPLOAD, false);
        InputStream is = null;
        OutputStream os = null;
        try {
            Part part = request.getPart(FieldConstants.APPLICANT_ADDRESS_UPLOAD);
            if (part != null && part.getSize() > 0) {
               is = part.getInputStream();
                String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
                if ( !Validator.isJPEG(is,contentType) ) {
                    is.close();
                    return sanitized;
                }
                //close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                os= new FileOutputStream(String.format("%s/%s-ApplicantAddress."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                is.close();
                os.flush();
                os.close();
                sanitized.put("applicant_address_upload", true);
                sanitized.put("applicant_address_previous_upload",true);
            }else{
            	
                	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
            	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
            		
                		 String idvField=String.format("%s/%s-ApplicantAddress."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                         File idvFile=new File(idvField);
                         if(idvFile.exists()){
                        	sanitized.put("applicant_address_upload", true);
                     		sanitized.put("applicant_address_previous_upload",true);
                             break;
                         }
            		
            	   }
            }
        } catch (Exception e) {
            logger.error("Exception occurred in Step14Servlet sanitizeApplicantAddressUpload method " + e);
            ErrorHandler.handleError(this, e);
        } finally {
          try {
            if (is != null) {
              is.close();
            }
          } catch (IOException e) {
            logger.error("Exception occurred while closing the inputstream in Step14Servlet sanitizeApplicantAddressUpload method" + e);
              ErrorHandler.handleError(this, e);
          }
          try {
            if (os != null) {
              os.close();
            }
          } catch (IOException e) {
            logger.error("Exception occurred while closing the outputstream in Step14Servlet sanitizeApplicantAddressUpload method" + e);
              ErrorHandler.handleError(this, e);
          }
        }
        return sanitized;
    }
    
    /**
     * This method checks the existence of element "applicant_permanent_address_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantPermanentAddressUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("applicant_permanent_address_upload", false);
      try {
          Part part = request.getPart("applicant_permanent_address_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-ApplicantPermanentAddress."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while ((bytesRead = is.read(buffer)) != -1) {
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("applicant_permanent_address_upload", true);
              sanitized.put("applicant_permanent_address_previous_upload",true);
          }else{
          	
              	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
       	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
       		
           		 String idvField=String.format("%s/%s-ApplicantPermanentAddress."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                    File idvFile=new File(idvField);
                    if(idvFile.exists()){
                    	sanitized.put("applicant_permanent_address_upload", true);
                  		sanitized.put("applicant_permanent_address_previous_upload",true);
                        break;
                    }
       		
       	   }
              	
          }
      } catch (Exception e) {
          e.printStackTrace();
      }

      return sanitized;
  }
    
    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");
        try {
        	
        	Map<String,String>  docUpload=new HashMap<String,String>();

        ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
        for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
         String ext=iterator.next();
         if(!docUpload.containsKey(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous")){
        	String appBack=String.format("%s/%s-%s."+ext,
                    System.getProperty("java.io.tmpdir"), getApplication(request).getId(), FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD_DOCUMENT_TAG);
            	File appBackFile=new File(appBack);
            	if(appBackFile.exists()){
            		docUpload.put(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous","true");
            	}
         }
           if(!docUpload.containsKey(FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD+"_previous")){
            	String inBack=String.format("%s/%s-%s."+ext,
                        System.getProperty("java.io.tmpdir"), getApplication(request).getId(), FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD_DOCUMENT);
                	File inBackFile=new File(inBack);
                	if(inBackFile.exists()){
                		docUpload.put(FieldConstants.INSURED_RELEVANT_PASS_BACK_UPLOAD+"_previous","true");
                	}
           }
           if(!docUpload.containsKey("applicant_idv_previous_upload")){
            String idvField=String.format("%s/%s-ApplicantIDV."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
        	File idvFile=new File(idvField);
        	if(idvFile.exists()){
        		docUpload.put("applicant_idv_previous_upload", "true");
        		
        	}
          }
        	if(!docUpload.containsKey("applicant_idv_previous_upload_2")){
	        	String appIDV2=String.format("%s/%s-ApplicantIDV2."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	        	File appIDV2File=new File(appIDV2);
	        	if(appIDV2File.exists()){
	        		docUpload.put("applicant_idv_previous_upload_2", "true");
	        		
	        	}
        	}
        	if(!docUpload.containsKey("insured_idv_previous_upload")){
	        	String insuredidvField=String.format("%s/%s-InsuredIDV."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	        	File insuredidvFile=new File(insuredidvField);
	        	
	        	if(insuredidvFile.exists()){
	        	    docUpload.put("insured_idv_previous_upload", "true");
	        	}
        	}
        	if(!docUpload.containsKey("insured_idv_previous_upload_2")){
	        	String insuredidv2Field=String.format("%s/%s-InsuredIDV2."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	        	File insuredidv2File=new File(insuredidv2Field);
	        	
	        	if(insuredidv2File.exists()){
	        	    docUpload.put("insured_idv_previous_upload_2", "true");
	        	}
        	}
        	if(!docUpload.containsKey("applicant_address_previous_upload")){
	        	String applicantAddressField=String.format("%s/%s-ApplicantAddress."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	        	File applicantAddressFile=new File(applicantAddressField);
	        	
	        	if(applicantAddressFile.exists()){
	         	   docUpload.put("applicant_address_previous_upload", "true");
	         	}
        	}
        	if(!docUpload.containsKey("applicant_permanent_address_previous_upload")){
	        	String applicantPermanentAddressField=String.format("%s/%s-ApplicantPermanentAddress."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	          	File applicantPermanentAddressFile=new File(applicantPermanentAddressField);
	          	if(applicantPermanentAddressFile.exists()){
	          		docUpload.put("applicant_permanent_address_previous_upload", "true");
	          	}
        	}
          	
          	if(!docUpload.containsKey("applicant_idv_back_nric_previous_upload")){
	        	String appBackNric=String.format("%s/%s-ApplicantIDVBackNric."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	        	File appBackNricFile=new File(appBackNric);
	        	if(appBackNricFile.exists()){
	        		docUpload.put("applicant_idv_back_nric_previous_upload","true");
	        	}
          	}
        	if(!docUpload.containsKey("insured_idv_back_nric_previous_upload")){
	        	String insuredBackNric=String.format("%s/%s-InsuredIDVBackNric."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	        	File insuredBackNricFile=new File(insuredBackNric);
	        	if(insuredBackNricFile.exists()){
	        		docUpload.put("insured_idv_back_nric_previous_upload","true");
	        	}
        	}
        	if(!docUpload.containsKey("insured_nationality_three_idv_previous_upload")){
	        	String nationality3=String.format("%s/%s-InsuredNationalityThreeIDV."+ext, System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId());
	          	File nationality3File=new File(nationality3);
	          	if(nationality3File.exists()){
	          		docUpload.put("insured_nationality_three_idv_previous_upload","true");
	          	}
        	}
          	if(!docUpload.containsKey("insured_nationality_two_idv_previous_upload")){
	        	String nationality2=String.format("%s/%s-InsuredNationalityTwoIDV."+ext, System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId());
	          	File nationality2File=new File(nationality2);
	          	if(nationality2File.exists()){
	          		docUpload.put("insured_nationality_two_idv_previous_upload","true");
	          	}
          	}
          	if(!docUpload.containsKey("applicant_nationality_three_idv_previous_upload")){
	          	String appnationality3=String.format("%s/%s-ApplicantNationalityThreeIDV."+ext, System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId());
	          	File appnationality3File=new File(appnationality3);
	          	if(appnationality3File.exists()){
	          		docUpload.put("applicant_nationality_three_idv_previous_upload","true");
	          	}
          	}
          if(!docUpload.containsKey("applicant_nationality_two_idv_previous_upload")){
          	String appnationality2=String.format("%s/%s-ApplicantNationalityTwoIDV."+ext, System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId());
          	File appnationality2File=new File(appnationality2);
          	if(appnationality2File.exists()){
          		docUpload.put("applicant_nationality_two_idv_previous_upload","true");
          	}
          }
          if(!docUpload.containsKey("beneficial_idv_previous_upload")){
          	String beneficial=String.format("%s/%s-BeneficalOwnerIDV."+ext, System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId());
          	File beneficialFile=new File(beneficial);
          	if(beneficialFile.exists()){
          		docUpload.put("beneficial_idv_previous_upload","true");
          	}
          }
        }

        	request.setAttribute("docUpload", docUpload);
        	
        
            data.put("cc_type", application.getCreditCardType());
            data.put("cc_name", application.getCreditCardName());
            data.put("cc_num", application.getCreditCardNum());
            String ccExpiry = application.getCreditCardExpiry();
            if (ccExpiry != null && !ccExpiry.isEmpty()) {
                data.put("cc_expiry_month", ccExpiry.split("/")[0]);
                data.put("cc_expiry_year", ccExpiry.split("/")[1]);
            }

            Map premiums = application.getPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
            double premiumModal = (double)premiums.get("modalPremium");
            double premiumAnnual = premiumModal * application.getPaymentMode();

            double ciRiderPremiumModal = 0.0;
            double ciRiderPremiumAnnual = 0.0;
            if (application.getIncludeCriticalIllnessRider()) {
                premiums = application.getCriticalIllnessRiderPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));             
                ciRiderPremiumModal = (double)premiums.get("modalPremium");
                ciRiderPremiumAnnual = ciRiderPremiumModal * application.getPaymentMode();
            }

            float ropRiderPremiumModal = 0;
            float ropRiderPremiumAnnual = 0;
            if (application.getIncludeRefundRider()) {
                premiums = application.getRefundRiderPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
                ropRiderPremiumModal = (float)premiums.get("modalPremium");
                ropRiderPremiumAnnual = ropRiderPremiumModal * application.getPaymentMode();
            }

            DecimalFormat df = new DecimalFormat("S$#,###.00");
            DecimalFormat df1 = new DecimalFormat("####.00");
            String premiumModalString=df1.format(premiumModal);
            float premiumModalTwo=Float.parseFloat(premiumModalString);
            data.put("summary_modal", df.format(premiumModal));
            
            String premiumAnnualString=df1.format(premiumAnnual);
            float premiumAnnualTwo=Float.parseFloat(premiumAnnualString);
            
            data.put("summary_annually", df.format(premiumAnnual));
            
            String ciRiderPremiumModalString=df1.format(ciRiderPremiumModal);
            float ciRiderPremiumModalTwo=Float.parseFloat(ciRiderPremiumModalString);
            
            data.put("summary_ci_modal", df.format(ciRiderPremiumModal));
            
            String ciRiderPremiumAnnualString=df1.format(ciRiderPremiumAnnual);
            float ciRiderPremiumAnnualTwo=Float.parseFloat(ciRiderPremiumAnnualString);
            
            data.put("summary_ci_annually", df.format(ciRiderPremiumAnnual));
            
            String ropRiderPremiumModalString=df1.format(ropRiderPremiumModal);
            float ropRiderPremiumModalTwo=Float.parseFloat(ropRiderPremiumModalString);
            
            data.put("summary_rop_modal", df.format(ropRiderPremiumModal));
            
            String ropRiderPremiumAnnualString=df1.format(ropRiderPremiumAnnual);
            float ropRiderPremiumAnnualTwo=Float.parseFloat(ropRiderPremiumAnnualString);
            data.put("summary_rop_annually", df.format(ropRiderPremiumAnnual));
            
            data.put("summary_total_modal", df.format(premiumModalTwo + ciRiderPremiumModalTwo + ropRiderPremiumModalTwo));
            
            data.put("summary_total_annually",df.format( premiumAnnualTwo + ciRiderPremiumAnnualTwo + ropRiderPremiumAnnualTwo));

            request.setAttribute("data", data);
        } catch (Exception e) {
            ErrorHandler.handleError(application, e);
        }
    }
    
    /**
     * This method checks whether user has uploaded a copy of the back of nric for applicant, validates 
     * the format to be JPEG and saves it to the temp folder.
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantBackNricUpload(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, false);
        try {
            Part part = request.getPart(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD);
            if (part != null && part.getSize() > 0) {
                InputStream is = part.getInputStream();
                String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
                if ( !Validator.isJPEG(is,contentType) ) {
                    is.close();
                    sanitized.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, false);
                    return sanitized;
                }
                //close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                OutputStream os = new FileOutputStream(String.format("%s/%s-ApplicantIDVBackNric."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
                try {
                	byte[] buffer = new byte[1024];
                	int bytesRead;
	                while((bytesRead = is.read(buffer)) !=-1){
	                    os.write(buffer, 0, bytesRead);
	                }
                } finally {
                	is.close();
                	os.flush();
                	os.close();
                }
                sanitized.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, true);
                sanitized.put("applicant_idv_back_nric_previous_upload",true);
            }else{
            	
            	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
            	  for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
            		
            		 String idvField=String.format("%s/%s-ApplicantIDVBackNric."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                     
                     File idvFile=new File(idvField);
                     if(idvFile.exists()){
                         sanitized.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, true);
                         sanitized.put("applicant_idv_back_nric_previous_upload",true);
                         break;
                     }
            		
            	}
           

        }
        } catch (Exception e) {
            logger.error("sanitizeApplicantBackNricUpload:",e);
        }

        return sanitized;
    }
    
    /**
     * This method checks whether user has uploaded a copy of the back of nric for the insured, validates 
     * the format to be JPEG and saves it to the temp folder.
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantBackNricInsuredUpload(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized.put(FieldConstants.INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD, false);
        try {
            Part part = request.getPart(FieldConstants.INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD);
            if (part != null && part.getSize() > 0) {
                InputStream is = part.getInputStream();
                String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
                if ( !Validator.isJPEG(is,contentType) ) {
                    is.close();
                    sanitized.put(FieldConstants.INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD, false);
                    return sanitized;
                }
                //close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                OutputStream os = new FileOutputStream(String.format("%s/%s-InsuredIDVBackNric."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
                try {
                	byte[] buffer = new byte[1024];
                	int bytesRead;
	                while((bytesRead = is.read(buffer)) !=-1){
	                    os.write(buffer, 0, bytesRead);
	                }
                } finally {
                	is.close();
                	os.flush();
                	os.close();
                }
                sanitized.put(FieldConstants.INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD, true);
                sanitized.put("insured_idv_back_nric_previous_upload",true);
            }else{ 	
            	
            	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
            	  for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
            		
            		 String idvField=String.format("%s/%s-InsuredIDVBackNric."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                     
                     File idvFile=new File(idvField);
                     if(idvFile.exists()){
                    	 sanitized.put(FieldConstants.INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD, true);
                 		sanitized.put("insured_idv_back_nric_previous_upload",true);
                         break;
                     }
            		

            	}
            }
        } catch (Exception e) {
            logger.error("sanitizeApplicantBackNricInsuredUpload:",e);
        }

        return sanitized;
    }

}
