package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.AgeUtils;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/3", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step3.jsp")
})
public class Step3Servlet extends BaseServlet {

	private final String DOB_FORMAT = "dd/MM/yyyy";

    public float getCurrentStep() {
        return 3;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizeApplicantResidency(request, sanitized);
        sanitized = sanitizeApplicantFirstName(request, sanitized);
        sanitized = sanitizeApplicantLastName(request, sanitized);
        sanitized = sanitizeApplicantDob(request, sanitized);
        sanitized = sanitizeLifeInsured(request, sanitized);
        sanitized = sanitizeInsuredResidency(request, sanitized);
        sanitized = sanitizeInsuredFirstName(request, sanitized);
        sanitized = sanitizeInsuredLastName(request, sanitized);
        sanitized = sanitizeInsuredDob(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();

        errors = validateApplicantResidency(data, errors);
        errors = validateApplicantFirstName(data, errors);
        errors = validateApplicantLastName(data, errors);
        errors = validateApplicantDob(data, errors);
        errors = validateLifeInsured(data, errors);
        errors = validateInsuredResidency(data, errors);
        errors = validateInsuredFirstName(data, errors);
        errors = validateInsuredLastName(data, errors);
        errors = validateInsuredDob(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setLifeInsured(data.get("life_insured").toString());
        application.setApplicantResidency(data.get("applicant_residency").toString());
       
        application.setApplicantFirstName(data.get("applicant_first_name").toString());
        application.setApplicantLastName(data.get("applicant_last_name").toString());
        if (!data.get("life_insured").toString().equals("self") && data.get("insured_residency") != null) {
            application.setInsuredResidency(data.get("insured_residency").toString());
        } else {
            application.setInsuredResidency(null);
        }
        if (!data.get("life_insured").toString().equals("self") && data.get("insured_first_name") != null) {
            application.setInsuredFirstName(data.get("insured_first_name").toString());
        } else {
            application.setInsuredFirstName(null);
        }
        if (!data.get("life_insured").toString().equals("self") && data.get("insured_last_name") != null) {
            application.setInsuredLastName(data.get("insured_last_name").toString());
        } else {
            application.setInsuredLastName(null);
        }

        try {
            Date applicantDob = (new SimpleDateFormat(DOB_FORMAT)).parse(String.format("%s", data.get("applicant_dob")));
            application.setApplicantDob(applicantDob);
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }

        if (!data.get("life_insured").toString().equals("self")) {
            try {
                Date insuredDob = (new SimpleDateFormat(DOB_FORMAT)).parse(String.format("%s", data.get("insured_dob")));
                application.setInsuredDob(insuredDob);
            } catch (ParseException e) {
                application.setInsuredDob(null);
            }
        } else {
            application.setInsuredDob(null);
        }

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateApplicantResidency(Map<String, Object> data, Map<String, String> errors) {
        String input = (data.get("applicant_residency") != null ? data.get("applicant_residency").toString() : "");
        boolean hasError = false;
        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_residency", "Your current residency status is missing or invalid.");
                hasError = true;
                break;
        }

        if (!hasError) {
            if (input.equalsIgnoreCase("NA")) {
                errors.put("applicant_residency", "Your current residency status is invalid.");
                errors.put("dropout", "1");

            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantFirstName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("applicant_first_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_first_name", "Your given name is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("applicant_first_name", "Your given name is too long.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("applicant_first_name", "Your given name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantLastName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("applicant_last_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_last_name", "Your surname is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("applicant_last_name", "Your surname is too long.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("applicant_last_name", "Your surname contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantDob(Map<String, Object> data, Map<String, String> errors) {
        boolean hasError = false;
        switch (Validator.validateDate((String)data.get("applicant_dob"))) {
            case Validator.ERROR_DATE_EMPTY:
                errors.put("applicant_dob", StringConstants.EMPTY_DATE);
                hasError = true;
                break;
            case Validator.ERROR_DATE_INVALID:
                errors.put("applicant_dob", "Your date of birth is invalid.");
                hasError = true;
                break;
        }

        if (!hasError) {
            if (data.get("life_insured") == null || data.get("life_insured").toString().equals("self")) {
                String dateString = String.format("%s", data.get("applicant_dob"));
                DateFormat df = new SimpleDateFormat(DOB_FORMAT);
                try {
                    int anb = AgeUtils.calculateAgeNextBirthday(df.parse(dateString));
                    if (anb < 19 || anb > 60) {
                        errors.put("applicant_dob", "We are unable to proceed with your online application.");
                        errors.put("dropout", "1");
                    }
                } catch (Exception e) {
                    errors.put("applicant_dob", "Your date of birth is invalid.");
                }
            }
        }

        return errors;
    }

    private Map<String, String> validateLifeInsured(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("life_insured"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("life_insured", "Please indicate who is the life insured.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredResidency(Map<String, Object> data, Map<String, String> errors) {
    	 String input = (data.get("insured_residency") != null ? data.get("insured_residency").toString() : "");
         boolean hasError = false;
        if (data.get("life_insured") == null || data.get("life_insured").toString().equals("self")) {
            return errors;
        }
        switch (Validator.validateString(data.get("insured_residency"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_residency", "Your spouse's residency status is missing or invalid.");
                break;
        }
        if (!hasError) {
          if (input.equalsIgnoreCase("NA")) {
              errors.put("insured_residency", "Your spouse's residency status is invalid.");
              errors.put("dropout", "1");

          }
      }
        return errors;
    }

    private Map<String, String> validateInsuredFirstName(Map<String, Object> data, Map<String, String> errors) {
        if (data.get("life_insured") == null || data.get("life_insured").toString().equals("self")) {
            return errors;
        }

        switch (Validator.validateName(data.get("insured_first_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_first_name", "Your spouse's given name is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("insured_first_name", "Your spouse's given name is too long.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("insured_first_name", "Your spouse's given name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredLastName(Map<String, Object> data, Map<String, String> errors) {
        if (data.get("life_insured") == null || data.get("life_insured").toString().equals("self")) {
            return errors;
        }

        switch (Validator.validateName(data.get("insured_last_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_last_name", "Your spouse's surname is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("insured_last_name", "Your spouse's surname is too long.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("insured_last_name", "Your spouse's surname contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredDob(Map<String, Object> data, Map<String, String> errors) {
        if (data.get("life_insured") == null || data.get("life_insured").toString().equals("self")) {
            return errors;
        }

        boolean hasError = false;

        switch (Validator.validateDate((String)data.get("insured_dob"))) {
            case Validator.ERROR_DATE_EMPTY:
                errors.put("insured_dob", StringConstants.EMPTY_DATE);
                hasError = true;
                break;
            case Validator.ERROR_DATE_INVALID:
                errors.put("insured_dob", "Your spouse's date of birth is invalid.");
                hasError = true;
                break;
        }

        if (!hasError) {
            String dateString = String.format("%s", data.get("insured_dob"));
            DateFormat df = new SimpleDateFormat(DOB_FORMAT);
            try {
                int anb = AgeUtils.calculateAgeNextBirthday(df.parse(dateString));
                if (anb < 19 || anb > 60) {
                    errors.put("insured_dob", "We are unable to proceed with your online application.");
                    errors.put("dropout", "1");
                }
            } catch (Exception e) {
                errors.put("insured_dob", "Your date of birth is invalid.");
            }
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantResidency(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_residency");
        String[] options = new String[] {
                "Singaporean",
                "Singapore PR",
                "Employment Pass",
                "Skilled Pass",
                "Personalised Employment Pass",
                "Dependent Pass",
                "Student Pass",
                "NA"
        };
        if (!Arrays.asList(options).contains(input)) {
            input = null;
        }

        sanitized.put("applicant_residency", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantFirstName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_first_name");
        sanitized.put("applicant_first_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantLastName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_last_name");
        sanitized.put("applicant_last_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantDob(HttpServletRequest request, Map<String, Object> sanitized) {
        String dob = request.getParameter("applicant_dob");

        sanitized.put("applicant_dob", dob);
        return sanitized;
    }

    private Map<String, Object> sanitizeLifeInsured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("life_insured");
        String[] options = new String[] { "self", "spouse" };
        if (!Arrays.asList(options).contains(input)) {
            input = null;
        }

        sanitized.put("life_insured", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredResidency(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_residency");
        String[] options = new String[] {
                "Singaporean",
                "Singapore PR",
                "Employment Pass",
                "Skilled Pass",
                "Personalised Employment Pass",
                "Dependent Pass",
                "Student Pass",
                "NA"
        };
        if (!Arrays.asList(options).contains(input)) {
            input = null;
        }

        sanitized.put("insured_residency", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredFirstName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_first_name");
        sanitized.put("insured_first_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredLastName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_last_name");
        sanitized.put("insured_last_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredDob(HttpServletRequest request, Map<String, Object> sanitized) {
        String dob = request.getParameter("insured_dob");

        sanitized.put("insured_dob", dob);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("life_insured", application.getLifeInsured());
        data.put("applicant_residency", application.getApplicantResidency());
        data.put("applicant_first_name", application.getApplicantFirstName());
        data.put("applicant_last_name", application.getApplicantLastName());

        data.put("insured_residency", application.getInsuredResidency());
        data.put("insured_first_name", application.getInsuredFirstName());
        data.put("insured_last_name", application.getInsuredLastName());

        Date applicantDob = application.getApplicantDob();
        if (applicantDob != null) {
            data.put("applicant_dob", (new SimpleDateFormat(DOB_FORMAT)).format(applicantDob));
        }

        Date insuredDob = application.getInsuredDob();
        if (insuredDob != null) {
            data.put("insured_dob", (new SimpleDateFormat(DOB_FORMAT)).format(insuredDob));
        }

        request.setAttribute("data", data);
    }

}
