package sg.com.hsbc.dreamcatcher.eclaims.terminalillness;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimBaseServlet;
import sg.com.hsbc.dreamcatcher.eclaims.IllnessApplication;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

public abstract class BaseServlet extends EClaimBaseServlet {
    static String FORM_TITLE = "eClaims - Terminal Illness - Make a Claim";
    public static String APPLICATION_SESSION_ATTRIBUTE_NAME = "EClaimTerminalIllness";

    public String getFormTitle() {
        return BaseServlet.FORM_TITLE;
    }

    public String getApplicationSessionAttributeName() {
        return BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME;
    }

    public String getBaseFormUrl() {
        return "/eclaims/4-terminal-illness";
    }

    public ApplicationForm getById(String id) throws SQLException {
        return TerminalIllnessApplication.getById(id);
    }

    public IllnessApplication getApplication(HttpServletRequest request) {
        return (IllnessApplication)super.getApplication(request);
    }
}
