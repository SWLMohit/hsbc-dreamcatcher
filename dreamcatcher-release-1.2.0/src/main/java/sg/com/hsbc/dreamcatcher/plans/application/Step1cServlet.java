package sg.com.hsbc.dreamcatcher.plans.application;

import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = "/our-plans/onlineinvestor-application-step1c/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/jsp/view/plans/application/step1c.jsp")
})
public class Step1cServlet extends BaseFormServlet {
}
