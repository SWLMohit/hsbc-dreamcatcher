package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoaccident;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/6-tpd-due-to-accident/1a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/6_tpd_due_to_accident/step1b.jsp")
})
@MultipartConfig
public class Step1bServlet extends BaseServlet {
    public float getCurrentStep() {
        return (float)1.5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        String input = request.getParameter("accident_date_day");
        sanitized.put("accident_date_day", input);

        input = request.getParameter("accident_place");
        sanitized.put("accident_place", input);
        input = request.getParameter("accident_detail");
        sanitized.put("accident_detail", input);
        handleUploadFile(request, sanitized, "accident_document", "AccidentDocument");
        handleUploadFile(request, sanitized, "clinical_abstract_form", "ClinicalAbstractForm");
        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        TpdDueToAccidentApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        try {
            Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", data.get("accident_date_day")));
            application.setAccidentDate(date);
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }
        application.setAccidentPlace(data.get("accident_place").toString());
        application.setAccidentDetail(data.get("accident_detail").toString());
        if (!data.get("accident_document").toString().equals("")) {
            application.setAccidentDocument(data.get("accident_document").toString());
        }

        if (!data.get("clinical_abstract_form").toString().equals("")) {
            application.setClinicalAbtractForm(data.get("clinical_abstract_form").toString());
        }

        
        /** 10MB File size implementation starts here **/ 
        if(application.getFileInfo()!=null){
          Map<String, Long>updatedFileInfo=application.getFileInfo();
          if(data.get("fileInfo")!=null) {
          updatedFileInfo.putAll((Map<String,Long>)data.get("fileInfo"));
          application.setFileInfo(updatedFileInfo);
          }
        }
        else {
          application.setFileInfo((Map<String,Long>)data.get("fileInfo")); 
        }
        
        /** 10MB File size implementation ends here **/ 


        application.setMaxStep(getCurrentStep() + getStep());
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        TpdDueToAccidentApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        Date date = application.getAccidentDate();
        if (date != null) {
            data.put("accident_date_day", (new SimpleDateFormat("dd/MM/y")).format(date));
        }
        data.put("accident_place", application.getAccidentPlace());
        data.put("accident_detail", application.getAccidentDetail());
        data.put("accident_document", application.getAccidentDocument());
        data.put("clinical_abstract_form", application.getClinicalAbtractForm());
        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("day_list", getDayList());
        data.put("month_list", getMonthList());
        data.put("year_list", getYearList());

        request.setAttribute("formData", data);
    }

    protected float getStep() {
        return (float)0.4;
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(getBaseFormUrl() + "/1b");  // 1c
    }
}
