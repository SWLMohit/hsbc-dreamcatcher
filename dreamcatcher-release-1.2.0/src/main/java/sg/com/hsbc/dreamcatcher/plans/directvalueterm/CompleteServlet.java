package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.commons.lang.StringUtils;

import sg.com.hsbc.dreamcatcher.helpers.Config;
import sg.com.hsbc.dreamcatcher.helpers.EmailSender;
import sg.com.hsbc.dreamcatcher.helpers.EmailSenderThread;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/complete", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/complete.jsp")
})
public class CompleteServlet extends BaseServlet  {

	
    
    public float getCurrentStep() {
        return 15;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        sanitized = sanitizeMailOption(request, sanitized);
        sanitized.put("dvt-rate-exp", request.getParameter("dvt-rate-exp"));
        sanitized.put("dvt-rate-ease", request.getParameter("dvt-rate-ease"));
        sanitized.put("feedback_recommend", request.getParameter("feedback_recommend"));
        sanitized.put("feedback_norecommend_reason", request.getParameter("feedback_norecommend_reason"));
        sanitized.put("feedback_improve", request.getParameter("feedback_improve"));
        sanitized.put("feedback_additional", request.getParameter("feedback_additional"));

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        if (data.get("mail_option") != null) {
            application.setMailOption(Integer.parseInt(data.get("mail_option").toString()));
        }

        if (data.get("dvt-rate-exp") != null && !data.get("dvt-rate-exp").toString().isEmpty()) {
            application.setFeedbackExperience(Integer.parseInt(data.get("dvt-rate-exp").toString()));
        }

        if (data.get("dvt-rate-ease") != null && !data.get("dvt-rate-ease").toString().isEmpty()) {
            application.setFeedbackEase(Integer.parseInt(data.get("dvt-rate-ease").toString()));
        }

        if (data.get("feedback_recommend") != null && !data.get("feedback_recommend").toString().isEmpty()) {
            application.setFeedbackRecommend(data.get("feedback_recommend").toString().equalsIgnoreCase("Y"));
        }

        if (data.get("feedback_norecommend_reason") != null && !data.get("feedback_norecommend_reason").toString().isEmpty()) {
            application.setFeedbackNoRecommendReason(data.get("feedback_norecommend_reason").toString());
        }

        if (data.get("feedback_improve") != null && !data.get("feedback_improve").toString().isEmpty()) {
            application.setFeedbackImprove(data.get("feedback_improve").toString());
        }

        if (data.get("feedback_additional") != null && !data.get("feedback_additional").toString().isEmpty()) {
            application.setFeedbackAdditional(data.get("feedback_additional").toString());
        }

        application.setMaxStep(getCurrentStep() + 1);
        // Save into feedback table only
        application.save();
        application.saveFeedback();

        EmailSender.sendEmailUsingApplication(application,"DVT", this);
        return true;
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("/our-plans/direct-value-term/feedback");
        return;
    }

    private Map<String, Object> sanitizeMailOption(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("mail_option");

        sanitized.put("mail_option", input);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("mail_option", application.getMailOption());

        request.setAttribute("data", data);
    }

}
