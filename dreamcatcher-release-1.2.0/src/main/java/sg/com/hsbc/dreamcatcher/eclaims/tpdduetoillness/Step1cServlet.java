package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoillness;

import sg.com.hsbc.dreamcatcher.eclaims.DoctorPortion;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/5-tpd-due-to-illness/1b", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/5_tpd_due_to_illness/step1c.jsp")
})
@MultipartConfig
public class Step1cServlet extends BaseServlet {
    public float getCurrentStep() {
        return (float)1.9;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeDoctor(request, sanitized);
        sanitized = sanitizeDocument(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizeDoctor(HttpServletRequest request, Map<String, Object> sanitized) {
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        String[] fromDate = request.getParameterValues("doctor_from_date");
        String[] toDate = request.getParameterValues("doctor_to_date");

        String[] doctorNames = request.getParameterValues("doctor_name");
        String[] doctorHospitals = request.getParameterValues("doctor_hospital");
        String[] doctorHospitalStates = request.getParameterValues("doctor_hospital_state");
        String[] doctorHospitalStreet1s = request.getParameterValues("doctor_hospital_street1");
        String[] doctorHospitalStreet2s = request.getParameterValues("doctor_hospital_street2");

        for (int i = 0; i < doctorNames.length; i++) {
            Map<String, Object> subData = new HashMap<>();
            subData.put("doctor_from_date", fromDate[i]);
            subData.put("doctor_to_date", toDate[i]);
            subData.put("doctor_name", doctorNames[i]);
            subData.put("doctor_hospital", doctorHospitals[i]);
            subData.put("doctor_hospital_state", doctorHospitalStates[i]);
            subData.put("doctor_hospital_street1", doctorHospitalStreet1s[i]);
            subData.put("doctor_hospital_street2", doctorHospitalStreet2s[i]);

            list.add(subData);
        }

        sanitized.put("doctor_list", list.toArray());

        return sanitized;
    }

    private Map<String, Object> sanitizeDocument(HttpServletRequest request, Map<String, Object> sanitized) {

        handleUploadFile(request, sanitized, "medical_report", "MedicalReport");
        handleUploadFile(request, sanitized, "clinical_abstract_form", "ClinicalAbstractForm");

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        TpdDueToIllnessApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        ArrayList<DoctorPortion> list = new ArrayList<>();
        DoctorPortion[] oldList = application.getDoctorList();
        int i = 0;
        for (Object subDataItem : (Object[]) data.get("doctor_list")) {
            DoctorPortion portion;
            if (i < oldList.length) {
                portion = oldList[i];
            } else {
                portion = new DoctorPortion();
            }
            Map<String, Object> subData = (Map<String, Object>) subDataItem;

            try {
                Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s/%s", "01", subData.get("doctor_from_date")));
                portion.setFromDate(date);
            } catch (ParseException e) {
                ErrorHandler.handleError(application, e);
                return false;
            }
            try {
                Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s/%s", "01", subData.get("doctor_to_date")));
                portion.setToDate(date);
            } catch (ParseException e) {
                ErrorHandler.handleError(application, e);
                return false;
            }

            portion.setName(subData.get("doctor_name").toString());
            portion.setHospital(subData.get("doctor_hospital").toString());
            portion.setHospitalCity(subData.get("doctor_hospital_state").toString());
            portion.setHospitalStreet1(subData.get("doctor_hospital_street1").toString());
            portion.setHospitalStreet2(subData.get("doctor_hospital_street2").toString());

            list.add(portion);
            i++;
        }
        application.setDoctorList(list.toArray(new DoctorPortion[list.size()]));
        if (!data.get("medical_report").toString().equals("")) {
            application.setMedicalReport(data.get("medical_report").toString());
        }
        if (!data.get("clinical_abstract_form").toString().equals("")) {
            application.setClinicalAbtractForm(data.get("clinical_abstract_form").toString());
        }
        
        
        /** 10MB File size implementation starts here **/ 
        if(application.getFileInfo()!=null){
          Map<String, Long>updatedFileInfo=application.getFileInfo();
          if(data.get("fileInfo")!=null) {
          updatedFileInfo.putAll((Map<String,Long>)data.get("fileInfo"));
          application.setFileInfo(updatedFileInfo);
          }
        }
        else {
          application.setFileInfo((Map<String,Long>)data.get("fileInfo")); 
        }
        /** 10MB File size implementation ends here **/ 

        application.setMaxStep(getCurrentStep() + getStep());
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        TpdDueToIllnessApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (DoctorPortion portion : application.getDoctorList()) {
            Map<String, Object> subData = new HashMap<>();

            Date date = portion.getFromDate();
            if (date != null) {
                subData.put("doctor_from_date", (new SimpleDateFormat("MM/y")).format(date));
            }
            date = portion.getToDate();
            if (date != null) {
                subData.put("doctor_to_date", (new SimpleDateFormat("MM/y")).format(date));
            }

            subData.put("doctor_name", portion.getName());
            subData.put("doctor_hospital", portion.getHospital());
            subData.put("doctor_hospital_state", portion.getHospitalCity());
            subData.put("doctor_hospital_street1", portion.getHospitalStreet1());
            subData.put("doctor_hospital_street2", portion.getHospitalStreet2());

            list.add(subData);
        }
        data.put("doctor_list", list.toArray());
        data.put("medical_report", application.getMedicalReport());
        data.put("clinical_abstract_form", application.getClinicalAbtractForm());

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("country_list", getCountryList());
        data.put("day_list", getDayList());
        data.put("month_list", getMonthList());
        data.put("year_list", getYearList());

        request.setAttribute("formData", data);
    }

    protected float getStep() {
        return (float)0.1;
    }
}
