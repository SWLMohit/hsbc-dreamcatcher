package sg.com.hsbc.dreamcatcher.eclaims;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DeathIndexProcessor {
    protected EClaimBaseServlet servletInstance;

    DeathIndexProcessor() {}

    public DeathIndexProcessor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizePolicyNumber(request, sanitized);
        sanitized = sanitizePolicyownerName(request, sanitized);
        sanitized = sanitizePolicyownerNRICNumber(request, sanitized);
        sanitized = sanitizePolicyownerBirthday(request, sanitized);
        sanitized = sanitizePolicyownerPassport(request, sanitized);
        sanitized = sanitizePolicyownerContact(request, sanitized);
        sanitized = sanitizePolicyownerEmail(request, sanitized);
        sanitized = sanitizePolicyownerLifeInsured(request, sanitized);

        sanitized = moreSanitize(request, sanitized);

        return sanitized;
    }

    // For sub class extension
    Map<String, Object> moreSanitize(HttpServletRequest request, Map<String, Object> sanitized) {
        return sanitized;
    }

    private Map<String, Object> sanitizePolicyNumber(HttpServletRequest request, Map<String, Object> sanitized) {
        String[] input = request.getParameterValues("policy_number");
        sanitized.put("policy_number_list", input);

        return sanitized;
    }

    private Map<String, Object> sanitizePolicyownerName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("policyowner_first_name");
        sanitized.put("policyowner_first_name", input);

        input = request.getParameter("policyowner_last_name");
        sanitized.put("policyowner_last_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizePolicyownerNRICNumber(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("policy_owner_nric");
        sanitized.put("policy_owner_nric", input);

        return sanitized;
    }
    private Map<String, Object> sanitizePolicyownerBirthday(HttpServletRequest request, Map<String, Object> sanitized) {

        String birthDay = request.getParameter("policy_owner_birth_day");
        sanitized.put("policy_owner_birth_day", birthDay);

        return sanitized;
    }
    private Map<String, Object> sanitizePolicyownerPassport(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized=servletInstance.sanitizeUploadedDocument(request, sanitized, "policyowner_passport", "NRICOrPassport");

        return sanitized;
    }

    private Map<String, Object> sanitizePolicyownerContact(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("policyowner_contact_country");
        sanitized.put("policyowner_contact_country", input);

        input = request.getParameter("policyowner_contact_number");
        sanitized.put("policyowner_contact_number", input);

        return sanitized;
    }

    private Map<String, Object> sanitizePolicyownerEmail(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("policyowner_email");
        sanitized.put("policyowner_email", input);

        return sanitized;
    }

    private Map<String, Object> sanitizePolicyownerLifeInsured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("policyowner_life_insured");
        boolean policyownerLifeInsured = Boolean.parseBoolean(input);
        sanitized.put("policyowner_life_insured", policyownerLifeInsured);
        if (!policyownerLifeInsured) {
            input = request.getParameter("policyowner_life_insured_first_name");
            sanitized.put("policyowner_life_insured_first_name", input);
            input = request.getParameter("policyowner_life_insured_last_name");
            sanitized.put("policyowner_life_insured_last_name", input);
            servletInstance.sanitizeUploadedDocument(request, sanitized, "policyowner_life_insured_passport", "LifeInsuredNRICOrPassport");
        }

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setPolicyNumberList((String[])data.get("policy_number_list"));
        application.setPolicyownerFirstName(data.get("policyowner_first_name").toString());
        application.setPolicyownerLastName(data.get("policyowner_last_name").toString());
        application.setPolicyownerNRIC(data.get("policy_owner_nric").toString());
        try {
            Date policyownerBirthday = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", data.get("policy_owner_birth_day")));
            application.setPolicyownerBirthday(policyownerBirthday);
            application.setPolicyownerDay(String.format("%s", data.get("policy_owner_birth_day")));
            application.setPolicyownerMonth(String.format("%s", data.get("policy_owner_birth_month")));
            application.setPolicyownerYear(String.format("%s", data.get("policy_owner_birth_year")));
            
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }
        
        /** 10MB File size limit implementation starts here **/ 
        if(application.getFileInfo()!=null){
          Map<String, Long>updatedFileInfo=application.getFileInfo();
          if(data.get("fileInfo")!=null) {
          updatedFileInfo.putAll((Map<String,Long>)data.get("fileInfo"));
          application.setFileInfo(updatedFileInfo);
          }
        }
        else {
          application.setFileInfo((Map<String,Long>)data.get("fileInfo")); 
        }
        /** 10MB File size limit implementation ends here **/ 
        application.setPolicyownerPassport(servletInstance.docList(request, data, "policyowner_passport", "NRICOrPassport"));
        application.setPolicyownerContactCountry(data.get("policyowner_contact_country").toString());
        application.setPolicyownerContactNumber(data.get("policyowner_contact_number").toString());
        application.setPolicyownerEmail(data.get("policyowner_email").toString());
        application.setPolicyownerLifeInsured((boolean)data.get("policyowner_life_insured"));
        if (!application.isPolicyownerLifeInsured()) {
            application.setPolicyownerLifeInsuredFirstName(data.get("policyowner_life_insured_first_name").toString());
            application.setPolicyownerLifeInsuredLastName(data.get("policyowner_life_insured_last_name").toString());
            
              application.setPolicyownerLifeInsuredPassport(servletInstance.docList(request, data, "policyowner_life_insured_passport", "LifeInsuredNRICOrPassport"));
        } else {
            application.setPolicyownerLifeInsuredFirstName(null);
            application.setPolicyownerLifeInsuredLastName(null);
            application.setPolicyownerLifeInsuredPassport(null);
        }

        moreProcess(request, application, data);

        application.setMaxStep(servletInstance.getCurrentStep());
        return application.save();
    }

    // For sub class extension
    void moreProcess(HttpServletRequest request, EClaimApplication application, Map<String, Object> data) {

    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("policy_number_list", application.getPolicyNumberList());
        data.put("policyowner_first_name", application.getPolicyownerFirstName());
        data.put("policyowner_last_name", application.getPolicyownerLastName());
        data.put("policy_owner_nric", application.getPolicyownerNRIC());
        Date policyownerBirthday = application.getPolicyownerBirthday();
        if (policyownerBirthday != null) {
            data.put("policy_owner_birth_day", (new SimpleDateFormat("dd/MM/y")).format(policyownerBirthday));
        }
        data.put("policyowner_passport", application.getPolicyownerPassport());
        data.put("policyowner_contact_country", application.getPolicyownerContactCountry());
        data.put("policyowner_contact_number", application.getPolicyownerContactNumber());
        data.put("policyowner_email", application.getPolicyownerEmail());
        data.put("policyowner_life_insured", application.isPolicyownerLifeInsured());
        if (application.isPolicyownerLifeInsured()!=null &&!application.isPolicyownerLifeInsured()) {
            data.put("policyowner_life_insured_first_name", application.getPolicyownerLifeInsuredFirstName());
            data.put("policyowner_life_insured_last_name", application.getPolicyownerLifeInsuredLastName());
            data.put("policyowner_life_insured_passport", application.getPolicyownerLifeInsuredPassport());
        }

        morePopulateDataFromObject(request, application, data);

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("day_list", servletInstance.getDayList());
        data.put("month_list", servletInstance.getMonthList());
        data.put("year_list", servletInstance.getYearList());
        data.put("past_year_list", servletInstance.getYearList(1900, Calendar.getInstance().get(Calendar.YEAR)));

        request.setAttribute("formData", data);
    }

    // For sub class extension
    void morePopulateDataFromObject(HttpServletRequest request, EClaimApplication application, Map<String, Object> data) {

    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(servletInstance.getBaseFormUrl() + "/0");
    }
}
