package sg.com.hsbc.dreamcatcher.helpers;

import be.quodlibet.boxable.*;
import be.quodlibet.boxable.line.LineStyle;
import sg.com.hsbc.dreamcatcher.ApplicationStartUpListener;
import sg.com.hsbc.dreamcatcher.plans.Constants;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import be.quodlibet.boxable.image.Image;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

public class PlanDataTable {

    public static final Boolean HASHEADER = true;
    public static final Boolean NOHEADER = false;
    private Table table;
    private final Cell titleCellTemplate;
    private final Cell headerCellTemplate;
    private final Cell centreHeaderCellTemplate;
    private final Cell dataCellTemplateEven;
    private final Cell dataCellTemplateOdd;
    private final Cell firstColumnCellTemplate;
    private final Cell lastColumnCellTemplate;
    private final Cell defaultCellTemplate;
    private final static Logger logger = Logger.getLogger(PlanDataTable.class);
    public PlanDataTable(Table table, PDPage page) throws IOException {
        this(table, page, HorizontalAlignment.LEFT);
    }
    public PlanDataTable(Table table, PDPage page, HorizontalAlignment headerAlignment) throws IOException {
        this.table = table;
        PDDocument ddoc = new PDDocument();
        PDPage dpage = new PDPage();
        dpage.setMediaBox(page.getMediaBox());
        dpage.setRotation(page.getRotation());
        ddoc.addPage(dpage);
        BaseTable dummyTable = new BaseTable(10f, 10f, 10f, table.getWidth(), 10f, ddoc, dpage, false, false);
        Row dr = dummyTable.createRow(0f);

        titleCellTemplate = dr.createCell(10f, "A", headerAlignment, VerticalAlignment.TOP);
        headerCellTemplate = dr.createCell(10f, "A", headerAlignment, VerticalAlignment.TOP);
        centreHeaderCellTemplate = dr.createCell(10f, "A", HorizontalAlignment.CENTER, VerticalAlignment.TOP);
        dataCellTemplateEven = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
        dataCellTemplateOdd = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
        firstColumnCellTemplate = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
        lastColumnCellTemplate = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
        defaultCellTemplate = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.TOP);

        setDefaultStyles();
        ddoc.close();
    }

    private void setDefaultStyles() throws IOException {
        LineStyle thinline = new LineStyle(Color.BLACK, 0.75f);
        PDFGenerator pd = new PDFGenerator(null, PDFGenerator.PDF_TYPE_DUMMY);

        pd.setBuiltInFont(PDType1Font.HELVETICA_BOLD,9);
        titleCellTemplate.setFillColor(Color.LIGHT_GRAY);
        titleCellTemplate.setTextColor(Color.BLACK);
        titleCellTemplate.setFont(pd.getFont());
        
        titleCellTemplate.setBorderStyle(thinline);
        pd.setBuiltInFont(null,9);
        pd.setFont(PDFGenerator.FONT_BOLD);
        headerCellTemplate.setFillColor(Color.LIGHT_GRAY);
        headerCellTemplate.setTextColor(Color.BLACK);
        headerCellTemplate.setFont(pd.getFont());
        headerCellTemplate.setBorderStyle(thinline);

        pd.setFont(PDFGenerator.FONT_NORMAL);
        defaultCellTemplate.setTextColor(Color.BLACK);
        defaultCellTemplate.setFont(pd.getFont());
        defaultCellTemplate.setBorderStyle(thinline);

        dataCellTemplateEven.copyCellStyle(defaultCellTemplate);
        dataCellTemplateOdd.copyCellStyle(defaultCellTemplate);
        firstColumnCellTemplate.copyCellStyle(defaultCellTemplate);
        lastColumnCellTemplate.copyCellStyle(defaultCellTemplate);
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public Cell getTitleCellTemplate() {
        return titleCellTemplate;
    }

    public Cell getHeaderCellTemplate() {
        return headerCellTemplate;
    }

    public Cell getDataCellTemplateEven() {
        return dataCellTemplateEven;
    }

    public Cell getDataCellTemplateOdd() {
        return dataCellTemplateOdd;
    }

    public Cell getFirstColumnCellTemplate() {
        return firstColumnCellTemplate;
    }

    public Cell getLastColumnCellTemplate() {
        return lastColumnCellTemplate;
    }

    public void addListToTable(List<List> data, Boolean hasHeader, String title) throws IOException {
        HorizontalAlignment horizontalAlignment = HorizontalAlignment.CENTER;
        addListToTable(data, hasHeader, title, horizontalAlignment);
    }

    public void addListToTable(List<List> data, Boolean hasHeader, String title, HorizontalAlignment horizontalAlignment) throws IOException {
        char separator = ';';
        if (data == null || data.isEmpty()) {
            return;
        }
        String output = "";
        float firstColumnWidth=0.8f;
        float secondColumnWidth=1.08f;
        // Convert Map of arbitrary objects to a csv String
        for (List inputList : data) {
            for (Object v : inputList) {
            	if(v==null){
            		v="NA";
            	}
                String value = v+"";

                if (value.contains("" + separator)) {
                    // surround value with quotes if it contains the escape
                    // character
                    value = "\"" + value + "\"";
                }
                output += value + separator;
            }
            // remove the last separator
            output = removeLastChar(output);
            output += "\n";
        }
        if(StringUtils.equalsIgnoreCase(title, Constants.DECLARATION_HEADER_OP_DVT)){
        	firstColumnWidth=0.08f;
        	secondColumnWidth=1.78f;
        }
        addCsvToTable(output, hasHeader, separator, title, horizontalAlignment,firstColumnWidth,secondColumnWidth);
    }

    private static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }

    /**
     * Creates a table from the String of data provided.The string is broken down into
     * lists based on the ";" character.
     * Added changes for Declarations table .The image cell gets created if the checbox
     * is present which is indicated by <img-check> tag
     * @param data         data to be displayed
     * @param hasHeader    boolean value based on the table has a header or no
     * @param separator    the character based on which the data string is split using Csv parser
     * @param title        the title of the table
     * @param horizontalAlignment  the alignment of the text in the table
     * @param first         the width of the first cell in the row of the table
     * @param second        the width of the second cell in the row of the table
     * @throws IOException
     */
    public void addCsvToTable(String data, Boolean hasHeader, char separator, String title, HorizontalAlignment horizontalAlignment,float first,float second) throws IOException {
        Iterable<CSVRecord> records = CSVParser.parse(data, CSVFormat.EXCEL.withDelimiter(separator));
        int i = 0;
        Iterator<CSVRecord> csv=records.iterator();
        boolean headerAdded=false;
        LineStyle thinline = new LineStyle(Color.BLACK, 0.75f);
        File checkFile=null;
        File uncheckedFile=null;
        Image check=null;
        Image uncheck=null;
        if(StringUtils.equalsIgnoreCase(title, Constants.DECLARATION_HEADER_OP_DVT)){
        	checkFile=new File(String.format("%s/assets/images/icons-checkbox.png", ApplicationStartUpListener.rootPath));
        	uncheckedFile=new File(String.format("%s/assets/images/icons-unchecked.png", ApplicationStartUpListener.rootPath));
        	check= new Image(ImageIO.read(checkFile),10,9);
        	uncheck=new Image(ImageIO.read(uncheckedFile),10,9);
        }
        while (csv.hasNext()) {
        	CSVRecord line=csv.next();
            float colWidth = 100 / line.size();
            if (i == 0 && (hasHeader || (title != null && !title.isEmpty()))) {
                if (title != null && !title.isEmpty()) {
                	if(StringUtils.equalsIgnoreCase(title,Constants.DECLARATION_HEADER_OP_DVT )){
                        if(!headerAdded){
                        	headerAdded=true;
                        }
                  	}
                    Row t = table.createRow(titleCellTemplate.getCellHeight());
                    Cell c = t.createCell(100, title, titleCellTemplate.getAlign(), titleCellTemplate.getValign());
                    c.copyCellStyle(titleCellTemplate);
                   if(StringUtils.equalsIgnoreCase(title, Constants.DECLARATION_HEADER_OP_DVT)){
	                    if(!headerAdded){
	                     table.addHeaderRow(t);
	                    }
                   }else{
                	   table.addHeaderRow(t);
                    }
                }

                if (hasHeader) {
                    Row h = table.createRow(headerCellTemplate.getCellHeight());
                    // For ticket 273 display issue
                    float countWidth = 0.0f;
                    float firstWidth = colWidth*0.8f;
                    float normalWidth = colWidth*1.08f;
                    for (int j = 0; j < line.size(); j++) {
                        if (j==0) {
                            colWidth = firstWidth;
                        } else {
                            colWidth = normalWidth;
                        }
                        if (j == line.size()-1) {
                            colWidth = 100 - countWidth;
                        }
                        Cell c = h.createCell(colWidth, line.get(j), centreHeaderCellTemplate.getAlign(), headerCellTemplate.getValign());
                        c.copyCellStyle(headerCellTemplate);
                        countWidth += colWidth;
                    }
                    
                    table.addHeaderRow(h);
                }
            } else {
            	
            	Row r=null;
            	if(StringUtils.equalsIgnoreCase(title,Constants.DECLARATION_HEADER_OP_DVT)){
                  r = table.createRow(9f);
            	}else{
            		r=table.createRow(defaultCellTemplate.getCellHeight());
            	}
                
                
                // For ticket 273 display issue
                float countWidth = 0.0f;
                float firstWidth = colWidth*first;
                float normalWidth = colWidth*second;
                for (int j = 0; j < line.size(); j++) {
                    if (j==0) {
                        colWidth = firstWidth;
                    } else {
                    		colWidth = normalWidth;
                        
                    }
                    if (j == line.size()-1) {
                        colWidth = 100 - countWidth;
                    }
                    boolean isRed = false;
                    boolean outerUl=false;
                    boolean innerUl=false;
                    boolean border=false;
                    boolean image=false;
                    String cellValue = line.get(j);
                    cellValue = cellValue.replace("\\n", "\r");
                    if ( cellValue.contains("<red>") ) {
                        isRed = true;
                        cellValue = cellValue.replaceFirst("<red>", "");
                    }
                   if(StringUtils.equalsIgnoreCase(title, Constants.DECLARATION_HEADER_OP_DVT)){
                	   if(StringUtils.contains(cellValue, "<intend>")){
                		   cellValue=StringUtils.replace(cellValue, "<intend>", "");
                		   colWidth=6;
                		   normalWidth=88;
                		   innerUl=true;
                		   
                		   
                	   }else if(StringUtils.contains(cellValue, "<inner-intend>")){
                		   cellValue=StringUtils.replace(cellValue, "<inner-intend>", "");
                		   colWidth=7;
                		   normalWidth=87;
                		   innerUl=true;
                		   
                   }else if(StringUtils.contains(cellValue, "\u2022")||StringUtils.equals(cellValue, "para")){
                    	cellValue=StringUtils.replace(cellValue, "para", "");
                    	colWidth=3;
                    	normalWidth=91;
                    	outerUl=true;
                 
                    }else if(StringUtils.equalsIgnoreCase(cellValue, "\u25E6")){
                    	innerUl=true;
                    	
                    }else if(j>0){
                    	outerUl=true;
                    }
                    
                    border=true;
                    if(StringUtils.containsIgnoreCase(cellValue,"<img-check>")||StringUtils.containsIgnoreCase(cellValue, "<img-uncheck>")){	
                    	Cell c1=null;
                    	image=true;
	                    if(StringUtils.contains(cellValue,"<img-check>")){
	                    	cellValue=StringUtils.replace(cellValue, "<img-check>", "");
	                    	c1=r.createImageCell(4, check, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
	                    }else{
	                    	cellValue=StringUtils.replace(cellValue, "<img-uncheck>", "");
	                    	c1=r.createImageCell(4, uncheck, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
	                    }
                    	try{
								

								c1.setBorderStyle(null);
								c1.setLeftBorderStyle(thinline);
								colWidth = 96;
								c1.setFontSize(9);
                    	}catch(Exception e){
                    		logger.error("error in creating table in pdf "+e);
                    	}
                    }
                   }
                  
                    
                    Cell c = r.createCell(colWidth, cellValue, defaultCellTemplate.getAlign(), defaultCellTemplate.getValign());
                   
                    c.copyCellStyle(defaultCellTemplate);
                    if ( isRed ){
                        c.setTextColor(Color.RED);
                    }else{
                    	c.setTextColor(Color.BLACK);
                    }
                    if(border){
                    	c.setFontSize(9);
                    	c.setBorderStyle(null);
                    	c.setBottomBorderStyle(null);
                    	c.setTopBorderStyle(null);
                    }
                    if(border&&line.size()==1){
                    	if(!image){
                    	  c.setLeftBorderStyle(thinline);
                    	}
                    	c.setRightBorderStyle(thinline);
                    }
                    if(border&&!csv.hasNext()){
                    	c.setBottomBorderStyle(thinline);
                    }
                    if(j==0&&border){
                    	if(!image){
                    	 c.setLeftBorderStyle(thinline);
                    	}
          
                    }else if(j==1&&border){
                    	c.setRightBorderStyle(thinline);
                    }
                   
                    if(outerUl==true){
                    	
                    	c.setLeftPadding(4);
                    	c.setRightPadding(0);
                    	c.setTopPadding(0);
                    	c.setBottomPadding(0);
                    	c.setFontSize(9);
                    }
                    if(innerUl){
                    	c.setLeftPadding(0);
                    	c.setAlign(HorizontalAlignment.RIGHT);
                    	c.setRightPadding(0);
                    	c.setTopPadding(0);
                    	c.setBottomPadding(0);
                    	c.setFontSize(9);
                    }
                   
                    if(!innerUl){
                     c.setAlign(horizontalAlignment);
                    }
                    countWidth += colWidth;
                }
            }
            i++;
        }
    }
}
