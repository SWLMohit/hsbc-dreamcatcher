package sg.com.hsbc.dreamcatcher.eclaims;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


public class LifeIndexProcessor extends DeathIndexProcessor {
    public LifeIndexProcessor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    Map<String, Object> moreSanitize(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized = sanitizeClaimant(request, sanitized);
        return sanitized;
    }

    private Map<String, Object> sanitizeClaimant(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized.put("policyowner_city", request.getParameter("policyowner_city"));
        sanitized.put("policyowner_postal_code", request.getParameter("policyowner_postal_code"));
        sanitized.put("policyowner_building_number", request.getParameter("policyowner_building_number"));
        sanitized.put("policyowner_unit_number_1", request.getParameter("policyowner_unit_number_1"));
        sanitized.put("policyowner_unit_number_2", request.getParameter("policyowner_unit_number_2"));
        sanitized.put("policyowner_street_1", request.getParameter("policyowner_street_1"));
        sanitized.put("policyowner_street_2", request.getParameter("policyowner_street_2"));
        sanitized.put("policyowner_building_name", request.getParameter("policyowner_building_name"));

        return sanitized;
    }

    void moreProcess(HttpServletRequest request, EClaimApplication application, Map<String, Object> data) {
        LifeApplication app = (LifeApplication)application;
        app.setPolicyownerCity(data.get("policyowner_city").toString());
        app.setPolicyownerPostalCode(data.get("policyowner_postal_code").toString());
        app.setPolicyownerBuildingNumber(data.get("policyowner_building_number").toString());
        app.setPolicyownerUnitNumber1(data.get("policyowner_unit_number_1").toString());
        app.setPolicyownerUnitNumber2(data.get("policyowner_unit_number_2").toString());
        app.setPolicyownerStreet1(data.get("policyowner_street_1").toString());
        app.setPolicyownerStreet2(data.get("policyowner_street_2").toString());
        app.setPolicyownerBuildingName(data.get("policyowner_building_name").toString());
    }

    void morePopulateDataFromObject(HttpServletRequest request, EClaimApplication application, Map<String, Object> data) {
        LifeApplication app = (LifeApplication)application;
        data.put("policyowner_city", app.getPolicyownerCity());
        data.put("policyowner_postal_code", app.getPolicyownerPostalCode());
        data.put("policyowner_building_number", app.getPolicyownerBuildingNumber());
        data.put("policyowner_unit_number_1", app.getPolicyownerUnitNumber1());
        data.put("policyowner_unit_number_2", app.getPolicyownerUnitNumber2());
        data.put("policyowner_street_1", app.getPolicyownerStreet1());
        data.put("policyowner_street_2", app.getPolicyownerStreet2());
        data.put("policyowner_building_name", app.getPolicyownerBuildingName());
    }
}
