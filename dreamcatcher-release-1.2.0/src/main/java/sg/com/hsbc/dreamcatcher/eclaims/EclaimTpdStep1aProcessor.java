package sg.com.hsbc.dreamcatcher.eclaims;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class EclaimTpdStep1aProcessor {
    private EClaimBaseServlet servletInstance;

    public EclaimTpdStep1aProcessor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeBeforeDisability(request, sanitized);
        sanitized = sanitizeAfterDisability(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizeBeforeDisability(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("before_occupation");
        sanitized.put("before_occupation", input);
        input = request.getParameter("before_employer_name");
        sanitized.put("before_employer_name", input);
        input = request.getParameter("before_employer_city");
        sanitized.put("before_employer_city", input);
        input = request.getParameter("before_employer_postal_code");
        sanitized.put("before_employer_postal_code", input);
        input = request.getParameter("before_employer_building_number");
        sanitized.put("before_employer_building_number", input);
        input = request.getParameter("before_employer_unit_number_1");
        sanitized.put("before_employer_unit_number_1", input);
        input = request.getParameter("before_employer_unit_number_2");
        sanitized.put("before_employer_unit_number_2", input);
        input = request.getParameter("before_employer_street_1");
        sanitized.put("before_employer_street_1", input);
        input = request.getParameter("before_employer_street_2");
        sanitized.put("before_employer_street_2", input);
        input = request.getParameter("before_employer_building_name");
        sanitized.put("before_employer_building_name", input);
        input = request.getParameter("before_salary");
        sanitized.put("before_salary", input);
        try {
            sanitized.put("before_salary", Integer.valueOf(input.replaceAll(",","")));
        } catch (NumberFormatException e) {
            sanitized.put("before_salary", 0);
            ErrorHandler.handleError(this, e);
        }
        input = request.getParameter("before_work_duty");
        sanitized.put("before_work_duty", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeAfterDisability(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("after_occupation");
        sanitized.put("after_occupation", input);
        input = request.getParameter("after_employer_name");
        sanitized.put("after_employer_name", input);
        input = request.getParameter("after_employer_city");
        sanitized.put("after_employer_city", input);
        input = request.getParameter("after_employer_postal_code");
        sanitized.put("after_employer_postal_code", input);
        input = request.getParameter("after_employer_building_number");
        sanitized.put("after_employer_building_number", input);
        input = request.getParameter("after_employer_unit_number_1");
        sanitized.put("after_employer_unit_number_1", input);
        input = request.getParameter("after_employer_unit_number_2");
        sanitized.put("after_employer_unit_number_2", input);
        input = request.getParameter("after_employer_street_1");
        sanitized.put("after_employer_street_1", input);
        input = request.getParameter("after_employer_street_2");
        sanitized.put("after_employer_street_2", input);
        input = request.getParameter("after_employer_building_name");
        sanitized.put("after_employer_building_name", input);
        input = request.getParameter("after_salary");
        sanitized.put("after_salary", input);
        try {
            sanitized.put("after_salary", Integer.valueOf(input.replaceAll(",","")));
        } catch (NumberFormatException e) {
            sanitized.put("after_salary", null);
            ErrorHandler.handleError(this, e);
        }
        input = request.getParameter("after_work_duty");
        sanitized.put("after_work_duty", input);

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        TpdApplication application = (TpdApplication)servletInstance.getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setBeforeOccupation(data.get("before_occupation").toString());
        application.setBeforeEmployerName(data.get("before_employer_name").toString());
        application.setBeforeEmployerCity(data.get("before_employer_city").toString());
        application.setBeforeEmployerPostalCode(data.get("before_employer_postal_code").toString());
        application.setBeforeEmployerBuildingNumber(data.get("before_employer_building_number").toString());
        application.setBeforeEmployerUnitNumber1(data.get("before_employer_unit_number_1").toString());
        application.setBeforeEmployerUnitNumber2(data.get("before_employer_unit_number_2").toString());
        application.setBeforeEmployerStreet1(data.get("before_employer_street_1").toString());
        application.setBeforeEmployerStreet2(data.get("before_employer_street_2").toString());
        application.setBeforeEmployerBuildingName(data.get("before_employer_building_name").toString());
        application.setBeforeSalary((Integer)data.get("before_salary"));
        application.setBeforeWorkDuty(data.get("before_work_duty").toString());

        application.setAfterOccupation(data.get("after_occupation").toString());
        application.setAfterEmployerName(data.get("after_employer_name").toString());
        application.setAfterEmployerCity(data.get("after_employer_city").toString());
        application.setAfterEmployerPostalCode(data.get("after_employer_postal_code").toString());
        application.setAfterEmployerBuildingNumber(data.get("after_employer_building_number").toString());
        application.setAfterEmployerUnitNumber1(data.get("after_employer_unit_number_1").toString());
        application.setAfterEmployerUnitNumber2(data.get("after_employer_unit_number_2").toString());
        application.setAfterEmployerStreet1(data.get("after_employer_street_1").toString());
        application.setAfterEmployerStreet2(data.get("after_employer_street_2").toString());
        application.setAfterEmployerBuildingName(data.get("after_employer_building_name").toString());
        application.setAfterSalary((Integer)data.get("after_salary"));
        application.setAfterWorkDuty(data.get("after_work_duty").toString());

        application.setMaxStep(servletInstance.getCurrentStep() + servletInstance.getStep());
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        TpdApplication application = (TpdApplication)servletInstance.getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        data.put("before_occupation", application.getBeforeOccupation());
        data.put("before_employer_name", application.getBeforeEmployerName());
        data.put("before_employer_city", application.getBeforeEmployerCity());
        data.put("before_employer_postal_code", application.getBeforeEmployerPostalCode());
        data.put("before_employer_building_number", application.getBeforeEmployerBuildingNumber());
        data.put("before_employer_unit_number_1", application.getBeforeEmployerUnitNumber1());
        data.put("before_employer_unit_number_2", application.getBeforeEmployerUnitNumber2());
        data.put("before_employer_street_1", application.getBeforeEmployerStreet1());
        data.put("before_employer_street_2", application.getBeforeEmployerStreet2());
        data.put("before_employer_building_name", application.getBeforeEmployerBuildingName());
        data.put("before_salary", application.getBeforeSalary());
        data.put("before_work_duty", application.getBeforeWorkDuty());

        data.put("after_occupation", application.getAfterOccupation());
        data.put("after_employer_name", application.getAfterEmployerName());
        data.put("after_employer_city", application.getAfterEmployerCity());
        data.put("after_employer_postal_code", application.getAfterEmployerPostalCode());
        data.put("after_employer_building_number", application.getAfterEmployerBuildingNumber());
        data.put("after_employer_unit_number_1", application.getAfterEmployerUnitNumber1());
        data.put("after_employer_unit_number_2", application.getAfterEmployerUnitNumber2());
        data.put("after_employer_street_1", application.getAfterEmployerStreet1());
        data.put("after_employer_street_2", application.getAfterEmployerStreet2());
        data.put("after_employer_building_name", application.getAfterEmployerBuildingName());
        data.put("after_salary", application.getBeforeSalary());
        data.put("after_work_duty", application.getBeforeWorkDuty());


        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("country_list", servletInstance.getCountryList());

        request.setAttribute("formData", data);
    }
}
