package sg.com.hsbc.dreamcatcher.helpers;

public class NumberUtilities {
    public static int parseInt(String str) {
        return parseInt(str, 0);
    }
    public static int parseInt(String str, int defaultValue) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    public static float parseFloat(String str) {
        return parseFloat(str, 0f);
    }
    public static float parseFloat(String str, float defaultValue) {
        try {
            return Float.parseFloat(str);
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

}
