package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/9", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step9.jsp")
})
public class Step9Servlet extends BaseServlet {

	private final static Logger logger = Logger.getLogger(Step9Servlet.class);

    public float getCurrentStep() {
        return 9;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeAcceptDeclaration(request, sanitized);
        sanitized = sanitizeAcceptDeclarationInsured(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        if (!application.insuredIsSelf()) {
            errors = validateAcceptDeclarationInsured(data, errors);
        } else {
        	errors = validateAcceptDeclaration(data, errors);
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        //HSBCICDU-961 Skip page 10 if applicant/insured is not working or studying
        if (application.applicantIsWorking()||application.applicantIsStudying()) {
            application.setMaxStep(getCurrentStep() + 1);
        } else if (!application.insuredIsSelf()) {
           if (application.insuredIsWorking()||application.insuredIsStudying()) {
                application.setMaxStep(getCurrentStep() + 1);
           } else {
                application.setMaxStep(getCurrentStep() + 2);
           }
        } else {
           application.setMaxStep(getCurrentStep() + 2);
        }
        return application.save();
    }

    private Map<String, String> validateAcceptDeclaration(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("declare_accept"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateAcceptDeclarationInsured(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("declare_accept_insured"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept_insured", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeAcceptDeclaration(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("declare_accept");

        sanitized.put("declare_accept", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeAcceptDeclarationInsured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("declare_accept_insured");

        sanitized.put("declare_accept_insured", input);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

    /**
     * Handle the success flow to redirect to next page
     *
     * @param request       The request object from the servlet
     * @param response      The response object from the servlet
     * @return              void
     */
    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) {
        OnlineProtectorApplication application = getApplication(request);
        try {
        	//HSBCICDU-961 Skip page 10 if applicant/insured is not working or studying
            if (application.applicantIsWorking()||application.applicantIsStudying()) {
               response.sendRedirect("/our-plans/online-protector/10");
            } else if (!application.insuredIsSelf()) {
               if (application.insuredIsWorking()||application.insuredIsStudying()) {
                  response.sendRedirect("/our-plans/online-protector/10");
               } else {
                  response.sendRedirect("/our-plans/online-protector/11");
               }
            } else {
               response.sendRedirect("/our-plans/online-protector/11");
            }

            return;
        } catch (Exception e) {
            logger.error("handleSuccess:",e);
        }
    }

}
