package sg.com.hsbc.dreamcatcher.dreams;

import com.google.gson.Gson;
import sg.com.hsbc.dreamcatcher.helpers.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/your-dreams/search")
public class SearchServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String keywords = request.getParameter("term");
        String query = "SELECT id, category, description, image FROM dreams WHERE MATCH(description, keywords) AGAINST (? IN BOOLEAN MODE)";

        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(query);
            pstmt.setString( 1, keywords + "*");
            rs = pstmt.executeQuery();
            ArrayList<Map<String, String>> options = new ArrayList<>();
            while(rs.next()) {
                Map hash = new HashMap();
                hash.put("value", rs.getString("category") + "," + rs.getInt("id") + "," + rs.getString("image"));
                hash.put("label", rs.getString("description"));
                options.add(hash);
            }
            String json = new Gson().toJson(options);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
        } finally {
            db.closeAll(conn,pstmt, rs);
        }
    }

}
