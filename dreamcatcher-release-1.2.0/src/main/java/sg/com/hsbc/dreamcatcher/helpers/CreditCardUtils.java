package sg.com.hsbc.dreamcatcher.helpers;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class CreditCardUtils {
  
  /**
   * This method generates credit card number name map, where it considers the first 6 digit of the credit card number as key of the map.
   * It returns unmodifiable map.
   * 
   * @return cc6DigitNumberNameMap
   * 
   * @see Collections#unmodifiableNavigableMap(java.util.NavigableMap)
   * **/
  public static final Map<String, String> getCreditCardNumberNameMap() {
    Map<String, String> ccNumberNameMap = new ConcurrentHashMap<>();
    ccNumberNameMap.put("483585", "483585 Revolution Visa Platinum");
    ccNumberNameMap.put("451297", "451297 Visa Platinum");
    ccNumberNameMap.put("496645", "496645 Visa Platinum (Converted from Gold)");
    ccNumberNameMap.put("492160", "492160 Visa Platinum (Converted from Classic)");
    ccNumberNameMap.put("455228", "455228 Visa USD Gold");
    ccNumberNameMap.put("455227", "455227 Visa USD Classic");
    ccNumberNameMap.put("411903", "411903 Visa Corporate Gold");
    ccNumberNameMap.put("411902", "411902 Visa Corporate Classic");
    ccNumberNameMap.put("512043", "512043 Premier MasterCard");
    ccNumberNameMap.put("541287", "541287 MasterCard Classic");
    ccNumberNameMap.put("544234", "544234 MasterCard Gold");
    ccNumberNameMap.put("539700", "539700 Premier MasterCard USD");
    ccNumberNameMap.put("489085", "489085 Visa Infinite");
    ccNumberNameMap.put("436326", "436326 Advance Debit Card");
    ccNumberNameMap.put("436327", "436327 Advance Ploc Debit Card");
    ccNumberNameMap.put("458555", "458555 Debit Ploc Debit Card");
    ccNumberNameMap.put("458556", "458556 Premier Debit Card");
    ccNumberNameMap.put("458557", "458557 Debit Card");
    ccNumberNameMap.put("458558", "458558 Premier Ploc Debit Card");
    ccNumberNameMap.put("458556002", "458556002 Jade by HSBC Premier Debit Card");
    ccNumberNameMap.put("436324", "436324 Advance Visa Platinum");
    
    
    return Collections.unmodifiableMap(ccNumberNameMap);
  }
  
  /**
   * This method accepts {@code creditCardNumberStr} and returns the appropriate name for it.
   * 
   * @param creditCardNumberStr - Credit Card Number in String form whose length should be greater than 6 digits.
   * 
   * @return {@code ccName} - Credit Card Name in String format. eg: 489085 Visa Infinite
   * 
   * @throws RuntimeException If ccName is null
   * @throws NullPointerException If creditCardNumberStr is null
   * 
   * @see ApplicationUtils#getCreditCard6DigitsNumberNameMap()
   * **/
  public static final String getCreditCardName(String creditCardNumberStr) {
    Objects.requireNonNull(creditCardNumberStr);
    
    String ccName = null;
    Map<String, String> ccNumberNameMap = getCreditCardNumberNameMap();
    if(creditCardNumberStr.length()>6) {
      String ccFirst6DigitsStr = creditCardNumberStr.substring(0, 6);
      String ccFirst9DigitsStr = creditCardNumberStr.substring(0, 9);
     if(!Objects.isNull(ccNumberNameMap.get(ccFirst9DigitsStr))) {
       ccName=ccNumberNameMap.get(ccFirst9DigitsStr);
       ccName= FieldConstants.HSBC_CREDIT_CARD+"- Paid with "+ccName;
     }
     else if(!Objects.isNull(ccNumberNameMap.get(ccFirst6DigitsStr))) {
       ccName=ccNumberNameMap.get(ccFirst6DigitsStr);
       ccName= FieldConstants.HSBC_CREDIT_CARD+"- Paid with "+ccName;
     }
     else {
       ccName= FieldConstants.NON_HSBC_CREDIT_CARD;
     }
     
    }
    /*if(Objects.isNull(ccName)) {
      throw new RuntimeException(
          MessageFormat.format(
              "Credit Card Name Not found! [ccNumber:{0},ccName:{1},ccList:{2}]",
              creditCardNumberStr,ccName,
              cc6DigitNumberNameMap.entrySet().stream().map(Entry::toString).collect(Collectors.joining(",", "[", "]")))
      );
    }*/
    return ccName;
  }
}
