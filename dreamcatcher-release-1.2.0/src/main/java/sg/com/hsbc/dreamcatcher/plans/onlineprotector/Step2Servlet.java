package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/2", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step2.jsp")
})
public class Step2Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 2;
    }

    public boolean validate(HttpServletRequest request) {
        return true;
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        application.setAgreeImportantNotes(true);
        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

}
