package sg.com.hsbc.dreamcatcher.eclaims;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.BaseFormServlet;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.helpers.Validator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import java.util.*;

public abstract class EClaimBaseServlet extends BaseFormServlet {
	private final static Logger logger = Logger.getLogger(EClaimBaseServlet.class);
    public void init(HttpServletRequest request) {
        float currentStep = getCurrentStep();

        ApplicationForm application = this.saveToSession(request);

        request.setAttribute("maxStep", application.getMaxStep());
        request.setAttribute("currentStep", currentStep);
        request.setAttribute("application", application);
        request.setAttribute("data", new HashMap<>());
        request.setAttribute("formData", new HashMap<>());
        request.setAttribute("errors", new HashMap<String, String>());
        request.setAttribute("otpSent", ((application.getDropoutPassword() != null && !application.getDropoutPassword().isEmpty()) ? "1" : "0"));
        request.getSession().setMaxInactiveInterval(SESSION_TIMEOUT_SECONDS);
    }

    private ApplicationForm saveToSession(HttpServletRequest request) {
        UUID applicationSession = (UUID)request.getSession().getAttribute(this.getApplicationSessionAttributeName());
        if (applicationSession == null) {
            applicationSession = UUID.randomUUID();
            request.getSession().setAttribute(this.getApplicationSessionAttributeName(), applicationSession);
        }

        String applicationSessionStr = "application_" + applicationSession.toString();
        ApplicationForm application = (ApplicationForm)request.getSession().getAttribute(applicationSessionStr);
        if (application == null) {
            try {
                application = this.getById(applicationSession.toString());
                request.getSession().setAttribute(applicationSessionStr, application);
            } catch (SQLException e) {
                ErrorHandler.handleError(this, e);
            }
        }

        return application;
    }

    protected void removeFromSession(HttpServletRequest request) {
        UUID applicationSession = (UUID)request.getSession().getAttribute(this.getApplicationSessionAttributeName());
        if (applicationSession != null) {
            String applicationSessionStr = "application_" + applicationSession.toString();
            ApplicationForm application = (ApplicationForm)request.getSession().getAttribute(applicationSessionStr);
            if (application != null) {
                request.getSession().removeAttribute(applicationSessionStr);
                request.getSession().removeAttribute(this.getApplicationSessionAttributeName());
            }
        }
    }

    public boolean preverify(HttpServletRequest request) {
          return ((getMaxStep(request) == 0 && getCurrentStep() == 0) || (getCurrentStep() <= getMaxStep(request)));
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        float nextStep = getCurrentStep() + getStep();
        if (nextStep % 1 == 0) {
            response.sendRedirect(this.getBaseFormUrl() + (nextStep > 0 ? "/" + ((int) nextStep) : ""));
        } else {
            response.sendRedirect(this.getBaseFormUrl() + (nextStep > 0 ? "/" + ((int) nextStep) + "a" : ""));
        }
    }

    public EClaimApplication getApplication(HttpServletRequest request) {
        return ((EClaimApplication) request.getAttribute("application"));
    }

    protected float getStep() {
        return 1;
    }

    private String getUploadDir() {
        return System.getProperty("java.io.tmpdir");
    }
    
    /**
     * This method returns a list of all the documents
     * in the sanitized data starting with the source name
     * 
     * @param request         HttpServletRequest request form data
     * @param data            Hashmap of data of sanitized object.
     * @param sourceFileName  String containing the text to be used as a key in the sanitized map
     * @param destnfileName   String containing text to rename the file for the end user
     * @return                List of String containing names of all the documents.
     */
    public List<String> docList(HttpServletRequest request,Map<String, Object> data, String sourceFileName, String destnfileName){
        List<String> documents=new ArrayList<>();
        for(String key:data.keySet()) {
            if (key.startsWith(sourceFileName)) {
                documents.add(data.get(key).toString());
            }
        }
      logger.info("documents "+documents);
      return documents;
    }
    
    /**
     * This method gets the file names from the HttpServletRequest request 
     * and calls the handleUploadFile method which saves uploaded files in temp folder
     * 
     * @param request         HttpServletRequest request form data
     * @param sanitized       HashMap of sanitized request data
     * @param sourceFileName  String containing the text to be used as a key in the sanitized map
     * @param destnfileName   String containing text to rename the file for the end user
     * @return
     */
    
    public Map<String, Object> sanitizeUploadedDocument(HttpServletRequest request,Map<String, Object> sanitized, String sourceFileName, String destnfileName){
        List<String> documents = new ArrayList<>();
        try {
            Collection<Part> parts = request.getParts();
            for(Part part:parts){
                if (part.getSubmittedFileName() !=  null && part.getName().startsWith(sourceFileName)) {
                    documents.add(part.getName());
                }
            }
        }catch (Exception e) {
            logger.info("Get the files type error: {}", e);
        }
      try{
          int i = 0;
          for(String filename : documents){
            String displayName=destnfileName+filename.replaceAll("[^0-9]+", "");
              sanitized = handleUploadFile(request,sanitized,filename,displayName);
              if (i>100) {
                  break;
              } else {
                  i++;
              }
          }
     }catch(Exception e){
      logger.error("Exception occurred in sanitizeUploadedDocument "+e);
     }
      logger.info("Exiting sanitizeUploadedDocument ");
      return sanitized;
    }

    
    /**
     * This method returns a HashMap<String, Long> containing file names as key 
     * and the file size as corresponding value
     * 
     * @param filename            String  containing file names
     * @param fileUploadedSize    Long file size
     * @param data                Map<String, Object> sanitized data
     * @param application         EclaimApplication object
     * @return                    Updated Map<String, Object> containing updated info of files
     */
    
    public Map<String, Object> fileSizeUploaded(String filename, Long fileUploadedSize, Map<String, Object> data, EClaimApplication application ){
      Map<String, Long>fileInfoMap;
      if(data.get("fileInfo")!=null) {
        fileInfoMap=(Map<String, Long>) data.get("fileInfo");
      }
      else if(application.getFileInfo()!=null){
        fileInfoMap= application.getFileInfo(); 
      }
      else {
        fileInfoMap= new HashMap<String, Long>(); 
      }
      fileInfoMap.put(filename, (Long)fileUploadedSize);
      data.put("fileInfo", fileInfoMap);
      return data;
    }
    
    
    public Map<String, Object> handleUploadFile(HttpServletRequest request, Map<String, Object> data, String name, String fileName) {
        try {
            EClaimApplication application = this.getApplication(request);
            Map<String,String> fileMap;
            // Build file upload
            String filePath = new File(String.format("%s/%s", getUploadDir(), application.getId())).getPath();
            // Create temp folder with application id
            File destDir = new File(filePath);
            destDir.mkdir();

            Part part = request.getPart(name);
            if (part != null && part.getSize() > 0) {
            	String file=request.getParameter(name);
            	 String content=part.getHeader("content-disposition");
            	 String[] fileNameInfo=content.split(";");
            	 String fileNameFromHeader=part.getSubmittedFileName();
            	 if(fileNameInfo!=null&& fileNameInfo.length>0){
            		 fileNameFromHeader=fileNameInfo[fileNameInfo.length-1];
            		 fileNameFromHeader=StringUtils.replace(fileNameFromHeader, "filename=", "");
            		 fileNameFromHeader=StringUtils.replace(fileNameFromHeader, "\"", "");
            		 if(StringUtils.contains(fileNameFromHeader, "\\")){
            			 String[] actualFileName=StringUtils.split(fileNameFromHeader,"\\");
            			 fileNameFromHeader=actualFileName[actualFileName.length-1];
            		 }else{
            			 fileNameFromHeader=part.getSubmittedFileName(); 
            		 }
            	 }
            	 logger.info("the file name to be uploaded is  "+fileNameFromHeader);
                String ext = FilenameUtils.getExtension(part.getSubmittedFileName());
                //JIRA HSBCIDCU-826 - Add original file name as part of the filename saved to server
                String newName = String.format("%s-%s", fileName, fileNameFromHeader);
                InputStream is = part.getInputStream();
                String contentType=FilenameUtils.getExtension(fileNameFromHeader);
                if ( !Validator.isJPEG(is,contentType) ) {
                    is.close();
                    throw new IllegalArgumentException();
                }
                //close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                File outputFile=new File(destDir, newName);
                OutputStream os = new FileOutputStream(outputFile.getPath());
                byte[] buffer = new byte[1024];
                int bytesRead;
                while((bytesRead = is.read(buffer)) !=-1){
                    os.write(buffer, 0, bytesRead);
                }
                data=fileSizeUploaded(newName,FileUtils.sizeOf(outputFile),data,application);
                is.close();
                os.flush();
                os.close();
                data.put(name, newName);
               
                if(application.getFileMapping()!=null) {
                  fileMap=application.getFileMapping();
                }
                else {
                  fileMap= new HashMap<String,String>();
                }
                fileMap.put(name, newName);
                application.setFileMapping(fileMap);
            } else{
              if(application.getFileMapping()!=null) {
                fileMap=application.getFileMapping();
                if(fileMap.get(name)!=null) {
                  data.put(name, fileMap.get(name));
                }
              }    
            }
            
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
        }
        return data;
    }

    // Data loader

    protected String[] getDayList() {
        return new String[] {
                "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
                "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"
        };
    }

    protected String[] getMonthList() {
        return new String[] {
                "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"
        };
    }

    protected String[] getYearList() {
        int cur = 1970;
        int stop = Calendar.getInstance().get(Calendar.YEAR);
        List<String> list = new ArrayList<>();
        while (cur <= stop) {
            list.add(String.valueOf(cur++));
        }
        return list.toArray(new String[list.size()]);
    }

    protected String[] getYearList(int startYear, int endYear) {
        int cur = startYear;
        int stop = endYear;
        List<String> list = new ArrayList<>();
        while (cur <= stop) {
            list.add(String.valueOf(cur++));
        }
        return list.toArray(new String[list.size()]);
    }


    /**
     * The method creates the country names to be displayed
     * 
     * @return   List<String> containing the country names
     *           to be displayed in the frontend 
     */
    protected List<String> getCountryList() {
        String[] country= new String[] {
            "Singapore",
            "Australia",
            "Austria",
            "Bahrain",
            "Belgium",
            "Bermuda",
            "Brunei",
            "Canada",
            "China",
            "Denmark",
            "Finland",
            "France",
            "Germany",
            "Hong Kong",
            "India",
            "Indonesia",
            "Ireland",
            "Italy",
            "Japan",
            "Liechtenstein",
            "Luxembourg",
            "Malaysia",
            "Netherlands",
            "New Zealand",
            "Norway",
            "Philippines",
            "Singapore",
            "South Korea",
            "Spain",
            "Sweden",
            "Switzerland",
            "Taiwan",
            "Thailand",
            "United Arab Emirates",
            "United Kingdom",
            "United States of America",
            "Vatican City",
            "Vietnam",
            "None of the above"
        };

       return Arrays.asList(country);

    }

    // End data loader
   
}
