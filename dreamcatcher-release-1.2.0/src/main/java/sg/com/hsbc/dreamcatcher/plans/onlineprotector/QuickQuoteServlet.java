package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.BaseServlet;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.StringConstants;
import sg.com.hsbc.dreamcatcher.helpers.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.AgeUtils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/our-plans/online-protector/quote", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/quote.jsp")
})
public class QuickQuoteServlet extends BaseServlet {

	private final static Logger logger = Logger.getLogger(QuickQuoteServlet.class);
	private final String DOB_FORMAT = "dd/MM/yyyy";

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();
        float maxRider=0;
        data.put("applicant_gender", request.getParameter("applicant_gender"));
        data.put("applicant_dob", request.getParameter("applicant_dob"));
        data.put("applicant_smoker", request.getParameter("applicant_smoker"));
        data.put("sum_assured", request.getParameter("sum_assured"));
        data.put("rider_sum_assured", request.getParameter("rider_sum_assured"));
        data.put("include_ci_rider", request.getParameter("include_ci_rider"));
        data.put("include_pr_rider", request.getParameter("include_pr_rider"));
        data.put("campaign_code", request.getParameter("campaign_code"));

        if (data.get("applicant_gender") == null) {
            errors.put("applicant_gender", "Please select an option.");
        }
        float riderValueFloat=0;
        float sumassuredFloat=0;
        String riderValue=data.get("rider_sum_assured");
        riderValue=StringUtils.replace(riderValue, ",", "");
        riderValueFloat= Float.parseFloat(riderValue);
        String sum_assured=data.get("sum_assured");
        sum_assured=StringUtils.replace(sum_assured, ",", "");
        sumassuredFloat= Float.parseFloat(sum_assured);

        if(riderValueFloat>sumassuredFloat){
             errors.put("rider_sum_assured", "Please input up to max policy coverage of SGDX");
        }else if(riderValueFloat<100000){
            errors.put("rider_sum_assured", "Please input up to min policy coverage of SGD100,000");
        }

        switch (Validator.validateDate(data.get("applicant_dob"))) {
            case Validator.ERROR_DATE_EMPTY:
                errors.put("applicant_dob", StringConstants.EMPTY_DATE);
                break;
            case Validator.ERROR_DATE_INVALID:
                errors.put("applicant_dob", StringConstants.DATE_OF_BIRTH_INVALID);
                break;
        }

        if (errors.get("applicant_dob") == null) {
        	String dateString = data.get("applicant_dob");

            DateFormat df = new SimpleDateFormat(DOB_FORMAT);
            df.setLenient(false);

            try {
                Date dob = df.parse(dateString);
                int anb = AgeUtils.calculateAgeNextBirthday(dob);
                String dateString1 = new SimpleDateFormat("d/M/y").format(dob);
                if (!Validator.isValidAge(dateString1, FieldConstants.ONLINEPROTECTOR)) {
                    errors.put("applicant_dob", StringConstants.OP_VALID_AGE);
                } else if(anb >= 19 && anb <=49) {
                    maxRider = 650000;
                } else {
                    maxRider = 500000;
                }
                DecimalFormat dfn = new DecimalFormat("#,###");
                String formatted=dfn.format(maxRider);
                if(riderValueFloat>maxRider&&maxRider<=sumassuredFloat){
                    errors.put("rider_sum_assured", "Please input up to max policy coverage of SGD"+formatted);
                }
            } catch (ParseException e) {
                errors.put("applicant_dob", "Your date of birth is invalid.");
            }
        }

        if (data.get("applicant_smoker") == null) {
             errors.put("applicant_smoker", "Please select an option.");
        }

        OnlineProtectorApplication application = new OnlineProtectorApplication();
        application.setLifeInsured("self");
        application.setTermYears(10);

        application.setSumAssured(Float.parseFloat(data.get("sum_assured").toString().replace(",", "")));
        if (data.get("include_ci_rider") != null && data.get("rider_sum_assured") != null && riderValueFloat<=sumassuredFloat) {
            application.setIncludeCriticalIllnessRider(true);
            application.setRiderSumAssured(Float.parseFloat(data.get("rider_sum_assured").toString().replace(",", "")));
        }
        if (data.get("include_pr_rider") != null) {
            application.setIncludeRefundRider(true);
        }
        application.setApplicantGender(data.get("applicant_gender"));
        if (data.get("applicant_smoker") != null) {
            application.setApplicantSmoker(data.get("applicant_smoker").toString().equals("Y"));
        }

        try {
            application.setApplicantDob((new SimpleDateFormat(DOB_FORMAT)).parse(String.format("%s", data.get("applicant_dob"))));
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
        }

        if (data.get("campaign_code") != null) {
            application.setCampaignCode(data.get("campaign_code"));
        }
        DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormat df1 = new DecimalFormat("#,###.00");
        if (errors.size() == 0) {
            try {
                int anb = AgeUtils.calculateAgeNextBirthday(application.getApplicantDob());
                Map<String, Object> monthly = application.getPremiumAtAge(anb, OnlineProtectorApplication.PAYMENT_MODE_MONTHLY);
                Map<String, Object> yearly = application.getPremiumAtAge(anb, OnlineProtectorApplication.PAYMENT_MODE_ANNUALLY);
                /*
                 * Truncates the premium monthly and yearly values to 2 decimal places
                 * for basic, ci and rop before the total is calculated
                 * do that the precision is maintained
                 */
                double premiumMonthly = (double)monthly.get("modalPremium");
                double premiumYearly = (double)yearly.get("modalPremium");
                double premiumMonthlyTrunc= (Math.round(premiumMonthly * 100.0) / 100.0);
                double premiumYearlyTrunc= (Math.round(premiumYearly * 100.0) / 100.0);
                if (application.getIncludeCriticalIllnessRider() != null && application.getIncludeCriticalIllnessRider()) {
                    monthly = application.getCriticalIllnessRiderPremiumAtAge(anb, OnlineProtectorApplication.PAYMENT_MODE_MONTHLY);
                    yearly = application.getCriticalIllnessRiderPremiumAtAge(anb, OnlineProtectorApplication.PAYMENT_MODE_ANNUALLY);
                    double ciMonthly=(double) monthly.get("modalPremium");
                    double ciYearly=(double)yearly.get("modalPremium");
                    double ciYearlyTrunc=Math.round(ciYearly * 100.0) / 100.0;
                    double ciMonthlyTrunc= Math.round(ciMonthly * 100.0) / 100.0;
                    premiumMonthly = (premiumMonthlyTrunc+ciMonthlyTrunc);
                    premiumYearly = (premiumYearlyTrunc+ciYearlyTrunc);
                }

                if (application.getIncludeRefundRider() != null && application.getIncludeRefundRider()) {
                    monthly = application.getRefundRiderPremiumAtAge(anb, OnlineProtectorApplication.PAYMENT_MODE_MONTHLY);
                    yearly = application.getRefundRiderPremiumAtAge(anb, OnlineProtectorApplication.PAYMENT_MODE_ANNUALLY);
                    float ropMonthly=(float)monthly.get("modalPremium");
                    float ropYearly=(float)yearly.get("modalPremium");
                    double ropYearlyDouble=Double.parseDouble(Float.toString(ropYearly));
                    double ropMonthlyDouble=Double.parseDouble(Float.toString(ropMonthly));
                    float ropYearlyTrunc=(float) (Math.round(ropYearlyDouble * 100.0) / 100.0);
                    float ropMonthlyTrunc=(float) (Math.round(ropMonthlyDouble * 100.0) / 100.0);
                    premiumMonthly += ropMonthlyTrunc;
                    premiumYearly += ropYearlyTrunc;
                }


                request.setAttribute("monthlyPremium", df1.format(premiumMonthly));
                request.setAttribute("yearlyPremium", df1.format(premiumYearly));

                request.setAttribute("riderSumAssured", (application.getRiderSumAssured() != null ? df.format(application.getRiderSumAssured()) : 0));
                request.setAttribute("includeCriticalIllnessRider", application.getIncludeCriticalIllnessRider());
                request.setAttribute("includeRefundRider", application.getIncludeRefundRider());
                request.setAttribute("campaignCode", application.getCampaignCode());
            } catch (Exception e) {
                ErrorHandler.handleError(application, e);
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }
        }
        request.setAttribute("sumAssured", df.format(application.getSumAssured()));
        request.setAttribute("errors", errors);

        render(request, response);
    }

}
