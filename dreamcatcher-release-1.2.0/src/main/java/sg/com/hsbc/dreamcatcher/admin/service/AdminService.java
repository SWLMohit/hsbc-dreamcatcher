package sg.com.hsbc.dreamcatcher.admin.service;

import sg.com.hsbc.dreamcatcher.admin.entity.Applications;

import java.util.List;

public interface AdminService {
    public void resendEmail (Applications applications);
    public void resendSMS (Applications applications);
    public void resendVerification(Applications applications);
    public List<Applications> listAllPolicy(Applications applications);
    public String retrieveVerificationCode(Applications applications);
    public boolean isImcompleteEmailExisted(String email);
    public boolean isCompleteEmailExisted(String email);
    public boolean isImcompleteMobileExisted(String countryCode, String mobile);
    public boolean isCompleteMobileExisted(String countryCode, String mobile);
    public Applications findById(String id);
}
