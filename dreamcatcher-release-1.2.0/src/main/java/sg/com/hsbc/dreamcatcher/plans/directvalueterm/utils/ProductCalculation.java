package sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.Constants;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.BaseServlet;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.DirectValueTermApplication;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ProductCalculation {
    public static final Logger logger = Logger.getLogger(ProductCalculation.class);

    public static final int PAYMENT_MODE_ANNUALLY = 1;
    public static final int PAYMENT_MODE_SEMI_ANNUALLY = 2;
    public static final int PAYMENT_MODE_QUARTERLY = 4;
    public static final int PAYMENT_MODE_MONTHLY = 12;

    public static final int MAXIMUM_TERM_YEARS = 47;

    /**
     * gets GENDER/SMOKER marker
     *
     * @param gender
     * @param smoker
     * @return
     */
    public static String getGsMarker(String gender, Boolean smoker) {
        if (gender == null) return null;

        String genderPart = (gender.toUpperCase());
        String smokerPart = (Boolean.TRUE.equals(smoker) ? "S" : "N");

        if (genderPart != null && smokerPart != null) {
            return String.format("%s%s", genderPart, smokerPart);
        }

        return null;
    }

    /**
     * Gets underwriting Class factor
     *
     * @param gender
     * @param smoker
     * @param underwritingClass
     * @return
     */
    public static float getUnderwritingClassFactor(String gender, Boolean smoker, int underwritingClass) {
        Map<String, float[]> factors = new HashMap<>();
        factors.put("MN", new float[]{0.96f, 1, 0.94f, 0.98f});
        factors.put("FN", new float[]{0.96f, 1, 0.94f, 0.98f});
        factors.put("MS", new float[]{1, 1, 1, 0.98f});
        factors.put("FS", new float[]{1, 1, 1, 0.98f});

        String gsMarker = getGsMarker(gender, smoker);

        if (gsMarker == null) {
            logger.error("getUnderwritingClassFactor got some invalid data - probably due to skipping a form step");
            ErrorHandler.handleError(ProductCalculation.class, null, "getUnderwritingClassFactor got some invalid data - probably due to skipping a form step");
            return 0.0f;
        }

        return factors.get(gsMarker)[underwritingClass];
    }

    /**
     * Premium factor of payment mode
     *
     * @param paymentMode
     * @return
     */
    public static double getPaymentModePremiumFactor(int paymentMode) {
        double factor;

        //todo values are slightly higher. is this intentional?
        switch (paymentMode) {
            case PAYMENT_MODE_MONTHLY:
                factor = 0.085;
                break;
            case PAYMENT_MODE_QUARTERLY:
                factor = 0.255;
                break;
            case PAYMENT_MODE_SEMI_ANNUALLY:
                factor = 0.51;
                break;
            case PAYMENT_MODE_ANNUALLY:
            default:
                factor = 1;
                break;
        }

        return factor;
    }

    /**
     * Get Premium factor for Payment frequency
     *
     * @param paymentMode
     * @return
     */
    public static float getPremiumPaymentFrequencyFactor(int paymentMode) {
        float factor;
        switch (paymentMode) {
            case PAYMENT_MODE_MONTHLY:
                factor = 12;
                break;
            case PAYMENT_MODE_QUARTERLY:
                factor = 4;
                break;
            case PAYMENT_MODE_SEMI_ANNUALLY:
                factor = 2;
                break;
            case PAYMENT_MODE_ANNUALLY:
            default:
                factor = 1;
                break;
        }
        return factor;
    }

    /**
     * Calculate premium at specific age of applicant
     *
     * @param age
     * @param application
     * @return
     */
    public static Map<String, Object> getPremiumAtAge(int age, DirectValueTermApplication application) {
        return getPremiumAtAge(age, application.getPaymentMode(), application.getProductCode(), application.getApplicantDob(), application.getApplicantGender(), application.getApplicantSmoker(), application.getUnderwritingClass(), application.getTermYears(), application.getSumAssured());
    }

    public static Map<String, Object> getPremiumAtAge(int age, int paymentMode, DirectValueTermApplication application) {
        return getPremiumAtAge(age, paymentMode, application.getProductCode(), application.getApplicantDob(), application.getApplicantGender(), application.getApplicantSmoker(), application.getUnderwritingClass(), application.getTermYears(), application.getSumAssured());
    }

    /**
     * Calculate premium at age
     *
     * @param age
     * @param paymentMode
     * @param prodCode
     * @param applicantDob
     * @param gender
     * @param smoker
     * @param underwritingClass
     * @param term
     * @param sumAssured
     * @return
     */
    public static Map<String, Object> getPremiumAtAge(int age, int paymentMode, String prodCode, Date applicantDob, String gender, Boolean smoker, int underwritingClass, int term, Float sumAssured) {
        Map<String, Object> result = new HashMap<>();

        if (term == 0) {
            // FIXME: work out the proper ordering to avoid this
            logger.error("DVTApplication:getPremiumAtAge - getTermYears() was zero");
            ErrorHandler.handleError(ProductCalculation.class, null, "DVTApplication:getPremiumAtAge - getTermYears() was zero");
            term = MAXIMUM_TERM_YEARS;
        }

        int initialAge = DirectValueTermApplication.calculateAgeNextBirthday(applicantDob);
     
        int cycle = (age - 1 - initialAge) / term;
        int cycleAge = initialAge + (cycle * term);
        logger.info("the term is "+term);
        String gsMarker = getGsMarker(gender, smoker);
        float adjustedPremiumWithDiscount=0.0f;
        double premium = 0.0;
        float premiumRate = 0.0f;
        float premiumRateUnitAmount = 0.0f;
        float discountRate = 0.0f;
        float discountRateUnitAmount = 0.0f;
        float adjustedPremium = 0.0f;
        double modalPremium = 0.0;
        double modalFactor = getPaymentModePremiumFactor(paymentMode);
        float underwritingClassFactor = getUnderwritingClassFactor(gender, smoker, underwritingClass);
        float paymentFrequencyFactor = getPremiumPaymentFrequencyFactor(paymentMode);

        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            stmt = conn.prepareStatement("SELECT * FROM premiums WHERE product_code = ? AND anb = ?");
            stmt.setString(1, prodCode);
            stmt.setInt(2, cycleAge);
            rs = stmt.executeQuery();
            logger.info("the cycle age is "+cycleAge);
            if (rs.next()) {
                premiumRate = rs.getFloat("premium_" + gsMarker.toLowerCase());
                premiumRateUnitAmount = rs.getFloat("unit_amount");
            }
        } catch (Exception e) {
            logger.error(e.getStackTrace().toString());
        }finally {
            db.closeAll(conn,stmt, rs);
        }

        try{
            conn = db.getConnection();
            stmt = conn.prepareStatement("SELECT * FROM discounts WHERE product_code = ? AND gs_marker = ? AND sa_min <= ? AND sa_max >= ?");
            stmt.setString(1, prodCode);
            stmt.setString(2, gsMarker.toUpperCase());
            stmt.setFloat(3, sumAssured);
            stmt.setFloat(4, sumAssured);
            rs = stmt.executeQuery();
            if (rs.next()) {
                discountRate = rs.getFloat("discount");
                discountRateUnitAmount = rs.getFloat("unit_amount");

            }
        } catch (Exception e) {
            ErrorHandler.handleError(ProductCalculation.class, e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }

        if (premiumRate > 0) {
        	adjustedPremiumWithDiscount=((premiumRate / premiumRateUnitAmount) - (discountRate / discountRateUnitAmount));
            logger.info("the adjusted premium for term "+term+"  is "+adjustedPremiumWithDiscount);
        	adjustedPremium = ((premiumRate / premiumRateUnitAmount) - (discountRate / discountRateUnitAmount)) * sumAssured * underwritingClassFactor;
            modalPremium = Math.round(adjustedPremium * modalFactor * 100f) / 100f;
            premium = modalPremium * paymentFrequencyFactor;
          
           
        }

        result.put("age", age);
        result.put("premium", premium);
        result.put("premiumRate", premiumRate);
        result.put("premiumRateUnitAmount", premiumRateUnitAmount);
        result.put("discountRate", discountRate);
        result.put("discountRateUnitAmount", discountRateUnitAmount);
        result.put("adjustedPremium", adjustedPremium);
        result.put("modalPremium", modalPremium);
        result.put("adjustedPremiumWithDiscount", adjustedPremiumWithDiscount);
        result.put("underwritingClassFactor", underwritingClassFactor);
        result.put("modalFactor", modalFactor);
        logger.info("the modal premium i s "+modalPremium);

        return result;
    }

    public static Map<String, Object> getCriticalIllnessRiderPremiumAtAge(int age, DirectValueTermApplication application) {
        return getCriticalIllnessRiderPremiumAtAge(age, application.getPaymentMode(), application.getRiderProductCode(), application.getApplicantDob(), application.getApplicantGender(), application.getApplicantSmoker(), application.getTermYears(), application.getRiderSumAssured(),application.getProductCode());
    }

    public static Map<String, Object> getCriticalIllnessRiderPremiumAtAge(int age, int paymentMode, DirectValueTermApplication application) {
        return getCriticalIllnessRiderPremiumAtAge(age, paymentMode, application.getRiderProductCode(), application.getApplicantDob(), application.getApplicantGender(), application.getApplicantSmoker(), application.getTermYears(), application.getRiderSumAssured(),application.getProductCode());
    }

    /**
     * Calculate CI Rider premium at age
     *
     * @param age
     * @param paymentMode
     * @param riderProdCode
     * @param applicantDob
     * @param gender
     * @param smoker
     * @param termYears
     * @param sumAssured
     * @return
     */
    public static Map<String, Object> getCriticalIllnessRiderPremiumAtAge(int age, int paymentMode, String riderProdCode, Date applicantDob, String gender, Boolean smoker, int termYears, Float sumAssured, String productCode) {
       //System.out.println("=====================getCriticalIllnessRiderPremiumAtAge===================");
    	Map<String, Object> result = new HashMap<>();

        if (termYears == 0) {
            // FIXME: work out the proper ordering to avoid this
            logger.error("DVTApplication:getPremiumAtAge - getTermYears() was zero");
            ErrorHandler.handleError(ProductCalculation.class, null, "DVTApplication:getPremiumAtAge - getTermYears() was zero");
            termYears = MAXIMUM_TERM_YEARS;
        }

        int initialAge = DirectValueTermApplication.calculateAgeNextBirthday(applicantDob);
        
        int cycle = (age - 1 - initialAge) / termYears;
        int cycleAge = initialAge + (cycle * termYears);
        String gsMarker = getGsMarker(gender, smoker);
       
        double premium = 0.0;
        float premiumRate = 0.0f;
        float premiumRateUnitAmount = 0.0f;
        float adjustedPremium = 0.0f;
        float adjustedPremiumWithDiscount=0.0f;
        double modalPremium = 0.0;
        double modalFactor = getPaymentModePremiumFactor(paymentMode);
        //System.out.println("modalFactor "+modalFactor);
        if(sumAssured==null){
        	sumAssured=200000f;
        }
        float paymentFrequencyFactor = getPremiumPaymentFrequencyFactor(paymentMode);
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM premiums WHERE product_code = ? AND anb = ?");
            stmt.setString(1, riderProdCode);
            stmt.setInt(2, cycleAge);
            try{
                rs = stmt.executeQuery();
				if (productCode.equals("VT93") && riderProdCode.equals("CD79")) { //upto 65 years
					while (rs.next()) {
						int term = (65 - cycleAge);
						if (rs.getInt("term") == term) {
							premiumRate = rs.getFloat("premium_" + gsMarker.toLowerCase());
							premiumRateUnitAmount = rs.getFloat("unit_amount");
							
						}
					}
				} else {
					if (rs.next()) {
                        premiumRate = rs.getFloat("premium_" + gsMarker.toLowerCase());
                        premiumRateUnitAmount = rs.getFloat("unit_amount");
                    }
				}
				
            } catch(Exception e ){
            	logger.error("Error Message "+e);
            	e.printStackTrace();
            }

            if (premiumRate > 0) {
            	adjustedPremiumWithDiscount=premiumRate / premiumRateUnitAmount;
                adjustedPremium = (premiumRate / premiumRateUnitAmount) * sumAssured;
                modalPremium = Math.round(adjustedPremium * modalFactor * 100f) / 100f;
                premium = modalPremium * paymentFrequencyFactor;
                
            }
        } catch (Exception e) {
        	logger.error("Error messgage in getCriticalIllnessRiderPremiumAtAge "+e);
            ErrorHandler.handleError(ProductCalculation.class, e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }

        result.put("age", age);
        result.put("premium", premium);
        result.put("premiumRate", premiumRate);
        result.put("premiumRateUnitAmount", premiumRateUnitAmount);
        result.put("discountRate", 1);
        result.put("adjustedPremiumWithDiscount", adjustedPremiumWithDiscount);
        result.put("discountRateUnitAmount", 1);
        result.put("adjustedPremium", adjustedPremium);
        result.put("modalPremium", modalPremium);
        result.put("modalFactor", modalFactor);
        logger.info("RESULT "+result);
        return result;
    }

    /**
     * Calculates the benefit illustration of an application
     *
     * @param application
     * @return benefit illustration list
     */
    public static ArrayList<Map<String, Object>> getBenefitIllustration(DirectValueTermApplication application) {
        ArrayList<Map<String, Object>> rows = new ArrayList();
       
        //System.out.println("==================getBenefitIllustration=============");
        float premiumPaid = 0;
        int anb = application.calculateAgeNextBirthday((application.getApplicantDob()));
        //System.out.println("ANB fetched getBenefitIllustration "+anb);
        int startAge = anb + 1;
        int lastAgeBaseCover = application.getLastAgeBaseCover();
        int year = 1;
        do {
            Map result = getPremiumAtAge(startAge, application);
            double premium = Double.parseDouble(result.get("premium").toString());
            premiumPaid += premium;

            result.put("premium", premium);
            result.put("premiumPaid", premiumPaid);
            result.put("year", year);
            result.put("age", startAge);
            //System.out.println("getBenefitIllustration result "+result);
            rows.add(result);
            year++;
            startAge++;
        } while (startAge <= lastAgeBaseCover);
        //System.out.println("==============================================");
        return rows;
    }

    /**
     * Calculates the benefit illustration of the rider
     *
     * @param application
     * @return rider benefit illustration list
     */
    public static ArrayList<Map<String, Object>> getCriticalIllnessRiderBenefitIllustration(DirectValueTermApplication application) {
       //System.out.println("===============getCriticalIllnessRiderBenefitIllustration started==================");
    	ArrayList<Map<String, Object>> rows = new ArrayList();
        float premiumPaid = 0;
        int anb = application.calculateAgeNextBirthday(application.getApplicantDob());
        int startAge = anb + 1;
        int lastAgeRiderCover = application.getLastAgeRiderCover();
        //System.out.println("anb "+anb);
        int year = 1;
        do {
            Map result = getCriticalIllnessRiderPremiumAtAge(startAge, application);
            double premium = Double.parseDouble(result.get("premium").toString());
            premiumPaid += premium;

            result.put("premiumPaid", premiumPaid);
            result.put("year", year);
            result.put("age", startAge);
            rows.add(result);

            year++;
            startAge++;
        } while (startAge <= lastAgeRiderCover);
        //System.out.println("==============================================");
        return rows;
    }
    
    /**
	 * Fetches the discount rates from the database
	 * 
	 * @param gsMarker     value based on the gender/smoker
	 * @param sumAssured    value based on the ajax request
	 * @return Map          discount rates from the database                 
	 */
	public static Map<String, Double> getDiscountRates(String gsMarker,float sumAssured){
		  Database db = new Database();
		  Connection conn = db.getConnection();
		  PreparedStatement stmt = null;
		  ResultSet rs = null;
		  Map<String,Double> discountRates=new HashMap(); 
		try{
		  
		  double discountRate=0.0;
		  double discountRateUnitAmount=1000.0;
		  
            conn = db.getConnection();
            stmt = conn.prepareStatement("SELECT * FROM discounts WHERE  gs_marker = ? AND sa_min <= ? AND sa_max >= ?");
            
            stmt.setString(1, gsMarker.toUpperCase());
            stmt.setFloat(2, sumAssured);
            stmt.setFloat(3, sumAssured);
            rs = stmt.executeQuery();
            while (rs.next()) {
            	String productCode=rs.getString("product_code");
            	 discountRate = rs.getFloat("discount");
                 discountRateUnitAmount = rs.getFloat("unit_amount");
            	if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_5_YEARS_PRODUCT_CODE)){
            		discountRates.put("discountRate5",discountRate );
            		discountRates.put("discountRateUnitAmount5", discountRateUnitAmount);
            	}else if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_20_YEARS_PRODUCT_CODE)){
            		discountRates.put("discountRate20",discountRate );
            		discountRates.put("discountRateUnitAmount20", discountRateUnitAmount);
            	}else if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_AGE_65_PRODUCT_CODE)){
            		discountRates.put("discountRate65",discountRate );
            		discountRates.put("discountRateUnitAmount65", discountRateUnitAmount);
            	}
               

            }
            if(!discountRates.containsKey("discountRate5")){
            	discountRates.put("discountRate5",discountRate );
        		discountRates.put("discountRateUnitAmount5", discountRateUnitAmount);
            }
            if(!discountRates.containsKey("discountRate65")){
            	discountRates.put("discountRate65",discountRate );
        		discountRates.put("discountRateUnitAmount65", discountRateUnitAmount);
            }
            if(!discountRates.containsKey("discountRate20")){
            	discountRates.put("discountRate20",discountRate );
        		discountRates.put("discountRateUnitAmount20", discountRateUnitAmount);
            }
            
            
        } catch (Exception e) {
            ErrorHandler.handleError(ProductCalculation.class, e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }
		return discountRates;
	}
	
	/**
	 * Fetches the premium rates from the database
	 * 
	 * 
	 * @param gsMarker     value based on the gender/smoker
	 * @param sumAssured    value based on the ajax request
	 * @return Map          discount rates from the database                 
	 */
	public static Map<String, Double> getPremiumRates(int cycleAge,String gsMarker){
		 Database db = new Database();
		  Connection conn = db.getConnection();
		  PreparedStatement stmt = null;
		  ResultSet rs = null;
		  int remainingterm = (65 - cycleAge);
		  Map<String,Double> premiumRates=new HashMap(); 
		 try{
	            stmt = conn.prepareStatement("SELECT * FROM premiums WHERE  anb = ?");
	            
	            stmt.setInt(1, cycleAge);
	            rs = stmt.executeQuery();
	            logger.info("the cycle age is "+cycleAge);
	            while(rs.next()) {
	            	String productCode=rs.getString("product_code");
	               double premiumRate = rs.getFloat("premium_" + gsMarker.toLowerCase());
	               double premiumRateUnitAmount = rs.getFloat("unit_amount");
	               int term=rs.getInt("term");
	               if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_5_YEARS_PRODUCT_CODE)){
	            	   premiumRates.put("premiumRate5",premiumRate );
	            	   premiumRates.put("premiumRateUnitAmount5", premiumRateUnitAmount);
	            	}else if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_20_YEARS_PRODUCT_CODE)){
	            		premiumRates.put("premiumRate20",premiumRate );
	            		premiumRates.put("premiumRateUnitAmount20", premiumRateUnitAmount);
	            	}else if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_AGE_65_PRODUCT_CODE)){
	            		premiumRates.put("premiumRate65",premiumRate );
	            		premiumRates.put("premiumRateUnitAmount65", premiumRateUnitAmount);
	            	}else if (StringUtils.equalsIgnoreCase(productCode, "CD79") && term==remainingterm ) { //upto 65 years
	            	   premiumRates.put("ciPremiumRate65",premiumRate );
	            	   premiumRates.put("ciPremiumRate65UnitAmount", premiumRateUnitAmount);
					} else if(StringUtils.equalsIgnoreCase(productCode, "CD79")&& term==20){
						premiumRates.put("ciPremiumRate20",premiumRate );
		            	premiumRates.put("ciPremiumRate20UnitAmount", premiumRateUnitAmount);	
					} else if(StringUtils.equalsIgnoreCase(productCode, "CD69")){
						premiumRates.put("ciPremiumRate5",premiumRate );
		            	premiumRates.put("ciPremiumRate5UnitAmount", premiumRateUnitAmount);	
					}
	             
	               

	            }
	            if(!premiumRates.containsKey("ciPremiumRate65")){
	            	   premiumRates.put("ciPremiumRate65",0.0 );
		            	premiumRates.put("ciPremiumRate65UnitAmount", 1000.0);	
	               }
	              if(!premiumRates.containsKey("ciPremiumRate20")){
	            	   premiumRates.put("ciPremiumRate20",0.0 );
		            	premiumRates.put("ciPremiumRate20UnitAmount", 1000.0);	
	               }
	               if(!premiumRates.containsKey("ciPremiumRate5")){
	            	   premiumRates.put("ciPremiumRate5",0.0 );
		            	premiumRates.put("ciPremiumRate5UnitAmount", 1000.0);	
	               }
	               if(!premiumRates.containsKey("premiumRate20")){
	            	   premiumRates.put("premiumRate10",0.0 );
	            	   premiumRates.put("premiumRateUnitAmount10", 1000.0);
	               }
	               if(!premiumRates.containsKey("premiumRate5")){
	            	   premiumRates.put("premiumRate5",0.0 );
	            	   premiumRates.put("premiumRateUnitAmount5", 1000.0);
	               }
	               if(!premiumRates.containsKey("premiumRate65")){
	            	   premiumRates.put("premiumRate65",0.0 );
	            	   premiumRates.put("premiumRateUnitAmount65", 1000.0);
	               }
	               
	        } catch (Exception e) {
	            logger.error(e.getStackTrace().toString());
	        }finally {
	            db.closeAll(conn,stmt, rs);
	        }

		return premiumRates;
		
	}
	/**
	 * Uses the premium calculation formula to calculate the premiums to be 
	 * paid using disount rates and premium rates provided
	 * 
	 * @param premiumRate     premium rates obtained from database for DVT
	 * @param discountRates   discount rates obtained from database for DVT
	 * @param paymentMode     payment frequency used
	 * @param application
	 * @return
	 */
	public static Map<String, Double> getPremiumCalculation(Map<String,Double>  premiumRate,Map<String,Double> discountRates,int paymentMode,DirectValueTermApplication application){
		 Map<String,Double> premiums=new HashMap(); 
		 double premiumRateUnitAmount = premiumRate.get("premiumRateUnitAmount5");
		 double discountRateUnitAmount = discountRates.get("discountRateUnitAmount5");
		 logger.info("the application is "+application+"  ");
		 double sumAssured=application.getSumAssured();
		 double riderSumAssured=0.0;
		if(application.getIncludeCriticalIllnessRider()){
		 riderSumAssured=application.getRiderSumAssured();
		}else{
			riderSumAssured=sumAssured;
	      
		}
		 double modalFactor = getPaymentModePremiumFactor(paymentMode);
	     double underwritingClassFactor = getUnderwritingClassFactor(application.getApplicantGender(), application.getApplicantSmoker(),application.getUnderwritingClass() );
	     float paymentFrequencyFactor = getPremiumPaymentFrequencyFactor(paymentMode);
		 String[] productCodes={Constants.DVT_5_YEARS_PRODUCT_CODE,Constants.DVT_20_YEARS_PRODUCT_CODE,Constants.DVT_AGE_65_PRODUCT_CODE,"CD79Rem","CD7920","CD695"};
		 String period="";
		 String ci="";
		    premiums.put("underwritingClassFactor", underwritingClassFactor);
	        premiums.put("modalFactor", modalFactor);
		 
		 for(String productCode:productCodes){
			double premiumRateFloat=0.0;
			double discountRateFloat=0.0;			
			double adjustedPremiumWithDiscount=0.0;
	        double premium = 0.0;
	        
	        period="";
	        double adjustedPremium = 0.0;
	        double modalPremium = 0.0;
	        
			
			 if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_5_YEARS_PRODUCT_CODE)){
				 premiumRateFloat=premiumRate.get("premiumRate5");
				 discountRateFloat=discountRates.get("discountRate5");
				 
			 }else if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_20_YEARS_PRODUCT_CODE)){
				 premiumRateFloat=premiumRate.get("premiumRate20");
				 discountRateFloat=discountRates.get("discountRate20");
				 period="20";
			 }else if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_AGE_65_PRODUCT_CODE)){
				 premiumRateFloat=premiumRate.get("premiumRate65");
				 discountRateFloat=discountRates.get("discountRate65");
				 period="65";
			 }else if(StringUtils.equalsIgnoreCase(productCode, "CD695")){
				 underwritingClassFactor=1;
				 discountRateFloat=0;
				 ci="ci";
				 sumAssured=riderSumAssured;
				 premiumRateFloat=premiumRate.get("ciPremiumRate5");
			 }else if(StringUtils.equalsIgnoreCase(productCode, "CD7920")){
				 underwritingClassFactor=1;
				 sumAssured=riderSumAssured;
				 discountRateFloat=0;
				 ci="ci";
				 period="20";
				 premiumRateFloat=premiumRate.get("ciPremiumRate20");
			 }else if(StringUtils.equalsIgnoreCase(productCode, "CD79Rem")){
				 underwritingClassFactor=1;
				 discountRateFloat=0;
				 sumAssured=riderSumAssured;
				 ci="ci";
				 period="65";
				 premiumRateFloat=premiumRate.get("ciPremiumRate65");
			 }
			 if (premiumRateFloat > 0) {
				 if(StringUtils.isBlank(ci)){
		        	adjustedPremiumWithDiscount=((premiumRateFloat / premiumRateUnitAmount) - (discountRateFloat / discountRateUnitAmount));
				 }else{
					 adjustedPremiumWithDiscount=premiumRateFloat / premiumRateUnitAmount;
				 }
		        	logger.info("the adjusted premium for term "+productCode+" with discount"+discountRateFloat+"  is "+adjustedPremiumWithDiscount);
					adjustedPremium = adjustedPremiumWithDiscount * sumAssured * underwritingClassFactor;
		            modalPremium = Math.round(adjustedPremium * modalFactor * 100f) / 100f;
		            premium = modalPremium * paymentFrequencyFactor;
		          
		           
		        }
			 
			    premiums.put(ci+"premium"+period, premium);
		        premiums.put(ci+"premiumRate"+period, premiumRateFloat);
		        premiums.put(ci+"premiumRateUnitAmount"+period, premiumRateUnitAmount);
		        premiums.put(ci+"discountRate"+period, discountRateFloat);
		        premiums.put(ci+"discountRateUnitAmount"+period, discountRateUnitAmount);
		        premiums.put(ci+"adjustedPremium"+period, adjustedPremium);
		        premiums.put(ci+"modalPremium"+period, modalPremium);
		        premiums.put(ci+"adjustedPremiumWithDiscount"+period, adjustedPremiumWithDiscount);
		 }
		 logger.info("the adjusted premiums is "+premiums);
		return premiums;
		
		
	}
}
