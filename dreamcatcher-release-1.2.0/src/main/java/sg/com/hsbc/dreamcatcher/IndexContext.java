package sg.com.hsbc.dreamcatcher;

public class IndexContext implements java.io.Serializable {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
