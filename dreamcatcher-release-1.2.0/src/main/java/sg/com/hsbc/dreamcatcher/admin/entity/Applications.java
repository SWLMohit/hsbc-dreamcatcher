package sg.com.hsbc.dreamcatcher.admin.entity;

import java.util.Date;

public class Applications {
    private String id;
    private String referenceNum;
    private String productType;
    private String lifeInsured;
    private int status;
    private String dropoutPassword;
    private String applicantFirstName;
    private String applicantLastName;
    private Date applicantDob;
    private char applicantGender;
    private String applicantMobileCountryCode;
    private String applicantMobileNum;
    private String applicantEmail;
    private String applicantNric;
    private String insuredFirstName;
    private String insuredLastName;
    private String insuredNric;
    private float maxStep;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceNum() {
        return referenceNum;
    }

    public void setReferenceNum(String referenceNum) {
        this.referenceNum = referenceNum;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDropoutPassword() {
        return dropoutPassword;
    }

    public void setDropoutPassword(String dropoutPassword) {
        this.dropoutPassword = dropoutPassword;
    }

    public String getApplicantFirstName() {
        return applicantFirstName;
    }

    public void setApplicantFirstName(String applicantFirstName) {
        this.applicantFirstName = applicantFirstName;
    }

    public String getApplicantLastName() {
        return applicantLastName;
    }

    public void setApplicantLastName(String applicantLastName) {
        this.applicantLastName = applicantLastName;
    }

    public Date getApplicantDob() {
        return applicantDob;
    }

    public void setApplicantDob(Date applicantDob) {
        this.applicantDob = applicantDob;
    }

    public char getApplicantGender() {
        return applicantGender;
    }

    public void setApplicantGender(char applicantGender) {
        this.applicantGender = applicantGender;
    }

    public String getApplicantMobileCountryCode() {
        return applicantMobileCountryCode;
    }

    public void setApplicantMobileCountryCode(String applicantMobileCountryCode) {
        this.applicantMobileCountryCode = applicantMobileCountryCode;
    }

    public String getApplicantMobileNum() {
        return applicantMobileNum;
    }

    public void setApplicantMobileNum(String applicantMobileNum) {
        this.applicantMobileNum = applicantMobileNum;
    }

    public String getApplicantEmail() {
        return applicantEmail;
    }

    public void setApplicantEmail(String applicantEmail) {
        this.applicantEmail = applicantEmail;
    }

    public String getLifeInsured() {
        return lifeInsured;
    }

    public void setLifeInsured(String lifeInsured) {
        this.lifeInsured = lifeInsured;
    }

    public String getApplicantNric() {
        return applicantNric;
    }

    public void setApplicantNric(String applicantNric) {
        this.applicantNric = applicantNric;
    }

    public String getInsuredFirstName() {
        return insuredFirstName;
    }

    public void setInsuredFirstName(String insuredFirstName) {
        this.insuredFirstName = insuredFirstName;
    }

    public String getInsuredLastName() {
        return insuredLastName;
    }

    public void setInsuredLastName(String insuredLastName) {
        this.insuredLastName = insuredLastName;
    }

    public String getInsuredNric() {
        return insuredNric;
    }

    public void setInsuredNric(String insuredNric) {
        this.insuredNric = insuredNric;
    }

    public float getMaxStep() {
        return maxStep;
    }

    public void setMaxStep(float maxStep) {
        this.maxStep = maxStep;
    }
    
    public String getApplicantFullMobileNum() {
        return String.format("%s%s", getApplicantMobileCountryCode(), getApplicantMobileNum());
    }
}
