package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.Validator;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.AgeUtils;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/8", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step8.jsp")
})
public class Step8Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 8;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> sanitized = new HashMap<>();

        if (application.isEligiblePreferentialRates()) {
            sanitized = sanitizeLifestyleQuestionsOptin(request, sanitized);

            if (sanitized.get("uw_lifestyle_questions_optin") != null && (boolean)sanitized.get("uw_lifestyle_questions_optin")) {
                sanitized = sanitizeLifestyleMortgage(request, sanitized);
                sanitized = sanitizeLifestyleProfile(request, sanitized);
                sanitized = sanitizeLifestyleHeight(request, sanitized);
                sanitized = sanitizeLifestyleWeight(request, sanitized);
                sanitized = sanitizeLifestyleExercise(request, sanitized);
            }
        }

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();

        if (application.isEligiblePreferentialRates()) {
            errors = validateLifestyleQuestionsOptin(data, errors);

            if (data.get("uw_lifestyle_questions_optin") != null && (boolean)data.get("uw_lifestyle_questions_optin")) {
                errors = validateLifestyleMortgage(data, errors);
                errors = validateLifestyleProfile(data, errors);
                errors = validateLifestyleHeight(data, errors);
                errors = validateLifestyleWeight(data, errors);
                errors = validateLifestyleExercise(data, errors);
            }
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);
        
        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        if (data.get("uw_lifestyle_questions_optin") != null) {
            application.setUnderwritingLifestyleQuestionsOptin((boolean)data.get("uw_lifestyle_questions_optin"));
        } else {
            application.setUnderwritingLifestyleQuestionsOptin(null);
        }

        if (data.get("uw_lifestyle_mortgage") != null) {
            application.setUnderwritingLifestyleMortgage((int)data.get("uw_lifestyle_mortgage"));
        } else {
            application.setUnderwritingLifestyleMortgage(null);
        }

        if (data.get("uw_lifestyle_profile") != null) {
            application.setUnderwritingLifestyleProfile((int)data.get("uw_lifestyle_profile"));
        } else {
            application.setUnderwritingLifestyleProfile(null);
        }

        if (data.get("uw_lifestyle_height") != null) {
            application.setUnderwritingLifestyleHeight((float)data.get("uw_lifestyle_height"));
        } else {
            application.setUnderwritingLifestyleHeight(null);
        }

        if (data.get("uw_lifestyle_weight") != null) {
            application.setUnderwritingLifestyleWeight((float)data.get("uw_lifestyle_weight"));
        } else {
            application.setUnderwritingLifestyleWeight(null);
        }

        if (data.get("uw_lifestyle_exercise") != null) {
            application.setUnderwritingLifestyleExercise((int)data.get("uw_lifestyle_exercise"));
        } else {
            application.setUnderwritingLifestyleExercise(null);
        }

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateLifestyleQuestionsOptin(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("uw_lifestyle_questions_optin"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("uw_lifestyle_questions_optin", "Please indicate if you have wish to answer the lifestyle questions.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateLifestyleMortgage(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("uw_lifestyle_mortgage"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("uw_lifestyle_mortgage", "Please select an option.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateLifestyleProfile(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("uw_lifestyle_profile"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("uw_lifestyle_profile", "Please select an option.");
                break;
        }
        

        return errors;
    }

    private Map<String, String> validateLifestyleHeight(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("uw_lifestyle_height"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("uw_lifestyle_height", "Please indicate the height.");
                break;
        }
        if(data.get("uw_lifestyle_height")!=null){
        float height=Float.parseFloat(data.get("uw_lifestyle_height").toString());
        if(height==0){
        	errors.put("uw_lifestyle_height", "Please enter valid height.");
        }
        }
        return errors;
    }

    private Map<String, String> validateLifestyleWeight(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("uw_lifestyle_weight"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("uw_lifestyle_weight", "Please indicate the weight.");
                break;
        }
        if(data.get("uw_lifestyle_weight")!=null){
        float weight=Float.parseFloat(data.get("uw_lifestyle_weight").toString());
        if(weight==0){
        	errors.put("uw_lifestyle_weight", "Please enter valid weight.");;
        }
        }

        return errors;
    }

    private Map<String, String> validateLifestyleExercise(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("uw_lifestyle_exercise"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("uw_lifestyle_exercise", "Please select an option.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeLifestyleQuestionsOptin(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("uw_lifestyle_questions_optin");
        if (input != null) {
            sanitized.put("uw_lifestyle_questions_optin", input.equals("Y"));
        } else {
            sanitized.put("uw_lifestyle_questions_optin", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeLifestyleMortgage(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("uw_lifestyle_mortgage");

        try {
            sanitized.put("uw_lifestyle_mortgage", Integer.parseInt(input));
        } catch (Exception e) {
            sanitized.put("uw_lifestyle_mortgage", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeLifestyleProfile(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("uw_lifestyle_profile");

        try {
            sanitized.put("uw_lifestyle_profile", Integer.parseInt(input));
        } catch (Exception e) {
            sanitized.put("uw_lifestyle_profile", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeLifestyleHeight(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("uw_lifestyle_height");

        try {
            sanitized.put("uw_lifestyle_height", Float.parseFloat(input));
        } catch (Exception e) {
            sanitized.put("uw_lifestyle_height", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeLifestyleWeight(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("uw_lifestyle_weight");

        try {
            sanitized.put("uw_lifestyle_weight", Float.parseFloat(input));
        } catch (Exception e) {
            sanitized.put("uw_lifestyle_weight", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeLifestyleExercise(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("uw_lifestyle_exercise");

        try {
            sanitized.put("uw_lifestyle_exercise", Integer.parseInt(input));
        } catch (Exception e) {
            sanitized.put("uw_lifestyle_exercise", null);
        }

        return sanitized;
    }

   
    /* 
     * Sets the keys to be used as attributes in the jsp to be rendered 
     *  
     * @param request     The request parameter passed from the doGet method 
     *                    of the Servlet
     * @return            void
     * 
     */
    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");
        int anb = AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob());

        data.put("uw_lifestyle_questions_optin", application.getUnderwritingLifestyleQuestionsOptin());
        data.put("uw_lifestyle_mortgage", application.getUnderwritingLifestyleMortgage());
        data.put("uw_lifestyle_profile", application.getUnderwritingLifestyleProfile());
        data.put("uw_lifestyle_height", application.getUnderwritingLifestyleHeight());
        data.put("uw_lifestyle_weight", application.getUnderwritingLifestyleWeight());
        data.put("uw_lifestyle_exercise", application.getUnderwritingLifestyleExercise());
        
        /*
         * Calculates the premium values to be showed to the end user
         * Calls the method which calculates the prmium and sets it as 
         * as a attribute of the request parameter
        */
        Map premiums = application.getPremiumAtAge(anb);
        double premiumModal = (double)premiums.get("modalPremium");
        double premiumAnnual = premiumModal * application.getPaymentMode();

        double ciRiderPremiumModal = 0;
        double ciRiderPremiumAnnual = 0;
        if (application.getIncludeCriticalIllnessRider()) {
            premiums = application.getCriticalIllnessRiderPremiumAtAge(anb);
            ciRiderPremiumModal = (double)premiums.get("modalPremium");
            ciRiderPremiumAnnual = ciRiderPremiumModal * application.getPaymentMode();
        }

        float ropRiderPremiumModal = 0;
        float ropRiderPremiumAnnual = 0;
        if (application.getIncludeRefundRider()) {
            premiums = application.getRefundRiderPremiumAtAge(anb);
            ropRiderPremiumModal = (float)premiums.get("modalPremium");
            ropRiderPremiumAnnual = ropRiderPremiumModal * application.getPaymentMode();
        }

        DecimalFormat df = new DecimalFormat("S$#,###.00");
        DecimalFormat df1 = new DecimalFormat("####.00");
        String premiumModalString=df1.format(premiumModal);
        float premiumModalTwo=Float.parseFloat(premiumModalString);
        data.put("summary_modal", df.format(premiumModal));
        
        String premiumAnnualString=df1.format(premiumAnnual);
        float premiumAnnualTwo=Float.parseFloat(premiumAnnualString);
        
        data.put("summary_annually", df.format(premiumAnnual));
        
        String ciRiderPremiumModalString=df1.format(ciRiderPremiumModal);
        float ciRiderPremiumModalTwo=Float.parseFloat(ciRiderPremiumModalString);
        
        data.put("summary_ci_modal", df.format(ciRiderPremiumModal));
        
        String ciRiderPremiumAnnualString=df1.format(ciRiderPremiumAnnual);
        float ciRiderPremiumAnnualTwo=Float.parseFloat(ciRiderPremiumAnnualString);
        
        data.put("summary_ci_annually", df.format(ciRiderPremiumAnnual));
        
        String ropRiderPremiumModalString=df1.format(ropRiderPremiumModal);
        float ropRiderPremiumModalTwo=Float.parseFloat(ropRiderPremiumModalString);
        
        data.put("summary_rop_modal", df.format(ropRiderPremiumModal));
        
        String ropRiderPremiumAnnualString=df1.format(ropRiderPremiumAnnual);
        float ropRiderPremiumAnnualTwo=Float.parseFloat(ropRiderPremiumAnnualString);
        
        data.put("summary_rop_annually", df.format(ropRiderPremiumAnnual));
        
        data.put("summary_total_modal", df.format(premiumModalTwo + ciRiderPremiumModalTwo + ropRiderPremiumModalTwo));
        
        data.put("summary_total_annually",df.format( premiumAnnualTwo + ciRiderPremiumAnnualTwo + ropRiderPremiumAnnualTwo));

        request.setAttribute("data", data);
    }

}