package sg.com.hsbc.dreamcatcher.eclaims.unemployment;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/7-unemployment/0", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/7_unemployment/step0.jsp")
})
@MultipartConfig
public class Step0Servlet extends BaseServlet {

    public void init(HttpServletRequest request) {
        super.init(request);
    }

    public float getCurrentStep() {
        return (float)0;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {

        Map<String, Object> sanitized = new HashMap<>();
        handleUploadFile(request, sanitized, "policyowner_unemployment_document", "UnemploymentDocument");

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        UnemploymentApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        if (!data.get("policyowner_unemployment_document").toString().equals("")) {
            application.setEffectiveUnemploymentDocument(data.get("policyowner_unemployment_document").toString());
        }
        
        /** 10MB File size implementation starts here **/ 
        if(application.getFileInfo()!=null){
          Map<String, Long>updatedFileInfo=application.getFileInfo();
          if(data.get("fileInfo")!=null) {
          updatedFileInfo.putAll((Map<String,Long>)data.get("fileInfo"));
          application.setFileInfo(updatedFileInfo);
          }
        }
        else {
          application.setFileInfo((Map<String,Long>)data.get("fileInfo")); 
        }
        
        /** 10MB File size implementation ends here **/ 

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        UnemploymentApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("policyowner_unemployment_document", application.getEffectiveUnemploymentDocument());

        request.setAttribute("data", data);
    }

    protected float getStep() {
        return (float)1;
    }

}
