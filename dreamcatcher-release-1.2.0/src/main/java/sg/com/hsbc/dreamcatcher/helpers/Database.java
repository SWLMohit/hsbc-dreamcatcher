package sg.com.hsbc.dreamcatcher.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.sql.*;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Database Utility
 */
public class Database {
    static private Logger logger = LoggerFactory.getLogger(Database.class);
    private static DataSource ds;
    static {
        try {
            Context ctx = new InitialContext();
            ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/dreamcatcher");
        } catch (NamingException ne) {
            logger.error("Create datasource error {}", ne);
        }
    }
    /**
     * create the connection
     * @return
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = ds.getConnection();
        } catch (Exception e) {
            logger.error("create DB connection error: {}" ,e.getMessage());
        }
        return connection;
    }

    /**
     * Close all resources for connection, statement and resultset
     * @param conn
     * @param stmt
     * @param rs
     */
    public void closeAll(Connection conn, Statement stmt, ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException sql) {
            logger.error("close all resource failed: {}" ,sql.getMessage());
        }
    }

    /**
     * close the connection and statement
     * @param conn
     * @param stmt
     */
    public void closeAll(Connection conn, Statement stmt) {
        closeAll(conn, stmt, null);
    }

    /**
     * close connection
     * @param conn
     */
    public void close(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                logger.error("close connection failed: {}" ,e.getMessage());
            }
        }
    }
}
