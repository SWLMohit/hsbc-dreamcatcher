package sg.com.hsbc.dreamcatcher.helpers;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.plans.TimePeriod;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

public class ApplicationsReconciliationExcelGenerator {

	private final static Logger logger = LoggerFactory.getLogger(ApplicationsReconciliationExcelGenerator.class);
	
    private XSSFWorkbook workbook;
    private XSSFSheet currentSheet;
    private Date dateGenerated = null;
    private int documentType;
    private String fileName;
    private int rownum;

    private CellStyle boldHeaderStyle, boldStyle, normalStyle, yellowHighlightedStyle, yellowHighlightedBoldStyle, rightAlignedStyle;
    private CellStyle topAndDoubleBottomBorderStyle;
    private CellStyle topBorderStyle, topLeftBorderStyle, topRightBorderStyle;
    private CellStyle bottomBorderStyle, bottomLeftBorderStyle, bottomRightBorderStyle;
    private CellStyle leftBorderStyle, leftBorderWithBoldFontStyle;
    private CellStyle rightBorderStyle;
    private XSSFFont boldFont, normalFont;

    public ApplicationsReconciliationExcelGenerator(String fileName, int type) {
        this.fileName = fileName;
        this.documentType = type;
        this.workbook = createWorkbook();

        this.boldFont = makeBoldFont();
        this.normalFont = makeNormalFont();

        this.boldHeaderStyle = makeBoldHeaderStyle();
        this.boldStyle = makeBoldStyle();
        this.normalStyle = makeNormalStyle();
        this.yellowHighlightedStyle = makeYellowHighlighedStyle();
        this.yellowHighlightedBoldStyle = makeYellowHighlighedBoldStyle();
        this.topAndDoubleBottomBorderStyle = makeTopAndDoubleBottomBorderStyle();
        this.rightAlignedStyle = makeRightAlignedStyle();

        this.topBorderStyle = makeTopBorderStyle();
        this.topLeftBorderStyle = makeTopLeftBorderStyle();
        this.topRightBorderStyle = makeTopRightBorderStyle();
        this.bottomBorderStyle = makeBottomBorderStyle();
        this.bottomLeftBorderStyle = makeBottomLeftBorderStyle();
        this.bottomRightBorderStyle = makeBottomRightBorderStyle();
        this.leftBorderStyle = makeLeftBorderStyle();
        this.rightBorderStyle = makeRightBorderStyle();
        this.leftBorderWithBoldFontStyle = makeLeftBorderWithBoldFontStyle();
    }

    private XSSFFont makeNormalFont() {
        XSSFFont normalFont = this.workbook.createFont();
        normalFont.setFontHeightInPoints((short) 11);
        normalFont.setFontName("Arial");
        normalFont.setColor(IndexedColors.BLACK.getIndex());
        normalFont.setBold(false);
        normalFont.setItalic(false);

        return normalFont;
    }

    private XSSFFont makeBoldFont() {
        XSSFFont boldFont = this.workbook.createFont();
        boldFont.setFontHeightInPoints((short) 11);
        boldFont.setFontName("Arial");
        boldFont.setColor(IndexedColors.BLACK.getIndex());
        boldFont.setBold(true);
        boldFont.setItalic(false);

        return boldFont;
    }

    private CellStyle makeTopAndDoubleBottomBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(BorderStyle.DOUBLE);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }
    private CellStyle makeTopLeftBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }
    private CellStyle makeTopRightBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }
    private CellStyle makeTopBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }
    private CellStyle makeBottomLeftBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }
    private CellStyle makeBottomRightBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }
    private CellStyle makeBottomBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }
    private CellStyle makeLeftBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }
    private CellStyle makeRightBorderStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.normalFont);

        return style;
    }

    private CellStyle makeLeftBorderWithBoldFontStyle() {
        CellStyle style = this.workbook.createCellStyle();
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setFont(this.boldFont);

        return style;
    }

    private CellStyle makeYellowHighlighedStyle() {
        CellStyle yellowHighlightStyle = this.workbook.createCellStyle();
        yellowHighlightStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        yellowHighlightStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        yellowHighlightStyle.setFont(this.normalFont);
        return yellowHighlightStyle;
    }

    private CellStyle makeYellowHighlighedBoldStyle() {
        CellStyle yellowHighlightStyle = this.workbook.createCellStyle();
        yellowHighlightStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        yellowHighlightStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        yellowHighlightStyle.setFont(this.normalFont);
        yellowHighlightStyle.setFont(this.boldFont);
        return yellowHighlightStyle;
    }

    private CellStyle makeNormalStyle() {
        CellStyle normalStyle = null;

        normalStyle = this.workbook.createCellStyle();
        normalStyle.setFont(this.normalFont);

        return normalStyle;
    }

    private CellStyle makeBoldStyle() {
        CellStyle boldStyle = null;

        boldStyle = this.workbook.createCellStyle();
        boldStyle.setFont(this.boldFont);

        return boldStyle;
    }

    private CellStyle makeBoldHeaderStyle() {
        CellStyle style = null;

        style = this.workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setFont(this.boldFont);

        return style;
    }

    private CellStyle makeRightAlignedStyle() {
        CellStyle style = null;

        style = this.workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setFont(this.normalFont);

        return style;
    }

    public XSSFWorkbook createWorkbook() {
        //create blank workbook
        return new XSSFWorkbook();
    }

    public void createSheet(String sheetName) {
        XSSFSheet sheet = this.workbook.createSheet(sheetName);
        this.currentSheet = sheet;
    }

    public void save() {
        try {
            //auto space
            for (int col = 0; col < 7; col++) {
                this.currentSheet.autoSizeColumn(col);
            }
            //highlighted column is 8char wide
            this.currentSheet.setColumnWidth(4, 16 * 256);

            //Write the workbook to the file system
            FileOutputStream out = new FileOutputStream(new File(this.fileName));
            this.workbook.write(out);
            out.close();
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
        }
    }

    public void close() {
        try {
            this.workbook.close();
        } catch (IOException e) {
            ErrorHandler.handleError(this, e);
        }
    }

    public void addTotalSection(String title){
        this.addTopRow();

        Row totalRow = this.currentSheet.createRow(this.rownum++);

        Cell r1c1 = totalRow.createCell(0);
        r1c1.setCellValue(title);
        r1c1.setCellStyle(this.rightAlignedStyle);

        Cell r1c2 = totalRow.createCell(1);
        r1c2.setCellValue("TOTAL NO. OF SUBMISSIONS");

        r1c2.setCellStyle(this.leftBorderWithBoldFontStyle);


        // Setting cell formula and cell type
        Cell cell = totalRow.createCell(4);
        //TODO: get formula when new business case data is available
//        cell.setCellFormula("SUM(E" + (rownum - 5) + ":E" + (rownum - 1) + ")");
        if(title.equals("Cumulative")){
            cell.setCellFormula("SUM(E" + (this.rownum - 7) + ")");

        }else{
            cell.setCellFormula("SUM(E" + (this.rownum - 13) + ")");
        }
        cell.setCellType(Cell.CELL_TYPE_FORMULA);
        cell.setCellStyle(this.topAndDoubleBottomBorderStyle);

        Cell c5 = totalRow.createCell(5);
        c5.setCellStyle(this.rightBorderStyle);

        this.addBottomRow();
    }

    public void addSubtotalSection(String title, TimePeriod interval) {
        this.addTopRow();

        Row row1 = this.currentSheet.createRow(this.rownum++);

        Cell r1c1 = row1.createCell(0);
        r1c1.setCellValue(title);
        r1c1.setCellStyle(this.rightAlignedStyle);

        Cell r1c2 = row1.createCell(1);
        r1c2.setCellValue("Total no. of New Business Cases: ");

        r1c2.setCellStyle(this.leftBorderWithBoldFontStyle);

        Cell r1c5 = row1.createCell(5);
//        r1c5.setCellValue(title);
        r1c5.setCellStyle(this.rightBorderStyle);

        String[] array = {"SExxxx xxxx (Easy E-submission)", "SMxxxx xxxx (Easy Non-E-submission)", "CExxxx xxxx (Complicated E-submission)", "CMxxxx xxxx (Complicated Non-E-submission)"};

        ArrayList<Object> data = getIntervalCount(interval);

        for (int typeNum = 0; typeNum < 4; typeNum++) {
            Row row = this.currentSheet.createRow(this.rownum++);
            Cell col2 = row.createCell(1);
            col2.setCellValue(array[typeNum]);
            col2.setCellStyle(this.leftBorderStyle);

            Cell col5 = row.createCell(4);

            col5.setCellValue((Integer)data.get(typeNum));
            col5.setCellStyle(this.yellowHighlightedStyle);

            Cell col6 = row.createCell(5);
            col6.setCellStyle(this.rightBorderStyle);
        }


        Row totalRow = this.currentSheet.createRow(rownum++);
        // Setting cell formula and cell type
        Cell c1 = totalRow.createCell(1);
        c1.setCellStyle(this.leftBorderStyle);

        Cell cell = totalRow.createCell(4);
        cell.setCellFormula("SUM(E" + (rownum - 5) + ":E" + (rownum - 1) + ")");
        cell.setCellType(Cell.CELL_TYPE_FORMULA);
        cell.setCellStyle(this.topAndDoubleBottomBorderStyle);

        Cell c5 = totalRow.createCell(5);
        c5.setCellStyle(this.rightBorderStyle);

        this.addBottomRow();

    }

    private void addTopRow() {
        Row bottomSpacing = this.currentSheet.createRow(rownum++);
        for(int i = 1; i <= 5; i++){
            Cell cell = bottomSpacing.createCell(i);

            switch(i){
                case 1:
                    cell.setCellStyle(this.topLeftBorderStyle);
                    break;
                case 5:
                    cell.setCellStyle(this.topRightBorderStyle);
                    break;
                default:
                    cell.setCellStyle(this.topBorderStyle);
                    break;
            }

        }
    }
    private void addBottomRow() {
        Row bottomSpacing = this.currentSheet.createRow(rownum++);
        for(int i = 1; i <= 5; i++){
            Cell cell = bottomSpacing.createCell(i);

            switch(i){
                case 1:
                    cell.setCellStyle(this.bottomLeftBorderStyle);
                    break;
                case 5:
                    cell.setCellStyle(this.bottomRightBorderStyle);
                    break;
                default:
                    cell.setCellStyle(this.bottomBorderStyle);
                    break;
            }

        }
    }

    public void addBlankRow() {
        this.currentSheet.createRow(this.rownum++);
    }

    public void addReportHeader(String type, TimePeriod interval, boolean addTimestamp) {
        LocalDateTime now = LocalDateTime.now();

        Row headerRow = this.currentSheet.createRow(this.rownum++);
        Cell headerCell = headerRow.createCell(0);

        if (addTimestamp) {
            headerCell.setCellStyle(this.boldStyle);
            headerCell.setCellValue("Date:");
            headerCell = headerRow.createCell(1);
            headerCell.setCellValue(now.format(DateTimeFormatter.ofPattern("ddMMyyyy")));

            headerRow = this.currentSheet.createRow(this.rownum++);
            headerCell = headerRow.createCell(0);
            headerCell.setCellStyle(this.boldStyle);
            headerCell.setCellValue("Time Period:");
            headerCell = headerRow.createCell(1);
            headerCell.setCellValue(TimePeriod.getTimePeriodDescription(interval));

            headerRow = this.currentSheet.createRow(this.rownum++);
            headerCell = headerRow.createCell(0);
            headerCell.setCellStyle(this.boldStyle);
            headerCell.setCellValue("Time Generated:");
            headerCell = headerRow.createCell(1);
            headerCell.setCellValue(now.format(DateTimeFormatter.ofPattern("HH:mm")));

            addBlankRow();
        }

        headerRow = this.currentSheet.createRow(this.rownum++);
        headerCell = headerRow.createCell(0);
        headerCell.setCellStyle(this.yellowHighlightedBoldStyle);
        headerCell.setCellValue("Type of cases : ");

        headerCell = headerRow.createCell(1);
        headerCell.setCellStyle(this.yellowHighlightedBoldStyle);
        headerCell.setCellValue((type.equalsIgnoreCase("simple") ? "Simple NB" : "Complex NB"));

        this.currentSheet.createRow(this.rownum++);

        Row row = this.currentSheet.createRow(this.rownum++);
        String[] arr = new String[]{"S/N", "Reference Number", "Email Date (yyyy-mm-dd)", "Email Time", "Plan Name", "Policyholder Name", "NRIC Number", "Hard Copy Print Indicator (Y/N)"};

        for (int cellnum = 0; cellnum < arr.length; cellnum++) {
            Cell cell = row.createCell(cellnum);
            cell.setCellValue(arr[cellnum]);
            if (cellnum == 0) {
                cell.setCellStyle(this.boldHeaderStyle);
            }else
            {
                cell.setCellStyle(this.boldStyle);
            }
        }
    }

    public void addSubmissions(TimePeriod interval, String type) {

        ArrayList<Object[]> data = new ArrayList<>();
        java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis());

        String timeFrom = interval.getTimeFrom();
        String timeTo = interval.getTimeTo();
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement applications = null;
        ResultSet rs = null;
        try {

            String sql;
            String sqlComplexType = "";

            if (type.equalsIgnoreCase("simple")) {
                sqlComplexType = "and reference_num like 'S%'";
            } else {
                sqlComplexType = "and reference_num like 'C%'";
            }


            if (interval == TimePeriod.SEVENPM_NINEAM) {
            	sql = "select reference_num, DATE_ADD(email_sent_date, INTERVAL 8 HOUR) emailDate, product_type, applicant_first_name, applicant_last_name, applicant_nric, mail_option"
            			+ " from applications where status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= DATE_ADD(?, INTERVAL -1 DAY) "
            			+ " and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? "
            			+ " and (reference_num like ? or reference_num like ?) order by email_sent_date";
                applications = conn.prepareStatement(sql);
                applications.setString(1, sqlDate.toString()+" "+timeFrom);
                applications.setString(2, sqlDate.toString()+" "+timeTo);
                if (type.equalsIgnoreCase("simple")) {
                	applications.setString(3, "S%");
                	applications.setString(4, "D%S%");
                } else {
                    applications.setString(3, "C%");
                	applications.setString(4, "D%C%");
                }
            } else {
                sql = "select reference_num, DATE_ADD(email_sent_date, INTERVAL 8 HOUR) emailDate, product_type, applicant_first_name, applicant_last_name, applicant_nric, mail_option"
                		+ " from applications where status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= ? "
                		+ " and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? "
                		+ " and (reference_num like ? or reference_num like ?)  order by email_sent_date";
                applications = conn.prepareStatement(sql);
                applications.setString(1, sqlDate.toString()+" "+timeFrom);
                applications.setString(2, sqlDate.toString()+" "+timeTo);
                if (type.equalsIgnoreCase("simple")) {
                	applications.setString(3, "S%");
                	applications.setString(4, "D%S%");
                } else {
                    applications.setString(3, "C%");
                	applications.setString(4, "D%C%");
                }
            }
            
            logger.info("SQL: " + sql);
            logger.info("SQL Parameter: " + sqlDate.toString()+" "+timeFrom);
            logger.info("SQL Parameter: " + sqlDate.toString()+" "+timeTo);
            if (type.equalsIgnoreCase("simple")) {
            	logger.info("SQL Parameter:S%");
            	logger.info("SQL Parameter:D%S%");
            } else {
            	logger.info("SQL Parameter:C%");
            	logger.info("SQL Parameter:D%C%");
            }

            rs = applications.executeQuery();
            SimpleDateFormat localTime = new SimpleDateFormat("HH:mm");
            int index = 1;
            while (rs.next()) {
                //HSBCICDU-592: Compute hardcopy indicator
                String hardcopy = "N";
                if (rs.getString("reference_num")!=null) {
                    if (rs.getString("reference_num").contains("SM")||rs.getString("reference_num").contains("CM")) {
                        hardcopy = "Y";
                    }
                }
                data.add(new String[]{
                        index + "",
                        rs.getString("reference_num"),
                        rs.getDate("emailDate").toLocalDate().toString(),
                        localTime.format(rs.getTime("emailDate")),
                        (rs.getString("product_type").equalsIgnoreCase("onlineprotector") ? "OnlineProtector" : "DIRECT - ValueTerm"),
                        rs.getString("applicant_first_name") + " " + rs.getString("applicant_last_name"),
                        rs.getString("applicant_nric"),
                        hardcopy
                });
                logger.info("Reference Number:" + rs.getString("reference_num"));
                index++;
            }

            writeToSheet(data);

        } catch (Exception e) {
            logger.error("addSubmissions error:" , e);
            ErrorHandler.handleError(this, e);
        } finally {
            db.closeAll(conn,applications, rs);
        }

    }


    public void writeToSheet(ArrayList<Object[]> data) {
        //Iterate over data and write to sheet
        try {
            for (Object[] items : data) {
                Row row = this.currentSheet.createRow(this.rownum++);

                int cellnum = 0;
                for (Object obj : items) {
                    Cell cell = row.createCell(cellnum++);
                    if (obj instanceof String)
                        cell.setCellValue((String) obj);
                    else if (obj instanceof Double)
                        cell.setCellValue((Double) obj);
                    else if (obj instanceof Integer)
                        cell.setCellValue((Integer) obj);

                    if(cellnum == 0){
                        cell.setCellStyle(this.rightAlignedStyle);
                    }
                }

            }
        }catch (Exception e){
            logger.error("writeToSheet:",e);
            ErrorHandler.handleError(this, e);
        }

    }

    private ArrayList<Object> getIntervalCount(TimePeriod interval) {
        ArrayList<Object> data = new ArrayList<>();
        java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis());

        String timeFrom = interval.getTimeFrom();
        String timeTo = interval.getTimeTo();
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement applicationCounts = null;
        ResultSet rs = null;

        try {
            String sql;
            if (interval == TimePeriod.SEVENPM_SEVENPM || interval == TimePeriod.SEVENPM_NINEAM) {
                sql = "SELECT\n" +
                        "  (SELECT count(*)\n" +
                        "  FROM applications\n" +
                        "  WHERE status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= DATE_ADD(?, INTERVAL -1 DAY) AND DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? AND\n" +
                        "        (reference_num LIKE 'SE%' OR reference_num LIKE 'D%SE%') ) AS 'se_count'\n" +
                        ",\n" +
                        "  (SELECT count(*)\n" +
                        "  FROM applications\n" +
                        "  WHERE status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= DATE_ADD(?, INTERVAL -1 DAY) AND DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? AND\n" +
                        "        (reference_num LIKE 'SM%' OR reference_num LIKE 'D%SM%') ) AS 'sm_count'\n" +
                        ",\n" +
                        "  (SELECT count(*)\n" +
                        "   FROM applications\n" +
                        "  WHERE status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= DATE_ADD(?, INTERVAL -1 DAY) AND DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? AND\n" +
                        "        (reference_num LIKE 'CE%' OR reference_num LIKE 'D%CE%') ) AS 'ce_count'\n" +
                        ",\n" +
                        "  (SELECT count(*)\n" +
                        "  FROM applications\n" +
                        "  WHERE status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= DATE_ADD(?, INTERVAL -1 DAY) AND DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? AND\n" +
                        "        (reference_num LIKE 'CM%' OR reference_num LIKE 'D%CM%') ) AS 'cm_count'\n";

                applicationCounts = conn.prepareStatement(sql);
                applicationCounts.setString(1, sqlDate.toString()+" "+timeFrom);
                applicationCounts.setString(2, sqlDate.toString()+" "+timeTo);
                applicationCounts.setString(3, sqlDate.toString()+" "+timeFrom);
                applicationCounts.setString(4, sqlDate.toString()+" "+timeTo);
                applicationCounts.setString(5, sqlDate.toString()+" "+timeFrom);
                applicationCounts.setString(6, sqlDate.toString()+" "+timeTo);
                applicationCounts.setString(7, sqlDate.toString()+" "+timeFrom);
                applicationCounts.setString(8, sqlDate.toString()+" "+timeTo);

            } else {
                sql = "SELECT\n" +
                        "  (SELECT count(*)\n" +
                        "  FROM applications\n" +
                        "  WHERE status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= ? AND DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? AND\n" +
                        "        (reference_num LIKE 'SE%' OR reference_num LIKE 'D%SE%') ) AS 'se_count'\n" +
                        ",\n" +
                        "  (SELECT count(*)\n" +
                        "  FROM applications\n" +
                        "  WHERE status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= ? AND DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? AND\n" +
                        "        (reference_num LIKE 'SM%' OR reference_num LIKE 'D%SM%') ) AS 'sm_count'\n" +
                        ",\n" +
                        "  (SELECT count(*)\n" +
                        "   FROM applications\n" +
                        "  WHERE status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= ? AND DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? AND\n" +
                        "        (reference_num LIKE 'CE%' OR reference_num LIKE 'D%CE%') ) AS 'ce_count'\n" +
                        ",\n" +
                        "  (SELECT count(*)\n" +
                        "  FROM applications\n" +
                        "  WHERE status = 1 and DATE_ADD(email_sent_date, INTERVAL 8 HOUR) >= ? AND DATE_ADD(email_sent_date, INTERVAL 8 HOUR) <= ? AND\n" +
                        "        (reference_num LIKE 'CM%' OR reference_num LIKE 'D%CM%') ) AS 'cm_count'\n";

                applicationCounts = conn.prepareStatement(sql);
                applicationCounts.setString(1, sqlDate.toString()+" "+timeFrom);
                applicationCounts.setString(2, sqlDate.toString()+" "+timeTo);
                applicationCounts.setString(3, sqlDate.toString()+" "+timeFrom);
                applicationCounts.setString(4, sqlDate.toString()+" "+timeTo);
                applicationCounts.setString(5, sqlDate.toString()+" "+timeFrom);
                applicationCounts.setString(6, sqlDate.toString()+" "+timeTo);
                applicationCounts.setString(7, sqlDate.toString()+" "+timeFrom);
                applicationCounts.setString(8, sqlDate.toString()+" "+timeTo);
            }

            logger.info("SQL: " + sql);
            logger.info("SQL Parameter: " + sqlDate.toString()+" "+timeFrom);
            logger.info("SQL Parameter: " + sqlDate.toString()+" "+timeTo);

            rs = applicationCounts.executeQuery();
            while (rs.next()) {

                data.add(rs.getInt("se_count"));
                data.add(rs.getInt("sm_count"));
                data.add(rs.getInt("ce_count"));
                data.add(rs.getInt("cm_count"));

            }

        } catch (Exception e) {
            logger.error("getIntervalCount error:" , e);
            ErrorHandler.handleError(this, e);
        } finally {
            db.closeAll(conn,applicationCounts, rs);
        }

        return data;
    }

}
