package sg.com.hsbc.dreamcatcher.helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class BuildDetails {

    public static ResourceBundle getGitConfigurationBundle() {
        return ResourceBundle.getBundle("git");
    }

    public static Map<String, Object> getMap() {
        ResourceBundle gitBundle = getGitConfigurationBundle();
        Map<String, Object> details = new HashMap<>();
        details.put("branch", gitBundle.getString("git.branch"));
        details.put("commitId", gitBundle.getString("git.commit.id"));
        details.put("commitUser", gitBundle.getString("git.commit.user.name"));
        details.put("commitMessage", gitBundle.getString("git.commit.message.full"));
        details.put("commitTime", gitBundle.getString("git.commit.time"));
        details.put("buildUser", gitBundle.getString("git.build.user.name"));
        details.put("buildTime", gitBundle.getString("git.build.time"));
        return details;
    }

}
