package sg.com.hsbc.dreamcatcher;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.DirectValueTermApplication;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.HashMap;
import java.util.UUID;

public abstract class BaseFormServlet extends BaseFormHandlerServlet {

    public static final int SESSION_TIMEOUT_SECONDS = 4 * 60 * 60;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("formTitle", this.getFormTitle());
        super.doGet(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("formTitle", this.getFormTitle());
        super.doPost(request, response);
    }

    public void init(HttpServletRequest request) {
        float currentStep = getCurrentStep();

        if (request.getSession().getAttribute(this.getApplicationSessionAttributeName()) == null) {
            request.getSession().setAttribute(this.getApplicationSessionAttributeName(), UUID.randomUUID());
        }
        try {
            ApplicationForm application = this.getById(request.getSession().getAttribute(this.getApplicationSessionAttributeName()).toString());
        
            request.setAttribute("maxStep", application.getMaxStep());
            request.setAttribute("currentStep", currentStep);
            request.setAttribute("application", application);
            request.setAttribute("data", new HashMap<>());
            request.setAttribute("formData", new HashMap<>());
            request.setAttribute("errors", new HashMap<String, String>());
            request.setAttribute("otpSent", ((application.getDropoutPassword() != null && !application.getDropoutPassword().isEmpty()) ? "1" : "0"));
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
        }

        request.getSession().setMaxInactiveInterval(SESSION_TIMEOUT_SECONDS);
    }

    public boolean preverify(HttpServletRequest request) {
        if (!super.preverify(request)) {
            return false;
        }

        ApplicationForm application = getApplication(request);
        if (application.getStatus() == ApplicationForm.STATUS_COMPLETE) {
            // FIXME: this 14 shouldn't be hardcoded, and the logic should exist in application
            return (getCurrentStep() > 14);
        }

        return true;
    }

    public void handlePreverifyFail(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ApplicationForm application = getApplication(request);
        float maxStep = getMaxStep(request);

        // FIXME: always true
        if (maxStep % 1 == 0) {
            if (application.getStatus() == ApplicationForm.STATUS_COMPLETE) {
                request.getSession().setAttribute(getApplicationSessionAttributeName(), null);
                if (getCurrentStep() == 1) {
                    response.sendRedirect(this.getBaseFormUrl() + "/1");
                } else {
                    response.sendRedirect(this.getBaseFormUrl());
                }
            } else {
                response.sendRedirect(this.getBaseFormUrl() + (maxStep > 0 ? "/" + ((int) maxStep) : ""));
            }
        } else {
            response.sendRedirect(this.getBaseFormUrl() + (maxStep > 0 ? "/" + ((int) maxStep) + "a" : ""));
        }
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // refactored to utilise getNextStep() (which defaults to falling back to currentStep() + 1)
        response.sendRedirect(this.getBaseFormUrl() + (int)getNextStep());

        // next step is not always perfectly aligned (skipped steps, etc...), so we update maxStep to whatever the nextStep is
        ApplicationForm application = getApplication(request);
        application.setMaxStep(getNextStep());
        application.save();
    }

    public float getMaxStep(HttpServletRequest request) {
        return getApplication(request).getMaxStep();
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return (new HashMap<>());
    }

    public ApplicationForm getApplication(HttpServletRequest request) {
        return ((ApplicationForm) request.getAttribute("application"));
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public <T> T getValueOrDefault(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }
}
