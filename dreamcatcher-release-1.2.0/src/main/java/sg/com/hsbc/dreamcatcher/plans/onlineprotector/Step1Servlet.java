package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import sg.com.hsbc.dreamcatcher.helpers.Database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/1", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step1.jsp")
})
public class Step1Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 1;
    }

    public void init(HttpServletRequest request) {
        super.init(request);
        String sql = "Update applications SET dropout_password = NULL,status=NULL  where id=?" ;
        String id=null;
        if(request.getSession().getAttribute(this.getApplicationSessionAttributeName())!=null){
           id=request.getSession().getAttribute(this.getApplicationSessionAttributeName()).toString();
           if(StringUtils.isNotBlank(id)){
               Database db = new Database();
               Connection conn = db.getConnection();
               PreparedStatement stmt = null;
        	  try{
        		  
        	   stmt = conn.prepareStatement(sql);
        	   stmt.setString(1, id);
        	   stmt.setMaxRows(1);
        	   stmt.executeUpdate();
        	   
        	  }catch(Exception e){
        		  System.out.println("error is thrown in updating the dropout password");
        	  }finally {
                  db.closeAll(conn,stmt);
              }
        	   
           }
        }
        boolean isDirty = false;
        OnlineProtectorApplication application = getApplication(request);

        if (request.getParameter("applicant_gender") != null) {
            application.setApplicantGender(request.getParameter("applicant_gender"));
            isDirty = true;
        }

        if (request.getParameter("applicant_smoker") != null) {
            application.setApplicantSmoker(request.getParameter("applicant_smoker").equalsIgnoreCase("Y"));
            isDirty = true;
        }

        if (request.getParameter("applicant_smoker") != null) {
            application.setApplicantSmoker(request.getParameter("applicant_smoker").equalsIgnoreCase("Y"));
            isDirty = true;
        }

        if (request.getParameter("sum_assured") != null) {
            application.setSumAssured(Float.parseFloat(request.getParameter("sum_assured").replace(",", "")));
            isDirty = true;
        }

        if (request.getParameter("include_ci_rider") != null) {
            application.setIncludeCriticalIllnessRider(true);
            isDirty = true;
        }

        if (request.getParameter("include_pr_rider") != null) {
            application.setIncludeRefundRider(true);
            isDirty = true;
        }

        if (request.getParameter("rider_sum_assured") != null) {
            application.setRiderSumAssured(Float.parseFloat(request.getParameter("rider_sum_assured").replace(",", "")));
            isDirty = true;
        }

        if (request.getParameter("campaign_code") != null) {
            application.setCampaignCode(request.getParameter("campaign_code"));
            isDirty = true;
        }

        if (request.getParameter("applicant_dob") != null) {
            try {
                application.setApplicantDob((new SimpleDateFormat("d/M/y")).parse(String.format("%s", request.getParameter("applicant_dob"))));
                isDirty = true;
            } catch (Exception e) {
                ErrorHandler.handleError(this, e);
            }
        }

        if (isDirty) {
            application.save();
        }
    }

    public boolean validate(HttpServletRequest request) {
        return true;
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        application.setAgreePrivacy(true);
        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

}
