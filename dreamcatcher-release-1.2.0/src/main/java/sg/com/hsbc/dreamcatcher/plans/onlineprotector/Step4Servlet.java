package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.DirectValueTermApplication;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/4", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step4.jsp")
})
public class Step4Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 4;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeApplicantMobileNumber(request, sanitized);
        sanitized = sanitizeApplicantEmail(request, sanitized);
        sanitized = sanitizeApplicantGender(request, sanitized);
        sanitized = sanitizeApplicantNationality(request, sanitized);
        sanitized = sanitizeApplicantMultipleNationalities(request, sanitized);
        sanitized = sanitizeApplicantNationality2(request, sanitized);
        sanitized = sanitizeApplicantNationality3(request, sanitized);
        sanitized = sanitizeApplicantBirthCountry(request, sanitized);
        sanitized = sanitizeApplicantNric(request, sanitized);
        sanitized = sanitizeApplicantEnProficient(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);
        errors = validateApplicantGender(data, errors);
        errors = validateApplicantMobileNumber(data, errors);
        errors = validateApplicantEmail(data, errors);
        errors = validateApplicantNationality(data, errors);
        errors = validateApplicantMultipleNationalities(data, errors);

        if (data.get("applicant_multiple_nationalities") != null && ((boolean)data.get("applicant_multiple_nationalities"))) {
            errors = validateApplicantNationality2(data, errors);
            errors = validateApplicantNationality3(data, errors);
        }
        if (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) {
          errors = validateApplicantNric(data, errors);
        } else {
          errors = validateApplicantPassport(data, errors);
        }
        errors = validateApplicantBirthCountry(data, errors);
      //  errors = validateApplicantNric(data, errors);
        errors = validateApplicantEnProficient(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setApplicantMobileCountryCode(data.get("applicant_mobile_country_code").toString());
        application.setApplicantMobileNum(data.get("applicant_mobile_num").toString());
        application.setApplicantEmail(data.get("applicant_email").toString());
        application.setApplicantGender(data.get("applicant_gender").toString());
        application.setApplicantNationality(data.get("applicant_nationality").toString());
        application.setApplicantNationality2(data.get("applicant_nationality2").toString());
        application.setApplicantNationality3(data.get("applicant_nationality3").toString());
        application.setApplicantBirthCountry(data.get("applicant_birth_country").toString());
        application.setApplicantNric(data.get("applicant_nric").toString());

        if (application.insuredIsSelf()) {
            application.setMaxStep(getCurrentStep() + 1);
        } else {
            application.setMaxStep(getCurrentStep() + 0.1f);
        }

        if (data.get("applicant_multiple_nationalities") != null) {
            application.setApplicantMultipleNationalities((Boolean)data.get("applicant_multiple_nationalities"));
        }

        if (data.get("applicant_en_proficient") != null) {
            application.setApplicantEnProficient((Boolean)data.get("applicant_en_proficient"));
        }

        return application.save();
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (getApplication(request).insuredIsSelf()) {
            response.sendRedirect("/our-plans/online-protector/5");
        } else {
            response.sendRedirect("/our-plans/online-protector/4a");
        }
    }

    private Map<String, String> validateApplicantGender(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_gender"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_gender", "Your gender is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantMobileNumber(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validatePhoneCountryCode(data.get("applicant_mobile_country_code"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_mobile_country_code", "Your mobile phone country code is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("applicant_mobile_country_code", "Your mobile phone country code is too long.");
                break;
            case Validator.ERROR_PHONE_COUNTRY_CODE_INVALID:
                errors.put("applicant_mobile_country_code", "Your mobile phone country code is invalid.");
                break;
        }

        switch (Validator.validatePhoneNumber(data.get("applicant_mobile_num"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_mobile_num", "Your mobile phone number is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("applicant_mobile_num", "Your mobile phone number is too long.");
                break;
            case Validator.ERROR_PHONE_NUMBER_INVALID:
                errors.put("applicant_mobile_num", "Your mobile phone number is invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantEmail(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateEmail(data.get("applicant_email"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_email", "Your email address is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("applicant_email", "Your email address is too long.");
                break;
            case Validator.ERROR_EMAIL_FORMAT_INVALID:
                errors.put("applicant_email", "Your email address is invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantNationality(Map<String, Object> data, Map<String, String> errors) {
        String input = data.get("applicant_nationality").toString();
        boolean hasError = false;

        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_nationality", "Your nationality is missing or invalid.");
                break;
        }

        if (!hasError) {
            if (input.equalsIgnoreCase("NA")) {
                errors.put("applicant_nationality", "Your nationality is missing or invalid.");
                errors.put("dropout", "1");
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantMultipleNationalities(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_multiple_nationalities"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_multiple_nationalities", "Please indicate if you have multiple nationalities.");
                break;
        }

        return errors;
    }

    /**
     * This method checks the request object (sanitized hashmap) for element 'applicant_nationality2' to be not empty and
     * to be different from element 'applicant_nationality'.
     * if not, then error messages will added to the error hashmap.
     * 
     * @param data    Hashmap of santized http request data. 
     * @param errors  Hashmap of error messages to be displayed to the end user.
     * @return        Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantNationality2(Map<String, Object> data, Map<String, String> errors) {
        String input = data.get("applicant_nationality2").toString();
        boolean hasError = false;

        if (data.get("applicant_multiple_nationalities").toString().equalsIgnoreCase("true")) {
            switch (Validator.validateString(input)) {
                case Validator.ERROR_STRING_EMPTY:
                    errors.put("applicant_nationality2", "Your nationality is missing or invalid.");
                    hasError = true;
                    break;
            }
            String mainNationality = data.get("applicant_nationality").toString();// checking for duplicate nationality JIRA HSBCIDCU-963 
            String thirdNationality = data.get("applicant_nationality3").toString();
            if (StringUtils.isNotBlank(mainNationality)&& StringUtils.isNotBlank(input)
                &&Validator.isUniqueNationality(mainNationality, input)){
                errors.put("applicant_nationality2",
                    "Your nationality selected cannot be same as above.");
                hasError = true;
            }
            if (!hasError) {
                if (input.equalsIgnoreCase("NA")) {
                    errors.put("applicant_nationality2", "We are unable to proceed with your online application.");
                    errors.put("dropout", "1");
                }
            }
        }

        return errors;
    }

    /**
     * This method checks the request object (sanitized hashmap) for element 'applicant_nationality3' to be not empty and
     * to be different from element 'applicant_nationality' and 'applicant_nationality2'.
     * if not, then error messages will added to the error hashmap.
     * 
     * @param data    Hashmap of santized http request data. 
     * @param errors  Hashmap of error messages to be displayed to the end user.
     * @return        Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantNationality3(Map<String, Object> data, Map<String, String> errors) { 
      String input = data.get("applicant_nationality3").toString();
      if (data.get("applicant_multiple_nationalities").toString().equalsIgnoreCase("true")) {
        String mainNationality = data.get("applicant_nationality").toString();// checking for duplicate nationality JIRA HSBCIDCU-963 
        String secondNationality = data.get("applicant_nationality2").toString();
        if ((StringUtils.isNotBlank(mainNationality)&& StringUtils.isNotBlank(input)
            &&Validator.isUniqueNationality(mainNationality, input))
            ||(StringUtils.isNotBlank(secondNationality)&& StringUtils.isNotBlank(input)
                &&Validator.isUniqueNationality(secondNationality, input))){
            errors.put("applicant_nationality3",
                "Your nationality selected cannot be same as above.");
            
          }
      }
      return errors;
    }

    private Map<String, String> validateApplicantBirthCountry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_birth_country"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_birth_country", "Your country of birth is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantNric(Map<String, Object> data, Map<String, String> errors) { //JIRA 625
        switch (Validator.validateNric(data.get(FieldConstants.APPLICANT_NRIC))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_NRIC, "Your NRIC number is missing.");
             //   errors.put(FieldConstants.APPLICANT_PASSPORT, "Your Passport number is missing.");
                break;
            case Validator.ERROR_NRIC_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_NRIC, "Please enter a valid NRIC number.");
               // errors.put(FieldConstants.APPLICANT_PASSPORT, "Please enter a valid Passport number.");
                break;
        }

        return errors;
    }
   
    private Map<String, String> validateApplicantPassport(Map<String, Object> data, Map<String, String> errors) { //JIRA 625
        switch (Validator.validatePassport(data.get(FieldConstants.APPLICANT_NRIC))) {
        
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_PASSPORT, "Your passport number is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.APPLICANT_PASSPORT, "Please enter a valid Passport number.");
                break;
            case Validator.ERROR_PASSPORT_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_PASSPORT, "Please enter a valid Passport number.");
                break;
               
          
        }

        return errors;
    }

    private Map<String, String> validateApplicantEnProficient(Map<String, Object> data, Map<String, String> errors) {
        String input = (data.get("applicant_en_proficient") != null ? data.get("applicant_en_proficient").toString() : "");
        boolean hasError = false;

        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_en_proficient", "Please indicate if you are proficient in English.");
                hasError = true;
                break;
        }

        if (!hasError) {
            if (!input.equalsIgnoreCase("true")) {
                errors.put("applicant_en_proficient", "We are unable to proceed with your online application.");
                errors.put("dropout", "1");
            }
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantGender(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_gender");
        String[] options = new String[] { "M", "F" };
        if (!Arrays.asList(options).contains(input)) {
            input = null;
        }

        sanitized.put("applicant_gender", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantMobileNumber(HttpServletRequest request, Map<String, Object> sanitized) {
        String countryCode = request.getParameter("applicant_mobile_country_code");
        String mobileNum = request.getParameter("applicant_mobile_num");
        sanitized.put("applicant_mobile_country_code", countryCode);
        sanitized.put("applicant_mobile_num", mobileNum);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantEmail(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_email");
        sanitized.put("applicant_email", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantNationality(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_nationality");

        sanitized.put("applicant_nationality", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantMultipleNationalities(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_multiple_nationalities");
        if (input != null) {
            sanitized.put("applicant_multiple_nationalities", input.equals("Y"));
        } else {
            sanitized.put("applicant_multiple_nationalities", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantNationality2(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_nationality2");

        sanitized.put("applicant_nationality2", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantNationality3(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_nationality3");

        sanitized.put("applicant_nationality3", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantBirthCountry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_birth_country");

        sanitized.put("applicant_birth_country", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantNric(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_nric");

        sanitized.put("applicant_nric", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantEnProficient(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_en_proficient");
        if (input != null) {
            sanitized.put("applicant_en_proficient", input.equals("Y"));
        } else {
            sanitized.put("applicant_en_proficient", null);
        }

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("applicant_mobile_country_code", application.getApplicantMobileCountryCode());
        data.put("applicant_mobile_num", application.getApplicantMobileNum());
        data.put("applicant_email", application.getApplicantEmail());
        data.put("applicant_gender", application.getApplicantGender());
        data.put("applicant_multiple_nationalities", application.getApplicantMultipleNationalities());
        data.put("applicant_nationality", application.getApplicantNationality());
        data.put("applicant_nationality2", application.getApplicantNationality2());
        data.put("applicant_nationality3", application.getApplicantNationality3());
        data.put("applicant_birth_country", application.getApplicantBirthCountry());
        data.put("applicant_nric", application.getApplicantNric());
        data.put("applicant_en_proficient", application.getApplicantEnProficient());

        if (data.get("applicant_birth_country") == null) {
            data.put("applicant_birth_country", application.getApplicantNationality());
        }

        request.setAttribute("data", data);
    }

}
