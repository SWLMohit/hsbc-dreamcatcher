package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoillness;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/5-tpd-due-to-illness/4", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/5_tpd_due_to_illness/step4a.jsp")
})
public class Step4aServlet extends BaseServlet {
    public float getCurrentStep() {
        return (float) 4;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        application.setMaxStep(getCurrentStep() + (float) 0.5);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        data.put("payment", application.getPayment());
        request.setAttribute("data", data);
    }

    protected float getStep() {
        return (float) 0.5;
    }
}
