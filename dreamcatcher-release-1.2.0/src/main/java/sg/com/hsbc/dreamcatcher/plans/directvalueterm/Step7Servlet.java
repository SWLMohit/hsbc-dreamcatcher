package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.ProductCalculation;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/7", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step7.jsp")
})
public class Step7Servlet extends BaseServlet {
	private final static Logger logger = Logger.getLogger(Step7Servlet.class);
    public float getCurrentStep() {
        return 7;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeSumAssured(request, sanitized);
        sanitized = sanitizeRiderSumAssured(request, sanitized);
        sanitized = sanitizeTermDuration(request, sanitized);
        sanitized = sanitizePaymentMode(request, sanitized);
        sanitized = sanitizeIncludeCriticalIllnessRider(request, sanitized);
        sanitized = sanitizeInsuredMedical4(request, sanitized);
        sanitized= sanitizeHiddenFields(request, sanitized);
        sanitized.put(FieldConstants.MAX_SUM_ASSURED, application.getMaxSumAssured());
        sanitized.put(FieldConstants.MAX_RIDER_SUM_ASSURED, application.getMaxRiderSumAssured());

        return sanitized;
    }
    /**
     * Accepts the hidden fields saved in the form and puts them in the data map.
     * This helps in preventing the additional backend call on post of form
     * @param request      request received by the servlet
     * @param sanitized    the map saving data received 
     * @return
     */
    private Map<String, Object> sanitizeHiddenFields(HttpServletRequest request, Map<String, Object> sanitized) {
		// TODO Auto-generated method stub
	     String modalFactor = request.getParameter("modalFactor");
	   	 String underwritingclassfactor = request.getParameter("underwritingclassfactor");
	   	 String ciadjustedpremium = request.getParameter("ciadjustedpremium");
	   	 String premiumrate = request.getParameter("premiumrate");
	   	 String premiumrateunitamount = request.getParameter("premiumrateunitamount");
	   	 String adjustedpremium = request.getParameter("adjustedpremium");
	   	 String basic = request.getParameter("basic");
	   	 String ci=request.getParameter("ci");
	   	 String ciadjustedpremium20 = request.getParameter("ciadjustedpremium20");
	   	 String premiumrate20 = request.getParameter("premiumrate20");
	   	 String premiumrateunitamount20 = request.getParameter("premiumrateunitamount20");
	   	 String adjustedpremium20 = request.getParameter("adjustedpremium20");
	   	String ciadjustedpremium65 = request.getParameter("ciadjustedpremium65");
	   	 String premiumrate65 = request.getParameter("premiumrate65");
	   	 String premiumrateunitamount65 = request.getParameter("premiumrateunitamount65");
	   	 String adjustedpremium65 = request.getParameter("adjustedpremium65");
	   	 String actualDiscount5=request.getParameter("actualdiscount5");
	   	String actualDiscount20=request.getParameter("actualdiscount20");
	   	String actualDiscount65=request.getParameter("actualdiscount65");
	   	 
			 sanitized.put("modalFactor",modalFactor);
		   	 sanitized.put("underwritingClassFactor",underwritingclassfactor);
		   	 sanitized.put("ciAdjustedPremium",ciadjustedpremium);
		   	 sanitized.put("premiumRate",premiumrate);
		   	 sanitized.put("premiumRateUnitAmount",premiumrateunitamount);	 
		   	 sanitized.put("adjustedPremium",adjustedpremium);
		   	 sanitized.put("ciAdjustedPremium20",ciadjustedpremium20);
		   	 sanitized.put("premiumRate20",premiumrate20);
		   	 sanitized.put("premiumRateUnitAmount20",premiumrateunitamount20);   	 
		   	 sanitized.put("adjustedPremium20",adjustedpremium20);   
		   	sanitized.put("ciAdjustedPremium65",ciadjustedpremium65);
		   	 sanitized.put("premiumRate65",premiumrate65);
		   	 sanitized.put("premiumRateUnitAmount65",premiumrateunitamount65);   	
		   	 sanitized.put("adjustedPremium65",adjustedpremium65);
		   	 sanitized.put("actualDiscount5", actualDiscount5);
		        sanitized.put("actualDiscount20", actualDiscount20);
		        sanitized.put("actualDiscount65", actualDiscount65);
		        if(StringUtils.contains(basic, "S$")){
   	         sanitized.put("summary_modal", basic);
		        }else{
		        	sanitized.put("summary_modal", "S$"+basic);
		        }
		        if(StringUtils.contains(basic, "S$")){
   	                sanitized.put("summary_ci_modal", ci);
		        }else{
		        	sanitized.put("summary_ci_modal", "S$"+ci);
		        }
   	         float basicFloat=0.0f;
        	 float ciFloat=0.0f;
        	 float total=0.0f;
   	         if(StringUtils.isNotBlank(basic)&&StringUtils.isNotBlank(ci)){ 
   	        	 basic=StringUtils.replace(basic, "S$", "");
   	        	 ci=StringUtils.replace(ci, "S$", "");
   	        	 try{
		   	        	basicFloat=Float.parseFloat(basic);
		   	        	ciFloat=Float.parseFloat(ci);
   	        	 }catch(Exception e){
   	        		 logger.error("Number cannot be parsed "+e);
   	        	 }
   	        	total=basicFloat;
   	        	if(sanitized.get(FieldConstants.INCLUDE_CI_RIDER)!=null){
   	        		total+=ciFloat;
   	        	}
   	         }
   	      sanitized.put(FieldConstants.SUMMARY_TOTAL_MODAL,"S$"+(total));
	   	 
		return sanitized;
	}

	public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);
        
        
        errors = validateSumAssured(data, errors);
        errors = validateRiderSumAssured(data, errors);
        errors = validateTermDuration(data, errors);
        errors = validatePaymentMode(data, errors);

        if (data.get(FieldConstants.INCLUDE_CI_RIDER) != null) {
            errors = validateInsuredMedical4(data, errors);
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        try {
            application.setSumAssured(NumberFormat.getNumberInstance(java.util.Locale.US).parse(data.get(FieldConstants.SUM_ASSURED).toString()).floatValue());
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }

        if (data.get(FieldConstants.INCLUDE_CI_RIDER) != null && (boolean) data.get(FieldConstants.INCLUDE_CI_RIDER)) {
            try {
                application.setRiderSumAssured(NumberFormat.getNumberInstance(java.util.Locale.US).parse(data.get(FieldConstants.RIDER_SUM_ASSURED).toString()).floatValue());
            } catch (ParseException e) {
                ErrorHandler.handleError(application, e);
                return false;
            }
        } else {
                application.setRiderSumAssured(null);
            }

        application.setTermDuration(data.get(FieldConstants.TERM_DURATION).toString());

        if (data.get(FieldConstants.PAYMENT_MODE) != null) {
            application.setPaymentMode(Integer.parseInt(data.get(FieldConstants.PAYMENT_MODE).toString()));
        } else {
            application.setIncludeCriticalIllnessRider(false);
        }

        if (data.get(FieldConstants.INCLUDE_CI_RIDER) != null) {
            application.setIncludeCriticalIllnessRider((boolean) data.get(FieldConstants.INCLUDE_CI_RIDER));
            application.setInsuredMedical4((boolean) data.get(FieldConstants.INSURED_MEDICAL_4));
        } else {
            application.setIncludeCriticalIllnessRider(false);
            application.setInsuredMedical4(null);
        }

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateSumAssured(Map<String, Object> data, Map<String, String> errors) {
        float sa = (data.get(FieldConstants.SUM_ASSURED) != null) ? Float.parseFloat(data.get(FieldConstants.SUM_ASSURED).toString().replace(",", "")) : 0f;
        if (sa > Float.parseFloat(data.get(FieldConstants.MAX_SUM_ASSURED).toString())) {
            errors.put(FieldConstants.SUM_ASSURED, "The amount you specified is above the limit allowed.");
        }

        return errors;
    }

    private Map<String, String> validateRiderSumAssured(Map<String, Object> data, Map<String, String> errors) {
        float sa = (data.get(FieldConstants.RIDER_SUM_ASSURED) != null) ? Float.parseFloat(data.get(FieldConstants.RIDER_SUM_ASSURED).toString().replace(",", "")) : 0f;
        if (sa > Float.parseFloat(data.get(FieldConstants.MAX_RIDER_SUM_ASSURED).toString())) {
            errors.put(FieldConstants.RIDER_SUM_ASSURED, "The amount you specified is above the limit allowed.");
        }

        return errors;
    }

    private Map<String, String> validateTermDuration(Map<String, Object> data, Map<String, String> errors) {
        String input = (data.get(FieldConstants.TERM_DURATION) != null ? data.get(FieldConstants.TERM_DURATION).toString() : "");
        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.TERM_DURATION, "Please select term duration.");
                break;
        }

        return errors;
    }

    private Map<String, String> validatePaymentMode(Map<String, Object> data, Map<String, String> errors) {
        String input = (data.get(FieldConstants.PAYMENT_MODE) != null ? data.get(FieldConstants.PAYMENT_MODE).toString() : "");
        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.PAYMENT_MODE, "Please select payment frequency.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredMedical4(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.INSURED_MEDICAL_4))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.INSURED_MEDICAL_4, "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeInsuredMedical4(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.INSURED_MEDICAL_4);
        if (input != null) {
            sanitized.put(FieldConstants.INSURED_MEDICAL_4, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.INSURED_MEDICAL_4, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeSumAssured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.SUM_ASSURED);

        sanitized.put(FieldConstants.SUM_ASSURED, Float.parseFloat(input.replace(",", "")));

        return sanitized;
    }

    private Map<String, Object> sanitizeRiderSumAssured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.RIDER_SUM_ASSURED);

        sanitized.put(FieldConstants.RIDER_SUM_ASSURED, Float.parseFloat(input.replace(",", "")));

        return sanitized;
    }

    private Map<String, Object> sanitizeTermDuration(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.TERM_DURATION);

        sanitized.put(FieldConstants.TERM_DURATION, input);

        return sanitized;
    }

    private Map<String, Object> sanitizePaymentMode(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.PAYMENT_MODE);

        try {
            sanitized.put(FieldConstants.PAYMENT_MODE, Integer.parseInt(input));
        } catch (Exception e) {
            sanitized.put(FieldConstants.PAYMENT_MODE, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeIncludeCriticalIllnessRider(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.INCLUDE_CI_RIDER);
        if (input != null) {
            sanitized.put(FieldConstants.INCLUDE_CI_RIDER, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.INCLUDE_CI_RIDER, null);
        }

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        if (application.getSumAssured() != null) {
            data.put(FieldConstants.SUM_ASSURED, application.getSumAssured());
        } else {
            data.put(FieldConstants.SUM_ASSURED, 0);
        }

        if (application.getRiderSumAssured() != null) {
            data.put(FieldConstants.RIDER_SUM_ASSURED, application.getRiderSumAssured());
        } else {
            data.put(FieldConstants.RIDER_SUM_ASSURED, 0);
        }

        data.put(FieldConstants.TERM_DURATION, application.getTermDuration());
        data.put(FieldConstants.PAYMENT_MODE, application.getPaymentMode());

        data.put(FieldConstants.INSURED_MEDICAL_4, application.getInsuredMedical4());
        data.put(FieldConstants.INCLUDE_CI_RIDER, application.getIncludeCriticalIllnessRider());
        if(request!=null){
         data=addPremiumData(application, data);
        }
        request.setAttribute("data", data);
    }
    
    /**
     * Added changes for setting the premium table  
     * as a request attribute so that it can be used in
     * jsp
     * 
     * @param application     Op application object to find anb 
     * @param data            data map where attributes are saved as request
     * @return Map            Map containinng the attributes
     */
    private Map<String,Object>  addPremiumData(DirectValueTermApplication application,Map<String,Object> data){
    	int anb = DirectValueTermApplication.calculateAgeNextBirthday(application.getApplicantDob());
    	String gsMarker=ProductCalculation.getGsMarker(application.getApplicantGender(), application.getApplicantSmoker());
    	Map<String,Double> premiumRates=ProductCalculation.getPremiumRates(anb, gsMarker);	
    	Map<String,Double> discountRates=ProductCalculation.getDiscountRates(gsMarker, application.getSumAssured());
    	
    	Map premiums=ProductCalculation.getPremiumCalculation(premiumRates, discountRates, 12, application);
    	
    	double premiumModal = (double)premiums.get("modalPremium");
        double adjustedPremium=(double)premiums.get("adjustedPremiumWithDiscount");
        double discountRate=(double)premiums.get("discountRate");
        double discountRateUnit=(double)premiums.get("discountRateUnitAmount");
        double actualDiscount5=discountRate/discountRateUnit;
        double modalFactor=(double)premiums.get("modalFactor");
        double underwritingClassFactor=(double)premiums.get("underwritingClassFactor");
        double premiumRate=(double)premiums.get("premiumRate");
        double premiumRateUnitAmount=(double)premiums.get("premiumRateUnitAmount");	
        double premiumRate20=0.0;
        double premiumRateUnit20=0.0;
        double premiumRate65=0.0;
        double premiumRateUnit65=0.0;
        double adjustedPremium20=0.0;  
        double discountRate20=0.0;
        double actualDiscount65=0.0;   
        double discountRate65=0.0;
        double discountRateUnit20=0.0;
        double discountRateUnit65=0.0;
        double adjustedPremium65=0.0;
        int lastTerm=65 - anb;
    
        adjustedPremium20=(double)premiums.get("adjustedPremiumWithDiscount20");
        premiumRate20=(double)premiums.get("premiumRate20");
        premiumRateUnit20=(double)premiums.get("premiumRateUnitAmount20");	
         discountRate20=(double)premiums.get("discountRate20");
         discountRateUnit20=(double)premiums.get("discountRateUnitAmount20");
        double actualDiscount20=discountRate20/discountRateUnit20;
        if(application.getApplicantAge()<65){
	        adjustedPremium65=(double)premiums.get("adjustedPremiumWithDiscount65");
	        discountRate65=(double)premiums.get("discountRate65");
	         discountRateUnit65=(double)premiums.get("discountRateUnitAmount65");
	         actualDiscount65=discountRate65/discountRateUnit65;
	         premiumRate65=(double)premiums.get("premiumRate65");
	         premiumRateUnit65=(double)premiums.get("premiumRateUnitAmount65");	
	        
        }
        double ciadjustedPremiums=0.0;
        double ciadjustedPremiums20=0.0;
        double ciadjustedPremiums65=0.0;
        double ciRiderPremiumModal = (double)premiums.get("cimodalPremium");
        ciadjustedPremiums=(double)premiums.get("ciadjustedPremiumWithDiscount");
        ciadjustedPremiums20=(double)premiums.get("ciadjustedPremiumWithDiscount20");
        if(application.getApplicantAge()<65){
	        ciadjustedPremiums65=(double)premiums.get("ciadjustedPremiumWithDiscount65");
        }
     
        double total=premiumModal;
        if (application.getIncludeCriticalIllnessRider()!=null&&application.getIncludeCriticalIllnessRider()) {
        	total+=ciRiderPremiumModal;
        }
       
        DecimalFormat df = new DecimalFormat("S$#,###.00");
        data.put(FieldConstants.SUMMARY_MODAL, df.format(premiumModal));
        data.put(FieldConstants.SUMMARY_CI_MODAL, df.format(ciRiderPremiumModal));
        data.put(FieldConstants.SUMMARY_TOTAL_MODAL, df.format(total));
        data.put("ciriderincluded", application.getIncludeCriticalIllnessRider());
        data.put("premiumRate",premiumRate);
        data.put("premiumRateUnitAmount20",premiumRateUnit20);
        data.put("premiumRateUnitAmount65",premiumRateUnit65);
        data.put("premiumRate20",premiumRate20);
        data.put("premiumRate65", premiumRate65);
        data.put("actualDiscount5", actualDiscount5);
        data.put("actualDiscount20", actualDiscount20);
        data.put("actualDiscount65", actualDiscount65);
        data.put("premiumRateUnitAmount",premiumRateUnitAmount);
        data.put("adjustedPremium", adjustedPremium);
        data.put("adjustedPremium20", adjustedPremium20);
        data.put("adjustedPremium65", adjustedPremium65);
        data.put("ciAdjustedPremium",ciadjustedPremiums);
        data.put("ciAdjustedPremium20",ciadjustedPremiums20);
        data.put("ciAdjustedPremium65",ciadjustedPremiums65);
        data.put("modalFactor",modalFactor);
        data.put("underwritingClassFactor",underwritingClassFactor);
        return data;
    }

}
