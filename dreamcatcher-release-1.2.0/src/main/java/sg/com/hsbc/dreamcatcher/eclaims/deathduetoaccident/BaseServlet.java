package sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimBaseServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public abstract class BaseServlet extends EClaimBaseServlet {
    static String FORM_TITLE = "eClaims - Death Due to Accident - Make a Claim";
    static String APPLICATION_SESSION_ATTRIBUTE_NAME = "EClaimDeathDueToAccident";

    public String getFormTitle() {
        return BaseServlet.FORM_TITLE;
    }

    public String getApplicationSessionAttributeName() {
        return BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME;
    }

    public String getBaseFormUrl() {
        return "/eclaims/1-death-due-to-accident";
    }

    public ApplicationForm getById(String id) throws SQLException {
        return DeathDueToAccidentApplication.getById(id);
    }

    public DeathDueToAccidentApplication getApplication(HttpServletRequest request) {
        return (DeathDueToAccidentApplication)super.getApplication(request);
    }
}
