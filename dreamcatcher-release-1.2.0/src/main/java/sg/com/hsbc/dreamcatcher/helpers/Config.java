package sg.com.hsbc.dreamcatcher.helpers;

import java.util.ResourceBundle;

public class Config {

    public static ResourceBundle getConfigurationBundle() {
        return ResourceBundle.getBundle("app");
    }

    public static String getString(String key) {
        return getConfigurationBundle().getString(key);
    }

}
