package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.AgeUtils;


/**
 * Step8CalculationServlet is a servlet which allows
 * the application to fetch the fresh premium values 
 * ,both annual and mode opted,
 * based on the inputs passed to the post method
 * of the class via a ajax request.
 * 
 * 
 * @author hiteshchellani
 *
 */
@WebServlet(urlPatterns = "/our-plans/online-protector/recalculate")
public class Step8CalculationServlet extends HttpServlet {

	private final static Logger logger = Logger.getLogger(Step8CalculationServlet.class);
  protected DecimalFormat df = new DecimalFormat("#,###.00");
	
	/* The method creates the object of OnlineProtectorApplication based on the applicationid saved in the session
	 * It sends the map which contains the premium values to be used by the frontend  
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 * @return void
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			OnlineProtectorApplication application = OnlineProtectorApplication
					.getById(request.getSession().getAttribute("OnlineProtectorApplication").toString());
			Map<String, Object> data = sanitize(request, application);
			Map premiums = processData(application, data);
			JSONObject n = new JSONObject(premiums);
			response.setContentType("application/json");
			response.getWriter().println(n);
		} catch (Exception e) {
			logger.error("Error in handling the post request for calculating premiums " + e);
		}

	}

	
	/**
	 * Set the values received as request parameter  in the OnlineProtectorApplication to calculate the premium
	 * to be paid
	 * Creates a map which holds the values returned after calculation of the premium
	 * 
	 * @param application    The OnlineProtector application created in doPost method
	 * @param data           The values received from the request parameter in map form
	 * @return               Map holding the premium values to be used in the frontend.
	 *                       Empty map if exception is thrown
	 */
	private Map processData(OnlineProtectorApplication application, Map<String, Object> data) {

		if (data.get("uw_lifestyle_questions_optin") != null) {
			application.setUnderwritingLifestyleQuestionsOptin((boolean) data.get("uw_lifestyle_questions_optin"));
		} else {
			application.setUnderwritingLifestyleQuestionsOptin(null);
		}

		if (data.get("uw_lifestyle_mortgage") != null) {
			application.setUnderwritingLifestyleMortgage((int) data.get("uw_lifestyle_mortgage"));
		} else {
			application.setUnderwritingLifestyleMortgage(null);
		}

		if (data.get("uw_lifestyle_profile") != null) {
			application.setUnderwritingLifestyleProfile((int) data.get("uw_lifestyle_profile"));
		} else {
			application.setUnderwritingLifestyleProfile(null);
		}

		if (data.get("uw_lifestyle_height") != null) {
			application.setUnderwritingLifestyleHeight((float) data.get("uw_lifestyle_height"));
		} else {
			application.setUnderwritingLifestyleHeight(null);
		}

		if (data.get("uw_lifestyle_weight") != null) {
			application.setUnderwritingLifestyleWeight((float) data.get("uw_lifestyle_weight"));
		} else {
			application.setUnderwritingLifestyleWeight(null);
		}

		if (data.get("uw_lifestyle_exercise") != null) {
			application.setUnderwritingLifestyleExercise((int) data.get("uw_lifestyle_exercise"));
		} else {
			application.setUnderwritingLifestyleExercise(null);
		}
		Map premiums = new HashMap<>();
		try {
			premiums = application.getPremiumAtAge(AgeUtils.calculateAgeNextBirthday(
					application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
			if (application.getIncludeCriticalIllnessRider()) {
				Map cipremiums = application.getCriticalIllnessRiderPremiumAtAge(AgeUtils.calculateAgeNextBirthday(
						application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
				double ciRiderPremiumModal = (double) cipremiums.get("modalPremium");
				double ciRiderPremiumAnnual = ciRiderPremiumModal * application.getPaymentMode();
				premiums.put("ciRiderPremiumModal", ciRiderPremiumModal);
				premiums.put("ciRiderPremiumAnnual",ciRiderPremiumAnnual);
				

			}
			if (application.getIncludeRefundRider()) {
				Map roppremiums = application.getRefundRiderPremiumAtAge(AgeUtils.calculateAgeNextBirthday(
						application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
				float ropRiderPremiumModal = (float) roppremiums.get("modalPremium");
				float ropRiderPremiumAnnual = ropRiderPremiumModal * application.getPaymentMode();
				premiums.put("ropRiderPremiumModal", df.format(ropRiderPremiumModal));
				premiums.put("ropRiderPremiumAnnual",df.format(ropRiderPremiumAnnual));

			}
		} catch (Exception e) {
			logger.error("Error in calculating the premium based on new lifestyle questions " + e);
		}

		return premiums;
	}

	
	/**
	 * This method creates a map which contains the parameter values recived as request from the 
	 * ajax call.The map is updated based on the values received as input
	 * 
	 * @param request      The request object received from the servlet doPost method
	 * @param application  The OnlineProtector application created in doPost method
	 * @return             Map of values retrieved from the ajax request
	 */
	public Map<String, Object> sanitize(HttpServletRequest request, OnlineProtectorApplication application) {

		Map<String, Object> sanitized = new HashMap<>();

		if (application.isEligiblePreferentialRates()) {
			sanitized = sanitizeLifestyleQuestionsOptin(request, sanitized);

			if (sanitized.get("uw_lifestyle_questions_optin") != null
					&& (boolean) sanitized.get("uw_lifestyle_questions_optin")) {
				sanitized = sanitizeLifestyleMortgage(request, sanitized);
				sanitized = sanitizeLifestyleProfile(request, sanitized);
				sanitized = sanitizeLifestyleHeight(request, sanitized);
				sanitized = sanitizeLifestyleWeight(request, sanitized);
				sanitized = sanitizeLifestyleExercise(request, sanitized);
			}
		}
		return sanitized;
	}

	
	/**
	 * Read lifestyle opted in parameter passed in ajax request.Add null to the map
	 * parameter value if null is passed from ajax
	 * 
	 * @param request       The request object received from the servlet doPost method
	 * @param sanitized     The map which contains the parameter values passed as input via ajax
	 * @return              The updated map which contains the parameter values passed as input
	 *                      to the servlet via ajax request
	 */
	private Map<String, Object> sanitizeLifestyleQuestionsOptin(HttpServletRequest request,
			Map<String, Object> sanitized) {
		String input = request.getParameter("uw_lifestyle_questions_optin");
		if (input != null) {
			sanitized.put("uw_lifestyle_questions_optin", input.equals("Y"));
		} else {
			sanitized.put("uw_lifestyle_questions_optin", null);
		}

		return sanitized;
	}

	
	/**
	 * Read lifestyle mortgage parameter passed in ajax request.Add null to the map
	 * parameter value if null is passed from ajax
	 * 
	 * @param request       The request object received from the servlet doPost method
	 * @param sanitized     The map which contains the parameter values passed as input via ajax
	 * @return              The updated map which contains the parameter values passed as input
	 *                      to the servlet via ajax request
	 */
	private Map<String, Object> sanitizeLifestyleMortgage(HttpServletRequest request, Map<String, Object> sanitized) {
		String input = request.getParameter("uw_lifestyle_mortgage");

		try {
			sanitized.put("uw_lifestyle_mortgage", Integer.parseInt(input));

		} catch (Exception e) {
			sanitized.put("uw_lifestyle_mortgage", null);
			System.out.println("err is thrown in setting mortgage value " + e);
		}

		return sanitized;
	}
	
	/**
	 * Read lifestyle profile parameter passed in ajax request.Add null to the map
	 * parameter value if null is passed from ajax
	 * 
	 * @param request       The request object received from the servlet doPost method
	 * @param sanitized     The map which contains the parameter values passed as input via ajax
	 * @return              The updated map which contains the parameter values passed as input
	 *                      to the servlet via ajax request
	 */
	private Map<String, Object> sanitizeLifestyleProfile(HttpServletRequest request, Map<String, Object> sanitized) {
		String input = request.getParameter("uw_lifestyle_profile");

		try {
			sanitized.put("uw_lifestyle_profile", Integer.parseInt(input));
		} catch (Exception e) {
			sanitized.put("uw_lifestyle_profile", null);
		}

		return sanitized;
	}

	
	/**
	 * Read lifestyle height parameter passed in ajax request.Add null to the map
	 * parameter value if null is passed from ajax
	 * 
	 * @param request       The request object received from the servlet doPost method
	 * @param sanitized     The map which contains the parameter values passed as input via ajax
	 * @return              The updated map which contains the parameter values passed as input
	 *                      to the servlet via ajax request
	 */
	private Map<String, Object> sanitizeLifestyleHeight(HttpServletRequest request, Map<String, Object> sanitized) {
		String input = request.getParameter("uw_lifestyle_height");

		try {
			sanitized.put("uw_lifestyle_height", Float.parseFloat(input));
		} catch (Exception e) {
			sanitized.put("uw_lifestyle_height", null);
		}

		return sanitized;
	}

	/**
	 * Read lifestyle height parameter passed in ajax request.Add null to the map
	 * parameter value if null is passed from ajax
	 * 
	 * @param request       The request object received from the servlet doPost method
	 * @param sanitized     The map which contains the parameter values passed as input via ajax
	 * @return              The updated map which contains the parameter values passed as input
	 *                      to the servlet via ajax request
	 */
	private Map<String, Object> sanitizeLifestyleWeight(HttpServletRequest request, Map<String, Object> sanitized) {
		String input = request.getParameter("uw_lifestyle_weight");

		try {
			sanitized.put("uw_lifestyle_weight", Float.parseFloat(input));
		} catch (Exception e) {
			sanitized.put("uw_lifestyle_weight", null);
		}

		return sanitized;
	}

	/**
	 * Read lifestyle exercise parameter passed in ajax request.Add null to the map
	 * parameter value if null is passed from ajax
	 * 
	 * @param request       The request object received from the servlet doPost method
	 * @param sanitized     The map which contains the parameter values passed as input via ajax
	 * @return              The updated map which contains the parameter values passed as input
	 *                      to the servlet via ajax request
	 */
	private Map<String, Object> sanitizeLifestyleExercise(HttpServletRequest request, Map<String, Object> sanitized) {
		String input = request.getParameter("uw_lifestyle_exercise");

		try {
			sanitized.put("uw_lifestyle_exercise", Integer.parseInt(input));
		} catch (Exception e) {
			sanitized.put("uw_lifestyle_exercise", null);
		}

		return sanitized;
	}

}
