package sg.com.hsbc.dreamcatcher.admin.entity;

public enum ResendStatus {
    PENDING("PENDING"),
    SUCCESS("SUCCESS"),
    FAIL("FAIL");

    private final String code;

    ResendStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
