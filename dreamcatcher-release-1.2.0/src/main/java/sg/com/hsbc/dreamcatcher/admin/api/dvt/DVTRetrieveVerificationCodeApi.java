package sg.com.hsbc.dreamcatcher.admin.api.dvt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sg.com.hsbc.dreamcatcher.admin.AdminUtil;
import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendHistory;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendStatus;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendType;
import sg.com.hsbc.dreamcatcher.admin.repository.ApplicationsRepository;
import sg.com.hsbc.dreamcatcher.admin.repository.ResendHistoryRepository;
import sg.com.hsbc.dreamcatcher.admin.service.AdminService;
import sg.com.hsbc.dreamcatcher.admin.service.impl.AdminDVTServiceImpl;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.DirectValueTermApplication;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/retrieve-verification-code/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/retrieve_verification_code.jsp")
})
public class DVTRetrieveVerificationCodeApi extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(DVTRetrieveVerificationCodeApi.class);

    private AdminService adminDVTService;
    private ResendHistoryRepository resendHistoryRepository;
    private ResendHistory resendHistory;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("direct-value-term retrieve verification code page");
        render(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> data = new HashMap<>();
        Map<String, String> errors;

        errors = validate(request);
        if (!errors.isEmpty()) {
            render(request, response);
            return;
        }

        String email = request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL);
        String countryCode = request.getParameter(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE);
        String mobileNo = request.getParameter(FieldConstants.APPLICANT_MOBILE_NUM);

        adminDVTService = new AdminDVTServiceImpl();

        try{
        	ApplicationsRepository applicationsRepository = new ApplicationsRepository();
            List<Applications> applicationList = applicationsRepository.findByMobileNoAndEmail(DirectValueTermApplication.PRODUCT_TYPE, 0, countryCode, mobileNo, email);
            if ( applicationList==null||applicationList.size()<=0) {
                boolean emailExist = adminDVTService.isImcompleteEmailExisted(email);
                boolean mobileExist = adminDVTService.isImcompleteMobileExisted(countryCode,mobileNo);

                errors = AdminUtil.validateEmailAndMobileNotMatch(emailExist, mobileExist);
            } else {
            	Applications applications = applicationList.get(0);
            	//Send verifcation code via sms
            	if (applications.getDropoutPassword() == null) {
                    logger.info("Generating new verification code");
            		String code = UUID.randomUUID().toString().split("-")[0].toLowerCase();
                    applications.setDropoutPassword(code);
                    applicationsRepository.updateVerficiationCode(applications,code);
                }
                adminDVTService.resendVerification(applications);
                logger.info("Send SMS successful");

                //send sms success and save to db
                resendHistoryRepository = new ResendHistoryRepository();
                resendHistory = AdminUtil.convertApplicationsToResendHistory(
                        applications, ResendType.DVT_RESEND_VERIFICATION_CODE, ResendStatus.SUCCESS,new Date());
                resendHistoryRepository.save(resendHistory);
                logger.info("{} save to db success", ResendType.DVT_RESEND_VERIFICATION_CODE);
                errors.put("success", "success");
            }
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
            errors.put(FieldConstants.APPLICANT_EMAIL, "Sorry, either your email address or phone number is incorrect. Please try again.");
            throw e;
        }

        request.setAttribute("errors", errors);
        request.setAttribute(FieldConstants.APPLICANT_EMAIL, email);
        request.setAttribute(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, countryCode);
        request.setAttribute(FieldConstants.APPLICANT_MOBILE_NUM, mobileNo);

        render(request, response);
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }
    /**
     * This is the main validation function for the retrieval servlet. It will call individual function to validateEmailAndVerificationCode each field.
     * @param request
     * @return errors
     */
    public Map<String, String> validate(HttpServletRequest request) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();

        String inputEmail = request.getParameter(FieldConstants.APPLICANT_EMAIL);
        data.put(FieldConstants.APPLICANT_EMAIL, inputEmail);

        String applicantCountryCode = request.getParameter(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE);
        data.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, applicantCountryCode);

        String applicantPhoneNum = request.getParameter(FieldConstants.APPLICANT_MOBILE_NUM);
        data.put(FieldConstants.APPLICANT_MOBILE_NUM, applicantPhoneNum);

        errors = validateApplicantEmail(data, errors);
        errors = AdminUtil.validateApplicantCountryCode(data, errors);
        errors = validateApplicantPhoneNum(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return errors;
    }

    /**
     * This function validates the email field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantEmail(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_EMAIL))) {
	        case Validator.ERROR_STRING_EMPTY:
	            errors.put(FieldConstants.APPLICANT_EMAIL, "Your email address is missing.");
	            break;
	    }
	    
	    switch (Validator.validateEmail(data.get(FieldConstants.APPLICANT_EMAIL))) {
	        case Validator.ERROR_EMAIL_FORMAT_INVALID:
	            errors.put(FieldConstants.APPLICANT_EMAIL, "Your email address is invalid.");
	            break;
	    }
        return errors;
    }

    /**
     * This function validates the phone number field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantPhoneNum(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_MOBILE_NUM))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_MOBILE_NUM, "Your phone number is missing.");
                break;
        }
        return errors;
    }
}