package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import sg.com.hsbc.dreamcatcher.helpers.*;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/5", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step5.jsp")
})
public class Step5Servlet extends BaseServlet {
    private final static Logger logger = Logger.getLogger(Step5Servlet.class);

    public float getCurrentStep() {
        return 5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeApplicantSmoker(request, sanitized);
        sanitized = sanitizeExistingLifeInsuranceCoverage(request, sanitized);
        sanitized = sanitizeInsuredMedical1(request, sanitized);
        sanitized = sanitizeInsuredMedical2(request, sanitized);
        sanitized = sanitizeInsuredMedical3(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();

        errors = validateApplicantSmoker(data, errors);
        errors = validateExistingLifeInsuranceCoverage(data, errors);
        errors = validateInsuredMedical1(data, errors);
        errors = validateInsuredMedical2(data, errors);
        errors = validateInsuredMedical3(data, errors);
        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        if (data.get(FieldConstants.APPLICANT_SMOKER) != null) {
            application.setApplicantSmoker((boolean) data.get(FieldConstants.APPLICANT_SMOKER));
        }

        if (data.get(FieldConstants.EXISTING_LIFE_INSURANCE_COVERAGE) != null) {
            try {
                application.setExistingLifeInsuranceCoverage(NumberFormat.getNumberInstance(java.util.Locale.US).parse(data.get(FieldConstants.EXISTING_LIFE_INSURANCE_COVERAGE).toString()).floatValue());
                logger.info("the existing life insurance coverage is "+application.getExistingLifeInsuranceCoverage());
            } catch (ParseException e) {
                ErrorHandler.handleError(application, e, null);
                return false;
            }
        }

        if (data.get(FieldConstants.INSURED_MEDICAL_1) != null) {
            application.setInsuredMedical1((boolean) data.get(FieldConstants.INSURED_MEDICAL_1));
        } else {
            application.setInsuredMedical1(null);
        }

        if (data.get(FieldConstants.INSURED_MEDICAL_2) != null) {
            application.setInsuredMedical2((boolean) data.get(FieldConstants.INSURED_MEDICAL_2));
        } else {
            application.setInsuredMedical2(null);
        }

        if (data.get(FieldConstants.INSURED_MEDICAL_3) != null) {
            application.setInsuredMedical3((boolean) data.get(FieldConstants.INSURED_MEDICAL_3));
        } else {
            application.setInsuredMedical3(null);
        }

        if (application.getInsuredMedical1() || application.getInsuredMedical2() || application.getInsuredMedical3()) {
            application.setUnderwritingLifestyleQuestionsOptin(null);
            application.setUnderwritingLifestyleMortgage(null);
            application.setUnderwritingLifestyleProfile(null);
            application.setUnderwritingLifestyleHeight(null);
            application.setUnderwritingLifestyleWeight(null);
            application.setUnderwritingLifestyleExercise(null);
        }

        application.setMaxStep(getCurrentStep() + 2);
        System.out.println("now getting saved in step 5"+application.getExistingLifeInsuranceCoverage());
        return application.save();
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("/our-plans/direct-value-term/7");
    }


    private Map<String, String> validateApplicantSmoker(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_SMOKER))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_SMOKER, "Please indicate if you have smoked in the last 12 months.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateExistingLifeInsuranceCoverage(Map<String, Object> data, Map<String, String> errors) {
        try {
            float sa = !StringUtils.isEmpty(data.get(FieldConstants.EXISTING_LIFE_INSURANCE_COVERAGE).toString()) ? Float.parseFloat(data.get(FieldConstants.EXISTING_LIFE_INSURANCE_COVERAGE).toString().replace(",", "")) : 0f;
        } catch (Exception e) {
            logger.error(e.getStackTrace());
            errors.put(FieldConstants.EXISTING_LIFE_INSURANCE_COVERAGE, "The number format cannot be determined.");
        }
        return errors;
    }

    private Map<String, String> validateInsuredMedical1(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.INSURED_MEDICAL_1))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.INSURED_MEDICAL_1, "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredMedical2(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.INSURED_MEDICAL_2))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.INSURED_MEDICAL_2, "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredMedical3(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.INSURED_MEDICAL_3))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.INSURED_MEDICAL_3, "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredMedical4(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.INSURED_MEDICAL_4))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.INSURED_MEDICAL_4, "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantSmoker(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_SMOKER);
        if (input != null) {
            sanitized.put(FieldConstants.APPLICANT_SMOKER, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.APPLICANT_SMOKER, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeExistingLifeInsuranceCoverage(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.EXISTING_LIFE_INSURANCE_COVERAGE);

        float coverage = !StringUtils.isEmpty(input) ? Float.parseFloat(input.toString().replace(",", "")) : 0f;
        sanitized.put(FieldConstants.EXISTING_LIFE_INSURANCE_COVERAGE, coverage);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMedical1(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.INSURED_MEDICAL_1);
        if (input != null) {
            sanitized.put(FieldConstants.INSURED_MEDICAL_1, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.INSURED_MEDICAL_1, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMedical2(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.INSURED_MEDICAL_2);
        if (input != null) {
            sanitized.put(FieldConstants.INSURED_MEDICAL_2, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.INSURED_MEDICAL_2, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMedical3(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.INSURED_MEDICAL_3);
        if (input != null) {
            sanitized.put(FieldConstants.INSURED_MEDICAL_3, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.INSURED_MEDICAL_3, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMedical4(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.INSURED_MEDICAL_4);
        if (input != null) {
            sanitized.put(FieldConstants.INSURED_MEDICAL_4, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.INSURED_MEDICAL_4, null);
        }

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        data.put(FieldConstants.APPLICANT_SMOKER, application.getApplicantSmoker());
        data.put(FieldConstants.EXISTING_LIFE_INSURANCE_COVERAGE, application.getExistingLifeInsuranceCoverage());
        data.put(FieldConstants.INSURED_MEDICAL_1, application.getInsuredMedical1());
        data.put(FieldConstants.INSURED_MEDICAL_2, application.getInsuredMedical2());
        data.put(FieldConstants.INSURED_MEDICAL_3, application.getInsuredMedical3());
        data.put(FieldConstants.INSURED_MEDICAL_4, application.getInsuredMedical4());
        data.put(FieldConstants.INCLUDE_CI_RIDER, application.getIncludeCriticalIllnessRider());

        request.setAttribute("data", data);
    }

}
