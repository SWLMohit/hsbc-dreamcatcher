package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoaccident;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.eclaims.EclaimStep4aProcessor;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/6-tpd-due-to-accident/3", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/6_tpd_due_to_accident/step3a.jsp")
})
public class Step3aServlet extends BaseServlet {
    private EclaimStep4aProcessor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new EclaimStep4aProcessor(this);
    }

    public float getCurrentStep() {
        return (float)3;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        return processor.process(request);
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        EClaimApplication application = getApplication(request);
        if (application.getPayment() == EClaimApplication.PAYMENT_TRANSFER_TO_BANK_ACCOUNT) {
            response.sendRedirect(getBaseFormUrl() + "/3a");
        } else {
            response.sendRedirect(getBaseFormUrl() + "/4");
        }
    }
}
