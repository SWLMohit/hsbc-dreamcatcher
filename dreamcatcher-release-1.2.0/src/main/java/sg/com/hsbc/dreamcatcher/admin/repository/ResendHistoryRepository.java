package sg.com.hsbc.dreamcatcher.admin.repository;

import sg.com.hsbc.dreamcatcher.admin.entity.ResendHistory;
import sg.com.hsbc.dreamcatcher.helpers.Database;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;

import java.sql.*;
import java.util.List;

public class ResendHistoryRepository {

    public void save (ResendHistory resendHistory) {
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("INSERT INTO resend_history (application_id, email, country_code, mobile_no, type, status, send_time, date_created) VALUES(?,?,?,?,?,?,NOW(),NOW())");
            stmt.setString(1, resendHistory.getApplicationId());
            stmt.setString(2, resendHistory.getEmail());
            stmt.setString(3, resendHistory.getCountryCode());
            stmt.setString(4, resendHistory.getMobileNo());
            stmt.setString(5, resendHistory.getType());
            stmt.setString(6, resendHistory.getStatus());

            stmt.executeUpdate();
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
        }finally {
            db.closeAll(conn,stmt);
        }
    }
}
