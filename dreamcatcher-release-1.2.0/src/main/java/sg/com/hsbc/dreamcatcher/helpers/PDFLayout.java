package sg.com.hsbc.dreamcatcher.helpers;

import java.awt.Color;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.HorizontalAlignment;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.VerticalAlignment;
import be.quodlibet.boxable.line.LineStyle;

public class PDFLayout {
    private final static Logger logger = Logger.getLogger(PDFLayout.class);
    public static float myFont=0;
	public static int border;

	public static float mainTitleList(String[] titles, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        pd.setFont(PDFGenerator.FONT_BOLD, 10);
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
        for (String title : titles) {
            Row r1 = table.createRow(9);
            Cell c1 = r1.createCell(100, title, HorizontalAlignment.CENTER, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(10);
            c1.setBottomPadding(0);
            c1.setTopPadding(0);
        }

        yStart = table.draw() - btmPad;
        return yStart;
    }
    public static float mainTitleListLeft(String[] titles, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
       // pd.setFont(PDFGenerator.FONT_BOLD, 10);
    	BaseTable table=null;
    	if(PDFLayout.border>0){
         table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, true, true);
    	}else{
    		 table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
    	}
    	int titlesLength=0;
        for (String title : titles) {
        	titlesLength++;

            boolean isRed = false;
            boolean isBol = false;
            if ( title.contains("<red>") ) {
                isRed = true;
                title = title.replaceFirst("<red>", "");
            }
            if ( title.contains("<bol>") ) {
                isBol=true;
                title = title.replaceFirst("<bol>", "");
            }

            Row r1 = table.createRow(9);
            Cell c1 = r1.createCell(100, title, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
           
         
            c1.setTextColor(Color.BLACK);
            if ( isRed )
               c1.setTextColor(Color.RED);
            
            if(PDFLayout.myFont>0){
            	c1.setFontSize(10);
            }else{
            	c1.setFontSize(pd.fontSize());
            }
            if(PDFLayout.border>0){
            	logger.info("the border is to be added");
            	c1.setLeftPadding(7);
            	c1.setRightPadding(7);
            	
             c1.setLeftBorderStyle(new LineStyle(Color.GRAY, 1));
             c1.setRightBorderStyle(new LineStyle(Color.GRAY, 1));
             if(titlesLength==1){
            	 c1.setTopPadding(5);
             }else{
            	 c1.setTopPadding(0);
             }
             if(titlesLength==titles.length){
            	 c1.setBottomBorderStyle(new LineStyle(Color.GRAY, 1));
            	 c1.setBottomPadding(5);
             }else{
             c1.setBottomBorderStyle(null);
             c1.setBottomPadding(0);
             }
             c1.setTopBorderStyle(null);
             
            }else{
            	c1.setLeftPadding(0);
            	c1.setTopPadding(0);
            	c1.setBottomPadding(0);
            	c1.setRightPadding(0);
            }
            
              if(isBol){
            	 
                c1.setFontBold(PDType1Font.HELVETICA_BOLD);
                
            }
        }
        
        yStart = table.draw() - btmPad;
        return yStart;
    }
    
    public static float mainTitleListRight(String[] titles, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        // pd.setFont(PDFGenerator.FONT_BOLD, 10);
         BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
         for (String title : titles) {
             Row r1 = table.createRow(9);
             Cell c1 = r1.createCell(100, title, HorizontalAlignment.RIGHT, VerticalAlignment.TOP);
             c1.setFont(pd.getFont());
             
             if(PDFLayout.myFont>0){
             	c1.setFontSize(myFont);
             }else{
             c1.setFontSize(9);
             }
             c1.setBottomPadding(0);
             c1.setRightPadding(0);
             c1.setTopPadding(0);
             c1.setLeftPadding(0);
         }

         yStart = table.draw() - btmPad;
         return yStart;
     }

    public static float title(String title, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
        Row rTitle = table.createRow(9);
        Cell cTitle = rTitle.createCell(100, title, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
        cTitle.setTopPadding(0);
        cTitle.setBottomPadding(10);
        cTitle.setLeftPadding(0);
        cTitle.setRightPadding(0);
        pd.setFont(PDFGenerator.FONT_BOLD, 9);
        cTitle.setFont(pd.getFont());
        cTitle.setFontSize(9);
        yStart = table.draw() - btmPad;
        return yStart;
    }

    public static float titledParagraphList(String title, String[] lines, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        setCurrentPage(page, pd);
        yStart = title(title, yStart, yStartNewPage, tableWidth, page, pd, 10);
        yStart = paragraphList(lines, yStart, yStartNewPage, tableWidth, page, pd, btmPad);
        return yStart;
    }

    public static float paragraphList(String[] lines, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        setCurrentPage(page, pd);
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
        for (int j = 0; j < lines.length; j++) {
            Row r = table.createRow(9);
            Cell c = r.createCell(100, lines[j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c.setTopPadding(0);
            c.setBottomPadding(10);
            c.setLeftPadding(0);
            c.setRightPadding(0);
            pd.setFont(PDFGenerator.FONT_NORMAL, 9);
            c.setFont(pd.getFont());
            c.setFontSize(9);
        }
        yStart = table.draw() - btmPad;
        return yStart;
    }

    public static float boldParagraphList(String[] lines, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        setCurrentPage(page, pd);
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
        for (int j = 0; j < lines.length; j++) {
            Row r = table.createRow(9);
            Cell c = r.createCell(100, lines[j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c.setTopPadding(0);
            c.setBottomPadding(10);
            c.setLeftPadding(0);
            c.setRightPadding(0);
            pd.setFont(PDFGenerator.FONT_BOLD, 9);
            c.setFont(pd.getFont());
            c.setFontSize(9);
        }
        yStart = table.draw() - btmPad;
        return yStart;
    }

    public static float labelValueList(List<String[]> labelValueList, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        setCurrentPage(page, pd);
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
        for (int i = 0; i < labelValueList.size(); i++) {
            if (i == 15) {
                pd.addTableRow(table, labelValueList.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY3);
            } else {
                if (i == 16 || i == 17) {
                    pd.addTableRow(table, labelValueList.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY4);
                } else {
                    pd.addTableRow(table, labelValueList.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY2);
                }
            }

        }
        yStart = table.draw() - btmPad;
        return yStart;
    }

    public static float tableAlignCentre(String title, List<List> labelValueList, boolean hasHeader, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        setCurrentPage(page, pd);
        BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
        PlanDataTable t = new PlanDataTable(dataTable, page,HorizontalAlignment.CENTER);
        t.addListToTable(labelValueList, hasHeader ? PlanDataTable.HASHEADER : PlanDataTable.NOHEADER, title, HorizontalAlignment.CENTER);
        yStart = dataTable.draw() - btmPad;
        return yStart;
    }

    public static float table(String title, List<List> labelValueList, boolean hasHeader, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad) throws IOException {
        boolean drawline = true;
        return table(title, labelValueList, hasHeader, yStart, yStartNewPage, tableWidth, page, pd, btmPad, drawline);
    }
    public static float table(String title, List<List> labelValueList, boolean hasHeader, float yStart, float yStartNewPage, float tableWidth, PDPage page, PDFGenerator pd, float btmPad, boolean drawline) throws IOException {
        //setCurrentPage(page, pd);
        BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, drawline, true);
        PlanDataTable t = new PlanDataTable(dataTable, page);
        t.addListToTable(labelValueList, hasHeader ? PlanDataTable.HASHEADER : PlanDataTable.NOHEADER, title,HorizontalAlignment.LEFT);
        
        yStart = dataTable.draw() - btmPad;
        return yStart;
    }

    /**
     * This exists to ensure no text overwriting due to page overflow
     * @param page
     * @param pd
     */
    public static void setCurrentPage(PDPage page, PDFGenerator pd){
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
    }
}
