package sg.com.hsbc.dreamcatcher.plans.directvalueterm;


import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/bi")
public class DownloadBIServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME) != null) {
            DirectValueTermApplication application = DirectValueTermApplication.getById(request.getSession().getAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME).toString());
            application.generateBIPDF();

            String filename = application.getPDFFilename(PDFGenerator.PDF_TYPE_BENEFIT_ILLUSTRATION);
            File file = new File(filename);

            response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename=" + "BenefitIllustration.pdf");
            response.setContentLength((int) file.length());

            FileInputStream fileInputStream = new FileInputStream(file);
            OutputStream responseOutputStream = response.getOutputStream();
            int bytes;
            while ((bytes = fileInputStream.read()) != -1) {
                responseOutputStream.write(bytes);
            }

            file.delete();
        }
    }
}
