package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.BaseServlet;
import sg.com.hsbc.dreamcatcher.helpers.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/index.jsp")
})
public class IndexServlet extends BaseServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map<String, Object> data = new HashMap<>();
        data.put("sum_assured", 400000f);
        data.put("rider_sum_assured", 50000f);

        //HSBCICDU-973 disable ROP
        data.put("rop_enabled", Boolean.FALSE);
        String ropFlag = Config.getString("dreamcatcher.rop.enabled");
        if ("true".equalsIgnoreCase(ropFlag))
           data.put("rop_enabled", Boolean.TRUE);

        request.setAttribute("data", data);
        render(request, response);
    }
}
