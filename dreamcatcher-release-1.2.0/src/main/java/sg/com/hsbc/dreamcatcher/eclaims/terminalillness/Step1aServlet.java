package sg.com.hsbc.dreamcatcher.eclaims.terminalillness;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.eclaims.EclaimIllnessStep1aProcessor;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/4-terminal-illness/1", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/4_terminal_illness/step1a.jsp")
})
public class Step1aServlet extends BaseServlet {
    private EclaimIllnessStep1aProcessor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new EclaimIllnessStep1aProcessor(this);
    }

    public float getCurrentStep() {
        return (float) 1;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        return processor.process(request);
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void populateFormData(HttpServletRequest request) {
        processor.populateFormData(request);
    }

    protected float getStep() {
        return (float) 0.5;
    }
}
