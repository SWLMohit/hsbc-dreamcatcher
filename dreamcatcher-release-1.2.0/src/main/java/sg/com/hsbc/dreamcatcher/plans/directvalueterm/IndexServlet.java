package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.BaseServlet;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/index.jsp")
})
public class IndexServlet extends BaseServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map<String, Object> data = new HashMap<>();
        data.put(FieldConstants.SUM_ASSURED, 200000f);
        data.put(FieldConstants.RIDER_SUM_ASSURED, 200000f);
        data.put(FieldConstants.MIN_SUM_ASSURED, DirectValueTermApplication.getMinSumAssured());
        data.put(FieldConstants.MAX_SUM_ASSURED, DirectValueTermApplication.getMaxSumAssured());
        data.put(FieldConstants.MIN_RIDER_SUM_ASSURED, DirectValueTermApplication.getMinRiderSumAssured());
        data.put(FieldConstants.MAX_RIDER_SUM_ASSURED, DirectValueTermApplication.getMaxRiderSumAssured());

        request.setAttribute("data", data);
        render(request, response);
    }
}
