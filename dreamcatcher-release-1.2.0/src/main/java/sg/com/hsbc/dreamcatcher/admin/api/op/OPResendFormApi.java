package sg.com.hsbc.dreamcatcher.admin.api.op;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.com.hsbc.dreamcatcher.admin.AdminUtil;
import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendHistory;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendStatus;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendType;
import sg.com.hsbc.dreamcatcher.admin.repository.ResendHistoryRepository;
import sg.com.hsbc.dreamcatcher.admin.service.AdminService;
import sg.com.hsbc.dreamcatcher.admin.service.impl.AdminOPServiceImpl;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/resend-form/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/resend_form.jsp")
})
public class OPResendFormApi extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(OPResendFormApi.class);

    private AdminService adminOPService;
    private Applications applications;
    private ResendHistoryRepository resendHistoryRepository;
    private ResendHistory resendHistory;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        render(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> errors;

        errors = new AdminUtil().validateEmailAndMobileNo(request);
        if (!errors.isEmpty()) {
            render(request, response);
            return;
        }
        adminOPService = new AdminOPServiceImpl();

        applications = new Applications();
        applications.setProductType(FieldConstants.OP_PRODUCT_TYPE);
        applications.setApplicantEmail(request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL));
        applications.setApplicantMobileCountryCode(request.getParameter(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE));
        applications.setApplicantMobileNum(request.getParameter(FieldConstants.APPLICANT_MOBILE_NUM));
        logger.info("product type:{}, email: {}, mobile: {},{}",
                applications.getProductType(), applications.getApplicantEmail(),applications.getApplicantMobileCountryCode(),applications.getApplicantMobileNum());


        List<Applications> applicationsList = adminOPService.listAllPolicy(applications);

        if (applicationsList != null && applicationsList.size() > 0) {
            if (applicationsList.size() > 1) {
                // multiple policy
                String policyList = AdminUtil.convertListToString(applicationsList);
                logger.info("Policy list ", policyList);

                request.getSession().setAttribute("PolicyList", policyList);
                request.getSession().setAttribute(FieldConstants.RETRIEVE_TYPE, FieldConstants.RETRIEVE_FORM);

                response.sendRedirect("/our-plans/online-protector/resend-form-multi/");
                return;
            } else {
                applications = applicationsList.get(0);
                //single policy
                adminOPService.resendEmail(applications);
                errors.put("success", "success");
                logger.info("send email successful");

                resendHistoryRepository = new ResendHistoryRepository();
                resendHistory = AdminUtil.convertApplicationsToResendHistory(
                        applications, ResendType.OP_RESEND_EMAIL_AND_PASS_CODE, ResendStatus.SUCCESS,new Date());
                resendHistoryRepository.save(resendHistory);
                logger.info("{} save to db success", ResendType.OP_RESEND_EMAIL_AND_PASS_CODE);
            }
        } else {
            boolean emailExist = adminOPService.isCompleteEmailExisted(applications.getApplicantEmail());
            boolean mobileExist = adminOPService.isCompleteMobileExisted(applications.getApplicantMobileCountryCode(), applications.getApplicantMobileNum());

            errors = AdminUtil.validateEmailAndMobileNotMatch(emailExist, mobileExist);

        }
        request.setAttribute("errors", errors);

        render(request, response);
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }
}
