package sg.com.hsbc.dreamcatcher.plans.onlineprotector;


import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;

@WebServlet(urlPatterns = "/our-plans/online-protector/bi")
public class DownloadBIServlet extends HttpServlet {
	private final static Logger logger = Logger.getLogger(DownloadBIServlet.class);
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException   {
    	
    	if (request.getSession().getAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME) != null) {
            OnlineProtectorApplication application = OnlineProtectorApplication.getById(request.getSession().getAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME).toString());
            application.generateAdminBIPDF();

            String filename = application.getPDFFilename(PDFGenerator.PDF_TYPE_BENEFIT_ILLUSTRATION, true);
            File file = new File(filename);
            response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename=" + "BenefitIllustration.pdf");
            response.setContentLength((int) file.length());

            FileInputStream fileInputStream = null;
            OutputStream responseOutputStream = null;
			try {
				fileInputStream = new FileInputStream(file);
				responseOutputStream = response.getOutputStream();
				byte[] n=IOUtils.toByteArray(fileInputStream);
				responseOutputStream.write(n);
				file.delete();
			}
            catch (FileNotFoundException e) {
			logger.error("Error Message "+e);
			}
			catch (IOException e) {
				logger.error("Error Message "+e);
			}
			catch(Exception e){
				logger.error("Error Message "+e);
			}
           finally{
        	   try {
        		   fileInputStream.close();
			} catch (IOException e) {
				logger.error("Error Message "+e);
			}	
               try {
				responseOutputStream.close();
			} catch (IOException e) {
				logger.error("Error Message "+e);
			}
           }
        }
    }
}
