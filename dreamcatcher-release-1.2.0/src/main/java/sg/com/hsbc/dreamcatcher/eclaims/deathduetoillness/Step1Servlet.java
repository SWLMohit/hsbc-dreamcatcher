package sg.com.hsbc.dreamcatcher.eclaims.deathduetoillness;

import org.apache.commons.io.FileUtils;
import sg.com.hsbc.dreamcatcher.eclaims.EclaimStep1Processor;
import sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident.DeathDueToAccidentApplication;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/2-death-due-to-illness/1", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/2_death_due_to_illness/step1.jsp")
})
@MultipartConfig
public class Step1Servlet extends BaseServlet {
    private EclaimStep1Processor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new EclaimStep1Processor(this);
    }

    public float getCurrentStep() {
        return (float)1;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        boolean result = false;
        try {
            result = processor.process(request);
        }
        catch(Exception e) {
            processor.populateFormData(request);
        }
        return result;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void populateFormData(HttpServletRequest request) {
        processor.populateFormData(request);
    }

    protected float getStep() {
        return (float)1;
    }
}
