package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.NumberUtilities;
import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/11", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step11.jsp")
})
public class Step11Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 11;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        sanitized = sanitizeApplicantAddressPostalCode(request, sanitized);
        sanitized = sanitizeApplicantAddressHouse(request, sanitized);
        sanitized = sanitizeApplicantAddressUnit(request, sanitized);
        sanitized = sanitizeApplicantAddressStreet(request, sanitized);
        sanitized = sanitizeApplicantAddressBuilding(request, sanitized);

        if (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) {
            sanitized = sanitizeApplicantAddressRegistered(request, sanitized);
        }

        sanitized = sanitizeApplicantAddressPeriodStay(request, sanitized);

        if (sanitized.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY) != null && (int)sanitized.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY) < 60) {
            sanitized = sanitizeApplicantPreviousAddressCountry(request, sanitized);
        }

        sanitized = sanitizeApplicantAddressIsPermanent(request, sanitized);
        if (application.applicantIsPassHolder() ||
                (sanitized.get(FieldConstants.APPLICANT_ADDRESS_PERMANENT) != null && !((boolean)sanitized.get(FieldConstants.APPLICANT_ADDRESS_PERMANENT)))) {
            sanitized = sanitizeApplicantPermanentAddressLine1(request, sanitized);
            sanitized = sanitizeApplicantPermanentAddressLine2(request, sanitized);
            sanitized = sanitizeApplicantPermanentAddressPostalCode(request, sanitized);
            sanitized = sanitizeApplicantPermanentAddressCountry(request, sanitized);
        }

        sanitized = sanitizeApplicantTaxCountries(request, sanitized);
        sanitized = sanitizeApplicantAddressNoTaxReason(request, sanitized);

        sanitized = sanitizeApplicantIsHsbcCustomer(request, sanitized);
        sanitized = sanitizeApplicantIsPep(request, sanitized);
        sanitized = sanitizeApplicantIsBeneficialOwner(request, sanitized);

        if (sanitized.get(FieldConstants.APPLICANT_BO) != null && !((boolean)sanitized.get(FieldConstants.APPLICANT_BO))) {
            sanitized = sanitizeBeneficalOwnerFirstName(request, sanitized);
            sanitized = sanitizeBeneficalOwnerLastName(request, sanitized);
            sanitized = sanitizeBeneficalOwnerNric(request, sanitized);
            sanitized = sanitizeBeneficalOwnerRelation(request, sanitized);
        }

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        errors = validateApplicantAddressPostalCode(data, errors);
        errors = validateApplicantAddressHouse(data, errors);
        errors = validateApplicantAddressUnit(data, errors);
        errors = validateApplicantAddressStreet(data, errors);
        errors = validateApplicantAddressBuilding(data, errors);

        if (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) {
            errors = validateApplicantAddressRegistered(data, errors);
        }

        errors = validateApplicantAddressPeriodStay(data, errors);

        if (data.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY) != null && (int)data.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY) < 60) {
            errors = validateApplicantPreviousAddressCountry(data, errors);
        }

        if (application.applicantIsPermanentResident() || application.applicantIsPassHolder()) {
            if (application.applicantIsPermanentResident()) {
                errors = validateApplicantAddressIsPermanent(data, errors);
            }
            if (application.applicantIsPassHolder() ||
                    (data.get(FieldConstants.APPLICANT_ADDRESS_PERMANENT) != null && !((boolean)data.get(FieldConstants.APPLICANT_ADDRESS_PERMANENT)))) {
                errors = validateApplicantPermanentAddressLine1(data, errors);
                errors = validateApplicantPermanentAddressLine2(data, errors);
                errors = validateApplicantPermanentAddressPostalCode(data, errors);
                errors = validateApplicantPermanentAddressCountry(data, errors);
            }
        }

        errors = validateApplicantTaxCountries(data, errors);
       
        /* JIRA HSBCIDCU-608
         * Removed validation for applicant address no tax reason 
         * as these fields have been remove on frontend
         */
       // errors = validateApplicantAddressNoTaxReason(data, errors); 
      

        errors = validateApplicantIsHsbcCustomer(data, errors);
        errors = validateApplicantIsPep(data, errors);
        errors = validateApplicantIsBeneficialOwner(data, errors);

        if (data.get(FieldConstants.APPLICANT_BO) != null && !((boolean)data.get(FieldConstants.APPLICANT_BO))) {
            errors = validateBeneficalOwnerFirstName(data, errors);
            errors = validateBeneficalOwnerLastName(data, errors);
            errors = validateBeneficalOwnerNric(data, errors);
            errors = validateBeneficalOwnerRelation(data, errors);
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setApplicantAddressPostalCode(data.get(FieldConstants.APPLICANT_ADDRESS_POSTAL_CODE).toString());
        application.setApplicantAddressHouse(data.get(FieldConstants.APPLICANT_ADDRESS_HOUSE).toString());

        if (data.get(FieldConstants.APPLICANT_ADDRESS_UNIT) != null) {
            application.setApplicantAddressUnit(data.get(FieldConstants.APPLICANT_ADDRESS_UNIT).toString());
        }

        application.setApplicantAddressStreet(data.get(FieldConstants.APPLICANT_ADDRESS_STREET).toString());
        application.setApplicantAddressBuilding(data.get(FieldConstants.APPLICANT_ADDRESS_BUILDING).toString());
        application.setApplicantAddressIsRegistered((Boolean)data.get(FieldConstants.APPLICANT_ADDRESS_REGISTERED));

        int periodStayYears = Integer.parseInt(data.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_YEARS).toString());
        // We use NumberUtilities here as an empty value should be elegantly handled by the backend application
        // (bad UX to force typing a '0' in this particular field)
        int periodStayMonths = NumberUtilities.parseInt(data.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_MONTHS).toString(), 0);
        application.setApplicantAddressPeriodOfStay((periodStayYears * 12) + periodStayMonths);

        if (data.get(FieldConstants.APPLICANT_PREVIOUS_ADDRESS_COUNTRY) != null && !data.get(FieldConstants.APPLICANT_PREVIOUS_ADDRESS_COUNTRY).toString().isEmpty()) {
            application.setApplicantPreviousAddressCountry(data.get(FieldConstants.APPLICANT_PREVIOUS_ADDRESS_COUNTRY).toString());
        }

        if (data.get(FieldConstants.APPLICANT_ADDRESS_PERMANENT) != null && (boolean)data.get(FieldConstants.APPLICANT_ADDRESS_PERMANENT)) {
            application.setApplicantAddressIsPermanent(true);
            application.setApplicantPermanentAddressLine1(null);
            application.setApplicantPermanentAddressLine2(null);
            application.setApplicantPermanentAddressPostalCode(null);
            application.setApplicantPermanentAddressCountry(null);
        } else {
            application.setApplicantAddressIsPermanent(false);
            application.setApplicantPermanentAddressLine1(data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_1) != null ? data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_1).toString() : null);
            application.setApplicantPermanentAddressLine2(data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_2) != null ? data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_2).toString() : null);
            application.setApplicantPermanentAddressPostalCode(data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_POSTAL_CODE) != null ? data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_POSTAL_CODE).toString() : null);
            application.setApplicantPermanentAddressCountry(data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_COUNTRY) != null ? data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_COUNTRY).toString() : null);
        }

        if (data.get("applicant_tax_country1") != null && !data.get("applicant_tax_country1").toString().isEmpty()) {
            application.setApplicantTaxCountry1(data.get("applicant_tax_country1").toString());

            if (data.get("applicant_tax_country1_tin") != null) {
                application.setApplicantTaxCountry1Tin(data.get("applicant_tax_country1_tin").toString());
            } else {
                application.setApplicantTaxCountry1Tin(null);
            }

            if (data.get("applicant_tax_country1_tin_reason") != null) {
                application.setApplicantTaxCountry1NoTinReason(data.get("applicant_tax_country1_tin_reason").toString());
            } else {
                application.setApplicantTaxCountry1NoTinReason(null);
            }

            if (data.get("applicant_tax_country1_tin_reason_custom") != null) {
                application.setApplicantTaxCountry1NoTinReasonCustom(data.get("applicant_tax_country1_tin_reason_custom").toString());
            } else {
                application.setApplicantTaxCountry1NoTinReasonCustom(null);
            }
        } else {
            application.setApplicantTaxCountry1(null);
            application.setApplicantTaxCountry1Tin(null);
            application.setApplicantTaxCountry1NoTinReason(null);
            application.setApplicantTaxCountry1NoTinReasonCustom(null);
        }

        if (data.get("applicant_tax_country2") != null && !data.get("applicant_tax_country2").toString().isEmpty()) {
            application.setApplicantTaxCountry2(data.get("applicant_tax_country2").toString());

            if (data.get("applicant_tax_country2_tin") != null) {
                application.setApplicantTaxCountry2Tin(data.get("applicant_tax_country2_tin").toString());
            } else {
                application.setApplicantTaxCountry2Tin(null);
            }

            if (data.get("applicant_tax_country2_tin_reason") != null) {
                application.setApplicantTaxCountry2NoTinReason(data.get("applicant_tax_country2_tin_reason").toString());
            } else {
                application.setApplicantTaxCountry2NoTinReason(null);
            }

            if (data.get("applicant_tax_country2_tin_reason_custom") != null) {
                application.setApplicantTaxCountry2NoTinReasonCustom(data.get("applicant_tax_country2_tin_reason_custom").toString());
            } else {
                application.setApplicantTaxCountry2NoTinReasonCustom(null);
            }
        }
        else {
            application.setApplicantTaxCountry2(null);
            application.setApplicantTaxCountry2Tin(null);
            application.setApplicantTaxCountry2NoTinReason(null);
            application.setApplicantTaxCountry2NoTinReasonCustom(null);
        }

        if (data.get("applicant_tax_country3") != null && !data.get("applicant_tax_country3").toString().isEmpty()) {
            application.setApplicantTaxCountry3(data.get("applicant_tax_country3").toString());

            if (data.get("applicant_tax_country3_tin") != null) {
                application.setApplicantTaxCountry3Tin(data.get("applicant_tax_country3_tin").toString());
            } else {
                application.setApplicantTaxCountry3Tin(null);
            }

            if (data.get("applicant_tax_country3_tin_reason") != null) {
                application.setApplicantTaxCountry3NoTinReason(data.get("applicant_tax_country3_tin_reason").toString());
            } else {
                application.setApplicantTaxCountry3NoTinReason(null);
            }

            if (data.get("applicant_tax_country3_tin_reason_custom") != null) {
                application.setApplicantTaxCountry3NoTinReasonCustom(data.get("applicant_tax_country3_tin_reason_custom").toString());
            } else {
                application.setApplicantTaxCountry3NoTinReasonCustom(null);
            }
        } else {
            application.setApplicantTaxCountry3(null);
            application.setApplicantTaxCountry3Tin(null);
            application.setApplicantTaxCountry3NoTinReason(null);
            application.setApplicantTaxCountry3NoTinReasonCustom(null);
        }

        if (data.get(FieldConstants.APPLICANT_ADDRESS_NO_TAX_REASON) != null) {
            application.setApplicantAddressNoTaxReason(data.get(FieldConstants.APPLICANT_ADDRESS_NO_TAX_REASON).toString());
        }

        application.setApplicantIsHsbcCustomer((Boolean)data.get(FieldConstants.APPLICANT_HSBC_CUSTOMER));
        application.setApplicantIsPep((Boolean)data.get(FieldConstants.APPLICANT_PEP));
        application.setApplicantIsBeneficalOwner((Boolean)data.get(FieldConstants.APPLICANT_BO));

        if (data.get(FieldConstants.BO_FIRST_NAME) != null) {
            application.setBeneficialOwnerFirstName(data.get(FieldConstants.BO_FIRST_NAME).toString());
        }

        if (data.get(FieldConstants.BO_LAST_NAME) != null) {
            application.setBeneficialOwnerLastName(data.get(FieldConstants.BO_LAST_NAME).toString());
        }

        if (data.get(FieldConstants.BO_NRIC) != null) {
            application.setBeneficialOwnerNric(data.get(FieldConstants.BO_NRIC).toString());
        }

        if (data.get(FieldConstants.BO_RELATION) != null) {
            application.setBeneficialOwnerRelation(data.get(FieldConstants.BO_RELATION).toString());
        }

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateApplicantAddressPostalCode(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_ADDRESS_POSTAL_CODE))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_ADDRESS_POSTAL_CODE, "Postal code is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressHouse(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_ADDRESS_HOUSE))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_ADDRESS_HOUSE, "House/Block number is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressUnit(Map<String, Object> data, Map<String, String> errors) {
        if (data.get(FieldConstants.APPLICANT_ADDRESS_UNIT) != null) {
            Matcher matcher = Pattern.compile("^.+-.+$").matcher(data.get(FieldConstants.APPLICANT_ADDRESS_UNIT).toString());
            if (!matcher.matches()) {
                errors.put(FieldConstants.APPLICANT_ADDRESS_UNIT, "Unit number is invalid.");
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressStreet(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_ADDRESS_STREET))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_ADDRESS_STREET, "Street is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressBuilding(Map<String, Object> data, Map<String, String> errors) {
        return errors;
    }

    private Map<String, String> validateApplicantAddressRegistered(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_ADDRESS_REGISTERED))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_ADDRESS_REGISTERED, "Please indicate your selection");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressPeriodStay(Map<String, Object> data, Map<String, String> errors) {
        if (data.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_YEARS).toString().isEmpty() && data.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_MONTHS).toString().isEmpty()) {
            errors.put(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY, "Period of stay is missing.");
        } else {
            Matcher matcher1 = Pattern.compile("^[0-9]{0,3}$").matcher(data.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_YEARS).toString());
            Matcher matcher2 = Pattern.compile("^[0-9]{0,3}$").matcher(data.get(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_MONTHS).toString());
            if (!matcher1.matches() || !matcher2.matches()) {
                errors.put(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY, "Period of stay is invalid.");
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantPreviousAddressCountry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_PREVIOUS_ADDRESS_COUNTRY))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_PREVIOUS_ADDRESS_COUNTRY, "Country of address is missing.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressIsPermanent(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_ADDRESS_PERMANENT))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_ADDRESS_PERMANENT, "Please indicate your selection.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantPermanentAddressLine1(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_1))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_1, "Address is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantPermanentAddressLine2(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_2))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_2, "Address is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantPermanentAddressPostalCode(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_POSTAL_CODE))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_POSTAL_CODE, "Postal code is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantPermanentAddressCountry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_PERMANENT_ADDRESS_COUNTRY))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_COUNTRY, "Country is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantTaxCountries(Map<String, Object> data, Map<String, String> errors) {
        for (int i = 1; i <= 3; i++) {
            String keyPrefix = FieldConstants.APPLICANT_TAX_COUNTRY_PREFIX + i;
            if(i==1) {
            	if(data.get(keyPrefix).toString().isEmpty()) {
            		errors.put(keyPrefix + "_mandatory", "Please select a country.");
            		//return errors;
            	}
            	if (data.get(keyPrefix + "_tin_reason") == null) {
                     switch (Validator.validateTinNumber(data.get(keyPrefix + "_tin"))) {
                         case Validator.ERROR_STRING_EMPTY:
                             errors.put(keyPrefix + "_tin", "Taxpayer Identification Number is missing.");
                             break;
                         case Validator.ERROR_STRING_TOO_LONG:
                             errors.put(keyPrefix + "_tin", "Taxpayer Identification Number too long.");
                             break;
                         case Validator.ERROR_TIN_FORMAT_INVALID:
                             errors.put(keyPrefix + "_tin", "Taxpayer Identification Number is invalid.");
                             break;
                     }                 
            	 }
            	
            }
            if (!data.get(keyPrefix).toString().isEmpty()) {
                if (data.get(keyPrefix + "_tin_reason") == null) {
                    switch (Validator.validateTinNumber(data.get(keyPrefix + "_tin"))) {
                        case Validator.ERROR_STRING_EMPTY:
                            errors.put(keyPrefix + "_tin", "Taxpayer Identification Number is missing.");
                            break;
                        case Validator.ERROR_STRING_TOO_LONG:
                            errors.put(keyPrefix + "_tin", "Taxpayer Identification Number too long.");
                            break;
                        case Validator.ERROR_TIN_FORMAT_INVALID:
                            errors.put(keyPrefix + "_tin", "Taxpayer Identification Number is invalid.");
                            break;
                    }
                } else {
                    if (data.get(keyPrefix + "_tin_reason").toString().equalsIgnoreCase("B")) {
                        if (data.get(keyPrefix + "_tin_reason_custom") == null || data.get(keyPrefix + "_tin_reason_custom").toString().isEmpty()) {
                            errors.put(keyPrefix + "_tin_reason_custom", "Please provide an explanation.");
                        }
                    }
                }
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressNoTaxReason(Map<String, Object> data, Map<String, String> errors) {
        boolean residentialAddressIsTaxCountry = false;
        for (int i = 1; i <= 3; i++) {
            String keyPrefix = FieldConstants.APPLICANT_TAX_COUNTRY_PREFIX + i;
            if (!data.get(keyPrefix).toString().isEmpty() && data.get(keyPrefix).toString().equalsIgnoreCase("Singapore")) {
                residentialAddressIsTaxCountry = true;
            }
        }

        if (!residentialAddressIsTaxCountry) {
            switch (Validator.validateString(data.get(FieldConstants.APPLICANT_ADDRESS_NO_TAX_REASON))) {
                case Validator.ERROR_STRING_EMPTY:
                    errors.put(FieldConstants.APPLICANT_ADDRESS_NO_TAX_REASON, "Reason is missing or invalid.");
                    break;
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantIsHsbcCustomer(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_HSBC_CUSTOMER))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_HSBC_CUSTOMER, "Please indicate your selection.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantIsPep(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_PEP))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_PEP, "Please indicate your selection.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantIsBeneficialOwner(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_BO))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_BO, "Please indicate your selection.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateBeneficalOwnerFirstName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.BO_FIRST_NAME))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.BO_FIRST_NAME, "Given name is missing or invalid.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.BO_FIRST_NAME, "Given name is too long.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.BO_FIRST_NAME, "Given name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateBeneficalOwnerLastName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.BO_LAST_NAME))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.BO_LAST_NAME, "Surname is missing or invalid.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.BO_LAST_NAME, "Surname is too long.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.BO_FIRST_NAME, "Given name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateBeneficalOwnerNric(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.BO_NRIC))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.BO_NRIC, "NRIC/Passport number is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateBeneficalOwnerRelation(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.BO_RELATION))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.BO_RELATION, "Relationship is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantAddressPostalCode(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_ADDRESS_POSTAL_CODE);
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_POSTAL_CODE, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressHouse(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_ADDRESS_HOUSE);
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_HOUSE, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressUnit(HttpServletRequest request, Map<String, Object> sanitized) {
        String unitFloor = request.getParameter(FieldConstants.APPLICANT_ADDRESS_UNIT_FLOOR);
        String unitNum = request.getParameter(FieldConstants.APPLICANT_ADDRESS_UNIT_NUM);

        sanitized.put(FieldConstants.APPLICANT_ADDRESS_UNIT_FLOOR, unitFloor);
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_UNIT_NUM, unitNum);

        if (!unitFloor.isEmpty() || !unitNum.isEmpty()) {
            sanitized.put(FieldConstants.APPLICANT_ADDRESS_UNIT, String.format("%s-%s", unitFloor, unitNum));
        } else {
            sanitized.put(FieldConstants.APPLICANT_ADDRESS_UNIT, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressStreet(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_ADDRESS_STREET);
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_STREET, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressBuilding(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_ADDRESS_BUILDING);
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_BUILDING, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressRegistered(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_ADDRESS_REGISTERED);
        if (input != null) {
            sanitized.put(FieldConstants.APPLICANT_ADDRESS_REGISTERED, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.APPLICANT_ADDRESS_REGISTERED, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressPeriodStay(HttpServletRequest request, Map<String, Object> sanitized) {
        String years = request.getParameter(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_YEARS);
        String months = request.getParameter(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_MONTHS);

        sanitized.put(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_YEARS, years);
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_MONTHS, months);

        int periodStayYears = 0;
        int periodStayMonths = 0;
        if (!years.isEmpty()) {
            periodStayYears = Integer.parseInt(years);
        }
        if (!months.isEmpty()) {
            periodStayMonths = Integer.parseInt(months);
        }
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY, (periodStayYears * 12 + periodStayMonths));

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPreviousAddressCountry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_PREVIOUS_ADDRESS_COUNTRY);
        sanitized.put(FieldConstants.APPLICANT_PREVIOUS_ADDRESS_COUNTRY, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressIsPermanent(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_ADDRESS_PERMANENT);
        if (input != null) {
            sanitized.put(FieldConstants.APPLICANT_ADDRESS_PERMANENT, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.APPLICANT_ADDRESS_PERMANENT, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPermanentAddressLine1(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_1);
        sanitized.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_1, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPermanentAddressLine2(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_2);
        sanitized.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_2, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPermanentAddressPostalCode(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_PERMANENT_ADDRESS_POSTAL_CODE);
        sanitized.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_POSTAL_CODE, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPermanentAddressCountry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_PERMANENT_ADDRESS_COUNTRY);
        sanitized.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_COUNTRY, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantTaxCountries(HttpServletRequest request, Map<String, Object> sanitized) {
        String key;
        String country;
        for (int i = 1; i <= 3; i++) {
            key = FieldConstants.APPLICANT_TAX_COUNTRY_PREFIX + i;
            sanitized.put(key, request.getParameter(key));
           // if (!"Singapore".equals(request.getParameter(key))) {
                sanitized.put(key + "_tin", request.getParameter(key + "_tin"));
                sanitized.put(key + "_tin_reason", request.getParameter(key + "_tin_reason"));
                sanitized.put(key + "_tin_reason_custom", request.getParameter(key + "_tin_reason_custom"));
           // }
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressNoTaxReason(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_ADDRESS_NO_TAX_REASON);
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_NO_TAX_REASON, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIsHsbcCustomer(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_HSBC_CUSTOMER);
        if (input != null) {
            sanitized.put(FieldConstants.APPLICANT_HSBC_CUSTOMER, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.APPLICANT_HSBC_CUSTOMER, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIsPep(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_PEP);
        if (input != null) {
            sanitized.put(FieldConstants.APPLICANT_PEP, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.APPLICANT_PEP, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIsBeneficialOwner(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_BO);
        if (input != null) {
            sanitized.put(FieldConstants.APPLICANT_BO, input.equals("Y"));
        } else {
            sanitized.put(FieldConstants.APPLICANT_BO, null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeBeneficalOwnerFirstName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.BO_FIRST_NAME);
        sanitized.put(FieldConstants.BO_FIRST_NAME, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeBeneficalOwnerLastName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.BO_LAST_NAME);
        sanitized.put(FieldConstants.BO_LAST_NAME, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeBeneficalOwnerNric(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.BO_NRIC);
        sanitized.put(FieldConstants.BO_NRIC, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeBeneficalOwnerRelation(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.BO_RELATION);
        sanitized.put(FieldConstants.BO_RELATION, input);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put(FieldConstants.APPLICANT_ADDRESS_POSTAL_CODE, application.getApplicantAddressPostalCode());
        data.put(FieldConstants.APPLICANT_ADDRESS_HOUSE, application.getApplicantAddressHouse());

        if (application.getApplicantAddressUnit() != null && !application.getApplicantAddressUnit().isEmpty()) {
            String[] parts = application.getApplicantAddressUnit().split("-");
            data.put(FieldConstants.APPLICANT_ADDRESS_UNIT, application.getApplicantAddressUnit());
            data.put(FieldConstants.APPLICANT_ADDRESS_UNIT_FLOOR, parts[0]);
            data.put(FieldConstants.APPLICANT_ADDRESS_UNIT_NUM, parts[1]);
        }

        data.put(FieldConstants.APPLICANT_ADDRESS_STREET, application.getApplicantAddressStreet());
        data.put(FieldConstants.APPLICANT_ADDRESS_BUILDING, application.getApplicantAddressBuilding());
        data.put(FieldConstants.APPLICANT_ADDRESS_REGISTERED, application.getApplicantAddressIsRegistered());

        Integer addressPeriodOfStay = application.getApplicantAddressPeriodOfStay();
        if (addressPeriodOfStay != null) {
            data.put(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_YEARS, (int)Math.floor(addressPeriodOfStay / 12));
            data.put(FieldConstants.APPLICANT_ADDRESS_PERIOD_STAY_MONTHS, addressPeriodOfStay % 12);
        }

        data.put(FieldConstants.APPLICANT_PREVIOUS_ADDRESS_COUNTRY, application.getApplicantPreviousAddressCountry());
        data.put(FieldConstants.APPLICANT_ADDRESS_PERMANENT, application.getApplicantAddressIsPermanent());
        data.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_1, application.getApplicantPermanentAddressLine1());
        data.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_LINE_2, application.getApplicantPermanentAddressLine2());
        data.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_POSTAL_CODE, application.getApplicantPermanentAddressPostalCode());
        data.put(FieldConstants.APPLICANT_PERMANENT_ADDRESS_COUNTRY, application.getApplicantPermanentAddressCountry());

        data.put("applicant_tax_country1", application.getApplicantTaxCountry1());
        data.put("applicant_tax_country1_tin", application.getApplicantTaxCountry1Tin());
        data.put("applicant_tax_country1_tin_reason", application.getApplicantTaxCountry1NoTinReason());
        data.put("applicant_tax_country1_tin_reason_custom", application.getApplicantTaxCountry1NoTinReasonCustom());
        data.put("applicant_tax_country2", application.getApplicantTaxCountry2());
        data.put("applicant_tax_country2_tin", application.getApplicantTaxCountry2Tin());
        data.put("applicant_tax_country2_tin_reason", application.getApplicantTaxCountry2NoTinReason());
        data.put("applicant_tax_country2_tin_reason_custom", application.getApplicantTaxCountry2NoTinReasonCustom());
        data.put("applicant_tax_country3", application.getApplicantTaxCountry3());
        data.put("applicant_tax_country3_tin", application.getApplicantTaxCountry3Tin());
        data.put("applicant_tax_country3_tin_reason", application.getApplicantTaxCountry3NoTinReason());
        data.put("applicant_tax_country3_tin_reason_custom", application.getApplicantTaxCountry3NoTinReasonCustom());

        data.put(FieldConstants.APPLICANT_ADDRESS_NO_TAX_REASON, application.getApplicantAddressNoTaxReason());
        data.put(FieldConstants.APPLICANT_HSBC_CUSTOMER, application.getApplicantIsHsbcCustomer());
        data.put(FieldConstants.APPLICANT_PEP, application.getApplicantIsPep());
        data.put(FieldConstants.APPLICANT_BO, application.getApplicantIsBeneficalOwner());

        data.put(FieldConstants.BO_FIRST_NAME, application.getBeneficialOwnerFirstName());
        data.put(FieldConstants.BO_LAST_NAME, application.getBeneficialOwnerLastName());
        data.put(FieldConstants.BO_NRIC, application.getBeneficialOwnerNric());
        data.put(FieldConstants.BO_RELATION, application.getBeneficialOwnerRelation());

        request.setAttribute("data", data);
    }

}
