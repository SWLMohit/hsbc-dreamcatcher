package sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident;

import be.quodlibet.boxable.*;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;
import sg.com.hsbc.dreamcatcher.eclaims.*;
import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.mail.MessagingException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;

import be.quodlibet.boxable.BaseTable;
import sg.com.hsbc.dreamcatcher.eclaims.DeathApplication;
import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

public class DeathDueToAccidentApplication extends DeathApplication {
    private final static Logger logger = Logger.getLogger(DeathDueToAccidentApplication.class);

    protected Date accidentDate;
    protected String accidentPlace;
    protected String accidentDetails;
    protected List<String> deathCertificate; //HSBCIDCU-798
    protected List<String> policeInvestigation; //HSBCIDCU-798
    protected Boolean postMortem;
    protected List<String> postMortemDocument; //HSBCIDCU-798
    protected Boolean suicide;
    protected Boolean leftAWill;
    protected List<String> leftAWillDocument; //HSBCIDCU-798
    protected Boolean occurInSingapore;
    protected List<String> occurInSingaporeDocument; //HSBCIDCU-798

    public DeathDueToAccidentApplication() {
        claimsType = "D";
        claimsTitle = "Death Due to Accident/Unnatural Causes";
    }

    public static DeathDueToAccidentApplication getById(String id) throws SQLException {
        DeathDueToAccidentApplication application = new DeathDueToAccidentApplication();
        application.load(id);

        return application;
    }

    /**
     * Start generate the pdf document
     */
    @Override
    public void generatePages(PDFGenerator pd) {
        float yStart;
        try{
            yStart = generatePage1(pd);
            yStart = generatePage2(pd,yStart);
            writeFileList(pd, yStart);
            for(int i=1;i<pd.getDocument().getNumberOfPages();i++){
                pd.applyPageHeader(pd.getDocument().getPage(i),pd.getDocument());
            }
        }catch(Exception e){
            logger.error("error in pdf creation "+e);
        }
    }

    @Override
    public float writeClaimInformation(PDFGenerator pd, float yStart) throws IOException {
        float tableWidth = getTableWidth(pd);
        PDPage page;
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        List<String[]> rows = new ArrayList<>();

        pd.addTableHeader(table, "Claim information");


        List<String> deathCertificateDocs=getDeathCertificate();
        if (deathCertificateDocs != null) {
          Collections.sort(deathCertificateDocs);
          for (int i = 0; i < deathCertificateDocs.size(); i++) {
            String doc = deathCertificateDocs.get(i);
    
            if (!uploadedDocuments.contains(doc)) {
              uploadedDocuments.add(doc);
            }
          }
        }

        rows.add(new String[]{"Date of Accident", formatDate(getAccidentDate())});
        rows.add(new String[]{"Place of accident", getAccidentPlace()});
        rows.add(new String[]{"Details of Accident", getAccidentDetails()});
        rows.add(new String[]{"Please upload a copy of the Death Certificate", "Number of documents uploaded - "+deathCertificateDocs.size()});
        
        if(!getPoliceInvestigation().isEmpty()){
       
        List<String> policeInvestigationDocs = getPoliceInvestigation();
        rows.add(new String[]{"Please upload a copy of the Police Investigation report", "Number of documents uploaded - "+policeInvestigationDocs.size()});
        if (policeInvestigationDocs != null) {
          Collections.sort(policeInvestigationDocs);
          for (int i = 0; i < policeInvestigationDocs.size(); i++) {
            String doc = policeInvestigationDocs.get(i);
            if (!uploadedDocuments.contains(doc)) {
              uploadedDocuments.add(doc);
            }
          }

        }
        }
        /*Removed special characters not understood by pdfbox*/
        rows.add(new String[]{"Was a postmortem, autopsy done or Coroner's inquest held?", getPostMortem() ? "Yes" : "No"});


        if(getPostMortem()){

          List<String> postMortemDocuments=getPostMortemDocument();
          rows.add(new String[]{"Please upload a copy of the Post-Mortem or Toxicology report or Coroner's Inquest report","Number of documents uploaded - "+((postMortemDocuments==null||postMortemDocuments.isEmpty())?0:postMortemDocuments.size()) });
        
          
          if (postMortemDocuments != null) {
            Collections.sort(postMortemDocuments);
            for (int i = 0; i < postMortemDocuments.size(); i++) {
              String doc = postMortemDocuments.get(i);
              if (!uploadedDocuments.contains(doc)) {
                uploadedDocuments.add(doc);
              }
            }
          }

        }

        rows.add(new String[]{"Was the death due to suicide?", getSuicide() ? "Yes" : "No"});
        rows.add(new String[]{"Did deceased leave a Will?", getLeftAWill() ? "Yes" : "No"});


        if(getLeftAWill()){

          List<String> leftAWillDocuments=getLeftAWillDocument();
          rows.add(new String[]{"Please upload a copy of the Will","Number of documents uploaded - "+ ((leftAWillDocuments==null||leftAWillDocuments.isEmpty())?0:leftAWillDocuments.size())});
          if (leftAWillDocuments != null) {
            Collections.sort(leftAWillDocuments);
            for (int i = 0; i < leftAWillDocuments.size(); i++) {
              String doc = leftAWillDocuments.get(i);
              if (!uploadedDocuments.contains(doc)) {
                uploadedDocuments.add(doc);
              }
            }
          }

        }

        
        List<String> occurInSingaporeDocuments=getOccurInSingaporeDocument();

        rows.add(new String[]{"Did the Death occur in Singapore?", getOccurInSingapore() ? "Yes" : "No"});
        if (!getOccurInSingapore()&& occurInSingaporeDocuments != null) {
        	
        	rows.add(new String[]{"Please upload a copy of the letter from ICA confirming receipt of the Singapore IC, Passport and overseas Death Certificate",
            		"Number of documents uploaded - "+((occurInSingaporeDocuments==null||occurInSingaporeDocuments.isEmpty())?0:occurInSingaporeDocuments.size())});
            Collections.sort(occurInSingaporeDocuments);
            for (int i = 0; i < occurInSingaporeDocuments.size(); i++) {
              String doc = occurInSingaporeDocuments.get(i);
              if (!uploadedDocuments.contains(doc)) {
                uploadedDocuments.add(doc);
              }
            }
          }
    try {
      for (String[] row : rows) {
        pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
      }
    } catch (Exception e) {
      logger.error("Exception occurred in writeClaimInformation "+e);
    }

       
    return table.draw() - 10;
    }

    

   
    public String getAccidentDetails() {
        return accidentDetails;
    }

    public void setAccidentDetails(String accidentDetails) {
        this.accidentDetails = accidentDetails;
    }

    public Date getAccidentDate() {
        return accidentDate;
    }

    public void setAccidentDate(Date accidentDate) {
        this.accidentDate = accidentDate;
    }

    public String getAccidentPlace() {
        return accidentPlace;
    }

    public void setAccidentPlace(String accidentPlace) {
        this.accidentPlace = accidentPlace;
    }

    public List<String> getDeathCertificate() {
        return deathCertificate;
    }

    public void setDeathCertificate(List<String> list) {
        this.deathCertificate = list;
    }

    public List<String> getPoliceInvestigation() {
        return policeInvestigation;
    }

    public void setPoliceInvestigation(List<String> list) {
        this.policeInvestigation = list;
    }

    public Boolean getPostMortem() {
        return postMortem;
    }

    public void setPostMortem(Boolean postMortem) {
        this.postMortem = postMortem;
    }

    public Boolean getSuicide() {
        return suicide;
    }

    public void setSuicide(Boolean suicide) {
        this.suicide = suicide;
    }

    public Boolean getLeftAWill() {
        return leftAWill;
    }

    public void setLeftAWill(Boolean leftAWill) {
        this.leftAWill = leftAWill;
    }

    public Boolean getOccurInSingapore() {
        return occurInSingapore;
    }

    public void setOccurInSingapore(Boolean occurInSingapore) {
        this.occurInSingapore = occurInSingapore;
    }

    public List<String> getLeftAWillDocument() {
        return leftAWillDocument;
    }


    public void setLeftAWillDocument(List<String> list) {
        this.leftAWillDocument = list;

    }

    public List<String> getPostMortemDocument() {
        return postMortemDocument;
    }

    public void setPostMortemDocument(List<String> list) {
        this.postMortemDocument = list;
    }

    public List<String> getOccurInSingaporeDocument() {
        return occurInSingaporeDocument;
    }

    public void setOccurInSingaporeDocument(List<String> list) {
        this.occurInSingaporeDocument = list;
    }

    public void sendEmail(){
        try {
      super.sendEmail(getPolicyownerEmail(), EMAIL_TEMPLATE_DEATH, getPDFFilename(false));
    } catch (MessagingException e) {
      logger.error("error in sending email "+e);
    }

    }
}
