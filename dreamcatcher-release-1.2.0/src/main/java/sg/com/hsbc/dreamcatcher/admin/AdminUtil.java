package sg.com.hsbc.dreamcatcher.admin;


import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendHistory;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendStatus;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendType;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminUtil {
    private static Logger logger = LoggerFactory.getLogger(AdminUtil.class);
    /**
     * This is the main validation function for the retrieval servlet. It will call individual function to validateEmailAndVerificationCode each field.
     * @param request
     * @return errors
     */
    public Map<String, String> validateEmailAndVerificationCode(HttpServletRequest request) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();

        String inputEmail = request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL);
        data.put(FieldConstants.RETRIEVAL_FORM_EMAIL, inputEmail);

        String inputVerifyCode = request.getParameter(FieldConstants.RETRIEVAL_VERIFY_CODE);
        data.put(FieldConstants.RETRIEVAL_VERIFY_CODE, inputVerifyCode);

        logger.info("validateEmailAndVerificationCode");
        errors = validateApplicantEmail(data, errors);
        errors = validateApplicantVerifyCode(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return errors;
    }

    /**
     * This is the main validation function for the retrieval servlet. It will call individual function to validateEmailAndVerificationCode each field.
     * @param request
     * @return errors
     */
    public Map<String, String> validateEmailAndMobileNo(HttpServletRequest request) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();

        String inputEmail = request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL);
        data.put(FieldConstants.RETRIEVAL_FORM_EMAIL, inputEmail);

        String inputMobileCountryCode = request.getParameter(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE);
        data.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, inputMobileCountryCode);

        String inputMobileNum = request.getParameter(FieldConstants.APPLICANT_MOBILE_NUM);
        data.put(FieldConstants.APPLICANT_MOBILE_NUM, inputMobileNum);

        logger.info("validateEmailAndMobileNo");
        errors = validateApplicantEmail(data, errors);
        errors = validateApplicantCountryCode(data, errors);
        errors = validateApplicantMobileNum(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return errors;
    }

    /**
     * This function validates the email field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantEmail(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.RETRIEVAL_FORM_EMAIL))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.RETRIEVAL_FORM_EMAIL, "Your email address is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.RETRIEVAL_FORM_EMAIL, "Your email address is too long.");
                break;
        }

        switch (Validator.validateEmail(data.get(FieldConstants.RETRIEVAL_FORM_EMAIL))) {
            case Validator.ERROR_EMAIL_FORMAT_INVALID:
                errors.put(FieldConstants.RETRIEVAL_FORM_EMAIL, "Your email address is invalid.");
                break;
        }

        return errors;
    }

    /**
     * This function validates the verification code field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantVerifyCode(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.RETRIEVAL_VERIFY_CODE))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.RETRIEVAL_VERIFY_CODE, "Your verification code is missing.");
                break;
        }
        return errors;
    }

    /**
     * This function validates the Mobile Nun field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantMobileNum(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_MOBILE_NUM))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_MOBILE_NUM, "Your mobile number is missing.");
                break;
        }
        return errors;
    }

    public static void sendSMSService(Applications applications, String content, String code) {
        AmazonSNS sns = AmazonSNSClient.builder().withRegion(Regions.AP_SOUTHEAST_1).build();
        String message = String.format(content, code);
        String phoneNumber = applications.getApplicantFullMobileNum();

        Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
        smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                .withStringValue("HSBCInsure") //The sender ID shown on the device.
                .withDataType("String"));
        sns.publish(new PublishRequest()
                .withMessage(message)
                .withPhoneNumber(phoneNumber)
                .withMessageAttributes(smsAttributes));
    }

    /**
     * convert the application list to String
     * it uses to transfer the data to next page
     * @param applicationsList
     * @return
     */
    public static String convertListToString(List<Applications> applicationsList) {
        logger.info("convertListToString");
        if (applicationsList == null) {
            return null;
        }
        StringBuilder policyList = new StringBuilder();
        boolean delimiter = false;
        for(Applications app: applicationsList) {
            if (delimiter) {
                policyList.append("&&");
            }
            policyList.append(app.getId()).append("==").append(app.getInsuredFirstName()==null?app.getApplicantFirstName():app.getInsuredFirstName())
                    .append(" ").append(app.getInsuredLastName()==null?app.getApplicantLastName():app.getInsuredLastName());
            delimiter = true;
        }
        return policyList.toString();
    }

    /**
     * Set the data to resendHistory entity to save into DB
     * @param applications
     * @param type
     * @param status
     * @param sendTime
     * @return
     */
    public static ResendHistory convertApplicationsToResendHistory (Applications applications, ResendType type, ResendStatus status, Date sendTime) {
        logger.info("convertApplicationsToResendHistory");
        ResendHistory resendHistory = new ResendHistory();

        resendHistory.setApplicationId(applications.getId());
        resendHistory.setEmail(applications.getApplicantEmail());
        resendHistory.setCountryCode(applications.getApplicantMobileCountryCode());
        resendHistory.setMobileNo(applications.getApplicantMobileNum());
        resendHistory.setType(type.name());
        resendHistory.setStatus(status.name());
        resendHistory.setDateCreated(new Date());
        resendHistory.setSendTime(sendTime);

        return resendHistory;
    }

    public static Map validateEmailAndMobileNotMatch(boolean emailExist, boolean mobileExist) {
        Map<String, String> errors = new HashMap<>();
        if (emailExist) {
            if (mobileExist) {
                errors.put("form_not_found", "form_not_found");
            } else {
                errors.put(FieldConstants.APPLICANT_MOBILE_NUM, "The mobile number is either invalid or does not match our records.");
            }
        } else {
            if (mobileExist) {
                errors.put(FieldConstants.RETRIEVAL_FORM_EMAIL, "The email address is either invalid or does not match our records.");
            } else {
                errors.put(FieldConstants.RETRIEVAL_FORM_EMAIL, "The email address is either invalid or does not match our records.");
                errors.put(FieldConstants.APPLICANT_MOBILE_NUM, "The mobile number is either invalid or does not match our records.");
            }
        }
        return errors;
    }

    /**
     * This function validates the country code field.
     * @param data
     * @param errors
     * @return errors
     */
    public static Map<String, String> validateApplicantCountryCode(Map<String, Object> data, Map<String, String> errors) {

        switch (Validator.validatePhoneCountryCode(data.get(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE))) {
            case Validator.ERROR_PHONE_COUNTRY_CODE_INVALID:
                errors.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, "Your country code is invalid.");
                break;
                
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, "Your country code is missing.");
                break;
        }

        return errors;
    }
}
