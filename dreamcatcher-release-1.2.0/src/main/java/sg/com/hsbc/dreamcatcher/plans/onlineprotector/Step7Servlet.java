package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.AgeUtils;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hchellani
 * Servlet to validate the fields submitted in the 
 * step 7 form of online protector application
 */
@WebServlet(urlPatterns = "/our-plans/online-protector/7", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step7.jsp")
})
public class Step7Servlet extends BaseServlet {
	private final static Logger logger = Logger.getLogger(Step7Servlet.class);
    public float getCurrentStep() {
        return 7;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeSumAssured(request, sanitized);
        sanitized = sanitizeRiderSumAssured(request, sanitized);
        sanitized = sanitizePaymentMode(request, sanitized);
        sanitized = sanitizeIncludeCriticalIllnessRider(request, sanitized);
        sanitized = sanitizeIncludeRefundRider(request, sanitized);
        sanitized = sanitizeInsuredMedical4(request, sanitized);
        sanitized=sanitizeHiddenFields(request, sanitized);
        sanitized.put("max_sum_assured", application.getMaxSumAssured());
        sanitized.put("max_rider_sum_assured", application.getMaxRiderSumAssured());

        return sanitized;
    }
    
    /**
     * Accepts the hidden fields saved in the form and puts them in the data map.
     * This helps in preventing the additional backend call on post of form
     * @param request     
     * @param sanitized
     * @return
     */
    private Map<String, Object> sanitizeHiddenFields(HttpServletRequest request, Map<String, Object> sanitized) {
		// TODO Auto-generated method stub
    	 String modalFactor = request.getParameter("modalFactor");
    	 String underwritingclassfactor = request.getParameter("underwritingclassfactor");
    	 String ciadjustedpremium = request.getParameter("ciadjustedpremium");
    	 String premiumrate = request.getParameter("premiumrate");
    	 String premiumrateunitamount = request.getParameter("premiumrateunitamount");
    	 String adjustedpremium = request.getParameter("adjustedpremium");
    	 String basic = request.getParameter("basic");
    	 String actualDiscount=request.getParameter("actualdiscount");
    	 sanitized.put("modalFactor",modalFactor);
    	 sanitized.put("underwritingClassFactor",underwritingclassfactor);
    	 sanitized.put("ciAdjustedPremium",ciadjustedpremium);
    	 sanitized.put("premiumRate",premiumrate);
    	 sanitized.put("premiumRateUnitAmount",premiumrateunitamount);
    	 sanitized.put("adjustedPremium",adjustedpremium);
    	 
    	 if (StringUtils.contains(basic, "S$")) {
 			sanitized.put("summary_modal", basic);
 		} else {
 			sanitized.put("summary_modal", "S$" + basic);
 		}
    	 sanitized.put("actualDiscount",actualDiscount);
    	 
    	 return sanitized;
    	 
	}

	public boolean validate(HttpServletRequest request) {
    	
        Map<String, Object> data = sanitize(request);
        
        Map<String, String> errors = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);
        
        data.put("summary_total_modal", 0);
        data.put("summary_ci_modal", 0);
        data.put("summary_rop_modal", 0);
        
        errors = validateSumAssured(data, errors);
        errors = validatePaymentMode(data, errors);
        
        if (data.get("include_ci_rider") != null && (boolean)data.get("include_ci_rider")) {
            errors = validateRiderSumAssured(data, errors);
            errors = validateInsuredMedical4(data, errors);
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        try {
            application.setSumAssured((float)data.get("sum_assured"));
            if (data.get("include_ci_rider") != null && (boolean)data.get("include_ci_rider")) {
                application.setRiderSumAssured((float)data.get("rider_sum_assured"));
            } else {
                application.setRiderSumAssured(null);
            }
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
            return false;
        }

        if (data.get("payment_mode") != null) {
            application.setPaymentMode(Integer.parseInt(data.get("payment_mode").toString()));
        } else {
            application.setIncludeCriticalIllnessRider(false);
        }

        if (data.get("include_ci_rider") != null) {
            application.setIncludeCriticalIllnessRider((boolean)data.get("include_ci_rider"));
        } else {
            application.setIncludeCriticalIllnessRider(false);
        }

        if (data.get("include_pr_rider") != null) {
            application.setIncludeRefundRider((boolean)data.get("include_pr_rider"));
        } else {
            application.setIncludeRefundRider(false);
        }

        if (data.get("insured_medical4") != null) {
            application.setInsuredMedical4((boolean)data.get("insured_medical4"));
        } else {
            application.setInsuredMedical4(null);
        }

        if (application.getInsuredMedical4() == null || application.getInsuredMedical4()) {
            application.setUnderwritingLifestyleQuestionsOptin(null);
            application.setUnderwritingLifestyleMortgage(null);
            application.setUnderwritingLifestyleProfile(null);
            application.setUnderwritingLifestyleHeight(null);
            application.setUnderwritingLifestyleWeight(null);
            application.setUnderwritingLifestyleExercise(null);
        }

        if (application.isEligiblePreferentialRates()) {
            application.setMaxStep(getCurrentStep() + 1);
        } else {
            application.setMaxStep(getCurrentStep() + 2);
        }

        return application.save();
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        OnlineProtectorApplication application = getApplication(request);
        if (application.isEligiblePreferentialRates()) {
            response.sendRedirect("/our-plans/online-protector/8");
        } else {
            response.sendRedirect("/our-plans/online-protector/9");
        }
    }

    private Map<String, String> validateSumAssured(Map<String, Object> data, Map<String, String> errors) {
        float sa = (data.get("sum_assured") != null) ? Float.parseFloat(data.get("sum_assured").toString().replace(",", "")) : 0f;
        if (sa > Float.parseFloat(data.get("max_sum_assured").toString())) {
            errors.put("sum_assured", "The amount you specified is above the limit allowed.");
        }

        return errors;
    }

    private Map<String, String> validateRiderSumAssured(Map<String, Object> data, Map<String, String> errors) {
        float sa = (data.get("rider_sum_assured") != null) ? Float.parseFloat(data.get("rider_sum_assured").toString().replace(",", "")) : 0f;
        if (sa > Float.parseFloat(data.get("max_rider_sum_assured").toString())) {
            errors.put("rider_sum_assured", "The amount you specified is above the limit allowed.");
        }

        return errors;
    }

    private Map<String, String> validatePaymentMode(Map<String, Object> data, Map<String, String> errors) {
        if (data.get("payment_mode") == null) {
            errors.put("payment_mode", "Please indicate your preferred payment frequency.");
        }

        return errors;
    }

    private Map<String, String> validateInsuredMedical4(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_medical4"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_medical4", "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeSumAssured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("sum_assured");

        if (input != null) {
            try {
                sanitized.put("sum_assured", NumberFormat.getNumberInstance(java.util.Locale.US).parse(input.toString()).floatValue());
            } catch (Exception e) {
                ErrorHandler.handleError(this, e);
            }
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeRiderSumAssured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("rider_sum_assured");

        if (input != null) {
            try {
                sanitized.put("rider_sum_assured", NumberFormat.getNumberInstance(java.util.Locale.US).parse(input.toString()).floatValue());
            } catch (Exception e) {
                ErrorHandler.handleError(this, e);
            }
        }

        return sanitized;
    }

    private Map<String, Object> sanitizePaymentMode(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("payment_mode");

        try {
            sanitized.put("payment_mode", Integer.parseInt(input));
        } catch (Exception e) {
            sanitized.put("payment_mode", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeIncludeCriticalIllnessRider(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("include_ci_rider");
        if (input != null) {
            sanitized.put("include_ci_rider", input.equals("Y"));
        } else {
            sanitized.put("include_ci_rider", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeIncludeRefundRider(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("include_pr_rider");
        if (input != null) {
            sanitized.put("include_pr_rider", input.equals("Y"));
        } else {
            sanitized.put("include_pr_rider", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMedical4(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_medical4");
        if (input != null) {
            sanitized.put("insured_medical4", input.equals("Y"));
        } else {
            sanitized.put("insured_medical4", null);
        }

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
    try{
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        if (application.getSumAssured() != null) {
            data.put("sum_assured", application.getSumAssured());
        } else {
            data.put("sum_assured", 0);
        }

        if (application.getRiderSumAssured() != null) {
            data.put("rider_sum_assured", application.getRiderSumAssured());
        } else {
            data.put("rider_sum_assured", 0);
        }

        data.put("payment_mode", application.getPaymentMode());
        data.put("include_ci_rider", application.getIncludeCriticalIllnessRider());
        data.put("include_pr_rider", application.getIncludeRefundRider());
        data.put("insured_medical4", application.getInsuredMedical4());

        int anb = AgeUtils.calculateAgeNextBirthday((application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()));
        if (anb <= 50) {
            data.put("eligible_for_refund_rider", true);
        } else {
            data.put("eligible_for_refund_rider", false);
            data.put("include_pr_rider", false);
        }
        data=addPremiumData(application,data);
        
        request.setAttribute("data", data);
        logger.info("the adjusted premium is "+data.get("adjustedPremium")+"  "+data.get("ciadjustedPremiums"));
    }catch(Exception e){
    	logger.error("there is error in populating the jsp "+e);
    }
    }
    
    /**
     * Added changes for setting the premium table  
     * as a request attribute so that it can be used in
     * jsp
     * 
     * @param application     Op application object to find anb 
     * @param data            data map where attributes are saved as request
     * @return Map            Map containinng the attributes
     */
    private Map<String,Object>  addPremiumData(OnlineProtectorApplication application,Map<String,Object> data){
     try{	
    	Map premiums = application.getPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()),12);
        double premiumModal = (double)premiums.get("modalPremium");
        float adjustedPremium=(float)premiums.get("adjustedPremiumWithDiscount");
        float premiumRate=(float)premiums.get("premiumRate");
        float premiumRateUnitAmount=(float)premiums.get("premiumRateUnitAmount");
        double modalFactor=(double)premiums.get("modalFactor");
        float underwritingClassFactor=(float)premiums.get("underwritingClassFactor");
        float discountRate=(float)premiums.get("discountRate");
        float discountRateUnitAmount=(float)premiums.get("discountRateUnitAmount");
        float actualDiscount=discountRate/discountRateUnitAmount;
        double ciRiderPremiumModal = 0.0;
        float ciadjustedPremiums=0.0f;
            premiums = application.getCriticalIllnessRiderPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()),12);
            ciRiderPremiumModal = (double)premiums.get("modalPremium");
            ciadjustedPremiums=(float)premiums.get("adjustedPremiumWithDiscount");
    

        float ropRiderPremiumModal = 0.0f;
        float ropadjustedPremiums=0.0f;
       
            premiums = application.getRefundRiderPremiumAtAge(AgeUtils.calculateAgeNextBirthday(application.insuredIsSelf() ? application.getApplicantDob() : application.getInsuredDob()),12);
            ropRiderPremiumModal = (float)premiums.get("modalPremium");
            ropadjustedPremiums=(float)premiums.get("adjustedPremiumWithDiscount");
      
    
        DecimalFormat df = new DecimalFormat("S$#,###.00");
        data.put("summary_modal", df.format(premiumModal));
        data.put("summary_ci_modal", df.format(ciRiderPremiumModal));
        data.put("summary_rop_modal", df.format(ropRiderPremiumModal));
        double total=premiumModal;
        if (application.getIncludeCriticalIllnessRider()!=null&&application.getIncludeCriticalIllnessRider()) {
        	total= (total+ciRiderPremiumModal);
        }
        if (application.getIncludeRefundRider()!=null&&application.getIncludeRefundRider()) {
        	total=total+ropRiderPremiumModal;
        }
        data.put("summary_total_modal", df.format(total));
        data.put("premiumRate",premiumRate);
        data.put("premiumRateUnitAmount", premiumRateUnitAmount);
        data.put("discountRate", discountRate);
        data.put("discountRateUnitAmount", discountRateUnitAmount);
        data.put("actualDiscount",actualDiscount);
        data.put("adjustedPremium", adjustedPremium);
        data.put("ciAdjustedPremium",ciadjustedPremiums);
        data.put("modalFactor",modalFactor);
        data.put("underwritingClassFactor",underwritingClassFactor);
     }catch(Exception e){
    	 logger.info("the error is thrown  "+e);
    	 e.printStackTrace();
     }
        return data;
    }
}
