package sg.com.hsbc.dreamcatcher.eclaims.deathduetoillness;

import be.quodlibet.boxable.BaseTable;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;
import sg.com.hsbc.dreamcatcher.eclaims.DeathApplication;
import sg.com.hsbc.dreamcatcher.eclaims.DoctorPortion;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.eclaims.LifeApplication;
import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.mail.MessagingException;

public class DeathDueToIllnessApplication extends DeathApplication {
	private final static Logger logger = Logger.getLogger(DeathDueToIllnessApplication.class);
    protected Date deceasedSymptomsPresentDate;
    protected String deceasedSymptoms;
    protected DoctorPortion[] doctorList;
    protected String deathCertificate;
    protected Boolean leftAWill;
    protected String leftAWillDocument;
    protected Boolean occurInSingapore;
    protected String occurInSingaporeDocument;

    public DeathDueToIllnessApplication() {
        claimsType = "D";
        claimsTitle = "Death due to Illness";
    }

    public static DeathDueToIllnessApplication getById(String id) throws SQLException {
        DeathDueToIllnessApplication application = new DeathDueToIllnessApplication();
        application.load(id);

        return application;
    }

    @Override
    public float writeClaimInformation(PDFGenerator pd, float yStart) throws IOException {
        float tableWidth = getTableWidth(pd);
        PDPage page = pd.getCurrentPage();
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        List<String[]> rows = new ArrayList<>();

        pd.addTableHeader(table, "Claim information");

        rows.add(new String[]{"When did the deceased first present with symptoms of the illness?", formatDate(getDeceasedSymptomsPresentDate())});
        rows.add(new String[]{"What symptoms did the Deceased present before consultation with doctors?", getDeceasedSymptoms()});
        int doctorNo=1;
        for (DoctorPortion doctor : getDoctorList()) {
        	String consultationPeriod="Consultation Period";
        	String nameofDoctor="Name of Doctor / Specialist";
        	String nameOfHospital="Name of Hospital / Clinic";
        	String addressOfHospital="Address of Hospital / Clinic";
        	if(doctorNo>1){
        		consultationPeriod="Consultation "+doctorNo+" Period";
        		nameofDoctor+=" "+doctorNo;
        		nameOfHospital+=" "+doctorNo;
        		addressOfHospital+=" "+doctorNo;
        	}
        
        	if(doctor.getFromDate()!=null &&doctor.getToDate()!=null ){
	            rows.add(new String[]{
	            		"<no-border>"+consultationPeriod,
	                   (doctor.getFromDate()!=null? formatDateMonthYear(doctor.getFromDate()):"") + " - " + (doctor.getToDate()!=null?formatDateMonthYear(doctor.getToDate()):"")
	            });
        	}
        	if(StringUtils.isNotBlank(doctor.getName())){
            rows.add(new String[]{"<no-border>"+nameofDoctor, doctor.getName()});
        	}
        	if(StringUtils.isNotBlank(doctor.getHospital())){
              rows.add(new String[]{"<no-border>"+nameOfHospital, doctor.getHospital()});
        	}
        	if(StringUtils.isNotBlank(doctor.getHospitalCity())||StringUtils.isNotBlank(doctor.getHospitalStreet1())||StringUtils.isNotBlank(doctor.getHospitalStreet2())){
	            
	            StringBuffer address=new StringBuffer();
	            
	            if(doctor.getHospitalCity()!=null&&!StringUtils.equalsIgnoreCase(StringUtils.trim(doctor.getHospitalCity()), "null")&&StringUtils.isNotBlank(doctor.getHospitalCity())){
	            	address.append(doctor.getHospitalCity()+"<br>");
	            }
	            if(doctor.getHospitalStreet1()!=null&&!StringUtils.equalsIgnoreCase(StringUtils.trim(doctor.getHospitalStreet1()), "null")&&StringUtils.isNotBlank(doctor.getHospitalStreet1())){
	            	address.append(doctor.getHospitalStreet1()+"<br>");
	            }
	            if(doctor.getHospitalStreet2()!=null&&!(StringUtils.equalsIgnoreCase(StringUtils.trim(doctor.getHospitalStreet2()), "null")&&StringUtils.isNotBlank(doctor.getHospitalStreet2()))){
	            	address.append(doctor.getHospitalStreet2() +"");
	            }
	            String add=address.toString();
        		rows.add(new String[]{"<no-border-last>"+addressOfHospital,
        				add
	            });
        	}
            doctorNo++;
        }


        rows.add(new String[]{"Please upload a copy of the Death Certificate", "Number of documents uploaded - 1"});
        if(!uploadedDocuments.contains(getDeathCertificate())){
            uploadedDocuments.add(getDeathCertificate());
        }

        rows.add(new String[]{"Did the Death occur in Singapore?", getOccurInSingapore() ? "Yes" : "No"});
        if(!getOccurInSingapore()){

            rows.add(new String[]{"Please upload a copy of the letter from ICA confirming receipt of the Singapore IC, Passport and overseas Death Certificate",
            		"Number of documents uploaded - 1"});
            if(!uploadedDocuments.contains(getOccurInSingaporeDocument())){
                uploadedDocuments.add(getOccurInSingaporeDocument());
            }
        }

        rows.add(new String[]{"Did deceased leave a Will?", getLeftAWill() ? "Yes" : "No"});
        if(getLeftAWill()){
            rows.add(new String[]{"Please upload a copy of the Will","Number of documents uploaded - 1" });
            if(!uploadedDocuments.contains(getLeftAWillDocument())){
                uploadedDocuments.add(getLeftAWillDocument());
            }

        }

        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
        }

        return table.draw() - 10;
    }

    @Override

    public void generatePages(PDFGenerator pd) {

    	float yStart=0;
    	try{
            yStart=generatePage1(pd);
            yStart=generatePage2(pd,yStart);
            writeFileList(pd, yStart);
            for(int i=1;i<pd.getDocument().getNumberOfPages();i++){
                pd.applyPageHeader(pd.getDocument().getPage(i),pd.getDocument());
            }
    	}catch(Exception e){
    		logger.error("error in pdf creation "+e);
    	}

    }

    protected void initializeListField() {
        super.initializeListField();

        this.doctorList = new DoctorPortion[1];
        this.doctorList[0] = new DoctorPortion();
    }

    public Date getDeceasedSymptomsPresentDate() {
        return deceasedSymptomsPresentDate;
    }

    public void setDeceasedSymptomsPresentDate(Date deceasedSymptomsPresentDate) {
        this.deceasedSymptomsPresentDate = deceasedSymptomsPresentDate;
    }

    public String getDeceasedSymptoms() {
        return deceasedSymptoms;
    }

    public void setDeceasedSymptoms(String deceasedSymptoms) {
        this.deceasedSymptoms = deceasedSymptoms;
    }

    public DoctorPortion[] getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(DoctorPortion[] doctorList) {
        this.doctorList = doctorList;
    }

    public String getDeathCertificate() {
        return deathCertificate;
    }

    public void setDeathCertificate(String deathCertificate) {
        this.deathCertificate = deathCertificate;
    }

    public Boolean getLeftAWill() {
        return leftAWill;
    }

    public void setLeftAWill(Boolean leftAWill) {
        this.leftAWill = leftAWill;
    }

    public Boolean getOccurInSingapore() {
        return occurInSingapore;
    }

    public void setOccurInSingapore(Boolean occurInSingapore) {
        this.occurInSingapore = occurInSingapore;
    }

    public String getLeftAWillDocument() {
        return leftAWillDocument;
    }

    public void setLeftAWillDocument(String leftAWillDocument) {
        this.leftAWillDocument = leftAWillDocument;
    }

    public String getOccurInSingaporeDocument() {
        return occurInSingaporeDocument;
    }

    public void setOccurInSingaporeDocument(String occurInSingaporeDocument) {
        this.occurInSingaporeDocument = occurInSingaporeDocument;
    }


    public void sendEmail(){
        try {
			super.sendEmail(getPolicyownerEmail(), EMAIL_TEMPLATE_DEATH, getPDFFilename(false));
		} catch (MessagingException e) {
			logger.error("error in email "+e);
		}

    }
}
