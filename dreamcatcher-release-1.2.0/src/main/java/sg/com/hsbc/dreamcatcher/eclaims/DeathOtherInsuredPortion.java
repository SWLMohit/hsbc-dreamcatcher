package sg.com.hsbc.dreamcatcher.eclaims;

import java.math.BigDecimal;
import java.util.Date;

public class DeathOtherInsuredPortion {
    protected String companyName;
    protected Date policyIssueDate;
    protected String planType;
    protected BigDecimal claimAmount;
    protected Boolean claimAdmitted;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getPolicyIssueDate() {
        return policyIssueDate;
    }

    public void setPolicyIssueDate(Date policyIssueDate) {
        this.policyIssueDate = policyIssueDate;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public BigDecimal getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(BigDecimal claimAmount) {
        this.claimAmount = claimAmount;
    }

    public Boolean getClaimAdmitted() {
        return claimAdmitted;
    }

    public void setClaimAdmitted(Boolean claimAdmitted) {
        this.claimAdmitted = claimAdmitted;
    }

}
