package sg.com.hsbc.dreamcatcher.plans.application;

import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = "/our-plans/onlineinvestor-application-step2d/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/jsp/view/plans/application/step2d.jsp")
})
public class Step2dServlet extends BaseFormServlet {
}
