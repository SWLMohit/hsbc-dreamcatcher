package sg.com.hsbc.dreamcatcher.admin.api.op;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.com.hsbc.dreamcatcher.admin.AdminUtil;
import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendHistory;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendStatus;
import sg.com.hsbc.dreamcatcher.admin.entity.ResendType;
import sg.com.hsbc.dreamcatcher.admin.repository.ResendHistoryRepository;
import sg.com.hsbc.dreamcatcher.admin.service.AdminService;
import sg.com.hsbc.dreamcatcher.admin.service.impl.AdminOPServiceImpl;
import sg.com.hsbc.dreamcatcher.helpers.Database;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.plans.onlineprotector.OnlineProtectorApplication;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

@WebServlet(urlPatterns = "/our-plans/online-protector/resend-form-multi/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/resend_form_multi.jsp")
})
public class OPResendFormMultiApi extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(OPResendFormMultiApi.class);

    private AdminService adminOPService;
    private Map applicaitonMap;
    private ResendHistoryRepository resendHistoryRepository;
    private ResendHistory resendHistory;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> data = new HashMap<>();
        appendPolicyList(request, data);
        request.setAttribute("data", data);
        render(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();

        Applications applications = new Applications();
        applications.setId(request.getParameter("form_self"));
        if (applications.getId() == null) {
            // user not choose any record
            errors.put("not_choose", "Please choose one record to resend.");
            appendPolicyList(request, data);

            request.setAttribute("errors", errors);
            request.setAttribute("data", data);
            render(request, response);
            return;
        }

        adminOPService = new AdminOPServiceImpl();
        String retrieveType = request.getSession().getAttribute(FieldConstants.RETRIEVE_TYPE).toString();
        if (retrieveType != null && retrieveType.equals(FieldConstants.RETRIEVE_PASS_CODE)) {
            adminOPService.resendSMS(applications);

            //send sms success and save to db
            applications = adminOPService.findById(applications.getId());
            resendHistoryRepository = new ResendHistoryRepository();
            resendHistory = AdminUtil.convertApplicationsToResendHistory(
                    applications, ResendType.OP_RESEND_PASS_CODE_MUL, ResendStatus.SUCCESS,new Date());
            resendHistoryRepository.save(resendHistory);
            errors.put(FieldConstants.RETRIEVE_PASS_CODE,retrieveType);
        } else {
            adminOPService.resendEmail(applications);
            logger.info("resend form multi send email successful");
            //send email success and save to db
            resendHistoryRepository = new ResendHistoryRepository();
            resendHistory = AdminUtil.convertApplicationsToResendHistory(
                    applications, ResendType.OP_RESEND_EMAIL_AND_PASS_CODE_MUL, ResendStatus.SUCCESS,new Date());
            resendHistoryRepository.save(resendHistory);
            logger.info("{} save to db successful", ResendType.OP_RESEND_EMAIL_AND_PASS_CODE_MUL);

        }

        // get the email and mobile No
        applications = adminOPService.findById(applications.getId());
        data.put(FieldConstants.APPLICANT_EMAIL,applications.getApplicantEmail());
        data.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, applications.getApplicantMobileCountryCode());
        data.put(FieldConstants.APPLICANT_MOBILE_NUM, applications.getApplicantMobileNum());
        errors.put("success", "success");

        appendPolicyList(request, data);
        request.setAttribute("errors", errors);
        request.setAttribute("data", data);

        render(request, response);
    }

    /**
     * covert the policy list from map to list and append to request data
     * @param request Http request
     * @param data data map
     */
    public void appendPolicyList(HttpServletRequest request, Map<String, Object> data) {
        // put the record back on the page
        applicaitonMap = new HashMap();
        String policyList = request.getSession().getAttribute("PolicyList").toString();
        String[] policies = policyList.split("&&");
        for(String policy:policies) {
            String[] idAndName = policy.split("==");
            applicaitonMap.put(idAndName[0],idAndName[1]);
        }
        data.put("applicant_map",applicaitonMap);
        String retrieveType = request.getSession().getAttribute(FieldConstants.RETRIEVE_TYPE).toString();
        if (retrieveType != null && retrieveType.equals(FieldConstants.RETRIEVE_PASS_CODE)) {
            data.put("retrieve_pass_code",FieldConstants.RETRIEVE_PASS_CODE);
        }
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }
}
