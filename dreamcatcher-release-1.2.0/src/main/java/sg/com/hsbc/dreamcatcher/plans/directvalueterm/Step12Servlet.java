package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/12", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step12.jsp")
})
public class Step12Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 12;
    }

    /* 
     * Receives all the input parameters sent on the declaration page
     * 
     * param HttpServletRequest  request having input params
     * Returns Map   The key value pair having the inut parameters 
     *              and thier value
     */
    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        sanitized = sanitizeAcceptDeclaration1(request, sanitized);
        sanitized = sanitizeAcceptDeclaration2(request, sanitized);
        sanitized = sanitizeAcceptDeclaration3(request, sanitized);
        sanitized = sanitizeAcceptDeclaration4(request, sanitized);
        sanitized = sanitizeAcceptDeclaration5(request, sanitized);

        return sanitized;
    }

    /* 
     * Validates if all the checkboxes in the declaration page
     * are checked or no
     * param HttpServletRequest  request having input params
     * Returns Boolean true if all checkboxes are selected except 
     *                 the optional checkbox of marketing consent
     */
    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        errors = validateAcceptDeclaration2(data, errors);
        errors = validateAcceptDeclaration3(data, errors);
        errors = validateAcceptDeclaration4(data, errors);
        errors = validateAcceptDeclaration5(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    /* 
     * Processes the options selected by user in the declaration page
     * param HttpServletRequest  request having input params
     * Returns Boolean true if no errors received 
     */
    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");
        String marketing=(String) data.get("declare_accept5");
        if(StringUtils.equalsIgnoreCase(marketing, "N")){
        	 request.getSession().setAttribute("marketingConsent", false);
        	 application.setMarketingConsent(false);
        }else{
        	 request.getSession().setAttribute("marketingConsent", true);
        	 application.setMarketingConsent(true);
        }
        application.setMaxStep(getCurrentStep() + 2);
        return application.save();
    }
    /**
     * This method validate whether the accept declaration 1 check-box is checked
     * @param data    Hash map of inputs from front-end
     * @param errors  Hash map of errors for this form
     * @return        errors
     */
    private Map<String, String> validateAcceptDeclaration1(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("declare_accept1"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept1", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }
    /**
     * This method validate whether the accept declaration 2 check-box is checked
     * @param data    Hash map of inputs from front-end
     * @param errors  Hash map of errors for this form
     * @return        errors
     */
    private Map<String, String> validateAcceptDeclaration2(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("declare_accept2"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept2", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }

    /**
     * This method validate whether the accept declaration 3 check-box is checked
     * @param data    Hash map of inputs from front-end
     * @param errors  Hash map of errors for this form
     * @return        errors
     */
    private Map<String, String> validateAcceptDeclaration3(Map<java.lang.String, Object> data, Map<java.lang.String, java.lang.String> errors) {
        switch (Validator.validateString(data.get("declare_accept3"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept3", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }

    /**
     * This method validate whether the accept declaration 4 check-box is checked
     * @param data    Hash map of inputs from front-end
     * @param errors  Hash map of errors for this form
     * @return        errors
     */
    private Map<String, String> validateAcceptDeclaration4(Map<java.lang.String, Object> data, Map<java.lang.String, java.lang.String> errors) {
        switch (Validator.validateString(data.get("declare_accept4"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept4", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }

    /**
     * This method validate whether the accept declaration 5 check-box is checked
     * @param data    Hash map of inputs from front-end
     * @param errors  Hash map of errors for this form
     * @return        errors
     */
    private Map<String, String> validateAcceptDeclaration5(Map<java.lang.String, Object> data, Map<java.lang.String, java.lang.String> errors) {
        switch (Validator.validateString(data.get("declare_accept5"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept5", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }
    /**
     * This method place declaration 1 check-box input to sanitized hash map
     * @param request    This is the data from front-end
     * @param sanitized  The hash map to store front-end data
     * @return           sanitized hash map
     */
    private Map<String, Object> sanitizeAcceptDeclaration1(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("declare_accept1");

        sanitized.put("declare_accept1", input);

        return sanitized;
    }
    /**
     * This method place declaration 2 check-box input to sanitized hash map
     * @param request    This is the data from front-end
     * @param sanitized  The hash map to store front-end data
     * @return           sanitized hash map
     */
    private Map<String, Object> sanitizeAcceptDeclaration2(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("declare_accept2");

        sanitized.put("declare_accept2", input);

        return sanitized;
    }

    /**
     * This method place declaration 3 check-box input to sanitized hash map
     * @param request    This is the data from front-end
     * @param sanitized  The hash map to store front-end data
     * @return           sanitized hash map
     */
   private Map<String, Object> sanitizeAcceptDeclaration3(HttpServletRequest request, Map<String, Object> sanitized) {
       String input = request.getParameter("declare_accept3");

       sanitized.put("declare_accept3", input);

       return sanitized;
   }

   /**
    * This method place declaration 4 check-box input to sanitized hash map
    * @param request    This is the data from front-end
    * @param sanitized  The hash map to store front-end data
    * @return           sanitized hash map
    */
   private Map<String, Object> sanitizeAcceptDeclaration4(HttpServletRequest request, Map<String, Object> sanitized) {
      String input = request.getParameter("declare_accept4");

      sanitized.put("declare_accept4", input);

      return sanitized;
   }

   /**
    * This method place declaration 5 check-box input to sanitized hash map
    * @param request    This is the data from front-end
    * @param sanitized  The hash map to store front-end data
    * @return           sanitized hash map
    */
   private Map<String, Object> sanitizeAcceptDeclaration5(HttpServletRequest request, Map<String, Object> sanitized) {
      String input = request.getParameter("declare_accept5");
      if(StringUtils.isBlank(input)){
   	   sanitized.put("declare_accept5", "N");
      }else{
      sanitized.put("declare_accept5", input);
      }
      return sanitized;
   }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

}
