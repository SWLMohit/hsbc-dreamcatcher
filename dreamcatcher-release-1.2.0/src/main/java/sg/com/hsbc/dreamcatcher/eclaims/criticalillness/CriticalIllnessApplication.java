package sg.com.hsbc.dreamcatcher.eclaims.criticalillness;

import sg.com.hsbc.dreamcatcher.eclaims.IllnessApplication;
import java.sql.SQLException;

public class CriticalIllnessApplication extends IllnessApplication {
    public CriticalIllnessApplication() {
        claimsType = "C";
        claimsTitle = "Critical Illness";
    }

    public static CriticalIllnessApplication getById(String id) throws SQLException {
        CriticalIllnessApplication application = new CriticalIllnessApplication();
        application.load(id);

        return application;
    }
}
