package sg.com.hsbc.dreamcatcher.helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.log4j.Logger;

public abstract class ApplicationUtils {
	private ApplicationUtils() {};
	
	private final static Logger logger = Logger.getLogger(ApplicationUtils.class);
	
	public static String getNextAppGlobalRefNum(String refNumComplexityFlag, String refNumMailOptionFlag) {
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyMM");
		String d = "D";
		String yyMM = format.format(calendar.getTime());
		
		BiFunction<Long,Long,Integer> setOrUpdateAppGlobalRefNum = (yyMMId,currentRefNum) -> {
			Database db = new Database();
			Connection conn = db.getConnection();
			PreparedStatement pstmt = null;
			try {
				String query = "INSERT INTO application_global_reference_number(yy_mm,reference_number) values(?,?) ON DUPLICATE KEY UPDATE reference_number=?";
				pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, yyMMId);
				pstmt.setLong(2, currentRefNum);
				pstmt.setLong(3, currentRefNum);
				int updateCount = pstmt.executeUpdate();
				if(updateCount==0) {
					logger.error("No rows updated!!!");
				}
				return updateCount;
			} catch (SQLException e) {
				logger.error("setOrUpdateAppGlobalRefNum:",e);
			} finally {
				db.closeAll(conn,pstmt);
			}
			return null;
		};
		
		Function<Long,Long> getAndUpdateAppGlobalRefNum = (yyMMId) -> {
			Long referenceNumId = null;
			Database db = new Database();
			Connection conn = db.getConnection();
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				String query = "SELECT reference_number from application_global_reference_number where yy_mm=?";
				pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, yyMMId);
				rs = pstmt.executeQuery();
				if(rs.next()) {
					referenceNumId = rs.getLong("reference_number");
					setOrUpdateAppGlobalRefNum.apply(yyMMId, referenceNumId+1);
				}else {
					Long initialCounter = 1L;
					//HSBCICDU-979 Fix for application number 1 repeats
					//Reference number 1 already used for first application, so next counter is 2 for next application
					logger.info("initialCounter:" + initialCounter);
					Long nextCounter = initialCounter + 1L;
					logger.info("nextCounter:" + nextCounter);
					Integer updateCount = setOrUpdateAppGlobalRefNum.apply(yyMMId, nextCounter);
					if(Objects.nonNull(updateCount) && updateCount>0) {
						referenceNumId = initialCounter;
					}
				}
				return referenceNumId;
				
			} catch (SQLException e) {
				logger.error("getAndUpdateAppGlobalRefNum:",e);
			}finally {
			db.closeAll(conn, pstmt, rs);
		}
			return referenceNumId;
		};
		
		Long referenceNumId = getAndUpdateAppGlobalRefNum.apply(Long.parseLong(yyMM));
		logger.info("referenceNumId:" + referenceNumId);
		String finalReferenceNum = String.format("%s%s%06d-%s%s", d,yyMM, referenceNumId, refNumComplexityFlag, refNumMailOptionFlag);//D18010000001-SE
		logger.info("finalReferenceNum:" + finalReferenceNum);
		
		return finalReferenceNum;
	}
}
