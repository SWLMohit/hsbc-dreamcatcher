package sg.com.hsbc.dreamcatcher.eclaims.unemployment;

import be.quodlibet.boxable.BaseTable;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.eclaims.LifeApplication;
import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UnemploymentApplication extends LifeApplication {
    protected String effectiveUnemploymentDocument;
    private final static Logger logger = Logger.getLogger(UnemploymentApplication.class);
    public UnemploymentApplication() {
        claimsType = "U";
        claimsTitle = "Unemployment";
    }

    public static UnemploymentApplication getById(String id) throws SQLException {
        UnemploymentApplication application = new UnemploymentApplication();
        application.load(id);

        return application;
    }

    public String getEffectiveUnemploymentDocument() {
        return effectiveUnemploymentDocument;
    }

    public void setEffectiveUnemploymentDocument(String effectiveUnemploymentDocument) {
        this.effectiveUnemploymentDocument = effectiveUnemploymentDocument;
    }


    public void sendEmail() {
        try{
            super.sendEmail(getPolicyownerEmail(), EMAIL_TEMPLATE_ILLNESS, getPDFFilename(false));
        }catch(Exception e){
            logger.error("error in sending email "+e);
        }

    }


    public float generatePage2(PDFGenerator pd,float yStart) {
    	 PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
         pd.setCurrentPage(page);
     
        try {
           yStart= writeDeclaration(pd, yStart);
        } catch (Exception e) {
        }
        return yStart;

    }


    @Override

    public void generatePages(PDFGenerator pd) {
        try{
            float yStart = generatePage1(pd);
            yStart = generatePage2(pd,yStart);
            writeFileList(pd, yStart);
            for(int i=1;i<pd.getDocument().getNumberOfPages();i++){
                pd.applyPageHeader(pd.getDocument().getPage(i),pd.getDocument());
            }
        }catch(Exception e){
            logger.error("error in generating pages "+e);
        }
    }

    @Override
    public float writeClaimInformation(PDFGenerator pd, float yStart) throws IOException {
        float tableWidth = getTableWidth(pd);
        PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        List<String[]> rows = new ArrayList<>();

        pd.addTableHeader(table, "Claim information");

        rows.add(new String[]{"Letter of unemployment", "Number of documents uploaded - 1"});
        if(!uploadedDocuments.contains(getEffectiveUnemploymentDocument())){
            uploadedDocuments.add(getEffectiveUnemploymentDocument());
        }

        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
        }
        return table.draw() - 10;
    }

    public boolean save() {
        if (this.getMaxStep() == (float)1.5) {
            this.setStatus(STATUS_COMPLETE);
        }
        return super.save();
    }
}
