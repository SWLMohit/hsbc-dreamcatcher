package sg.com.hsbc.dreamcatcher.eclaims;

import be.quodlibet.boxable.BaseTable;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;
import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.mail.MessagingException;

public class IllnessApplication extends LifeApplication {
	 private final static Logger logger = Logger.getLogger(LifeApplication.class);
    protected String illnessNature;
    protected Date illnessDoctorConsultDate;
    protected Boolean illnessPreviouslySuffered;
    protected String illnessPreviouslySufferedDetail;
    protected DoctorPortion[] doctorList;
    protected Boolean relationshipIllness;
    protected RelationshipPortion[] relationshipList;
    protected String medicalReport;
    protected String clinicalAbtractForm;

    protected void initializeListField() {
        super.initializeListField();

        this.doctorList = new DoctorPortion[1];
        this.doctorList[0] = new DoctorPortion();
        this.relationshipList = new RelationshipPortion[1];
        this.relationshipList[0] = new RelationshipPortion();
    }

    public String getIllnessNature() {
        return illnessNature;
    }

    public void setIllnessNature(String illnessNature) {
        this.illnessNature = illnessNature;
    }

    public Date getIllnessDoctorConsultDate() {
        return illnessDoctorConsultDate;
    }

    public void setIllnessDoctorConsultDate(Date illnessDoctorConsultDate) {
        this.illnessDoctorConsultDate = illnessDoctorConsultDate;
    }

    public Boolean getIllnessPreviouslySuffered() {
        return illnessPreviouslySuffered;
    }

    public void setIllnessPreviouslySuffered(Boolean illnessPreviouslySuffered) {
        this.illnessPreviouslySuffered = illnessPreviouslySuffered;
    }

    public String getIllnessPreviouslySufferedDetail() {
        return illnessPreviouslySufferedDetail;
    }

    public void setIllnessPreviouslySufferedDetail(String illnessPreviouslySufferedDetail) {
        this.illnessPreviouslySufferedDetail = illnessPreviouslySufferedDetail;
    }

    public DoctorPortion[] getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(DoctorPortion[] doctorList) {
        this.doctorList = doctorList;
    }

    public Boolean getRelationshipIllness() {
        return relationshipIllness;
    }

    public void setRelationshipIllness(Boolean relationshipIllness) {
        this.relationshipIllness = relationshipIllness;
    }

    public RelationshipPortion[] getRelationshipList() {
        return relationshipList;
    }

    public void setRelationshipList(RelationshipPortion[] relationshipList) {
        this.relationshipList = relationshipList;
    }

    public String getMedicalReport() {
        return medicalReport;
    }

    public void setMedicalReport(String medicalReport) {
        this.medicalReport = medicalReport;
    }

    public String getClinicalAbtractForm() {
        return clinicalAbtractForm;
    }

    public void setClinicalAbtractForm(String clinicalAbtractForm) {
        this.clinicalAbtractForm = clinicalAbtractForm;
    }

    public void sendEmail(){
        try {
			super.sendEmail(getPolicyownerEmail(), EMAIL_TEMPLATE_ILLNESS, getPDFFilename(false));
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void generatePages(PDFGenerator pd) {
    	float yStart=0;
    	try{
        yStart=generatePage1(pd);
        yStart=generatePage2(pd,yStart);
        writeFileList(pd, yStart);
        for(int i=1;i<pd.getDocument().getNumberOfPages();i++){
        	pd.applyPageHeader(pd.getDocument().getPage(i),pd.getDocument());
        }
    	}catch(Exception e){
    		logger.error("error in pdf creation "+e);
    	}

    }

    @Override
    public float writeClaimInformation(PDFGenerator pd, float yStart) throws IOException {
        float tableWidth = getTableWidth(pd);
        PDPage page = pd.getCurrentPage();
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        List<String[]> rows = new ArrayList<>();

        pd.addTableHeader(table, "Claim information");

        rows.add(new String[]{"Please describe fully the extent and nature of your illness", getIllnessNature()});
        rows.add(new String[]{"When did you first consult a doctor in connection with your illness?", formatDate(getIllnessDoctorConsultDate())});
        rows.add(new String[]{"Have you previously suffered from, or received treatment for a similar or related illness?", getIllnessPreviouslySuffered() ? "Yes" : "No"});
        if (getIllnessPreviouslySuffered()) {
            rows.add(new String[]{"Please give full details", getIllnessPreviouslySufferedDetail()});
        }
        int doctorNo=1;
        for (DoctorPortion doctor : getDoctorList()) {
        	String consultationPeriod="Consultation Period";
        	String nameofDoctor="Name of Doctor / Specialist";
        	String nameOfHospital="Name of Hospital / Clinic";
        	String addressOfHospital="Address of Hospital / Clinic";
        	if(doctorNo>1){
        		consultationPeriod="Consultation "+doctorNo+" Period";
        		nameofDoctor+=" "+doctorNo;
        		nameOfHospital+=" "+doctorNo;
        		addressOfHospital+=" "+doctorNo;
        	}
            rows.add(new String[]{
            		"<no-border>"+consultationPeriod,
                    (doctor.getFromDate()!=null? formatDateMonthYear(doctor.getFromDate()):"") + " - " + (doctor.getToDate()!=null?formatDateMonthYear(doctor.getToDate()):"")
            });
            rows.add(new String[]{"<no-border>"+nameofDoctor, doctor.getName()});
            rows.add(new String[]{"<no-border>"+nameOfHospital, doctor.getHospital()});
            StringBuffer address=new StringBuffer();
			   if(StringUtils.isNotBlank(doctor.getHospitalCity())||StringUtils.isNotBlank(doctor.getHospitalStreet1())||StringUtils.isNotBlank(doctor.getHospitalStreet2())){
				      
				            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(doctor.getHospitalCity()), "null")&&StringUtils.isNotBlank(doctor.getHospitalCity())){
				            	address.append(doctor.getHospitalCity()+"<br>");
				            }
				            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(doctor.getHospitalStreet1()), "null")&&StringUtils.isNotBlank(doctor.getHospitalStreet1())){
				            	address.append(doctor.getHospitalStreet1()+"<br>");
				            }
				            if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(doctor.getHospitalStreet2()), "null")&&StringUtils.isNotBlank(doctor.getHospitalStreet2()))){
				            	address.append(doctor.getHospitalStreet2() +"");
				            }
			      }
String addres=address.toString();
            rows.add(new String[]{"<no-border-last>"+addressOfHospital,
            		addres
            });

            doctorNo++;
        }

        rows.add(new String[]{
                "Have any of your parents and/or siblings suffered from, or received treatment for a similar or related illness?",
                getRelationshipIllness() ? "Yes" : "No"
        });


        int relationNo=1;
        if (getRelationshipIllness()) {
            for (RelationshipPortion relationship : getRelationshipList()) {
            	String relationshipName="Relationship";
            	String nature="Nature of Illness";
            	String diagnosedDate="Diagnosed date";
            	if(relationNo>1){
            		relationshipName+=" "+relationNo;
            		nature+=" "+relationNo;
            		diagnosedDate+=" "+relationNo;
            	}
                rows.add(new String[]{"<no-border>"+relationshipName, relationship.getRelationship()});
                rows.add(new String[]{"<no-border>"+nature, relationship.getIllnessNature()});
                rows.add(new String[]{"<no-border-last>"+diagnosedDate, formatDate(relationship.getDiagnosedDate())});

                relationNo++;
            }


        }

        rows.add(new String[]{"Please upload a copy of your medical reports from your attending Doctor(s)","Number of documents uploaded - 1"});
        if(!uploadedDocuments.contains(getMedicalReport())){
            uploadedDocuments.add(getMedicalReport());
        }

        rows.add(new String[]{"Please upload a duly signed Clinical Abstract Application form", "Number of documents uploaded - 1"});
        if(!uploadedDocuments.contains(getClinicalAbtractForm())){
            uploadedDocuments.add(getClinicalAbtractForm());
        }

        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
        }
        return table.draw() - 10;
    }
}
