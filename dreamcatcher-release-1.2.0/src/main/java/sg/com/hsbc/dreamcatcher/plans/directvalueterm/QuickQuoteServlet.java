package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.BaseServlet;
import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.ProductCalculation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/quote", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/quote.jsp")
})
public class QuickQuoteServlet extends BaseServlet {

    private final static Logger logger = Logger.getLogger(QuickQuoteServlet.class);
    private final String DOB_FORMAT = "dd/MM/yyyy";

    private String[] validTermDurations = new String[] {
        "5Y",
        "20Y",
        "@65"
    };

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();

        data.put("applicant_gender", request.getParameter("applicant_gender"));
        data.put("applicant_dob", request.getParameter("applicant_dob"));
        data.put("applicant_smoker", request.getParameter("applicant_smoker"));
        data.put("sum_assured", request.getParameter("sum_assured"));
        data.put("rider_sum_assured", request.getParameter("rider_sum_assured"));
        data.put("include_ci_rider", request.getParameter("include_ci_rider"));
        data.put("campaign_code", request.getParameter("campaign_code"));
        data.put("term_duration", request.getParameter("term_duration"));

        if (data.get("applicant_gender") == null) {
            errors.put("applicant_gender", "Please select an option.");
        }

        switch (Validator.validateDate((String)data.get("applicant_dob"))) {
            case Validator.ERROR_DATE_EMPTY:
                errors.put("applicant_dob", StringConstants.EMPTY_DATE);
                break;
            case Validator.ERROR_DATE_INVALID:
                errors.put("applicant_dob", StringConstants.DATE_OF_BIRTH_INVALID);
                break;
        }

        if (errors.get("applicant_dob") == null) {
	        String dateString = String.format("%s", data.get("applicant_dob"));
	        DateFormat sdf = new SimpleDateFormat(DOB_FORMAT);
	        sdf.setLenient(false);
	        try {
	            Date dob = sdf.parse(dateString);
	            String dateString1 = new SimpleDateFormat("d/M/y").format(dob);
	            if (!Validator.isValidAge(dateString1, FieldConstants.DIRECTVALUETERM)) {
	                errors.put("applicant_dob", StringConstants.DVT_VALID_AGE);
	            }
	        } catch (ParseException e) {
	            logger.error("QuickQuoteServlet:",e);
	            errors.put("applicant_dob", "Your date of birth is invalid.");
	        }
        }

        if (data.get("applicant_smoker") == null) {
            errors.put("applicant_smoker", "Please select an option.");
        }

        if (data.get("term_duration") == null || !Arrays.asList(validTermDurations).contains(data.get("term_duration"))) {
            errors.put("term_duration", "Please select an option.");
        }

        // TODO: use product codes for duration instead of Years (etc)

        DirectValueTermApplication application = new DirectValueTermApplication();
        application.setLifeInsured("self");
        // FIXME: remove this once term years are being extracted properly
        application.setTermDuration(data.get("term_duration"));

        application.setSumAssured(Float.parseFloat(data.get("sum_assured").toString().replace(",", "")));
        if (data.get("include_ci_rider") != null && data.get("rider_sum_assured") != null) {
            application.setIncludeCriticalIllnessRider(true);
            application.setRiderSumAssured(Float.parseFloat(data.get("rider_sum_assured").toString().replace(",", "")));
        }
        application.setApplicantGender(data.get("applicant_gender"));
        if (data.get("applicant_smoker") != null) {
            application.setApplicantSmoker(data.get("applicant_smoker").toString().equals("Y"));
        }

        try {
            application.setApplicantDob((new SimpleDateFormat(DOB_FORMAT)).parse(String.format("%s", data.get("applicant_dob"))));
        } catch (Exception e) {
            ErrorHandler.handleError(application, e);
        }

        if (data.get("campaign_code") != null) {
            application.setCampaignCode(data.get("campaign_code"));
        }
        /*
         * Truncates the premium monthly and yearly values to 2 decimal places
         * for basic, ci and rop before the total is calculated
         * do that the precision is maintained
         */
        if (errors.size() == 0) {
            int anb = DirectValueTermApplication.calculateAgeNextBirthday(application.getApplicantDob());
            logger.info("===========Monthly BASIC CALCULATIONS===========");
            Map<String, Object> monthly = ProductCalculation.getPremiumAtAge(anb, DirectValueTermApplication.PAYMENT_MODE_MONTHLY, application);
            logger.info("===========YEARLY BASIC CALCULATIONS============");
            Map<String, Object> yearly = ProductCalculation.getPremiumAtAge(anb, DirectValueTermApplication.PAYMENT_MODE_ANNUALLY, application);

            double premiumMonthly = (double)monthly.get("modalPremium");
            double premiumYearly = (double)yearly.get("modalPremium");
            double premiumMonthlyTrunc= (Math.round(premiumMonthly * 100.0) / 100.0);
            double premiumYearlyTrunc= (Math.round(premiumYearly * 100.0) / 100.0);
            
            if (application.getIncludeCriticalIllnessRider() != null && application.getIncludeCriticalIllnessRider()) {
                logger.info("===========Monthly RIDER CALCULATIONS===========");
            	monthly = ProductCalculation.getCriticalIllnessRiderPremiumAtAge(anb, DirectValueTermApplication.PAYMENT_MODE_MONTHLY, application);
                logger.info("===========YEARLY RIDER CALCULATIONS===========");
            	yearly = ProductCalculation.getCriticalIllnessRiderPremiumAtAge(anb, DirectValueTermApplication.PAYMENT_MODE_ANNUALLY, application);
            	double ciMonthly=(double)monthly.get("modalPremium");
                double ciYearly=(double)yearly.get("modalPremium");
                double ciYearlyTrunc=(Math.round(ciYearly * 100.0) / 100.0);
                double ciMonthlyTrunc=(Math.round(ciMonthly * 100.0) / 100.0);
                
                premiumMonthly = (premiumMonthlyTrunc+ciMonthlyTrunc);
                premiumYearly = (premiumYearlyTrunc+ciYearlyTrunc);
                logger.info("fINAL RIDER MONTHLY PREMIUM "+premiumMonthly);
                logger.info("FINAL RIDER YEARLY PREMIUM "+premiumYearly);
            }

            DecimalFormat df = new DecimalFormat("#,###");
            DecimalFormat df1 = new DecimalFormat("#,###.00");
            request.setAttribute("monthlyPremium", df1.format(premiumMonthly));
            request.setAttribute("yearlyPremium", df1.format(premiumYearly));
            request.setAttribute("sumAssured", df.format(application.getSumAssured()));
            String duration=application.getTermDuration();
            if(StringUtils.equalsIgnoreCase(application.getTermDuration(), "5Y")){
            	duration="5 Years";
            }else if(StringUtils.equalsIgnoreCase(application.getTermDuration(), "20Y")){
            	duration="20 Years";
            }else if(StringUtils.equalsIgnoreCase(application.getTermDuration(), "@65")){
            	duration="Up to 65 Years";
            }
            request.setAttribute("termDuration", duration);
            request.setAttribute("riderSumAssured", (application.getRiderSumAssured() != null ? df.format(application.getRiderSumAssured()) : 0));
            request.setAttribute("includeCriticalIllnessRider", application.getIncludeCriticalIllnessRider());
            request.setAttribute("campaignCode", application.getCampaignCode());
        }

        request.setAttribute("errors", errors);

        render(request, response);
    }

}
