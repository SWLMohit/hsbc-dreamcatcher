package sg.com.hsbc.dreamcatcher.dreams.calculator;

import sg.com.hsbc.dreamcatcher.BaseServlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {
		"/your-dreams/savings-calculator/"
	}, initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/yourdreams/savingscalculator.jsp")
})
public class SavingsCalculatorServlet extends CalculatorServlet {
}
