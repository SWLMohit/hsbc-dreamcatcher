package sg.com.hsbc.dreamcatcher.eclaims;

import java.util.Date;

public class DeathBankruptPortion {
    protected String firstName;
    protected String lastName;
    protected String country;
    protected String state;
    protected Date bankruptcyDate;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getBankruptcyDate() {
        return bankruptcyDate;
    }

    public void setBankruptcyDate(Date bankruptcyDate) {
        this.bankruptcyDate = bankruptcyDate;
    }

}
