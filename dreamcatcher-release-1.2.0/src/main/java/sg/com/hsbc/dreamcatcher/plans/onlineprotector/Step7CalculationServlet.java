package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import sg.com.hsbc.dreamcatcher.helpers.Database;

/**
 * Step7CalculationServlet is a servlet which allows
 * the application to fetch the fresh discount values 
 * from the database 
 * based on the sum insured inputs passed to the post 
 * method of the class via a ajax request.
 * @author hchellani
 *
 */
@WebServlet(urlPatterns = "/our-plans/online-protector/calculation")
public class Step7CalculationServlet extends HttpServlet {
	 private final static Logger logger = Logger.getLogger(Step7CalculationServlet.class);
	/* 
	 * The method creates the object of OnlineProtectorApplication based on the applicationid saved in the session
	 * It sends the map which contains the discount values to be used by the frontend  
	 * 
	 * @param request request object received by the servlet
	 * @param response response object sent  to the ajax request submitted
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 try{
		String id = request.getSession().getAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME).toString();
		OnlineProtectorApplication application = OnlineProtectorApplication.getById(id);
		float sumAssuredFloat=200000.0f; 
		
		String sumAssured=request.getParameter("sum_assured");
		if(StringUtils.isNotBlank(sumAssured)){
			sumAssuredFloat=Float.parseFloat(sumAssured);
		}else{
			logger.info("the sum assured value is blank");
		}
		
		
		String gsMarker=application.getGsMarker();
		Map premiums=getDiscountRates(gsMarker,sumAssuredFloat);
		logger.info("the premiums returned is "+premiums);
		JSONObject n = new JSONObject(premiums);
		response.setContentType("application/json");
		response.getWriter().println(n);
		
	 }catch(Exception e){
		 logger.error("error i calculating "+e);
	 }
		
	}
	/**
	 * Fetches the discount rates from the database
	 * The discount rate is used in the js to calculate
	 * the new premium value based on the sum assured 
	 * sent as a parameter
	 * 
	 * @param gsMarker     value based on the gender/smoker
	 * @param sumAssured    value based on the ajax request
	 * @return Map          discount rates from the database                 
	 */
	private Map<String, Float> getDiscountRates(String gsMarker,float sumAssured){
		  Database db = new Database();
		  Connection conn = db.getConnection();
		  PreparedStatement stmt = null;
		  ResultSet rs = null;
		  Map<String,Float> discountRates=new HashMap(); 
		  float discountRate=0.0f;
		  float discountRateUnitAmount=0.0f;
		  try{
	            conn = db.getConnection();
	            
	            /*
	             * Reads all the discount rates for the sum assured specified
	             * for the specified gender and smoker combination gs_marker
	             * Only discount rates for op are picked up
	             */
	            stmt = conn.prepareStatement("SELECT * FROM discounts WHERE product_code = ? AND gs_marker = ? AND sa_min <= ? AND sa_max >= ?");
	            stmt.setString(1, OnlineProtectorApplication.PRODUCT_CODE);
	            stmt.setString(2, gsMarker.toUpperCase());
	            stmt.setFloat(3, sumAssured);
	            stmt.setFloat(4, sumAssured);

	            rs = stmt.executeQuery();
	            if (rs.next()) {
	                discountRate = rs.getFloat("discount");
	                discountRateUnitAmount = rs.getFloat("unit_amount");
	                discountRates.put("discountRate",discountRate );
            		discountRates.put("discountRateUnitAmount", discountRateUnitAmount);
            		float discountRateActual=discountRate/discountRateUnitAmount;
            		discountRates.put("actualDiscount", discountRateActual);
            		
	            }
	        } catch (Exception e) {
	        	logger.error("error in calculation "+e);
	        }finally {
	            db.closeAll(conn,stmt, rs);
	        }
		return discountRates;
	}
	
}
