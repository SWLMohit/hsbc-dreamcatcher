package sg.com.hsbc.dreamcatcher.eclaims.terminalillness;

import sg.com.hsbc.dreamcatcher.eclaims.EclaimIllnessStep1bProcessor;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/4-terminal-illness/1a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/4_terminal_illness/step1b.jsp")
})
@MultipartConfig
public class Step1bServlet extends BaseServlet {
    private EclaimIllnessStep1bProcessor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new EclaimIllnessStep1bProcessor(this);
    }

    public float getCurrentStep() {
        return (float) 1.5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        boolean result = false;
        try {
            result = processor.process(request);
        }
        catch(Exception e) {
            processor.populateFormData(request);
        }
        return result;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void populateFormData(HttpServletRequest request) {
        processor.populateFormData(request);
    }

    protected float getStep() {
        return (float) 0.5;
    }
}
