package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.BaseFormServlet;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseServlet extends BaseFormServlet {
    static String FORM_TITLE = "HSBC Insurance Direct-ValueTerm";
    static String APPLICATION_SESSION_ATTRIBUTE_NAME = "DirectValueTermApplication";

    /**
     * Handles turning current step index into next step index
     *
     * This is to allow as much parity between OP & DVT flow files as possible
     * during development
     *
     * (ie. Step 9 has the same purpose in both flows)
     *
     * @param currentStep the step we're currently on (or from which we're calculating)
     * @return the next step
     */
    static float nextStepMapping(float currentStep) {
        // could easily be a HashMap, but static initialisation in java is ugly :)
        switch ((int)currentStep) {
            case 7:
                return 9;
            default:
                return currentStep + 1;
        }
    }

    public float getNextStep() {
        return nextStepMapping(getCurrentStep());
    }

//    public void init(HttpServletRequest request) {
//        float currentStep = getCurrentStep();
//
//        if (request.getSession().getAttribute(this.getApplicationSessionAttributeName()) == null) {
//            request.getSession().setAttribute(this.getApplicationSessionAttributeName(), "2993d98b-d789-402a-8dca-ebd1d11a7fef");
//        }
//        ApplicationForm application = this.getById(request.getSession().getAttribute(this.getApplicationSessionAttributeName()).toString());
//
//        request.setAttribute("maxStep", application.getMaxStep());
//        request.setAttribute("currentStep", currentStep);
//        request.setAttribute("application", application);
//        request.setAttribute("data", new HashMap<>());
//        request.setAttribute("errors", new HashMap<String, String>());
//        request.setAttribute("otpSent", ((application.getDropoutPassword() != null && !application.getDropoutPassword().isEmpty()) ? "1" : "0"));
//        request.getSession().setMaxInactiveInterval(SESSION_TIMEOUT_SECONDS);
//  }

    public String getFormTitle() {
        return BaseServlet.FORM_TITLE;
    }

    public String getApplicationSessionAttributeName() {
        return BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME;
    }

    public String getBaseFormUrl() {
        return "/our-plans/direct-value-term/";
    }

    public ApplicationForm getById(String id) {
        return DirectValueTermApplication.getById(id);
    }

    public DirectValueTermApplication getApplication(HttpServletRequest request) {
        return (DirectValueTermApplication)super.getApplication(request);
    }
}
