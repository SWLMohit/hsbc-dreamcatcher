package sg.com.hsbc.dreamcatcher.helpers;

import java.util.Calendar;
import org.apache.commons.lang.StringUtils;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.AgeUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("Duplicates")
public class Validator {

    public static final int VALIDATE_OK = 0;
    public static final int ERROR_STRING_EMPTY = 101;
    public static final int ERROR_STRING_TOO_LONG = 102;
    public static final int ERROR_EMAIL_FORMAT_INVALID = 201;
    public static final int ERROR_PHONE_COUNTRY_CODE_INVALID = 301;
    public static final int ERROR_PHONE_NUMBER_INVALID = 302;
    public static final int ERROR_DATE_EMPTY = 401;
    public static final int ERROR_DATE_INVALID = 402;
    public static final int ERROR_CC_NUM_NOT_LOCAL = 501;
    public static final int ERROR_CC_NUM_INVALID = 502;
    public static final int ERROR_NAME_FORMAT_INVALID = 601;
    public static final int ERROR_TIN_FORMAT_INVALID = 701;
    public static final int ERROR_NRIC_FORMAT_INVALID = 801;// JIRA 625
    public static final int ERROR_PASSPORT_FORMAT_INVALID = 802;// JIRA 625

    public static final String NAME_PATTERN = "[A-Za-z\\s\\-]+";
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String PHONE_COUNTRY_CODE_PATTERN = "^\\+(?!0{2})[0-9]{2,3}$"; // JIRA 420
    public static final String PHONE_NUMBER_PATTERN = "^(?!0{10})[0-9]{8,10}$"; // JIRA 420
    public static final String CREDIT_CARD_NUM_PATTERN = "^[0-9]{16}$";
    public static final String TIN_NUM_PATTERN = "^[A-Za-z0-9]*$";
    public static final String NRIC_PATTERN="[STFGstfg][0-9]{7}[A-Za-z]";// JIRA 625
    public static final String ALPHANUMERIC_PATTERN="^[a-zA-Z0-9]*$";// JIRA 625
    public static final String JPEG_FORMAT_HEADER = "FFD8";

    public static int validateEmail(Object input) {
        int error = validateString(input, 255);
        if (error != VALIDATE_OK) {
            return error;
        }

        Matcher matcher = Pattern.compile(EMAIL_PATTERN).matcher(input.toString());
        if (!matcher.matches()) {
            return ERROR_EMAIL_FORMAT_INVALID;
        }

        return VALIDATE_OK;
    }

    public static int validatePhoneCountryCode(Object input) {
        if (input == null || input.toString().isEmpty()) {
          return ERROR_STRING_EMPTY;
        }
        else if(input.toString().equals("+")) {
          return ERROR_STRING_EMPTY;
        }
        else {
          if (input.toString().length() > 4) {
              return ERROR_STRING_TOO_LONG;
          } 
      }
        Matcher matcher = Pattern.compile(PHONE_COUNTRY_CODE_PATTERN).matcher(input.toString());
        if (!matcher.matches()) {
            return ERROR_PHONE_COUNTRY_CODE_INVALID;
        }

        return VALIDATE_OK;
    }

    public static int validatePhoneNumber(Object input) {
        int error = validateString(input, 20);
        if (error != VALIDATE_OK) {
            return error;
        }

        Matcher matcher = Pattern.compile(PHONE_NUMBER_PATTERN).matcher(input.toString());
        if (!matcher.matches()) {
            return ERROR_PHONE_NUMBER_INVALID;
        }

        return VALIDATE_OK;
    }

    public static int validateDate(Object day, Object month, Object year) {
        if (day.toString().isEmpty() && month.toString().isEmpty() && year.toString().isEmpty()) {
            return ERROR_DATE_EMPTY;
        }

        if (day.toString().isEmpty() || month.toString().isEmpty() || year.toString().isEmpty()) {
            return ERROR_DATE_INVALID;
        }

        String dateString = String.format("%s/%s/%s", day, month, year);
        DateFormat df = new SimpleDateFormat("d/M/y");
        df.setLenient(false);
        try {
            df.format(df.parse(dateString));
        } catch (ParseException e) {
            return ERROR_DATE_INVALID;
        }

        return VALIDATE_OK;
    }

    public static int validateDateExpiry(Object month, Object year) {
       
        String mm = "";
        String yy = "";
        if(month != null)
            mm = month.toString();
        if(year != null)
            yy = year.toString();


        if (mm.isEmpty() && yy.isEmpty()) {
            return ERROR_DATE_EMPTY;
        }

        if (mm.isEmpty() || yy.isEmpty()) {
            return ERROR_DATE_INVALID;
        }
        
        Calendar now = Calendar.getInstance();
        int currentYear = now.get(Calendar.YEAR) % 100;
        int currentMonth = now.get(Calendar.MONTH) + 1;

        if(Integer.parseInt(yy) < currentYear || ((Integer.parseInt(yy) == currentYear) && (Integer.parseInt(mm) < currentMonth)) || (Integer.parseInt(mm) > 12) || (Integer.parseInt(mm) < 1)){
            return ERROR_DATE_INVALID;
        }
        return VALIDATE_OK;
    }

    public static int validateName(Object input) {
        int error = validateString(input, 50);

        if (error != VALIDATE_OK) {
            return error;
        }

        Matcher matcher = Pattern.compile(NAME_PATTERN).matcher(input.toString());
        if (!matcher.matches()) {
            return ERROR_NAME_FORMAT_INVALID;
        }

        return VALIDATE_OK;
    }

    public static int validateNric(Object input) { //JIRA 625
		if (input == null || input.toString().isEmpty()) {
			return ERROR_STRING_EMPTY;
		} else {
			if (input.toString().length() == 9) {
				Matcher matcher = Pattern.compile(NRIC_PATTERN).matcher(input.toString());
				if (!matcher.matches()) {
					return ERROR_NRIC_FORMAT_INVALID;
				}
			} else {
				return ERROR_NRIC_FORMAT_INVALID;
			}
		}

		return VALIDATE_OK;
        
    }
    
    public static int validatePassport(Object input) { //JIRA 625
        int error = validateString(input, 20);
        if (error != VALIDATE_OK) {
            return error;
        }
        Matcher matcher = Pattern.compile(ALPHANUMERIC_PATTERN).matcher(input.toString());
        if (!matcher.matches()) {
            return ERROR_PASSPORT_FORMAT_INVALID;
        }
        return VALIDATE_OK;
    }

    public static int validateTinNumber(Object input) {
        int error = validateString(input, 20);

        if (error != VALIDATE_OK) {
            return error;
        }

        Matcher matcher = Pattern.compile(TIN_NUM_PATTERN).matcher(input.toString());
        if (!matcher.matches()) {
            return ERROR_TIN_FORMAT_INVALID;
        }
        return VALIDATE_OK;
    }

    public static int validateString(Object input) {
        return validateString(input, 0);
    }

    public static int validateString(Object input, int maxCharLength) {
        if (input == null || input.toString().isEmpty()) {
            return ERROR_STRING_EMPTY;
        } else {
            if (maxCharLength > 0 && input.toString().length() > maxCharLength) {
                return ERROR_STRING_TOO_LONG;
            }
        }

        return VALIDATE_OK;
    }
    
    public static int validateAlphanumeric(Object input) {
    	
    	
    	return VALIDATE_OK;
    }

    public static int validateAgeNextBirthday(Date input, int min, int max) {


        return VALIDATE_OK;
    }

    public static int validateSingaporeCreditCardNum(Object input) {
        String[] prefixes = new String[] {
            "411911", "414746", "421808", "423179", "426569", "426588", "430092", "434922", "436324", "436326",
            "451297", "452419", "454415", "456598", "458557", "467870", "469352", "483585", "486374", "486418",
            "489085", "492160", "493468", "496643", "496645", "496679", "512043", "514916", "518834", "524040",
            "526471", "540012", "540042", "540188", "540877", "541287", "542089", "542125", "542550", "544234",
            "545234", "549834", "552038", "552163", "552253", "552352", "554939", "436327", "458555", "411902",
            "458556", "458558", "455228", "455227", "539700", "411903"
        };

        Matcher matcher = Pattern.compile(CREDIT_CARD_NUM_PATTERN).matcher(input.toString());
        if (!matcher.matches()) {
            return ERROR_CC_NUM_INVALID;
        } else {
            if (!StringUtils.startsWithAny(input.toString(), prefixes)) {
                return ERROR_CC_NUM_NOT_LOCAL;
            }
        }

        return VALIDATE_OK;
    }

    public static boolean isJPEG(InputStream is,String contentType) throws IOException {
        if(StringUtils.containsIgnoreCase(contentType, "pdf")||StringUtils.containsIgnoreCase(contentType, "jpg")||StringUtils.containsIgnoreCase(contentType, "jpeg")||StringUtils.containsIgnoreCase(contentType, "png")){
        	System.out.println("valid jpeg");
        	return true;
        }

        return false;
    }
   

    public static boolean isValidAge (String dateString, String type) {
        DateFormat df = new SimpleDateFormat("d/M/y");
        df.setLenient(false);
        try {
            Date dob = df.parse(dateString);
            int anb = AgeUtils.calculateAgeNextBirthday(dob);

            if (type.equals(FieldConstants.ONLINEPROTECTOR)) {
                if (anb < 19 || anb > 60)
                    return false;
            }  else if (type.equals(FieldConstants.DIRECTVALUETERM)) {
                if (anb < 19 || anb > 62)
                    return false;
            }
            return true;
        } catch (ParseException e) {
            return false;
        }
    }


    /**
     * This method validateEmailAndVerificationCode the string dateStr to make sure it is a valid date
     * @param     dateStr (date string in dd/MM/yyyy format)
     * @return    VALIDATE_OK if success
     *            ERROR_DATE_EMPTY or ERROR_DATE_INVALID if failed
     */
    public static int validateDate(String dateStr) {
        if (dateStr==null || dateStr.isEmpty()) {
            return ERROR_DATE_EMPTY;
        }

        if (dateStr.contains("0000")) { //year should not be zeros
            return ERROR_DATE_INVALID;
        }

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        try {
            df.format(df.parse(dateStr));
        } catch (ParseException e) {
            return ERROR_DATE_INVALID;
        }

        return VALIDATE_OK;
    }

    
    /**
     * This method compares the two String values and returns true when the strings are equal.
     * if the strings are not equal will return false.
     * 
     * @param nationality1  String variable containing primary nationality
     * @param nationality2  String variable containing secondary nationality
     * @return              boolean result of the comparsion 
     */
    public static boolean isUniqueNationality(String nationality1, String nationality2 ) {
       return (nationality1.equals(nationality2));
    }
}
