package sg.com.hsbc.dreamcatcher.eclaims.terminalillness;

import sg.com.hsbc.dreamcatcher.eclaims.IllnessApplication;
import java.sql.SQLException;

public class TerminalIllnessApplication extends IllnessApplication {
    public TerminalIllnessApplication() {
        claimsType = "L";
        claimsTitle = "Terminal Illness";
    }

    public static TerminalIllnessApplication getById(String id) throws SQLException {
        TerminalIllnessApplication application = new TerminalIllnessApplication();
        application.load(id);

        return application;
    }
}
