package sg.com.hsbc.dreamcatcher.eclaims.deathduetoillness;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/2-death-due-to-illness/5", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/2_death_due_to_illness/step5a.jsp")
})
public class Step5aServlet extends BaseServlet {
    public float getCurrentStep() {
        return (float)5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        application.setMaxStep(getCurrentStep() + (float)0.5);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("payment", application.getPayment());
        request.setAttribute("data", data);
    }

    protected float getStep() {
        return (float)0.5;
    }
}
