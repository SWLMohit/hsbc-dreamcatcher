package sg.com.hsbc.dreamcatcher.eclaims;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class EclaimIllnessStep1aProcessor {
    private EClaimBaseServlet servletInstance;

    public EclaimIllnessStep1aProcessor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        String input = request.getParameter("illness_nature");
        sanitized.put("illness_nature", input);
        input = request.getParameter("illness_doctor_consult_date_day");
        sanitized.put("illness_doctor_consult_date_day", input);
        input = request.getParameter("illness_previously_suffered");
        sanitized.put("illness_previously_suffered", Boolean.parseBoolean(input));
        if (Boolean.parseBoolean(input)) {
            input = request.getParameter("illness_previously_suffered_detail");
            sanitized.put("illness_previously_suffered_detail", input);
        }

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        IllnessApplication application = (IllnessApplication)servletInstance.getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setIllnessNature(data.get("illness_nature").toString());
        try {
            Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", data.get("illness_doctor_consult_date_day")));
            application.setIllnessDoctorConsultDate(date);
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;

        }
        application.setIllnessPreviouslySuffered((boolean)data.get("illness_previously_suffered"));
        if (application.getIllnessPreviouslySuffered()) {
            application.setIllnessPreviouslySufferedDetail(data.get("illness_previously_suffered_detail").toString());
        } else {
            application.setIllnessPreviouslySufferedDetail(null);
        }

        application.setMaxStep(servletInstance.getCurrentStep() + servletInstance.getStep());
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        IllnessApplication application = (IllnessApplication)servletInstance.getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        data.put("illness_nature", application.getIllnessNature());
        Date date = application.getIllnessDoctorConsultDate();
        if (date != null) {
            data.put("illness_doctor_consult_date_day", (new SimpleDateFormat("dd/MM/y")).format(date));
        }
        data.put("illness_previously_suffered", application.getIllnessPreviouslySuffered());
        if (application.getIllnessPreviouslySuffered() != null && application.getIllnessPreviouslySuffered()) {
            data.put("illness_previously_suffered_detail", application.getIllnessPreviouslySufferedDetail());
        }

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("day_list", servletInstance.getDayList());
        data.put("month_list", servletInstance.getMonthList());
        data.put("year_list", servletInstance.getYearList());

        request.setAttribute("formData", data);
    }
}
