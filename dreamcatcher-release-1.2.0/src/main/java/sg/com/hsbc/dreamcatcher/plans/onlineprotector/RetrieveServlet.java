package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/*@WebServlet(urlPatterns = "/our-plans/online-protector/retrieve/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/retrieve.jsp")
})*/
public class RetrieveServlet extends HttpServlet {
	
	private final static Logger logger = LoggerFactory.getLogger(RetrieveServlet.class);
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        render(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();
        
        errors = validate(request);
        if (!errors.isEmpty()) {
        	render(request, response);
        	return;
        }

        data.put("email", request.getParameter("email"));
        data.put("verify_code", request.getParameter("verify_code"));

        OnlineProtectorApplication application = null;
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT id FROM applications WHERE product_type = ? AND applicant_email = ? AND BINARY dropout_password = ? AND status = ? ORDER BY date_updated DESC");//JIRA HSBCIDCU-962 Case sensitive matching
            stmt.setString(1, OnlineProtectorApplication.PRODUCT_TYPE);
            stmt.setString(2, data.get("email").toString());
            stmt.setString(3, data.get("verify_code").toString());
            stmt.setInt(4, OnlineProtectorApplication.STATUS_INCOMPLETE);
            rs = stmt.executeQuery();
            if (rs.next()) {
                application = OnlineProtectorApplication.getById(rs.getString("id"));
            } else {
                errors.put("retrieve", "Sorry, either your email address or verification code is incorrect. Please try again.");
            }
            
            if (application != null) {
                request.getSession().setAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME, application.getId());
                response.sendRedirect("/our-plans/online-protector" + (application.getMaxStep() > 0 ? "/" + ((int)application.getMaxStep()) : ""));
                return;
            }
        } catch (Exception e) {
            ErrorHandler.handleError(this, e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        } finally {
            db.closeAll(conn,stmt, rs);
        }

        request.setAttribute("errors", errors);
        request.setAttribute("data", data);

        render(request, response);
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }
    
    /**
     * This is the main validation function for the retrieval servlet. It will call individual function to validate each field.
     * @param request
     * @return errors
     */
    public Map<String, String> validate(HttpServletRequest request) {
        Map<String, Object> data = new HashMap<>();
        Map<String, String> errors = new HashMap<>();
        
        String inputEmail = request.getParameter(FieldConstants.RETRIEVAL_EMAIL);
        data.put(FieldConstants.RETRIEVAL_EMAIL, inputEmail);
        
        String inputVerifyCode = request.getParameter(FieldConstants.RETRIEVAL_VERIFY_CODE);
        data.put(FieldConstants.RETRIEVAL_VERIFY_CODE, inputVerifyCode);
        
        errors = validateApplicantEmail(data, errors);
        errors = validateApplicantVerifyCode(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return errors;
    }

    /**
     * This function validates the email field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantEmail(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.RETRIEVAL_EMAIL))) {
	        case Validator.ERROR_STRING_EMPTY:
	            errors.put(FieldConstants.RETRIEVAL_EMAIL, "Your email address is missing.");
	            break;
	    }
	    
	    switch (Validator.validateEmail(data.get(FieldConstants.RETRIEVAL_EMAIL))) {
	        case Validator.ERROR_EMAIL_FORMAT_INVALID:
	            errors.put(FieldConstants.RETRIEVAL_EMAIL, "Your email address is invalid.");
	            break;
	    }

        return errors;
    }
    
    /**
     * This function validates the verification code field.
     * @param data
     * @param errors
     * @return errors
     */
    private Map<String, String> validateApplicantVerifyCode(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.RETRIEVAL_VERIFY_CODE))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.RETRIEVAL_VERIFY_CODE, "Your verification code is missing.");
                break;
        }

        return errors;
    }
    
}
