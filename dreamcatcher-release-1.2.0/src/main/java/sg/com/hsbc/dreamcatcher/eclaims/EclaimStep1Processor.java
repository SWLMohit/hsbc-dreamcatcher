package sg.com.hsbc.dreamcatcher.eclaims;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.util.*;


public class EclaimStep1Processor {
    private final static Logger logger = LoggerFactory.getLogger(EclaimStep1Processor.class);
    private EClaimBaseServlet servletInstance;

    public EclaimStep1Processor(EClaimBaseServlet servletInstance) {
        this.servletInstance = servletInstance;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizeClaimantList(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizeClaimantList(HttpServletRequest request, Map<String, Object> sanitized) {
        ArrayList<Map<String, Object>> claimantList = new ArrayList<>();
        String[] firstName = request.getParameterValues("claimant_first_name");
        String[] lastName = request.getParameterValues("claimant_last_name");
        String[] relationship = request.getParameterValues("claimant_relationship");
        String[] relationshipCity = request.getParameterValues("claimant_city");
        String[] postalCode = request.getParameterValues("claimant_postal_code");
        String[] buildingNumber = request.getParameterValues("claimant_building_number");
        String[] unitNumber1 = request.getParameterValues("claimant_unit_number_1");
        String[] unitNumber2 = request.getParameterValues("claimant_unit_number_2");
        String[] street1 = request.getParameterValues("claimant_street_1");
        String[] street2 = request.getParameterValues("claimant_street_2");
        String[] buildingName = request.getParameterValues("claimant_building_name");

        for (int i = 0; i < firstName.length; i++) {
            Map<String, Object> subData = new HashMap<>();
            subData.put("claimant_first_name", firstName[i]);
            subData.put("claimant_last_name", lastName[i]);
            subData.put("claimant_relationship", relationship[i]);
            servletInstance.sanitizeUploadedDocument(request, subData, "claimant_relationship_document_"+ (i+1), "ClaimantRelationshipDocument");
            subData.put("claimant_city", relationshipCity[i]);
            subData.put("claimant_postal_code", postalCode[i]);
            subData.put("claimant_building_number", buildingNumber[i]);
            subData.put("claimant_unit_number_1", unitNumber1[i]);
            subData.put("claimant_unit_number_2", unitNumber2[i]);
            subData.put("claimant_street_1", street1[i]);
            subData.put("claimant_street_2", street2[i]);
            subData.put("claimant_building_name", buildingName[i]);
            servletInstance.sanitizeUploadedDocument(request, subData, "claimant_address_document_"+ (i+1), "ClaimantAddressDocument");

            claimantList.add(subData);
        }
        sanitized.put("claimant_list", claimantList.toArray());

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
      try {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        ArrayList<DeathClaimantPortion> claimantList = new ArrayList<>();
        DeathClaimantPortion[] oldClaimantList = application.getClaimantList();
        int i = 0;
        List<String> claimantRelationshipDocs;
        List<String> claimantAddressDocs;
        for (Object subDataItem : (Object[])data.get("claimant_list")) {
            DeathClaimantPortion claimant;
            if (i < oldClaimantList.length) {
                claimant = oldClaimantList[i];
            } else {
                claimant = new DeathClaimantPortion();
            }
            Map<String, Object> subData = (Map<String, Object>)subDataItem;
            claimant.setFirstName(subData.get("claimant_first_name").toString());
            claimant.setLastName(subData.get("claimant_last_name").toString());
            claimant.setRelationship(subData.get("claimant_relationship").toString());
            claimantRelationshipDocs = new ArrayList<>();
            claimantAddressDocs = new ArrayList<>();
            for (String key:subData.keySet()) {
                if (key.startsWith("claimant_address_document")) {
                    claimantAddressDocs.add(subData.get(key).toString());
                } else if (key.startsWith("claimant_relationship_document")) {
                    claimantRelationshipDocs.add(subData.get(key).toString());
                }
            }
            claimant.setRelationshipDocument(claimantRelationshipDocs);
            claimant.setCity(subData.get("claimant_city").toString());
            claimant.setPostalCode(subData.get("claimant_postal_code").toString());
            claimant.setBuildingNumber(subData.get("claimant_building_number").toString());
            claimant.setUnitNumber1(subData.get("claimant_unit_number_1").toString());
            claimant.setUnitNumber2(subData.get("claimant_unit_number_2").toString());
            claimant.setStreet1(subData.get("claimant_street_1").toString());
            claimant.setStreet2(subData.get("claimant_street_2").toString());
            claimant.setBuildingName(subData.get("claimant_building_name").toString());
            claimant.setAddressDocument(claimantAddressDocs);
            
            /** 10MB File size implementation starts here **/ 
            if(application.getFileInfo()!=null){
              Map<String, Long>updatedFileInfo=application.getFileInfo();
              if(subData.get("fileInfo")!=null) {
              updatedFileInfo.putAll((Map<String,Long>)subData.get("fileInfo"));
              application.setFileInfo(updatedFileInfo);
              }
            }
            else {
              application.setFileInfo((Map<String,Long>)subData.get("fileInfo")); 
            }
            /** 10MB File size implementation ends here **/ 
            
            claimantList.add(claimant);
            i++;
        }
        application.setClaimantList(claimantList.toArray(new DeathClaimantPortion[claimantList.size()]));

        application.setMaxStep(servletInstance.getCurrentStep() + 1);
        return application.save();
      }
      catch(Exception e) {
        logger.error("Exception occurred in EclaimStep1Processor process method "+e);
      }
      return false;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = servletInstance.getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");
        ArrayList<Map<String, Object>> claimantList = new ArrayList<>();
        int i=0;
        for (DeathClaimantPortion claimant : application.getClaimantList()) {
            Map<String, Object> subData = new HashMap<>();
            i++;
            subData.put("claimant_first_name", claimant.getFirstName());
            subData.put("claimant_last_name", claimant.getLastName());
            subData.put("claimant_relationship", claimant.getRelationship());
            subData.put("claimant_relationship_document_" + i, claimant.getRelationshipDocument());
            subData.put("claimant_city", claimant.getCity());
            subData.put("claimant_postal_code", claimant.getPostalCode());
            subData.put("claimant_building_number", claimant.getBuildingNumber());
            subData.put("claimant_unit_number_1", claimant.getUnitNumber1());
            subData.put("claimant_unit_number_2", claimant.getUnitNumber2());
            subData.put("claimant_street_1", claimant.getStreet1());
            subData.put("claimant_street_2", claimant.getStreet2());
            subData.put("claimant_building_name", claimant.getBuildingName());
            subData.put("claimant_address_document_" + i, claimant.getAddressDocument());

            claimantList.add(subData);
        }
        data.put("claimant_list", claimantList.toArray());

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("country_list", servletInstance.getCountryList());

        request.setAttribute("formData", data);
    }
}
