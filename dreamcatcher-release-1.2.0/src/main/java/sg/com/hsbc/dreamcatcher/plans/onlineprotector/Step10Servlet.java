package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.StringConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/10", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step10.jsp")
})
public class Step10Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 10;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        if (application.applicantIsWorking()) {
            sanitized = sanitizeApplicantOccupation(request, sanitized);
            sanitized = sanitizeApplicantIndustry(request, sanitized);
            sanitized = sanitizeApplicantCompanyName(request, sanitized);
            sanitized = sanitizeApplicantCompanyCity(request, sanitized);
            sanitized = sanitizeApplicantCompanyCountry(request, sanitized);
        }

        if (application.applicantIsStudying()) {
            sanitized = sanitizeApplicantStudiesEndDate(request, sanitized);
        }

        if (!application.insuredIsSelf()) {
            if (application.insuredIsWorking()) {
                sanitized = sanitizeInsuredOccupation(request, sanitized);
                sanitized = sanitizeInsuredIndustry(request, sanitized);
                sanitized = sanitizeInsuredCompanyName(request, sanitized);
            }

            if (application.insuredIsStudying()) {
                sanitized = sanitizeInsuredStudiesEndDate(request, sanitized);
            }
        }

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        if (application.applicantIsWorking()) {
            errors = validateApplicantOccupation(data, errors);
            errors = validateApplicantIndustry(data, errors);
            errors = validateApplicantCompanyName(data, errors);
            errors = validateApplicantCompanyCity(data, errors);
            errors = validateApplicantCompanyCountry(data, errors);
        }

        if (application.applicantIsStudying()) {
            errors = validateApplicantStudiesEndDate(data, errors);
        }

        if (!application.insuredIsSelf()) {
            if (application.insuredIsWorking()) {
                errors = validateInsuredOccupation(data, errors);
                errors = validateInsuredIndustry(data, errors);
                errors = validateInsuredCompanyName(data, errors);
            }

            if (application.insuredIsStudying()) {
                errors = validateInsuredStudiesEndDate(data, errors);
            }
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        if (data.get("applicant_occupation") != null) {
            application.setApplicantOccupation(data.get("applicant_occupation").toString());
        } else {
            application.setApplicantOccupation(null);
        }

        if (data.get("applicant_industry") != null) {
            application.setApplicantIndustry(data.get("applicant_industry").toString());
        } else {
            application.setApplicantIndustry(null);
        }

        if (data.get("applicant_company_name") != null) {
            application.setApplicantCompanyName(data.get("applicant_company_name").toString());
        } else {
            application.setApplicantCompanyName(null);
        }

        if (data.get("applicant_company_city") != null) {
            application.setApplicantCompanyCity(data.get("applicant_company_city").toString());
        } else {
            application.setApplicantCompanyCity(null);
        }

        if (data.get("applicant_company_country") != null) {
            application.setApplicantCompanyCountry(data.get("applicant_company_country").toString());
        } else {
            application.setApplicantCompanyCountry(null);
        }

        if (data.get("insured_occupation") != null) {
            application.setInsuredOccupation(data.get("insured_occupation").toString());
        } else {
            application.setInsuredOccupation(null);
        }

        if (data.get("insured_industry") != null) {
            application.setInsuredIndustry(data.get("insured_industry").toString());
        } else {
            application.setInsuredIndustry(null);
        }

        if (data.get("insured_company_name") != null) {
            application.setInsuredCompanyName(data.get("insured_company_name").toString());
        } else {
            application.setInsuredCompanyName(null);
        }

        try {
            Date applicantStudiesEndDate = (new SimpleDateFormat("d/M/y")).parse(String.format("%s/%s", 1, data.get("applicant_studies_end_date")));
            application.setApplicantStudiesEndDate(applicantStudiesEndDate);
        } catch (ParseException e) {
            application.setApplicantStudiesEndDate(null);
        }

        try {
            Date insuredStudiesEndDate = (new SimpleDateFormat("d/M/y")).parse(String.format("%s/%s", 1, data.get("insured_studies_end_date")));
            application.setInsuredStudiesEndDate(insuredStudiesEndDate);
        } catch (ParseException e) {
            application.setInsuredStudiesEndDate(null);
        }

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateApplicantOccupation(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("applicant_occupation"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_occupation", "Your occupation is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("applicant_occupation", "Your occupation contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantIndustry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_industry"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_industry", "Your industry is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantCompanyName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("applicant_company_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_company_name", "Your company name is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("applicant_company_name", "Your company name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantCompanyCity(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("applicant_company_city"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_company_city", "Your company city is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("applicant_company_city", "Your company city contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantCompanyCountry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("applicant_company_country"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_company_country", "Your company country is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("applicant_company_country", "Your company country contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantStudiesEndDate(Map<String, Object> data, Map<String, String> errors) {
        String endDate = String.format("%s", data.get("applicant_studies_end_date"));
        if ( !StringUtils.isBlank(endDate) ) 
        	endDate = String.format("%s/%s", 1, data.get("applicant_studies_end_date"));
        switch (Validator.validateDate(endDate)) {
            case Validator.ERROR_DATE_EMPTY:
                errors.put("applicant_studies_end_date", StringConstants.EMPTY_DATE);
                break;
            case Validator.ERROR_DATE_INVALID:
                errors.put("applicant_studies_end_date", "The end date of your studies is invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredOccupation(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("insured_occupation"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_occupation", "Your spouse's occupation is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("insured_occupation", "Your spouse's occupation contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredIndustry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_industry"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_industry", "Your spouse's industry is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredCompanyName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("insured_company_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_company_name", "Your spouse's company name is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("insured_company_name", "Your spouse's company name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredStudiesEndDate(Map<String, Object> data, Map<String, String> errors) {
        String endDate = String.format("%s", data.get("insured_studies_end_date"));
        if ( !StringUtils.isBlank(endDate) ) 
        	endDate = String.format("%s/%s", 1, data.get("insured_studies_end_date"));
        switch (Validator.validateDate(endDate)) {
            case Validator.ERROR_DATE_EMPTY:
                errors.put("insured_studies_end_date", StringConstants.EMPTY_DATE);
                break;
            case Validator.ERROR_DATE_INVALID:
                errors.put("insured_studies_end_date", "The end date of spouse's studies is invalid.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantOccupation(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_occupation");
        sanitized.put("applicant_occupation", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIndustry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_industry");
        sanitized.put("applicant_industry", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantCompanyName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_company_name");
        sanitized.put("applicant_company_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantCompanyCity(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_company_city");
        sanitized.put("applicant_company_city", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantCompanyCountry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_company_country");
        sanitized.put("applicant_company_country", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantStudiesEndDate(HttpServletRequest request, Map<String, Object> sanitized) {
        String endDate = request.getParameter("applicant_studies_end_date");
        sanitized.put("applicant_studies_end_date", endDate);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredOccupation(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_occupation");
        sanitized.put("insured_occupation", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredIndustry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_industry");
        sanitized.put("insured_industry", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredCompanyName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_company_name");
        sanitized.put("insured_company_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredStudiesEndDate(HttpServletRequest request, Map<String, Object> sanitized) {
        String endDate = request.getParameter("insured_studies_end_date");
        sanitized.put("insured_studies_end_date", endDate);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("applicant_occupation", application.getApplicantOccupation());
        data.put("applicant_industry", application.getApplicantIndustry());
        data.put("applicant_company_name", application.getApplicantCompanyName());
        data.put("applicant_company_city", application.getApplicantCompanyCity());
        data.put("applicant_company_country", application.getApplicantCompanyCountry());
        data.put("insured_occupation", application.getInsuredOccupation());
        data.put("insured_industry", application.getInsuredIndustry());
        data.put("insured_company_name", application.getInsuredCompanyName());

        Date applicantStudiesEndDate = application.getApplicantStudiesEndDate();
        if (applicantStudiesEndDate != null) {
            data.put("applicant_studies_end_date", (new SimpleDateFormat("MM/y")).format(applicantStudiesEndDate));
        }

        Date insuredStudiesEndDate = application.getInsuredStudiesEndDate();
        if (insuredStudiesEndDate != null) {
            data.put("insured_studies_end_date", (new SimpleDateFormat("MM/y")).format(insuredStudiesEndDate));
        }

        if (StringUtils.isBlank(application.getApplicantCompanyCity())) {
            if (application.applicantIsSingaporean() || application.applicantIsPermanentResident() || application.applicantIsPassHolder()) {
            	data.put("applicant_company_city", "Singapore");
            }
        }
        if (StringUtils.isBlank(application.getApplicantCompanyCountry())) {
            if (application.applicantIsSingaporean() || application.applicantIsPermanentResident() || application.applicantIsPassHolder()) {
                data.put("applicant_company_country", "Singapore");
            }
        }

        request.setAttribute("data", data);
    }

}
