package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/13", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step13.jsp")
})
public class Step13Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 13;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        sanitized = sanitizeAcceptDeclaration(request, sanitized);
        sanitized = sanitizeAcceptDeclarationInsured(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        if (!application.insuredIsSelf()) {
            errors = validateAcceptDeclarationInsured(data, errors);
        } else {
        	errors = validateAcceptDeclaration(data, errors);
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateAcceptDeclaration(Map<java.lang.String, Object> data, Map<java.lang.String, java.lang.String> errors) {
        switch (Validator.validateString(data.get("declare_accept"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateAcceptDeclarationInsured(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("declare_accept_insured"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("declare_accept_insured", "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeAcceptDeclaration(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("declare_accept");

        sanitized.put("declare_accept", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeAcceptDeclarationInsured(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("declare_accept_insured");

        sanitized.put("declare_accept_insured", input);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

}
