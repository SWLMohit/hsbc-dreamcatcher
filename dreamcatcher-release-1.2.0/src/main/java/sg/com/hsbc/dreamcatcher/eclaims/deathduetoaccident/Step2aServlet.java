package sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/1-death-due-to-accident/2", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/1_death_due_to_accident/step2a.jsp")
})
public class Step2aServlet extends BaseServlet {
    public float getCurrentStep() {
        return (float)2;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizeAccidentDate(request, sanitized);
        sanitized = sanitizeAccidentPlace(request, sanitized);
        sanitized = sanitizeAccidentDetails(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizeAccidentDate(HttpServletRequest request, Map<String, Object> sanitized) {
        String accidentDateDay = request.getParameter("accident_date_day");

        sanitized.put("accident_date_day", accidentDateDay);

        return sanitized;
    }

    private Map<String, Object> sanitizeAccidentPlace(HttpServletRequest request, Map<String, Object> sanitized) {
        String accidentPlace = request.getParameter("accident_place");

        sanitized.put("accident_place", accidentPlace);

        return sanitized;
    }

    private Map<String, Object> sanitizeAccidentDetails(HttpServletRequest request, Map<String, Object> sanitized) {
        String accidentDetails = request.getParameter("accident_details");

        sanitized.put("accident_details", accidentDetails);

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        DeathDueToAccidentApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        try {
            Date accidentDate = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", data.get("accident_date_day")));
            application.setAccidentDate(accidentDate);
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }
        application.setAccidentPlace(data.get("accident_place").toString());
        application.setAccidentDetails(data.get("accident_details").toString());

        application.setMaxStep(getCurrentStep() + (float)0.5);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DeathDueToAccidentApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        Date accidentDate = application.getAccidentDate();
        if (accidentDate != null) {
            data.put("accident_date_day", (new SimpleDateFormat("dd/MM/y")).format(accidentDate));
        }
        data.put("accident_place", application.getAccidentPlace());
        data.put("accident_details", application.getAccidentDetails());

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("day_list", this.getDayList());
        data.put("month_list", this.getMonthList());
        data.put("year_list", this.getYearList());

        request.setAttribute("formData", data);
    }

    protected float getStep() {
        return (float)0.5;
    }
}
