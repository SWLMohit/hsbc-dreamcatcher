package sg.com.hsbc.dreamcatcher.helpers;

public class FieldConstants {

    //online protector
    public static final String ONLINEPROTECTOR = "OP";
    public static final String DIRECTVALUETERM= "DVT";
    
    public static final String JAVA_TEMP_DIR= "java.io.tmpdir";// HSBCIDCU-661
    
    //OP step 14
    public static final String INSURED_IDV_UPLOAD = "insured_idv_upload";// HSBCIDCU-661
    public static final String INSURED_SECOND_IDV_UPLOAD = "insured_idv_two_upload";// HSBCIDCU-661

    // dvt quote
    public static final String SUM_ASSURED = "sum_assured";
    public static final String RIDER_SUM_ASSURED = "rider_sum_assured";
    public static final String MIN_SUM_ASSURED = "min_sum_assured";
    public static final String MAX_SUM_ASSURED = "max_sum_assured";
    public static final String MIN_RIDER_SUM_ASSURED = "min_rider_sum_assured";
    public static final String MAX_RIDER_SUM_ASSURED = "max_rider_sum_assured";

    // dvt step 3
    public static final String APPLICANT_RESIDENCY = "applicant_residency";
    public static final String APPLICANT_FIRST_NAME = "applicant_first_name";
    public static final String APPLICANT_LAST_NAME = "applicant_last_name";
    public static final String APPLICANT_MOBILE_COUNTRY_CODE = "applicant_mobile_country_code";
    public static final String APPLICANT_MOBILE_NUM = "applicant_mobile_num";
    public static final String APPLICANT_EMAIL = "applicant_email";
    public static final String APPLICANT_GENDER = "applicant_gender";
    public static final String APPLICANT_DOB = "applicant_dob";
    public static final String APPLICANT_DOB_DAY = "applicant_dob_day";
    public static final String APPLICANT_DOB_MONTH = "applicant_dob_month";
    public static final String APPLICANT_DOB_YEAR = "applicant_dob_year";
    public static final String LIFE_INSURED = "life_insured";

    // dvt step 4
    public static final String APPLICANT_BIRTH_COUNTRY = "applicant_birth_country";
    public static final String APPLICANT_MULTIPLE_NATIONALITIES = "applicant_multiple_nationalities";
    public static final String APPLICANT_NATIONALITY = "applicant_nationality";
    public static final String APPLICANT_NATIONALITY_2 = "applicant_nationality2";
    public static final String APPLICANT_NATIONALITY_3 = "applicant_nationality3";
    public static final String APPLICANT_NRIC = "applicant_nric";
    public static final String APPLICANT_PASSPORT = "applicant_passport";
    public static final String APPLICANT_ENGLISH_PROFICIENT = "applicant_en_proficient";

    //dvt step 5
    public static final String APPLICANT_SMOKER = "applicant_smoker";
    public static final String EXISTING_LIFE_INSURANCE_COVERAGE = "existing_life_insurance_coverage";
    public static final String INSURED_MEDICAL_1 = "insured_medical1";
    public static final String INSURED_MEDICAL_2 = "insured_medical2";
    public static final String INSURED_MEDICAL_3 = "insured_medical3";
    public static final String INSURED_MEDICAL_4 = "insured_medical4";
    public static final String INSURED_NRIC = "insured_nric";		//JIRA 978
    public static final String INSURED_PASSPORT = "insured_passport"; //JIRA 978

    //dvt step 7
    public static final String TERM_DURATION = "term_duration";
    public static final String PAYMENT_MODE = "payment_mode";
    public static final String INCLUDE_CI_RIDER = "include_ci_rider";

    //dvt step 9
    public static final String DECLARE_ACCEPT = "declare_accept";

    //dvt step 10
    public static final String APPLICANT_EMPLOYMENT = "applicant_employment";
    public static final String APPLICANT_INCOME = "applicant_income";
    public static final String APPLICANT_OCCUPATION = "applicant_occupation";
    public static final String APPLICANT_INDUSTRY = "applicant_industry";
    public static final String APPLICANT_COMPANY_NAME = "applicant_company_name";
    public static final String APPLICANT_COMPANY_CITY = "applicant_company_city";
    public static final String APPLICANT_COMPANY_COUNTRY = "applicant_company_country";
    public static final String APPLICANT_STUDIES_END_DATE_MONTH = "applicant_studies_end_date_month";
    public static final String APPLICANT_STUDIES_END_DATE_YEAR = "applicant_studies_end_date_year";
    public static final String APPLICANT_STUDIES_END_DATE = "applicant_studies_end_date";

    // dvt step 11 - applicant residential details
    public static final String APPLICANT_ADDRESS_POSTAL_CODE = "applicant_address_postal_code";
    public static final String APPLICANT_ADDRESS_HOUSE = "applicant_address_house";
    public static final String APPLICANT_ADDRESS_UNIT = "applicant_address_unit";
    public static final String APPLICANT_ADDRESS_STREET = "applicant_address_street";
    public static final String APPLICANT_ADDRESS_REGISTERED = "applicant_address_registered";
    public static final String APPLICANT_ADDRESS_PERIOD_STAY_MONTHS = "applicant_address_period_stay_months";
    public static final String APPLICANT_ADDRESS_PERIOD_STAY_YEARS = "applicant_address_period_stay_years";
    public static final String APPLICANT_ADDRESS_PERIOD_STAY = "applicant_address_period_stay";
    public static final String APPLICANT_PREVIOUS_ADDRESS_COUNTRY = "applicant_previous_address_country";
    public static final String APPLICANT_ADDRESS_PERMANENT = "applicant_address_permanent";
    public static final String APPLICANT_PERMANENT_ADDRESS_LINE_1 = "applicant_permanent_address_line1";
    public static final String APPLICANT_PERMANENT_ADDRESS_LINE_2 = "applicant_permanent_address_line2";
    public static final String APPLICANT_PERMANENT_ADDRESS_POSTAL_CODE = "applicant_permanent_address_postal_code";
    public static final String APPLICANT_PERMANENT_ADDRESS_COUNTRY = "applicant_permanent_address_country";
    public static final String APPLICANT_TAX_COUNTRY_PREFIX = "applicant_tax_country";
    public static final String APPLICANT_ADDRESS_NO_TAX_REASON = "applicant_address_no_tax_reason";
    public static final String APPLICANT_HSBC_CUSTOMER = "applicant_hsbc_customer";
    public static final String APPLICANT_PEP = "applicant_pep";
    public static final String APPLICANT_BO = "applicant_bo";
    public static final String BO_FIRST_NAME = "bo_first_name";
    public static final String BO_LAST_NAME = "bo_last_name";
    public static final String BO_NRIC = "bo_nric";
    public static final String BO_RELATION = "bo_relation";
    public static final String APPLICANT_ADDRESS_UNIT_FLOOR = "applicant_address_unit_floor";
    public static final String APPLICANT_ADDRESS_UNIT_NUM = "applicant_address_unit_num";
    public static final String APPLICANT_ADDRESS_BUILDING = "applicant_address_building";

    //dvt step 14
    public static final String CC_TYPE = "cc_type";
    public static final String CC_NAME = "cc_name";
    public static final String CC_NUM = "cc_num";
    public static final String CC_EXPIRY_MONTH = "cc_expiry_month";
    public static final String CC_EXPIRY_YEAR = "cc_expiry_year";
    public static final String APPLICANT_IDV_UPLOAD = "applicant_idv_upload";
    public static final String APPLICANT_ADDRESS_UPLOAD = "applicant_address_upload";
    public static final String APPLICANT_IDV_BACK_NRIC_UPLOAD = "applicant_idv_back_nric_upload";
    public static final String INSURED_IDV_BACK_NRIC_INSUDRED_UPLOAD = "insured_idv_back_nric_upload";
    public static final String APPLICANT_SECOND_IDV_UPLOAD="applicant_idv_two_upload"; // JIRA 661 Added for relevant field

    public static final String SUMMARY_MODAL = "summary_modal";
    public static final String SUMMARY_ANNUALLY = "summary_annually";
    public static final String SUMMARY_CI_MODAL = "summary_ci_modal";
    public static final String SUMMARY_CI_ANNUALLY = "summary_ci_annually";
    public static final String SUMMARY_TOTAL_MODAL = "summary_total_modal";
    public static final String SUMMARY_TOTAL_ANNUALLY = "summary_total_annually";
    
    //op and dvt retrieval form
    public static final String RETRIEVAL_EMAIL = "email";
    public static final String RETRIEVAL_VERIFY_CODE = "verify_code";

    //common to DVT and OP
    public static final String APPLICANT_RELEVANT_PASS_FRONT_UPLOAD="applicant_relevant_front_upload";
    public static final String APPLICANT_RELEVANT_PASS_FRONT_UPLOAD_DOCUMENT_TAG="ApplicantRelevantPassFront";
    public static final String APPLICANT_RELEVANT_PASS_BACK_UPLOAD="applicant_relevant_back_upload";
    public static final String APPLICANT_RELEVANT_PASS_BACK_UPLOAD_DOCUMENT_TAG="ApplicantRelevantPassBack";
    public static final String EMPTY_FILE_UPLOAD_MESSAGE="Please upload a file.";
    public static final String INSURED_RELEVANT_PASS_FRONT_UPLOAD="insured_relevant_front_upload";
    public static final String INSURED_RELEVANT_PASS_BACK_UPLOAD="insured_relevant_back_upload";
    public static final String INSURED_RELEVANT_PASS_FRONT_UPLOAD_DOCUMENT="InsuredRelevantPassFront";
    public static final String INSURED_RELEVANT_PASS_BACK_UPLOAD_DOCUMENT="InsuredRelevantPassBack";

    //op and dvt retrieval form
    public static final String RETRIEVAL_FORM_EMAIL = "applicant_email";

    public static String OP_APPLICATION_SESSION_ATTRIBUTE_NAME = "OnlineProtectorApplication";
    public static String DVT_APPLICATION_SESSION_ATTRIBUTE_NAME = "DirectValueTermApplication";
    public static String OP_PRODUCT_TYPE = "onlineprotector";
    public static String DVT_PRODUCT_TYPE = "directvalueterm";
    //credit card
    public static final String HSBC_CREDIT_CARD = "DCC";
    public static final String NON_HSBC_CREDIT_CARD = "NA";
    
    public static String RETRIEVE_TYPE = "retrieve_type";
    public static String RETRIEVE_PASS_CODE = "retrieve_pass_code";
    public static String RETRIEVE_FORM = "retrieve_form";
}
