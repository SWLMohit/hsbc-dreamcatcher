package sg.com.hsbc.dreamcatcher.eclaims.deathduetoillness;

import sg.com.hsbc.dreamcatcher.eclaims.DeathIndexProcessor;
import sg.com.hsbc.dreamcatcher.eclaims.LifeIndexProcessor;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/2-death-due-to-illness", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/2_death_due_to_illness/index.jsp")
})

@MultipartConfig
public class IndexServlet extends BaseServlet {
    private DeathIndexProcessor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new DeathIndexProcessor(this);
    }

    public float getCurrentStep() {
        return (float)0;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        boolean result = false;
        try {
            result = processor.process(request);
        }
        catch(Exception e) {
            processor.populateFormData(request);
        }
        return result;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processor.handleSuccess(request, response);
    }

    public void populateFormData(HttpServletRequest request) { processor.populateFormData(request); }

}
