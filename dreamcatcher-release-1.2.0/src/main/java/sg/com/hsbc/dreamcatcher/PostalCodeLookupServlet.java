package sg.com.hsbc.dreamcatcher;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import sg.com.hsbc.dreamcatcher.helpers.Config;
import sg.com.hsbc.dreamcatcher.security.XSSHandler;
import sg.com.hsbc.dreamcatcher.security.XSSHttpServletRequestWrapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.regex.Pattern;

@WebServlet(urlPatterns = "/lookup")
public class PostalCodeLookupServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();

        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair("APIKey", Config.getString("dreamcatcher.sglocate.api_key")));
        postParameters.add(new BasicNameValuePair("APISecret", Config.getString("dreamcatcher.sglocate.api_secret")));
        postParameters.add(new BasicNameValuePair("Postcode", request.getParameter("postalCode")));

        HttpPost req = new HttpPost("https://www.sglocate.com/api/json/searchwithpostcode.aspx");
        req.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
        HttpResponse res = httpClient.execute(req);

        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(res.getEntity().getContent()), 65728);
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        String santizedOutput=XSSHandler.cleanXSS(sb.toString());
        JSONObject obj = new JSONObject(santizedOutput);
        if (obj.getString("ErrorCode").equals("1")) {
            JSONArray results = obj.getJSONArray("Postcodes");
            JSONObject output = new JSONObject();
            output.put("buildingNumber", ((JSONObject)results.get(0)).getString("BuildingNumber"));
            output.put("streetName", ((JSONObject)results.get(0)).getString("StreetName"));
            output.put("buildingName", ((JSONObject)results.get(0)).getString("BuildingName"));

            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(output);
            out.flush();
            out.close();
        }
        // FIXME what happens if ErrorCode is not 1?
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
