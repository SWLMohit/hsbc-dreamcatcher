package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/feedback", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/feedback.jsp")
})
public class FeedbackServlet extends BaseServlet {

    public float getCurrentStep() {
        return 16;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

}
