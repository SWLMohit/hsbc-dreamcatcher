package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoillness;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/5-tpd-due-to-illness/1a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/5_tpd_due_to_illness/step1b.jsp")
})
public class Step1bServlet extends BaseServlet {
    public float getCurrentStep() {
        return (float)1.5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        String input = request.getParameter("illness_symptoms_start_date_day");
        sanitized.put("illness_symptoms_start_date_day", input);
        input = request.getParameter("illness_suffered");
        sanitized.put("illness_suffered", input);

        input = request.getParameter("disability_last_work_date_day");
        sanitized.put("disability_last_work_date_day", input);

        input = request.getParameter("disability_return_work_date_day");
        sanitized.put("disability_return_work_date_day", input);

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        TpdDueToIllnessApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        try {
            Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", data.get("illness_symptoms_start_date_day")));
            application.setIllnessSymptomsStart(date);
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }
        application.setIllnessSuffered(data.get("illness_suffered").toString());
        try {
            Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", data.get("disability_last_work_date_day")));
            application.setDisabilityLastWorkDate(date);
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }
        try {

         if(data.get("disability_return_work_date_day")!=null&&StringUtils.isNotBlank((String)data.get("disability_return_work_date_day"))){

            Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", data.get("disability_return_work_date_day")));
            application.setDisabilityReturnWorkDate(date);
         }
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }

        application.setMaxStep(getCurrentStep() + getStep());
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        TpdDueToIllnessApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        Date date = application.getIllnessSymptomsStart();
        if (date != null) {
            data.put("illness_symptoms_start_date_day", (new SimpleDateFormat("dd/MM/y")).format(date));
        }
        data.put("illness_suffered", application.getIllnessSuffered());
        date = application.getDisabilityLastWorkDate();
        if (date != null) {
            data.put("disability_last_work_date_day", (new SimpleDateFormat("dd/MM/y")).format(date));
        }
        date = application.getDisabilityReturnWorkDate();
        if (date != null) {
            data.put("disability_return_work_date_day", (new SimpleDateFormat("dd/MM/y")).format(date));
        }

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("day_list", getDayList());
        data.put("month_list", getMonthList());
        data.put("year_list", getYearList());
        data.put("future_year_list", getYearList(1970, Calendar.getInstance().get(Calendar.YEAR) + 30));

        request.setAttribute("formData", data);
    }

    protected float getStep() {
        return (float)0.4;
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(getBaseFormUrl() + "/1b");  // 1c
    }
}
