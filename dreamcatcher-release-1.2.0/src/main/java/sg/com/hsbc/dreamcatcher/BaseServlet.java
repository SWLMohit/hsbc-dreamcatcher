package sg.com.hsbc.dreamcatcher;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BaseServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        render(request, response);
    }

    /**
     * The URL to redirect to when the form gets processed.
     *
     * @return Next URL
     */
    public String getSuccessUrl() {
        return getServletContext().getContextPath() + getInitParameter("success_url");
    }

    /**
     * The URL to redirect to when the form gets processed.
     *
     * @return Next URL
     */
    public String getPreviousUrl() {
        return getInitParameter("previous_url");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        requestContext(req);

        super.service(req, resp);
    }

    /**
     * Add context for the template.
     *
     * @param request The http request
     */
    public void requestContext(HttpServletRequest request) {
    }

    public void render(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }
}
