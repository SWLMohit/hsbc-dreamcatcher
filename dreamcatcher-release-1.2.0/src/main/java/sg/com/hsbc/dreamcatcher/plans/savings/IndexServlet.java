package sg.com.hsbc.dreamcatcher.plans.savings;

import sg.com.hsbc.dreamcatcher.IndexContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/our-plans/savings/")
public class IndexServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        IndexContext indexContext = new IndexContext();
        indexContext.setTitle("My index title");
        request.setAttribute("context", indexContext);

        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/view/index.jsp");

        rd.forward(request, response);
    }
}
