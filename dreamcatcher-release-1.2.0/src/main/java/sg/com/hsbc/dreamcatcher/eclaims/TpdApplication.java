package sg.com.hsbc.dreamcatcher.eclaims;

import java.util.Date;
import javax.mail.MessagingException;

import org.apache.avalon.framework.logger.Loggable;

public class TpdApplication extends LifeApplication {
    protected String beforeOccupation;
    protected String beforeEmployerName;
    protected String beforeEmployerCity;
    protected String beforeEmployerPostalCode;
    protected String beforeEmployerBuildingNumber;
    protected String beforeEmployerUnitNumber1;
    protected String beforeEmployerUnitNumber2;
    protected String beforeEmployerStreet1;
    protected String beforeEmployerStreet2;
    protected String beforeEmployerBuildingName;
    protected Integer beforeSalary;
    protected String beforeWorkDuty;

    protected String afterOccupation;
    protected String afterEmployerName;
    protected String afterEmployerCity;
    protected String afterEmployerPostalCode;
    protected String afterEmployerBuildingNumber;
    protected String afterEmployerUnitNumber1;
    protected String afterEmployerUnitNumber2;
    protected String afterEmployerStreet1;
    protected String afterEmployerStreet2;
    protected String afterEmployerBuildingName;
    protected Integer afterSalary;
    protected String afterWorkDuty;

    protected String illnessSuffered;
    protected Date disabilityLastWorkDate;
    protected Date disabilityReturnWorkDate;

    public String getBeforeOccupation() {
        return beforeOccupation;
    }

    public void setBeforeOccupation(String beforeOccupation) {
        this.beforeOccupation = beforeOccupation;
    }

    public String getBeforeEmployerName() {
        return beforeEmployerName;
    }

    public void setBeforeEmployerName(String beforeEmployerName) {
        this.beforeEmployerName = beforeEmployerName;
    }

    public String getBeforeEmployerCity() {
        return beforeEmployerCity;
    }

    public void setBeforeEmployerCity(String beforeEmployerCity) {
        this.beforeEmployerCity = beforeEmployerCity;
    }

    public String getBeforeEmployerPostalCode() {
        return beforeEmployerPostalCode;
    }

    public void setBeforeEmployerPostalCode(String beforeEmployerPostalCode) {
        this.beforeEmployerPostalCode = beforeEmployerPostalCode;
    }

    public String getBeforeEmployerBuildingNumber() {
        return beforeEmployerBuildingNumber;
    }

    public void setBeforeEmployerBuildingNumber(String beforeEmployerBuildingNumber) {
        this.beforeEmployerBuildingNumber = beforeEmployerBuildingNumber;
    }

    public String getBeforeEmployerUnitNumber1() {
        return beforeEmployerUnitNumber1;
    }

    public void setBeforeEmployerUnitNumber1(String beforeEmployerUnitNumber1) {
        this.beforeEmployerUnitNumber1 = beforeEmployerUnitNumber1;
    }

    public String getBeforeEmployerUnitNumber2() {
        return beforeEmployerUnitNumber2;
    }

    public void setBeforeEmployerUnitNumber2(String beforeEmployerUnitNumber2) {
        this.beforeEmployerUnitNumber2 = beforeEmployerUnitNumber2;
    }

    public String getBeforeEmployerStreet1() {
        return beforeEmployerStreet1;
    }

    public void setBeforeEmployerStreet1(String beforeEmployerStreet1) {
        this.beforeEmployerStreet1 = beforeEmployerStreet1;
    }

    public String getBeforeEmployerStreet2() {
        return beforeEmployerStreet2;
    }

    public void setBeforeEmployerStreet2(String beforeEmployerStreet2) {
        this.beforeEmployerStreet2 = beforeEmployerStreet2;
    }

    public String getBeforeEmployerBuildingName() {
        return beforeEmployerBuildingName;
    }

    public void setBeforeEmployerBuildingName(String beforeEmployerBuildingName) {
        this.beforeEmployerBuildingName = beforeEmployerBuildingName;
    }

    public Integer getBeforeSalary() {
        return beforeSalary;
    }

    public void setBeforeSalary(Integer beforeSalary) {
        this.beforeSalary = beforeSalary;
    }

    public String getBeforeWorkDuty() {
        return beforeWorkDuty;
    }

    public void setBeforeWorkDuty(String beforeWorkDuty) {
        this.beforeWorkDuty = beforeWorkDuty;
    }

    public String getAfterOccupation() {
        return afterOccupation;
    }

    public void setAfterOccupation(String afterOccupation) {
        this.afterOccupation = afterOccupation;
    }

    public String getAfterEmployerName() {
        return afterEmployerName;
    }

    public void setAfterEmployerName(String afterEmployerName) {
        this.afterEmployerName = afterEmployerName;
    }

    public String getAfterEmployerCity() {
        return afterEmployerCity;
    }

    public void setAfterEmployerCity(String afterEmployerCity) {
        this.afterEmployerCity = afterEmployerCity;
    }

    public String getAfterEmployerPostalCode() {
        return afterEmployerPostalCode;
    }

    public void setAfterEmployerPostalCode(String afterEmployerPostalCode) {
        this.afterEmployerPostalCode = afterEmployerPostalCode;
    }

    public String getAfterEmployerBuildingNumber() {
        return afterEmployerBuildingNumber;
    }

    public void setAfterEmployerBuildingNumber(String afterEmployerBuildingNumber) {
        this.afterEmployerBuildingNumber = afterEmployerBuildingNumber;
    }

    public String getAfterEmployerUnitNumber1() {
        return afterEmployerUnitNumber1;
    }

    public void setAfterEmployerUnitNumber1(String afterEmployerUnitNumber1) {
        this.afterEmployerUnitNumber1 = afterEmployerUnitNumber1;
    }

    public String getAfterEmployerUnitNumber2() {
        return afterEmployerUnitNumber2;
    }

    public void setAfterEmployerUnitNumber2(String afterEmployerUnitNumber2) {
        this.afterEmployerUnitNumber2 = afterEmployerUnitNumber2;
    }

    public String getAfterEmployerStreet1() {
        return afterEmployerStreet1;
    }

    public void setAfterEmployerStreet1(String afterEmployerStreet1) {
        this.afterEmployerStreet1 = afterEmployerStreet1;
    }

    public String getAfterEmployerStreet2() {
        return afterEmployerStreet2;
    }

    public void setAfterEmployerStreet2(String afterEmployerStreet2) {
        this.afterEmployerStreet2 = afterEmployerStreet2;
    }

    public String getAfterEmployerBuildingName() {
        return afterEmployerBuildingName;
    }

    public void setAfterEmployerBuildingName(String afterEmployerBuildingName) {
        this.afterEmployerBuildingName = afterEmployerBuildingName;
    }

    public Integer getAfterSalary() {
        return afterSalary;
    }

    public void setAfterSalary(Integer afterSalary) {
        this.afterSalary = afterSalary;
    }

    public String getAfterWorkDuty() {
        return afterWorkDuty;
    }

    public void setAfterWorkDuty(String afterWorkDuty) {
        this.afterWorkDuty = afterWorkDuty;
    }

    public String getIllnessSuffered() {
        return illnessSuffered;
    }

    public void setIllnessSuffered(String illnessSuffered) {
        this.illnessSuffered = illnessSuffered;
    }

    public Date getDisabilityLastWorkDate() {
        return disabilityLastWorkDate;
    }

    public void setDisabilityLastWorkDate(Date disabilityLastWorkDate) {
        this.disabilityLastWorkDate = disabilityLastWorkDate;
    }

    public Date getDisabilityReturnWorkDate() {
        return disabilityReturnWorkDate;
    }

    public void setDisabilityReturnWorkDate(Date disabilityReturnWorkDate) {
        this.disabilityReturnWorkDate = disabilityReturnWorkDate;
    }


    public void sendEmail(){
        try {
			super.sendEmail(getPolicyownerEmail(), EMAIL_TEMPLATE_ILLNESS, getPDFFilename(false));
		} catch (MessagingException e) {
			
		}

    }
}
