package sg.com.hsbc.dreamcatcher;

public abstract class ApplicationForm {
    public static final int STATUS_INCOMPLETE = 0;
    public static final int STATUS_COMPLETE = 1;

    protected int status;
    protected float maxStep;
    protected String dropoutPassword;

    public float getMaxStep() {
        return maxStep;
    }

    public void setMaxStep(float maxStep) {
        if (this.maxStep < maxStep) {
            this.maxStep = maxStep;
        }
    }

    public abstract boolean save();
    public int getStatus() { return status; }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDropoutPassword() {
        return dropoutPassword;
    }

    public void setDropoutPassword(String dropoutPassword) {
        this.dropoutPassword = dropoutPassword;
    }
}
