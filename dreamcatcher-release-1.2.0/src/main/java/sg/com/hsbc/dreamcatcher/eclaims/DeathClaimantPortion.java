package sg.com.hsbc.dreamcatcher.eclaims;

import java.util.List;

public class DeathClaimantPortion {
    protected String firstName;
    protected String lastName;
    protected String relationship;
    protected List<String> relationshipDocument;
    protected String city;
    protected String postalCode;
    protected String buildingNumber;
    protected String unitNumber1;
    protected String unitNumber2;
    protected String street1;
    protected String street2;
    protected String buildingName;
    protected List<String> addressDocument;

    public String getUnitNumber1() {
        return unitNumber1;
    }

    public void setUnitNumber1(String unitNumber1) {
        this.unitNumber1 = unitNumber1;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public List<String> getRelationshipDocument() {
        return relationshipDocument;
    }

    public void setRelationshipDocument(List<String> list) {
        this.relationshipDocument = list;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getUnitNumber2() {
        return unitNumber2;
    }

    public void setUnitNumber2(String unitNumber2) {
        this.unitNumber2 = unitNumber2;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public List<String> getAddressDocument() {
        return addressDocument;
    }

    public void setAddressDocument(List<String> list) {
        this.addressDocument = list;
    }
}
