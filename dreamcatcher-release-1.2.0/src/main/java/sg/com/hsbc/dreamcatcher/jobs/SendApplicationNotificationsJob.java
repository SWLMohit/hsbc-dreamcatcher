package sg.com.hsbc.dreamcatcher.jobs;

import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.DirectValueTermApplication;
import sg.com.hsbc.dreamcatcher.plans.onlineprotector.OnlineProtectorApplication;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SendApplicationNotificationsJob implements Runnable {

    @Override
    public void run() {
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT id, product_type FROM applications WHERE status = ? AND notify_status = ?");
            stmt.setInt(1, 1);
            stmt.setInt(2, 0);

            rs = stmt.executeQuery();
            if (rs.next()) {
                String id = rs.getString("id");
                String productType = rs.getString("product_type");

                if (id != null && !id.isEmpty()) {
                    switch (productType) {
                        case OnlineProtectorApplication.PRODUCT_TYPE:
                            OnlineProtectorApplication opa = OnlineProtectorApplication.getById(id);
                            opa.setNotifyStatus(OnlineProtectorApplication.NOTIFY_STATUS_PROCESSING);
                            opa.save();

                           /* opa.sendNotifications();
                            opa.setNotifyStatus(OnlineProtectorApplication.NOTIFY_STATUS_SENT);
                            opa.save();*/
                            break;
                        case DirectValueTermApplication.PRODUCT_TYPE:
                            DirectValueTermApplication dva = DirectValueTermApplication.getById(id);
                            dva.setNotifyStatus(DirectValueTermApplication.NOTIFY_STATUS_PROCESSING);
                            dva.save();

                           /* dva.sendNotifications();
                            dva.setNotifyStatus(DirectValueTermApplication.NOTIFY_STATUS_SENT);
                            dva.save();*/
                            break;
                    }
                }
            }
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }
    }
}
