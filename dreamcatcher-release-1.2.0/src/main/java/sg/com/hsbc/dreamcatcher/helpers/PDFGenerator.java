package sg.com.hsbc.dreamcatcher.helpers;

import be.quodlibet.boxable.*;
import be.quodlibet.boxable.line.LineStyle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDBorderStyleDictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PDFGenerator {
    private static Logger logger = LoggerFactory.getLogger(PDFGenerator.class);
    public static final int PDF_TYPE_DUMMY = 0;
    public static final int PDF_TYPE_BENEFIT_ILLUSTRATION = 1;
    public static final int PDF_TYPE_APPLICATION = 2;
    public static final int PDF_TYPE_ECLAIMS = 3;

    public static final int PAGE_MARGIN_TOP = 60;
    public static final int PAGE_MARGIN_RIGHT = 60;
    public static final int PAGE_MARGIN_BOTTOM = 100;
    public static final int PAGE_MARGIN_LEFT = 60;

    public static final int PAGE_CONTENT_AREA_MARGIN_TOP = PAGE_MARGIN_TOP + 60;

    public static final int FONT_NORMAL = 0;
    public static final int FONT_BOLD = 1;
    public static final int FONT_ARIAL = 3;

    public static final int TABLE_ROW_TYPE_SUMMARY = 1;
    public static final int TABLE_ROW_TYPE_SUMMARY2 = 2;
    public static final int TABLE_ROW_TYPE_SUMMARY3 = 3;
    public static final int TABLE_ROW_TYPE_SUMMARY4 = 4;
    public static final int TABLE_ROW_TYPE_CLAIMS = 5;
    public static final int TABLE_ROW_TYPE_CLAIMS_SINGLE = 6;
   
    /** user space units per millimeter */
    private static final float POINTS_PER_INCH = 72;
    private static final float POINTS_PER_MM = 1 / (10 * 2.54f) * POINTS_PER_INCH;

    private PDDocument document;
    private PDPage currentPage;
    private String fileName;
    private PDFont font;
    private PDFont builtInFont;
    private int redColor=0;
    private float fontSize;
    private float scalingFactor=0;
    private float rotationFactor=0;
    public float getRotationFactor() {
		return rotationFactor;
	}

	public void setRotationFactor(float rotationFactor) {
		this.rotationFactor = rotationFactor;
	}

	private Date dateGenerated = null;
    private int documentType;

    public PDFGenerator(String fileName, int type) {
        this.fileName = fileName;
        this.document = new PDDocument();
        dateGenerated = new Date();
        documentType = type;
    }

    public PDPage createPage() {
        PDPage page = new PDPage(new PDRectangle(612, 297 * POINTS_PER_MM)); //Custom Size of PDF file 
        //PDPage page = new PDPage(PDRectangle.A4); //Use this code for A4 standard
        return page;
    }

    public PDPage addPage(PDPage page) {
        document.addPage(page);
        setCurrentPage(page);

        return page;
    }

    public PDPage applyPageHeader(PDPage page) throws IOException {
        if (documentType == PDF_TYPE_BENEFIT_ILLUSTRATION) {
            String text = "HSBC Insurance (Singapore) Pte. Limited (Reg. No. 195400150N)\n" +
                    "21 Collyer Quay #02-01 Singapore 049320, Monday to Friday 9.30 am to 5 pm. insuranceonline.hsbc.com.sg\n" +
                    "Customer Care Hotline: (65) 6225 6111 Fax: (65) 6221 2188\n" +
                    "Mailing address: Robinson Road Post Office P.O. BOX 1538 Singapore 903038";
            setFont(FONT_NORMAL, 9);
            page = writeText(page, text, PAGE_MARGIN_LEFT, page.getMediaBox().getHeight() - PAGE_MARGIN_TOP);

            //fixme replace 1.0 with <Quotation Version>
            page = writeText(page, "1.0", page.getMediaBox().getWidth() - PAGE_MARGIN_RIGHT - 20, page.getMediaBox().getHeight() - PAGE_MARGIN_TOP - 35);
        }

        return page;
    }

    public PDPage applyPageHeader(PDPage page, PDDocument doc) throws IOException {
        if (documentType == PDF_TYPE_ECLAIMS||documentType== PDF_TYPE_APPLICATION) {
            try {
                String filePath = this.getClass().getResource("/images/logo.png").getPath();
                PDImageXObject pdImage = PDImageXObject.createFromFile(filePath, doc);
                try (PDPageContentStream contents = new PDPageContentStream(doc, page,AppendMode.APPEND,true,false)) {
                    contents.drawImage(pdImage, PAGE_MARGIN_LEFT, page.getMediaBox().getHeight() - PAGE_MARGIN_TOP, 118, 45);
                }
            } catch (IOException e) {
                logger.info("apply Header got error {}",e);
            }
        } else {
            return applyPageHeader(page);
        }
        return page;
    }

    public PDPage applyPageFooter(PDPage page, int pageNum) throws IOException {
        int footerPageMarginBottom = PAGE_MARGIN_BOTTOM - 10;
        if (documentType == PDF_TYPE_BENEFIT_ILLUSTRATION) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm aa");
            String text = "Date Generated: " + sdf.format(dateGenerated) + "\r\n" +
                    "This document is issued by HSBC Insurance (Singapore) Pte. Limited";
            setFont(FONT_NORMAL, 9);
            page = writeText(page, text, PAGE_MARGIN_LEFT, footerPageMarginBottom);

            page = writeText(page, "Page " + pageNum + " of " + (getDocument().getNumberOfPages()), page.getMediaBox().getWidth() - PAGE_MARGIN_RIGHT - 33, footerPageMarginBottom);
            page = writeText(page, "\r\nE. & OE", page.getMediaBox().getWidth() - PAGE_MARGIN_RIGHT - 20, footerPageMarginBottom);
        }else if(documentType==PDF_TYPE_APPLICATION){
        	page = writeText(page, "Page " + pageNum + " of " + (getDocument().getNumberOfPages()), page.getMediaBox().getWidth() - page.getMediaBox().getWidth()/2-33 , 20);
        }

        return page;
    }

    public PDPage writeText(PDPage page, String text, float ty) throws IOException {
        return writeText(page, text, PAGE_MARGIN_LEFT, ty);
    }

    public PDPage writeTextWithUnderline(PDPage page, String text, float tx, float ty) {
        String lines[] = text.split("\\r?\\n");
        try {
            PDPageContentStream cs = getContentStream(page);
            cs.beginText();
            cs.newLineAtOffset(tx, ty);
            cs.setLeading(12f);
            cs.setFont(getFont(), fontSize);
            for (int i = 0; i < lines.length; i++) {
                if (i > 0) {
                    cs.newLine();
                    // create a link annotation
                    PDAnnotationLink txtLink = new PDAnnotationLink();
                    // add an underline
                    PDBorderStyleDictionary underline = new PDBorderStyleDictionary();
                    underline.setStyle(PDBorderStyleDictionary.STYLE_UNDERLINE);
                    txtLink.setBorderStyle(underline);
                }
                cs.showText(lines[i]);
            }
            cs.endText();
            cs.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return page;
    }

    public PDPage writeText(PDPage page, String text, float tx, float ty) throws IOException {
        String lines[] = text.split("\\r?\\n");

        PDPageContentStream cs = getContentStream(page);
        try {
        	if(this.getRedColor()==255){
        		cs.setNonStrokingColor(Color.RED);
        	}else{
        		cs.setNonStrokingColor(Color.BLACK);
        	}
        	//cs.setStrokingColor(Color.RED);

            cs.beginText();
            try{
            if(rotationFactor>0){
            	System.out.println("the rottion is now called for text "+text);
           	}
            }catch(Exception e){
            	System.out.println("the error in rotation "+e);
            	e.printStackTrace();
            }
            cs.newLineAtOffset(tx, ty);
            cs.setLeading(12f);
            cs.setFont(getFont(), fontSize);
            for (int i = 0; i < lines.length; i++) {
                if (i > 0) {
                    cs.newLine();
                }
                cs.showText(lines[i]);
            }

            if(getScalingFactor()>0) {
              cs.setTextMatrix(Matrix.getScaleInstance(3, 0)); 
            }
            
            System.out.println("the scaling is now done");

          } catch (Exception e) {
            System.out.println("error in writing text "+e);
            e.printStackTrace();
        } finally {
            if (cs != null) {
                cs.endText();
                cs.close();
            }
        }
      return page;
    }
    
    
    
    public PDPage drawRectangle(PDPage page, float left ,float top,float height,float width){
    	PDPageContentStream cs = null;
    	try{
            cs = getContentStream(page);
    		cs.setNonStrokingColor(Color.TRANSLUCENT);
    		if(getRedColor()>0){
    		cs.setStrokingColor(Color.RED);
    		}else{
    			cs.setStrokingColor(Color.BLACK);
    		}
    		cs.setLineWidth(0.75f);
    		cs.addRect(left, top, width, height);
    		
    		cs.stroke();
    		cs.fill();
    	}catch(Exception e){
    		System.out.println("the error is thrown in creating rectangle"+e);
    	}finally{
    		try {
				if (cs != null) {
				    cs.close();
                }
			} catch (IOException e) {
				logger.error("IOException occurred in writeText " + e);
			}
    	}
    	return null;
    	
    }
    public void addTableHeader(BaseTable table, String title) throws IOException {
        Row<PDPage> row = table.createRow(9);

        Cell cell = row.createCell(100, title, HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
        cell.setFillColor(Color.decode("#a2c2e3"));
        cell.setBottomPadding(0);
        cell.setTopPadding(0);
        setFont(PDFGenerator.FONT_BOLD);
        cell.setFont(getFont());

        table.addHeaderRow(row);
    }

    /**
     * Draws the table on the pdf based on the String array
     * provided which contains the data for the columns
     * of a row
     * 
     * @param table  the table to be drawn on the pdf
     * @param cols   the String array to be written in the row of table
     * @param type   type of table to be created
     * @throws IOException
     */
    public void addTableRow(BaseTable table, String[] cols, int type) throws IOException {
        Row<PDPage> row = table.createRow(12);
        Cell cell;

        switch (type) {
            case TABLE_ROW_TYPE_SUMMARY:
                for (int i = 0; i < cols.length; i++) {
                    if (i == 0) {
                        setFont(PDFGenerator.FONT_BOLD);
                        cell = row.createCell(20, cols[i]);
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);

                        setFont(PDFGenerator.FONT_NORMAL);
                        cell = row.createCell(3, (!cols[i].isEmpty() ? ":" : ""));
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);
                    } else {
                        setFont(PDFGenerator.FONT_NORMAL);
                        cell = row.createCell(30, cols[i]);
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);
                    }
                }
                break;

            case TABLE_ROW_TYPE_SUMMARY2:
                for (int i = 0; i < cols.length; i++) {
                    if (i == 0) {
                        setFont(PDFGenerator.FONT_BOLD);
                        cell = row.createCell(20, cols[i]);
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);

                        setFont(PDFGenerator.FONT_NORMAL);
                        cell = row.createCell(3, (!cols[i].isEmpty() ? ":" : ""));
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);
                    } else {
                        setFont(PDFGenerator.FONT_NORMAL);
                        cell = row.createCell(34, cols[i]);
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);

                        if (i == cols.length - 1) {
                            for (int j = 0; j < 3; j++) {
                                if (j == 2) {
                                    cell = row.createCell(17, "");
                                } else {
                                    cell = row.createCell(13, "");
                                }
                                cell.setFont(getFont());
                                cell.setBottomPadding(0);
                                cell.setTopPadding(0);
                            }
                        }
                    }
                }
                break;

            case TABLE_ROW_TYPE_SUMMARY3:
                for (int i = 0; i < cols.length; i++) {
                    if (i == 0) {
                        setFont(PDFGenerator.FONT_BOLD);
                        cell = row.createCell(20, cols[i]);
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);

                        setFont(PDFGenerator.FONT_NORMAL);
                        cell = row.createCell(3, (!cols[i].isEmpty() ? ":" : ""));
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);
                    } else {
                        setFont(PDFGenerator.FONT_BOLD);
                        if (i == 1) {
                            cell = row.createCell(34, cols[i]);
                        } else {
                            if (i == cols.length - 1) {
                                cell = row.createCell(17, cols[i]);
                            } else {
                                cell = row.createCell(13, cols[i]);
                            }
                        }
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);
                    }
                }
                break;

            case TABLE_ROW_TYPE_SUMMARY4:
                for (int i = 0; i < cols.length; i++) {
                    if (i == 0) {
                        setFont(PDFGenerator.FONT_NORMAL);
                        cell = row.createCell(20, "");
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);

                        setFont(PDFGenerator.FONT_NORMAL);
                        cell = row.createCell(3, "");
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);

                        setFont(PDFGenerator.FONT_NORMAL);
                        cell = row.createCell(34, cols[i]);
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);
                    } else {
                        setFont(PDFGenerator.FONT_NORMAL);
                        if (i == cols.length - 1) {
                            cell = row.createCell(17, cols[i]);
                        } else {
                            cell = row.createCell(13, cols[i]);
                        }
                        cell.setFont(getFont());
                        cell.setBottomPadding(0);
                        cell.setTopPadding(0);
                    }
                }
                break;

            case TABLE_ROW_TYPE_CLAIMS:
                setFont(PDFGenerator.FONT_BOLD);
                if(cols.length==1){
                	cell = row.createCell(100, cols[0]);
                	cell.setFont(getFont());
                    cell.setBottomPadding(0);
                    cell.setTopPadding(0);
                    cell.setAlign(HorizontalAlignment.CENTER);
                }
                if(cols.length==2){
                	boolean noBorder=false;
                	boolean noBorderLast=false;
                	
                	if(StringUtils.startsWithIgnoreCase(cols[0], "<no-border>")){
                    	cols[0]=StringUtils.replace(cols[0], "<no-border>", "");
                    	noBorder=true;
                    }
                	if(StringUtils.startsWithIgnoreCase(cols[0], "<no-border-last>")){
                		cols[0]=StringUtils.replace(cols[0], "<no-border-last>", "");
                		noBorderLast=true;
                	}
                cell = row.createCell(40, cols[0]);
                cell.setFont(getFont());
                cell.setBottomPadding(0);
                cell.setTopPadding(0);
                if(noBorder){
                	cell.setBottomBorderStyle(null);
                	cell.setTopBorderStyle(null);
                }else if(noBorderLast){
                	cell.setTopBorderStyle(null);
                }
                

                setFont(PDFGenerator.FONT_NORMAL);
               
                cell = row.createCell(60, cols[1]);
                cell.setFont(getFont());
                cell.setBottomPadding(0);
                cell.setTopPadding(0);
                if(noBorder){
                	cell.setBottomBorderStyle(null);
                	cell.setTopBorderStyle(null);
                }else if(noBorderLast){
                	cell.setTopBorderStyle(null);
                }
                }
                break;

            case TABLE_ROW_TYPE_CLAIMS_SINGLE:
                setFont(PDFGenerator.FONT_NORMAL);
                cell = row.createCell(100, cols[0]);
                cell.setFont(getFont());
                cell.setBottomPadding(0);
                cell.setTopPadding(0);
                break;
        }
    }

    public void save() throws IOException {
        document.save(fileName);
    }

    public void close() throws IOException {
        document.close();
    }

    public void protect(String password) throws IOException {
        AccessPermission ap = new AccessPermission();
        StandardProtectionPolicy spp = new StandardProtectionPolicy(password, password, ap);
        spp.setEncryptionKeyLength(128);
        spp.setPermissions(ap);
        document.protect(spp);
    }

    public PDDocument getDocument() {
        return document;
    }

    public void setDocument(PDDocument document) {
        this.document = document;
    }

    public PDPage getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(PDPage currentPage) {
        this.currentPage = currentPage;
    }

    public PDPageContentStream getContentStream(PDPage page) throws IOException {
        return new PDPageContentStream(getDocument(), page, PDPageContentStream.AppendMode.APPEND, false);
    }

    public PDFont getFont() throws IOException {
        return (builtInFont==null?(font == null ? getDefaultFont() : font):getBuiltInFont());
    }

    public void setFont(int type) throws IOException {
        setFont(type, getDefaultFontSize());
    }

    public void setFont(int type, float size) throws IOException {
        String path = null;
        switch (type) {
            case FONT_BOLD:
                path = "/fonts/Calibri Bold.ttf";
                break;
            case FONT_ARIAL:
            	path="/fonts/Arial.ttf";
            case FONT_NORMAL:
            default:
                path = "/fonts/Calibri.ttf";
                break;
        }

        if (path != null) {
            FileInputStream fis = null;
            try {
                File fontFile = new File(this.getClass().getResource(path).toURI());
                fis = new FileInputStream(fontFile);
                this.font = PDType0Font.load(document, fis);
            } catch (Exception e) {
                logger.error("error in  loading font "+e);

                logger.error("setFont:",e);
            } finally {
                try {
                    if (fis!=null)
                        fis.close();
                } catch (IOException e) {
                    logger.error("setFont:",e);
                }
            }
        } else {
            this.font = PDType1Font.HELVETICA;
        }

        fontSize = size;
    }

    public PDFont getBuiltInFont() {
		return builtInFont;
	}

	public void setBuiltInFont(PDFont builtInFont, float i) {
		this.builtInFont = builtInFont;
		this.fontSize=i;
	}
	
	
	/**
	 * Gets the font size which is set for pdf
	 * @return float the font size
	 */
	public float fontSize(){
		return fontSize;
		
	}
    public PDFont getDefaultFont() throws IOException {
        PDFont font = PDType1Font.HELVETICA;
        FileInputStream fis = null;
        try {
            File fontFile = new File(this.getClass().getResource("/fonts/Calibri.ttf").toURI());
            fis = new FileInputStream(fontFile);
            font = PDType0Font.load(document, fis);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            try {
                if (fis!=null)
                    fis.close();
            } catch (IOException e) {
                logger.error("setFont:",e);
            }
        }

        return font;
    }

    public float getDefaultFontSize() {
        return 9;
    }

	public int getRedColor() {
		return redColor;
	}

	public void setRedColor(int redColor) {
		this.redColor = redColor;
	}

	public float getScalingFactor() {
		return scalingFactor;
	}

	public void setScalingFactor(float scalingFactor) {
		this.scalingFactor = scalingFactor;
	}
}
