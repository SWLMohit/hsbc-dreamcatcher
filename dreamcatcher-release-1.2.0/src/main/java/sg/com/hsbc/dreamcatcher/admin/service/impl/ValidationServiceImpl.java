package sg.com.hsbc.dreamcatcher.admin.service.impl;

import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.repository.ApplicationsRepository;
import sg.com.hsbc.dreamcatcher.admin.service.ValidationService;

import java.util.List;

public class ValidationServiceImpl implements ValidationService {

    ApplicationsRepository applicationsRepository;
    @Override
    public boolean validateMobileNo(String type, String countryCode, String mobileNo, int status) {
        if (type==null || mobileNo ==null) {
            return false;
        }
        applicationsRepository = new ApplicationsRepository();
        List<Applications> applicationsList = applicationsRepository.findByMobileNo(type, countryCode, mobileNo, status);
        if (applicationsList.size() > 0){
            return true;
        }
        return false;
    }
    @Override
    public boolean validateEmail(String type, String email, int status) {
        if (type==null || email==null) {
            return false;
        }
        applicationsRepository = new ApplicationsRepository();
        List<Applications> applicationsList = applicationsRepository.findByEmail(type, email, status);
        if (applicationsList.size() > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean validateMobileNoAndEmail(String type, String countryCode, String mobileNo, String email) {
        if (type==null || mobileNo ==null || email ==null) {
            return false;
        }
        applicationsRepository = new ApplicationsRepository();
        List<Applications> applicationsList = applicationsRepository.findByMobileNoAndEmail(type, 1, countryCode, mobileNo, email);
        if (applicationsList.size() > 0){
            return true;
        }
        return false;
    }
}
