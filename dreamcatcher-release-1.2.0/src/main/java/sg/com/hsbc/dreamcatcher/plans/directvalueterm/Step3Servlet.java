package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.helpers.*;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/3", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step3.jsp")
})
public class Step3Servlet extends BaseServlet {
	
	private final String DOB_FORMAT = "dd/MM/yyyy";

    public float getCurrentStep() {
        return 3;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizeApplicantResidency(request, sanitized);
        sanitized = sanitizeApplicantFirstName(request, sanitized);
        sanitized = sanitizeApplicantLastName(request, sanitized);
        sanitized = sanitizeApplicantDob(request, sanitized);
        sanitized = sanitizeApplicantMobileNumber(request, sanitized);
        sanitized = sanitizeApplicantEmail(request, sanitized);
        sanitized = sanitizeApplicantGender(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();

        errors = validateApplicantResidency(data, errors);
        errors = validateApplicantFirstName(data, errors);
        errors = validateApplicantLastName(data, errors);
        errors = validateApplicantDob(data, errors);
        errors = validateApplicantGender(data, errors);
        errors = validateApplicantMobileNumber(data, errors);
        errors = validateApplicantEmail(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setLifeInsured("self");
        application.setApplicantResidency(data.get(FieldConstants.APPLICANT_RESIDENCY).toString());
        application.setApplicantFirstName(data.get(FieldConstants.APPLICANT_FIRST_NAME).toString());
        application.setApplicantLastName(data.get(FieldConstants.APPLICANT_LAST_NAME).toString());
        application.setApplicantMobileCountryCode(data.get(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE).toString());
        application.setApplicantMobileNum(data.get(FieldConstants.APPLICANT_MOBILE_NUM).toString());
        application.setApplicantEmail(data.get(FieldConstants.APPLICANT_EMAIL).toString());
        application.setApplicantGender(data.get(FieldConstants.APPLICANT_GENDER).toString());
        application.setInsuredResidency(null);
        application.setInsuredFirstName(null);
        application.setInsuredLastName(null);
        application.setInsuredDob(null);
        try {
            Date applicantDob = (new SimpleDateFormat(DOB_FORMAT)).parse(String.format("%s", data.get(FieldConstants.APPLICANT_DOB)));
            application.setApplicantDob(applicantDob);
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateApplicantResidency(Map<String, Object> data, Map<String, String> errors) {
        String input = (data.get(FieldConstants.APPLICANT_RESIDENCY) != null ? data.get(FieldConstants.APPLICANT_RESIDENCY).toString() : "");
        boolean hasError = false;
        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_RESIDENCY, StringConstants.CURRENT_RESIDENCY_STATUS_MISSING_OR_INVALID);
                hasError = true;
                break;
        }

        if (!hasError) {
            if (input.equalsIgnoreCase("NA")) {
                errors.put(FieldConstants.APPLICANT_RESIDENCY, StringConstants.CURRENT_RESIDENCY_STATUS_INVALID);
                errors.put("dropout", "1");

            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantFirstName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.APPLICANT_FIRST_NAME))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_FIRST_NAME, StringConstants.GIVEN_NAME_MISSING);
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.APPLICANT_FIRST_NAME, StringConstants.GIVEN_NAME_TOO_LONG);
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_FIRST_NAME, StringConstants.GIVEN_NAME_CONTAINS_INVALID_CHARACTERS);
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantLastName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.APPLICANT_LAST_NAME))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_LAST_NAME, StringConstants.SURNAME_MISSING);
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.APPLICANT_LAST_NAME, StringConstants.SURNAME_TOO_LONG);
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_LAST_NAME, StringConstants.SURNAME_CONTAINS_INVALID_CHARACTERS);
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantDob(Map<String, Object> data, Map<String, String> errors) {
        boolean hasError = false;
        switch (Validator.validateDate((String)data.get(FieldConstants.APPLICANT_DOB))) {
            case Validator.ERROR_DATE_EMPTY:
                errors.put(FieldConstants.APPLICANT_DOB, StringConstants.EMPTY_DATE);
                hasError = true;
                break;
            case Validator.ERROR_DATE_INVALID:
                errors.put(FieldConstants.APPLICANT_DOB, StringConstants.DATE_OF_BIRTH_INVALID);
                hasError = true;
                break;
        }

        if (!hasError) {
            if (data.get(FieldConstants.LIFE_INSURED) == null || data.get(FieldConstants.LIFE_INSURED).toString().equals("self")) {
                String dateString = String.format("%s", data.get(FieldConstants.APPLICANT_DOB));
                DateFormat df = new SimpleDateFormat(DOB_FORMAT);
                try {
                    int anb = DirectValueTermApplication.calculateAgeNextBirthday(df.parse(dateString));
                    // FIXME: anb minimum should be 19?
                    // ticket 607 for the 20 and age65 plan max age is 60, 5years is 85
                    if (anb < 19 || anb > 62) {
                        errors.put(FieldConstants.APPLICANT_DOB, StringConstants.UNABLE_TO_PROCEED_WITH_ONLINE_APPLICATION);
                        errors.put("dropout", "1");
                    }
                } catch (Exception e) {
                    // FIXME: Copy (may be) wrong, the date has already been valid, this is an exception handler
                    errors.put(FieldConstants.APPLICANT_DOB, StringConstants.DATE_OF_BIRTH_INVALID);
                }
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantGender(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_GENDER))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_GENDER, StringConstants.GENDER_MISSING_OR_INVALID);
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantMobileNumber(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validatePhoneCountryCode(data.get(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, "Your mobile phone country code is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, "Your mobile phone country code is too long.");
                break;
            case Validator.ERROR_PHONE_COUNTRY_CODE_INVALID:
                errors.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, "Your mobile phone country code is invalid.");
                break;
        }

        switch (Validator.validatePhoneNumber(data.get(FieldConstants.APPLICANT_MOBILE_NUM))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_MOBILE_NUM, "Your mobile phone number is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.APPLICANT_MOBILE_NUM, "Your mobile phone number is too long.");
                break;
            case Validator.ERROR_PHONE_NUMBER_INVALID:
                errors.put(FieldConstants.APPLICANT_MOBILE_NUM, "Your mobile phone number is invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantEmail(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateEmail(data.get(FieldConstants.APPLICANT_EMAIL))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_EMAIL, "Your email address is missing.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put(FieldConstants.APPLICANT_EMAIL, "Your email address is too long.");
                break;
            case Validator.ERROR_EMAIL_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_EMAIL, "Your email address is invalid.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantGender(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_GENDER);
        String[] options = new String[]{"M", "F"};
        if (!Arrays.asList(options).contains(input)) {
            input = null;
        }

        sanitized.put(FieldConstants.APPLICANT_GENDER, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantMobileNumber(HttpServletRequest request, Map<String, Object> sanitized) {
        String countryCode = request.getParameter(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE);
        String mobileNum = request.getParameter(FieldConstants.APPLICANT_MOBILE_NUM);
        sanitized.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, countryCode);
        sanitized.put(FieldConstants.APPLICANT_MOBILE_NUM, mobileNum);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantEmail(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_EMAIL);
        sanitized.put(FieldConstants.APPLICANT_EMAIL, input);

        return sanitized;
    }


    private Map<String, Object> sanitizeApplicantResidency(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_RESIDENCY);
        String[] options = new String[] {
                "Singaporean",
                "Singapore PR",
                "Employment Pass",
                "Skilled Pass",
                "Personalised Employment Pass",
                "Dependent Pass",
                "Student Pass",
                "NA"
        };
        if (!Arrays.asList(options).contains(input)) {
            input = null;
        }

        sanitized.put(FieldConstants.APPLICANT_RESIDENCY, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantFirstName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_FIRST_NAME);
        sanitized.put(FieldConstants.APPLICANT_FIRST_NAME, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantLastName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_LAST_NAME);
        sanitized.put(FieldConstants.APPLICANT_LAST_NAME, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantDob(HttpServletRequest request, Map<String, Object> sanitized) {
        String dob = request.getParameter(FieldConstants.APPLICANT_DOB);
        sanitized.put(FieldConstants.APPLICANT_DOB, dob);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put(FieldConstants.LIFE_INSURED, application.getLifeInsured());
        data.put(FieldConstants.APPLICANT_RESIDENCY, application.getApplicantResidency());
        data.put(FieldConstants.APPLICANT_FIRST_NAME, application.getApplicantFirstName());
        data.put(FieldConstants.APPLICANT_LAST_NAME, application.getApplicantLastName());

        Date applicantDob = application.getApplicantDob();
        if (applicantDob != null) {
            data.put(FieldConstants.APPLICANT_DOB, (new SimpleDateFormat(DOB_FORMAT)).format(applicantDob));
        }

        data.put(FieldConstants.APPLICANT_MOBILE_COUNTRY_CODE, application.getApplicantMobileCountryCode());
        data.put(FieldConstants.APPLICANT_MOBILE_NUM, application.getApplicantMobileNum());
        data.put(FieldConstants.APPLICANT_EMAIL, application.getApplicantEmail());
        data.put(FieldConstants.APPLICANT_GENDER, application.getApplicantGender());

        request.setAttribute("data", data);
    }

}
