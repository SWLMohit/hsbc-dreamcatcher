package sg.com.hsbc.dreamcatcher.eclaims;

import java.util.Date;

/**
 * Created by KhoaTran on 11/21/2017.
 */
public class DoctorPortion {
    protected Date fromDate;
    protected Date toDate;
    protected String name;
    protected String hospital;
    protected String hospitalCity;
    protected String hospitalStreet1;
    protected String hospitalStreet2;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getHospitalCity() {
        return hospitalCity;
    }

    public void setHospitalCity(String hospitalCity) {
        this.hospitalCity = hospitalCity;
    }

    public String getHospitalStreet1() {
        return hospitalStreet1;
    }

    public void setHospitalStreet1(String hospitalStreet1) {
        this.hospitalStreet1 = hospitalStreet1;
    }

    public String getHospitalStreet2() {
        return hospitalStreet2;
    }

    public void setHospitalStreet2(String hospitalStreet2) {
        this.hospitalStreet2 = hospitalStreet2;
    }
}
