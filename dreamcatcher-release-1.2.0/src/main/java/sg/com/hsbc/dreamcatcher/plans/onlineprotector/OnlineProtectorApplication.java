package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import be.quodlibet.boxable.*;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.poi.util.StringUtil;
import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.ApplicationStartUpListener;
import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.AgeUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;



public class OnlineProtectorApplication extends ApplicationForm {

    public static final int PAYMENT_MODE_ANNUALLY = 1;
    public static final int PAYMENT_MODE_SEMI_ANNUALLY = 2;
    public static final int PAYMENT_MODE_QUARTERLY = 4;
    public static final int PAYMENT_MODE_MONTHLY = 12;
    public static final int UNDERWRITING_CLASS_PREFERRED = 0;
    public static final int UNDERWRITING_CLASS_STANDARD = 1;
    public static final int UNDERWRITING_CLASS_PREFERRED_PLUS = 2;
    public static final int UNDERWRITING_CLASS_STANDARD_PLUS = 3;
    public static final int NOTIFY_STATUS_NOT_SENT = 0;
    public static final int NOTIFY_STATUS_PROCESSING = 99;
    public static final int NOTIFY_STATUS_SENT = 1;
    public static final int MAIL_OPTION_EMAIL = 1;
    public static final int MAIL_OPTION_MAILOUT = 0;
    public static final int PDF_TYPE_BENEFIT_ILLUSTRATION = 1;
    public static final int PDF_TYPE_APPLICATION = 2;

    public static final int UNDERWRITING_LIFESTYLE_MORTGAGE_NA = 0;
    public static final int UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_12_MONTHS = 1;
    public static final int UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_24_MONTHS = 2;
    public static final int UNDERWRITING_LIFESTYLE_MORTGAGE_MORE_THAN_24_MONTHS = 3;

    public static final int UNDERWRITING_LIFESTYLE_PROFILE_NON_HSBC = 0;
    public static final int UNDERWRITING_LIFESTYLE_PROFILE_HSBC_JADE = 1;
    public static final int UNDERWRITING_LIFESTYLE_PROFILE_HSBC_PREMIER = 2;
    public static final int UNDERWRITING_LIFESTYLE_PROFILE_HSBC_ADVANCE = 3;
    public static final int UNDERWRITING_LIFESTYLE_PROFILE_HSBC_OTHERS = 4;

    public static final int UNDERWRITING_LIFESTYLE_EXERCISE_NA = 0;
    public static final int UNDERWRITING_LIFESTYLE_EXERCISE_DAILY = 1;
    public static final int UNDERWRITING_LIFESTYLE_EXERCISE_3_TIMES_WEEKLY = 2;
    public static final int UNDERWRITING_LIFESTYLE_EXERCISE_WEEKEND = 3;

    public static final String PRODUCT_TYPE = "onlineprotector";
    public static final String PRODUCT_CODE = "OT13";
    public static final String RIDER_PRODUCT_CODE = "OT19";
    public static final String PRODUCT_NAME = "HSBC Insurance OnlineProtector";

    protected DecimalFormat df = new DecimalFormat("#,###");
    protected DecimalFormat df2 = new DecimalFormat("#,###.00");

    private final static Logger logger = Logger.getLogger(OnlineProtectorApplication.class);

    protected String id;
    protected String referenceNum;
    protected String campaignCode;
    protected Boolean agreePrivacy;
    protected Boolean agreeImportantNotes;
    protected String lifeInsured;
    protected int status;
    protected String dropoutPassword;
    protected int notifyStatus;
    protected int mailOption;

    protected String applicantResidency;
    protected String applicantFirstName;
    protected String applicantLastName;
    protected Date applicantDob;
    protected String applicantGender;
    protected String applicantMobileCountryCode;
    protected String applicantMobileNum;
    protected String applicantEmail;
    protected String applicantNationality;
    protected Boolean applicantMultipleNationalities;
    protected String applicantNationality2;
    protected String applicantNationality3;
    protected String applicantBirthCountry;
    protected String applicantNric;
    protected Boolean applicantEnProficient;
    protected String applicantEmployment;
    protected Integer applicantIncome;
    protected Boolean applicantSmoker;
    protected String applicantOccupation;
    protected String applicantIndustry;
    protected String applicantCompanyName;
    protected String applicantCompanyCity;
    protected String applicantCompanyCountry;
    protected Date applicantStudiesEndDate;
    protected String applicantAddressPostalCode;
    protected String applicantAddressHouse;
    protected String applicantAddressUnit;
    protected String applicantAddressStreet;
    protected String applicantAddressBuilding;
    protected Boolean applicantAddressIsRegistered;
    protected Integer applicantAddressPeriodOfStay;
    protected String applicantPreviousAddressCountry;
    protected Boolean applicantAddressIsPermanent;
    protected String applicantPermanentAddressLine1;
    protected String applicantPermanentAddressLine2;
    protected String applicantPermanentAddressPostalCode;
    protected String applicantPermanentAddressCountry;

    protected String applicantTaxCountry1;
    protected String applicantTaxCountry1Tin;
    protected String applicantTaxCountry1NoTinReason;
    protected String applicantTaxCountry1NoTinReasonCustom;
    protected String applicantTaxCountry2;
    protected String applicantTaxCountry2Tin;
    protected String applicantTaxCountry2NoTinReason;
    protected String applicantTaxCountry2NoTinReasonCustom;
    protected String applicantTaxCountry3;
    protected String applicantTaxCountry3Tin;
    protected String applicantTaxCountry3NoTinReason;
    protected String applicantTaxCountry3NoTinReasonCustom;

    protected String applicantAddressNoTaxReason;
    protected Boolean applicantIsHsbcCustomer;
    protected Boolean applicantIsPep;
    protected Boolean applicantIsBeneficalOwner;
    protected Boolean pdfRequired;
    protected String insuredResidency;
    protected String insuredFirstName;
    protected String insuredLastName;
    protected Date insuredDob;
    protected String insuredGender;
    protected String insuredNationality;
    protected Boolean insuredMultipleNationalities;
    protected String insuredNationality2;
    protected String insuredNationality3;
    protected String insuredNric;
    protected String insuredEmployment;
    protected Integer insuredIncome;
    protected Boolean insuredSmoker;
    protected Boolean insuredMedical1;
    protected Boolean insuredMedical2;
    protected Boolean insuredMedical3;
    protected Boolean insuredMedical4;
    protected String insuredOccupation;
    protected String insuredIndustry;
    protected String insuredCompanyName;
    protected Date insuredStudiesEndDate;
    protected String insuredBirthCountry; //Added as per HSBCIDCU-892
    
    protected String beneficialOwnerFirstName;
    protected String beneficialOwnerLastName;
    protected String beneficialOwnerNric;
    protected String beneficialOwnerRelation;

    protected int termYears;
    protected Float sumAssured;
    protected Float riderSumAssured;
    protected int paymentMode;
    protected Boolean includeCriticalIllnessRider;
    protected Boolean includeRefundRider;
    protected Boolean underwritingLifestyleQuestionsOptin;
    protected Integer underwritingLifestyleMortgage;
    protected Integer underwritingLifestyleProfile;
    protected Float underwritingLifestyleHeight;
    protected Float underwritingLifestyleWeight;
    protected Integer underwritingLifestyleExercise;

    protected String creditCardType;
    protected String creditCardName;
    protected String creditCardNum;
    protected String creditCardExpiry;
    protected Date dateCreated;
    protected Date dateUpdated;
    protected float maxStep;

    protected Integer feedbackExperience;
    protected Integer feedbackEase;
    protected Boolean feedbackRecommend;
    protected String feedbackNoRecommendReason;
    protected String feedbackImprove;
    protected String feedbackAdditional;
    
    protected Date emailSentDate;
    private float pageHalfPosition = 300f;

    protected double basePremiumAnnual = -0.0000001;
    protected double basePremiumSemiAnnual = -0.0000001;
    protected double basePremiumQuarter = -0.0000001;
    protected double basePremiumMonth = -0.0000001;
    protected double ciPremiumAnnual = -0.0000001;
    protected double ciPremiumSemiAnnual = -0.0000001;
    protected double ciPremiumQuarter = -0.0000001;
    protected double ciPremiumMonth = -0.0000001;
    protected float refundPremiumAnnual = -0.0000001f;
    protected float refundPremiumSemiAnnual = -0.0000001f;
    protected float refundPremiumQuarter = -0.0000001f;
    protected float refundPremiumMonth = -0.0000001f;
    protected Boolean marketingConsent=true;
    
    public static OnlineProtectorApplication getById(String id) {
        OnlineProtectorApplication application = new OnlineProtectorApplication();
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try{
            stmt = conn.prepareStatement("SELECT * FROM applications WHERE id = ?");
            stmt.setString(1, id);

            rs = stmt.executeQuery();
            if (rs.next()) {
                application = populate(application, rs);
            } else {
                try (
                        PreparedStatement stmt2 = conn.prepareStatement("INSERT INTO applications (id, product_type, date_created, date_updated, max_step, status, notify_status) VALUES (?, ?, NOW(), NOW(), 0, ?, ?)");
                ) {
                    stmt2.setString(1, id);
                    stmt2.setString(2, PRODUCT_TYPE);
                    stmt2.setInt(3, STATUS_INCOMPLETE);
                    stmt2.setInt(4, NOTIFY_STATUS_NOT_SENT);
                    stmt2.executeUpdate();
                    application = getById(id);
                }
            }
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }finally {
            db.closeAll(conn,stmt, rs);
        }

        application.setTermYears(10);

        return application;
    }

    public ArrayList<Map<String, Object>> getBenefitIllustration() {
        ArrayList<Map<String, Object>> rows = new ArrayList();
        float premiumPaid = 0;
        int anb = AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob());
        int startAge = anb + 1;

        int year = 1;
        do {
            Map result = getPremiumAtAge(startAge);
            double premium = Double.parseDouble(result.get("premium").toString());
            premiumPaid += premium;

            if (year<=10 ) {
                result.put("premiumPaid", premiumPaid);
                result.put("year", year);
                rows.add(result);
            } else if(year<=20 && year%5 == 0) {
                result.put("premiumPaid", premiumPaid);
                result.put("year", year);
                rows.add(result);
            } else if(year%10 == 0){
                result.put("premiumPaid", premiumPaid);
                result.put("year", year);
                rows.add(result);
            }
            year++;
            startAge++;
        } while (startAge <= 90);

        return rows;
    }

    public ArrayList<Map<String, Object>> getCriticalIllnessRiderBenefitIllustration() {
        ArrayList<Map<String, Object>> rows = new ArrayList();
        float premiumPaid = 0;
        int anb = AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob());
        int startAge = anb + 1;

        int year = 1;
        do {
            Map result = getCriticalIllnessRiderPremiumAtAge(startAge);
            double premium = Double.parseDouble(result.get("premium").toString());
            premiumPaid += premium;

            if (year<=10 ) {
                result.put("premiumPaid", premiumPaid);
                result.put("year", year);
                rows.add(result);
            } else if(year<=20 && year%5 == 0) {
                result.put("premiumPaid", premiumPaid);
                result.put("year", year);
                rows.add(result);
            } else if(year%10 == 0){
                result.put("premiumPaid", premiumPaid);
                result.put("year", year);
                rows.add(result);
            }
            year++;
            startAge++;
        } while (startAge <= 90);

        return rows;
    }

    public ArrayList<Map<String, Object>> getRefundRiderBenefitIllustration() {
        ArrayList<Map<String, Object>> rows = new ArrayList();
        float premiumPaid = 0;
        int anb = AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob());
        int startAge = anb + 1;


        int year = 1;
        do {
            Map result = getRefundRiderPremiumAtAge(startAge);
            float premium = Float.parseFloat(result.get("premium").toString());
            premiumPaid += premium;

            if (year <= 10 || year == 15 || year % 10 == 0) {
                result.put("premiumPaid", premiumPaid);
                result.put("year", year);
                rows.add(result);

            }
            year++;
            startAge++;
        } while (startAge <= 90 && year <= 10);

        return rows;
    }

    public void sendNotifications() {
        sendNotifications(true);
    }
    public void sendNotifications(boolean isNeedSendToBO) {
        generateBIPDF();
        generateAdminBIPDF();
        generateApplicationPDF();
        /* JIRA HSBCIDCU-956 */
        generateAdminApplicationPDF();

        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", Config.getString("dreamcatcher.smtp.port"));
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        String DefaultCharSet = MimeUtility.getDefaultJavaCharset();
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        Transport transport = null;

        String subject = String.format("HSBC Insurance Online Singapore (%s)", getReferenceNum());
        //JIRA HSBCICDU-620 Underwriting Class in Email Notification should follow that as shown in Benefit Illustration
        String underwritingClassLabel = "";
        switch (getUnderwritingClass()) {
            case UNDERWRITING_CLASS_PREFERRED:
                underwritingClassLabel = "preferred rates";
                break;
            case UNDERWRITING_CLASS_STANDARD:
                underwritingClassLabel = "standard rates";
                break;
            case UNDERWRITING_CLASS_PREFERRED_PLUS:
                underwritingClassLabel = "preferred plus";
                break;
            case UNDERWRITING_CLASS_STANDARD_PLUS:
                underwritingClassLabel = "standard plus";
                break;
        }
        String content = "This is a system generated message. Please do not reply to this email directly\n\n " +
                "Dear " + getApplicantFirstName() + ",\n\n" +
                "Thank you for your application for a HSBC insurance policy.\n\n" +
                "We are pleased to inform you that we have accepted your application at " + underwritingClassLabel + " and insurance coverage will commence immediately subject to our receipt of the first premium payment and the completion of internal administrative checks, such as customer due diligence. We will send you the policy documents upon issuance of the policy within 7 working days.\n\n" +
                "Meanwhile, we enclose a copy of your application form for your reference. If the information given in the application form is not correct or should you have any questions pertaining to your application, please do not hesitate to contact us at (65) 62256111 (Mondays to Fridays, 9am to 5pm) or via email at e-surance@hsbc.com.sg by quoting the application number given in this email.\n\n\n" +
                "Sincerely,\n" +
                "HSBC Insurance Online Team";

        if (getReferenceNumComplexityFlag().equalsIgnoreCase("C")) {
            content = "This is a system generated message. Please do not reply to this email directly\n\n" +
                    "Dear " + getApplicantFirstName() + ",\n\n" +
                    "Thank you for your application for a HSBC insurance policy. " +
                    "We will be writing to you shortly on the requirements to process application.\n\nMeanwhile, we enclose a copy of your application form for your reference. If the information given in the application form is not correct or should you have any questions pertaining to your application, please do not hesitate to contact us at (65) 62256111 (Mondays to Fridays, 9am to 5pm) or via email at e-surance@hsbc.com.sg by quoting the application number given in this email.\n\n\n" +
                    "Sincerely,\n" +
                    "HSBC Insurance Online Team";
        }
      
        String toEmail = String.format("%s <%s>", getApplicantFullName(), getApplicantEmail());
        String fromEmail = Config.getString("dreamcatcher.email.from");

        /**
         * CUSTOMER EMAIL NOTIFICATION
         */

        String zipFilename = generateZipFile();

        try {
            message.setSubject(subject, "UTF-8");
            message.setFrom(new InternetAddress(fromEmail));
            message.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(toEmail));

            MimeMultipart msg_body = new MimeMultipart("alternative");
            MimeBodyPart wrap = new MimeBodyPart();
            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setContent(MimeUtility.encodeText(content, DefaultCharSet,"B"), "text/plain; charset=UTF-8");
            textPart.setHeader("Content-Transfer-Encoding", "base64");
            msg_body.addBodyPart(textPart);
            wrap.setContent(msg_body);
            MimeMultipart msg = new MimeMultipart("mixed");
            message.setContent(msg);
            msg.addBodyPart(wrap);

            MimeBodyPart att = new MimeBodyPart();
            DataSource fds = new FileDataSource(zipFilename);
            att.setDataHandler(new DataHandler(fds));
            att.setFileName(fds.getName());

            msg.addBodyPart(att);
        } catch (Exception e) {
            logger.error("Error Message ",e);
        }

        try {
            transport = session.getTransport();
            transport.connect(Config.getString("dreamcatcher.smtp.host"), Config.getString("dreamcatcher.smtp.username"), Config.getString("dreamcatcher.smtp.password"));
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }


        /**
         * CUSTOMER SMS NOTIFICATION
         */
        try{
            AmazonSNS sns = AmazonSNSClient.builder().withRegion(Regions.AP_SOUTHEAST_1).build();
            String smsMessage = "Thank you for your application. Please allow 7 working days to process the first premium payment and complete internal checks.";

            if (getReferenceNumComplexityFlag().equalsIgnoreCase("C")) {
                smsMessage = "Thank you for your application. We will be contacting you shortly on the requirements to process your application.";
            }

            smsMessage += String.format(" Your PDF password is: %s.", getPDFPassword(PDF_TYPE_BENEFIT_ILLUSTRATION));
            String smsPhoneNumber = getApplicantFullMobileNum();

            Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
            smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                    .withStringValue("HSBCInsure") //The sender ID shown on the device.
                    .withDataType("String"));

            PublishResult result = sns.publish(new PublishRequest()
                    .withMessage(smsMessage)
                    .withPhoneNumber(smsPhoneNumber)
                    .withMessageAttributes(smsAttributes));
        }catch(Exception e){
            e.printStackTrace();
        }


        /**
         * ADMIN EMAIL NOTIFICATION
         */
        if (isNeedSendToBO) {
            zipFilename = generateAdminZipFile();
//        content = "This is a system generated message. Please do not reply to this email directly";
            toEmail = Config.getString("dreamcatcher.email.admin.applications");
            if (isNationalitySpecialCase()) {
                toEmail += "," + Config.getString("dreamcatcher.email.admin.applications.nationality");
            }
            if (isIndustrySpecialCase()) {
                toEmail += "," + Config.getString("dreamcatcher.email.admin.applications.industry");
            }

            try {
                message = new MimeMessage(session);
                message.setSubject(subject, "UTF-8");
                message.setFrom(new InternetAddress(fromEmail));
                message.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(toEmail));

                MimeMultipart msg_body = new MimeMultipart("alternative");
                MimeBodyPart wrap = new MimeBodyPart();
                MimeBodyPart textPart = new MimeBodyPart();
                textPart.setContent(MimeUtility.encodeText(content, DefaultCharSet, "B"), "text/plain; charset=UTF-8");
                textPart.setHeader("Content-Transfer-Encoding", "base64");
                msg_body.addBodyPart(textPart);
                wrap.setContent(msg_body);
                MimeMultipart msg = new MimeMultipart("mixed");
                message.setContent(msg);
                msg.addBodyPart(wrap);

                MimeBodyPart att = new MimeBodyPart();
                DataSource fds = new FileDataSource(zipFilename);
                att.setDataHandler(new DataHandler(fds));
                att.setFileName(fds.getName());

                msg.addBodyPart(att);
            } catch (Exception e) {
                logger.error("Error Message " + e);
            }

            try {
                transport = session.getTransport();
                transport.connect(Config.getString("dreamcatcher.smtp.host"), Config.getString("dreamcatcher.smtp.username"), Config.getString("dreamcatcher.smtp.password"));
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();
            } catch (Exception e) {
                logger.error("Error Message " + e);
            }
        }
    }

    public String getPDFPassword(int type) {
        String password = null;
        switch (type) {
            case PDF_TYPE_BENEFIT_ILLUSTRATION:
                String nric = getApplicantNric();
                password = String.format("%s%s", (new SimpleDateFormat("ddMMM")).format(getApplicantDob()).toUpperCase(), nric.substring(nric.length() - 6, nric.length() - 1));
                break;
            case PDF_TYPE_APPLICATION:
                break;
        }

        return password;
    }

    public String getPDFFilename(int type) {
        return getPDFFilename(type, false);
    }

    public String getPDFFilename(int type, boolean forAdmin) {
        String filename = null;
        switch (type) {
            case PDF_TYPE_BENEFIT_ILLUSTRATION:
                if (forAdmin) {
                    filename = String.format("%s/%s-BenefitIllustration-HSBC.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum());
                } else {
                    filename = String.format("%s/%s-BenefitIllustration.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum());
                }
                break;
            case PDF_TYPE_APPLICATION:
              /* JIRA HSBCIDCU-856 */
                if (forAdmin) {
                  filename = String.format("%s/%s-ApplicationForm-HSBC.pdf", System.getProperty("java.io.tmpdir"),
                      getReferenceNum());
                } else {
                  filename = String.format("%s/%s-ApplicationForm.pdf", System.getProperty("java.io.tmpdir"),
                      getReferenceNum());
                }
                break;
        }

        return filename;
    }

    public String generateZipFile() {
        String filename = String.format("%s/%s.zip", System.getProperty("java.io.tmpdir"), getReferenceNum());
        String pdfFilename = String.format("%s/%s.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum());
        PDFMergerUtility pdfMerger = new PDFMergerUtility();
        try {
            pdfMerger.addSource(new File(getPDFFilename(PDF_TYPE_APPLICATION)));
            pdfMerger.addSource(new File(getPDFFilename(PDF_TYPE_BENEFIT_ILLUSTRATION)));
            pdfMerger.addSource(new File(String.format("%s/assets/downloads/GPPSOnlineProtector10.pdf", ApplicationStartUpListener.rootPath)));
            if (getIncludeCriticalIllnessRider()) {
                pdfMerger.addSource(new File(String.format("%s/assets/downloads/AXPSOnlineCI.pdf", ApplicationStartUpListener.rootPath)));
            }
            if (getIncludeRefundRider()) {
                pdfMerger.addSource(new File(String.format("%s/assets/downloads/AXPSOnlinePremiumRefund.pdf", ApplicationStartUpListener.rootPath)));
            }
            pdfMerger.setDestinationFileName(pdfFilename);
            pdfMerger.mergeDocuments(MemoryUsageSetting.setupMixed(0, 1000000000000l));
            String password = getPDFPassword(PDF_TYPE_BENEFIT_ILLUSTRATION);
            PDDocument doc = PDDocument.load(new File(pdfMerger.getDestinationFileName()), MemoryUsageSetting.setupTempFileOnly());
            AccessPermission ap = new AccessPermission();
            StandardProtectionPolicy spp = new StandardProtectionPolicy(password, password, ap);
            spp.setEncryptionKeyLength(128);
            spp.setPermissions(ap);
            doc.protect(spp);
            doc.save(pdfFilename);
            doc.close();
        } catch (Exception e) {
            logger.error("Error Message ",e);
        }

        try (FileOutputStream fos = new FileOutputStream(filename);
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            ArrayList<String> files = new ArrayList<>(Arrays.asList(
                    pdfFilename,
                    String.format("%s/%s-ApplicantIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantIDV2", System.getProperty("java.io.tmpdir"), getId()),//JIRA 661 relevant pass file addition for self
                    String.format("%s/%s-InsuredIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantAddress", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-BeneficalOwnerIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantNationalityTwoIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantNationalityThreeIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-InsuredNationalityTwoIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-InsuredNationalityThreeIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantPermanentAddress", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantIDVBackNric", System.getProperty("java.io.tmpdir"), getId()),  //HSBCICDU-950 back of nric
                    String.format("%s/%s-InsuredIDVBackNric", System.getProperty("java.io.tmpdir"), getId()),  //HSBCICDU-950 back of nric
                    String.format("%s/%s-InsuredIDV2", System.getProperty("java.io.tmpdir"), getId()), //JIRA 661 relevant pass file addition for spouse
                    String.format("%s/%s-ApplicantRelevantPassBack", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-InsuredRelevantPassBack", System.getProperty("java.io.tmpdir"), getId())
            ));

            for (Iterator<String> iterator = files.iterator(); iterator.hasNext();) {
            	 String f = iterator.next();
             	if(!StringUtils.contains(f, ".pdf")){
                  String fileToCheck=f+".jpg";
                  File file = new File(fileToCheck);
                  if (file != null && file.exists()) {
                  	readFileIntoZip(zos,file);
                  }else{
                  	fileToCheck=f+".jpeg";
                  	file = new File(fileToCheck);
                  	if (file != null && file.exists()) {
                  		readFileIntoZip(zos,file);
                  	}else{
                  		fileToCheck=f+".png";
                      	file = new File(fileToCheck);
                      	if (file != null && file.exists()) {
                      		readFileIntoZip(zos,file);
                      	}else{
                      		fileToCheck=f+".pdf";
                          	file = new File(fileToCheck);
                          	if (file != null && file.exists()) {
                          		readFileIntoZip(zos,file);
                          	}
                      	}
                  	}
                  }
             }else{
             	File file = new File(f);
             	readFileIntoZip(zos,file);
             }
            }

            zos.close();
            fos.close();
        } catch (Exception e) {
            logger.error("Error Message ",e);
            return null;
        }

        return filename;
    }
    public void readFileIntoZip(ZipOutputStream zos,File file){
    	try{  
    		FileInputStream fis = new FileInputStream(file) ;
            ZipEntry zipEntry = new ZipEntry(file.getName());
            zos.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zos.write(bytes, 0, length);
            }
            zos.closeEntry();
            fis.close();
        }catch(Exception e){
        	
        }
    }
    public String generateAdminZipFile() {
        String filename = String.format("%s/%s-HSBC.zip", System.getProperty("java.io.tmpdir"), getReferenceNum());
        PDFMergerUtility pdfMerger = new PDFMergerUtility();
        try {
            pdfMerger.addSource(new File(getPDFFilename(PDF_TYPE_APPLICATION,true)));//admin application PDF as a part of HSBCIDCU-856
            pdfMerger.addSource(new File(getPDFFilename(PDF_TYPE_BENEFIT_ILLUSTRATION)));
            pdfMerger.addSource(new File(String.format("%s/assets/downloads/GPPSOnlineProtector10.pdf", ApplicationStartUpListener.rootPath)));
            if (getIncludeCriticalIllnessRider()) {
                pdfMerger.addSource(new File(String.format("%s/assets/downloads/AXPSOnlineCI.pdf", ApplicationStartUpListener.rootPath)));
            }
            if (getIncludeRefundRider()) {
                pdfMerger.addSource(new File(String.format("%s/assets/downloads/AXPSOnlinePremiumRefund.pdf", ApplicationStartUpListener.rootPath)));
            }
            pdfMerger.setDestinationFileName(String.format("%s/%s.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum()));
            pdfMerger.mergeDocuments(MemoryUsageSetting.setupMixed(0, 1000000000000l));
        } catch (Exception e) {
            logger.error("Exception occurred in generateAdminZipFile"+e.getMessage());
        }

        try (FileOutputStream fos = new FileOutputStream(filename);
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            ArrayList<String> files = new ArrayList<>(Arrays.asList(
                    String.format("%s/%s.pdf", System.getProperty("java.io.tmpdir"), getReferenceNum()),
                    String.format("%s/%s-ApplicantIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantIDV2", System.getProperty("java.io.tmpdir"), getId()),//JIRA 661- relevant pass addition for self
                    String.format("%s/%s-InsuredIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantAddress", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-BeneficalOwnerIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantNationalityTwoIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantNationalityThreeIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-InsuredNationalityTwoIDV", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-InsuredNationalityThreeIDV", System.getProperty("java.io.tmpdir"), getId()), 
                    String.format("%s/%s-ApplicantPermanentAddress", System.getProperty("java.io.tmpdir"), getId()),
                    String.format("%s/%s-ApplicantIDVBackNric", System.getProperty("java.io.tmpdir"), getId()), //HSBCICDU-950 back of nric
                    String.format("%s/%s-InsuredIDVBackNric", System.getProperty("java.io.tmpdir"), getId()), //HSBCICDU-950 back of nric
                    String.format("%s/%s-InsuredIDV2", System.getProperty("java.io.tmpdir"), getId()),//JIRA 661-relevant pass addition for spouse  
                    String.format("%s/%s-ApplicantRelevantPassBack", System.getProperty("java.io.tmpdir"), getId()),               
                    String.format("%s/%s-InsuredRelevantPassBack", System.getProperty("java.io.tmpdir"), getId())
            ));


            for (Iterator<String> iterator = files.iterator(); iterator.hasNext();) {
            	 String f = iterator.next();
            	if(!StringUtils.contains(f, ".pdf")){
                 String fileToCheck=f+".jpg";
                 File file = new File(fileToCheck);
                 if (file != null && file.exists()) {
                 	readFileIntoZip(zos,file);
                 }else{
                 	fileToCheck=f+".jpeg";
                 	file = new File(fileToCheck);
                 	if (file != null && file.exists()) {
                 		readFileIntoZip(zos,file);
                 	}else{
                 		fileToCheck=f+".png";
                     	file = new File(fileToCheck);
                     	if (file != null && file.exists()) {
                     		readFileIntoZip(zos,file);
                     	}else{
                     		fileToCheck=f+".pdf";
                         	file = new File(fileToCheck);
                         	if (file != null && file.exists()) {
                         		readFileIntoZip(zos,file);
                         	}
                     	}
                 	}
                 }
            }else{
            	File file = new File(f);
            	readFileIntoZip(zos,file);
            }
           }

            zos.close();
            fos.close();
        } catch (Exception e) {
            return null;
        }

        return filename;
    }

    public void generateApplicationPDF() {
        generatePDF(PDF_TYPE_APPLICATION, false);
    }
    
    /**
     * This method is used to generate the PDf for the back office.
     */
    public void generateAdminApplicationPDF() { 
        generatePDF(PDF_TYPE_APPLICATION, true);
     }

    public void generateBIPDF() {
        generatePDF(PDF_TYPE_BENEFIT_ILLUSTRATION, false);
    }

    public void generateAdminBIPDF() {
        generatePDF(PDF_TYPE_BENEFIT_ILLUSTRATION, true);
    }

    public void generatePDF(int type, boolean makePublic) {
        PDFGenerator pd = null;
        try {
            switch (type) {
                case PDF_TYPE_BENEFIT_ILLUSTRATION:
                    if (makePublic) {
                        pd = new PDFGenerator(getPDFFilename(type, true), PDFGenerator.PDF_TYPE_BENEFIT_ILLUSTRATION);
                    } else {
                        pd = new PDFGenerator(getPDFFilename(type), PDFGenerator.PDF_TYPE_BENEFIT_ILLUSTRATION);
                    }
                    generateBIPDFPage1(pd);
                    generateBIPDFPage2(pd);
                    generateBIPDFPage3(pd);
                    if (getIncludeCriticalIllnessRider()) {
                        generateBIPDFPage4(pd);
                    }
                    if (getIncludeRefundRider()) {
                        generateBIPDFPage5(pd);
                    }
                    PDPage page;
                    for (int i = 0; i < pd.getDocument().getNumberOfPages(); i++) {
                        page = pd.getDocument().getPage(i);
                        pd.applyPageHeader(page);
                        pd.applyPageFooter(page, i + 1);
                    }
                    break;
                case PDF_TYPE_APPLICATION:
                  /*JIRA HSBCIDCU-856*/
                	
                  if (makePublic) {
                    pd = new PDFGenerator(getPDFFilename(type, true), PDFGenerator.PDF_TYPE_APPLICATION);
                  } else {
                    pd = new PDFGenerator(getPDFFilename(type), PDFGenerator.PDF_TYPE_APPLICATION);
                  }
                  generateApplicationPDFPages(pd, makePublic);
                  break;
            }

            if (pd != null) {
                try {
                    String password = getPDFPassword(type);
                    if (!makePublic && password != null && !password.isEmpty()) {
//                    pd.protect(password);
                    }
                    pd.save();
                } catch (Exception e) {
                    logger.error("Error Message "+e);
                }
            }
        } catch(Exception e){
            logger.error("Error Message in BiPDf "+e);
        }finally {
            if (pd != null) {
                try {
                    pd.close();
                } catch (IOException ioe) {
                    logger.error("Error Message "+ioe);
                }
            }
        }
    }

    private void generateApplicationPDFPages(PDFGenerator pd,boolean isForAdmin) {
        PDPage page = pd.createPage();
        pd.addPage(page);
        String[] arrayDeclaration = {};
        PDFont font = PDType1Font.TIMES_ITALIC;   
        List<List> dataDec = new ArrayList();
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
		try {
			pd.applyPageHeader(page, pd.getDocument());
		     //Pdf header
       	
		/*code for address and description of 
		 * the pdf generated above address*/


            PDFLayout.myFont=12;
            float yStartHead=PDFLayout.mainTitleListRight(
                    new String[]{
                            "<b>Proposal for HSBC Insurance OnlineProtector</b>",  //JIRA HSBCIDCU-630 Correct policy name to OnlineProtector
                    }, page.getMediaBox().getHeight() - PDFGenerator.PAGE_MARGIN_TOP, page.getMediaBox().getHeight() - PDFGenerator.PAGE_MARGIN_TOP, tableWidth, page, pd, 15);
            PDFLayout.myFont=0;
            pd.setFont(PDFGenerator.FONT_NORMAL, 9);
            pd.setBuiltInFont(null, 9);
            yStartHead = PDFLayout.mainTitleListLeft(
                    new String[]{
                            "<b>HSBC Insurance (Singapore) Pte. Limited </b>(Reg. No. 195400150N)",
                            "21 Collyer Quay #02-01 Singapore 049320, Monday to Friday 9.30am to 5pm. insuranceonline.hsbc.com.sg",
                            "Customer Care Hotline: (65) 6225 6111 Fax: (65) 6221 2188",
                            "Mailing address: Robinson Road Post Office P.O. BOX 1538 Singapore 903038"
                    }, yStartHead, yStartHead, tableWidth-20, page, pd, 0);

            pd.setRedColor(255);
            pd.drawRectangle(page,PDFGenerator.PAGE_MARGIN_LEFT ,yStartHead-250, 230, tableWidth);

            //JIRA HSBCICDU-630 Update important notes
            String guidelines = "VERY IMPORTANT NOTES\n\nDUE TO US INSURANCE REGULATORY REQUIREMENTS, YOU ARE NOT TO ENTER THE US OR ANY TERRITORY \nSUBJECT TO US JURISDICTION DURING THE APPLICATION PROCESS, OTHERWISE THE REQUEST EFFECTED \nHEREUNDER MAY BE MADE VOID.\n\n"+
                    "THIS IS A SELF-DIRECTED ONLINE PLATFORM TO PURCHASE INSURANCE PRODUCTS. " +
                    "HSBC INSURANCE\n(SINGAPORE) PTE. LIMITED (\"HSBC INSURANCE\") DOES NOT PROVIDE ANY ADVICE TO CLIENTS TRANSACTING ON\nTHIS PLATFORM.\n\n"+
                    "YOU AGREE THAT THE INFORMATION PROVIDED IN THIS ONLINE APPLICATION FORM, SUCH OTHER FORMS, \nDOCUMENTS AND QUESTIONNAIRES PROVIDED BY YOU IN RELATION TO THIS APPLICATION WILL FORM THE BASIS \nOF THE CONTRACT OF INSURANCE BETWEEN YOU AND HSBC INSURANCE.\n\n"+
                    "PLEASE NOTE, WE ARE REQUIRED UNDER THE INSURANCE ACT TO INFORM YOU THAT YOU MUST FULLY AND \nFAITHFULLY DISCLOSE ALL THE FACTS WHICH YOU KNOW OR OUGHT TO KNOW (INCLUDING ANY EXISTING \nMEDICAL CONDITIONS). OTHERWISE, IN ACCORDANCE WITH SECTION 25(5) OF THE INSURANCE ACT (CAP. 142), \nYOUR POLICY MAY BE CANCELLED AND YOU MAY RECEIVE NO BENEFITS.";

            pd.setBuiltInFont(PDType1Font.HELVETICA_BOLD, 8);
            page = pd.writeText(page, guidelines, PDFGenerator.PAGE_MARGIN_LEFT+10,page.getMediaBox().getHeight()-3*PDFGenerator.PAGE_MARGIN_TOP+5);
            pd.setRedColor(0);
            String note = "The products and its related features offered on this online platform are available only to customers who are currently \nresiding in Singapore";
            pd.setBuiltInFont(PDType1Font.HELVETICA,9);
            page = pd.writeText(page, note, 60,page.getMediaBox().getHeight()-7*PDFGenerator.PAGE_MARGIN_TOP+5);
            pd.setBuiltInFont(null, 9);
            pd.setFont(PDFGenerator.FONT_NORMAL, 9);

        } catch (Exception e) {
        	logger.info("error in creating pdf imp notes "+e);
        }


		/*Declarations and acknoledgement code logic
		 * Reading of declarations from jsp and parsing
		 * the document to String */


        try{
            File fileDeclaration1=null;
            File fileDeclaration2=null;
            File fileDeclaration3=null;
            File fileDeclaration0=new File(String.format("%sWEB-INF/ourplans/onlineprotector/step1.jsp", ApplicationStartUpListener.rootPath));
            if(insuredIsSelf()){
                if(getIncludeCriticalIllnessRider()&&getIncludeRefundRider()){
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/declaration_plan_ci_pr.jsp", ApplicationStartUpListener.rootPath));

                    fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack2_plan_ci_pr.jsp", ApplicationStartUpListener.rootPath));

                }else if(getIncludeCriticalIllnessRider()){
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/declaration_plan_ci.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration2=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack1_ci.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack2_plan_ci.jsp", ApplicationStartUpListener.rootPath));

                }else if(getIncludeRefundRider()){
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/declaration_plan_pr.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack2_plan_pr.jsp", ApplicationStartUpListener.rootPath));
                }
                else{
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/declaration_plan.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration2=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack1_no_ci.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack2_plan.jsp", ApplicationStartUpListener.rootPath));

                }
            }else{
                if(getIncludeCriticalIllnessRider()&&getIncludeRefundRider()){
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/declaration_spouse_plan_ci_pr", ApplicationStartUpListener.rootPath));
                    fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack2_spouse_plan_ci_pr.jsp", ApplicationStartUpListener.rootPath));
                }else if(getIncludeCriticalIllnessRider()){
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/declaration_spouse_plan_ci.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration2=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack1_spouse_ci.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack2_spouse_plan_ci.jsp", ApplicationStartUpListener.rootPath));

                }else if(getIncludeRefundRider()){
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/declaration_spouse_plan_pr.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack2_spouse_plan_pr.jsp", ApplicationStartUpListener.rootPath));
                }else{
                    fileDeclaration1=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/declaration_spouse_plan.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration2=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack1_spouse_no_ci.jsp", ApplicationStartUpListener.rootPath));
                    fileDeclaration3=new File(String.format("%s/WEB-INF/ourplans/onlineprotector/includes/ack2_spouse_plan.jsp", ApplicationStartUpListener.rootPath));
                }
            }
           
            dataDec.add(new ArrayList<>(Arrays.asList("")));
            dataDec.add(new ArrayList<>(Arrays.asList("")));
            dataDec=parsingHtmlToText(fileDeclaration0,dataDec);
            dataDec= parsingHtmlToText(fileDeclaration1,dataDec);
           
            //JIRA HSBCIDCU-630 fix missing i accept
            if (!insuredIsSelf()) {
                String insuredAccept = "<img-check><b>We, "+ getApplicantFullName() + " and " + getInsuredFullName() + " agree with the above statements.</b>";
                dataDec.add(new ArrayList<>(Arrays.asList(insuredAccept)));
            } else {
                String iAccept = "<img-check><b>I " + getApplicantFullName() + " agree with the above statements.</b>";
                dataDec.add(new ArrayList<>(Arrays.asList(iAccept)));
            }
            	

           dataDec= parsingHtmlToText(fileDeclaration2,dataDec);
            
            dataDec=parsingHtmlToText(fileDeclaration3,dataDec);
            if (!insuredIsSelf()) {
            	String submission="<img-check><b>By submitting this application, we, " + getApplicantFullName() + " and " + getInsuredFullName() + " acknowledge that we have read, understood and agree with the statements above.</b>";
            	dataDec.add(new ArrayList<>(Arrays.asList(submission)));
            }else {
            	String selfSubmit="<img-check><b>By submitting this application, I " + getApplicantFullName() + " acknowledge that I have read, understood and agree with the statements above.</b>";
                dataDec.add(new ArrayList<>(Arrays.asList(selfSubmit)));
            }
            if (!insuredIsSelf()) {
            	dataDec.add(new ArrayList<>(Arrays.asList("<img-check><b>We, "+ getApplicantFullName() + " and " + getInsuredFullName() + " agree with the statement below")));
            	dataDec.add(new ArrayList<>(Arrays.asList("","If we are customer(s) of, or become a customer of, HSBC Bank (Singapore) Limited (\"HSBC Bank\"), we agree that HSBC Insurance may disclose our personal data provided in this form to HSBC Bank, to enable HSBC Bank to provide us with a consolidated view of all the products we may hold with HSBC Bank, HSBC Insurance and (where relevant) any other member of the HSBC Group.")));
            	 String checkbox="<img-uncheck>";
                 if(getMarketingConsent()){
                 	checkbox="<img-check>";
                 }
                 dataDec.add(new ArrayList<>(Arrays.asList("")));
            	dataDec.add(new ArrayList<>(Arrays.asList(checkbox+"<b>We, "+ getApplicantFullName() + " and " + getInsuredFullName() + " agree with the statement below")));
                dataDec.add(new ArrayList<>(Arrays.asList("","We consent to the use and disclosure of our personal data provided in this form to the HSBC Group and its respective agents, authorised service parties and relevant third parties for the purpose of sending us news and alerts on new products and special promotions offered by HSBC Insurance and its group companies.")));
            } else {
                dataDec.add(new ArrayList<>(Arrays.asList("<img-check><b>I "+ getApplicantFullName() + " agree with the statement below")));
                dataDec.add(new ArrayList<>(Arrays.asList("","If I am a customer of, or become a customer of, HSBC Bank (Singapore) Limited (\"HSBC Bank\"), I agree that HSBC Insurance may disclose my personal data provided in this form to HSBC Bank, to enable HSBC Bank to provide me with a consolidated view of all the products I may hold with HSBC Bank, HSBC Insurance and (where relevant) any other member of the HSBC Group.")));
                String checkbox="<img-uncheck>";
                if(getMarketingConsent()){
                	checkbox="<img-check>";
                }
                dataDec.add(new ArrayList<>(Arrays.asList("")));
                dataDec.add(new ArrayList<>(Arrays.asList(checkbox+"<b>I "+ getApplicantFullName() + " agree with the statement below")));
                dataDec.add(new ArrayList<>(Arrays.asList("","I consent to the use and disclosure of my personal data provided in this form to the HSBC Group and its respective agents, authorised service parties and relevant third parties for the purpose of sending me news and alerts on new products and special promotions offered by HSBC Insurance and its group companies.")));
            }
            dataDec.add(new ArrayList<>(Arrays.asList("")));
        }
        catch(Exception e){
            logger.error("Error Message ",e);
        }

        /*User entered values displayed in the pdf
         * Additional details */
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        yStart =  page.getMediaBox().getHeight() - (7*PDFGenerator.PAGE_MARGIN_TOP+40);
        try{
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("", "")));
            data.add(new ArrayList<>(Arrays.asList(String.format("Application Date:<br>%s", (new Date())), String.format("Reference Number:<br>%s", StringUtils.isBlank(getReferenceNum())?"NA":getReferenceNum()))));
            data.add(new ArrayList<>(Arrays.asList(String.format("Agent Code:<br>%s", "14000"), String.format("Campaign Code:<br>%s", StringUtils.isBlank(getCampaignCode())?"NA":getCampaignCode()))));

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, "Additional Details",HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

	  /*User entered values displayed in the pdf
	   * My details Section */
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("", "")));
            data.add(new ArrayList<>(Arrays.asList(String.format("Last Name/Surname:<br>%s", StringUtils.isBlank(getApplicantLastName())?"NA":getApplicantLastName()), String.format("First/Given Name:<br>%s", StringUtils.isBlank(getApplicantFirstName())?"NA":getApplicantFirstName()))));

            data.add(new ArrayList<>(Arrays.asList(String.format("Date of Birth:<br>%s", getApplicantDob()==null?"NA":new SimpleDateFormat("dd-MM-yyyy").format(getApplicantDob())), String.format("Gender:<br>%s", StringUtils.isBlank(getApplicantGenderLabel())?"NA":getApplicantGenderLabel()))));
			      data.add(new ArrayList<>(Arrays.asList(String.format("Country of Birth:<br>%s", StringUtils.isBlank(getApplicantBirthCountry())?"NA":getApplicantBirthCountry()), String.format(((applicantIsSingaporean()) || (applicantIsPermanentResident()) ? "NRIC No.:<br>%s" : "Passport No.:<br>%s"), StringUtils.isBlank(getApplicantNric())?"NA":getApplicantNric()))));
			      String nationalities = getApplicantNationality();

            if (!getApplicantNationality2().isEmpty()) {
                nationalities += "<br>" + getApplicantNationality2();
            }
            if (!getApplicantNationality3().isEmpty()) {
                nationalities += "<br>" + getApplicantNationality3();
            }
            data.add(new ArrayList<>(Arrays.asList(String.format("Nationalities: (please list all)<br>%s", StringUtils.isBlank(nationalities)?"NA":nationalities), String.format("Residency Status:<br>%s", StringUtils.isBlank(getApplicantResidency())?"NA":getApplicantResidency()))));
            /*moved here to solve table break as a part of HSBCIDCU-892*/
            data.add(new ArrayList<>(Arrays.asList("Are you proficient in the English Language (spoken and written) and have you completed at least a secondary school education or its equivalent?<br>Yes")));
            data.add(new ArrayList<>(Arrays.asList(String.format("Are you an HSBC customer?<br>%s", (getApplicantIsHsbcCustomer()==null?"NA":getApplicantIsHsbcCustomer() ? "Yes" : "No")))));

            if (!insuredIsSelf()) {
                data.add(new ArrayList<>(Arrays.asList("Who are you buying the policy for?:<br>My spouse")));
            } else {
                //JIRA HSBCICDU-630 Add myself if insured is myself
                data.add(new ArrayList<>(Arrays.asList("Who are you buying the policy for?:<br>Myself")));
            }

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, (insuredIsSelf() ? "My Details" : "Applicant's Details"),HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
  
           
        }
        catch (Exception e) {
            logger.error("Error Message "+e);

        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        if (!insuredIsSelf()) {
            try {
                List<List> data = new ArrayList();
                data.add(new ArrayList<>(Arrays.asList("", "")));
                data.add(new ArrayList<>(Arrays.asList(String.format("Last Name/Surname:<br>%s", StringUtils.isBlank(getInsuredLastName())?"NA":getInsuredLastName()), String.format("First/Given Name:<br>%s", StringUtils.isBlank(getInsuredFirstName())?"NA":getInsuredFirstName()))));

                data.add(new ArrayList<>(Arrays.asList(String.format("Date of Birth:<br>%s", getInsuredDob()==null?"NA":new SimpleDateFormat("dd-MM-yyyy").format(getInsuredDob())), String.format("Gender:<br>%s", StringUtils.isBlank(getInsuredGenderLabel())?"NA":getInsuredGenderLabel()))));
                data.add(new ArrayList<>(Arrays.asList(String.format("Country of Birth:<br>%s", StringUtils.isBlank(getInsuredBirthCountry())?"NA":getInsuredBirthCountry()), String.format(((insuredIsSingaporean()) || (insuredIsPermanentResident()) ? "NRIC No.:<br>%s" : "Passport No.:<br>%s"), StringUtils.isBlank(getInsuredNric())?"NA":getInsuredNric()))));

                String nationalities = getInsuredNationality();
                if (!getInsuredNationality2().isEmpty()) {
                    nationalities += "<br>" + getInsuredNationality2();
                }
                if (!getInsuredNationality3().isEmpty()) {
                    nationalities += "<br>" + getInsuredNationality3();
                }
                data.add(new ArrayList<>(Arrays.asList(String.format("Nationalities: (please list all)<br>%s", StringUtils.isBlank(nationalities)?"NA":nationalities), String.format("Residency Status:<br>%s", StringUtils.isBlank(getInsuredResidency())?"NA":getInsuredResidency()))));

                BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
                PlanDataTable t = new PlanDataTable(dataTable, page);
                t.addListToTable(data, PlanDataTable.NOHEADER, "Life Insured's Details",HorizontalAlignment.LEFT);
                yStart = dataTable.draw() - 20;
            } catch (Exception e) {
                logger.error("Error Message "+e);
            }
        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);

        /*User entered values displayed in the pdf
    	 *Contact Details Section */

        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("", "")));
            StringBuffer address=new StringBuffer();
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressHouse()), "null")){
            	address.append(getApplicantAddressHouse()+" ");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressStreet()), "null")){
            	address.append(getApplicantAddressStreet()+" ");
            }
            if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressUnit()), "null")||StringUtils.containsIgnoreCase(StringUtils.trim(getApplicantAddressUnit()), "null"))){
            	address.append("<br>"+getApplicantAddressUnit()+" ");
            }else{
            	address.append("<br>");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressBuilding()), "null")){
            	address.append(getApplicantAddressBuilding()+" ");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getApplicantAddressPostalCode()), "null")){
            	address.append("<br>Postal Code: "+getApplicantAddressPostalCode()+" ");
            }
            data.add(new ArrayList<>(Arrays.asList(String.format("Mobile No.:<br>%s", StringUtils.isBlank(getApplicantFullMobileNum())?"NA":getApplicantFullMobileNum()), String.format("Residential/Mailing Address:<br>%s", StringUtils.isBlank(address.toString())?"NA":address))));
            //JIRA HSBCICDU-630 My Contact Details is not shown in pdf because getApplicantAddressIsRegistered is null - fix

            if(applicantIsSingaporean() || applicantIsPermanentResident()){
          	  if(getApplicantAddressPeriodOfStay() % 12==1) {  
                  data.add(new ArrayList<>(Arrays.asList(String.format("Is the above residential address the same as stated on your NRIC?:<br>%s", (getApplicantAddressIsRegistered()==null?"No":getApplicantAddressIsRegistered() ? "Yes" : "No")), String.format("How long have you been living at the residential address that you provided above?:<br>%s years %s month", Math.round((getApplicantAddressPeriodOfStay() / 12)), ((getApplicantAddressPeriodOfStay() % 12))))));
          	  }else{
          		  data.add(new ArrayList<>(Arrays.asList(String.format("Is the above residential address the same as stated on your NRIC?:<br>%s", (getApplicantAddressIsRegistered()==null?"No":getApplicantAddressIsRegistered() ? "Yes" : "No")), String.format("How long have you been living at the residential address that you provided above?:<br>%s years %s months", Math.round((getApplicantAddressPeriodOfStay() / 12)), ((getApplicantAddressPeriodOfStay() % 12))))));  
          	  }
          	}else{
          		if(getApplicantAddressPeriodOfStay() % 12==1) { 
          		  data.add(new ArrayList<>(Arrays.asList(String.format("How long have you been living at the residential address that you provided above?:<br>%s years %s month", Math.round((getApplicantAddressPeriodOfStay() / 12)), ((getApplicantAddressPeriodOfStay() % 12))))));
          		}else{
          			data.add(new ArrayList<>(Arrays.asList(String.format("How long have you been living at the residential address that you provided above?:<br>%s years %s months", Math.round((getApplicantAddressPeriodOfStay() / 12)), ((getApplicantAddressPeriodOfStay() % 12))))));
          		}
          	}
           String permanentAddress = "";


            if (!getApplicantAddressIsPermanent()) {
                permanentAddress += (getApplicantPermanentAddressLine1() != null ? getApplicantPermanentAddressLine1()+" " : "");
                permanentAddress += (getApplicantPermanentAddressLine2() != null ? "<br>" + getApplicantPermanentAddressLine2() : "");
                permanentAddress+=(getApplicantPermanentAddressPostalCode()!=null?"<br>Postal Code: "+getApplicantPermanentAddressPostalCode()+" ":"");
                permanentAddress+=(getApplicantPermanentAddressCountry()!=null?"<br>Country: "+getApplicantPermanentAddressCountry():"");
            }
            //JIRA HSBCIDCU-630 Show NA in permanent address for Singaporean

            if(applicantIsPermanentResident()){
            	data.add(new ArrayList<>(Arrays.asList(String.format("Is the above residential address also your Permanent Address?:<br>%s", (getApplicantAddressIsPermanent()==null?"No":getApplicantAddressIsPermanent() ? "Yes" : "No")), String.format("Permanent Address:<br>%s", StringUtils.isBlank(permanentAddress)?"NA":permanentAddress))));
            }else if(applicantIsPassHolder()){
            	data.add(new ArrayList<>(Arrays.asList(  String.format("Permanent Address:<br>%s", StringUtils.isBlank(permanentAddress)?"NA":permanentAddress))));
            }
            if(StringUtils.isNotBlank(getApplicantPreviousAddressCountry())){
                data.add(new ArrayList<>(Arrays.asList(String.format("Previous Address:<br>%s", (StringUtils.isNotBlank(getApplicantPreviousAddressCountry())? getApplicantPreviousAddressCountry() : "NA")), String.format("Email Address:<br>%s", StringUtils.isBlank(getApplicantEmail())?"NA":getApplicantEmail()))));
               }else{
               	data.add(new ArrayList<>(Arrays.asList(String.format("Email Address:<br>%s", StringUtils.isBlank(getApplicantEmail())?"NA":getApplicantEmail()))));
               }


            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, (insuredIsSelf() ? "My Contact Details and Address" : "Applicant's Contact Details and Address"),HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {
            logger.error("Error Message:" + e);
        }
        /*User entered values displayed in the pdf
  	   * My Occupation Section */

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("", "")));
            data.add(new ArrayList<>(Arrays.asList(String.format("Employment Status:<br>%s", StringUtils.isBlank(getApplicantEmployment())?"NA":getApplicantEmployment()), String.format("Industry:<br>%s", StringUtils.isBlank(getApplicantIndustry())?"NA":getApplicantIndustry()))));
            data.add(new ArrayList<>(Arrays.asList(String.format("Occupation:<br>%s", StringUtils.isBlank(getApplicantOccupation())?"NA":getApplicantOccupation()), String.format("Current Annual Income:<br>S$%s", getApplicantIncome()==null?"NA":df.format(getApplicantIncome())))));
            data.add(new ArrayList<>(Arrays.asList(String.format("Employer's Name:<br>%s", StringUtils.isBlank(getApplicantCompanyName())?"NA":getApplicantCompanyName()), String.format("City / Country:<br>%s / %s", StringUtils.isBlank(getApplicantCompanyCity())?"NA":getApplicantCompanyCity(), StringUtils.isBlank(getApplicantCompanyCountry())?"NA":getApplicantCompanyCountry()))));
            if (applicantIsStudying()) {
                data.add(new ArrayList<>(Arrays.asList(String.format("Student Course End Date: (if applicable)<br>%s", getApplicantStudiesEndDate()==null?"NA":new SimpleDateFormat("MM-yyyy").format(getApplicantStudiesEndDate())), "")));
            }

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, (insuredIsSelf() ? "My Occupation" : "Applicant's Occupation"),HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        if (!insuredIsSelf()) {
            try {
                List<List> data = new ArrayList();
                data.add(new ArrayList<>(Arrays.asList("", "")));
                data.add(new ArrayList<>(Arrays.asList(String.format("Employment Status:<br>%s", StringUtils.isBlank(getInsuredEmployment())?"NA":getInsuredEmployment()), String.format("Industry:<br>%s", StringUtils.isBlank(getInsuredIndustry())?"NA":getInsuredIndustry()))));
                data.add(new ArrayList<>(Arrays.asList(String.format("Occupation:<br>%s", StringUtils.isBlank(getInsuredOccupation())?"NA":getInsuredOccupation()), String.format("Current Annual Income:<br>S$%s",getInsuredIncome()==null?"NA": df.format(getInsuredIncome())))));
                String study = "";
                if (insuredIsStudying()) {
                    study = String.format("Student Course End Date: (if applicable)<br>%s", getInsuredStudiesEndDate()==null?"NA":new SimpleDateFormat("MM-yyyy").format(getInsuredStudiesEndDate()));
                }
                data.add(new ArrayList<>(Arrays.asList(String.format("Employer's Name:<br>%s", StringUtils.isBlank(getInsuredCompanyName())?"NA":getInsuredCompanyName()), study)));

                BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
                PlanDataTable t = new PlanDataTable(dataTable, page);
                t.addListToTable(data, PlanDataTable.NOHEADER, "Life Insured's Occupation",HorizontalAlignment.LEFT);
                yStart = dataTable.draw() - 20;
            } catch (Exception e) {
                logger.error("Error Message "+e);
            }
        }

        /*User entered values displayed in the pdf
    	   * My Insurance Section */

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("", "")));
            //JIRA HSBCICDU-630 Show premium in two decimal places
            String premium = df2.format(((double)getPremiumAtAge(AgeUtils.calculateAgeNextBirthday((insuredIsSelf() ? getApplicantDob() : getInsuredDob())), getPaymentMode()).get("modalPremium")));
            logger.info("the premium value is "+premium+" and payment mode is  "+getPaymentMode()+" "+(getPremiumAtAge(AgeUtils.calculateAgeNextBirthday((insuredIsSelf() ? getApplicantDob() : getInsuredDob())), getPaymentMode())));
            data.add(new ArrayList<>(Arrays.asList(String.format("Plan Name:<br>%s", PRODUCT_NAME), String.format("Premium Amount:<br>S$%s", StringUtils.isBlank(premium)?"NA":premium))));
            String riders = "";
            String riderMonthlyPremium="";
            if (getIncludeRefundRider()) {
                riders += "<br>Online Premium Refund Rider"; 
                riderMonthlyPremium+="<br>true";
           }
            if (getIncludeCriticalIllnessRider()) {
                riders += String.format("<br>Online Critical Illness Rider: S$%s",getRiderSumAssured()==null?"NA": df.format(getRiderSumAssured()));
                riderMonthlyPremium+="<br>S$"+df2.format(getCriticalIllnessRiderPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge()).get("modalPremium"));
            }
            //format coverage amount to two decimal places - HSBCICDU-630

          
            data.add(new ArrayList<>(Arrays.asList(String.format("Coverage:<br>S$%s", getSumAssured()==null?"NA":df2.format(getSumAssured())), 
            		String.format("Premium Discount Type: (if applicable)<br>%s", StringUtils.isBlank(getUnderwritingClassLabel())?"NA":getUnderwritingClassLabel()))));
            data.add(new ArrayList<>(Arrays.asList(String.format("Riders: (if applicable) %s", StringUtils.isBlank(riders)?"NA":riders),String.format("Rider Premium Amount:%s", StringUtils.isBlank(riderMonthlyPremium)?"NA":riderMonthlyPremium) )));
            data.add(new ArrayList<>(Arrays.asList(String.format("Payment Frequency:<br>%s", StringUtils.isBlank(getPaymentModeLabelPdf())?"NA":getPaymentModeLabelPdf()),String.format("Policy Term:<br>%s", getTermYears()+" years") )));


            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, "Insurance Details",HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);


        /*User entered values displayed in the pdf
    	 * Underwriting Questions Section */

        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("")));
            String medical = "";

            if (insuredIsSelf()) {
                data.add(new ArrayList<>(Arrays.asList(String.format("Have you smoked in the last 12 months?:<br>%s", (getApplicantSmoker() ? "Yes" : "No")))));
                medical += "1. Have you ever been diagnosed with cancer, heart disease or stroke?<br>";
                medical += "%s<br><br>";
                medical += "2. In the last 5 years, have you been diagnosed with or suffered from diabetes , HIV/AIDS or any medical condition affecting your brain, blood, heart, lungs, liver, kidneys for which you were required to undergo medical treatment for more than 14 days?<br>";
                medical += "%s<br><br>";
                medical += "3. In the last 12 months, have you had any symptom such as unexplained bleeding, weight loss, lump or growth, blood in the stool, chest pain or weakness of limbs for which you are still under investigation or have not yet sought medical advice?<br>";
                medical += "%s<br><br>";

                //JIRA HSBCIDCU-630 ("Have any of your parents or siblings ever suffered from cancer, heart disease, stroke or any hereditary condition before age 60?") must not reflect if CI is not added
                if (getIncludeCriticalIllnessRider()) {
                    medical += "4. Have any of your parents or siblings ever suffered from cancer, heart disease, stroke or any hereditary condition before age 60?<br>";
                    medical += "%s";
                }
            } else {
                data.add(new ArrayList<>(Arrays.asList(String.format("Has the Life Insured smoked in the last 12 months?:<br>%s", (getInsuredSmoker() ? "Yes" : "No")))));
                medical += "1. Has the Life Insured ever been diagnosed with cancer, heart disease or stroke?<br>";
                medical += "%s<br><br>";
                medical += "2. In the last 5 years, has the Life Insured been diagnosed with or suffered from diabetes , HIV/AIDS or any medical condition affecting the brain, blood, heart, lungs, liver, kidneys for which they were required to undergo medical treatment for more than 14 days?<br>";
                medical += "%s<br><br>";
                medical += "3. In the last 12 months, has the Life Insured had any symptom such as unexplained bleeding, weight loss, lump or growth, blood in the stool, chest pain or weakness of limbs for which they are still under investigation or have not yet sought medical advice?<br>";
                medical += "%s<br><br>";

                if (getIncludeCriticalIllnessRider()) {
                	 medical += "4. Have any of the Life Insured's parents or siblings ever suffered from cancer, heart disease, stroke or any hereditary condition before age 60?<br>";
                   medical += "%s";
                }
            }

            medical = String.format(medical,
                    (getInsuredMedical1()==null?"NA":getInsuredMedical1() ? "Yes" : "No"),
                    (getInsuredMedical2()==null ?"NA":getInsuredMedical2()? "Yes" : "No"),
                    (getInsuredMedical3()==null ? "NA":getInsuredMedical3()?"Yes" : "No"),
                    (getInsuredMedical4()==null ?"NA":getInsuredMedical4()? "Yes" : "No"));
            data.add(new ArrayList<>(Arrays.asList(medical)));
            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, "Life Insured's Health Details",HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);

            if (getUnderwritingLifestyleQuestionsOptin()) {
                String lifestyle = "";
                List<List> dataLifeStyle = new ArrayList();
                dataLifeStyle.add(new ArrayList<>(Arrays.asList("")));
                lifestyle += "a. When was the last mortgage approved?<br>";
                lifestyle += "%s<br><br>";
                lifestyle += "b. HSBC Customer Profile<br>";
                lifestyle += "%s<br><br>";
                lifestyle += "c. Height / Weight<br>";
                lifestyle += "%s cm / %s kg<br><br>";
                lifestyle += "d. How often does the life insured exercise?<br>";
                lifestyle += "%s";
                lifestyle = String.format(lifestyle,
                        getUnderwritingLifestyleMortgageLabel(),
                        getUnderwritingLifestyleProfileLabel(),
                        getUnderwritingLifestyleHeight(),
                        getUnderwritingLifestyleWeight(),
                        getUnderwritingLifestyleExerciseLabel());
                dataLifeStyle.add(new ArrayList<>(Arrays.asList(lifestyle)));
                BaseTable lifedataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
                PlanDataTable lifet = new PlanDataTable(lifedataTable, page);
                lifet.addListToTable(dataLifeStyle, PlanDataTable.NOHEADER, "Underwriting Questions",HorizontalAlignment.LEFT);
                yStart = lifedataTable.draw() - 20;
            }


        } catch (Exception e) {
            e.printStackTrace();

        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);

        /*User entered values displayed in the pdf
    	 *Country of Residence for Tax Purposes and related Taxpayer
    	 *Identification Number or equivalent number (TIN) */


        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("Country of Tax Residence", "TIN", "If no TIN available Reasons as stated")));
            if (getApplicantTaxCountry1() != null) {
                String reason = "NA";
                if (getApplicantTaxCountry1NoTinReason() != null) {
                    reason = getApplicantTaxCountryNoTinReasonLabel(getApplicantTaxCountry1NoTinReason());
                    if (getApplicantTaxCountry1NoTinReasonCustom() != null) {
                        reason += "<br>" + getApplicantTaxCountry1NoTinReasonCustom();
                    }
                }
                data.add(new ArrayList<>(Arrays.asList(getApplicantTaxCountry1(), StringUtils.isBlank(getApplicantTaxCountry1Tin())?"NA":getApplicantTaxCountry1Tin(), reason)));
            }
            if (getApplicantTaxCountry2() != null) {
                String reason = "NA";
                if (getApplicantTaxCountry2NoTinReason() != null) {
                    reason = getApplicantTaxCountryNoTinReasonLabel(getApplicantTaxCountry2NoTinReason());
                    if (getApplicantTaxCountry2NoTinReasonCustom() != null) {
                        reason += "<br>" + getApplicantTaxCountry2NoTinReasonCustom();
                    }
                }
                data.add(new ArrayList<>(Arrays.asList(getApplicantTaxCountry2(), StringUtils.isBlank(getApplicantTaxCountry2Tin())?"NA":getApplicantTaxCountry2Tin(), reason)));
            }
            if (getApplicantTaxCountry3() != null) {
                String reason = "NA";
                if (getApplicantTaxCountry3NoTinReason() != null) {
                    reason = getApplicantTaxCountryNoTinReasonLabel(getApplicantTaxCountry3NoTinReason());
                    if (getApplicantTaxCountry3NoTinReasonCustom() != null) {
                        reason += "<br>" + getApplicantTaxCountry3NoTinReasonCustom();
                    }
                }
                data.add(new ArrayList<>(Arrays.asList(getApplicantTaxCountry3(), StringUtils.isBlank(getApplicantTaxCountry3Tin())?"NA":getApplicantTaxCountry3Tin(), reason)));
            }

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.HASHEADER, "Country of Residence for Tax Purposes and related Taxpayer Identification Number or equivalent number (\"TIN\")",HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {

        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        if (getApplicantAddressNoTaxReason() != null) {
            yStart += 21;
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
            try {
                List<List> data = new ArrayList();

                data.add(new ArrayList<>(Arrays.asList(String.format("Reason if Residential/Mailing Address doesn't match Country of Tax Residence:<br>%s" ,StringUtils.isBlank(getApplicantAddressNoTaxReason())?"NA":getApplicantAddressNoTaxReason()))));

                BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
                PlanDataTable t = new PlanDataTable(dataTable, page);
                t.addListToTable(data, PlanDataTable.NOHEADER, "",HorizontalAlignment.LEFT);
                yStart = dataTable.draw() - 20;
            } catch (Exception e) {

            }
        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);

        /*User entered values displayed in the pdf
    	 *My Payment Details Section */
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("", "")));
            data.add(new ArrayList<>(Arrays.asList(String.format("Name on Card:<br>%s", StringUtils.isBlank(getCreditCardName())?"NA":getCreditCardName()), String.format("Credit Card Number:<br>%s", StringUtils.isBlank(getCreditCardNum())?"NA":getCreditCardNum()))));
            data.add(new ArrayList<>(Arrays.asList(String.format("Expiry Date:<br>%s", StringUtils.isBlank(getCreditCardExpiry())?"NA":getCreditCardExpiry()), String.format("Payment Type:<br>%s", StringUtils.isBlank(getCreditCardTypeLabel())?"NA":getCreditCardTypeLabel()))));
           /* Displaying credit card campaign code as a part of 856 */
            if (isForAdmin) {
              data = new ArrayList<>(data);
              data.add(Arrays.asList(
                  String.format("Credit Card Campaign Code:<br>%s",
                      CreditCardUtils.getCreditCardName(getCreditCardNum())),
                  String.format("")));
            }
            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, (insuredIsSelf() ? "My Payment Details" : "Payment Details"),HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {
        	logger.error("error "+e);
        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);

        /*User entered values displayed in the pdf
    	 *Prominent Public Position* Declaration */

        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("")));
            data.add(new ArrayList<>(Arrays.asList(String.format("Have you or any person in connection with this policy* ever been a Politically Exposed Person (PEP), a family member of a PEP or close associate of a PEP?<br>%s", (getApplicantIsPep() ? "Yes" : "No")))));
            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, "Prominent Public Position* Declaration",HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 10;
        } catch (Exception e) {

        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        String prominentDec="*This includes policy owner, life insured, beneficial owner(s) and beneficiary(ies). A PEP is an individual who is or has been entrusted with prominent public positions in Singapore, a foreign country or an international organisation, which includes a current or former senior official in the executive, legislative, administrative, military or judicial branches of a government or a government agency, a member of a ruling royal family, a senior official of a political party, a senior executive of a government-owned or government-funded corporation, institution or charity and a senior executive of an international organisation.";

        try {
        	pd.setFont(PDFGenerator.FONT_NORMAL,8);
            yStart = PDFLayout.mainTitleListLeft(new String[]{
                    prominentDec,
            }, yStart, yStartNewPage, tableWidth, page, pd, 20);
            pd.setBuiltInFont(null, 9);
            
        }
        catch(Exception e){
            logger.error("Error Message "+e);
        }

   		/*User entered values displayed in the pdf
      	 *Declaration of Beneficial Ownership */
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("", "")));
            String boDetails = "";
            if (!getApplicantIsBeneficalOwner()) {
                boDetails += String.format("Surname: %s<br>", getBeneficialOwnerLastName());
                boDetails += String.format("Given Name: %s<br>", getBeneficialOwnerFirstName());
                boDetails += String.format("NRIC/Passport Number: %s<br>", getBeneficialOwnerNric());
                boDetails += String.format("Relationship to Benefical Owner: %s", getBeneficialOwnerRelation());
            }
            data.add(new ArrayList<>(Arrays.asList(String.format("Are you the Beneficial Owner\u002A of the Policy?:<br>%s", (getApplicantIsBeneficalOwner() ? "Yes" : "No")), String.format("If you are not the Beneficial Owner we are required by regulation to ask for the Name, nature of relationship and Identity documents of the Beneficial Owner.<br><br>%s", StringUtils.isBlank(boDetails)?"NA":boDetails))));

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, "Declaration of Beneficial Ownership",HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 10;
        } catch (Exception e) {

        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        String beneficialDec="*You are the Beneficial Owner if you are the individual who either controls the applicant or on whose behalf the policy was purchased. However it does not constitute nominating a Beneficiary who receives the policy proceeds";

        try {
        	pd.setFont(PDFGenerator.FONT_NORMAL,8);
            yStart = PDFLayout.mainTitleListLeft(new String[]{
                    beneficialDec
            }, yStart, yStartNewPage, tableWidth, page, pd, 20);
            
            pd.setBuiltInFont(null, 9);
        }
        catch(Exception e){
            logger.error("Error Message "+e);
        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        page = pd.createPage();
        pd.addPage(page);
        yStart=yStartNewPage;
        /*User entered values displayed in the pdf
      	 *Declarations Section */

        try {
            /*acknowledgement and dec table ctreation*/
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
            PDFLayout.border=5;
            /*added the declarations in the list of list to use data table to display the declarations*/
            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(dataDec, PlanDataTable.NOHEADER, "Declarations and Acknowledgements",HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;
            PDFLayout.border=0;
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
            /*acknowledgement and dec table ctreation*/

        } catch (Exception e) {
        }
        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        try {
            List<List> data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList(String.format("Acknowledgement Date: %s", (new Date())))));

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            t.addListToTable(data, PlanDataTable.NOHEADER, "",HorizontalAlignment.LEFT);
            yStart = dataTable.draw() - 20;

            for (int jj = 0; jj < pd.getDocument().getNumberOfPages(); jj++) {
                page = pd.getDocument().getPage(jj);
                pd.applyPageFooter(page, jj + 1);
                pd.setRotationFactor(90);
                pd.setBuiltInFont(font, 7);
                pd.writeText(page, "SG HSBCONLINE JAN 19 2018",page.getMediaBox().getWidth()-140 ,20);
                pd.setBuiltInFont(null, 7);
                pd.setRotationFactor(0);
            }

        } catch (Exception e) {

        }
    }
    /*Logic to parse the file with declarations
     * into array which is used to be written
     * on the pdf 
     * 
     * filedeclaration0 The file to be parsed from html to text
     * dataDec          The list of list used by the datatable to populate 
     *                  on the pdf
     *  Return          The declarations to be displayed in the pdf
     *  List<List>
     * 
     * */
    private List<List> parsingHtmlToText(File fileDeclaration0, List<List> dataDec) {


        final String AGGREGATE_TEXT = "all my existing and pending HSBC Insurance OnlineProtector applications do not exceed an aggregate coverage of";
        final String CRITICAL_ILLNESS_AGGREGATE_TEXT = "all my existing and pending HSBC Insurance Online Critical Illness Rider applications for critical illness cover in aggregate do not exceed";
        final String JAPAN_NATION = "I am agreeable to complete the Declaration of Non-Japanese Residency in the Supplementary Proposal Form as part of my application for this policy";
        final String JAPAN_NATION_SPOUSE = "we are agreeable to complete the Declaration of Non-Japanese Residency in the Supplementary Proposal Form as part of our application for this policy";
        final String AGGREGATE_TEXT_SPOUSE = "all our existing and pending HSBC Insurance OnlineProtector applications do not exceed an aggregate coverage of";
        final String CRITICAL_ILLNESS_AGGREGATE_TEXT_SPOUSE = "all our existing and pending HSBC Insurance Online Critical Illness Rider applications for critical illness cover in aggregate do not exceed";
        final String BOLD1 = "I understand the nature, objective and benefits of the Product and have read and acknowledged the information";
        final String BOLD2 = "I have initiated the purchase of this product and the entire application is made while I am a resident in Singapore";
        final String BOLD3 = "I understand that I have a 30-day free-look period to cancel a policy after HSBC Insurance confirms acceptance of my";
        final String BOLD4 = "We understand the nature, objective and benefits of the Product and have read and acknowledged the information";
        final String BOLD5 = "we have initiated the purchase of this product and the entire application is made while we are residents in Singapore";
        final String BOLD6 = "We understand that we have a 30-day free-look period to cancel a policy";
        final String BOLD7 = "I understand the nature, objective and benefits of the Product and the CI Rider and have read and acknowledged the information";
        final String BOLD8 = "We understand the nature, objective and benefits of the Product and the CI Rider and have read and acknowledged the information";
        final String IGNORE1 = "Your Guide to Health Insurance (if applicable)";
        final String IGNORE2 = "in the Product Summary and General Provision";
        final String IGNORE3 = "in the Product Summary and Annexure related to HSBC Insurance Online Critical Illness Rider";
        final String IGNORE4 = "If I am a customer of, or become a customer of, HSBC Bank (Singapore) Limited (\"HSBC Bank\"), I agree that";
        final String IGNORE5 = "I consent to the use and disclosure of my personal data provided in this form to the HSBC Group and its respective agents";
        final String IGNORE6 = "If we are customer(s) of, or become a customer of, HSBC Bank (Singapore) Limited (\"HSBC Bank\"), we agree";
        final String IGNORE7 = "We consent to the use and disclosure of our personal data provided in this form to the HSBC Group and its respective";
        final String RED1 = "the information given by me to HSBC Insurance in this application form or in any form or document relating to this";
        final String RED2 = "I understand that the commencement of risk coverage under the policy is subject to the receipt by HSBC Insurance of";
        final String RED3 = "the information given by us to HSBC Insurance in this application form or in any form or document relating to this";
        final String RED4 = "We understand that the commencement of risk coverage under the policy is subject to the receipt by HSBC Insurance";
        final String RED5 = "in the Benefit Illustration";
        final String RED6 = "in the Product Summary and General Provision";
        final String RED7 = "in the Product Summary and Annexure related to HSBC Insurance Online Critical Illness Rider";
        final String PDPA = "Your privacy is important to us.";
        final String titleText="In proceeding to make this purchase";

        // TODO Auto-generated method stub
        //Adds the file parsing logic and putting the string in a array
        try{
            String fileReceived=FileUtils.readFileToString(fileDeclaration0,"utf-8");
            int jspScript =StringUtils.lastIndexOf(fileReceived, "%>");
            fileReceived=StringUtils.substring(fileReceived, jspScript+2);
            int indexChoose=StringUtils.indexOf(fileReceived, "<c:choose>");
            int indexEndChoose=StringUtils.indexOf(fileReceived, "</c:choose>");
            String prev=StringUtils.substring(fileReceived, 0, indexChoose);
            String next=StringUtils.substring(fileReceived,indexEndChoose+11);
            if(indexChoose!=-1){
                fileReceived=prev+" "+next;
            }
            Document actualTags=Jsoup.parse(fileReceived);

            Elements formTag=actualTags.children();
            int liElements=0;
            for(int k=0;k<formTag.size();k++){
                Element n=formTag.get(k);
                Elements children=n.getAllElements();
                for(int i=0;i<children.size();i++){
                    String tagNameRetrieved=children.get(i).tagName();
                    Element elementencountered=children.get(i);
                    if(StringUtils.equalsIgnoreCase(tagNameRetrieved, "ol")){
                        Elements childOrdered=elementencountered.children();
                        for(int o=0;o<childOrdered.size();o++){
                        	if(childOrdered.get(o).hasAttr("inner-intended")){
                        		dataDec.add(new ArrayList<>(Arrays.asList("<inner-intend>\u25E6",childOrdered.get(o).text())));
                        	}else{
                             dataDec.add(new ArrayList<>(Arrays.asList("\u25E6",childOrdered.get(o).text())));
                        	}
                            int childOfolLinks=childOrdered.get(o).children().size();
                            if(childOfolLinks>0){
                                i=i+childOfolLinks;
                            }
                            if(o==childOrdered.size()-1){

                                i=i+childOrdered.size();
                            }
                        }

                    }else if(StringUtils.equalsIgnoreCase(tagNameRetrieved, "ul")){
                        Elements parentsOrdered=elementencountered.parents();
                        for(int p=0;p<parentsOrdered.size();p++){
                            Element parent=parentsOrdered.get(p);
                            if(StringUtils.equalsIgnoreCase(parent.tagName(),"ul")){
                                Elements innerUlChild= elementencountered.children();


                                for(int innerChild=0;innerChild<innerUlChild.size();innerChild++){
                                    if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),RED5) ||
                                            StringUtils.startsWithIgnoreCase(elementencountered.text(),RED6) ||
                                            StringUtils.startsWithIgnoreCase(elementencountered.text(),RED7) ) {
                                    	 dataDec.add(new ArrayList<>(Arrays.asList("\u25E6", "<red>"+ innerUlChild.get(innerChild).text())));
                                    } else{	
                                    String html=innerUlChild.get(innerChild).html();
          	  						String before=StringUtils.substringBefore(html, "<br>");
          	  						before=StringUtils.replace(before, "<li>", "");
          	  						String text=innerUlChild.get(innerChild).text();
          	  						if(StringUtils.contains(html, "<br>")){
          	  							text=before+"<br>  "+StringUtils.substringAfter(text, before);
          	  						}
          	  					if(innerUlChild.get(innerChild).hasAttr("inner-intended")){
                            		dataDec.add(new ArrayList<>(Arrays.asList("<inner-intend>\u25E6",String.format(text))));
                            	}else{
                                 dataDec.add(new ArrayList<>(Arrays.asList("\u25E6",String.format(text))));
                            	}
                                }
                                    int childOfolLinks=innerUlChild.get(innerChild).children().size();
                                    if(childOfolLinks>0){
                                        i=i+childOfolLinks;
                                    }
                                    if(innerChild==innerUlChild.size()-1){

                                        i=i+innerUlChild.size();
                                    }
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            }
                        }

                    }else if(StringUtils.equalsIgnoreCase(tagNameRetrieved, "li")){
                        liElements=liElements+1;
                        boolean ownText=false;
                        Elements child=elementencountered.children();
                        for(int l=0;l<child.size();l++){
                            String tag=child.get(l).tagName();
                            if(StringUtils.equalsIgnoreCase(tag, "ol")||StringUtils.equalsIgnoreCase(tag, "ul")){
                                ownText=true;
                            }
                        }
                        if(ownText){
                            //Print this sentence "Applicable only to Japanese Nationals residing outside Japan..." in pdf if nationality is japanese only
                            if ( StringUtils.startsWithIgnoreCase(elementencountered.ownText(),JAPAN_NATION) ) {
                                if ( getApplicantNationality().equals("Japan") || getApplicantNationality2().equals("Japan") || getApplicantNationality3().equals("Japan") ) {
                                	logger.info("the japan text is "+elementencountered.ownText());
                                    dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"), String.format(elementencountered.ownText()))));
                                    dataDec.add(new ArrayList<>(Arrays.asList("")));
                                }
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.ownText(),JAPAN_NATION_SPOUSE) ) {
                            	if ( getApplicantNationality().equals("Japan") || getApplicantNationality2().equals("Japan") || getApplicantNationality3().equals("Japan") ||
                                     getInsuredNationality().equals("Japan") || getInsuredNationality2().equals("Japan") || getInsuredNationality3().equals("Japan") ) {
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"), String.format(elementencountered.ownText()))));
                            		 dataDec.add(new ArrayList<>(Arrays.asList("")));
                                }
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.ownText(),AGGREGATE_TEXT) ) {
                                String aggregate = AGGREGATE_TEXT;
                                if (applicantIsWorking()) {
                                    aggregate = aggregate + " S$1million per life";
                                } else {
                                    aggregate = aggregate + " S$500,000 per life";
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"), String.format(aggregate))));
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.ownText(),AGGREGATE_TEXT_SPOUSE) ) {
                                String aggregate = AGGREGATE_TEXT_SPOUSE;
                                if (insuredIsWorking()) {
                                    aggregate = aggregate + " S$1million per life";
                                } else {
                                    aggregate = aggregate + " S$500,000 per life";
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"), String.format(aggregate))));
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.ownText(),CRITICAL_ILLNESS_AGGREGATE_TEXT) ) {
                                String aggregate2 = CRITICAL_ILLNESS_AGGREGATE_TEXT;
                                if (!applicantIsWorking()) {
                                    aggregate2 = aggregate2 + " S$400,000 per life";
                                } else { //working
                                    if (AgeUtils.calculateAgeNextBirthday(getApplicantDob())< 50) {
                                        aggregate2 = aggregate2 + " S$650,000 per life";
                                    } else if (AgeUtils.calculateAgeNextBirthday(getApplicantDob()) >= 50 && AgeUtils.calculateAgeNextBirthday(getApplicantDob()) <= 60) {
                                        aggregate2 = aggregate2 + " S$500,000 per life";
                                    }
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format( aggregate2))));
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.ownText(),CRITICAL_ILLNESS_AGGREGATE_TEXT_SPOUSE) ) {
                                String aggregate2 = CRITICAL_ILLNESS_AGGREGATE_TEXT_SPOUSE;
                                if (!insuredIsWorking()) {
                                    aggregate2 = aggregate2 + " S$400,000 per life";
                                } else { //working
                                    if (AgeUtils.calculateAgeNextBirthday(getInsuredDob())< 50) {
                                        aggregate2 = aggregate2 + " S$650,000 per life";
                                    } else if (AgeUtils.calculateAgeNextBirthday(getInsuredDob()) >= 50 && AgeUtils.calculateAgeNextBirthday(getApplicantDob()) <= 60) {
                                        aggregate2 = aggregate2 + " S$500,000 per life";
                                    }
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(aggregate2))));
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else {
                            	if(elementencountered.hasAttr("intended")){
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(elementencountered.ownText()))));
                            	}else{
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format(elementencountered.ownText()))));
                            	}
                            }
                         }else{
                            //Print this sentence "Applicable only to Japanese Nationals residing outside Japan..." in pdf if nationality is japanese only
                            //fix aggregate amounts not appearing in pdf
                            if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),JAPAN_NATION) ) {
                                if ( getApplicantNationality().equals("Japan") || getApplicantNationality2().equals("Japan") || getApplicantNationality3().equals("Japan") ){
                                    dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(elementencountered.text()))));
                                    dataDec.add(new ArrayList<>(Arrays.asList("")));
                                    
                                }
                            }else if ( StringUtils.startsWithIgnoreCase(elementencountered.ownText(),JAPAN_NATION_SPOUSE) ) {
                             	if ( getApplicantNationality().equals("Japan") || getApplicantNationality2().equals("Japan") || getApplicantNationality3().equals("Japan") ||
                                      getInsuredNationality().equals("Japan") || getInsuredNationality2().equals("Japan") || getInsuredNationality3().equals("Japan") ) {      
                             		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"), String.format(elementencountered.text()))));
                             		 dataDec.add(new ArrayList<>(Arrays.asList("")));
                                 }
                             }   else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),AGGREGATE_TEXT) ) {
                                String aggregate = AGGREGATE_TEXT;
                                if (applicantIsWorking()) {
                                    aggregate = aggregate + " S$1million per life";
                                } else {
                                    aggregate = aggregate + " S$500,000 per life";
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(aggregate))));
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),AGGREGATE_TEXT_SPOUSE) ) {
                                String aggregate = AGGREGATE_TEXT_SPOUSE;
                                if (insuredIsWorking()) {
                                    aggregate = aggregate + " S$1million per life";
                                } else {
                                    aggregate = aggregate + " S$500,000 per life";
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(aggregate))));
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),CRITICAL_ILLNESS_AGGREGATE_TEXT) ) {
                                String aggregate2 = CRITICAL_ILLNESS_AGGREGATE_TEXT;
                                if (!applicantIsWorking()) {
                                    aggregate2 = aggregate2 + " S$400,000 per life";
                                } else {
                                    if (AgeUtils.calculateAgeNextBirthday(getApplicantDob())< 50) {
                                        aggregate2 = aggregate2 + " S$650,000 per life";
                                    } else if (AgeUtils.calculateAgeNextBirthday(getApplicantDob()) >= 50 && AgeUtils.calculateAgeNextBirthday(getApplicantDob()) <= 60) {
                                        aggregate2 = aggregate2 + " S$500,000 per life";
                                    }
                                }
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(aggregate2))));
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),CRITICAL_ILLNESS_AGGREGATE_TEXT_SPOUSE) ) {
                                String aggregate2 = CRITICAL_ILLNESS_AGGREGATE_TEXT_SPOUSE;
                                if (!insuredIsWorking()) {
                                    aggregate2 = aggregate2 + " S$400,000 per life";
                                } else {
                                    if (AgeUtils.calculateAgeNextBirthday(getInsuredDob())< 50) {
                                        aggregate2 = aggregate2 + " S$650,000 per life";
                                    } else if (AgeUtils.calculateAgeNextBirthday(getInsuredDob()) >= 50 && AgeUtils.calculateAgeNextBirthday(getApplicantDob()) <= 60) {
                                        aggregate2 = aggregate2 + " S$500,000 per life";
                                    }
                                }
                            
                                dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(aggregate2))));
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if (StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE1) ||
                                    StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE2) ||
                                    StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE3) ) {
                                logger.info("not taking duplicate:" + IGNORE1 + "," + IGNORE2 + "," + IGNORE3);
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),BOLD1) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD2) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD3) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(),BOLD4) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD5) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD6) ||
                                        StringUtils.startsWithIgnoreCase(elementencountered.text(),BOLD7) ||
                                        StringUtils.equalsIgnoreCase(elementencountered.text(),BOLD8) ) {     
                            	if(StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD5)||StringUtils.startsWithIgnoreCase(elementencountered.text(), BOLD2)){
                                    dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format("<b>"+elementencountered.text()+"</b>"))));
                            	}else{
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format("<b>"+elementencountered.text()+"</b>"))));
                            	}
                                 dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(),RED1) ||
                                    StringUtils.startsWithIgnoreCase(elementencountered.text(),RED2) ||
                                    StringUtils.startsWithIgnoreCase(elementencountered.text(),RED3) ||
                                    StringUtils.startsWithIgnoreCase(elementencountered.text(),RED4) ) {
                            	if(StringUtils.startsWithIgnoreCase(elementencountered.text(),RED1)||StringUtils.startsWithIgnoreCase(elementencountered.text(),RED3)){
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format("<red>"+elementencountered.text()))));
                            	}else{
                               dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format("<red>"+elementencountered.text()))));
                            	}
                            	dataDec.add(new ArrayList<>(Arrays.asList("")));
                            } else {
                            	
                            	if(elementencountered.hasAttr("intended")){
                            		dataDec.add(new ArrayList<>(Arrays.asList(String.format("<intend>\u2022"),String.format(elementencountered.text()))));
                            	}else{
                                 dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format(elementencountered.text()))));
                            	}
                                dataDec.add(new ArrayList<>(Arrays.asList("")));
                            }
                        }

                    }else if(StringUtils.equalsIgnoreCase(tagNameRetrieved, "h3")||StringUtils.equalsIgnoreCase(tagNameRetrieved, "p")){
                        //this fixes blank signatures in pdf ...HSBCIDCU-630
                        if ( StringUtils.equals(elementencountered.text(),"I accept ()")||StringUtils.equals(elementencountered.text(),"checked> I accept ()") ) {
                            String iAccept = "<img-check>I accept (" + getApplicantFullName() + ")";
                            dataDec.add(new ArrayList<>(Arrays.asList(iAccept)));
                        } else if ( StringUtils.equals(elementencountered.text(),"We accept ( & )")||StringUtils.equals(elementencountered.text(),"checked> We accept ( & )") ) {
                            String weAccept = "<img-check>We accept (" + getApplicantFullName() + " & " + getInsuredFullName() + ")";
                            dataDec.add(new ArrayList<>(Arrays.asList(weAccept)));
                        } else if ( StringUtils.equals(elementencountered.text(),"I agree with the statement below") 
                                    || StringUtils.equals(elementencountered.text(),"checked> I agree with the statement below") ) {
                            logger.info("Ignore these:I agree with the statement below");
                        } else if ( StringUtils.equals(elementencountered.text(),"We and agree with the statement below") 
                                    || StringUtils.equals(elementencountered.text(),"checked> We and agree with the statement below") ) {
                            logger.info("Ignore these:We and agree with the statement below");
                        } else if ( StringUtils.startsWith(elementencountered.text(), "checked>") ) {
                        	logger.info("the string checked is "+elementencountered.text());
                            String checked = StringUtils.removeStart(elementencountered.text(), "checked> ");
                            checked="<img-check>"+checked;
                            dataDec.add(new ArrayList<>(Arrays.asList(checked)));
                        } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(), PDPA) ) {
                        	 dataDec.add(new ArrayList<>(Arrays.asList(String.format("\u2022"),String.format(elementencountered.text()))));
                        	 dataDec.add(new ArrayList<>(Arrays.asList("")));
                        } else if ( StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE4) ||
                                StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE5) ||
                                StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE6) ||
                                StringUtils.startsWithIgnoreCase(elementencountered.text(), IGNORE7) ) {
                           logger.info("Ignore these:" + IGNORE4);
                        } else{
                        	if(StringUtils.isNotBlank(elementencountered.text())){
                        		if(StringUtils.startsWithIgnoreCase(elementencountered.text(), titleText)){
                        			dataDec.add(new ArrayList<>(Arrays.asList(elementencountered.text())));
                        		}else{
	                        	   dataDec.add(new ArrayList<>(Arrays.asList("para",elementencountered.text())));
                        		}
	                        	dataDec.add(new ArrayList<>(Arrays.asList("")));
                        	}
                        }
                    }

                }
            }

        }catch(Exception e){
            logger.error("error in parsing "+e);
        }       
		return dataDec;
    }

    private void generateBIPDFPage1(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);

        try {
            pd.setFont(PDFGenerator.FONT_BOLD, 9);
            BaseTable table = new BaseTable(yStartNewPage, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
            Row r1 = table.createRow(9);
            String title = "Consolidated Summary for Benefit Illustration (HSBC Insurance OnlineProtector)";
            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {
                title = "Consolidated Summary for Benefit Illustration (HSBC Insurance OnlineProtector with Riders)";
            }
            Cell c1 = r1.createCell(100, title, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(9);
            c1.setBottomPadding(10);
            c1.setTopPadding(0);
            c1.setLeftPadding(0);
            Row r2 = table.createRow(9);
            Cell c2 = r2.createCell(100, "Policyowner and Plan Details", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c2.setFont(pd.getFont());
            c2.setFontSize(9);
            c2.setBottomPadding(0);
            c2.setTopPadding(0);
            c2.setLeftPadding(0);
            yStart = table.draw() - 20;

            table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);

            List<String[]> rows = new ArrayList<>();
            rows.add(new String[]{"Applicant Name", getApplicantFullName()});
            rows.add(new String[]{"", ""});
            rows.add(new String[]{"Proposed Life Insured", (insuredIsSelf() ? getApplicantFullName() : getInsuredFullName())});
            rows.add(new String[]{"Age Next Birthday", (insuredIsSelf() ? Integer.toString(AgeUtils.calculateAgeNextBirthday(getApplicantDob())) : Integer.toString(AgeUtils.calculateAgeNextBirthday(getInsuredDob())))});
            rows.add(new String[]{"Sex", (insuredIsSelf() ? getApplicantGenderLabel() : getInsuredGenderLabel())});
            rows.add(new String[]{"Smoker / Non Smoker", (insuredIsSelf() ? (getApplicantSmoker() ? "Smoker" : "Non-smoker") : (getInsuredSmoker() ? "Smoker" : "Non-smoker"))});
            rows.add(new String[]{"Underwriting Class", getUnderwritingClassLabel()});
            rows.add(new String[]{"", ""});
//            rows.add(new String[]{String.format("%s Premium", getPaymentModeLabel()), "S$" + df.format((double)getPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge()).get("modalPremium"))});
            rows.add(new String[]{String.format("%s Premium", getPaymentModeLabel()), "S$" + df2.format(getPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge()).get("modalPremium"))});
            rows.add(new String[]{"", ""});
            rows.add(new String[]{"Plan", "HSBC Insurance OnlineProtector"});
            rows.add(new String[]{"Policy Term", "10 Years"});
            rows.add(new String[]{"Premium Payment Term", "10 Years"});
            rows.add(new String[]{"Basic Sum Insured", "S$" + df2.format(getSumAssured())});

            int expiryAge = AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob()) + getTermYears();

            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {
                rows.add(new String[]{"", ""});
                rows.add(new String[]{"Riders Attached", "Riders", "Sum Insured", "Expiry Age", String.format("%s Premium", getPaymentModeLabel())});
                if (getIncludeRefundRider()) {
                    rows.add(new String[]{"HSBC Insurance Online Premium Refund Rider", "-", String.format("%s", expiryAge), "S$" + df2.format(getRefundRiderPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge()).get("modalPremium"))});
                }
                if (getIncludeCriticalIllnessRider()) {
                	double ciRider=(double) getCriticalIllnessRiderPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge()).get("modalPremium");          	 
                	 double ciRiderTrunc=Math.round(ciRider * 100.0) / 100.0;
                    rows.add(new String[]{"HSBC Insurance Online Critical Illness Rider", "S$" + df2.format(getRiderSumAssured()), String.format("%s", expiryAge), "S$" + df2.format(ciRiderTrunc)});
                }
            }

            for (int i = 0; i < rows.size(); i++) {
                if (i == 15) {
                    pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY3);
                } else {
                    if (i == 16 || i == 17) {
                        pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY4);
                    } else {
                        pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY2);
                    }
                }

            }

            yStart = table.draw() - 10;
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

        try {
            BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
            Row r1 = table.createRow(9);
            Cell c1 = r1.createCell(100, "<b>Below is the summary of what I intend to purchase:</b>", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(9);
            c1.setBottomPadding(0);
            c1.setTopPadding(0);
            yStart = table.draw() - 10;
        } catch (Exception e) {

        }

        try {
            ArrayList<Map<String, Object>> bi = getBenefitIllustration(); // Get basic product year, age and premium
            ArrayList<Map<String, Object>> cibi = getIncludeCriticalIllnessRider() ? getCriticalIllnessRiderBenefitIllustration() : null;
            ArrayList<Map<String, Object>> ropbi = getIncludeRefundRider() ? getRefundRiderBenefitIllustration() : null;
            List<List> data = new ArrayList();

            ArrayList tuple = new ArrayList<>(Arrays.asList("", String.format("The Base Product<br>%s", (getIncludeCriticalIllnessRider() || getIncludeRefundRider() ? "(For comparison)" : "(Your selected plan)"))));
            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {
                if (getIncludeCriticalIllnessRider() && getIncludeRefundRider()) {
                    tuple.add(1, "The Base Product, the PR Rider and the CI Rider<br>(Your selected plan)");
                } else {
                    if (getIncludeCriticalIllnessRider()) {
                        tuple.add(1, "The Base Product and the CI Rider<br>(Your selected plan)");
                    } else {
                        tuple.add(1, "The Base Product and the PR Rider<br>(Your selected plan)");
                    }
                }
            }
            data.add(tuple);

            double value = (double)bi.get(0).get("modalPremium") * getPaymentMode();
            tuple = new ArrayList<>(Arrays.asList("How much you pay in a year", String.format("S$%s annually for 10 years", df2.format(value))));
            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {
                if (getIncludeCriticalIllnessRider()) {
                    value += (double)cibi.get(0).get("modalPremium") * getPaymentMode();
                }
                if (getIncludeRefundRider()) {
                    value += (float)ropbi.get(0).get("modalPremium") * getPaymentMode();
                }
                tuple.add(1, String.format("S$%s annually for 10 years", df2.format(value)));
            }
            data.add(tuple);

            value = (float)bi.get(9).get("premiumPaid");
            tuple = new ArrayList<>(Arrays.asList("How much you pay for the first 10 years", String.format("S$%s in total premium", df2.format(value))));
            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {
                if (getIncludeCriticalIllnessRider() && getIncludeRefundRider()) {
                    if (getIncludeRefundRider()) {
                        value += (float)ropbi.get(9).get("premiumPaid");
                    }
                    tuple.add(1, String.format("S$%s in total premium for Base Product and PR Rider<br>+<br>S$%s in total premium for CI Rider", df2.format(value), df2.format((float)cibi.get(9).get("premiumPaid"))));

                } else {
                    if (getIncludeCriticalIllnessRider()) {
                        value += (float)cibi.get(9).get("premiumPaid");
                    }
                    if (getIncludeRefundRider()) {
                        value += (float)ropbi.get(9).get("premiumPaid");
                    }
                    tuple.add(1, String.format("S$%s in total premium", df2.format(value)));
                }
            }
            data.add(tuple);

            tuple = new ArrayList<>(Arrays.asList("What will be my Sum Insured payout", String.format("S$%s",  df2.format(getSumAssured()))));
            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {
                if (getIncludeCriticalIllnessRider()) {
                    tuple.add(1, String.format("S$%s for Base Product<br><br>S$%s for CI Rider", df2.format(getSumAssured()), df2.format(getRiderSumAssured())));
                } else {
                    tuple.add(1, String.format("S$%s for Base Product", df2.format(getSumAssured())));
                }
            }
            data.add(tuple);

            tuple = new ArrayList<>(Arrays.asList("What will you get back at the end of the 10th year if there is no claims", "Nil"));
            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {
                if (getIncludeRefundRider()) {
//                    df.format((float)result.get("premiumPaid") * 1.1)
                    tuple.add(1, String.format("S$%s<br>", df2.format((float)ropbi.get(9).get("premiumPaid") * 1.1)) +
                            "<br>Note:<ol>" +
                            "<li>You will only get back the above refund of premium benefit if you pay all the 10 scheduled Annual Premiums for the Base Product and the PR Rider.</li>" +
                            "<li>You will get a reduced Maturity Value refund of premium benefit if you stop payment after you paid the first 5 years of premiums in full for the Base Product and PR Rider.</li>" +
                            "<li>You will not get back any refund of premium benefit if you stop payment before the first 5 years of premiums are paid in full for the Base Product and PR Rider and your policy will lapse.</li>" +
                            "</ol>" +
                            "<br>Please see the Product Summary and Annexure for details<br>");
                } else {
                    tuple.add(1, "Nil");
                }
            }
            data.add(tuple);

            tuple = new ArrayList<>(Arrays.asList("Renewability", "Yes. The Product is renewable at the end of each 10-year policy term, subject to maximum renewal age of 80 next birthday. The policy will renew for 10 years at each renewal.\\n\\nThe premiums at point of renewal will change according to your age and the prevailing rates applicable at point of renewal."));//switch to <br> if necessary
            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {  	
                if (getIncludeCriticalIllnessRider() && getIncludeRefundRider()) {
                    tuple.add(1, "Yes. The Base Product, CI Rider and PR Rider are renewable at the end of each 10 year policy term subject maximum renewal ages as follow:<br><ol>" +
                            "<li>Base Product: age 80 next birthday</li>" +
                            "<li>CI Rider: age 80 next birthday</li>" +
                            "<li>PR Rider: age 55 (subject to the Rider availability)</li>" +
                            "</ol>The premiums for CI Rider is not guaranteed during the policy term. The premiums at point of renewal will change according to your age and rates applicable at point of renewal.<br>");
                } else {
                    if (getIncludeCriticalIllnessRider()) {
                        tuple.add(1, "Yes. The Base Product and CI Rider are renewable at the end of each 10 year policy term subject maximum renewal ages as follow:<br><ol>" +
                                "<li>Base Product: age 80 next birthday</li>" +
                                "<li>CI Rider: age 80 next birthday</li>" +
                                "</ol>The policy will renew for 10 years at each renewal." +
                                "<br>The premiums for the CI rider is not guaranteed during the policy term, and the premiums at point of renewal will change according to your age and the rates applicable at point of renewal.");
                    } else {
                        tuple.add(1, "Yes. The Base Product and PR Rider are renewable at the end of each 10 year policy term subject maximum renewal ages as follow:<br><ol>" +
                                "<li>Base Product: 80 next birthday</li>" +
                                "<li>PR Rider: 55 next birthday (subject to availability at point of renewal)</li>" +
                                "</ol>The policy will renew for 10 years at each renewal.<br>" +
                                "<br>The premiums at point of renewal will change according to your age and the rates applicable at point of renewal.");
                    }
                }
            }
           
            data.add(tuple);

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page);
            //JIRA HSBCIDCU-630 Correct policy name to OnlineProtector
            String title = "HSBC Insurance OnlineProtector (\"the Base Product\")";

            if (getIncludeCriticalIllnessRider() || getIncludeRefundRider()) {
                if (getIncludeCriticalIllnessRider() && getIncludeRefundRider()) {
                    //JIRA HSBCIDCU-630 Correct policy name to OnlineProtector
                    title = "HSBC Insurance OnlineProtector (\"the Base Product\"), the HSBC Insurance Online Premium Refund Rider (\"the PR Rider\") and the HSBC Insurance Online Critical Illness Rider (\"the CI Rider\")";
                } else {
                    //JIRA HSBCIDCU-630 Correct policy name to OnlineProtector
                    if (getIncludeCriticalIllnessRider()) {
                        title = "HSBC Insurance OnlineProtector (\"the Base Product\") and the HSBC Insurance Online Critical Illness Rider (\"the CI Rider\")";
                    } else {
                    	 title = "HSBC Insurance OnlineProtector (\"the Base Product\") and the HSBC Insurance Online Premium Refund Rider (\"the PR Rider\")";
                    }
                }
            }
            t.addListToTable(data, PlanDataTable.HASHEADER, title, HorizontalAlignment.LEFT);
            dataTable.draw();
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
    }

    private void generateBIPDFPage2(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);

        try {
            pd.setFont(PDFGenerator.FONT_BOLD, 10);
            BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
            Row r1 = table.createRow(9);
            Cell c1 = r1.createCell(100, "Benefit Illustration", HorizontalAlignment.CENTER, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(10);
            c1.setBottomPadding(0);
            c1.setTopPadding(0);
            Row r2 = table.createRow(9);
            Cell c2 = r2.createCell(100, "HSBC Insurance OnlineProtector (OL1)", HorizontalAlignment.CENTER, VerticalAlignment.TOP);
            c2.setFont(pd.getFont());
            c2.setFontSize(10);
            c2.setBottomPadding(0);
            c2.setTopPadding(0);
            yStart = table.draw() - 20;

            String[][] lines = new String[][] {
                    new String[] {
                            "Introduction",
                            "HSBC Insurance (Singapore) Pte. Limited believes that is important that you fully appreciate the benefits of your policy. You should also understand how the cost of your insurance cover and the expenses of administration and sales affect the benefits that you will receive.",
                            "The illustration that follows shows how the value of your policy progresses over time and the sum(s) that would be payable. The methods used to derive the values shown follow guidelines established by the Life Insurance Association, Singapore, to ensure that a fair and consistent approach is used in preparing this illustration.",
                            "Buying a life insurance policy can be a long-term commitment. An early termination of the policy usually involves high costs and the surrender value payable may be less than the total premiums paid.",
                            "If you need clarifications, please do not hesitate to contact HSBC Insurance Customer Service at (65) 6225 6111 or email us at e-surance@hsbc.com.sg."
                    },
                    new String[] {
                            "Important notes",
                            "This illustration assumes you are accepted on terms based on your underwriting class. This is not a contract of assurance. The precise benefits, terms and conditions will be provided in the Policy Contract.",
                            "The figures shown in the illustration and the actual value (premium and benefit amount) shown in the Policy Contract / Schedule may differ due to rounding difference. In the event of inconsistency, the amount shown in the Policy Contract / Schedule shall prevail.",
                            "We will deduct outstanding premium and any amount owing from the benefits payable.",
                            "There is no distribution cost for this product as the policy is sold without financial advice. Distribution costs include commissions and other benefits paid to the sales representative.",
                            "This Benefit Illustration is computer-generated and does not require a physical signature. This Benefit Illustration is generated based on the information you have provided."
                    }
            };

            for (int i = 0; i < lines.length; i++) {
                table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_TOP, pd.getDocument(), page, false, true);
                for (int j = 0; j < lines[i].length; j++) {
                    Row r = table.createRow(9);
                    Cell c = r.createCell(100, lines[i][j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                    c.setTopPadding(0);
                    c.setBottomPadding(10);
                    c.setLeftPadding(0);
                    c.setRightPadding(0);
                    if (j == 0) {
                        pd.setFont(PDFGenerator.FONT_BOLD, 9);
                    } else {
                        pd.setFont(PDFGenerator.FONT_NORMAL, 9);
                    }
                    if(i==1&&(j==4||j==5)){
                        pd.setFont(PDFGenerator.FONT_BOLD, 9);
                    }
                    c.setFont(pd.getFont());
                    c.setFontSize(9);
                }
                yStart = table.draw() - 20;
            }

        } catch (Exception e) {
            logger.error("Error Message "+e);
        }
    }

    private void generateBIPDFPage3(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);

        try {
            pd.setFont(PDFGenerator.FONT_BOLD, 10);

            BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
            Row r = table.createRow(9);
            Cell c = r.createCell(100, "Policyowner and Plan Details", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c.setFont(pd.getFont());
            c.setFontSize(9);
            c.setBottomPadding(0);
            c.setTopPadding(0);
            c.setLeftPadding(0);
            yStart = table.draw() - 20;

            table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);

            List<String[]> rows = new ArrayList<>();
            rows.add(new String[]{"Applicant Name", getApplicantFullName()});
            rows.add(new String[]{"", ""});
            rows.add(new String[]{"Proposed Life Insured", (insuredIsSelf() ? getApplicantFullName() : getInsuredFullName())});
            rows.add(new String[]{"Age Next Birthday", (insuredIsSelf() ? Integer.toString(AgeUtils.calculateAgeNextBirthday(getApplicantDob())) : Integer.toString(AgeUtils.calculateAgeNextBirthday(getInsuredDob())))});
            rows.add(new String[]{"Sex", (insuredIsSelf() ? getApplicantGenderLabel() : getInsuredGenderLabel())});
            rows.add(new String[]{"Smoker / Non Smoker", (insuredIsSelf() ? ((boolean) getApplicantSmoker() ? "Smoker" : "Non-Smoker") : ((boolean) getInsuredSmoker() ? "Smoker" : "Non-Smoker"))});
            rows.add(new String[]{"Underwriting Class", getUnderwritingClassLabel()});
            rows.add(new String[]{"", ""});
            rows.add(new String[]{String.format("%s Premium", getPaymentModeLabel()), String.format("S$%s", df2.format((double)getPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge()).get("modalPremium")))});
            rows.add(new String[]{"", ""});
            rows.add(new String[]{"Plan", "HSBC Insurance OnlineProtector"});
            rows.add(new String[]{"Policy Term", "10 Years"});
            rows.add(new String[]{"Premium Payment Term", "10 Years"});
            rows.add(new String[]{"Basic Sum Insured", String.format("S$%s", df2.format(getSumAssured()))});

            for (int i = 0; i < rows.size(); i++) {
                if (i == 15) {
                    pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY3);
                } else {
                    if (i == 16 || i == 17) {
                        pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY4);
                    } else {
                        pd.addTableRow(table, rows.get(i), PDFGenerator.TABLE_ROW_TYPE_SUMMARY2);
                    }
                }

            }

            yStart = table.draw() - 20;
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

        List<List> data = new ArrayList();
        Map<String, String> premiums = new HashMap<>();
        premiums.put("annually", getPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), PAYMENT_MODE_ANNUALLY).get("modalPremium").toString());
        premiums.put("semi_annually", getPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), PAYMENT_MODE_SEMI_ANNUALLY).get("modalPremium").toString());
        premiums.put("quarterly", getPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), PAYMENT_MODE_QUARTERLY).get("modalPremium").toString());
        premiums.put("monthly", getPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), PAYMENT_MODE_MONTHLY).get("modalPremium").toString());

        try {
            data.add(new ArrayList<>(Arrays.asList("", "Annually (S$)", "Semi Annually (S$)", "Quarterly (S$)", "Monthly (S$)")));

            data.add(new ArrayList<>(Arrays.asList("Basic Premium", df2.format(Float.valueOf(premiums.get("annually"))), df2.format(Float.valueOf(premiums.get("semi_annually"))), df2.format(Float.valueOf(premiums.get("quarterly"))), df2.format(Float.valueOf(premiums.get("monthly"))))));


            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page,HorizontalAlignment.CENTER);
            t.addListToTable(data, PlanDataTable.HASHEADER, null);
            yStart = dataTable.draw() - 30;
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

        try {
            pd.setFont(PDFGenerator.FONT_BOLD, 10);
            pd.writeText(page, "Main Benefit Illustration for Basic Plan", yStart);
            yStart -= 10;
        } catch (IOException ioe) {
            logger.error("Error Message "+ ioe);
        }
        ArrayList<Map<String, Object>> bi = getBenefitIllustration();

        try {
            data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("End of Policy Year / Age", "Total Premiums Paid To-Date (S$)", "Death Benefit Guaranteed (S$)", "Surrender Value Guaranteed (S$)")));
            for (int i = 0; i < bi.size(); i++) {
                Map<String, Object> result = bi.get(i);

                data.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", result.get("year"), result.get("age")), df.format((float) result.get("premiumPaid")), df.format(getSumAssured()), "0")));
            }

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM + 40, (tableWidth * 1f), PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page, HorizontalAlignment.CENTER);
            t.addListToTable(data, PlanDataTable.HASHEADER, null);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

        if (yStart < pageHalfPosition) {
            page = pd.createPage();
            pd.addPage(page);
            yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
            yStart = yStartNewPage;
            tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
        } else {
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
        }

        String[][] lines = new String[][] {
                new String[] {
                        "Notes:",
                        "Death Benefit is payable upon Death, Terminal Illness and Total and Permanent Disability (TPD).",
                        "The policy will be automatically renewed for an additional ten (10) years after the end of the Policy Term. ",
                        "Please note that at each renewal, premium will be based on the prevailing premium rates at the attained age of the Life Insured and will stay level throughout the renewed term.",
                        "The Basic Premium after the renewal will depend on the rates and age at the date of the renewal.",
                        "Refer to the Product Summary and General Provisions for the renewability terms and conditions."
                }
        };

        try {
            for (int i = 0; i < lines.length; i++) {
                BaseTable table = new BaseTable(yStart, yStart, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
                for (int j = 0; j < lines[i].length; j++) {
                    Row r = table.createRow(9);
                    if (j == 0) {
                        pd.setFont(PDFGenerator.FONT_BOLD, 9);
                        Cell c = r.createCell(100, lines[i][j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c.setFont(pd.getFont());
                        c.setFontSize(9);
                        c.setTopPadding(0);
                        c.setBottomPadding(2);
                        c.setLeftPadding(0);
                        c.setRightPadding(0);
                    } else {
                        pd.setFont(PDFGenerator.FONT_NORMAL, 9);
                        Cell c1 = r.createCell(3, "*", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c1.setFont(pd.getFont());
                        c1.setFontSize(9);
                        c1.setTopPadding(0);
                        c1.setBottomPadding(2);
                        c1.setLeftPadding(0);
                        c1.setRightPadding(0);
                        Cell c2 = r.createCell(97, lines[i][j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c2.setFont(pd.getFont());
                        c2.setFontSize(9);
                        c2.setTopPadding(0);
                        c2.setBottomPadding(2);
                        c2.setLeftPadding(0);
                        c2.setRightPadding(0);
                    }
                }
                yStart = table.draw() - 20;
            }
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }
    }

    private void generateBIPDFPage4(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);

        try {
            pd.setFont(PDFGenerator.FONT_NORMAL, 10);
            BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
            Row r = table.createRow(9);
            String text = "<p><b>Supplementary Benefit Illustration</b></p>" +
                    "<p>This is only a supplementary illustration and must be read in conjunction with the main illustration.</p>" +
                    "<p>Riders are optional insurance benefits payable in addition to the basic insurance benefit. The benefit terminates once a claim on the Basic Plan is fully paid.You may continue the policy and its other benefits after a claim for a chosen benefit has been admitted, unless we state otherwise.</p>";
            Cell c1 = r.createCell(100, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(9);
            c1.setBottomPadding(0);
            c1.setTopPadding(0);
            c1.setLeftPadding(0);
            yStart = table.draw() - 10;
        } catch (Exception e) {

        }

        try {
            pd.setFont(PDFGenerator.FONT_NORMAL, 10);
            BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
            Row r = table.createRow(9);

            String text = "<b>Riders Selected</b><br>HSBC Insurance Online Critical Illness Rider";
            Cell c1 = r.createCell(40, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(9);
            c1.setBottomPadding(0);
            c1.setTopPadding(0);
            c1.setLeftPadding(0);

            text = String.format("<b>Sum Insured (S$)</b><br>%s", df2.format(getRiderSumAssured()));
            Cell c2 = r.createCell(20, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c2.setFont(pd.getFont());
            c2.setFontSize(9);
            c2.setBottomPadding(0);
            c2.setTopPadding(0);
            c2.setLeftPadding(0);

            text = String.format("<b>Expiry Age</b><br>%s", AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob()) + getTermYears());
            Cell c3 = r.createCell(15, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c3.setFont(pd.getFont());
            c3.setFontSize(9);
            c3.setBottomPadding(0);
            c3.setTopPadding(0);
            c3.setLeftPadding(0);

//            text = String.format("%s Premium (S$)<br>%s", getPaymentModeLabel(), Integer.toString(Math.round(((double) getCriticalIllnessRiderPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), getPaymentMode()).get("modalPremium")))));
            text = String.format("<b>%s Premium (S$)</b><br>%s", getPaymentModeLabel(), df2.format(getCriticalIllnessRiderPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), getPaymentMode()).get("modalPremium")));
            Cell c4 = r.createCell(25, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c4.setFont(pd.getFont());
            c4.setFontSize(9);
            c4.setBottomPadding(0);
            c4.setTopPadding(0);
            c4.setLeftPadding(0);

            yStart = table.draw() - 15;
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }

        List<List> data = new ArrayList();

        Map<String, String> premiums = new HashMap<>();

        if (getIncludeRefundRider()) {
            calculateRefundRiderPremium();
        }
        DecimalFormat df = new DecimalFormat("####.00");
        double annualCritical=(double)(getCriticalIllnessRiderPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge(), PAYMENT_MODE_ANNUALLY).get("modalPremium"));
        annualCritical=Double.parseDouble(df.format(annualCritical));
        
        double semiannualCritical=(double)(getCriticalIllnessRiderPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge(), PAYMENT_MODE_SEMI_ANNUALLY).get("modalPremium"));
        semiannualCritical=Double.parseDouble(df.format(semiannualCritical));
        
        double quarterCritical=(double)(getCriticalIllnessRiderPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge(), PAYMENT_MODE_QUARTERLY).get("modalPremium"));
        quarterCritical=Double.parseDouble(df.format(quarterCritical));
        
        double monthlyCritical=(double)(getCriticalIllnessRiderPremiumAtAge(insuredIsSelf() ? getApplicantAge() : getInsuredAge(), PAYMENT_MODE_MONTHLY).get("modalPremium"));
        monthlyCritical=Double.parseDouble(df.format(monthlyCritical));
        
        premiums.put("annually", String.valueOf(annualCritical+basePremiumAnnual+refundPremiumAnnual));
        premiums.put("semi_annually", String.valueOf(semiannualCritical+basePremiumSemiAnnual+refundPremiumSemiAnnual));
        premiums.put("quarterly", String.valueOf(quarterCritical+basePremiumQuarter+refundPremiumQuarter));
        premiums.put("monthly", String.valueOf(monthlyCritical+basePremiumMonth+refundPremiumMonth));

        try {
            data.add(new ArrayList<>(Arrays.asList("", "Annually (S$)", "Semi Annually (S$)", "Quarterly (S$)", "Monthly (S$)")));
            logger.info(premiums);
            data.add(new ArrayList<>(Arrays.asList("Total Premium (including riders)", df2.format(Double.valueOf(premiums.get("annually"))), df2.format(Double.valueOf(premiums.get("semi_annually"))), df2.format(Double.valueOf(premiums.get("quarterly"))), df2.format(Double.valueOf(premiums.get("monthly"))))));

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page,HorizontalAlignment.CENTER);
            t.addListToTable(data, PlanDataTable.HASHEADER, null);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

        ArrayList<Map<String, Object>> bi = getCriticalIllnessRiderBenefitIllustration();

        try {
            data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("End of Policy Year / Age", "Total Rider Premiums Paid To-Date (S$)", "Critical Illness Benefit Guaranteed (S$)")));
            for (int i = 0; i < bi.size(); i++) {
                Map<String, Object> result = bi.get(i);
                data.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", result.get("year"), result.get("age")), df.format((float) result.get("premiumPaid")), df.format(getRiderSumAssured()))));
            }

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM + 40, (tableWidth * 1f), PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page,HorizontalAlignment.CENTER);
            t.addListToTable(data, PlanDataTable.HASHEADER, null);
            yStart = dataTable.draw() - 10;
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);

        // need to put another page base on ticket 682
        if (yStart < pageHalfPosition) {
            page = pd.createPage();
            pd.addPage(page);
            yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
            yStart = yStartNewPage;
            tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
        } else {
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
        }

        String[][] lines = new String[][] {
                new String[] {
                        "Notes:",
                        "The Death Benefit is reduced by the claim amount paid from Critical Illness Benefit.",
                        "This CI Rider will be automatically renewed for an additional ten (10) years after the end of the Policy Term subject to the terms and conditions available in the Product Summary and Annexure.",
                        "The CI Rider Premium during the policy term is not guaranteed and premium after the renewal will depend on the rates and age at the point of renewal.",
                        "Refer to the Product Summary and Annexures for the renewability terms and conditions."
                }
        };

        try {
            for (int i = 0; i < lines.length; i++) {
                BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
                for (int j = 0; j < lines[i].length; j++) {
                    Row r = table.createRow(9);
                    if (j == 0) {
                        pd.setFont(PDFGenerator.FONT_BOLD, 9);
                        Cell c = r.createCell(100, lines[i][j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c.setFont(pd.getFont());
                        c.setFontSize(9);
                        c.setTopPadding(0);
                        c.setBottomPadding(2);
                        c.setLeftPadding(0);
                        c.setRightPadding(0);
                    } else {
                        pd.setFont(PDFGenerator.FONT_NORMAL, 9);
                        Cell c1 = r.createCell(3, "*", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c1.setFont(pd.getFont());
                        c1.setFontSize(9);
                        c1.setTopPadding(0);
                        c1.setBottomPadding(2);
                        c1.setLeftPadding(0);
                        c1.setRightPadding(0);
                        Cell c2 = r.createCell(97, lines[i][j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c2.setFont(pd.getFont());
                        c2.setFontSize(9);
                        c2.setTopPadding(0);
                        c2.setBottomPadding(2);
                        c2.setLeftPadding(0);
                        c2.setRightPadding(0);
                    }
                }
                yStart = table.draw() - 20;
            }
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
    }

    private void generateBIPDFPage5(PDFGenerator pd) {
        PDPage page = pd.createPage();
        pd.addPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float yStart = yStartNewPage;
        float tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);

        try {
            pd.setFont(PDFGenerator.FONT_NORMAL, 10);
            BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
            Row r = table.createRow(9);
            String text = "<p><b>Supplementary Benefit Illustration</b></p>" +
                    "<p>This is only a supplementary illustration and must be read in conjunction with the main illustration.</p>" +
                    "<p>Riders are optional insurance benefits payable in addition to the basic insurance benefit. The benefit terminates once a claim on the Basic Plan is fully paid.</p>";
            Cell c1 = r.createCell(100, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(9);
            c1.setBottomPadding(0);
            c1.setTopPadding(0);
            c1.setLeftPadding(0);
            yStart = table.draw() - 10;
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }

        try {
            pd.setFont(PDFGenerator.FONT_NORMAL, 10);
            BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
            Row r = table.createRow(9);

            String text = "<b>Riders Selected</b><br>HSBC Insurance Premium Refund Rider";
            Cell c1 = r.createCell(40, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c1.setFont(pd.getFont());
            c1.setFontSize(9);
            c1.setBottomPadding(0);
            c1.setTopPadding(0);
            c1.setLeftPadding(0);

            text = String.format("<b>Sum Insured (S$)</b><br>%s", "-");
            Cell c2 = r.createCell(20, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c2.setFont(pd.getFont());
            c2.setFontSize(9);
            c2.setBottomPadding(0);
            c2.setTopPadding(0);
            c2.setLeftPadding(0);

            text = String.format("<b>Expiry Age</b><br>%s", AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob()) + getTermYears());
            Cell c3 = r.createCell(15, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c3.setFont(pd.getFont());
            c3.setFontSize(9);
            c3.setBottomPadding(0);
            c3.setTopPadding(0);
            c3.setLeftPadding(0);

//            text = String.format("%s Premium (S$)<br>%s", getPaymentModeLabel(), Integer.toString(Math.round(((double) getRefundRiderPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), getPaymentMode()).get("modalPremium")))));
            text = String.format("<b>%s Premium (S$)</b><br>%s", getPaymentModeLabel(), df2.format(getRefundRiderPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), getPaymentMode()).get("modalPremium")));
            Cell c4 = r.createCell(25, text, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
            c4.setFont(pd.getFont());
            c4.setFontSize(9);
            c4.setBottomPadding(0);
            c4.setTopPadding(0);
            c4.setLeftPadding(0);

            yStart = table.draw() - 15;
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }

        List<List> data = new ArrayList();
        Map<String, String> premiums = new HashMap<>();

        calculateRefundRiderPremium();

        premiums.put("annually", String.valueOf(refundPremiumAnnual+basePremiumAnnual+ciPremiumAnnual));
        premiums.put("semi_annually", String.valueOf(refundPremiumSemiAnnual+basePremiumSemiAnnual+ciPremiumSemiAnnual));
        premiums.put("quarterly", String.valueOf(refundPremiumQuarter+basePremiumQuarter+ciPremiumQuarter));
        premiums.put("monthly", String.valueOf(refundPremiumMonth+basePremiumMonth+ciPremiumMonth));

        try {
            data.add(new ArrayList<>(Arrays.asList("", "Annually (S$)", "Semi Annually (S$)", "Quarterly (S$)", "Monthly (S$)")));
            data.add(new ArrayList<>(Arrays.asList("Total Premium (including riders)", df2.format(Double.valueOf(premiums.get("annually"))), df2.format(Double.valueOf(premiums.get("semi_annually"))), df2.format(Double.valueOf(premiums.get("quarterly"))), df2.format(Double.valueOf(premiums.get("monthly"))))));

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page,HorizontalAlignment.CENTER);
            t.addListToTable(data, PlanDataTable.HASHEADER, null);
            yStart = dataTable.draw() - 20;
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }

        ArrayList<Map<String, Object>> bi = getRefundRiderBenefitIllustration();
        try {
            data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("End of Policy Year / Age", "Total Rider Premiums Paid To-Date (S$)", "Death Benefit Guaranteed (S$)", "Surrender Values Guaranteed (S$)")));
            for (int i = 0; i < bi.size(); i++) {
                Map<String, Object> result = bi.get(i);
                data.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", result.get("year"), result.get("age")), df.format((float) result.get("premiumPaid")), df.format(0f), df.format(0f))));
            }

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM + 40, (tableWidth * 1f), PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page,HorizontalAlignment.CENTER);
            t.addListToTable(data, PlanDataTable.HASHEADER, null);
            yStart = dataTable.draw() - 10;
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }

        try {
            data = new ArrayList();
            data.add(new ArrayList<>(Arrays.asList("End of Policy Year / Age", "Total Premiums Paid To-Date (S$)", "Maturity Value (S$)")));
            Map<String, Object> result = bi.get(bi.size() - 1);
            data.add(new ArrayList<>(Arrays.asList(String.format("%s/%s", result.get("year"), result.get("age")), df.format((float) result.get("premiumPaid")), df.format((float)result.get("premiumPaid") * 1.1))));

            BaseTable dataTable = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM + 40, (tableWidth * 1f), PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            PlanDataTable t = new PlanDataTable(dataTable, page, HorizontalAlignment.CENTER);
            t.addListToTable(data, PlanDataTable.HASHEADER, null);
            yStart = dataTable.draw() - 10;
        } catch (Exception e) {

            logger.error("Error Message "+e);

        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);

        // need to put another page base on ticket 682
        if (yStart < pageHalfPosition) {
            page = pd.createPage();
            pd.addPage(page);
            yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
            yStart = yStartNewPage;
            tableWidth = page.getMediaBox().getWidth() - (PDFGenerator.PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
        } else {
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);
        }

        String[][] lines = new String[][] {
                new String[] {
                        "Notes:",
                        "The Premium Refund Rider (\"the PR Rider\") premium is guaranteed during the Policy Term. ",
                        "This PR Rider may be renewed for an additional period of 10 years subject to availability of this Rider upon renewal and other terms and conditions available in the Product Summary and Annexure. The PR Rider Premium after the renewal will depend on the rates and age at the point of renewal.",
                        "You will only get back the above Maturity Value refund of premium benefit if you pay all the 10 scheduled Annual Premiums for the Base Product and PR Rider.",
                        "You will only get a reduced Maturity Value refund of premium benefit if you stop payment after you paid the first 5 years of premiums in full for the Base Product and PR Rider.",
                        "You will not get back any refund of premium benefit if you stop payment before the first 5 years of premiums are paid in full for the base product and PR Rider and your policy will lapse.",
                }
        };

        try {
            for (int i = 0; i < lines.length; i++) {
                BaseTable table = new BaseTable(yStart, yStartNewPage, PDFGenerator.PAGE_MARGIN_BOTTOM, tableWidth, PDFGenerator.PAGE_MARGIN_LEFT, pd.getDocument(), page, false, true);
                for (int j = 0; j < lines[i].length; j++) {
                    Row r = table.createRow(9);
                    if (j == 0) {
                        pd.setFont(PDFGenerator.FONT_BOLD, 9);
                        Cell c = r.createCell(100, lines[i][j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c.setFont(pd.getFont());
                        c.setFontSize(9);
                        c.setTopPadding(0);
                        c.setBottomPadding(2);
                        c.setLeftPadding(0);
                        c.setRightPadding(0);
                    } else {
                        pd.setFont(PDFGenerator.FONT_NORMAL, 9);
                        Cell c1 = r.createCell(3, "*", HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c1.setFont(pd.getFont());
                        c1.setFontSize(9);
                        c1.setTopPadding(0);
                        c1.setBottomPadding(2);
                        c1.setLeftPadding(0);
                        c1.setRightPadding(0);
                        Cell c2 = r.createCell(97, lines[i][j], HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                        c2.setFont(pd.getFont());
                        c2.setFontSize(9);
                        c2.setTopPadding(0);
                        c2.setBottomPadding(2);
                        c2.setLeftPadding(0);
                        c2.setRightPadding(0);
                    }
                }
                yStart = table.draw() - 20;
            }
        } catch (Exception e) {
            logger.error("Error Message "+e);
        }

        page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
    }

    /**
     * Calculation Refund Rider Premium
     * @return
     */
    private Map<String, Float> calculateRefundRiderPremium () {
        Map<String, Float> refundPremiums = new HashMap<>();
        refundPremiums.put("annually", (Float)(getRefundRiderPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), PAYMENT_MODE_ANNUALLY).get("modalPremium")));
        refundPremiums.put("semi_annually", (Float)(getRefundRiderPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), PAYMENT_MODE_SEMI_ANNUALLY).get("modalPremium")));
        refundPremiums.put("quarterly", (Float)(getRefundRiderPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), PAYMENT_MODE_QUARTERLY).get("modalPremium")));
        refundPremiums.put("monthly", (Float)(getRefundRiderPremiumAtAge((insuredIsSelf() ? getApplicantAge() : getInsuredAge()), PAYMENT_MODE_MONTHLY).get("modalPremium")));
        return refundPremiums;
    }



    public Map<String, Object> getPremiumAtAge(int age) {
        return getPremiumAtAge(age, getPaymentMode());
    }

    public Map<String, Object> getPremiumAtAge(int age, int paymentMode) {
        Map<String, Object> result = new HashMap<>();

        int term = getTermYears();
        int initialAge = AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob());
        int cycle = (age - 1 - initialAge) / term;
        int cycleAge = initialAge + (cycle * term);
        String gsMarker = getGsMarker();

        Float sumAssured = getSumAssured();
        double premium = 0.0;
        float premiumRate = 0.0f;
        float premiumRateUnitAmount = 0.0f;
        float discountRate = 0.0f;
        float discountRateUnitAmount = 0.0f;
        float adjustedPremium = 0.0f;
        double modalPremium = 0.0;
        double modalFactor = getPaymentModePremiumFactor(paymentMode);
        float underwritingClassFactor = getUnderwritingClassFactor();
        float paymentFrequencyFactor = getPremiumPaymentFrequencyFactor(paymentMode);
        float adjustedPremiumWithDiscount=0.0f;
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            stmt = conn.prepareStatement("SELECT * FROM premiums WHERE product_code = ? AND anb = ?");
            stmt.setString(1, PRODUCT_CODE);
            stmt.setInt(2, cycleAge);
            rs = stmt.executeQuery();
            if (rs.next()) {
                premiumRate = rs.getFloat("premium_" + gsMarker.toLowerCase());
                premiumRateUnitAmount = rs.getFloat("unit_amount");
            }
        } catch (Exception e) {
            logger.error("Error Message "+e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }

        try{
            conn = db.getConnection();
            stmt = conn.prepareStatement("SELECT * FROM discounts WHERE product_code = ? AND gs_marker = ? AND sa_min <= ? AND sa_max >= ?");
            stmt.setString(1, PRODUCT_CODE);
            stmt.setString(2, gsMarker.toUpperCase());
            stmt.setFloat(3, sumAssured);
            stmt.setFloat(4, sumAssured);

            rs = stmt.executeQuery();
            if (rs.next()) {
                discountRate = rs.getFloat("discount");
                discountRateUnitAmount = rs.getFloat("unit_amount");
            }
        } catch (Exception e) {
        	logger.error("Error Message "+e);
        }finally {
            db.closeAll(conn,stmt, rs);
        }

        if (premiumRate > 0) {
        	adjustedPremiumWithDiscount=((premiumRate / premiumRateUnitAmount) - (discountRate / discountRateUnitAmount));
            adjustedPremium = ((premiumRate / premiumRateUnitAmount) - (discountRate / discountRateUnitAmount)) * sumAssured * underwritingClassFactor;
            modalPremium = Math.round(adjustedPremium * modalFactor * 100f) / 100f;
            premium = modalPremium * paymentFrequencyFactor;
        }
        logger.info("the payment mode while calculation is "+paymentMode);

        

        result.put("age", age);
        result.put("premium", premium);
        result.put("premiumRate", premiumRate);
        result.put("premiumRateUnitAmount", premiumRateUnitAmount);
        result.put("discountRate", discountRate);
        result.put("discountRateUnitAmount", discountRateUnitAmount);
        result.put("adjustedPremium", adjustedPremium);
        result.put("modalPremium", modalPremium);
        result.put("adjustedPremiumWithDiscount", adjustedPremiumWithDiscount);
        result.put("underwritingClassFactor", underwritingClassFactor);
        result.put("modalFactor", modalFactor);
        logger.info("the modal premium i s "+modalPremium);
        DecimalFormat df = new DecimalFormat("####.00");
        String twoDecimal=df.format(modalPremium);
        modalPremium=Float.parseFloat(twoDecimal);
        switch (paymentMode) {
        case PAYMENT_MODE_MONTHLY:
            if (basePremiumMonth < 0)
                basePremiumMonth = modalPremium;
            break;
        case PAYMENT_MODE_QUARTERLY:
            if (basePremiumQuarter < 0)
                basePremiumQuarter = modalPremium;
            break;
        case PAYMENT_MODE_SEMI_ANNUALLY:
            if (basePremiumSemiAnnual < 0)
                basePremiumSemiAnnual = modalPremium;
            break;
        case PAYMENT_MODE_ANNUALLY:
        default:
            if (basePremiumAnnual < 0)
                basePremiumAnnual = modalPremium;
            break;
    }
        
        return result;
    }

    public Map<String, Object> getCriticalIllnessRiderPremiumAtAge(int age) {
        return getCriticalIllnessRiderPremiumAtAge(age, getPaymentMode());
    }

    public Map<String, Object> getCriticalIllnessRiderPremiumAtAge(int age, int paymentMode) {
        Map<String, Object> result = new HashMap<>();

        int term = getTermYears();
        int initialAge = AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob());
        int cycle = (age - 1 - initialAge) / term;
        int cycleAge = initialAge + (cycle * term);
        String gsMarker = getGsMarker();

        Float sumAssured = getRiderSumAssured();
        if(sumAssured==null){
        	sumAssured=100000.0f;
        }
        double premium = 0;
        float premiumRate = 0;
        float premiumRateUnitAmount = 0;
        float adjustedPremium = 0;
        double modalPremium = 0;
        double modalFactor = getPaymentModePremiumFactor(paymentMode);
        float paymentFrequencyFactor = getPremiumPaymentFrequencyFactor(paymentMode);
        float adjustedPremiumWithDiscount=0.0f;
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            stmt = conn.prepareStatement("SELECT * FROM premiums WHERE product_code = ? AND anb = ?");
            stmt.setString(1, RIDER_PRODUCT_CODE);
            stmt.setInt(2, cycleAge);

            rs = stmt.executeQuery();
            if (rs.next()) {
                premiumRate = rs.getFloat("premium_" + gsMarker.toLowerCase());
                premiumRateUnitAmount = rs.getFloat("unit_amount");
            }
        }  catch (Exception e) {
        	logger.error("Error Message "+e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }

        if (premiumRate > 0) {
        	adjustedPremiumWithDiscount=premiumRate / premiumRateUnitAmount;
            adjustedPremium = (premiumRate / premiumRateUnitAmount) * sumAssured;
            modalPremium = adjustedPremium * modalFactor;
            premium = modalPremium * paymentFrequencyFactor;
        }
       
        result.put("age", age);
        result.put("premium", premium);
        result.put("premiumRate", premiumRate);
        result.put("premiumRateUnitAmount", premiumRateUnitAmount);
        result.put("discountRate", 1);
        result.put("discountRateUnitAmount", 1);
        result.put("adjustedPremiumWithDiscount", adjustedPremiumWithDiscount);
        result.put("adjustedPremium", adjustedPremium);
        result.put("modalPremium", modalPremium);
        result.put("modalFactor", modalFactor);
logger.info("the adjusted premium for ci "+result);
        
        DecimalFormat df = new DecimalFormat("####.00");
        String twoDecimal=df.format(modalPremium);
        modalPremium=Float.parseFloat(twoDecimal);
        
        switch (paymentMode) {
        case PAYMENT_MODE_MONTHLY:
            if (ciPremiumMonth < 0)
                ciPremiumMonth = modalPremium;
            break;
        case PAYMENT_MODE_QUARTERLY:
            if (ciPremiumQuarter < 0)
                ciPremiumQuarter = modalPremium;
            break;
        case PAYMENT_MODE_SEMI_ANNUALLY:
            if (ciPremiumSemiAnnual < 0)
                ciPremiumSemiAnnual = modalPremium;
            break;
        case PAYMENT_MODE_ANNUALLY:
        default:
            if (ciPremiumAnnual < 0)
                ciPremiumAnnual = modalPremium;
            break;
    }

        
        return result;
    }

    public Map<String, Object> getRefundRiderPremiumAtAge(int age) {
        return getRefundRiderPremiumAtAge(age, getPaymentMode());
    }

    public Map<String, Object> getRefundRiderPremiumAtAge(int age, int paymentMode) {
        Map<String, Object> result = new HashMap<>();

        int term = getTermYears();
        int initialAge = AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob());
        int cycle = (age - 1 - initialAge) / term;
        int cycleAge = initialAge + (cycle * term);
        String gsMarker = getGsMarker();

        Float sumAssured = getSumAssured();
        if(sumAssured==null){
        	sumAssured=100000f;
        }
        float premium = 0;
        float premiumRate = 0;
        float premiumRateUnitAmount = 0;
        float discountRate = 0;
        float discountRateUnitAmount = 0;
        float adjustedPremium = 0;
        float adjustedPremiumWithDiscount=0.0f;
        float modalPremium = 0;
        double modalFactor = getPaymentModePremiumFactor(paymentMode);
        float underwritingClassFactor = getUnderwritingClassFactor();
        float paymentFrequencyFactor = getPremiumPaymentFrequencyFactor(paymentMode);

        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM premiums WHERE product_code = ? AND anb = ?");
            stmt.setString(1, PRODUCT_CODE);
            stmt.setInt(2, cycleAge);
            rs = stmt.executeQuery();
            if (rs.next()) {
                premiumRate = rs.getFloat("premium_" + gsMarker.toLowerCase());
                premiumRateUnitAmount = rs.getFloat("unit_amount");
            }
        } catch (Exception e) {
        	logger.error("Error Message "+e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }

        try {
            conn = db.getConnection();
            stmt = conn.prepareStatement("SELECT * FROM discounts WHERE product_code = ? AND gs_marker = ? AND sa_min <= ? AND sa_max >= ?");
            stmt.setString(1, PRODUCT_CODE);
            stmt.setString(2, gsMarker.toUpperCase());
            stmt.setFloat(3, sumAssured);
            stmt.setFloat(4, sumAssured);

            rs = stmt.executeQuery();
            if (rs.next()) {
                discountRate = rs.getFloat("discount");
                discountRateUnitAmount = rs.getFloat("unit_amount");
            }
        } catch (Exception e) {
        	logger.error("Error Message "+e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }


        if (premiumRate > 0) {
        	adjustedPremiumWithDiscount=((premiumRate / premiumRateUnitAmount) - (discountRate / discountRateUnitAmount));
            adjustedPremium = ((premiumRate / premiumRateUnitAmount) - (discountRate / discountRateUnitAmount)) * sumAssured * underwritingClassFactor;
            modalPremium = Math.round(adjustedPremium * modalFactor * 100f) / 100f;
            premium = modalPremium * paymentFrequencyFactor;
        }

        
        result.put("age", age);
        result.put("premium", premium * 10);
        result.put("premiumRate", premiumRate);
        result.put("premiumRateUnitAmount", premiumRateUnitAmount);
        result.put("discountRate", discountRate);
        result.put("discountRateUnitAmount", discountRateUnitAmount);
        result.put("adjustedPremium", adjustedPremium * 10);
        result.put("modalPremium", modalPremium * 10);
        result.put("adjustedPremiumWithDiscount", adjustedPremiumWithDiscount);
        result.put("modalFactor", modalFactor);
        result.put("underwritingClassFactor", underwritingClassFactor);


        DecimalFormat df = new DecimalFormat("####.00");
        String twoDecimal=df.format(modalPremium);
        modalPremium=Float.parseFloat(twoDecimal);
        
        switch (paymentMode) {
        case PAYMENT_MODE_MONTHLY:
            if (refundPremiumMonth < 0)
                refundPremiumMonth = modalPremium * 10;
            break;
        case PAYMENT_MODE_QUARTERLY:
            if (refundPremiumQuarter < 0)
                refundPremiumQuarter = modalPremium * 10;
            break;
        case PAYMENT_MODE_SEMI_ANNUALLY:
            if (refundPremiumSemiAnnual < 0)
                refundPremiumSemiAnnual = modalPremium * 10;
            break;
        case PAYMENT_MODE_ANNUALLY:
        default:
            if (refundPremiumAnnual < 0)
                refundPremiumAnnual = modalPremium * 10;
            break;
    }
        
        return result;
    }

    protected static OnlineProtectorApplication populate(OnlineProtectorApplication application, ResultSet rs) {
        try {
            application.setId(rs.getString("id"));
            application.setReferenceNum(rs.getString("reference_num"));
            application.setAgreePrivacy(getBooleanFromResultSet(rs, "agree_privacy"));
            application.setAgreeImportantNotes(getBooleanFromResultSet(rs, "agree_important_notes"));
            application.setLifeInsured(rs.getString("life_insured"));
            application.setStatus(rs.getInt("status"));
            application.setDropoutPassword(rs.getString("dropout_password"));
            application.setNotifyStatus(rs.getInt("notify_status"));
            application.setMailOption(rs.getInt("mail_option"));
            application.setCampaignCode(rs.getString("campaign_code"));

            application.setApplicantResidency(rs.getString("applicant_residency"));
            application.setApplicantFirstName(rs.getString("applicant_first_name"));
            application.setApplicantLastName(rs.getString("applicant_last_name"));
            application.setApplicantDob(rs.getDate("applicant_dob"));
            application.setApplicantGender(rs.getString("applicant_gender"));
            application.setApplicantMobileCountryCode(rs.getString("applicant_mobile_country_code"));
            application.setApplicantMobileNum(rs.getString("applicant_mobile_num"));
            application.setApplicantEmail(rs.getString("applicant_email"));
            application.setApplicantNationality(rs.getString("applicant_nationality"));
            application.setApplicantMultipleNationalities(getBooleanFromResultSet(rs, "applicant_multiple_nationalities"));
            application.setApplicantNationality2(rs.getString("applicant_nationality2"));
            application.setApplicantNationality3(rs.getString("applicant_nationality3"));
            application.setApplicantBirthCountry(rs.getString("applicant_birth_country"));
            application.setApplicantNric(rs.getString("applicant_nric"));
            application.setApplicantEnProficient(getBooleanFromResultSet(rs, "applicant_en_proficient"));
            application.setApplicantEmployment(rs.getString("applicant_employment"));
            application.setApplicantIncome(getIntegerFromResultSet(rs, "applicant_income"));
            application.setApplicantSmoker(getBooleanFromResultSet(rs, "applicant_smoker"));
            application.setApplicantOccupation(rs.getString("applicant_occupation"));
            application.setApplicantIndustry(rs.getString("applicant_industry"));
            application.setApplicantCompanyName(rs.getString("applicant_company_name"));
            application.setApplicantCompanyCity(rs.getString("applicant_company_city"));
            application.setApplicantCompanyCountry(rs.getString("applicant_company_country"));
            application.setApplicantStudiesEndDate(rs.getDate("applicant_studies_end_date"));
            application.setApplicantAddressPostalCode(rs.getString("applicant_address_postal_code"));
            application.setApplicantAddressHouse(rs.getString("applicant_address_house"));
            application.setApplicantAddressUnit(rs.getString("applicant_address_unit"));
            application.setApplicantAddressStreet(rs.getString("applicant_address_street"));
            application.setApplicantAddressBuilding(rs.getString("applicant_address_building"));
            application.setApplicantAddressIsRegistered(getBooleanFromResultSet(rs, "applicant_address_registered"));
            application.setApplicantAddressPeriodOfStay(getIntegerFromResultSet(rs, "applicant_address_period_stay"));
            application.setApplicantPreviousAddressCountry(rs.getString("applicant_previous_address_country"));
            application.setApplicantAddressIsPermanent(getBooleanFromResultSet(rs, "applicant_address_permanent"));
            application.setApplicantPermanentAddressLine1(rs.getString("applicant_permanent_address_line1"));
            application.setApplicantPermanentAddressLine2(rs.getString("applicant_permanent_address_line2"));
            application.setApplicantPermanentAddressPostalCode(rs.getString("applicant_permanent_address_postal_code"));
            application.setApplicantPermanentAddressCountry(rs.getString("applicant_permanent_address_country"));

            application.setApplicantTaxCountry1(rs.getString("applicant_tax_country1"));
            application.setApplicantTaxCountry1Tin(rs.getString("applicant_tax_country1_tin"));
            application.setApplicantTaxCountry1NoTinReason(rs.getString("applicant_tax_country1_tin_reason"));
            application.setApplicantTaxCountry1NoTinReasonCustom(rs.getString("applicant_tax_country1_tin_reason_custom"));
            application.setApplicantTaxCountry2(rs.getString("applicant_tax_country2"));
            application.setApplicantTaxCountry2Tin(rs.getString("applicant_tax_country2_tin"));
            application.setApplicantTaxCountry2NoTinReason(rs.getString("applicant_tax_country2_tin_reason"));
            application.setApplicantTaxCountry2NoTinReasonCustom(rs.getString("applicant_tax_country2_tin_reason_custom"));
            application.setApplicantTaxCountry3(rs.getString("applicant_tax_country3"));
            application.setApplicantTaxCountry3Tin(rs.getString("applicant_tax_country3_tin"));
            application.setApplicantTaxCountry3NoTinReason(rs.getString("applicant_tax_country3_tin_reason"));
            application.setApplicantTaxCountry3NoTinReasonCustom(rs.getString("applicant_tax_country3_tin_reason_custom"));

            application.setApplicantAddressNoTaxReason(rs.getString("applicant_address_no_tax_reason"));
            application.setApplicantIsHsbcCustomer(getBooleanFromResultSet(rs, "applicant_hsbc_customer"));
            application.setApplicantIsPep(getBooleanFromResultSet(rs, "applicant_pep"));
            application.setApplicantIsBeneficalOwner(getBooleanFromResultSet(rs, "applicant_bo"));

            application.setInsuredResidency(rs.getString("insured_residency"));
            application.setInsuredFirstName(rs.getString("insured_first_name"));
            application.setInsuredLastName(rs.getString("insured_last_name"));
            application.setInsuredDob(rs.getDate("insured_dob"));
            application.setInsuredGender(rs.getString("insured_gender"));
            application.setInsuredNationality(rs.getString("insured_nationality"));
            application.setInsuredMultipleNationalities(getBooleanFromResultSet(rs, "insured_multiple_nationalities"));
            application.setInsuredNationality2(rs.getString("insured_nationality2"));
            application.setInsuredNationality3(rs.getString("insured_nationality3"));
            application.setInsuredBirthCountry(rs.getString("insured_birth_country"));// added birth country as a part of HSBCIDCU-892
            application.setInsuredNric(rs.getString("insured_nric"));
            application.setInsuredEmployment(rs.getString("insured_employment"));
            application.setInsuredIncome(getIntegerFromResultSet(rs, "insured_income"));
            application.setInsuredSmoker(getBooleanFromResultSet(rs, "insured_smoker"));
            application.setInsuredMedical1(getBooleanFromResultSet(rs, "insured_medical1"));
            application.setInsuredMedical2(getBooleanFromResultSet(rs, "insured_medical2"));
            application.setInsuredMedical3(getBooleanFromResultSet(rs, "insured_medical3"));
            application.setInsuredMedical4(getBooleanFromResultSet(rs, "insured_medical4"));
            application.setInsuredOccupation(rs.getString("insured_occupation"));
            application.setInsuredIndustry(rs.getString("insured_industry"));
            application.setInsuredCompanyName(rs.getString("insured_company_name"));
            application.setInsuredStudiesEndDate(rs.getDate("insured_studies_end_date"));

            application.setBeneficialOwnerFirstName(rs.getString("bo_first_name"));
            application.setBeneficialOwnerLastName(rs.getString("bo_last_name"));
            application.setBeneficialOwnerNric(rs.getString("bo_nric"));
            application.setBeneficialOwnerRelation(rs.getString("bo_relation"));

            application.setPaymentMode(rs.getInt("payment_mode"));
            application.setSumAssured(getFloatFromResultSet(rs, "sum_assured"));
            application.setRiderSumAssured(getFloatFromResultSet(rs, "rider_sum_assured"));
            application.setIncludeCriticalIllnessRider(getBooleanFromResultSet(rs, "include_ci_rider"));
            application.setIncludeRefundRider(getBooleanFromResultSet(rs, "include_pr_rider"));
            application.setUnderwritingLifestyleQuestionsOptin(getBooleanFromResultSet(rs, "uw_lifestyle_questions_optin"));
            application.setUnderwritingLifestyleMortgage(getIntegerFromResultSet(rs, "uw_lifestyle_mortgage"));
            application.setUnderwritingLifestyleProfile(getIntegerFromResultSet(rs, "uw_lifestyle_profile"));
            application.setUnderwritingLifestyleHeight(getFloatFromResultSet(rs, "uw_lifestyle_height"));
            application.setUnderwritingLifestyleWeight(getFloatFromResultSet(rs, "uw_lifestyle_weight"));
            application.setUnderwritingLifestyleExercise(getIntegerFromResultSet(rs, "uw_lifestyle_exercise"));

            application.setCreditCardType(rs.getString("cc_type"));
            application.setCreditCardName((new TextEncryptor()).decrypt(rs.getString("cc_name")));
            application.setCreditCardNum((new TextEncryptor()).decrypt(rs.getString("cc_num")));
            application.setCreditCardExpiry((new TextEncryptor()).decrypt(rs.getString("cc_expiry")));

            application.setMaxStep(rs.getFloat("max_step"));
            application.setEmailSentDate(rs.getTimestamp("email_sent_date"));
        } catch (Exception e) {
        	logger.error("eror in populating aapplication getting float "+e);
        	logger.error("Error Message "+e);
        }

        return application;
    }

    public boolean complete() {
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("INSERT INTO application_reference_nums (application_id) VALUES(?)");
            stmt.setString(1, getId());
            stmt.executeUpdate();
        } catch (Exception e) {
        	logger.error("Error Message "+e);
            return false;
        }	finally {
            db.closeAll(conn,stmt, rs);
        }

        String referenceNum = ApplicationUtils.getNextAppGlobalRefNum(getReferenceNumComplexityFlag(), getReferenceNumMailOptionFlag());
        setReferenceNum(referenceNum);
        setStatus(STATUS_COMPLETE);

        //Save email sent date in UTC to be consistent with db
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -8);
        Date utc = cal.getTime();
        logger.info("Reference number:" + getReferenceNum() + ",Email sent (UTC):" + utc.toString());
        setEmailSentDate(utc);

        return save();
    }

    @Override
    public boolean save() {
        Database db = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        try {

            String sql = "UPDATE applications SET ";
            Object[][] columns = new Object[][] {
                    new Object[] { "reference_num", "string", (getReferenceNum()) },
                    new Object[] { "agree_privacy", "boolean", (getAgreePrivacy()) },
                    new Object[] { "agree_important_notes", "boolean", (getAgreeImportantNotes()) },
                    new Object[] { "max_step", "float", getMaxStep() },
                    new Object[] { "life_insured", "string", getLifeInsured() },
                    new Object[] { "status", "string", getStatus() },
                    new Object[] { "dropout_password", "string", getDropoutPassword() },
                    new Object[] { "notify_status", "string", getNotifyStatus() },
                    new Object[] { "mail_option", "string", getMailOption() },
                    new Object[] { "campaign_code", "string", getCampaignCode() },

                    new Object[] { "applicant_residency", "string", getApplicantResidency() },
                    new Object[] { "applicant_first_name", "string", getApplicantFirstName() },
                    new Object[] { "applicant_last_name", "string", getApplicantLastName() },
                    new Object[] { "applicant_dob", "date", getApplicantDob() },
                    new Object[] { "applicant_gender", "string", getApplicantGender() },
                    new Object[] { "applicant_mobile_country_code", "string", getApplicantMobileCountryCode() },
                    new Object[] { "applicant_mobile_num", "string", getApplicantMobileNum() },
                    new Object[] { "applicant_email", "string", getApplicantEmail() },
                    new Object[] { "applicant_nationality", "string", getApplicantNationality() },
                    new Object[] { "applicant_multiple_nationalities", "boolean", getApplicantMultipleNationalities() },
                    new Object[] { "applicant_nationality2", "string", getApplicantNationality2() },
                    new Object[] { "applicant_nationality3", "string", getApplicantNationality3() },
                    new Object[] { "applicant_birth_country", "string", getApplicantBirthCountry() },
                    new Object[] { "applicant_nric", "string", getApplicantNric() },
                    new Object[] { "applicant_en_proficient", "boolean", getApplicantEnProficient() },
                    new Object[] { "applicant_employment", "string", getApplicantEmployment() },
                    new Object[] { "applicant_income", "string", getApplicantIncome() },
                    new Object[] { "applicant_smoker", "boolean", getApplicantSmoker() },
                    new Object[] { "applicant_occupation", "string", getApplicantOccupation() },
                    new Object[] { "applicant_industry", "string", getApplicantIndustry() },
                    new Object[] { "applicant_company_name", "string", getApplicantCompanyName() },
                    new Object[] { "applicant_company_city", "string", getApplicantCompanyCity() },
                    new Object[] { "applicant_company_country", "string", getApplicantCompanyCountry() },
                    new Object[] { "applicant_studies_end_date", "date", getApplicantStudiesEndDate() },
                    new Object[] { "applicant_address_postal_code", "string", getApplicantAddressPostalCode() },
                    new Object[] { "applicant_address_house", "string", getApplicantAddressHouse() },
                    new Object[] { "applicant_address_unit", "string", getApplicantAddressUnit() },
                    new Object[] { "applicant_address_street", "string", getApplicantAddressStreet() },
                    new Object[] { "applicant_address_building", "string", getApplicantAddressBuilding() },
                    new Object[] { "applicant_address_registered", "boolean", getApplicantAddressIsRegistered() },
                    new Object[] { "applicant_address_period_stay", "string", getApplicantAddressPeriodOfStay() },
                    new Object[] { "applicant_previous_address_country", "string", getApplicantPreviousAddressCountry() },
                    new Object[] { "applicant_address_permanent", "boolean", getApplicantAddressIsPermanent() },
                    new Object[] { "applicant_permanent_address_line1", "string", getApplicantPermanentAddressLine1() },
                    new Object[] { "applicant_permanent_address_line2", "string", getApplicantPermanentAddressLine2() },
                    new Object[] { "applicant_permanent_address_postal_code", "string", getApplicantPermanentAddressPostalCode() },
                    new Object[] { "applicant_permanent_address_country", "string", getApplicantPermanentAddressCountry() },
                    new Object[] { "applicant_tax_country1", "string", getApplicantTaxCountry1() },
                    new Object[] { "applicant_tax_country1_tin", "string", getApplicantTaxCountry1Tin() },
                    new Object[] { "applicant_tax_country1_tin_reason", "string", getApplicantTaxCountry1NoTinReason() },
                    new Object[] { "applicant_tax_country1_tin_reason_custom", "string", getApplicantTaxCountry1NoTinReasonCustom() },
                    new Object[] { "applicant_tax_country2", "string", getApplicantTaxCountry2() },
                    new Object[] { "applicant_tax_country2_tin", "string", getApplicantTaxCountry2Tin() },
                    new Object[] { "applicant_tax_country2_tin_reason", "string", getApplicantTaxCountry2NoTinReason() },
                    new Object[] { "applicant_tax_country2_tin_reason_custom", "string", getApplicantTaxCountry2NoTinReasonCustom() },
                    new Object[] { "applicant_tax_country3", "string", getApplicantTaxCountry3() },
                    new Object[] { "applicant_tax_country3_tin", "string", getApplicantTaxCountry3Tin() },
                    new Object[] { "applicant_tax_country3_tin_reason", "string", getApplicantTaxCountry3NoTinReason() },
                    new Object[] { "applicant_tax_country3_tin_reason_custom", "string", getApplicantTaxCountry3NoTinReasonCustom() },
                    new Object[] { "applicant_address_no_tax_reason", "string", getApplicantAddressNoTaxReason() },
                    new Object[] { "applicant_hsbc_customer", "boolean", getApplicantIsHsbcCustomer() },
                    new Object[] { "applicant_pep", "boolean", getApplicantIsPep() },
                    new Object[] { "applicant_bo", "boolean", getApplicantIsBeneficalOwner() },

                    new Object[] { "insured_residency", "string", getInsuredResidency() },
                    new Object[] { "insured_first_name", "string", getInsuredFirstName() },
                    new Object[] { "insured_last_name", "string", getInsuredLastName() },
                    new Object[] { "insured_dob", "date", getInsuredDob() },
                    new Object[] { "insured_gender", "string", getInsuredGender() },
                    new Object[] { "insured_nationality", "string", getInsuredNationality() },
                    new Object[] { "insured_multiple_nationalities", "boolean", getInsuredMultipleNationalities() },
                    new Object[] { "insured_nationality2", "string", getInsuredNationality2() },
                    new Object[] { "insured_nationality3", "string", getInsuredNationality3() },
                    new Object[] { "insured_nric", "string", getInsuredNric() },
                    new Object[] { "insured_employment", "string", getInsuredEmployment() },
                    new Object[] { "insured_income", "string", getInsuredIncome() },
                    new Object[] { "insured_smoker", "boolean", getInsuredSmoker() },
                    new Object[] { "insured_medical1", "boolean", getInsuredMedical1() },
                    new Object[] { "insured_medical2", "boolean", getInsuredMedical2() },
                    new Object[] { "insured_medical3", "boolean", getInsuredMedical3() },
                    new Object[] { "insured_medical4", "boolean", getInsuredMedical4() },
                    new Object[] { "insured_occupation", "string", getInsuredOccupation() },
                    new Object[] { "insured_industry", "string", getInsuredIndustry() },
                    new Object[] { "insured_company_name", "string", getInsuredCompanyName() },
                    new Object[] { "insured_studies_end_date", "date", getInsuredStudiesEndDate() },
                    new Object[] { "insured_birth_country", "string", getInsuredBirthCountry() }, //added Birth country dropdown for spouse insured as per HSBCIDCU-892

                    new Object[] { "bo_first_name", "string", getBeneficialOwnerFirstName() },
                    new Object[] { "bo_last_name", "string", getBeneficialOwnerLastName() },
                    new Object[] { "bo_nric", "string", getBeneficialOwnerNric() },
                    new Object[] { "bo_relation", "string", getBeneficialOwnerRelation() },

                    new Object[] { "payment_mode", "string", getPaymentMode() },
                    new Object[] { "sum_assured", "float", getSumAssured() },
                    new Object[] { "rider_sum_assured", "float", getRiderSumAssured() },
                    new Object[] { "include_ci_rider", "boolean", getIncludeCriticalIllnessRider() },
                    new Object[] { "include_pr_rider", "boolean", getIncludeRefundRider() },
                    new Object[] { "uw_lifestyle_questions_optin", "boolean", getUnderwritingLifestyleQuestionsOptin() },
                    new Object[] { "uw_lifestyle_mortgage", "string", getUnderwritingLifestyleMortgage() },
                    new Object[] { "uw_lifestyle_profile", "string", getUnderwritingLifestyleProfile() },
                    new Object[] { "uw_lifestyle_height", "string", getUnderwritingLifestyleHeight() },
                    new Object[] { "uw_lifestyle_weight", "string", getUnderwritingLifestyleWeight() },
                    new Object[] { "uw_lifestyle_exercise", "string", getUnderwritingLifestyleExercise() },

                    new Object[] { "cc_type", "string", getCreditCardType() },
                    new Object[] { "cc_name", "string", (new TextEncryptor()).encrypt(getCreditCardName()) },
                    new Object[] { "cc_num", "string", (new TextEncryptor()).encrypt(getCreditCardNum()) },
                    new Object[] { "cc_expiry", "string", (new TextEncryptor()).encrypt(getCreditCardExpiry()) },
                    new Object[] { "email_sent_date", "timestamp", getEmailSentDate() }
            };
            for (int i = 0; i < columns.length; i++) {
                sql += columns[i][0] + " = ?, ";
            }
            sql += "date_updated = NOW() WHERE id = ?";

            db = new Database();
            conn = db.getConnection();
            stmt = conn.prepareStatement(sql);
            for (int i = 0; i < columns.length; i++) {
                switch (columns[i][1].toString()) {
                    case "string":
                        stmt.setObject((i + 1), columns[i][2]);
                        break;
                    case "boolean":
                        stmt.setObject((i + 1), columns[i][2]);
                        break;
                    case "float":
                        stmt.setObject((i + 1), columns[i][2]);
                        break;
                    case "date":
                        stmt.setDate((i + 1), (columns[i][2] != null ? new java.sql.Date(((Date)columns[i][2]).getTime()) : null));
                        break;
                    case "timestamp":
                        stmt.setTimestamp((i + 1), (columns[i][2] != null ? new java.sql.Timestamp(((Date)columns[i][2]).getTime()) : null));
                        break;
                }
            }
            stmt.setString((columns.length + 1), id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e, "Error saving application");
            return false;
        } finally {
            db.closeAll(conn,stmt);
        }

        if (getFeedbackExperience() != null || getFeedbackEase() != null || getFeedbackRecommend() != null || getFeedbackNoRecommendReason() != null || getFeedbackImprove() != null || getFeedbackAdditional() != null) {
             try{
                conn = db.getConnection();
                stmt = conn.prepareStatement("INSERT INTO feedback (application_id, feedback_experience, feedback_ease, feedback_recommend, feedback_norecommend_reason, feedback_improve, feedback_additional) VALUES (?, ?, ?, ?, ?, ?, ?) ");
                stmt.setString(1, id);
                stmt.setInt(2, getFeedbackExperience());
                stmt.setInt(3, getFeedbackEase());
                if (getFeedbackRecommend()!=null)
                    stmt.setBoolean(4, getFeedbackRecommend());
                else
                    stmt.setString(4, null);
                stmt.setString(5, getFeedbackNoRecommendReason());
                stmt.setString(6, getFeedbackImprove());
                stmt.setString(7, getFeedbackAdditional());
                stmt.executeUpdate();
             } catch (SQLException e) {
                ErrorHandler.handleError(this, e, "Error saving feedback for application");
                return false;
            } finally {
                db.closeAll(conn,stmt);
            }
        }
        return true;
    }

    /**
     * Save feedback into feedback table
     * @return
     */
    public boolean saveFeedback() {
        Database db = null;
        Connection conn = null;
        PreparedStatement stmt = null;

        if (getFeedbackExperience() != null || getFeedbackEase() != null || getFeedbackRecommend() != null || getFeedbackNoRecommendReason() != null || getFeedbackImprove() != null || getFeedbackAdditional() != null) {
            try{
                db = new Database();
                conn = db.getConnection();
                stmt = conn.prepareStatement("INSERT INTO feedback (application_id, feedback_experience, feedback_ease, feedback_recommend, feedback_norecommend_reason, feedback_improve, feedback_additional) VALUES (?, ?, ?, ?, ?, ?, ?) ");
                stmt.setString(1, id);
                stmt.setInt(2, getFeedbackExperience());
                stmt.setInt(3, getFeedbackEase());
                if (getFeedbackRecommend()!=null)
                    stmt.setBoolean(4, getFeedbackRecommend());
                else
                    stmt.setString(4, null);
                stmt.setString(5, getFeedbackNoRecommendReason());
                stmt.setString(6, getFeedbackImprove());
                stmt.setString(7, getFeedbackAdditional());
                stmt.executeUpdate();
            } catch (SQLException e) {
                ErrorHandler.handleError(this, e, "Error saving feedback for application");
                return false;
            } finally {
                db.closeAll(conn,stmt);
            }
        }
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceNum() {
        return referenceNum;
    }

    public void setReferenceNum(String referenceNum) {
        this.referenceNum = referenceNum;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public Boolean getAgreePrivacy() {
        return agreePrivacy;
    }

    public void setAgreePrivacy(Boolean agreePrivacy) {
        this.agreePrivacy = agreePrivacy;
    }

    public Boolean getAgreeImportantNotes() {
        return agreeImportantNotes;
    }

    public void setAgreeImportantNotes(Boolean agreeImportantNotes) {
        this.agreeImportantNotes = agreeImportantNotes;
    }

    public String getLifeInsured() {
        return lifeInsured;
    }

    public void setLifeInsured(String lifeInsured) {
        this.lifeInsured = lifeInsured;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDropoutPassword() {
        return dropoutPassword;
    }

    public void setDropoutPassword(String dropoutPassword) {
        this.dropoutPassword = dropoutPassword;
    }

    public int getNotifyStatus() {
        return notifyStatus;
    }

    public void setNotifyStatus(int notifyStatus) {
        this.notifyStatus = notifyStatus;
    }

    public int getMailOption() {
        return mailOption;
    }

    public void setMailOption(int mailOption) {
        this.mailOption = mailOption;
    }

    public String getApplicantResidency() {
        return applicantResidency;
    }

    public void setApplicantResidency(String applicantResidency) {
        this.applicantResidency = applicantResidency;
    }

    public String getApplicantFirstName() {
        return applicantFirstName;
    }

    public void setApplicantFirstName(String applicantFirstName) {
        this.applicantFirstName = applicantFirstName;
    }

    public String getApplicantLastName() {
        return applicantLastName;
    }

    public void setApplicantLastName(String applicantLastName) {
        this.applicantLastName = applicantLastName;
    }

    public Date getApplicantDob() {
        return applicantDob;
    }

    public void setApplicantDob(Date applicantDob) {
        this.applicantDob = applicantDob;
    }

    public String getApplicantGender() {
        return applicantGender;
    }

    public void setApplicantGender(String applicantGender) {
        this.applicantGender = applicantGender;
    }

    public String getApplicantMobileCountryCode() {
        return applicantMobileCountryCode;
    }

    public void setApplicantMobileCountryCode(String applicantMobileCountryCode) {
        this.applicantMobileCountryCode = applicantMobileCountryCode;
    }

    public String getApplicantMobileNum() {
        return applicantMobileNum;
    }

    public void setApplicantMobileNum(String applicantMobileNum) {
        this.applicantMobileNum = applicantMobileNum;
    }

    public String getApplicantEmail() {
        return applicantEmail;
    }

    public void setApplicantEmail(String applicantEmail) {
        this.applicantEmail = applicantEmail;
    }

    public String getApplicantNationality() {
        return applicantNationality;
    }

    public void setApplicantNationality(String applicantNationality) {
        this.applicantNationality = applicantNationality;
    }

    public Boolean getApplicantMultipleNationalities() {
        return applicantMultipleNationalities;
    }

    public void setApplicantMultipleNationalities(Boolean applicantMultipleNationalities) {
        this.applicantMultipleNationalities = applicantMultipleNationalities;
    }

    public String getApplicantNationality2() {
        return applicantNationality2;
    }

    public void setApplicantNationality2(String applicantNationality2) {
        this.applicantNationality2 = applicantNationality2;
    }

    public String getApplicantNationality3() {
        return applicantNationality3;
    }

    public void setApplicantNationality3(String applicantNationality3) {
        this.applicantNationality3 = applicantNationality3;
    }

    public String getApplicantBirthCountry() {
        return applicantBirthCountry;
    }

    public void setApplicantBirthCountry(String applicantBirthCountry) {
        this.applicantBirthCountry = applicantBirthCountry;
    }

    public String getApplicantNric() {
        return applicantNric;
    }

    public void setApplicantNric(String applicantNric) {
        this.applicantNric = applicantNric;
    }

    public Boolean getApplicantEnProficient() {
        return applicantEnProficient;
    }

    public void setApplicantEnProficient(Boolean applicantEnProficient) {
        this.applicantEnProficient = applicantEnProficient;
    }

    public String getApplicantEmployment() {
        return applicantEmployment;
    }

    public void setApplicantEmployment(String applicantEmployment) {
        this.applicantEmployment = applicantEmployment;
    }

    public Integer getApplicantIncome() {
        return applicantIncome;
    }

    public void setApplicantIncome(Integer applicantIncome) {
        this.applicantIncome = applicantIncome;
    }

    public Boolean getApplicantSmoker() {
        return applicantSmoker;
    }

    public void setApplicantSmoker(Boolean applicantSmoker) {
        this.applicantSmoker = applicantSmoker;
    }

    public String getApplicantOccupation() {
        return applicantOccupation;
    }

    public void setApplicantOccupation(String applicantOccupation) {
        this.applicantOccupation = applicantOccupation;
    }

    public String getApplicantIndustry() {
        return applicantIndustry;
    }

    public void setApplicantIndustry(String applicantIndustry) {
        this.applicantIndustry = applicantIndustry;
    }

    public String getApplicantCompanyName() {
        return applicantCompanyName;
    }

    public void setApplicantCompanyName(String applicantCompanyName) {
        this.applicantCompanyName = applicantCompanyName;
    }

    public String getApplicantCompanyCity() {
        return applicantCompanyCity;
    }

    public void setApplicantCompanyCity(String applicantCompanyCity) {
        this.applicantCompanyCity = applicantCompanyCity;
    }

    public String getApplicantCompanyCountry() {
        return applicantCompanyCountry;
    }

    public void setApplicantCompanyCountry(String applicantCompanyCountry) {
        this.applicantCompanyCountry = applicantCompanyCountry;
    }

    public Date getApplicantStudiesEndDate() {
        return applicantStudiesEndDate;
    }

    public void setApplicantStudiesEndDate(Date applicantStudiesEndDate) {
        this.applicantStudiesEndDate = applicantStudiesEndDate;
    }

    public String getApplicantAddressPostalCode() {
        return applicantAddressPostalCode;
    }

    public void setApplicantAddressPostalCode(String applicantAddressPostalCode) {
        this.applicantAddressPostalCode = applicantAddressPostalCode;
    }

    public String getApplicantAddressHouse() {
        return applicantAddressHouse;
    }

    public void setApplicantAddressHouse(String applicantAddressHouse) {
        this.applicantAddressHouse = applicantAddressHouse;
    }

    public String getApplicantAddressUnit() {
        return applicantAddressUnit;
    }

    public void setApplicantAddressUnit(String applicantAddressUnit) {
        this.applicantAddressUnit = applicantAddressUnit;
    }

    public String getApplicantAddressStreet() {
        return applicantAddressStreet;
    }

    public void setApplicantAddressStreet(String applicantAddressStreet) {
        this.applicantAddressStreet = applicantAddressStreet;
    }

    public String getApplicantAddressBuilding() {
        return applicantAddressBuilding;
    }

    public void setApplicantAddressBuilding(String applicantAddressBuilding) {
        this.applicantAddressBuilding = applicantAddressBuilding;
    }

    public Boolean getApplicantAddressIsRegistered() {
        return applicantAddressIsRegistered;
    }

    public void setApplicantAddressIsRegistered(Boolean applicantAddressRegistered) {
        this.applicantAddressIsRegistered = applicantAddressRegistered;
    }

    public Integer getApplicantAddressPeriodOfStay() {
        return applicantAddressPeriodOfStay;
    }

    public void setApplicantAddressPeriodOfStay(Integer applicantAddressPeriodOfStay) {
        this.applicantAddressPeriodOfStay = applicantAddressPeriodOfStay;
    }

    public String getApplicantPreviousAddressCountry() {
        return applicantPreviousAddressCountry;
    }

    public void setApplicantPreviousAddressCountry(String applicantPreviousAddressCountry) {
        this.applicantPreviousAddressCountry = applicantPreviousAddressCountry;
    }

    public Boolean getApplicantAddressIsPermanent() {
        return applicantAddressIsPermanent;
    }

    public void setApplicantAddressIsPermanent(Boolean applicantAddressIsPermanent) {
        this.applicantAddressIsPermanent = applicantAddressIsPermanent;
    }

    public String getApplicantPermanentAddressLine1() {
        return applicantPermanentAddressLine1;
    }

    public void setApplicantPermanentAddressLine1(String applicantPermanentAddressLine1) {
        this.applicantPermanentAddressLine1 = applicantPermanentAddressLine1;
    }

    public String getApplicantPermanentAddressLine2() {
        return applicantPermanentAddressLine2;
    }

    public void setApplicantPermanentAddressLine2(String applicantPermanentAddressLine2) {
        this.applicantPermanentAddressLine2 = applicantPermanentAddressLine2;
    }

    public String getApplicantPermanentAddressPostalCode() {
        return applicantPermanentAddressPostalCode;
    }

    public void setApplicantPermanentAddressPostalCode(String applicantPermanentAddressPostalCode) {
        this.applicantPermanentAddressPostalCode = applicantPermanentAddressPostalCode;
    }

    public String getApplicantPermanentAddressCountry() {
        return applicantPermanentAddressCountry;
    }

    public void setApplicantPermanentAddressCountry(String applicantPermanentAddressCountry) {
        this.applicantPermanentAddressCountry = applicantPermanentAddressCountry;
    }

    public String getApplicantTaxCountry1() {
        return applicantTaxCountry1;
    }

    public void setApplicantTaxCountry1(String applicantTaxCountry1) {
        this.applicantTaxCountry1 = applicantTaxCountry1;
    }

    public String getApplicantTaxCountry1Tin() {
        return applicantTaxCountry1Tin;
    }

    public void setApplicantTaxCountry1Tin(String applicantTaxCountry1Tin) {
        this.applicantTaxCountry1Tin = applicantTaxCountry1Tin;
    }

    public String getApplicantTaxCountry1NoTinReason() {
        return applicantTaxCountry1NoTinReason;
    }

    public void setApplicantTaxCountry1NoTinReason(String applicantTaxCountry1NoTinReason) {
        this.applicantTaxCountry1NoTinReason = applicantTaxCountry1NoTinReason;
    }

    public String getApplicantTaxCountry1NoTinReasonCustom() {
        return applicantTaxCountry1NoTinReasonCustom;
    }

    public void setApplicantTaxCountry1NoTinReasonCustom(String applicantTaxCountry1NoTinReasonCustom) {
        this.applicantTaxCountry1NoTinReasonCustom = applicantTaxCountry1NoTinReasonCustom;
    }

    public String getApplicantTaxCountry2() {
        return applicantTaxCountry2;
    }

    public void setApplicantTaxCountry2(String applicantTaxCountry2) {
        this.applicantTaxCountry2 = applicantTaxCountry2;
    }

    public String getApplicantTaxCountry2Tin() {
        return applicantTaxCountry2Tin;
    }

    public void setApplicantTaxCountry2Tin(String applicantTaxCountry2Tin) {
        this.applicantTaxCountry2Tin = applicantTaxCountry2Tin;
    }

    public String getApplicantTaxCountry2NoTinReason() {
        return applicantTaxCountry2NoTinReason;
    }

    public void setApplicantTaxCountry2NoTinReason(String applicantTaxCountry2NoTinReason) {
        this.applicantTaxCountry2NoTinReason = applicantTaxCountry2NoTinReason;
    }

    public String getApplicantTaxCountry2NoTinReasonCustom() {
        return applicantTaxCountry2NoTinReasonCustom;
    }

    public void setApplicantTaxCountry2NoTinReasonCustom(String applicantTaxCountry2NoTinReasonCustom) {
        this.applicantTaxCountry2NoTinReasonCustom = applicantTaxCountry2NoTinReasonCustom;
    }

    public String getApplicantTaxCountry3() {
        return applicantTaxCountry3;
    }

    public void setApplicantTaxCountry3(String applicantTaxCountry3) {
        this.applicantTaxCountry3 = applicantTaxCountry3;
    }

    public String getApplicantTaxCountry3Tin() {
        return applicantTaxCountry3Tin;
    }

    public void setApplicantTaxCountry3Tin(String applicantTaxCountry3Tin) {
        this.applicantTaxCountry3Tin = applicantTaxCountry3Tin;
    }

    public String getApplicantTaxCountry3NoTinReason() {
        return applicantTaxCountry3NoTinReason;
    }

    public void setApplicantTaxCountry3NoTinReason(String applicantTaxCountry3NoTinReason) {
        this.applicantTaxCountry3NoTinReason = applicantTaxCountry3NoTinReason;
    }

    public String getApplicantTaxCountry3NoTinReasonCustom() {
        return applicantTaxCountry3NoTinReasonCustom;
    }

    public void setApplicantTaxCountry3NoTinReasonCustom(String applicantTaxCountry3NoTinReasonCustom) {
        this.applicantTaxCountry3NoTinReasonCustom = applicantTaxCountry3NoTinReasonCustom;
    }

    public String getApplicantAddressNoTaxReason() {
        return applicantAddressNoTaxReason;
    }

    public void setApplicantAddressNoTaxReason(String applicantAddressNoTaxReason) {
        this.applicantAddressNoTaxReason = applicantAddressNoTaxReason;
    }

    public Boolean getApplicantIsHsbcCustomer() {
        return applicantIsHsbcCustomer;
    }

    public void setApplicantIsHsbcCustomer(Boolean applicantIsHsbcCustomer) {
        this.applicantIsHsbcCustomer = applicantIsHsbcCustomer;
    }

    public Boolean getApplicantIsPep() {
        return applicantIsPep;
    }

    public void setApplicantIsPep(Boolean applicantIsPep) {
        this.applicantIsPep = applicantIsPep;
    }

    public Boolean getApplicantIsBeneficalOwner() {
        return applicantIsBeneficalOwner;
    }

    public void setApplicantIsBeneficalOwner(Boolean applicantIsBeneficalOwner) {
        this.applicantIsBeneficalOwner = applicantIsBeneficalOwner;
    }

    public String getApplicantFullName() {
        return String.format("%s %s", getApplicantFirstName(), getApplicantLastName());
    }

    public int getApplicantAge() {
        Date dob = getApplicantDob();

        return Period.between((new java.sql.Date(dob.getTime())).toLocalDate(), (new java.sql.Date((new Date()).getTime())).toLocalDate()).getYears();
    }

    public String getApplicantFullMobileNum() {
        return String.format("%s%s", getApplicantMobileCountryCode(), getApplicantMobileNum());
    }

    public String getInsuredResidency() {
        return insuredResidency;
    }

    public void setInsuredResidency(String insuredResidency) {
        this.insuredResidency = insuredResidency;
    }

    public String getInsuredFirstName() {
        return insuredFirstName;
    }

    public void setInsuredFirstName(String insuredFirstName) {
        this.insuredFirstName = insuredFirstName;
    }

    public String getInsuredLastName() {
        return insuredLastName;
    }

    public void setInsuredLastName(String insuredLastName) {
        this.insuredLastName = insuredLastName;
    }

    public Date getInsuredDob() {
        return insuredDob;
    }

    public void setInsuredDob(Date insuredDob) {
        this.insuredDob = insuredDob;
    }

    public String getInsuredGender() {
        return insuredGender;
    }

    public void setInsuredGender(String insuredGender) {
        this.insuredGender = insuredGender;
    }

    public String getInsuredNationality() {
        return insuredNationality;
    }

    public void setInsuredNationality(String insuredNationality) {
        this.insuredNationality = insuredNationality;
    }

    public Boolean getInsuredMultipleNationalities() {
        return insuredMultipleNationalities;
    }

    public void setInsuredMultipleNationalities(Boolean insuredMultipleNationalities) {
        this.insuredMultipleNationalities = insuredMultipleNationalities;
    }

    public String getInsuredNationality2() {
        return insuredNationality2;
    }

    public void setInsuredNationality2(String insuredNationality2) {
        this.insuredNationality2 = insuredNationality2;
    }

    public String getInsuredNationality3() {
        return insuredNationality3;
    }

    public void setInsuredNationality3(String insuredNationality3) {
        this.insuredNationality3 = insuredNationality3;
    }

    public String getInsuredNric() {
        return insuredNric;
    }

    public void setInsuredNric(String insuredNric) {
        this.insuredNric = insuredNric;
    }

    public String getInsuredEmployment() {
        return insuredEmployment;
    }

    public void setInsuredEmployment(String insuredEmployment) {
        this.insuredEmployment = insuredEmployment;
    }

    public Integer getInsuredIncome() {
        return insuredIncome;
    }

    public void setInsuredIncome(Integer insuredIncome) {
        this.insuredIncome = insuredIncome;
    }

    public Boolean getInsuredSmoker() {
        return insuredSmoker;
    }

    public void setInsuredSmoker(Boolean insuredSmoker) {
        this.insuredSmoker = insuredSmoker;
    }

    public Boolean getInsuredMedical1() {
        return insuredMedical1;
    }

    public void setInsuredMedical1(Boolean insuredMedical1) {
        this.insuredMedical1 = insuredMedical1;
    }

    public Boolean getInsuredMedical2() {
        return insuredMedical2;
    }

    public void setInsuredMedical2(Boolean insuredMedical2) {
        this.insuredMedical2 = insuredMedical2;
    }

    public Boolean getInsuredMedical3() {
        return insuredMedical3;
    }

    public void setInsuredMedical3(Boolean insuredMedical3) {
        this.insuredMedical3 = insuredMedical3;
    }

    public Boolean getInsuredMedical4() {
        return insuredMedical4;
    }

    public void setInsuredMedical4(Boolean insuredMedical4) {
        this.insuredMedical4 = insuredMedical4;
    }

    public String getInsuredOccupation() {
        return insuredOccupation;
    }

    public void setInsuredOccupation(String insuredOccupation) {
        this.insuredOccupation = insuredOccupation;
    }

    public String getInsuredIndustry() {
        return insuredIndustry;
    }

    public void setInsuredIndustry(String insuredIndustry) {
        this.insuredIndustry = insuredIndustry;
    }

    public String getInsuredCompanyName() {
        return insuredCompanyName;
    }

    public void setInsuredCompanyName(String insuredCompanyName) {
        this.insuredCompanyName = insuredCompanyName;
    }

    public Date getInsuredStudiesEndDate() {
        return insuredStudiesEndDate;
    }

    public void setInsuredStudiesEndDate(Date insuredStudiesEndDate) {
        this.insuredStudiesEndDate = insuredStudiesEndDate;
    }

    public String getInsuredFullName() {
        return String.format("%s %s", getInsuredFirstName(), getInsuredLastName());
    }

    public int getInsuredAge() {
        Date dob = getInsuredDob();

        return Period.between((new java.sql.Date(dob.getTime())).toLocalDate(), (new java.sql.Date((new Date()).getTime())).toLocalDate()).getYears();
    }

    public String getBeneficialOwnerFirstName() {
        return beneficialOwnerFirstName;
    }

    public void setBeneficialOwnerFirstName(String beneficialOwnerFirstName) {
        this.beneficialOwnerFirstName = beneficialOwnerFirstName;
    }

    public String getBeneficialOwnerLastName() {
        return beneficialOwnerLastName;
    }

    public void setBeneficialOwnerLastName(String beneficialOwnerLastName) {
        this.beneficialOwnerLastName = beneficialOwnerLastName;
    }

    public String getBeneficialOwnerNric() {
        return beneficialOwnerNric;
    }

    public void setBeneficialOwnerNric(String beneficialOwnerNric) {
        this.beneficialOwnerNric = beneficialOwnerNric;
    }

    public String getBeneficialOwnerRelation() {
        return beneficialOwnerRelation;
    }

    public void setBeneficialOwnerRelation(String beneficialOwnerRelation) {
        this.beneficialOwnerRelation = beneficialOwnerRelation;
    }

    public int getTermYears() {
        return termYears;
    }

    public void setTermYears(int termYears) {
        this.termYears = termYears;
    }

    public Float getSumAssured() {
        return sumAssured;
    }

    public void setSumAssured(Float sumAssured) {
        this.sumAssured = sumAssured;
    }

    public Float getRiderSumAssured() {
        return riderSumAssured;
    }

    public void setRiderSumAssured(Float riderSumAssured) {
        this.riderSumAssured = riderSumAssured;
    }

    public int getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(int paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Boolean getIncludeCriticalIllnessRider() {
        return includeCriticalIllnessRider;
    }

    public void setIncludeCriticalIllnessRider(Boolean includeCriticalIllnessRider) {
        this.includeCriticalIllnessRider = includeCriticalIllnessRider;
    }

    public Boolean getIncludeRefundRider() {
        return includeRefundRider;
    }

    public void setIncludeRefundRider(Boolean includeRefundRider) {
        this.includeRefundRider = includeRefundRider;
    }
    public String getPaymentModeLabelPdf() {
        String label = null;
        switch (getPaymentMode()) {
            case PAYMENT_MODE_ANNUALLY:
                label = "Annually";
                break;
            case PAYMENT_MODE_SEMI_ANNUALLY:
                label = "Semi-Annually";
                break;
            case PAYMENT_MODE_QUARTERLY:
                label = "Quarterly";
                break;
            case PAYMENT_MODE_MONTHLY:
                label = "Monthly";
                break;
        }

        return label;
    }
    public String getPaymentModeLabel() {
        String label = null;
        switch (getPaymentMode()) {
            case PAYMENT_MODE_ANNUALLY:
                label = "Annual";
                break;
            case PAYMENT_MODE_SEMI_ANNUALLY:
                label = "Semi-Annual";
                break;
            case PAYMENT_MODE_QUARTERLY:
                label = "Quarterly";
                break;
            case PAYMENT_MODE_MONTHLY:
                label = "Monthly";
                break;
        }

        return label;
    }

    public Boolean getUnderwritingLifestyleQuestionsOptin() {
        return underwritingLifestyleQuestionsOptin;
    }

    public void setUnderwritingLifestyleQuestionsOptin(Boolean underwritingLifestyleQuestionsOptin) {
        this.underwritingLifestyleQuestionsOptin = underwritingLifestyleQuestionsOptin;
    }

    public Integer getUnderwritingLifestyleMortgage() {
        return underwritingLifestyleMortgage;
    }

    public void setUnderwritingLifestyleMortgage(Integer underwritingLifestyleMortgage) {
        this.underwritingLifestyleMortgage = underwritingLifestyleMortgage;
    }

    public Integer getUnderwritingLifestyleProfile() {
        return underwritingLifestyleProfile;
    }

    public void setUnderwritingLifestyleProfile(Integer underwritingLifestyleProfile) {
        this.underwritingLifestyleProfile = underwritingLifestyleProfile;
    }

    public Float getUnderwritingLifestyleHeight() {
        return underwritingLifestyleHeight;
    }

    public void setUnderwritingLifestyleHeight(Float underwritingLifestyleHeight) {
        this.underwritingLifestyleHeight = underwritingLifestyleHeight;
    }

    public Float getUnderwritingLifestyleWeight() {
        return underwritingLifestyleWeight;
    }

    public void setUnderwritingLifestyleWeight(Float underwritingLifestyleWeight) {
        this.underwritingLifestyleWeight = underwritingLifestyleWeight;
    }

    public Integer getUnderwritingLifestyleExercise() {
        return underwritingLifestyleExercise;
    }

    public void setUnderwritingLifestyleExercise(Integer underwritingLifestyleExercise) {
        this.underwritingLifestyleExercise = underwritingLifestyleExercise;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getCreditCardName() {
        return creditCardName;
    }

    public void setCreditCardName(String creditCardName) {
        this.creditCardName = creditCardName;
    }

    public String getCreditCardNum() {
        return creditCardNum;
    }

    public void setCreditCardNum(String creditCardNum) {
        this.creditCardNum = creditCardNum;
    }

    public String getCreditCardExpiry() {
        return creditCardExpiry;
    }

    public void setCreditCardExpiry(String creditCardExpiry) {
        this.creditCardExpiry = creditCardExpiry;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public float getMaxStep() {
        return maxStep;
    }

    public void setMaxStep(float maxStep) {
        if (this.maxStep < maxStep) {
            this.maxStep = maxStep;
        }
    }

    public Integer getFeedbackExperience() {
        return feedbackExperience;
    }

    public void setFeedbackExperience(Integer feedbackExperience) {
        this.feedbackExperience = feedbackExperience;
    }

    public Integer getFeedbackEase() {
        return feedbackEase;
    }

    public void setFeedbackEase(Integer feedbackEase) {
        this.feedbackEase = feedbackEase;
    }

    public Boolean getFeedbackRecommend() {
        return feedbackRecommend;
    }

    public void setFeedbackRecommend(Boolean feedbackRecommend) {
        this.feedbackRecommend = feedbackRecommend;
    }

    public String getFeedbackNoRecommendReason() {
        return feedbackNoRecommendReason;
    }

    public void setFeedbackNoRecommendReason(String feedbackNoRecommendReason) {
        this.feedbackNoRecommendReason = feedbackNoRecommendReason;
    }

    public String getFeedbackImprove() {
        return feedbackImprove;
    }

    public void setFeedbackImprove(String feedbackImprove) {
        this.feedbackImprove = feedbackImprove;
    }

    public String getFeedbackAdditional() {
        return feedbackAdditional;
    }

    public void setFeedbackAdditional(String feedbackAdditional) {
        this.feedbackAdditional = feedbackAdditional;
    }

   /**
    * This method is used for fetching insuredBirthCountry
    * 
    * @return  insuredBirthCountry
    */
    public String getInsuredBirthCountry() {
      return insuredBirthCountry;
    }
    
   /**
   * This method is used for setting insuredBirthCountry variable with the country 
   * of birth fetched from the form.
   *
   * @param insuredBirthCountry  String variable containing birth country of insured 
   */
    public void setInsuredBirthCountry(String insuredBirthCountry) {
      this.insuredBirthCountry = insuredBirthCountry;
    }

    public Boolean insuredIsSelf() {
        if (getLifeInsured() != null) {
            return getLifeInsured().equals("self");
        }

        return null;
    }

    public Boolean applicantIsWorking() {
        if (getApplicantEmployment() == null) {
            return false;
        }

        return (getApplicantEmployment().equals("Salaried") || getApplicantEmployment().equals("Self-employed"));
    }

    public Boolean applicantIsSingaporean() {
        if (getApplicantResidency() == null) {
            return false;
        }

        return (getApplicantResidency().equals("Singaporean"));
    }

    public Boolean applicantIsPermanentResident() {
        if (getApplicantResidency() == null) {
            return false;
        }

        return getApplicantResidency().equals("Singapore PR");
    }

    public Boolean applicantIsPassHolder() {
        if (getApplicantResidency() == null) {
            return false;
        }

        return (getApplicantResidency().equals("Employment Pass") ||
                getApplicantResidency().equals("Skilled Pass") ||
                getApplicantResidency().equals("Personalised Employment Pass") ||
                getApplicantResidency().equals("Dependent Pass") ||
                getApplicantResidency().equals("Student Pass"));
    }

    public Boolean applicantIsStudying() {
        return (getApplicantEmployment().equals("Student"));
    }

    public Boolean applicantIsSelfEmployed() {
        return (getApplicantEmployment().equals("Self-employed"));
    }

    public Boolean insuredIsWorking() {
        return (getInsuredEmployment().equals("Salaried") || getInsuredEmployment().equals("Self-employed"));
    }
    public Boolean pdfRequired(){
        if(getApplicantResidency().equalsIgnoreCase("Singaporean")||getApplicantResidency().equalsIgnoreCase("Singapore PR")){
            return false;
        }else{
            return true;
        }

    }
    public Boolean insuredIsSingaporean() {
        if (getInsuredResidency() == null) {
            return false;
        }

        return (getInsuredResidency().equals("Singaporean"));
    }

    public Boolean insuredIsPermanentResident() {
        if (getInsuredResidency() == null) {
            return false;
        }

        return getInsuredResidency().equals("Singapore PR");
    }

    public Boolean insuredIsPassHolder() {
        if (getInsuredResidency() == null) {
            return false;
        }

        return (getInsuredResidency().equals("Employment Pass") ||
                getInsuredResidency().equals("Skilled Pass") ||
                getInsuredResidency().equals("Personalised Employment Pass") ||
                getInsuredResidency().equals("Dependent Pass") ||
                getInsuredResidency().equals("Student Pass"));
    }

    public Boolean insuredIsStudying() {
        return (getInsuredEmployment().equals("Student"));
    }

    public Boolean insuredIsSelfEmployed() {
        return (getInsuredEmployment().equals("Self-employed"));
    }

    private static Boolean getBooleanFromResultSet(ResultSet rs, String columnLabel) {
        Boolean bool;
        try {
            bool = rs.getBoolean(columnLabel);
            if (rs.wasNull()) {
                bool = null;
            } else {
            }
        } catch (SQLException e) {
            bool = null;
        }

        return bool;
    }

    private double getPaymentModePremiumFactor() {
        return getPaymentModePremiumFactor(getPaymentMode());
    }

    private double getPaymentModePremiumFactor(int paymentMode) {
        double factor;
        switch (paymentMode) {
            case PAYMENT_MODE_MONTHLY:
                factor = 0.085;
                break;
            case PAYMENT_MODE_QUARTERLY:
                factor = 0.255;
                break;
            case PAYMENT_MODE_SEMI_ANNUALLY:
                factor = 0.51;
                break;
            case PAYMENT_MODE_ANNUALLY:
            default:
                factor = 1;
                break;
        }

        return factor;
    }

    private float getPremiumPaymentFrequencyFactor() {
        return getPremiumPaymentFrequencyFactor(getPaymentMode());
    }

    private float getPremiumPaymentFrequencyFactor(int paymentMode) {
        float factor;
        switch (paymentMode) {
            case PAYMENT_MODE_MONTHLY:
                factor = 12;
                break;
            case PAYMENT_MODE_QUARTERLY:
                factor = 4;
                break;
            case PAYMENT_MODE_SEMI_ANNUALLY:
                factor = 2;
                break;
            case PAYMENT_MODE_ANNUALLY:
            default:
                factor = 1;
                break;
        }

        return factor;
    }

    public int getUnderwritingClassPreferentialPoints() {
        int points = 0;
        if (isEligiblePreferentialRates() && getUnderwritingLifestyleQuestionsOptin() != null && getUnderwritingLifestyleQuestionsOptin()) {
        	//No points for smoking now
        	/* if ((insuredIsSelf() && !getApplicantSmoker()) || (!insuredIsSelf() && !getInsuredSmoker())) {
                points += 1;
            }*/

            float income = (insuredIsSelf() ? getApplicantIncome() : getInsuredIncome());
            if (income > 250000) {
                points += 2;
            } else {
                if (income > 150000) {
                    points += 1;
                }
            }
            if (getUnderwritingLifestyleMortgage()!= null) {
                switch (getUnderwritingLifestyleMortgage()) {
                    case UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_12_MONTHS:
                        points += 2;
                        break;
                    case UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_24_MONTHS:
                        points += 1;
                        break;
                }
            }
            if (getUnderwritingLifestyleProfile() != null) {
                switch (getUnderwritingLifestyleProfile()) {
                    case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_JADE:
                        points += 2;
                        break;
                    case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_PREMIER:
                        points += 1;
                        break;
                }
            }
            if (getUnderwritingLifestyleWeight() != null && getUnderwritingLifestyleHeight() != null) {
                float weight = getUnderwritingLifestyleWeight();
                float height = getUnderwritingLifestyleHeight();
                float heightInMetres=height/100;
                double bmi = weight  / (heightInMetres*heightInMetres);
                double bmiDec=bmi-Math.floor(bmi);
                if(bmiDec>=0.5){
                	bmi=Math.ceil(bmi);
                }else{
                	bmi=Math.floor(bmi);
                }

                if (bmi < 25) {
                    points += 2;
                } else {
                    if (bmi < 30) {
                        points += 1;
                    }
                }
            }
            if (getUnderwritingLifestyleExercise() != null) {
                switch (getUnderwritingLifestyleExercise()) {
                    case UNDERWRITING_LIFESTYLE_EXERCISE_DAILY:
                        points += 2;
                        break;
                    case UNDERWRITING_LIFESTYLE_EXERCISE_3_TIMES_WEEKLY:
                        points += 1;
                        break;
                }
            }
        }

        return points;
    }
    
    /**
     * Calls the method to assign points to the user
     * based on the underwriting questions.The Underwriting
     * Class is assigned to the user based on the points 
     * obtained
     * @return      The preferred class based on the points obtained
     */
    public int getUnderwritingClass() {
        int points = getUnderwritingClassPreferentialPoints();
        int underwritingClass = UNDERWRITING_CLASS_STANDARD;
     if((insuredIsSelf() && !getApplicantSmoker()) || (!insuredIsSelf() && !getInsuredSmoker())){
        if (points >= 1 && points <= 6) {
            underwritingClass = UNDERWRITING_CLASS_STANDARD_PLUS;
        }else
        if (points >= 7 && points <= 9 ) {
            underwritingClass = UNDERWRITING_CLASS_PREFERRED;
        }else
        if (points >= 10 ) {
            underwritingClass = UNDERWRITING_CLASS_PREFERRED_PLUS;
        }
     }else{
	    if(points >0){
		    underwritingClass = UNDERWRITING_CLASS_STANDARD_PLUS;
	    }
     }
        return underwritingClass;
    }

    public String getUnderwritingClassLabel() {
        String label = null;
        switch (getUnderwritingClass()) {
            case UNDERWRITING_CLASS_PREFERRED:
                label = "Preferred";
                break;
            case UNDERWRITING_CLASS_STANDARD:
                label = "Standard";
                break;
            case UNDERWRITING_CLASS_PREFERRED_PLUS:
                label = "Preferred Plus";
                break;
            case UNDERWRITING_CLASS_STANDARD_PLUS:
                label = "Standard Plus";
                break;
        }

        return label;
    }

    public String getReferenceNumComplexityFlag() {
        String flag = "S";

        if (getApplicantNationality().equalsIgnoreCase("Canada") || getApplicantNationality2().equalsIgnoreCase("Canada") || getApplicantNationality3().equalsIgnoreCase("Canada") ||
                getApplicantNationality().equalsIgnoreCase("Japan") || getApplicantNationality2().equalsIgnoreCase("Japan") || getApplicantNationality3().equalsIgnoreCase("Japan") ||
                getApplicantNationality().equalsIgnoreCase("Vatican City") || getApplicantNationality2().equalsIgnoreCase("Vatican City") || getApplicantNationality3().equalsIgnoreCase("Vatican City") ||
                getApplicantNationality().equalsIgnoreCase("United States of America") || getApplicantNationality2().equalsIgnoreCase("United States of America") || getApplicantNationality3().equalsIgnoreCase("United States of America") ||
                getApplicantNationality().equalsIgnoreCase("United Kingdom") || getApplicantNationality2().equalsIgnoreCase("United Kingdom") || getApplicantNationality3().equalsIgnoreCase("United Kingdom") || //JIRA 457
                (getApplicantIndustry() != null && (
                        getApplicantIndustry().equalsIgnoreCase("Casino & Gambling") ||
                                getApplicantIndustry().equalsIgnoreCase("Charity/Nonprofit organisation") ||
                                getApplicantIndustry().equalsIgnoreCase("Defence") ||
                                getApplicantIndustry().equalsIgnoreCase("Government/State-Owned bodies") ||
                                getApplicantIndustry().equalsIgnoreCase("Military product/Military production distributors") ||
                                getApplicantIndustry().equalsIgnoreCase("Money Service Business/Money Changer/Money Remittance") ||
                                getApplicantIndustry().equalsIgnoreCase("Oil & Gas/Petrolium /Oil Rigs") ||
                                getApplicantIndustry().equalsIgnoreCase("Others")
                )) ||
                getApplicantIsPep()
                ) {
            flag = "C";
        }

        if (!insuredIsSelf()) {
            if (getInsuredNationality().equalsIgnoreCase("Canada") || getInsuredNationality2().equalsIgnoreCase("Canada") || getInsuredNationality3().equalsIgnoreCase("Canada") ||
                    getInsuredNationality().equalsIgnoreCase("Japan") || getInsuredNationality2().equalsIgnoreCase("Japan") || getInsuredNationality3().equalsIgnoreCase("Japan") ||
                    getInsuredNationality().equalsIgnoreCase("Vatican City") || getInsuredNationality2().equalsIgnoreCase("Vatican City") || getInsuredNationality3().equalsIgnoreCase("Vatican City") ||
                    getInsuredNationality().equalsIgnoreCase("United States of America") || getInsuredNationality2().equalsIgnoreCase("United States of America") || getInsuredNationality3().equalsIgnoreCase("United States of America") ||
                    getInsuredNationality().equalsIgnoreCase("United Kingdom") || getInsuredNationality2().equalsIgnoreCase("United Kingdom") || getInsuredNationality3().equalsIgnoreCase("United Kingdom") || //JIRA 457
                    (getInsuredIndustry() != null && (
                            getInsuredIndustry().equalsIgnoreCase("Casino & Gambling") ||
                                    getInsuredIndustry().equalsIgnoreCase("Charity/Nonprofit organisation") ||
                                    getInsuredIndustry().equalsIgnoreCase("Defence") ||
                                    getInsuredIndustry().equalsIgnoreCase("Government/State-Owned bodies") ||
                                    getInsuredIndustry().equalsIgnoreCase("Military product/Military production distributors") ||
                                    getInsuredIndustry().equalsIgnoreCase("Money Service Business/Money Changer/Money Remittance") ||
                                    getInsuredIndustry().equalsIgnoreCase("Oil & Gas/Petrolium /Oil Rigs") ||
                                    getInsuredIndustry().equalsIgnoreCase("Others")
                    ))
                    ) {
                flag = "C";
            }
        }

        if ((getInsuredMedical1() != null && getInsuredMedical1()) ||
                (getInsuredMedical2() != null && getInsuredMedical2()) ||
                (getInsuredMedical3() != null && getInsuredMedical3()) ||
                (getInsuredMedical4() != null && getInsuredMedical4())) {
            flag = "C";
        }

        return flag;
    }

    public String getReferenceNumMailOptionFlag() {
        String flag = "E";

        if (getMailOption() == MAIL_OPTION_MAILOUT) {
            flag = "M";
        }

        return flag;
    }

    private float getUnderwritingClassFactor() {
        Map<String, float[]> factors = new HashMap<>();
        factors.put("MN", new float[]{ 0.96f, 1, 0.94f, 0.98f });
        factors.put("FN", new float[]{ 0.96f, 1, 0.94f, 0.98f });
        factors.put("MS", new float[]{ 1, 1, 1, 0.98f });
        factors.put("FS", new float[]{ 1, 1, 1, 0.98f });

        return factors.get(getGsMarker())[getUnderwritingClass()];
    }

    /**
     * Creates a string basedo the insurer's
     * gender and smoking.
     * @return String indicating gender/smoking permutation
     */
    public String getGsMarker() {
        String genderPart = null;
        String smokerPart = null;

        genderPart = (insuredIsSelf()) ? getApplicantGender().toUpperCase() : getInsuredGender().toUpperCase();
        smokerPart = (insuredIsSelf()) ? (getApplicantSmoker() ? "S" : "N") : (getInsuredSmoker() ? "S" : "N");

        if (genderPart != null && smokerPart != null) {
            return String.format("%s%s", genderPart, smokerPart);
        }

        return null;
    }

    private static Integer getIntegerFromResultSet(ResultSet rs, String columnLabel) {
        Integer integer;
        try {
            integer =  rs.getInt(columnLabel);
            if (rs.wasNull()) {
                integer = null;
            } else {
            }
        } catch (SQLException e) {
            integer = null;
        }

        return integer;
    }

    private static Float getFloatFromResultSet(ResultSet rs, String columnLabel) {
        Float num;
        try {
            num = rs.getFloat(columnLabel);
            if (rs.wasNull()) {
                num = null;
            } else {
            }
        } catch (SQLException e) {
            num = null;
        }

        return num;
    }

    public String getInsuredPossessiveLabel(boolean useName) {
        String label = null;

        if (useName) {
            label = String.format("%s's", getInsuredFirstName());
        }

        if (label == null) {
            switch (getInsuredGender()) {
                case "M":
                    label = "his";
                    break;
                case "F":
                    label = "her";
                    break;
            }
        }

        return label;
    }

    public String getInsuredPronounLabel() {
        String label = null;

        switch (getInsuredGender()) {
            case "M":
                label = "he";
                break;
            case "F":
                label = "she";
                break;
        }

        return label;
    }

    public String getUnderwritingLifestyleMortgageLabel() {
        String label = null;

        switch (getUnderwritingLifestyleMortgage()) {
            case UNDERWRITING_LIFESTYLE_MORTGAGE_NA:
                label = "Not applicable";
                break;
            case UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_12_MONTHS:
                label = "In the last 12 months";
                break;
            case UNDERWRITING_LIFESTYLE_MORTGAGE_LAST_24_MONTHS:
                label = "In the last 24 months";
                break;
            case UNDERWRITING_LIFESTYLE_MORTGAGE_MORE_THAN_24_MONTHS:
                label = "More than 24 months ago";
                break;
        }

        return label;
    }

    public String getUnderwritingLifestyleProfileLabel() {
        String label = null;

        switch (getUnderwritingLifestyleProfile()) {
            case UNDERWRITING_LIFESTYLE_PROFILE_NON_HSBC:
                label = "Non-HSBC";
                break;
            case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_JADE:
                label = "HSBC Jade or Private Banking";
                break;
            case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_PREMIER:
                label = "HSBC Premier";
                break;
            case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_ADVANCE:
                label = "HSBC Advance";
                break;
            case UNDERWRITING_LIFESTYLE_PROFILE_HSBC_OTHERS:
                label = "HSBC Others";
                break;
        }

        return label;
    }

    public String getUnderwritingLifestyleExerciseLabel() {
        String label = null;

        switch (getUnderwritingLifestyleExercise()) {
            case UNDERWRITING_LIFESTYLE_EXERCISE_NA:
                label = "Not applicable";
                break;
            case UNDERWRITING_LIFESTYLE_EXERCISE_DAILY:
                label = "Daily (for at least 30 minutes each time)";
                break;
            case UNDERWRITING_LIFESTYLE_EXERCISE_3_TIMES_WEEKLY:
                label = "3 times a week";
                break;
            case UNDERWRITING_LIFESTYLE_EXERCISE_WEEKEND:
                label = "Every weekend";
                break;
        }

        return label;
    }

    public String getCreditCardTypeLabel() {
        String label = "";

        switch (getCreditCardType()) {
            case "V":
                label = "VISA";
                break;
            case "M":
                label = "MasterCard";
                break;
        }

        return label;
    }

    public String getApplicantTaxCountryNoTinReasonLabel(String reasonType) {
        String label = null;

        switch (reasonType) {
            case "A":
                label = "A: The country where you are liable to pay tax does not issue TINs to its residents";
                break;
            case "B":
                label = "B: You are otherwise unable to obtain a TIN or equivalent number.";
                break;
            case "C":
                label = "C: No TIN is required.";
                break;
        }

        return label;
    }

    public String getApplicantGenderLabel() {
        String label = null;

        switch (getApplicantGender()) {
            case "F":
                label = "Female";
                break;
            case "M":
                label = "Male";
                break;
        }

        return label;
    }

    public String getInsuredGenderLabel() {
        String label = null;

        switch (getInsuredGender()) {
            case "F":
                label = "Female";
                break;
            case "M":
                label = "Male";
                break;
        }

        return label;
    }

    public float getMaxSumAssured() {
        float max = 0f;
        String employmentType = insuredIsSelf() ? getApplicantEmployment() : getInsuredEmployment();

        switch (employmentType) {
            case "Homemaker":
            case "Retired":
            case "Unemployed":
            case "Student":
                max = 500000f;
                break;
            default:
                max = 1000000;
                break;
        }


        return max;
    }

    public float getMaxRiderSumAssured() {
        float max = 0f;
        String employmentType = insuredIsSelf() ? getApplicantEmployment() : getInsuredEmployment();
        int anb = AgeUtils.calculateAgeNextBirthday(insuredIsSelf() ? getApplicantDob() : getInsuredDob());

        switch (employmentType) {
            case "Homemaker":
            case "Retired":
            case "Unemployed":
            case "Student":
                max = 400000f;
                break;
            default:
                if (anb >= 19 && anb <=49) {
                    max = 650000;
                } else {
                    max = 500000;
                }
                break;
        }

        return max;
    }

    public boolean isEligiblePreferentialRates() {
        boolean eligible = false;

        if (getInsuredMedical1() != null && !getInsuredMedical1() &&
                getInsuredMedical2() != null && !getInsuredMedical2() &&
                getInsuredMedical3() != null && !getInsuredMedical3()) {
            if (getIncludeCriticalIllnessRider()!=null &&getIncludeCriticalIllnessRider()) {
                if (getInsuredMedical4() != null && !getInsuredMedical4()) {
                    eligible = true;
                }
            } else {
                eligible = true;
            }
        }

        return eligible;
    }

    public boolean isNationalitySpecialCase() {
        boolean isSpecialCase = false;

        if (getApplicantNationality().equalsIgnoreCase("Canada") || getApplicantNationality2().equalsIgnoreCase("Canada") || getApplicantNationality3().equalsIgnoreCase("Canada") ||
                getApplicantNationality().equalsIgnoreCase("Japan") || getApplicantNationality2().equalsIgnoreCase("Japan") || getApplicantNationality3().equalsIgnoreCase("Japan") ||
                getApplicantNationality().equalsIgnoreCase("United States of America") || getApplicantNationality2().equalsIgnoreCase("United States of America") || getApplicantNationality3().equalsIgnoreCase("United States of America") ||
                getApplicantNationality().equalsIgnoreCase("Vatican City ") || getApplicantNationality2().equalsIgnoreCase("Vatican City ") || getApplicantNationality3().equalsIgnoreCase("Vatican City ")) {
            isSpecialCase = true;
        }

        if (!insuredIsSelf()) {
            if (getInsuredNationality().equalsIgnoreCase("Canada") || getInsuredNationality2().equalsIgnoreCase("Canada") || getInsuredNationality3().equalsIgnoreCase("Canada") ||
                    getInsuredNationality().equalsIgnoreCase("Japan") || getInsuredNationality2().equalsIgnoreCase("Japan") || getInsuredNationality3().equalsIgnoreCase("Japan") ||
                    getInsuredNationality().equalsIgnoreCase("United States of America") || getInsuredNationality2().equalsIgnoreCase("United States of America") || getInsuredNationality3().equalsIgnoreCase("United States of America") ||
                    getInsuredNationality().equalsIgnoreCase("Vatican City ") || getInsuredNationality2().equalsIgnoreCase("Vatican City ") || getInsuredNationality3().equalsIgnoreCase("Vatican City ")) {
                isSpecialCase = true;
            }
        }

        return isSpecialCase;
    }

    public boolean isIndustrySpecialCase() {
        boolean isSpecialCase = false;
        try{
            if (getApplicantIndustry().equalsIgnoreCase("Casino & Gambling") ||
                    getApplicantIndustry().equalsIgnoreCase("Charity/Nonprofit organisation") ||
                    getApplicantIndustry().equalsIgnoreCase("Defence") ||
                    getApplicantIndustry().equalsIgnoreCase("Government/State-Owned bodies") ||
                    getApplicantIndustry().equalsIgnoreCase("Military product/Military production distributors") ||
                    getApplicantIndustry().equalsIgnoreCase("Money Service Business/Money Changer/Money Remittance") ||
                    getApplicantIndustry().equalsIgnoreCase("Oil & Gas/Petrolium /Oil Rigs")) {
                isSpecialCase = true;
            }

            if (!insuredIsSelf()) {
                if (getInsuredIndustry().equalsIgnoreCase("Casino & Gambling") ||
                        getInsuredIndustry().equalsIgnoreCase("Charity/Nonprofit organisation") ||
                        getInsuredIndustry().equalsIgnoreCase("Defence") ||
                        getInsuredIndustry().equalsIgnoreCase("Government/State-Owned bodies") ||
                        getInsuredIndustry().equalsIgnoreCase("Military product/Military production distributors") ||
                        getInsuredIndustry().equalsIgnoreCase("Money Service Business/Money Changer/Money Remittance") ||
                        getInsuredIndustry().equalsIgnoreCase("Oil & Gas/Petrolium /Oil Rigs")) {
                    isSpecialCase = true;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return isSpecialCase;
    }
    
    /**
     * Getter method to return the email sent date
     * @return email sent date
     */
    public Date getEmailSentDate() {
		return emailSentDate;
	}

    /**
     * Setter method to set the email sent date
     * @param emailSentDate date which email was sent
     */
      public void setEmailSentDate(Date emailSentDate) {
        this.emailSentDate = emailSentDate;
      }
      
    /**
     * Getter method to return the email sent date
     * This is to indicate whether feedback has been done.
     * @return true if user has done the feedback, else false
     */
    public boolean isFeedbackCompleted() {
        boolean isFeedback = false;

        getFeedback();
        
        if (getFeedbackExperience() != null && getFeedbackEase() != null && getFeedbackRecommend() != null && 
        		StringUtils.isNotBlank(getFeedbackImprove()) && StringUtils.isNotBlank(getFeedbackAdditional())) {
        	isFeedback = true;
        }

        return isFeedback;
    }
    
    /**
     * This function will retrieve the feedback data from db and populate the feedback portion of the application object
     * @return void
     */
    private void getFeedback() {
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM feedback WHERE application_id = ?");
            stmt.setString(1, getId());
            rs = stmt.executeQuery();
            if (rs.next()) {
            	setFeedbackExperience(rs.getInt("feedback_experience"));
            	setFeedbackEase(rs.getInt("feedback_ease"));
            	setFeedbackRecommend(getBooleanFromResultSet(rs, "feedback_recommend"));
            	setFeedbackNoRecommendReason(rs.getString("feedback_norecommend_reason"));
            	setFeedbackImprove(rs.getString("feedback_improve"));
            	setFeedbackAdditional(rs.getString("feedback_additional"));
            }
        } catch (Exception e) {
             logger.error(e.getMessage());
        }finally {           
			try {
				if (rs != null) {
	                rs.close();
	                rs = null;
	            }
	            if (stmt != null) {
	                stmt.close();
	                stmt = null;
	            }
	            if (conn != null) {
	                conn.close();
	                conn = null;
	            }
			} catch (Exception e) {
				logger.error("getFeedBack:",e);
			}         
        }
    }


    /**
     * This is flag to indicate whether ROP will be enabled
     * Flag is in app.properties file
     * @return true/false
     */
    public boolean enableROP() {
        String ropFlag = Config.getString("dreamcatcher.rop.enabled");
        if ("true".equalsIgnoreCase(ropFlag))
            return true;
        return false;
    }

	/**
	 * Setting the value for marketing consent
	 * 
	 * @param b True if user has opted for news feeds
	 */
	public void setMarketingConsent(boolean b) {
		this.marketingConsent=b;
		
	}
	/**
	 * @return Boolean the marketing consent opted 
	 */
	public Boolean getMarketingConsent(){
		return this.marketingConsent;
	}

}
