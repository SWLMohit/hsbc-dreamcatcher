package sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AgeUtils {
    private static Logger logger = LoggerFactory.getLogger(AgeUtils.class);
    public static int calculateAgeNextBirthday(Date dob) {
        logger.info("the DOB is {}", dob);
        int age = 0;
        if (dob == null) return age;

        Calendar dobCal = Calendar.getInstance(Locale.US);
        dobCal.setTime(dob);

        Date now = new Date();
        Calendar nowCal = Calendar.getInstance(Locale.US);
        nowCal.setTime(now);
        nowCal.set(Calendar.HOUR_OF_DAY, 0);
        nowCal.set(Calendar.MINUTE, 0);
        nowCal.set(Calendar.SECOND, 0);
        nowCal.set(Calendar.MILLISECOND, 0);
        now = nowCal.getTime();

        Date dobThisYear = null;
        String dateString = String.format("%s/%s/%s", dobCal.get(Calendar.DAY_OF_MONTH), dobCal.get(Calendar.MONTH) + 1, nowCal.get(Calendar.YEAR));
        DateFormat df = new SimpleDateFormat("d/M/y");
        try {
            dobThisYear = df.parse(dateString);
        } catch (Exception e) {
            logger.error("convert DOB error: {}",e.getMessage());
        }

        if (now.after(dobThisYear)) {
            age = nowCal.get(Calendar.YEAR) - dobCal.get(Calendar.YEAR) + 1;
        } else {
            age = nowCal.get(Calendar.YEAR) - dobCal.get(Calendar.YEAR);
        }
        logger.info("the age is {}", age);
        return age;
    }
}
