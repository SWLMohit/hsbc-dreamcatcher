package sg.com.hsbc.dreamcatcher.admin.api.dvt;

import org.slf4j.LoggerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/retrieve/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/retrieve.jsp")
})
public class DVTRetrieveAPI extends HttpServlet {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(DVTRetrieveAPI.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("direct-value-term retrieve page");
        render(request, response);
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }
}
