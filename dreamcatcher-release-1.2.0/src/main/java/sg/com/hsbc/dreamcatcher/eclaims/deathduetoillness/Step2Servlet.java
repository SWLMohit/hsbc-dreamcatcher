package sg.com.hsbc.dreamcatcher.eclaims.deathduetoillness;

import sg.com.hsbc.dreamcatcher.eclaims.DoctorPortion;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/2-death-due-to-illness/2", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/2_death_due_to_illness/step2.jsp")
})
@MultipartConfig
public class Step2Servlet extends BaseServlet {
    public float getCurrentStep() {
        return (float)2;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizeDeceasedInformation(request, sanitized);
        sanitized = sanitizeDoctor(request, sanitized);
        sanitized = sanitizeYNQuestions(request, sanitized);

        return sanitized;
    }

    private Map<String, Object> sanitizeDeceasedInformation(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("deceased_symptoms_present_date_day");
        sanitized.put("deceased_symptoms_present_date_day", input);
        
        input = request.getParameter("deceased_symptoms");
        sanitized.put("deceased_symptoms", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeDoctor(HttpServletRequest request, Map<String, Object> sanitized) {
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        String[] fromDate = request.getParameterValues("doctor_from_date");
        String[] toDate = request.getParameterValues("doctor_to_date");

        String[] doctorNames = request.getParameterValues("doctor_name");
        String[] doctorHospitals = request.getParameterValues("doctor_hospital");
        String[] doctorHospitalStates = request.getParameterValues("doctor_hospital_state");
        String[] doctorHospitalStreet1s = request.getParameterValues("doctor_hospital_street1");
        String[] doctorHospitalStreet2s = request.getParameterValues("doctor_hospital_street2");

        for (int i = 0; i < doctorNames.length; i++) {
            Map<String, Object> subData = new HashMap<>();

          if(StringUtils.isNotBlank(fromDate[i])){
            subData.put("doctor_from_date", fromDate[i]);
          }
          if(StringUtils.isNotBlank(fromDate[i])){
            subData.put("doctor_to_date", toDate[i]);
          }

           if(StringUtils.isNotBlank(doctorNames[i])){
            subData.put("doctor_name", doctorNames[i]);
           }
           if(StringUtils.isNotBlank(doctorHospitals[i])){
            subData.put("doctor_hospital", doctorHospitals[i]);
           }
           if(StringUtils.isNotBlank(doctorHospitalStates[i])){
            subData.put("doctor_hospital_state", doctorHospitalStates[i]);
           }
           if(StringUtils.isNotBlank(doctorHospitalStreet1s[i])){
            subData.put("doctor_hospital_street1", doctorHospitalStreet1s[i]);
           }
           if(StringUtils.isNotBlank(doctorHospitalStreet2s[i])){
            subData.put("doctor_hospital_street2", doctorHospitalStreet2s[i]);
           }

            list.add(subData);
        }

        sanitized.put("doctor_list", list.toArray());

        return sanitized;
    }

    private Map<String, Object> sanitizeYNQuestions(HttpServletRequest request, Map<String, Object> sanitized) {

        handleUploadFile(request, sanitized, "death_certificate", "DeathCertificate");

        String input = request.getParameter("left_a_will");
        sanitized.put("left_a_will", Boolean.parseBoolean(input));
        if (Boolean.parseBoolean(input)) {
            handleUploadFile(request, sanitized, "left_a_will_document", "LeftAWillDocument");
        }
        input = request.getParameter("occur_in_singapore");
        sanitized.put("occur_in_singapore", Boolean.parseBoolean(input));
        if (!Boolean.parseBoolean(input)) {
            handleUploadFile(request, sanitized, "occur_in_singapore_document", "OccurInSingaporeDocument");
        }

        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        DeathDueToIllnessApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        try {
            Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s", data.get("deceased_symptoms_present_date_day")));
            application.setDeceasedSymptomsPresentDate(date);
        } catch (ParseException e) {
            ErrorHandler.handleError(application, e);
            return false;
        }
        application.setDeceasedSymptoms(data.get("deceased_symptoms").toString());

        ArrayList<DoctorPortion> list = new ArrayList<>();
        DoctorPortion[] oldList = application.getDoctorList();
        int i = 0;
        for (Object subDataItem : (Object[]) data.get("doctor_list")) {
            DoctorPortion portion;
            if (i < oldList.length) {
                portion = oldList[i];
            } else {
                portion = new DoctorPortion();
            }
            Map<String, Object> subData = (Map<String, Object>) subDataItem;

            try {

              if(subData.containsKey("doctor_from_date")){
                Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s/%s", "01", subData.get("doctor_from_date")));
                portion.setFromDate(date);
               }
            } catch (ParseException e) {
                ErrorHandler.handleError(application, e);
                return false;
            }
            try {
            if(subData.containsKey("doctor_to_date")){
                Date date = (new SimpleDateFormat("dd/MM/y")).parse(String.format("%s/%s", "01", subData.get("doctor_to_date")));
                portion.setToDate(date);
              }
            } catch (ParseException e) {
                ErrorHandler.handleError(application, e);
                return false;
            }
            if(subData.containsKey("doctor_name")){
              portion.setName(subData.get("doctor_name").toString());
            }
            if(subData.containsKey("doctor_hospital")){
             portion.setHospital(subData.get("doctor_hospital").toString());
            }
            if(subData.containsKey("doctor_hospital_state")){
             portion.setHospitalCity(subData.get("doctor_hospital_state").toString());
            }
            if(subData.containsKey("doctor_hospital_street1")){
             portion.setHospitalStreet1(subData.get("doctor_hospital_street1").toString());
            }
            if(subData.containsKey("doctor_hospital_street2")){
            portion.setHospitalStreet2(subData.get("doctor_hospital_street2").toString());
            }

            list.add(portion);
            i++;
        }
        application.setDoctorList(list.toArray(new DoctorPortion[list.size()]));

        if (!data.get("death_certificate").toString().equals("")) {
            application.setDeathCertificate(data.get("death_certificate").toString());
        }
        application.setLeftAWill((boolean)data.get("left_a_will"));
        if (application.getLeftAWill()) {
            if (!data.get("left_a_will").toString().equals("")) {
                application.setLeftAWillDocument(data.get("left_a_will_document").toString());
            }
        } else {
            application.setLeftAWillDocument(null);
        }
        application.setOccurInSingapore((boolean)data.get("occur_in_singapore"));
        if (!application.getOccurInSingapore()) {
            if (!data.get("occur_in_singapore_document").toString().equals("")) {
                application.setOccurInSingaporeDocument(data.get("occur_in_singapore_document").toString());
            }
        } else {
            application.setOccurInSingaporeDocument(null);
        }

        /** 10MB File size implementation starts here **/ 
        if(application.getFileInfo()!=null){
          Map<String, Long>updatedFileInfo=application.getFileInfo();
          if(data.get("fileInfo")!=null) {
          updatedFileInfo.putAll((Map<String,Long>)data.get("fileInfo"));
          application.setFileInfo(updatedFileInfo);
          }
        }
        else {
          application.setFileInfo((Map<String,Long>)data.get("fileInfo")); 
        }
        /** 10MB File size implementation ends here **/ 
        application.setMaxStep(getCurrentStep() + this.getStep());
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DeathDueToIllnessApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        Date d = application.getDeceasedSymptomsPresentDate();
        if (d != null) {
            data.put("deceased_symptoms_present_date_day", (new SimpleDateFormat("dd/MM/y")).format(d));
        }
        data.put("deceased_symptoms", application.getDeceasedSymptoms());

        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (DoctorPortion portion : application.getDoctorList()) {
            Map<String, Object> subData = new HashMap<>();

            Date date = portion.getFromDate();
            if (date != null) {
                subData.put("doctor_from_date", (new SimpleDateFormat("MM/Y")).format(date));
            }
            date = portion.getToDate();
            if (date != null) {
                subData.put("doctor_to_date", (new SimpleDateFormat("MM/y")).format(date));
            }

            subData.put("doctor_name", portion.getName());
            subData.put("doctor_hospital", portion.getHospital());
            subData.put("doctor_hospital_state", portion.getHospitalCity());
            subData.put("doctor_hospital_street1", portion.getHospitalStreet1());
            subData.put("doctor_hospital_street2", portion.getHospitalStreet2());

            list.add(subData);
        }
        data.put("doctor_list", list.toArray());

        data.put("death_certificate", application.getDeathCertificate());
        data.put("left_a_will", application.getLeftAWill());
        if (application.getLeftAWill() != null && application.getLeftAWill()) {
            data.put("left_a_will_document", application.getLeftAWillDocument());
        }
        data.put("occur_in_singapore", application.getOccurInSingapore());
        if (application.getOccurInSingapore() != null && !application.getOccurInSingapore()) {
            data.put("occur_in_singapore_document", application.getOccurInSingaporeDocument());
        }

        request.setAttribute("data", data);
    }

    public void populateFormData(HttpServletRequest request) {
        Map<String, Object> data = (HashMap)request.getAttribute("formData");

        data.put("country_list", this.getCountryList());
        data.put("day_list", this.getDayList());
        data.put("month_list", this.getMonthList());
        data.put("year_list", this.getYearList());

        request.setAttribute("formData", data);
    }
}
