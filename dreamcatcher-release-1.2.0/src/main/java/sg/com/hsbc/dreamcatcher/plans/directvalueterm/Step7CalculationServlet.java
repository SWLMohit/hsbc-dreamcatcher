package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import sg.com.hsbc.dreamcatcher.helpers.Database;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.plans.Constants;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.ProductCalculation;

/**
 * Step7CalculationServlet is a servlet which allows
 * the application to fetch the fresh discount values 
 * from the database 
 * based on the sum insured inputs passed to the post 
 * method of the class via a ajax request.
 * @author hchellani
 *
 */
@WebServlet(urlPatterns = "/our-plans/direct-value-term/calculation")
public class Step7CalculationServlet extends HttpServlet {
	 private final static Logger logger = Logger.getLogger(Step7CalculationServlet.class);
	/* 
	 * The method creates the object of DirectValueTermApplication based on the applicationid saved in the session
	 * It sends the map which contains the discount values to be used by the frontend  
	 * 
	 * @param request request object received by the servlet
	 * @param response response object sent  to the ajax request submitted
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 try{
		String id = request.getSession().getAttribute(BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME).toString();
		DirectValueTermApplication application = DirectValueTermApplication.getById(id);
		float sumAssuredFloat=200000.0f; 
		
		String sumAssured=request.getParameter("sum_assured");
		if(StringUtils.isNotBlank(sumAssured)){
			sumAssuredFloat=Float.parseFloat(sumAssured);
		}else{
			logger.info("the sum assured is blank");
		}
		
		
		String gsMarker=ProductCalculation.getGsMarker(application.getApplicantGender(), application.getApplicantSmoker());
		Map premiums=getDiscountRates(gsMarker,sumAssuredFloat);
		JSONObject n = new JSONObject(premiums);
		logger.info("the premiums returned is "+premiums);
		response.setContentType("application/json");
		response.getWriter().println(n);
		
	 }catch(Exception e){
		logger.info("error i calculating discounts"+e);
	 }
		
	}
	
	/**
	 * Fetches the discount rates from the database
	 * 
	 * @param gsMarker     value based on the gender/smoker
	 * @param sumAssured    value based on the ajax request
	 * @return Map          discount rates from the database                 
	 */
	private Map<String, Float> getDiscountRates(String gsMarker,float sumAssured){
		  Database db = new Database();
		  Connection conn = db.getConnection();
		  PreparedStatement stmt = null;
		  ResultSet rs = null;
		  Map<String,Float> discountRates=new HashMap(); 
		try{
		  
		  float discountRate=0.0f;
		  float discountRateUnitAmount=0.0f;
		  String productCode="";
		  float actualDiscount=0.0f;
		  
            conn = db.getConnection();
            /*
             * Reads all the discount rates for the sum assured specified
             * for the specified gender and smoker combination gs_marker
             * Discount rates for all term durations 5,20,65 are picked up
             */
            stmt = conn.prepareStatement("SELECT * FROM discounts WHERE  gs_marker = ? AND sa_min <= ? AND sa_max >= ?");
            
            stmt.setString(1, gsMarker.toUpperCase());
            stmt.setFloat(2, sumAssured);
            stmt.setFloat(3, sumAssured);
            rs = stmt.executeQuery();
            while (rs.next()) {
            	 productCode=rs.getString("product_code");
            	 discountRate = rs.getFloat("discount");
                 discountRateUnitAmount = rs.getFloat("unit_amount");
                 actualDiscount=discountRate/discountRateUnitAmount;
            	if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_5_YEARS_PRODUCT_CODE)){
            		discountRates.put("discountRate5",discountRate );
            		discountRates.put("discountRateUnitAmount5", discountRateUnitAmount);	
            		discountRates.put("actualDiscount5", actualDiscount);
            	}else if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_20_YEARS_PRODUCT_CODE)){
            		discountRates.put("discountRate20",discountRate );
            		discountRates.put("discountRateUnitAmount20", discountRateUnitAmount);
            		discountRates.put("actualDiscount20", actualDiscount);
            	}else if(StringUtils.equalsIgnoreCase(productCode, Constants.DVT_AGE_65_PRODUCT_CODE)){
            		discountRates.put("discountRate65",discountRate );
            		discountRates.put("discountRateUnitAmount65", discountRateUnitAmount);
            		discountRates.put("actualDiscount65", actualDiscount);
            	}
               

            }
            if(!discountRates.containsKey("discountRate5")){
            	discountRates.put("discountRate5",discountRate );
        		discountRates.put("discountRateUnitAmount5", discountRateUnitAmount);
            }
            if(!discountRates.containsKey("discountRate65")){
            	discountRates.put("discountRate65",discountRate );
        		discountRates.put("discountRateUnitAmount65", discountRateUnitAmount);
            }
            if(!discountRates.containsKey("discountRate20")){
            	discountRates.put("discountRate20",discountRate );
        		discountRates.put("discountRateUnitAmount20", discountRateUnitAmount);
            }
            
        } catch (Exception e) {
            ErrorHandler.handleError(ProductCalculation.class, e);
        } finally {
            db.closeAll(conn,stmt, rs);
        }
		return discountRates;
	}
	
}
