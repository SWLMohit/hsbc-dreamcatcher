package sg.com.hsbc.dreamcatcher.eclaims.deathduetoaccident;

import sg.com.hsbc.dreamcatcher.eclaims.EclaimStep4aProcessor;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/1-death-due-to-accident/4", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/1_death_due_to_accident/step4a.jsp")
})
public class Step4aServlet extends BaseServlet {
    private EclaimStep4aProcessor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new EclaimStep4aProcessor(this);
    }

    public float getCurrentStep() {
        return (float)4;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        return processor.process(request);
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        processor.handleSuccess(request, response);
    }
}
