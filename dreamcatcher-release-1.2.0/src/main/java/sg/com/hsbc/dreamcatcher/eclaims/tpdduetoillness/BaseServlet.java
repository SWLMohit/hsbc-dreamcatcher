package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoillness;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimBaseServlet;
import sg.com.hsbc.dreamcatcher.eclaims.unemployment.UnemploymentApplication;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;

public abstract class BaseServlet extends EClaimBaseServlet {
    static String FORM_TITLE = "eClaims - TPD Due to Illness - Make a Claim";
    public static String APPLICATION_SESSION_ATTRIBUTE_NAME = "EClaimTPDDueToIllness";

    public String getFormTitle() {
        return BaseServlet.FORM_TITLE;
    }

    public String getApplicationSessionAttributeName() {
        return BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME;
    }

    public String getBaseFormUrl() { return "/eclaims/5-tpd-due-to-illness"; }

    public ApplicationForm getById(String id) throws SQLException { return TpdDueToIllnessApplication.getById(id); }

    public TpdDueToIllnessApplication getApplication(HttpServletRequest request) {
        return (TpdDueToIllnessApplication)super.getApplication(request);
    }
}
