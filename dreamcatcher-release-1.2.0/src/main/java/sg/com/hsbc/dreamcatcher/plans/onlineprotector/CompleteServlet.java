package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.commons.lang.StringUtils;

import sg.com.hsbc.dreamcatcher.helpers.Config;
import sg.com.hsbc.dreamcatcher.helpers.EmailSender;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet(urlPatterns = "/our-plans/online-protector/complete", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/complete.jsp")
})
public class CompleteServlet extends BaseServlet {

	public float getCurrentStep() {
        return 15;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        sanitized = sanitizeMailOption(request, sanitized);
        sanitized.put("op-rate-exp", request.getParameter("op-rate-exp"));
        sanitized.put("op-rate-ease", request.getParameter("op-rate-ease"));
        sanitized.put("feedback_recommend", request.getParameter("feedback_recommend"));
        sanitized.put("feedback_norecommend_reason", request.getParameter("feedback_norecommend_reason"));
        sanitized.put("feedback_improve", request.getParameter("feedback_improve"));
        sanitized.put("feedback_additional", request.getParameter("feedback_additional"));

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        if (data.get("mail_option") != null) {
            application.setMailOption(Integer.parseInt(data.get("mail_option").toString()));
        }

        if (data.get("op-rate-exp") != null && !data.get("op-rate-exp").toString().isEmpty()) {
            application.setFeedbackExperience(Integer.parseInt(data.get("op-rate-exp").toString()));
        }

        if (data.get("op-rate-ease") != null && !data.get("op-rate-ease").toString().isEmpty()) {
            application.setFeedbackEase(Integer.parseInt(data.get("op-rate-ease").toString()));
        }

        if (data.get("feedback_recommend") != null && !data.get("feedback_recommend").toString().isEmpty()) {
            application.setFeedbackRecommend(data.get("feedback_recommend").toString().equalsIgnoreCase("Y"));
        }

        if (data.get("feedback_norecommend_reason") != null && !data.get("feedback_norecommend_reason").toString().isEmpty()) {
            application.setFeedbackNoRecommendReason(data.get("feedback_norecommend_reason").toString());
        }

        if (data.get("feedback_improve") != null && !data.get("feedback_improve").toString().isEmpty()) {
            application.setFeedbackImprove(data.get("feedback_improve").toString());
        }

        if (data.get("feedback_additional") != null && !data.get("feedback_additional").toString().isEmpty()) {
            application.setFeedbackAdditional(data.get("feedback_additional").toString());
        }

        application.setMaxStep(getCurrentStep() + 1);
        // Save the last step to application table
        application.save();
        application.saveFeedback();
        EmailSender.sendEmailUsingApplication(application,"OP", this);

        return true;
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("/our-plans/online-protector/feedback");
        return;
    }

    private Map<String, Object> sanitizeMailOption(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("mail_option");

        sanitized.put("mail_option", input);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("mail_option", application.getMailOption());

        request.setAttribute("data", data);
    }

}
