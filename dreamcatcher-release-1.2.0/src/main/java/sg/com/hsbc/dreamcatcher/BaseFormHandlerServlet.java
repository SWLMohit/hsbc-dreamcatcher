package sg.com.hsbc.dreamcatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;
import java.util.Map;
import sg.com.hsbc.dreamcatcher.helpers.*;

public abstract class BaseFormHandlerServlet extends BaseServlet {

    public abstract void init(HttpServletRequest request);
    public abstract void populateDataFromObject(HttpServletRequest request);
    public abstract Map<String, Object> sanitize(HttpServletRequest request);
    public abstract boolean validate(HttpServletRequest request);
    public abstract boolean process(HttpServletRequest request);
    public abstract float getCurrentStep();
    public float getNextStep() {
        // TODO: was originally abstract, but this will break onlineprotector flow - confirm with other team first

        // for now, provide a logical default as calculated ad-hoc elsewhere
        return ((int)getCurrentStep()) + 1;
    }
    public abstract float getMaxStep(HttpServletRequest request);
    public abstract ApplicationForm getById(String id) throws SQLException;
    public abstract String getBaseFormUrl();
    public abstract String getApplicationSessionAttributeName();
    public abstract String getFormTitle();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        init(request);
        if (!preverify(request)) {
            handlePreverifyFail(request, response);
            return;
        }
        populateFormData(request);
        populateDataFromObject(request);
        request.setAttribute("currentStep", getCurrentStep());
        render(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        init(request);
        if (!preverify(request)) {
            handlePreverifyFail(request, response);
            return;
        }
        if (validate(request)) {
            if (process(request)) {
                handleSuccess(request, response);
                return;
            }
        }
        request.setAttribute("currentStep", getCurrentStep());
        render(request, response);
    }

    public boolean preverify(HttpServletRequest request) {
        return ((getMaxStep(request) == 0 && getCurrentStep() <= 1) || (getCurrentStep() <= getMaxStep(request)));
    }

    public void handlePreverifyFail(HttpServletRequest request, HttpServletResponse response) throws IOException {
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
    }

    public void populateFormData(HttpServletRequest request) {
    }
}
