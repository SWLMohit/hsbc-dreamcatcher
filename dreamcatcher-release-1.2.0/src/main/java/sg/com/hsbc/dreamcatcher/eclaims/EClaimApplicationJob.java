package sg.com.hsbc.dreamcatcher.eclaims;

import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import java.io.IOException;
import javax.mail.MessagingException;
import java.util.logging.Logger;

/**
 * A job to generate the PDFs for an application and send them to the
 * recipients.
 *
 * Rather than firing off this in a separate thread, it would be better
 * to invoke it as an async job via AJAX, so that the user can be
 * informed about the progress of the PDF generation and the rest of the
 * process, and whether it was successful or not. Here is an example:
 * https://stackoverflow.com/questions/10587771/call-asynchronous-servlet-from-ajax
 * If so it would be better to use XML messages rather than JSON as in
 * this one, for security reasons (temptation of the JavaScript
 * developer to eval() the response).
 */

public class EClaimApplicationJob implements Runnable {

    private final EClaimApplication application;

    public EClaimApplicationJob(EClaimApplication application) {
        this.application = application;
    }

    public void run() {
        Logger logger = Logger.getLogger(getClass().getName());
        try {
            // Generate the PDF
            application.generatePDF();
            try {
                // Send the email to admin, and log that we have done it
                application.sendEmail();
                logger.info("Sent email to admin for application " + application.getId());
                try {
                    // Send the email and SMS to the user, and log that we
                    // have done it
                    application.sendSmsEmailPassword();
                    logger.info("Sent email/SMS to user for application " + application.getId());
                } catch (MessagingException e3) {
                    ErrorHandler.handleError(application, e3, "Failed to send email/SMS to user");
                }
            } catch (MessagingException e2) {
                ErrorHandler.handleError(application, e2, "Failed to send EClaims admin email");
            }
        } catch (IOException e) {
            ErrorHandler.handleError(application, e, "Failed to generate PDF");
        }
    }

}
