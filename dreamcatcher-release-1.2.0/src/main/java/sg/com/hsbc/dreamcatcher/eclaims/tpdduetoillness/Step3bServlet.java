package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoillness;

import sg.com.hsbc.dreamcatcher.eclaims.EclaimStep4bProcessor;
import sg.com.hsbc.dreamcatcher.helpers.EmailSender;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

@WebServlet(urlPatterns = "/eclaims/5-tpd-due-to-illness/3a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/5_tpd_due_to_illness/step3b.jsp")
})
@MultipartConfig
public class Step3bServlet extends BaseServlet {
    private EclaimStep4bProcessor processor;

    public void init(HttpServletRequest request) {
        super.init(request);
        this.processor = new EclaimStep4bProcessor(this);
    }

    public float getCurrentStep() {
        return (float)3.5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        return processor.sanitize(request);
    }

    public boolean process(HttpServletRequest request) {
        boolean result = false;
        try {
            result = processor.process(request);
        }
        catch(Exception e) {
            processor.populateFormData(request);
        }
        return result;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        processor.populateDataFromObject(request);
    }

    public void populateFormData(HttpServletRequest request) {
        processor.populateFormData(request);
    }

    protected float getStep() {
        return (float)0.5;
    }
}