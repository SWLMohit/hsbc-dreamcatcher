package sg.com.hsbc.dreamcatcher.admin.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.com.hsbc.dreamcatcher.admin.AdminUtil;
import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.repository.ApplicationsRepository;
import sg.com.hsbc.dreamcatcher.admin.service.AdminService;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.DirectValueTermApplication;
import sg.com.hsbc.dreamcatcher.plans.onlineprotector.OnlineProtectorApplication;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public class AdminDVTServiceImpl implements AdminService {
    ApplicationsRepository applicationsRepository;
    private static Logger logger = LoggerFactory.getLogger(AdminDVTServiceImpl.class);
    @Override
    public void resendEmail(Applications applications) {
        logger.info("send dvt email");
        DirectValueTermApplication application = DirectValueTermApplication.getById(applications.getId());
        logger.info("resend email successful");
        applications.setId(application.getId());
        applications.setApplicantEmail(application.getApplicantEmail());
        applications.setApplicantMobileCountryCode(application.getApplicantMobileCountryCode());
        applications.setApplicantMobileNum(application.getApplicantMobileNum());
        try {
            application.sendNotifications(false);
        } catch (IOException ioe) {
            logger.error("send Notification error:" + ioe);
        } catch (MessagingException me) {
            logger.error("send Notification error:" + me);
        } catch (Exception e) {
            logger.error("send Notification error:" + e);
        }
    }

    @Override
    public void resendSMS(Applications applications) {
        DirectValueTermApplication application = DirectValueTermApplication.getById(applications.getId());
        logger.info("resend sms for application id: {}", applications.getId());
        String content = "Thank you for your application. Please allow 7 working days to process the first premium payment and complete internal checks.";

        if (application.getReferenceNumComplexityFlag().equalsIgnoreCase("C")) {
            content = "Thank you for your application. We will be contacting you shortly on the requirements to process your application.";
        }
        content += " Your PDF password is: %s.";
        String code = application.getPDFPassword(DirectValueTermApplication.PDF_TYPE_BENEFIT_ILLUSTRATION);
        applications.setId(application.getId());
        applications.setApplicantMobileCountryCode(application.getApplicantMobileCountryCode());
        applications.setApplicantMobileNum(application.getApplicantMobileNum());
        applications.setApplicantEmail(application.getApplicantEmail());
        AdminUtil.sendSMSService(applications, content, code);
        logger.info("resend sms successful");
    }

    @Override
    public void resendVerification(Applications applications) {
        logger.info("resend verification");
        String content = "Your verification code is: %s. Please use this code to retrieve your application within the next 30 days.";
        applications.setProductType(FieldConstants.DVT_PRODUCT_TYPE);
        AdminUtil.sendSMSService(applications, content, applications.getDropoutPassword());
    }

    @Override
    public List<Applications> listAllPolicy(Applications applications) {
        applicationsRepository = new ApplicationsRepository();
        return applicationsRepository.findByMobileNoAndEmail(FieldConstants.DVT_PRODUCT_TYPE,
                1, applications.getApplicantMobileCountryCode(),
                applications.getApplicantMobileNum(), applications.getApplicantEmail());
    }

    @Override
    public String retrieveVerificationCode(Applications applications) {
        logger.info("retrieve verification code");
        applications.setProductType(FieldConstants.DVT_PRODUCT_TYPE);
        applicationsRepository = new ApplicationsRepository();
        return applicationsRepository.findByVerificationCode(applications);
    }

    @Override
    public boolean isImcompleteEmailExisted(String email) {
        applicationsRepository = new ApplicationsRepository();
        return applicationsRepository.findByVerificationCode(FieldConstants.DVT_PRODUCT_TYPE,email);
    }

    @Override
    public boolean isCompleteEmailExisted(String email) {
        applicationsRepository = new ApplicationsRepository();
        List applicationList = applicationsRepository.findByEmail(FieldConstants.DVT_PRODUCT_TYPE,email, OnlineProtectorApplication.STATUS_COMPLETE);
        return (applicationList == null || applicationList.size() == 0)?false:true;
    }

    @Override
    public boolean isImcompleteMobileExisted(String countryCode, String mobile) {
        applicationsRepository = new ApplicationsRepository();
        List applicationList = applicationsRepository.findByMobileNo(FieldConstants.DVT_PRODUCT_TYPE, countryCode, mobile, OnlineProtectorApplication.STATUS_INCOMPLETE);
        return (applicationList == null || applicationList.size() == 0)?false:true;
    }

    @Override
    public boolean isCompleteMobileExisted(String countryCode, String mobile) {
        applicationsRepository = new ApplicationsRepository();
        List applicationList = applicationsRepository.findByMobileNo(FieldConstants.DVT_PRODUCT_TYPE, countryCode, mobile, OnlineProtectorApplication.STATUS_COMPLETE);
        return (applicationList == null || applicationList.size() == 0)?false:true;
    }

    @Override
    public Applications findById(String id) {
        applicationsRepository = new ApplicationsRepository();
        return applicationsRepository.findApplicationById(id);
    }

}
