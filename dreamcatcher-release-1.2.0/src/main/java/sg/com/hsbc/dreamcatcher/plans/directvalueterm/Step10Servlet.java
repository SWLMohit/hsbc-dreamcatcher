package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.helpers.*;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/10", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step10.jsp")
})
public class Step10Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 10;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        sanitized = sanitizeApplicantEmployment(request, sanitized);
        sanitized = sanitizeApplicantIncome(request, sanitized);
        if(sanitized.get(FieldConstants.APPLICANT_EMPLOYMENT)!=null){
        if (StringUtils.equalsIgnoreCase((String)sanitized.get(FieldConstants.APPLICANT_EMPLOYMENT), "Salaried")||StringUtils.equalsIgnoreCase((String)sanitized.get(FieldConstants.APPLICANT_EMPLOYMENT), "Self-employed")) {
            sanitized = sanitizeApplicantOccupation(request, sanitized);
            sanitized = sanitizeApplicantIndustry(request, sanitized);
            sanitized = sanitizeApplicantCompanyName(request, sanitized);
            sanitized = sanitizeApplicantCompanyCity(request, sanitized);
            sanitized = sanitizeApplicantCompanyCountry(request, sanitized);
        }

        }

        if ("Student".equals(sanitized.get(FieldConstants.APPLICANT_EMPLOYMENT))) {
            sanitized = sanitizeApplicantStudiesEndDate(request, sanitized);
        }

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);


        errors = validateApplicantEmployment(data, errors);
        errors = validateApplicantIncome(data, errors);
        if(data.get(FieldConstants.APPLICANT_EMPLOYMENT)!=null){
            if (StringUtils.equalsIgnoreCase((String)data.get(FieldConstants.APPLICANT_EMPLOYMENT), "Salaried")||StringUtils.equalsIgnoreCase((String)data.get(FieldConstants.APPLICANT_EMPLOYMENT), "Self-employed")) {
            errors = validateApplicantOccupation(data, errors);
            errors = validateApplicantIndustry(data, errors);
            errors = validateApplicantCompanyName(data, errors);
            errors = validateApplicantCompanyCity(data, errors);
            errors = validateApplicantCompanyCountry(data, errors);
        }
        }
        if ("Student".equals(data.get(FieldConstants.APPLICANT_EMPLOYMENT))) {
            errors = validateApplicantStudiesEndDate(data, errors);
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        if (data.get(FieldConstants.APPLICANT_EMPLOYMENT) != null) {
            application.setApplicantEmployment(data.get(FieldConstants.APPLICANT_EMPLOYMENT).toString());
        }

        if (data.get(FieldConstants.APPLICANT_INCOME) != null) {
            try {
                application.setApplicantIncome(NumberFormat.getNumberInstance(java.util.Locale.US).parse(data.get(FieldConstants.APPLICANT_INCOME).toString()).intValue());
            } catch (Exception e) {
                ErrorHandler.handleError(application, e);
                return false;
            }
        }
        if (data.get(FieldConstants.APPLICANT_OCCUPATION) != null) {
            application.setApplicantOccupation(data.get(FieldConstants.APPLICANT_OCCUPATION).toString());
        } else {
            application.setApplicantOccupation(null);
        }

        if (data.get(FieldConstants.APPLICANT_INDUSTRY) != null) {
            application.setApplicantIndustry(data.get(FieldConstants.APPLICANT_INDUSTRY).toString());
        } else {
            application.setApplicantIndustry(null);
        }

        if (data.get(FieldConstants.APPLICANT_COMPANY_NAME) != null) {
            application.setApplicantCompanyName(data.get(FieldConstants.APPLICANT_COMPANY_NAME).toString());
        } else {
            application.setApplicantCompanyName(null);
        }

        if (data.get(FieldConstants.APPLICANT_COMPANY_CITY) != null) {
            application.setApplicantCompanyCity(data.get(FieldConstants.APPLICANT_COMPANY_CITY).toString());
        } else {
            application.setApplicantCompanyCity(null);
        }

        if (data.get(FieldConstants.APPLICANT_COMPANY_COUNTRY) != null) {
            application.setApplicantCompanyCountry(data.get(FieldConstants.APPLICANT_COMPANY_COUNTRY).toString());
        } else {
            application.setApplicantCompanyCountry(null);
        }

        try {
            Date applicantStudiesEndDate = (new SimpleDateFormat("d/M/y")).parse(String.format("%s/%s", 1, data.get(FieldConstants.APPLICANT_STUDIES_END_DATE)));
            application.setApplicantStudiesEndDate(applicantStudiesEndDate);
        } catch (ParseException e) {
            application.setApplicantStudiesEndDate(null);
        }

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateApplicantEmployment(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_EMPLOYMENT))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_EMPLOYMENT, "Your employment status is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantIncome(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_INCOME))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_INCOME, "Your annual income is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantOccupation(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.APPLICANT_OCCUPATION))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_OCCUPATION, "Your occupation is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_OCCUPATION, "Your occupation contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantIndustry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.APPLICANT_INDUSTRY))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_INDUSTRY, "Your industry is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantCompanyName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.APPLICANT_COMPANY_NAME))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_COMPANY_NAME, "Your company name is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_COMPANY_NAME, "Your company name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantCompanyCity(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.APPLICANT_COMPANY_CITY))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_COMPANY_CITY, "Your company city is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_COMPANY_CITY, "Your company city contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantCompanyCountry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.APPLICANT_COMPANY_COUNTRY))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.APPLICANT_COMPANY_COUNTRY, "Your company country is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.APPLICANT_COMPANY_COUNTRY, "Your company country contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantStudiesEndDate(Map<String, Object> data, Map<String, String> errors) {
    	String endDate = String.format("%s", data.get(FieldConstants.APPLICANT_STUDIES_END_DATE));
        if ( !StringUtils.isBlank(endDate) ) 
        	endDate = String.format("%s/%s", 1, data.get(FieldConstants.APPLICANT_STUDIES_END_DATE));
        switch (Validator.validateDate(endDate)) {
            case Validator.ERROR_DATE_EMPTY:
                errors.put("applicant_studies_end_date", StringConstants.EMPTY_DATE);
                break;
            case Validator.ERROR_DATE_INVALID:
                errors.put("applicant_studies_end_date", "The end date of your studies is invalid.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantEmployment(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_EMPLOYMENT);

        sanitized.put(FieldConstants.APPLICANT_EMPLOYMENT, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIncome(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_INCOME).toString().replace(",", "");

        sanitized.put(FieldConstants.APPLICANT_INCOME, input);

        return sanitized;
    }


    private Map<String, Object> sanitizeApplicantOccupation(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_OCCUPATION);
        sanitized.put(FieldConstants.APPLICANT_OCCUPATION, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIndustry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_INDUSTRY);
        sanitized.put(FieldConstants.APPLICANT_INDUSTRY, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantCompanyName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_COMPANY_NAME);
        sanitized.put(FieldConstants.APPLICANT_COMPANY_NAME, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantCompanyCity(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_COMPANY_CITY);
        sanitized.put(FieldConstants.APPLICANT_COMPANY_CITY, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantCompanyCountry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.APPLICANT_COMPANY_COUNTRY);
        sanitized.put(FieldConstants.APPLICANT_COMPANY_COUNTRY, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantStudiesEndDate(HttpServletRequest request, Map<String, Object> sanitized) {
        String endDate = request.getParameter(FieldConstants.APPLICANT_STUDIES_END_DATE);
        sanitized.put(FieldConstants.APPLICANT_STUDIES_END_DATE, endDate);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put(FieldConstants.APPLICANT_EMPLOYMENT, application.getApplicantEmployment());
        data.put(FieldConstants.APPLICANT_INCOME, application.getApplicantIncome());
        data.put(FieldConstants.APPLICANT_OCCUPATION, application.getApplicantOccupation());
        data.put(FieldConstants.APPLICANT_INDUSTRY, application.getApplicantIndustry());
        data.put(FieldConstants.APPLICANT_COMPANY_NAME, application.getApplicantCompanyName());
        data.put(FieldConstants.APPLICANT_COMPANY_CITY, application.getApplicantCompanyCity());
        data.put(FieldConstants.APPLICANT_COMPANY_COUNTRY, application.getApplicantCompanyCountry());
System.out.println("Income:"+application.getApplicantIncome());
        Date applicantStudiesEndDate = application.getApplicantStudiesEndDate();
        if (applicantStudiesEndDate != null) {
            data.put(FieldConstants.APPLICANT_STUDIES_END_DATE, (new SimpleDateFormat("MM/y")).format(applicantStudiesEndDate));
        }

        if (StringUtils.isBlank(application.getApplicantCompanyCity())) {
            if (application.applicantIsSingaporean() || application.applicantIsPermanentResident() || application.applicantIsPassHolder()) {
                data.put(FieldConstants.APPLICANT_COMPANY_CITY, "Singapore");
            }        		
        }
        if (StringUtils.isBlank(application.getApplicantCompanyCountry())) {
            if (application.applicantIsSingaporean() || application.applicantIsPermanentResident() || application.applicantIsPassHolder()) {
                data.put(FieldConstants.APPLICANT_COMPANY_COUNTRY, "Singapore");
            }        		
        }

        request.setAttribute("data", data);
    }

}
