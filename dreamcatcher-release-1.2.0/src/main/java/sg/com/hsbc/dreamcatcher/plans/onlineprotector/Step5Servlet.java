package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.*;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/5", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step5.jsp")
})
public class Step5Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeApplicantEmployment(request, sanitized);
        sanitized = sanitizeApplicantIncome(request, sanitized);
        sanitized = sanitizeApplicantSmoker(request, sanitized);

        if (!getApplication(request).insuredIsSelf()) {
            sanitized = sanitizeInsuredEmployment(request, sanitized);
            sanitized = sanitizeInsuredIncome(request, sanitized);
            sanitized = sanitizeInsuredSmoker(request, sanitized);
        }

        sanitized = sanitizeInsuredMedical1(request, sanitized);
        sanitized = sanitizeInsuredMedical2(request, sanitized);
        sanitized = sanitizeInsuredMedical3(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();

        errors = validateApplicantEmployment(data, errors);
        errors = validateApplicantIncome(data, errors);
        errors = validateApplicantSmoker(data, errors);

        if (!getApplication(request).insuredIsSelf()) {
            errors = validateInsuredEmployment(data, errors);
            errors = validateInsuredIncome(data, errors);
            errors = validateInsuredSmoker(data, errors);
        }

        errors = validateInsuredMedical1(data, errors);
        errors = validateInsuredMedical2(data, errors);
        errors = validateInsuredMedical3(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        if (data.get("applicant_employment") != null) {
            application.setApplicantEmployment(data.get("applicant_employment").toString());
        }

        if (data.get("applicant_income") != null) {
            try {
                application.setApplicantIncome(NumberFormat.getNumberInstance(java.util.Locale.US).parse(data.get("applicant_income").toString()).intValue());
            } catch (Exception e) {
                ErrorHandler.handleError(this, e);
                return false;
            }
        }

        if (data.get("applicant_smoker") != null) {
            application.setApplicantSmoker((boolean)data.get("applicant_smoker"));
        }

        if (data.get("insured_employment") != null) {
            application.setInsuredEmployment(data.get("insured_employment").toString());
        } else {
            application.setInsuredEmployment(null);
        }

        if (data.get("insured_income") != null) {
            application.setInsuredIncome(new Integer(data.get("insured_income").toString()));
        } else {
            application.setInsuredIncome(null);
        }

        if (data.get("insured_smoker") != null) {
            application.setInsuredSmoker((boolean)data.get("insured_smoker"));
        } else {
            application.setInsuredSmoker(null);
        }

        if (data.get("insured_medical1") != null) {
            application.setInsuredMedical1((boolean)data.get("insured_medical1"));
        } else {
            application.setInsuredMedical1(null);
        }

        if (data.get("insured_medical2") != null) {
            application.setInsuredMedical2((boolean)data.get("insured_medical2"));
        } else {
            application.setInsuredMedical2(null);
        }

        if (data.get("insured_medical3") != null) {
            application.setInsuredMedical3((boolean)data.get("insured_medical3"));
        } else {
            application.setInsuredMedical3(null);
        }

        if (application.getInsuredMedical1() || application.getInsuredMedical2() || application.getInsuredMedical3()) {
            application.setUnderwritingLifestyleQuestionsOptin(null);
            application.setUnderwritingLifestyleMortgage(null);
            application.setUnderwritingLifestyleProfile(null);
            application.setUnderwritingLifestyleHeight(null);
            application.setUnderwritingLifestyleWeight(null);
            application.setUnderwritingLifestyleExercise(null);
        }

        application.setMaxStep(getCurrentStep() + 2);
        return application.save();
    }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("/our-plans/online-protector/7");
    }

    private Map<String, String> validateApplicantEmployment(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_employment"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_employment", "Your employment status is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantIncome(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_income"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_income", "Your annual income is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantSmoker(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_smoker"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_smoker", "Please indicate if you have smoked in the last 12 months.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredEmployment(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_employment"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_employment", "Your spouse's employment status is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredIncome(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_income"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_income", "Your spouse's annual income is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredSmoker(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_smoker"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_smoker", "Please indicate if your spouse has smoked in the last 12 months.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredMedical1(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_medical1"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_medical1", "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredMedical2(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_medical2"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_medical2", "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredMedical3(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_medical3"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_medical3", "Please indicate your answer for this question.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantEmployment(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_employment");

        sanitized.put("applicant_employment", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIncome(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_income").toString().replace(",", "");

        sanitized.put("applicant_income", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantSmoker(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_smoker");
        if (input != null) {
            sanitized.put("applicant_smoker", input.equals("Y"));
        } else {
            sanitized.put("applicant_smoker", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredEmployment(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_employment");

        sanitized.put("insured_employment", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredIncome(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_income").toString().replace(",", "");

        sanitized.put("insured_income", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredSmoker(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_smoker");
        if (input != null) {
            sanitized.put("insured_smoker", input.equals("Y"));
        } else {
            sanitized.put("insured_smoker", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMedical1(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_medical1");
        if (input != null) {
            sanitized.put("insured_medical1", input.equals("Y"));
        } else {
            sanitized.put("insured_medical1", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMedical2(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_medical2");
        if (input != null) {
            sanitized.put("insured_medical2", input.equals("Y"));
        } else {
            sanitized.put("insured_medical2", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMedical3(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_medical3");
        if (input != null) {
            sanitized.put("insured_medical3", input.equals("Y"));
        } else {
            sanitized.put("insured_medical3", null);
        }

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("applicant_employment", application.getApplicantEmployment());
        data.put("applicant_income", application.getApplicantIncome());
        data.put("applicant_smoker", application.getApplicantSmoker());
        data.put("insured_employment", application.getInsuredEmployment());
        data.put("insured_income", application.getInsuredIncome());
        data.put("insured_smoker", application.getInsuredSmoker());
        data.put("insured_medical1", application.getInsuredMedical1());
        data.put("insured_medical2", application.getInsuredMedical2());
        data.put("insured_medical3", application.getInsuredMedical3());

        request.setAttribute("data", data);
    }

}
