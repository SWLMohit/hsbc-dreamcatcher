package sg.com.hsbc.dreamcatcher.eclaims;

import static sg.com.hsbc.dreamcatcher.helpers.PDFGenerator.PAGE_MARGIN_BOTTOM;
import static sg.com.hsbc.dreamcatcher.helpers.PDFGenerator.PAGE_MARGIN_LEFT;
import be.quodlibet.boxable.BaseTable;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDPage;

import be.quodlibet.boxable.BaseTable;
import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.helpers.Config;
import sg.com.hsbc.dreamcatcher.helpers.Database;
import sg.com.hsbc.dreamcatcher.helpers.EmailSender;
import sg.com.hsbc.dreamcatcher.helpers.ErrorHandler;
import sg.com.hsbc.dreamcatcher.helpers.PDFGenerator;

public abstract class EClaimApplication extends ApplicationForm {
    public static final int PAYMENT_CHEQUE_VIA_POSTAGE_MAIL = 1;
    public static final int PAYMENT_SELF_COLLECT_CHEQUE = 2;
    public static final int PAYMENT_TRANSFER_TO_BANK_ACCOUNT = 3;
    public static final int EMAIL_TEMPLATE_DEATH = 1;
    public static final int EMAIL_TEMPLATE_ILLNESS = 2;

    private final static Logger logger = Logger.getLogger(EClaimApplication.class);
    protected String id;
    protected String claimsType;
    protected String claimsTitle;
    protected String referenceNumber = "X-DDMMYYYY-XXX";
    protected String[] policyNumberList;
    protected String policyownerFirstName;
    protected String policyownerLastName;
    protected String policyownerNRIC;
    protected Date policyownerBirthday;
    protected List<String> policyownerPassport;// HSBCIDCU-798
    protected String policyownerDay;
    protected String policyownerMonth;
    protected String policyownerYear;

    protected String policyownerContactCountry;
    protected String policyownerContactNumber;
    protected String policyownerEmail;
    protected Boolean isPolicyownerLifeInsured;
    protected String policyownerLifeInsuredFirstName;
    protected String policyownerLifeInsuredLastName;
    protected List<String>  policyownerLifeInsuredPassport; //HSBCIDCU-798
    protected DeathClaimantPortion[] claimantList;
    protected Boolean bankrupt;
    protected DeathBankruptPortion[] bankruptList;
    protected Boolean otherInsured;
    protected DeathOtherInsuredPortion[] otherInsuredList;
    protected Integer payment;
    protected String bankName;
    protected String accountNumber;
    protected String bankCountry;
    protected List<String> accountDocument; //HSBCIDCU-798
    protected String bankCity;
    protected String bankPostalCode;
    protected String bankBuildingNumber;
    protected String bankUnitNumber1;
    protected String bankUnitNumber2;
    protected String bankStreet1;
    protected String bankStreet2;
    protected String bankBuildingName;
    protected String bankSwiftCode;

    protected List<String> uploadedDocuments;

    protected Map <String, Long>fileInfo; //HSBCIDCU-798
    protected Map <String,String>fileMapping;
 
    protected Date emailSent;
    protected SimpleDateFormat dateFormatter;
    protected SimpleDateFormat dateFormatterMonthYear;

    protected EClaimApplication() {
        this.setStatus(STATUS_INCOMPLETE);
        this.initializeListField();

        dateFormatter = new SimpleDateFormat("dd MMM yyyy");
        dateFormatterMonthYear= new SimpleDateFormat("MM/yyyy");
    }

    /**
     * This method retrieves the date format "MM/yyyy"
     *
     * @return  SimpleDateFormat object of given format "MM/yyyy"
     */
    public SimpleDateFormat getDateFormatterMonthYear() {
        return dateFormatterMonthYear;
    }

    /**
     * This method returns a string of the date object parameter
     * formatted into "MM/yyyy" format.
     *
     * @param date  Date object passed as a parameter
     * @return      String of formatted date.
     */
    public String formatDateMonthYear(Date date){
        return getDateFormatterMonthYear().format(date);
    }

    /**
     * This method returns a string of the date object parameter
     * formatted into "MM/yyyy" format.
     *
     * @param date  Date object passed as a parameter
     * @return      String of formatted date.
     */
    public SimpleDateFormat getDateFormatter() {
        return dateFormatter;
    }

    /**
     * Formats the date received on the 
     * basis of date formatter created
     * @return the date as a String
     */
    public String formatDate(Date date){
        return getDateFormatter().format(date);
    }


    /**
     * This method checks if the total size of all the attachments is greater than
     * 10MB. If the size is exceeded it returns a boolean true value else it will
     * return false.
     * 
     * @return true/false
     */
     public boolean fileSizeExceeded() {
      try {
        logger.info("Calculating File sizes for attachments");
        Long size = 0L;
        logger.info("Final List of files " + getFileInfo());
        for (Long value : getFileInfo().values()) {
          size = size + value;
        }
        logger.info("Total size of files in MB " + size / (1024.00 * 1024.00));
        if ((size / 1024.00) > 10240.00) {
          logger.info("File size of attachments exceeded");
          return true;
        }
      } catch (Exception e) {
        logger.error("Exception occurred in fileSizeExceeded method " + e);
        return false;
      }
      return false;
     }


    protected void initializeListField() {
        this.claimantList = new DeathClaimantPortion[1];
        this.claimantList[0] = new DeathClaimantPortion();

        this.bankruptList = new DeathBankruptPortion[1];
        this.bankruptList[0] = new DeathBankruptPortion();

        this.otherInsuredList = new DeathOtherInsuredPortion[1];
        this.otherInsuredList[0] = new DeathOtherInsuredPortion();

        this.uploadedDocuments = new ArrayList<>();
    }

    public String getClaimsTitle() {
        return claimsTitle;
    }

    public String getPDFFilename(boolean adminPdf) {
    	if(!adminPdf){
        return getUploadedFilePath(getReferenceNumber() + ".pdf");
    	}else{
    		return getUploadedFilePath(getReferenceNumber() + "-HSBC.pdf");
    	}
    }

    /**
     * write the eclaim title with type and reference number
     * @param pd
     * @param yStart
     * @return
     * @throws IOException
     */
    public float writeTitle(PDFGenerator pd, float yStart) throws IOException {
        PDPage page = pd.getCurrentPage();

        pd.setFont(PDFGenerator.FONT_BOLD, (float) 20);
        String title = String.format("Eclaim Submission: %s", getReferenceNumber());
        pd.writeText(page, title, yStart);
        pd.setFont(PDFGenerator.FONT_NORMAL, pd.getDefaultFontSize());
        yStart -= (20 + pd.getDefaultFontSize());
        return yStart;
    }

    public float getTableWidth(PDFGenerator pd){
        PDPage page = pd.getCurrentPage();
        return page.getMediaBox().getWidth() - (PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
    }

    /**
     * Claim type
     * @param pd
     * @param yStart
     * @return
     * @throws IOException
     */
    public float writeClaimsType(PDFGenerator pd, float yStart) throws IOException {

      try {
        PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        float tableWidth = getTableWidth(pd);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PAGE_MARGIN_BOTTOM, tableWidth, PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
        List<String[]> rows = new ArrayList<>();
        rows.add(new String[]{"Claim Type:", getClaimsTitle()});
        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
        }
        return table.draw() - 10;
      }
      catch(Exception e) {
        logger.error("Exception occurred in writeClaimsType "+e);
        return 0.0f;
      }
        
    }


    public float writeBasicInformation(PDFGenerator pd, float yStart) throws IOException {

        logger.info("Starting writeBasicInformation ");
        PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        float tableWidth = getTableWidth(pd);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PAGE_MARGIN_BOTTOM, tableWidth, PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        List<String[]> rows = new ArrayList<>();

        List<String> passportDocuments=getPolicyownerPassport();
        logger.info("No of Passport documents "+passportDocuments.size());
        rows.add(new String[]{"Policy number(s)", String.join(" , ", getPolicyNumberList())});
        rows.add(new String[]{"Name of Policyowner", getPolicyownerFirstName() + ", " + getPolicyownerLastName()});
        
        if (passportDocuments != null) {
          Collections.sort(passportDocuments);
          for (int i=0; i<passportDocuments.size();i++) {
            String doc=passportDocuments.get(i);
            if (!uploadedDocuments.contains(doc)) {
              uploadedDocuments.add(doc);
            }
          }

        }
          logger.info("Updated uploadedDocuments "+uploadedDocuments.size());
          
        rows.add(new String[]{"NRIC of Claimant",  getPolicyownerNRIC()});
        rows.add(new String[]{"Date of Birth of Claimant",  formatDate(getPolicyownerBirthday())});

        rows.add(new String[]{"NRIC/Passport of Claimant",  "Number of documents uploaded - "+(passportDocuments.isEmpty()?0:passportDocuments.size())});
        rows.add(new String[]{"Contact no. of Claimant", getPolicyownerContactCountry() + " - " + getPolicyownerContactNumber()});
        rows.add(new String[]{"Email address of Claimant", getPolicyownerEmail()});

        rows.add(new String[]{"Confirm email address", getPolicyownerEmail()});
        rows.add(new String[]{"Is the Policyowner the Life Insured?", isPolicyownerLifeInsured() ? "Yes": "No"});
    
        if (!isPolicyownerLifeInsured()) {
          List<String> insuredPassportDocuments=getPolicyownerLifeInsuredPassport();
          if(insuredPassportDocuments!=null) {
            logger.info(" No insuredPassportDocuments  "+insuredPassportDocuments.size());

            rows.add(new String[]{"Name of Life Insured", getPolicyownerLifeInsuredFirstName() + ", " + getPolicyownerLifeInsuredLastName()});
            rows.add(new String[]{"NRIC/Passport of the Life Insured",  "Number of documents uploaded - "+(insuredPassportDocuments.isEmpty()?0:insuredPassportDocuments.size())});
            
            Collections.sort(insuredPassportDocuments);
            for(int i=0;i<insuredPassportDocuments.size();i++){
              String doc=insuredPassportDocuments.get(i);
              if(!uploadedDocuments.contains(doc)){
               uploadedDocuments.add(doc);
              }

            }
           
        }
        }
        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
        }
        logger.info("Ending writeBasicInformation");
        return table.draw() - 10;

    }

    public float writeClaimantInformation(PDFGenerator pd, float yStart) throws IOException {
        logger.info("Starting writeClaimantInformation");
        PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        float tableWidth = page.getMediaBox().getWidth() - (PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        int count = 0;
        for (DeathClaimantPortion claimant : getClaimantList()) {
            BaseTable table = new BaseTable(yStart, yStartNewPage, PAGE_MARGIN_BOTTOM, tableWidth, PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

            List<String[]> rows = new ArrayList<>();
            count++;

            pd.addTableHeader(table,  "Claimant information");


            List<String> claimaintRelationshipDocument=claimant.getRelationshipDocument();
					if (count == 1) {
					    rows.add(new String[]{"Name of Claimant ",claimant.getFirstName() + ", " +claimant.getLastName() });
					}else {
		                rows.add(new String[]{"Name of Claimant "+ count,claimant.getFirstName() + ", " +claimant.getLastName() });
					}

            if (claimaintRelationshipDocument != null) {
              Collections.sort(claimaintRelationshipDocument);
              for (int i = 0; i < claimaintRelationshipDocument.size(); i++) {
                String doc = claimaintRelationshipDocument.get(i);
                if (!uploadedDocuments.contains(doc)) {
                  uploadedDocuments.add(doc);
                }
              }

            }

            StringBuffer claimantAdd=new StringBuffer();
            if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(claimant.getBuildingNumber()), "null"))&&StringUtils.isNotBlank(claimant.getBuildingNumber())){
            	claimantAdd.append(claimant.getBuildingNumber()+" ");
            }
            if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(claimant.getStreet1()), "null"))&&StringUtils.isNotBlank(claimant.getStreet1())){
            	claimantAdd.append(claimant.getStreet1());
            }
            if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(claimant.getStreet2()), "null"))&&StringUtils.isNotBlank(claimant.getStreet2())){
            	claimantAdd.append("<br>"+claimant.getStreet2() +" ");
            }
            if(!(StringUtils.equalsIgnoreCase(claimant.getUnitNumber1(), "null"))&&StringUtils.isNotBlank(claimant.getUnitNumber1())){
            	claimantAdd.append("<br>"+claimant.getUnitNumber1() +" -");
            }
            if(!(StringUtils.equalsIgnoreCase(claimant.getUnitNumber2(), "null"))&&StringUtils.isNotBlank(claimant.getUnitNumber2())){
            	claimantAdd.append(claimant.getUnitNumber2() +"");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(claimant.getBuildingName()), "null")&&StringUtils.isNotBlank(claimant.getBuildingName())){
            	claimantAdd.append("<br>"+claimant.getBuildingName()+"");
            }
            if(!StringUtils.equalsIgnoreCase(StringUtils.trim(claimant.getCity()), "null")&&StringUtils.isNotBlank(claimant.getCity())){
            	claimantAdd.append("<br>"+claimant.getCity()+"");
            }
            if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(claimant.getPostalCode()), "null"))&&StringUtils.isNotBlank(claimant.getPostalCode())){
            	claimantAdd.append(" "+claimant.getPostalCode()+"");
            }
            String claimantAddress=claimantAdd.toString();


            rows.add(new String[]{"Relationship to the deceased", claimant.getRelationship()});
            rows.add(new String[]{"Please upload a copy of the document showing proof of relationship / rights to administrator Deceased's Estate",
            		"Number of documents uploaded - "+((claimaintRelationshipDocument==null||claimaintRelationshipDocument.isEmpty())?0:claimaintRelationshipDocument.size())});
           

            rows.add(new String[]{
                    "Residential Address of Claimant",
                    claimantAddress
            });


            rows.add(new String[]{
                    "If the Residential Address is not indicated on Claimant's Identification document, please upload document showing proof of Claimant's Residential Address",
                    claimant.getAddressDocument()!=null && !"".equals(claimant.getAddressDocument())? "Number of documents uploaded - "+claimant.getAddressDocument().size() :  "No"
            });

            if(claimant.getAddressDocument()!=null && !claimant.getAddressDocument().isEmpty()){

           
              List<String> claimaintAddressDocument=claimant.getAddressDocument();
              Collections.sort(claimaintAddressDocument);
              for(int i=0;i<claimaintAddressDocument.size();i++){
                String doc=claimaintAddressDocument.get(i);
                if(!uploadedDocuments.contains(doc)){
                 uploadedDocuments.add(doc);
                }
              }

            }
            for (String[] row : rows) {
                pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
            }
            yStart = table.draw() - 10;


        }
        return yStart;
    }

    public float writeDeclaration(PDFGenerator pd, float yStart) throws IOException {
        PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
        float tableWidth = page.getMediaBox().getWidth() - (PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        BaseTable table = new BaseTable(yStart, yStartNewPage, PAGE_MARGIN_BOTTOM, tableWidth, PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        pd.addTableHeader(table, "Declaration & Authorisation");

        List<String[]> rows = new ArrayList<>();
        rows.add(new String[]{"" + "<ol>" +
                "<li>I declare that all the information given by me in this claim form is, to the best of my knowledge and belief, true, complete and accurate, and that no material information has been withheld nor is any relevant circumstances omitted.<br></li>" +
                "<li>I authorise HSBC Insurance (Singapore) Pte. Limited (the \"Company\") to seek medical information from any doctor who, at any time, has attended to me/the life insured concerning anything that affects my/the life insured's health, seek information from any insurance offices to which an insurance proposal has been made, seek information from any other sources (including employer, government institution, bank or other organisation or person) and disclose information including medical information about me/the life insured to other insurers, reinsurers or other third parties assisting with this claim.<br></li>" +
                "<li>I agree that all written statements and affidavits, of all doctors who has attended to me/the life insured or any other sources, and all other papers called for by instructions hereon shall constitute and they are hereby made a part of these Proofs of medical evidence and further agree that the furnishing of this form or any other forms supplemental thereof, by the Company shall not constitute nor be considered an admission by it that there is an insurance in force on the life in question, nor a waiver of any rights or defences.<br></li>" +
                "<li>I understand and agree that the Company shall have full access to the information above and a photographic copy of this authorisation shall be as valid as the original.<br></li>" +
                "<li>I understand and acknowledge that the personal data which I have submitted is being collected for the purposes stated in the HSBC Data Protection Policy (http://www.insuranceonline.hsbc.com.sg/privacy-policy/) and consent to the collection, use and disclosure of my personal data accordingly.<br></li>" +
                "</ol>"});

        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS_SINGLE);
        }

        yStart = table.draw() - 10;

        return yStart;
    }

    public float writeClaimInformation(PDFGenerator pd, float yStart) throws IOException {
        return yStart;
    }

    
    /**
     * Generates the pdf page with claimant and 
     * claim information
     * @param pd      PDFGenerator which is used to 
     * 				  write the pdf
     * return float   the next position where the table can be
     *                drawn in pdf
     */
    public float generatePage1(PDFGenerator pd) throws IOException {
        PDPage page = pd.createPage();

        // add the company logo image
        pd.applyPageHeader(page, pd.getDocument());
        pd.addPage(page);
        pd.setCurrentPage(page);
        
        float yStart = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        try {
            // Claim and  reference number
            yStart = writeTitle(pd, yStart);
            yStart = writeClaimsType(pd, yStart);

            yStart = writeBasicInformation(pd, yStart);
            String claimsTitle=StringUtils.trim(getClaimsTitle());
            
            /**
             * Added condition to print the claimant indormation table
             * only for Death due to Accident And Death due to Ilness
             * Claim types
             */
            
           if(StringUtils.equalsIgnoreCase(claimsTitle, "Death due to Accident/Unnatural Causes")||StringUtils.equalsIgnoreCase(claimsTitle, "Death due to Illness")){
              yStart = writeClaimantInformation(pd, yStart);
            }
            yStart=writeClaimInformation(pd, yStart);
        } catch (Exception e) {

            logger.error("Exception occurred in generatePage1 "+e);

        }
        return yStart;
    }

   
    /**
     * Genrates the 2nd page for the pdf which
     * includes other information table.New
     * page is created which contains the table
     * 
     * 
     * @param pd  the PDFGenerator to create write
     *            details on the pdf page
     *            
     */
    

    /**
     * Generate other information and payment information
     * @param pd
     * @param yStart
     * @return
     */
    public float generatePage2(PDFGenerator pd,float yStart) {
       logger.info("Entering generatePage2");
       PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        return bankruptcyDetailsAndDeclaration(page,pd,yStart);

    }
    
    
    /**
     * Creates the table which displays the payment details retrieved from the eclaims 
     * application form .The table creation for the payments page is done here
     * @param page           The page where the information would be written
     * @param pd             The PDFGenarator to write information on the page
     * @param yStart         The start position where table has to be drawn
     * @param tableWidth     The width of the table to be created
     * @return  yStart  float           The end position where the table finished
     */
    public float bankruptcyDetailsAndDeclaration(PDPage page,PDFGenerator pd,float yStart){
    	float tableWidth = page.getMediaBox().getWidth() - (PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
    	float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
    	try {
    		
    		
           BaseTable table = new BaseTable(yStart, yStartNewPage, PAGE_MARGIN_BOTTOM, tableWidth, PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);
            List<String[]> rows = new ArrayList<>();
            pd.addTableHeader(table, "Other information");

            rows.add(new String[]{"Has the Claimant, Life Insured or Policyowner been bankrupt or insolvent or has either executed any deed or transfer for the benefit of " +
                    "creditors since becoming interested in the policy?", getBankrupt() ? "Yes" : "No"});

                if(getBankruptList() != null) {
                	int bankrupt=1;
                    for (DeathBankruptPortion bankrupcy : getBankruptList()) {
                    	String bankruptName="Name of bankrupt person";
                    	if(bankrupt>1){
                    		bankruptName+=" "+bankrupt;
                    	}
                        rows.add(new String[]{"<no-border>"+bankruptName, bankrupcy.getFirstName() + ", " + bankrupcy.getLastName()});
                        rows.add(new String[]{"<no-border>"+"Country/state that issued the bankrupt order", bankrupcy.getCountry() + ", " + bankrupcy.getState()});
                        rows.add(new String[]{"<no-border-last>"+"Date of Bankruptcy", formatDate(bankrupcy.getBankruptcyDate())});
                        bankrupt++;

                }

                }
                rows.add(new String[]{

                        "Has the deceased or life insured been insured with other insurance company(ies)?", getOtherInsured() ? "Yes" : "No"

                });

                if(getOtherInsuredList() != null){
                	int insurance=1;
                    for (DeathOtherInsuredPortion insured : getOtherInsuredList()) {
                    	String nameOfInsurance="Name of Insurance company";
                    	if(insurance>1){
                    		nameOfInsurance+=" "+insurance;
                    	}

                        rows.add(new String[]{"<no-border>"+nameOfInsurance, insured.getCompanyName()});
                        rows.add(new String[]{"<no-border>"+"Issue date", formatDate(insured.getPolicyIssueDate())});
                        rows.add(new String[]{"<no-border>"+"Type of plan", insured.getPlanType()});


                        try{

                        DecimalFormat df1 = new DecimalFormat("#,###.00");
                        rows.add(new String[]{"<no-border>"+"Claim Amount", "S$"+df1.format(insured.getClaimAmount())});

                        }catch(Exception e){
                            logger.error("error in formatting the number "+e);
                        }
                        
                        rows.add(new String[]{"<no-border-last>"+"Claim Admitted", insured.getClaimAdmitted() ? "Yes" : "No"});
                        insurance++;

                  
                }

                

            }

            for (String[] row : rows) {
                pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
            }
            yStart = table.draw() - 10;

            // Payment page information
            page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
            pd.setCurrentPage(page);

          

            table = new BaseTable(yStart, yStartNewPage, PAGE_MARGIN_BOTTOM, tableWidth, PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

            pd.addTableHeader(table, "Payment option");
            rows = new ArrayList<>();

            String paymentOption = "";
            if(getPayment() == PAYMENT_CHEQUE_VIA_POSTAGE_MAIL){
                paymentOption = "Postage Mail";
            }else if(getPayment() == PAYMENT_SELF_COLLECT_CHEQUE){
                paymentOption = "Self-collect cheque";
            }else if(getPayment() == PAYMENT_TRANSFER_TO_BANK_ACCOUNT){
                paymentOption = "Transfer into my bank account";
            }
            rows.add(new String[]{"Option Chosen", paymentOption});
                if(getPayment() == PAYMENT_TRANSFER_TO_BANK_ACCOUNT){
                    rows.add(new String[]{"<no-border>Name of Bank", getBankName()});
                    rows.add(new String[]{"<no-border>Account Number / IBAN", getAccountNumber()});
                    rows.add(new String[]{"<no-border>Country of Bank", getBankCountry()});
                   if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getBankCountry()), "Singapore")){
                	   
                	   StringBuffer claimantAdd=new StringBuffer();
                       if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBankBuildingNumber()), "null"))&&StringUtils.isNotBlank(getBankBuildingNumber())){
                       	claimantAdd.append(getBankBuildingNumber()+" ");
                       }
                       if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBankStreet1()), "null"))&&StringUtils.isNotBlank(getBankStreet1())){
                       	claimantAdd.append(getBankStreet1());
                       }
                       if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBankStreet2()), "null"))&&StringUtils.isNotBlank(getBankStreet2())){
                       	claimantAdd.append("<br>"+getBankStreet2() +" ");
                       }
                       if(!(StringUtils.equalsIgnoreCase(getBankUnitNumber1(), "null"))&&StringUtils.isNotBlank(getBankUnitNumber1())){
                       	claimantAdd.append("<br>"+getBankUnitNumber1() +" -");
                       }
                       if(!(StringUtils.equalsIgnoreCase(getBankUnitNumber2(), "null"))&&StringUtils.isNotBlank(getBankUnitNumber2())){
                       	claimantAdd.append(getBankUnitNumber2() +"");
                       }
                       if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getBankBuildingName()), "null")&&StringUtils.isNotBlank(getBankBuildingName())){
                       	claimantAdd.append("<br>"+getBankBuildingName()+"");
                       }
                       if(!StringUtils.equalsIgnoreCase(StringUtils.trim(getBankCity()), "null")&&StringUtils.isNotBlank(getBankCity())){
                       	claimantAdd.append("<br>"+getBankCity()+"");
                       }
                       if(!(StringUtils.equalsIgnoreCase(StringUtils.trim(getBankPostalCode()), "null"))&&StringUtils.isNotBlank(getBankPostalCode())){
                       	claimantAdd.append(" "+getBankPostalCode()+"");
                       }
                       String bankAdd=claimantAdd.toString();
                    rows.add(new String[]{"<no-border>Address of Bank", 
                    		bankAdd
                    
                    });

                    
                    rows.add(new String[]{
                            "<no-border>Swift Code / Swift address",

                            getBankSwiftCode() 

                    });

                    
                   }

                   List<String> accountDocumentDocs=getAccountDocument(); 

                   rows.add(new String[]{"<no-border-last>Please submit a valid copy of your bank book / statement for account verification", "Number of documents uploaded - "+accountDocumentDocs.size()});
                    if (accountDocumentDocs != null) {
                      Collections.sort(accountDocumentDocs);
                      for (int i = 0; i < accountDocumentDocs.size(); i++) {
                        String doc = accountDocumentDocs.get(i);
                        if (!uploadedDocuments.contains(doc)) {
                          uploadedDocuments.add(doc);
                        }
                      }
                    }

                }
               
                      

            for (String[] row : rows) {
                pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS);
            }
            yStart = table.draw() - 10;

    	page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);
        pd.setCurrentPage(page);
            yStart=writeDeclaration(pd, yStart);

        } catch (Exception e) {


            logger.error("error in generate other and payment information page "+e);

        }

        logger.info("Exiting generatePage2");

        return yStart;
    }

    public float writeFileList(PDFGenerator pd, float yStart) throws IOException {

        logger.info("Entering writeFileList ");
        try {
        PDPage page = pd.getDocument().getPage(pd.getDocument().getNumberOfPages() - 1);

        pd.setCurrentPage(page);
        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;
        float tableWidth = page.getMediaBox().getWidth() - (PAGE_MARGIN_LEFT + PDFGenerator.PAGE_MARGIN_RIGHT);
        
        BaseTable table = new BaseTable(yStart, yStartNewPage, PAGE_MARGIN_BOTTOM, tableWidth, PAGE_MARGIN_LEFT, pd.getDocument(), page, true, true);

        List<String[]> rows = new ArrayList<>();

        pd.addTableHeader(table, "Uploaded documents (" + uploadedDocuments.size() + " - No. of documents)");

        StringBuilder sb = new StringBuilder("<ol>");
        for (String filename : uploadedDocuments) {
            sb.append("<li>").append(filename.substring(filename.indexOf("-")+1)).append("</li>");
        }
        sb.append("</ol>");
        rows.add(new String[]{sb.toString()});

        for (String[] row : rows) {
            pd.addTableRow(table, row, PDFGenerator.TABLE_ROW_TYPE_CLAIMS_SINGLE);
        }
        yStart = table.draw() - 10;

        }
        catch(Exception e) {
          logger.error("Exception occurred in writeFileList"+e);
        }
        logger.info("Exiting writeFileList ");

        return yStart;
    }


    public void generatePage3(PDFGenerator pd) {
        PDPage page = pd.getCurrentPage();

        float yStartNewPage = page.getMediaBox().getHeight() - PDFGenerator.PAGE_CONTENT_AREA_MARGIN_TOP;

        try {
        	 
            writeFileList(pd, yStartNewPage);
        } catch (Exception e) {

        	logger.error("error in generating the pdf "+e);

        }
    }

    public void generatePages(PDFGenerator pd) throws IOException {
    }

    public String getZipFilePath(boolean admin){
    	if(!admin){
         return getUploadedFilePath(getReferenceNumber() + ".zip");
    	}else{
    		return getUploadedFilePath(getReferenceNumber() + "-HSBC.zip");
    	}
    }

    public File getUploadedDir() {
        return new File(String.format("%s/%s", System.getProperty("java.io.tmpdir"), getId()));
    }

    public String getUploadedFilePath(String fileName){
        return new File(getUploadedDir(), fileName).getPath();
    }

    public ArrayList<String> getAllUploadedFiles() {
        File folder = getUploadedDir();
        File[] listOfFiles = folder.listFiles();
        ArrayList<String> files = new ArrayList<>();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                files.add(listOfFiles[i].getPath());
            }
        }
        return files;
    }

    public String generateZipFile(boolean admin) throws IOException {
        String filename = getZipFilePath(admin);

       
        FileOutputStream fos = new FileOutputStream(filename);
        ZipOutputStream zos = new ZipOutputStream(fos);

        try {
            ArrayList<String> files = new ArrayList<>(Arrays.asList(
                    getPDFFilename(admin)
            ));

            /*if total attachments size >10MB condition*/
            if(!fileSizeExceeded()) {

            for (String file : uploadedDocuments) {
            	if(!files.contains(file)){
                    files.add(getUploadedFilePath(file));
                 }

                
              }

            }
            files.stream().map(File::new).forEach(file -> {
                try(FileInputStream fis = new FileInputStream (file)){
                    logger.info("Add file to zip:" + file.getName());
                    ZipEntry zipEntry = new ZipEntry(file.getName());
                    zos.putNextEntry(zipEntry);

                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = fis.read(bytes)) >= 0) {
                        zos.write(bytes, 0, length);
                    }
                    //HSBCICDU-670: Fix corrupted zip file error
                    zos.closeEntry();
                    fis.close();
                } catch (IOException e) {

                    logger.error("generateZipFile:",e);

                }
            });
        } catch (Exception e) {
            logger.error("generateZipFile:",e);
            return null;
        } finally {
            //HSBCICDU-670: Fix corrupted zip file error
            zos.close();
            fos.close();
        }
        return filename;
    }

	/**
	 * Generates the admin pdf
	 * and the customer pdf
	 * @throws IOException 
	 */
	public void generatePDF() throws IOException {
		generateAdminPdf(false);
		generateAdminPdf(true);
    }
	/**
	 * Creates the application pdf to be send to the end user 

	 * Creates the object of PDFGenerator,calls the function
	 * to populate the pdf pages and adds footer to all pdf 
	 * pages.Handles the exception if thrown else closes the
	 * and saves te pdf created
	 * 
	 * @return void
	 * @throws IOException 
	 */

    private void generateAdminPdf(boolean adminPdf) throws IOException {
    	PDFGenerator pd;

		logger.info("the pdf is now getting generated");
		
		pd = new PDFGenerator(getPDFFilename(adminPdf), PDFGenerator.PDF_TYPE_ECLAIMS);
		
		generatePages(pd);
		logger.info("All the pages are now getting generated " + pd);
		PDPage page;
		int numberOfPages = pd.getDocument().getNumberOfPages();
		try {
			for (int i = 0; i < numberOfPages; i++) {
				page = pd.getDocument().getPage(i);
				pd.writeText(page, String.format("Page %d of %d", i + 1, numberOfPages), PAGE_MARGIN_LEFT,
						40);

				logger.info("Number of pages in pdf " + numberOfPages);
			}
		} catch (Exception e) {
			logger.error("Error occurred  generatePDF while creating " + e);
		}

		finally {

			try {
			   if(!adminPdf){
				 pd.protect(getPDFPassword());
				}
				pd.save();
				pd.close();


			} catch (Exception e) {
				logger.error("Exception occurred in saving block of generatePDF " + e);
			}

		}


     generateZipFile(adminPdf);


    }


	public String getClaimsType(){

        return claimsType;
    }

    protected void createReferenceNumber(String countNumber) {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
        String output = String.format("%s-%s-%s", getClaimsType(), formatter.format(new Date()), countNumber);
        this.setReferenceNumber(output);
    }

    /**
     * Save the state of the application to the database.
     * @return true if the save was successful
     */
    @Override
    public boolean save() {
        if (this.getStatus() == STATUS_COMPLETE) {
            Database db = new Database();
            Connection conn = db.getConnection();
            PreparedStatement stmt = null;
            try {
                prepareReferenceNumber(conn);
                existOrInsert(conn);
                String policy_list = String.join(",", getPolicyNumberList());
                String sql = "UPDATE eclaims SET `email_sent` = NOW(), ";
                Object[][] columns = new Object[][]{
                        new Object[]{"reference_num", "string", (getReferenceNumber())},
                        new Object[]{"policyowner_first_name", "string", (getPolicyownerFirstName())},
                        new Object[]{"policyowner_last_name", "string", (getPolicyownerLastName())},
                        new Object[]{"nric", "string", (getPolicyownerNRIC())},
                        new Object[]{"policy_no", "string", (policy_list)},
                        new Object[]{"status", "string", (String.valueOf(getStatus()))}
                };
                for (int i = 0; i < columns.length - 1; i++) {
                    sql += columns[i][0] + " = ?, ";
                }
                sql += columns[columns.length - 1][0] + " = ? ";
                sql += " WHERE id = ?";
                stmt = conn.prepareStatement(sql);
                for (int i = 0; i < columns.length; i++) {
                    switch (columns[i][1].toString()) {
                        case "string":
                            stmt.setObject((i + 1), columns[i][2]);
                            break;
                        case "boolean":
                            stmt.setObject((i + 1), columns[i][2]);
                            break;
                        case "float":
                            stmt.setObject((i + 1), columns[i][2]);
                            break;
                        case "date":
                            stmt.setDate((i + 1), (columns[i][2] != null ? new java.sql.Date(((Date) columns[i][2]).getTime()) : null));
                            break;
                    }
                }
                stmt.setString((columns.length + 1), id);
                stmt.executeUpdate();
            } catch (SQLException e) {
                ErrorHandler.handleError(this, e, "Error saving application");
                return false;
            } finally {
                db.closeAll(conn,stmt);
            }
        }
        return true;
    }

    private void prepareReferenceNumber(Connection conn) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT `count` FROM `eclaims_year` WHERE `type` = ? AND `year` = YEAR(NOW())")) {
            stmt.setString(1, this.getClaimsType());
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    // Recreate reference number
                    createReferenceNumber(String.format("%03d", rs.getInt(1)));
                    try (PreparedStatement stmt2 = conn.prepareStatement("UPDATE eclaims_year SET `count` = `count` + 1 WHERE `type` = ? AND `year` = YEAR(NOW())")) {
                        stmt2.setString(1, this.getClaimsType());
                        stmt2.executeUpdate();
                    }
                } else {
                    createReferenceNumber(String.format("%03d", 1));
                    try (PreparedStatement stmt2 = conn.prepareStatement("INSERT INTO eclaims_year (`year`, `count`, `type`) VALUES (YEAR(NOW()), 2, ?)")) {
                        stmt2.setString(1, this.getClaimsType());
                        stmt2.executeUpdate();
                    }
                }
            }
        }
    }

    private void existOrInsert(Connection conn) {
    try {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM eclaims WHERE id = ?")) {
            stmt.setString(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return;
                }
            }

        }
        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO eclaims (id, reference_num, type, date_created) VALUES (?, ?, ?, NOW())")) {
            stmt.setString(1, this.getId());
            stmt.setString(2, getReferenceNumber());
            stmt.setString(3, getClaimsType());
            stmt.executeUpdate();

        }
    } catch (Exception e) {
        logger.error("error "+e.getMessage());
    }
    }

    public void load(String id) throws SQLException {
        this.setId(id);
        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM eclaims WHERE id = ?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                populate(rs);
            }
        } finally {
            db.closeAll(conn,stmt, rs);
        }
    }

    public void populate(ResultSet rs) {
     try {
        setId(rs.getString("id"));
        setReferenceNumber(rs.getString("reference_num"));
        setPolicyownerFirstName(rs.getString("policyowner_first_name"));
        setPolicyownerLastName(rs.getString("policyowner_last_name"));
        setPolicyownerNRIC(rs.getString("nric"));
        setPolicyNumberList(rs.getString("policy_list").split(","));

        setEmailSent(rs.getDate("email_sent"));
         } catch (Exception e) {
         logger.error("error "+e.getMessage());

      }
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String[] getPolicyNumberList() {
        return policyNumberList;
    }

    public void setPolicyNumberList(String[] policyNumberList) {
        this.policyNumberList = policyNumberList;
    }

    public String getPolicyownerFirstName() {
        return policyownerFirstName;
    }

    public void setPolicyownerFirstName(String policyownerFirstName) {
        this.policyownerFirstName = policyownerFirstName;
    }

    public String getPolicyownerLastName() {
        return policyownerLastName;
    }

    public void setPolicyownerLastName(String policyownerLastName) {
        this.policyownerLastName = policyownerLastName;
    }

    public String getPolicyownerNRIC() { return policyownerNRIC; }

    public void setPolicyownerNRIC(String policyownerNRIC) { this.policyownerNRIC = policyownerNRIC; }

    public Date getPolicyownerBirthday() { return policyownerBirthday; }

    public void setPolicyownerBirthday(Date policyownerBirthday) {
        this.policyownerBirthday = policyownerBirthday;
    }
    

    public String getPolicyownerDay() {
		return policyownerDay;
	}

	public void setPolicyownerDay(String policyownerDay) {
		this.policyownerDay = policyownerDay;
	}

	public String getPolicyownerMonth() {
		return policyownerMonth;
	}

	public void setPolicyownerMonth(String policyownerMonth) {
		this.policyownerMonth = policyownerMonth;
	}

	public String getPolicyownerYear() {
		return policyownerYear;
	}

	public void setPolicyownerYear(String policyownerYear) {
		this.policyownerYear = policyownerYear;
	}

	public List<String> getPolicyownerPassport() {
        return policyownerPassport;
    }

    public void setPolicyownerPassport(List<String> list) {
        this.policyownerPassport = list;
    }

    public String getPolicyownerContactCountry() {
        return policyownerContactCountry;
    }

    public void setPolicyownerContactCountry(String policyownerContactCountry) {
        this.policyownerContactCountry = policyownerContactCountry;
    }

    public String getPolicyownerContactNumber() {
        return policyownerContactNumber;
    }

    public void setPolicyownerContactNumber(String policyownerContactNumber) {
        this.policyownerContactNumber = policyownerContactNumber;
    }

    public String getPolicyownerEmail() {
        return policyownerEmail;
    }

    public void setPolicyownerEmail(String policyownerEmail) {
        this.policyownerEmail = policyownerEmail;
    }

    public Boolean isPolicyownerLifeInsured() {
        return isPolicyownerLifeInsured;
    }

    public void setPolicyownerLifeInsured(Boolean policyownerLifeInsured) {
        isPolicyownerLifeInsured = policyownerLifeInsured;
    }

    public String getPolicyownerLifeInsuredFirstName() {
        return policyownerLifeInsuredFirstName;
    }

    public void setPolicyownerLifeInsuredFirstName(String policyownerLifeInsuredFirstName) {
        this.policyownerLifeInsuredFirstName = policyownerLifeInsuredFirstName;
    }

    public String getPolicyownerLifeInsuredLastName() {
        return policyownerLifeInsuredLastName;
    }

    public void setPolicyownerLifeInsuredLastName(String policyownerLifeInsuredLastName) {
        this.policyownerLifeInsuredLastName = policyownerLifeInsuredLastName;
    }

    public List<String> getPolicyownerLifeInsuredPassport() {
        return policyownerLifeInsuredPassport;
    }

    public void setPolicyownerLifeInsuredPassport(List<String> policyownerLifeInsuredPassport) {
        this.policyownerLifeInsuredPassport = policyownerLifeInsuredPassport;
    }

    public DeathClaimantPortion[] getClaimantList() {
        return claimantList;
    }

    public void setClaimantList(DeathClaimantPortion[] claimantList) {
        this.claimantList = claimantList;
    }

    public Boolean getBankrupt() {
        return bankrupt;
    }

    public void setBankrupt(Boolean bankrupt) {
        this.bankrupt = bankrupt;
    }

    public DeathBankruptPortion[] getBankruptList() {
        return bankruptList;
    }

    public void setBankruptList(DeathBankruptPortion[] bankruptList) {
        this.bankruptList = bankruptList;
    }

    public Boolean getOtherInsured() {
        return otherInsured;
    }

    public void setOtherInsured(Boolean otherInsured) {
        this.otherInsured = otherInsured;
    }

    public DeathOtherInsuredPortion[] getOtherInsuredList() {
        return otherInsuredList;
    }

    public void setOtherInsuredList(DeathOtherInsuredPortion[] otherInsuredList) {
        this.otherInsuredList = otherInsuredList;
    }

    public Integer getPayment() {
        return payment;
    }

    public void setPayment(Integer payment) {
        this.payment = payment;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankCountry() {
        return bankCountry;
    }

    public void setBankCountry(String bankCountry) {
        this.bankCountry = bankCountry;
    }

    public List<String> getAccountDocument() {
        return accountDocument;
    }

    public void setAccountDocument(List<String> list) {
        this.accountDocument = list;
    }

    public String getBankCity() {
        return bankCity;
    }

    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }

    public String getBankPostalCode() {
        return bankPostalCode;
    }

    public void setBankPostalCode(String bankPostalCode) {
        this.bankPostalCode = bankPostalCode;
    }

    public String getBankBuildingNumber() {
        return bankBuildingNumber;
    }

    public void setBankBuildingNumber(String bankBuildingNumber) {
        this.bankBuildingNumber = bankBuildingNumber;
    }

    public String getBankUnitNumber1() {
        return bankUnitNumber1;
    }

    public void setBankUnitNumber1(String bankUnitNumber1) {
        this.bankUnitNumber1 = bankUnitNumber1;
    }

    public String getBankUnitNumber2() {
        return bankUnitNumber2;
    }

    public void setBankUnitNumber2(String bankUnitNumber2) {
        this.bankUnitNumber2 = bankUnitNumber2;
    }

    public String getBankStreet1() {
        return bankStreet1;
    }

    public void setBankStreet1(String bankStreet1) {
        this.bankStreet1 = bankStreet1;
    }

    public String getBankStreet2() {
        return bankStreet2;
    }

    public void setBankStreet2(String bankStreet2) {
        this.bankStreet2 = bankStreet2;
    }

    public String getBankBuildingName() {
        return bankBuildingName;
    }

    public void setBankBuildingName(String bankBuildingName) {
        this.bankBuildingName = bankBuildingName;
    }

    public String getBankSwiftCode() {
        return bankSwiftCode;
    }

    public void setBankSwiftCode(String bankSwiftCode) {
        this.bankSwiftCode = bankSwiftCode;
    }

    public Date getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(Date emailSent) {
        this.emailSent = emailSent;
    }
   
    public Map<String, Long> getFileInfo() {
    return fileInfo;
    }
    
    


    public Map<String, String> getFileMapping() {
      return fileMapping;
    }

    /**
     * This method sets Map<String, String> with 
     * key file id and value as file name.
     * 
     * @param fileMapping     Updated Map<String,String>
     */
    public void setFileMapping(Map<String, String> fileMapping) {
      this.fileMapping = fileMapping;
    }


    /**
     * This method sets a Map<String, Long> containing 
     * file names and their sizes. 
     * 
     * @param fileInfo    Map<String, Long> of file names and sizes
     */

    public void setFileInfo(Map<String, Long> fileInfo) {
    this.fileInfo = fileInfo;
    }


    public abstract void sendEmail() throws MessagingException;

    /**
     * Sends the admin email.
     */
    public void sendEmail(String toAddress, int template, String attachmentPath) throws MessagingException {
        try {
            String subject = String.format("Eclaim Submission : %s", getReferenceNumber());
            String message = this.buildEmailMessage(template);
            EmailSender.sendEmail(toAddress, subject, message, getZipFilePath(false));


            String toAddresses = Config.getString("dreamcatcher.email.admin.eclaims");
            message = "This is a system generated message. Please do not reply to this email directly";
            EmailSender.sendEmail(toAddresses, subject, message, getZipFilePath(true));
        } catch (MessagingException ignored) {
    	    logger.error("error in sending email "+ignored);;
        }
    }

    public String getPDFPassword() {
        String nric = getPolicyownerNRIC();
        String birthday = new SimpleDateFormat("ddMMM").format(getPolicyownerBirthday()).toUpperCase();
        return String.format("%s%s", birthday, nric.substring(nric.length() - 6, nric.length() - 1));
    }

    public void sendSmsEmailPassword() throws MessagingException {
        String message = String.format("Your PDF password is: %s.", getPDFPassword());
        String phoneNumber = getPolicyownerContactCountry() + getPolicyownerContactNumber();
        EmailSender.sendSMS(message, phoneNumber, true);
    }

    private String buildEmailMessage(int template) {
    	DeathClaimantPortion[] claimantList=getClaimantList();
    	String email="";
    	int i=0;
    	for(DeathClaimantPortion claimant:claimantList){
    		if(i>0){
    			email+=",";
    		}
    		email+=claimant.getFirstName();
    		i++;
    	}
    	if(StringUtils.isEmpty(email)||StringUtils.equalsIgnoreCase(StringUtils.trim(email),"null")){
    		email=getPolicyownerFirstName();
    	}
        String body = "";

        switch(template){
            case EMAIL_TEMPLATE_DEATH:
                body = String.format("Dear %s,<br>" +
                        "<br>" +
                        "Thank you for submitting your claim. We are looking into it and will get back to you within 4 - 8 working days. <br>" +
                        "<br>" +
                        "Please note that we would require the certified true copies (by your lawyer or the notary public) of the uploaded documents to be mailed to us or verified by HSBC staff before the payment of the claim. <br>" +
                        "<br>" +
                        "Our mailing address is as follows:<br>" +
                        "Robinson Road Post Office P.O. BOX 1538<br>" +
                        "Singapore 903038<br>" +
                        "<br>" +
                        "If you have any queries, please contact our Customer Service Officers at (65) 6225 6111. You may also email us at lifeclaims.sgi@hsbc.com.sg. We will be pleased to assist.<br>" +
                        "<br>" +
                        "Sincerely,<br>" +
                        "HSBC Insurance Online Team<br>", email);
                break;
            case EMAIL_TEMPLATE_ILLNESS:
                body = String.format("Dear %s,<br>" +
                        "<br>" +
                        "Thank you for submitting your claim. We are looking into it and will get back to you within 8 working days. <br>" +
                        "<br>" +
                        "If you have any queries, please contact our Customer Service Officers at (65) 6225 6111. You may also email us at lifeclaims.sgi@hsbc.com.sg. We will be pleased to assist <br>" +
                        "<br>" +
                        "Sincerely,<br>" +
                        "HSBC Insurance Online Team<br>", email);
                break;
        }
        return body;
    }
    public String toString() {
        StringBuilder buf = new StringBuilder(getClass().getName());
        buf.append("[id=");
        buf.append(id);
        buf.append(']');
        return buf.toString();
    }
    /**
     * Current file list is save the field name and file name
     * For the pdf need to display the file name only
     * return the file name for input field and file name
     * @param fieldAndFilename
     * @return
     */
    public static String getFilename(String fieldAndFilename) {
        if (fieldAndFilename!=null ) {
            return fieldAndFilename.substring(fieldAndFilename.indexOf("-") + 1);
        }
        return null;
    }

    
    /**
     * Current file list is save the field name and file name
     * For the pdf need to display the file name only
     * return the file name for input field and file name list
     * @param fieldAndFilename
     * @return
     */
    public static String getFilename(List<String> fieldAndFilename) {
        if (fieldAndFilename!=null && fieldAndFilename.size()>0 ) {
            StringBuilder sb = new StringBuilder();
            for (String filename: fieldAndFilename) {
                sb.append(filename.substring(filename.indexOf("-") + 1)).append("<br>");
            }
            return sb.substring(0,sb.length() - 4);
        }
        return null;
    }

    
}
