package sg.com.hsbc.dreamcatcher.eclaims.tpdduetoaccident;

import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplication;
import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/eclaims/6-tpd-due-to-accident/0", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/6_tpd_due_to_accident/step0.jsp")
})
public class Step0Servlet extends BaseServlet {
    public float getCurrentStep() {
        return (float)0;
    }

    public boolean process(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");
        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    public void populateDataFromObject(HttpServletRequest request) {
        EClaimApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");
        request.setAttribute("data", data);
    }

    protected float getStep() {
        return (float)1;
    }
}
