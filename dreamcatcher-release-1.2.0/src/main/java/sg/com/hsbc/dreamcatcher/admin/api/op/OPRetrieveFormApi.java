package sg.com.hsbc.dreamcatcher.admin.api.op;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.com.hsbc.dreamcatcher.admin.AdminUtil;
import sg.com.hsbc.dreamcatcher.admin.entity.Applications;
import sg.com.hsbc.dreamcatcher.admin.service.AdminService;
import sg.com.hsbc.dreamcatcher.admin.service.impl.AdminOPServiceImpl;
import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.plans.onlineprotector.OnlineProtectorApplication;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/retrieve-form/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/retrieve_form.jsp")
})
public class OPRetrieveFormApi extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(OPRetrieveFormApi.class);

    private AdminService adminOPService;
    private Applications applications;
    private OnlineProtectorApplication application = null;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("online-protector retrieve form page");
        render(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> data = new HashMap<>();
        Map<String, String> errors;
        application = null;

        errors = new AdminUtil().validateEmailAndVerificationCode(request);
        if (!errors.isEmpty()) {
            render(request, response);
            return;
        }

        data.put(FieldConstants.RETRIEVAL_FORM_EMAIL, request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL));
        data.put(FieldConstants.RETRIEVAL_VERIFY_CODE, request.getParameter(FieldConstants.RETRIEVAL_VERIFY_CODE));

        applications = new Applications();
        adminOPService = new AdminOPServiceImpl();
        applications.setProductType(FieldConstants.OP_PRODUCT_TYPE);
        applications.setApplicantEmail(request.getParameter(FieldConstants.RETRIEVAL_FORM_EMAIL));
        applications.setDropoutPassword(request.getParameter(FieldConstants.RETRIEVAL_VERIFY_CODE));

        String applicationId = adminOPService.retrieveVerificationCode(applications);

        if (applicationId != null) {
            application = OnlineProtectorApplication.getById(applicationId);
        } else if (adminOPService.isImcompleteEmailExisted(applications.getApplicantEmail())) {
            errors.put("invalid_code", "invalid_code");
        } else {
            errors.put("form_not_found", "form_not_found");
        }

        if (application != null) {
            request.getSession().setAttribute(FieldConstants.OP_APPLICATION_SESSION_ATTRIBUTE_NAME, application.getId());
            response.sendRedirect("/our-plans/online-protector" + (application.getMaxStep() > 0 ? "/" + ((int)application.getMaxStep()) : ""));
            return;
        }

        request.setAttribute("errors", errors);
        request.setAttribute("data", data);

        render(request, response);
    }

    public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = getInitParameter("template");
        RequestDispatcher rd = request.getRequestDispatcher(test);
        rd.forward(request, response);
    }

}
