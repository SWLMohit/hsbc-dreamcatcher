package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/9", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step9.jsp")
})
public class Step9Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 9;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();

        sanitized = sanitizeAcceptDeclaration(request, sanitized);

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        errors = validateAcceptDeclaration(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateAcceptDeclaration(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.DECLARE_ACCEPT))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.DECLARE_ACCEPT, "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }
    
    private Map<String, Object> sanitizeAcceptDeclaration(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.DECLARE_ACCEPT);

        sanitized.put(FieldConstants.DECLARE_ACCEPT, input);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        request.setAttribute("data", data);
    }

}
