package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(urlPatterns = "/our-plans/online-protector/11", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step11.jsp")
})
public class Step11Servlet extends BaseServlet {

    public float getCurrentStep() {
        return 11;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        sanitized = sanitizeApplicantAddressPostalCode(request, sanitized);
        sanitized = sanitizeApplicantAddressHouse(request, sanitized);
        sanitized = sanitizeApplicantAddressUnit(request, sanitized);
        sanitized = sanitizeApplicantAddressStreet(request, sanitized);
        sanitized = sanitizeApplicantAddressBuilding(request, sanitized);

        if (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) {
            sanitized = sanitizeApplicantAddressRegistered(request, sanitized);
        }

        sanitized = sanitizeApplicantAddressPeriodStay(request, sanitized);

        if (sanitized.get("applicant_address_period_stay") != null && (int)sanitized.get("applicant_address_period_stay") < 60) {
            sanitized = sanitizeApplicantPreviousAddressCountry(request, sanitized);
        }

        sanitized = sanitizeApplicantAddressIsPermanent(request, sanitized);
        if (application.applicantIsPassHolder() ||
                (sanitized.get("applicant_address_permanent") != null && !((boolean)sanitized.get("applicant_address_permanent")))) {
            sanitized = sanitizeApplicantPermanentAddressLine1(request, sanitized);
            sanitized = sanitizeApplicantPermanentAddressLine2(request, sanitized);
            sanitized = sanitizeApplicantPermanentAddressPostalCode(request, sanitized);
            sanitized = sanitizeApplicantPermanentAddressCountry(request, sanitized);
        }

        sanitized = sanitizeApplicantTaxCountries(request, sanitized);
        sanitized = sanitizeApplicantAddressNoTaxReason(request, sanitized);

        sanitized = sanitizeApplicantIsHsbcCustomer(request, sanitized);
        sanitized = sanitizeApplicantIsPep(request, sanitized);
        sanitized = sanitizeApplicantIsBeneficialOwner(request, sanitized);

        if (sanitized.get("applicant_bo") != null && !((boolean)sanitized.get("applicant_bo"))) {
            sanitized = sanitizeBeneficalOwnerFirstName(request, sanitized);
            sanitized = sanitizeBeneficalOwnerLastName(request, sanitized);
            sanitized = sanitizeBeneficalOwnerNric(request, sanitized);
            sanitized = sanitizeBeneficalOwnerRelation(request, sanitized);
        }

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);

        errors = validateApplicantAddressPostalCode(data, errors);
        errors = validateApplicantAddressHouse(data, errors);
        errors = validateApplicantAddressUnit(data, errors);
        errors = validateApplicantAddressStreet(data, errors);
        errors = validateApplicantAddressBuilding(data, errors);

        if (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) {
            errors = validateApplicantAddressRegistered(data, errors);
        }

        errors = validateApplicantAddressPeriodStay(data, errors);

        if (data.get("applicant_address_period_stay") != null && (int)data.get("applicant_address_period_stay") < 60) {
            errors = validateApplicantPreviousAddressCountry(data, errors);
        }

        if (application.applicantIsPermanentResident() || application.applicantIsPassHolder()) {
            if (application.applicantIsPermanentResident()) {
                errors = validateApplicantAddressIsPermanent(data, errors);
            }
            if (application.applicantIsPassHolder() ||
                    (data.get("applicant_address_permanent") != null && !((boolean)data.get("applicant_address_permanent")))) {
                errors = validateApplicantPermanentAddressLine1(data, errors);
                errors = validateApplicantPermanentAddressLine2(data, errors);
                errors = validateApplicantPermanentAddressPostalCode(data, errors);
                errors = validateApplicantPermanentAddressCountry(data, errors);
            }
        }

        errors = validateApplicantTaxCountries(data, errors);
        //errors = validateApplicantAddressNoTaxReason(data, errors);
        if (application.getUnderwritingLifestyleQuestionsOptin()!= null && !application.getUnderwritingLifestyleQuestionsOptin().booleanValue() ||
                application.getUnderwritingLifestyleQuestionsOptin()!= null && application.getUnderwritingLifestyleProfile()=='0'){
        errors = validateApplicantIsHsbcCustomer(data, errors);
        }
        errors = validateApplicantIsPep(data, errors);
        errors = validateApplicantIsBeneficialOwner(data, errors);

        if (data.get("applicant_bo") != null && !((boolean)data.get("applicant_bo"))) {
            errors = validateBeneficalOwnerFirstName(data, errors);
            errors = validateBeneficalOwnerLastName(data, errors);
            errors = validateBeneficalOwnerNric(data, errors);
            errors = validateBeneficalOwnerRelation(data, errors);
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setApplicantAddressPostalCode(data.get("applicant_address_postal_code").toString());
        application.setApplicantAddressHouse(data.get("applicant_address_house").toString());

        if (data.get("applicant_address_unit") != null) {
            application.setApplicantAddressUnit(data.get("applicant_address_unit").toString());
        }

        application.setApplicantAddressStreet(data.get("applicant_address_street").toString());
        application.setApplicantAddressBuilding(data.get("applicant_address_building").toString());
        application.setApplicantAddressIsRegistered((Boolean)data.get("applicant_address_registered"));

        int periodStayYears = (data.get("applicant_address_period_stay_years") != null) ? Integer.parseInt(data.get("applicant_address_period_stay_years").toString()) : 0;
        int periodStayMonths = (data.get("applicant_address_period_stay_months") != null) ? Integer.parseInt(data.get("applicant_address_period_stay_months").toString()) : 0;
        application.setApplicantAddressPeriodOfStay((periodStayYears * 12) + periodStayMonths);

        if (data.get("applicant_previous_address_country") != null && !data.get("applicant_previous_address_country").toString().isEmpty()) {
            application.setApplicantPreviousAddressCountry(data.get("applicant_previous_address_country").toString());
        }

        if (data.get("applicant_address_permanent") != null && (boolean)data.get("applicant_address_permanent")) {
            application.setApplicantAddressIsPermanent(true);
            application.setApplicantPermanentAddressLine1(null);
            application.setApplicantPermanentAddressLine2(null);
            application.setApplicantPermanentAddressPostalCode(null);
            application.setApplicantPermanentAddressCountry(null);
        } else {
            application.setApplicantAddressIsPermanent(false);
            application.setApplicantPermanentAddressLine1(data.get("applicant_permanent_address_line1") != null ? data.get("applicant_permanent_address_line1").toString() : null);
            application.setApplicantPermanentAddressLine2(data.get("applicant_permanent_address_line2") != null ? data.get("applicant_permanent_address_line2").toString() : null);
            application.setApplicantPermanentAddressPostalCode(data.get("applicant_permanent_address_postal_code") != null ? data.get("applicant_permanent_address_postal_code").toString() : null);
            application.setApplicantPermanentAddressCountry(data.get("applicant_permanent_address_country") != null ? data.get("applicant_permanent_address_country").toString() : null);
        }

        if (data.get("applicant_tax_country1") != null && !data.get("applicant_tax_country1").toString().isEmpty()) {
            application.setApplicantTaxCountry1(data.get("applicant_tax_country1").toString());

            if (data.get("applicant_tax_country1_tin") != null) {
                application.setApplicantTaxCountry1Tin(data.get("applicant_tax_country1_tin").toString());
            } else {
                application.setApplicantTaxCountry1Tin(null);
            }

            if (data.get("applicant_tax_country1_tin_reason") != null) {
                application.setApplicantTaxCountry1NoTinReason(data.get("applicant_tax_country1_tin_reason").toString());
            } else {
                application.setApplicantTaxCountry1NoTinReason(null);
            }

            if (data.get("applicant_tax_country1_tin_reason_custom") != null) {
                application.setApplicantTaxCountry1NoTinReasonCustom(data.get("applicant_tax_country1_tin_reason_custom").toString());
            } else {
                application.setApplicantTaxCountry1NoTinReasonCustom(null);
            }
        } else {
            application.setApplicantTaxCountry1(null);
            application.setApplicantTaxCountry1Tin(null);
            application.setApplicantTaxCountry1NoTinReason(null);
            application.setApplicantTaxCountry1NoTinReasonCustom(null);
        }

        if (data.get("applicant_tax_country2") != null && !data.get("applicant_tax_country2").toString().isEmpty()) {
            application.setApplicantTaxCountry2(data.get("applicant_tax_country2").toString());

            if (data.get("applicant_tax_country2_tin") != null) {
                application.setApplicantTaxCountry2Tin(data.get("applicant_tax_country2_tin").toString());
            } else {
                application.setApplicantTaxCountry2Tin(null);
            }

            if (data.get("applicant_tax_country2_tin_reason") != null) {
                application.setApplicantTaxCountry2NoTinReason(data.get("applicant_tax_country2_tin_reason").toString());
            } else {
                application.setApplicantTaxCountry2NoTinReason(null);
            }

            if (data.get("applicant_tax_country2_tin_reason_custom") != null) {
                application.setApplicantTaxCountry2NoTinReasonCustom(data.get("applicant_tax_country2_tin_reason_custom").toString());
            } else {
                application.setApplicantTaxCountry2NoTinReasonCustom(null);
            }
        }
        else {
            application.setApplicantTaxCountry2(null);
            application.setApplicantTaxCountry2Tin(null);
            application.setApplicantTaxCountry2NoTinReason(null);
            application.setApplicantTaxCountry2NoTinReasonCustom(null);
        }

        if (data.get("applicant_tax_country3") != null && !data.get("applicant_tax_country3").toString().isEmpty()) {
            application.setApplicantTaxCountry3(data.get("applicant_tax_country3").toString());

            if (data.get("applicant_tax_country3_tin") != null) {
                application.setApplicantTaxCountry3Tin(data.get("applicant_tax_country3_tin").toString());
            } else {
                application.setApplicantTaxCountry3Tin(null);
            }

            if (data.get("applicant_tax_country3_tin_reason") != null) {
                application.setApplicantTaxCountry3NoTinReason(data.get("applicant_tax_country3_tin_reason").toString());
            } else {
                application.setApplicantTaxCountry3NoTinReason(null);
            }

            if (data.get("applicant_tax_country3_tin_reason_custom") != null) {
                application.setApplicantTaxCountry3NoTinReasonCustom(data.get("applicant_tax_country3_tin_reason_custom").toString());
            } else {
                application.setApplicantTaxCountry3NoTinReasonCustom(null);
            }
        } else {
            application.setApplicantTaxCountry3(null);
            application.setApplicantTaxCountry3Tin(null);
            application.setApplicantTaxCountry3NoTinReason(null);
            application.setApplicantTaxCountry3NoTinReasonCustom(null);
        }

        if (data.get("applicant_address_no_tax_reason") != null) {
            application.setApplicantAddressNoTaxReason(data.get("applicant_address_no_tax_reason").toString());
        }

        application.setApplicantIsHsbcCustomer((Boolean)data.get("applicant_hsbc_customer"));
        application.setApplicantIsPep((Boolean)data.get("applicant_pep"));
        application.setApplicantIsBeneficalOwner((Boolean)data.get("applicant_bo"));

        if (data.get("bo_first_name") != null) {
            application.setBeneficialOwnerFirstName(data.get("bo_first_name").toString());
        }

        if (data.get("bo_last_name") != null) {
            application.setBeneficialOwnerLastName(data.get("bo_last_name").toString());
        }

        if (data.get("bo_nric") != null) {
            application.setBeneficialOwnerNric(data.get("bo_nric").toString());
        }

        if (data.get("bo_relation") != null) {
            application.setBeneficialOwnerRelation(data.get("bo_relation").toString());
        }

        application.setMaxStep(getCurrentStep() + 1);
        return application.save();
    }

    private Map<String, String> validateApplicantAddressPostalCode(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_address_postal_code"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_address_postal_code", "Postal code is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressHouse(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_address_house"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_address_house", "House/Block number is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressUnit(Map<String, Object> data, Map<String, String> errors) {
        if (data.get("applicant_address_unit") != null) {
            Matcher matcher = Pattern.compile("^.+-.+$").matcher(data.get("applicant_address_unit").toString());
            if (!matcher.matches()) {
                errors.put("applicant_address_unit", "Unit number is invalid.");
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressStreet(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_address_street"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_address_street", "Street is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressBuilding(Map<String, Object> data, Map<String, String> errors) {
        return errors;
    }

    private Map<String, String> validateApplicantAddressRegistered(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_address_registered"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_address_registered", "Please indicate your selection");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressPeriodStay(Map<String, Object> data, Map<String, String> errors) {
        if (data.get("applicant_address_period_stay_years").toString().isEmpty() || data.get("applicant_address_period_stay_months").toString().isEmpty()) {
            errors.put("applicant_address_period_stay", "Period of stay is missing.");
        } else {
            Matcher matcher1 = Pattern.compile("^[0-9]{0,3}$").matcher(data.get("applicant_address_period_stay_years").toString());
            Matcher matcher2 = Pattern.compile("^[0-9]{0,3}$").matcher(data.get("applicant_address_period_stay_months").toString());
            if (!matcher1.matches() || !matcher2.matches()) {
                errors.put("applicant_address_period_stay", "Period of stay is invalid.");
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantPreviousAddressCountry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_previous_address_country"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_previous_address_country", "Country of address is missing.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantAddressIsPermanent(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_address_permanent"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_address_permanent", "Please indicate your selection.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantPermanentAddressLine1(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_permanent_address_line1"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_permanent_address_line1", "Address is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantPermanentAddressLine2(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_permanent_address_line2"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_permanent_address_line2", "Address is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantPermanentAddressPostalCode(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_permanent_address_postal_code"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_permanent_address_postal_code", "Postal code is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantPermanentAddressCountry(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_permanent_address_country"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_permanent_address_country", "Country is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantTaxCountries(Map<String, Object> data, Map<String, String> errors) {
       

    	for (int i = 1; i <= 3; i++) {
            String keyPrefix = "applicant_tax_country" + i;   
            if(i==1) {
            	if(data.get(keyPrefix).toString().isEmpty()) {
            		errors.put(keyPrefix + "_mandatory", "Please select a country");
            		//return errors;
            	}
            	 if (data.get(keyPrefix + "_tin_reason") == null) {
                     switch (Validator.validateTinNumber(data.get(keyPrefix + "_tin"))) {
                         case Validator.ERROR_STRING_EMPTY:
                             errors.put(keyPrefix + "_tin", "Taxpayer Identification Number is missing.");
                             break;
                         case Validator.ERROR_STRING_TOO_LONG:
                             errors.put(keyPrefix + "_tin", "Taxpayer Identification Number too long.");
                             break;
                         case Validator.ERROR_TIN_FORMAT_INVALID:
                             errors.put(keyPrefix + "_tin", "Taxpayer Identification Number is invalid.");
                             break;
                     }                 
            	 }
            }
            if (!data.get(keyPrefix).toString().isEmpty()) { //Removed the singapore ignore 
                if (data.get(keyPrefix + "_tin_reason") == null) {
                    switch (Validator.validateTinNumber(data.get(keyPrefix + "_tin"))) {
                        case Validator.ERROR_STRING_EMPTY:
                            errors.put(keyPrefix + "_tin", "Taxpayer Identification Number is missing.");
                            break;
                        case Validator.ERROR_STRING_TOO_LONG:
                            errors.put(keyPrefix + "_tin", "Taxpayer Identification Number too long.");
                            break;
                        case Validator.ERROR_TIN_FORMAT_INVALID:
                            errors.put(keyPrefix + "_tin", "Taxpayer Identification Number is invalid.");
                            break;
                    }                 
                } else {
                    if (data.get(keyPrefix + "_tin_reason").toString().equalsIgnoreCase("B")) {
                        if (data.get(keyPrefix + "_tin_reason_custom") == null || data.get(keyPrefix + "_tin_reason_custom").toString().isEmpty()) {
                            errors.put(keyPrefix + "_tin_reason_custom", "Please provide an explanation.");
                        }
                    }
                }
            }
            
        }
    	
        return errors;
    }

    private Map<String, String> validateApplicantAddressNoTaxReason(Map<String, Object> data, Map<String, String> errors) {
        boolean residentialAddressIsTaxCountry = false;
        for (int i = 1; i <= 3; i++) {
            String keyPrefix = "applicant_tax_country" + i;
            if (!data.get(keyPrefix).toString().isEmpty() && data.get(keyPrefix).toString().equalsIgnoreCase("Singapore")) {
                residentialAddressIsTaxCountry = true;
            }
        }

        if (!residentialAddressIsTaxCountry) {
            switch (Validator.validateString(data.get("applicant_address_no_tax_reason"))) {
                case Validator.ERROR_STRING_EMPTY:
                    errors.put("applicant_address_no_tax_reason", "Reason is missing or invalid.");
                    break;
            }
        }

        return errors;
    }

    private Map<String, String> validateApplicantIsHsbcCustomer(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_hsbc_customer"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_hsbc_customer", "Please indicate your selection.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantIsPep(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_pep"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_pep", "Please indicate your selection.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateApplicantIsBeneficialOwner(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("applicant_bo"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("applicant_bo", "Please indicate your selection.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateBeneficalOwnerFirstName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("bo_first_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("bo_first_name", "Given name is missing or invalid.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("bo_first_name", "Given name is too long.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("bo_first_name", "Given name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateBeneficalOwnerLastName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get("bo_last_name"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("bo_last_name", "Surname is missing or invalid.");
                break;
            case Validator.ERROR_STRING_TOO_LONG:
                errors.put("bo_last_name", "Surname is too long.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put("bo_first_name", "Given name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateBeneficalOwnerNric(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("bo_nric"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("bo_nric", "NRIC/Passport number is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateBeneficalOwnerRelation(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("bo_relation"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("bo_relation", "Relationship is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeApplicantAddressPostalCode(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_address_postal_code");
        sanitized.put("applicant_address_postal_code", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressHouse(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_address_house");
        sanitized.put("applicant_address_house", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressUnit(HttpServletRequest request, Map<String, Object> sanitized) {
        String unitFloor = request.getParameter("applicant_address_unit_floor");
        String unitNum = request.getParameter("applicant_address_unit_num");

        sanitized.put("applicant_address_unit_floor", unitFloor);
        sanitized.put("applicant_address_unit_num", unitNum);

        if (!unitFloor.isEmpty() || !unitNum.isEmpty()) {
            sanitized.put("applicant_address_unit", String.format("%s-%s", unitFloor, unitNum));
        } else {
            sanitized.put("applicant_address_unit", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressStreet(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_address_street");
        sanitized.put("applicant_address_street", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressBuilding(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_address_building");
        sanitized.put("applicant_address_building", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressRegistered(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_address_registered");
        if (input != null) {
            sanitized.put("applicant_address_registered", input.equals("Y"));
        } else {
            sanitized.put("applicant_address_registered", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressPeriodStay(HttpServletRequest request, Map<String, Object> sanitized) {
        String years = request.getParameter("applicant_address_period_stay_years");
        String months = request.getParameter("applicant_address_period_stay_months");

        sanitized.put("applicant_address_period_stay_years", years);
        sanitized.put("applicant_address_period_stay_months", months);

        int periodStayYears = 0;
        int periodStayMonths = 0;
        if (!years.isEmpty()) {
            try {
                periodStayYears = Integer.parseInt(years);
            } catch (Exception e) {
                periodStayYears = 0;
            }
        }
        if (!months.isEmpty()) {
            try {
                periodStayMonths = Integer.parseInt(months);
            } catch (Exception e) {
                periodStayMonths = 0;
            }
        }
        sanitized.put("applicant_address_period_stay", (periodStayYears * 12 + periodStayMonths));

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPreviousAddressCountry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_previous_address_country");
        sanitized.put("applicant_previous_address_country", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressIsPermanent(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_address_permanent");
        if (input != null) {
            sanitized.put("applicant_address_permanent", input.equals("Y"));
        } else {
            sanitized.put("applicant_address_permanent", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPermanentAddressLine1(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_permanent_address_line1");
        sanitized.put("applicant_permanent_address_line1", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPermanentAddressLine2(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_permanent_address_line2");
        sanitized.put("applicant_permanent_address_line2", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPermanentAddressPostalCode(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_permanent_address_postal_code");
        sanitized.put("applicant_permanent_address_postal_code", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantPermanentAddressCountry(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_permanent_address_country");
        sanitized.put("applicant_permanent_address_country", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantTaxCountries(HttpServletRequest request, Map<String, Object> sanitized) {
        String key;
        String country;
        for (int i = 1; i <= 3; i++) {
            key = "applicant_tax_country" + i;
            sanitized.put(key, request.getParameter(key));
         //   if (!"Singapore".equals(request.getParameter(key))) {
            sanitized.put(key + "_tin", request.getParameter(key + "_tin"));
            sanitized.put(key + "_tin_reason", request.getParameter(key + "_tin_reason"));
            sanitized.put(key + "_tin_reason_custom", request.getParameter(key + "_tin_reason_custom"));
       // }
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantAddressNoTaxReason(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_address_no_tax_reason");
        sanitized.put("applicant_address_no_tax_reason", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIsHsbcCustomer(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_hsbc_customer");
        if (input != null) {
            sanitized.put("applicant_hsbc_customer", input.equals("Y"));
        } else {
            sanitized.put("applicant_hsbc_customer", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIsPep(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_pep");
        if (input != null) {
            sanitized.put("applicant_pep", input.equals("Y"));
        } else {
            sanitized.put("applicant_pep", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeApplicantIsBeneficialOwner(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("applicant_bo");
        if (input != null) {
            sanitized.put("applicant_bo", input.equals("Y"));
        } else {
            sanitized.put("applicant_bo", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeBeneficalOwnerFirstName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("bo_first_name");
        sanitized.put("bo_first_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeBeneficalOwnerLastName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("bo_last_name");
        sanitized.put("bo_last_name", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeBeneficalOwnerNric(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("bo_nric");
        sanitized.put("bo_nric", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeBeneficalOwnerRelation(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("bo_relation");
        sanitized.put("bo_relation", input);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("applicant_address_postal_code", application.getApplicantAddressPostalCode());
        data.put("applicant_address_house", application.getApplicantAddressHouse());

        if (application.getApplicantAddressUnit() != null && !application.getApplicantAddressUnit().isEmpty()) {
            String[] parts = application.getApplicantAddressUnit().split("-");
            data.put("applicant_address_unit", application.getApplicantAddressUnit());
            data.put("applicant_address_unit_floor", parts[0]);
            data.put("applicant_address_unit_num", parts[1]);
        }

        data.put("applicant_address_street", application.getApplicantAddressStreet());
        data.put("applicant_address_building", application.getApplicantAddressBuilding());
        data.put("applicant_address_registered", application.getApplicantAddressIsRegistered());

        Integer addressPeriodOfStay = application.getApplicantAddressPeriodOfStay();
        if (addressPeriodOfStay != null) {
            data.put("applicant_address_period_stay_years", (int)Math.floor(addressPeriodOfStay / 12));
            data.put("applicant_address_period_stay_months", addressPeriodOfStay % 12);
        }

        data.put("applicant_previous_address_country", application.getApplicantPreviousAddressCountry());
        data.put("applicant_address_permanent", application.getApplicantAddressIsPermanent());
        data.put("applicant_permanent_address_line1", application.getApplicantPermanentAddressLine1());
        data.put("applicant_permanent_address_line2", application.getApplicantPermanentAddressLine2());
        data.put("applicant_permanent_address_postal_code", application.getApplicantPermanentAddressPostalCode());
        data.put("applicant_permanent_address_country", application.getApplicantPermanentAddressCountry());

        data.put("applicant_tax_country1", application.getApplicantTaxCountry1());
        data.put("applicant_tax_country1_tin", application.getApplicantTaxCountry1Tin());
        data.put("applicant_tax_country1_tin_reason", application.getApplicantTaxCountry1NoTinReason());
        data.put("applicant_tax_country1_tin_reason_custom", application.getApplicantTaxCountry1NoTinReasonCustom());
        data.put("applicant_tax_country2", application.getApplicantTaxCountry2());
        data.put("applicant_tax_country2_tin", application.getApplicantTaxCountry2Tin());
        data.put("applicant_tax_country2_tin_reason", application.getApplicantTaxCountry2NoTinReason());
        data.put("applicant_tax_country2_tin_reason_custom", application.getApplicantTaxCountry2NoTinReasonCustom());
        data.put("applicant_tax_country3", application.getApplicantTaxCountry3());
        data.put("applicant_tax_country3_tin", application.getApplicantTaxCountry3Tin());
        data.put("applicant_tax_country3_tin_reason", application.getApplicantTaxCountry3NoTinReason());
        data.put("applicant_tax_country3_tin_reason_custom", application.getApplicantTaxCountry3NoTinReasonCustom());

        data.put("applicant_address_no_tax_reason", application.getApplicantAddressNoTaxReason());
        data.put("applicant_hsbc_customer", application.getApplicantIsHsbcCustomer());
        data.put("applicant_pep", application.getApplicantIsPep());
        data.put("applicant_bo", application.getApplicantIsBeneficalOwner());

        data.put("bo_first_name", application.getBeneficialOwnerFirstName());
        data.put("bo_last_name", application.getBeneficialOwnerLastName());
        data.put("bo_nric", application.getBeneficialOwnerNric());
        data.put("bo_relation", application.getBeneficialOwnerRelation());
        data.put("uw_lifestyle_profile", application.getUnderwritingLifestyleProfile());
        data.put("uw_lifestyle_questions_optin", application.getUnderwritingLifestyleQuestionsOptin());
        request.setAttribute("data", data);
    }

}
