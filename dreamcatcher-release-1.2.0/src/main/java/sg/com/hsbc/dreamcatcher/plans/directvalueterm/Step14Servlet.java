package sg.com.hsbc.dreamcatcher.plans.directvalueterm;

import sg.com.hsbc.dreamcatcher.helpers.*;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.utils.ProductCalculation;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplicationJob;

@WebServlet(urlPatterns = "/our-plans/direct-value-term/14", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/directvalueterm/step14.jsp")
})
@MultipartConfig
public class Step14Servlet extends BaseServlet {
  private final static Logger logger = Logger.getLogger(Step14Servlet.class);
    public float getCurrentStep() {
        return 14;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        DirectValueTermApplication application = getApplication(request);
        sanitized = sanitizeApplicantIdvUpload(request, sanitized);
		if ((!application.applicantIsSingaporean() && !application.applicantIsPermanentResident())) { // JIRA 661
			sanitized = sanitizeApplicantSecondIdvUpload(request, sanitized);
			 /*sanitizing the relevant back upload as per JIRA HSBCIDCU-984 */
		      sanitized = sanitizeDocumentUpload(request, sanitized,
		         FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD,
		         FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD_DOCUMENT_TAG);
		}
        sanitized = sanitizeApplicantAddressUpload(request, sanitized);
        sanitized = sanitizeCcType(request, sanitized);
        sanitized = sanitizeCcName(request, sanitized);
        sanitized = sanitizeCcNum(request, sanitized);
        sanitized = sanitizeCcExpiry(request, sanitized);
        sanitized = sanitizeAcceptDeclaration(request, sanitized);
        sanitized = sanitizeMailOption(request, sanitized);

        /** upload fields for HSBCIDCU-820 starts here **/
        if (application.getApplicantMultipleNationalities()&&!application.getApplicantNationality2().isEmpty()) { //added second nationality documents as a part of HSBCIDCU-820
          sanitized = sanitizeApplicantNationalityTwoUpload(request, sanitized);
        }
        if (application.getApplicantMultipleNationalities()&&!application.getApplicantNationality3().isEmpty()) { //added third nationality documents as a part of HSBCIDCU-820
          sanitized = sanitizeApplicantNationalityThreeUpload(request, sanitized);
        }
        
        if(!application.applicantIsSingaporean() && !(application.applicantIsPermanentResident()
            &&application.getApplicantAddressIsPermanent()) ) {
          sanitized = sanitizeApplicantPermanentAddressUpload(request, sanitized);
        }
        if(!application.getApplicantIsBeneficalOwner()) { //added beneficial owner documents as a part of HSBCIDCU-820
          sanitized = sanitizeBeneficalIdvUpload(request, sanitized);
        }
        /** upload fields for HSBCIDCU-820 ends here **/


        if ((application.applicantIsSingaporean() || application.applicantIsPermanentResident())) { // JIRA HSBCIDCU-950
        	sanitized = sanitizeApplicantBackNricUpload(request, sanitized);
        }
        
        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        Map<String,String> docUpload=new HashMap<>();
        DirectValueTermApplication application = getApplication(request);

        errors = validateCcType(data, errors);
        errors = validateCcName(data, errors);
        errors = validateCcNum(data, errors);
        errors = validateCcExpiry(data, errors);
        errors = validateApplicantIdvUpload(data, errors);

        if(errors.containsKey("applicant_idv_previous_upload")){
        	docUpload.put("applicant_idv_previous_upload", "true");
        	errors.remove("applicant_idv_previous_upload");
        }

       
        
        /* JIRA 661 validateEmailAndVerificationCode when the applicant residency is singaporean or singaporean PR*/
        if ((!application.applicantIsSingaporean() 
            && !application.applicantIsPermanentResident())) { 
        	 errors = validateApplicantSecondIdvUpload(data, errors);
				if(errors.containsKey("applicant_idv_previous_upload_2")){
                docUpload.put("applicant_idv_previous_upload_2", "true");
                errors.remove("applicant_idv_previous_upload_2");
                }
			
				/*validating applicant relevant pass as per JIRA HSBCIDCU-984*/
	             errors = validateDocumentUpload(data, errors,
	                FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD,
	                FieldConstants.EMPTY_FILE_UPLOAD_MESSAGE);
	             if(errors.containsKey(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous")){
	                 docUpload.put(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous", "true");
	                 errors.remove(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous");
	                }
				
             }
       
            errors = validateApplicantAddressUpload(data, errors);
            if(errors.containsKey("applicant_address_previous_upload")){
            	docUpload.put("applicant_address_previous_upload", "true");
            	errors.remove("applicant_address_previous_upload");
            }
    
        errors = validateAcceptDeclaration(data, errors);
        /* validating applicant nationality 2 documents as a part of HSBCIDCU-820 */
        if (application.getApplicantMultipleNationalities()
            && !application.getApplicantNationality2().isEmpty()) { 
          errors =validateApplicantNationalityTwoIdvUpload(data, errors);
           if(errors.containsKey("applicant_nationality_two_idv_previous_upload")){
        	   docUpload.put("applicant_nationality_two_idv_previous_upload", "true");
           	   errors.remove("applicant_nationality_two_idv_previous_upload");
           }
          
        }
        
        /* validating applicant nationality 3 documents as a part of HSBCIDCU-820 */                          
        if (application.getApplicantMultipleNationalities()
            && !application.getApplicantNationality3().isEmpty()) {                                    
          errors = validateApplicantNationalityThreeIdvUpload(data, errors);
          if(errors.containsKey("applicant_nationality_three_idv_previous_upload")){
       	   docUpload.put("applicant_nationality_three_idv_previous_upload", "true");
          	   errors.remove("applicant_nationality_three_idv_previous_upload");
          }
          
        }
        if(!application.applicantIsSingaporean() && !(application.applicantIsPermanentResident()
            &&application.getApplicantAddressIsPermanent()) ) {
          errors =validatePermanentApplicantAddressUpload(data, errors);
             if(errors.containsKey("applicant_permanent_address_previous_upload")){
          	       docUpload.put("applicant_permanent_address_previous_upload", "true");
             	   errors.remove("applicant_permanent_address_previous_upload");
             }
          
        }
        
        /* validating beneficial owner documents as a part of HSBCIDCU-820 */
        if(!application.getApplicantIsBeneficalOwner()) { 
          errors = validateBeneficalIdvUpload(data, errors);
          if(errors.containsKey("beneficial_idv_previous_upload")){
     	       docUpload.put("beneficial_idv_previous_upload", "true");
        	   errors.remove("beneficial_idv_previous_upload");
         }
         
        }
        
        /* validating back of nric as a part of JIRA HSBCIDCU-950 */
        if (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) { 
          errors = validateApplicantIdvUploadBack(data, errors);
		  if(data.containsKey("applicant_idv_back_nric_previous_upload")) {
            docUpload.put("applicant_idv_back_nric_previous_upload", "true");
            errors.remove("applicant_idv_back_nric_previous_upload");
          }
        }

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);
        request.setAttribute("docUpload", docUpload);
        if (errors.size() > 0) {
            Map premiums = ProductCalculation.getPremiumAtAge(application.calculateAgeNextBirthday(application.getApplicantDob()), application);
            double premiumModal = (double)premiums.get("modalPremium");
            double premiumAnnual = premiumModal * application.getPaymentMode();

            premiums = ProductCalculation.getCriticalIllnessRiderPremiumAtAge(application.calculateAgeNextBirthday(application.getApplicantDob()), application);
            double riderPremiumModal = (double)premiums.get("modalPremium");
            double riderPremiumAnnual = riderPremiumModal * application.getPaymentMode();

            DecimalFormat df = new DecimalFormat("S$#,###.00");
            DecimalFormat df1 = new DecimalFormat("####.00");
            String premiumModalString=df1.format(premiumModal);
            float premiumModalTwo=Float.parseFloat(premiumModalString);
            
            data.put(FieldConstants.SUMMARY_MODAL, df.format(premiumModal));
            
            String premiumAnnualString=df1.format(premiumAnnual);
            float premiumAnnualTwo=Float.parseFloat(premiumAnnualString);
            
            data.put(FieldConstants.SUMMARY_ANNUALLY, df.format(premiumAnnual));
            
            String ciRiderPremiumModalString=df1.format(riderPremiumModal);
            float ciRiderPremiumModalTwo=Float.parseFloat(ciRiderPremiumModalString);
            
            data.put(FieldConstants.SUMMARY_CI_MODAL, df.format(riderPremiumModal));
            
            String ciRiderPremiumAnnualString=df1.format(riderPremiumAnnual);
            float ciRiderPremiumAnnualTwo=Float.parseFloat(ciRiderPremiumAnnualString);
            
            data.put(FieldConstants.SUMMARY_CI_ANNUALLY, df.format(riderPremiumAnnual));
            
            data.put(FieldConstants.SUMMARY_TOTAL_MODAL, df.format(premiumModalTwo + ciRiderPremiumModalTwo));
            data.put(FieldConstants.SUMMARY_TOTAL_ANNUALLY, df.format(premiumAnnualTwo + ciRiderPremiumAnnualTwo));
           

            request.setAttribute("data", data);
        }

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");
        application.setCreditCardType(data.get(FieldConstants.CC_TYPE).toString());
        application.setCreditCardName(data.get(FieldConstants.CC_NAME).toString());
        application.setCreditCardNum(data.get(FieldConstants.CC_NUM).toString());
        application.setCreditCardExpiry(String.format("%s/%s", data.get(FieldConstants.CC_EXPIRY_MONTH).toString(), data.get(FieldConstants.CC_EXPIRY_YEAR).toString()));
        application.setMaxStep(getCurrentStep() + 1);
        if(request.getSession().getAttribute("marketingConsent")!=null){
        	boolean marketing=(boolean) request.getSession().getAttribute("marketingConsent");
        	application.setMarketingConsent( marketing );
        }
        application.setMailOption(Integer.parseInt(data.get("mail_option").toString()));
        if (application.complete()) {
            ExecutorService threadPool = (ExecutorService) getServletConfig().getServletContext().getAttribute("threadPool");
            threadPool.execute(new DirectValueTermApplicationJob(application));
            return true;
        }
        return false;
        }

    public void handleSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("/our-plans/direct-value-term/complete");
    }

    private Map<String, String> validateCcType(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.CC_TYPE))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.CC_TYPE, "Your payment type is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateCcName(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateName(data.get(FieldConstants.CC_NAME))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.CC_NAME, "Your name is missing.");
                break;
            case Validator.ERROR_NAME_FORMAT_INVALID:
                errors.put(FieldConstants.CC_NAME, "Your name contains invalid characters.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateCcNum(Map<String, Object> data, Map<String, String> errors) {
        boolean hasError = false;
        switch (Validator.validateString(data.get(FieldConstants.CC_NUM))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.CC_NUM, "Your credit card number is missing.");
                hasError = true;
                break;
        }

        if (!hasError) {
            switch (Validator.validateSingaporeCreditCardNum(data.get(FieldConstants.CC_NUM))) {
                case Validator.ERROR_CC_NUM_INVALID:
                    errors.put(FieldConstants.CC_NUM, "Your credit card number is invalid.");
                    break;
                case Validator.ERROR_CC_NUM_NOT_LOCAL:
                    errors.put(FieldConstants.CC_NUM, "Only locally issued credit cards or debit cards are allowed.");
                    break;
            }
        }

        return errors;
    }

    private Map<String, String> validateCcExpiry(Map<String, Object> data, Map<String, String> errors) {
        if (data.get(FieldConstants.CC_EXPIRY_MONTH) == null && data.get(FieldConstants.CC_EXPIRY_YEAR) == null) {
            errors.put("cc_expiry", "Expiry date is missing.");
        } else {
            if (data.get(FieldConstants.CC_EXPIRY_MONTH) == null || data.get(FieldConstants.CC_EXPIRY_YEAR) == null) {
                errors.put("cc_expiry", "Expiry date is invalid.");
            } else {
                switch (Validator.validateDateExpiry(data.get(FieldConstants.CC_EXPIRY_MONTH), data.get(FieldConstants.CC_EXPIRY_YEAR))) {
                    case Validator.ERROR_DATE_EMPTY:
                        errors.put("cc_expiry", "Expiry date is missing.");
                        break;
                    case Validator.ERROR_DATE_INVALID:
                        errors.put("cc_expiry", "Expiry date is invalid.");
                        break;
                }
            }
        }

        return errors;
    }

    /**
     * This method checks the http request object (sanitized hashmap) for the
     * existence of the element given by the identifier string. if the element does
     * not exist, an error given by errorMessage string will be added to a map of
     * error messages. JIRA HSBCIDCU-984
     * 
     * @param data          Hashmap of santized http request data. 
     * @param errors        Hashmap of error messages to be displayed to the end user.
     * @param identifier    String of element identifier.
     * @param errorMessage  String of error message.
     * @return              Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateDocumentUpload(Map<String, Object> data,
        Map<String, String> errors, String identifier, String errorMessage) {
      if (StringUtils.isNotBlank(identifier)) {
        if (!(boolean) data.get(identifier)) {
          errors.put(identifier, errorMessage);
        }else if(data.containsKey(identifier+"_previous")&&(boolean)data.get(identifier+"_previous")){
        	errors.put(identifier+"_previous","true");
        }
      }
      return errors;
    }
    

    private Map<String, String> validateApplicantIdvUpload(Map<String, Object> data, Map<String, String> errors) {
        if (!(boolean)data.get(FieldConstants.APPLICANT_IDV_UPLOAD)) {
            errors.put(FieldConstants.APPLICANT_IDV_UPLOAD, "Please upload a file.");
        }else if(data.containsKey("applicant_idv_previous_upload")&&(boolean)data.get("applicant_idv_previous_upload")){
        	errors.put("applicant_idv_previous_upload","true");
        }

        return errors;
    }
    
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element APPLICANT_SECOND_IDV_UPLOAD.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantSecondIdvUpload(Map<String, Object> data, Map<String, String> errors) {//JIRA 661
    	if (!(boolean)data.get(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD)) {
            errors.put(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD, "Please upload a file.");
        }else if(data.containsKey("applicant_idv_previous_upload_2")&&(boolean)data.get("applicant_idv_previous_upload_2")){
            errors.put("applicant_idv_previous_upload_2","true");
        }

        return errors;
    }

    
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element APPLICANT_ADDRESS_UPLOAD.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantAddressUpload(Map<String, Object> data, Map<String, String> errors) {
        if (!(boolean)data.get(FieldConstants.APPLICANT_ADDRESS_UPLOAD)) {
            errors.put(FieldConstants.APPLICANT_ADDRESS_UPLOAD, "Please upload a file.");
        }else if(data.containsKey(FieldConstants.APPLICANT_ADDRESS_UPLOAD)&&(boolean)data.get("applicant_address_previous_upload")){
        	errors.put("applicant_address_previous_upload","true");
        }

        return errors;
    }
    
    
    /**
     * This method checks the http request object (sanitized hashmap) for the existence of the element applicant_nationality_two_idv_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantNationalityTwoIdvUpload(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get("applicant_nationality_two_idv_upload")) {
          errors.put("applicant_nationality_two_idv_upload", "Please upload a file.");

      }else if(data.containsKey("applicant_nationality_two_idv_previous_upload")&&(boolean)data.get("applicant_nationality_two_idv_previous_upload")){
          errors.put("applicant_nationality_two_idv_previous_upload","true");
      }

      return errors;
  }
  
   
    /**
     * This method checks the http request object (sanitized hashmap) for the
     *  existence of the element applicant_nationality_three_idv_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateApplicantNationalityThreeIdvUpload(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get("applicant_nationality_three_idv_upload")) {
          errors.put("applicant_nationality_three_idv_upload", "Please upload a file.");
      }else if(data.containsKey("applicant_nationality_three_idv_previous_upload")&&(boolean)data.get("applicant_nationality_three_idv_previous_upload")){
          errors.put("applicant_nationality_three_idv_previous_upload","true");
      }

      return errors;
  }
    /**
     * This method checks the http request object (sanitized hashmap) for the 
     * existence of the element applicant_permanent_address_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validatePermanentApplicantAddressUpload(Map<String, Object> data, Map<String, String> errors) {
      if ((!(boolean)data.get("applicant_permanent_address_upload"))) {
          errors.put("applicant_permanent_address_upload", "Please upload a file.");
      }else if(data.containsKey("applicant_permanent_address_previous_upload")&&(boolean)data.get("applicant_permanent_address_previous_upload")){
          errors.put("applicant_permanent_address_previous_upload","true");
      }

      return errors;
  }
    /**
     * This method checks the existence of element "benefical_idv_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeBeneficalIdvUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("beneficial_idv_upload", false);
      try {
          Part part = request.getPart("beneficial_idv_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-BeneficalOwnerIDV."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while((bytesRead = is.read(buffer)) !=-1){
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("beneficial_idv_upload", true);
              sanitized.put("beneficial_idv_previous_upload",true);
          }else{
          	
          	ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
      	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
      		
          		   String idvField=String.format("%s/%s-BeneficalOwnerIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                   File idvFile=new File(idvField);
                   if(idvFile.exists()){
                	   sanitized.put("beneficial_idv_upload", true);
                 		sanitized.put("beneficial_idv_previous_upload",true);
                       break;
                   }
      		
      	   }

          	
          }
      } catch (Exception e) {
          logger.error("Exception occurred in while uploading Benefical documents"+e);
      }

      return sanitized;
  }
    /**
     * This method checks the http request object (sanitized hashmap) for the
     * existence of the element benefical_idv_upload.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateBeneficalIdvUpload(Map<String, Object> data, Map<String, String> errors) {
      if (!(boolean)data.get("beneficial_idv_upload")) {
          errors.put("beneficial_idv_upload", "Please upload a file.");
      }else if(data.containsKey("beneficial_idv_previous_upload")&&(boolean)data.get("beneficial_idv_previous_upload")){
          errors.put("beneficial_idv_previous_upload","true");
      }

      return errors;
  }

    /** 
    * This method checks the http request object (sanitized hashmap) for the existence of the element APPLICANT_IDV_BACK_NRIC_UPLOAD.
    * if the element does not exist, an error will be added to a map of error messages.
    *      
    * @param data     Hashmap of santized http request data. 
    * @param errors   Hashmap of error messages to be displayed to the end user.
    * @return         Updated Hashmap of error messages to be displayed to the end user.   
    */
   private Map<String, String> validateApplicantIdvUploadBack(Map<String, Object> data, Map<String, String> errors) {
     if (!(boolean)data.get(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD)) {
         errors.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, "Please upload a file.");
     }else if(data.containsKey("applicant_idv_back_nric_previous_upload")&&(boolean)data.get("applicant_idv_back_nric_previous_upload")){
         errors.put("applicant_idv_back_nric_previous_upload","true");
     }
     
     return errors;
  }
    private boolean doesNeedAddressVerification(DirectValueTermApplication application) {
        return
                (application.applicantIsSingaporean() || application.applicantIsPermanentResident()) // only singaporeans
                && !application.getApplicantAddressIsRegistered();  // address is NOT on NRIC

    }

    private Map<String, String> validateAcceptDeclaration(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get(FieldConstants.DECLARE_ACCEPT))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put(FieldConstants.DECLARE_ACCEPT, "You must accept before proceeding with the application.");
                break;
        }

        return errors;
    }

    private Map<String, Object> sanitizeAcceptDeclaration(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.DECLARE_ACCEPT);

        sanitized.put(FieldConstants.DECLARE_ACCEPT, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeCcType(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.CC_TYPE);
        sanitized.put(FieldConstants.CC_TYPE, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeCcName(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.CC_NAME);
        sanitized.put(FieldConstants.CC_NAME, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeCcNum(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter(FieldConstants.CC_NUM);
        sanitized.put(FieldConstants.CC_NUM, input);

        return sanitized;
    }

    private Map<String, Object> sanitizeCcExpiry(HttpServletRequest request, Map<String, Object> sanitized) {
        String month = request.getParameter(FieldConstants.CC_EXPIRY_MONTH);
        String year = request.getParameter(FieldConstants.CC_EXPIRY_YEAR);

        sanitized.put(FieldConstants.CC_EXPIRY_MONTH, month);
        sanitized.put(FieldConstants.CC_EXPIRY_YEAR, year);

        return sanitized;
    }

	/**
	 * This method checks the existence of element APPLICANT_IDV_UPLOAD, validates 
	 * the format to be JPEG and  saves it to the temp folder.
	 * It will update the hashmap(santized) with true when the element is saved. 
	 * 
	 * @param request    Hashmap of http request data.    
	 * @param sanitized  Hashmap of santized request data
	 * @return           Hashmap of updated sanitized request data
	 */

    private Map<String, Object> sanitizeApplicantIdvUpload(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized.put(FieldConstants.APPLICANT_IDV_UPLOAD, false);
        InputStream is =null;
        OutputStream os=null;
        try {
            Part part = request.getPart(FieldConstants.APPLICANT_IDV_UPLOAD);
            String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
            if (part != null && part.getSize() > 0) {
                is= part.getInputStream();
                if ( !Validator.isJPEG(is,contentType) ) {
                    is.close();
                    sanitized.put(FieldConstants.APPLICANT_IDV_UPLOAD, false);
                    return sanitized;
                }
                //close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                os = new FileOutputStream(String.format("%s/%s-ApplicantIDV."+contentType,
                        System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId()));
                byte[] buffer = new byte[1024];
                int bytesRead;
                while((bytesRead = is.read(buffer)) !=-1){
                    os.write(buffer, 0, bytesRead);
                }
                is.close();
                os.flush();
                os.close();
                sanitized.put(FieldConstants.APPLICANT_IDV_UPLOAD, true);
                sanitized.put("applicant_idv_previous_upload",true);
            }else{	
            	 ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
           	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
           		
               		   String idvField=String.format("%s/%s-ApplicantIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                        File idvFile=new File(idvField);
                        if(idvFile.exists()){
                        	sanitized.put("applicant_idv_upload", true);
                    		sanitized.put("applicant_idv_previous_upload",true);
                            break;
                        }
           		
           	   }
            	
            }
        } catch (Exception e) {          
           logger.error( "Exception occurred in Step14Servlet sanitizeApplicantIdvUpload method " + e);     
    } finally {
      try {
        if (is != null) {
          is.close();
        }
      } catch (IOException e) {
          logger.error("Exception occurred while closing the inputstream in Step14Servlet sanitizeApplicantIdvUpload method" + e);
          ErrorHandler.handleError(this, e);
      }
      try {
        if (os != null) {
          os.close();
        }
      } catch (IOException e) {
            logger.error("Exception occurred while closing the outputstream in Step14Servlet sanitizeApplicantIdvUpload method" + e);
            ErrorHandler.handleError(this, e);
      }
    }
        return sanitized;
    }
  

/**
 * This method checks the existence of element given by identifier , validates 
 * the format to be JPEG and  saves it to the temp folder along with appending 
 * a text for identifying the document given in document tag.
 * It will update the hashmap(santized) with true when the element is saved. 
 * 
 * JIRA HSBCIDCU-984
 * 
 * 
 * @param request       Hashmap of http request data.
 * @param sanitized     Hashmap of santized request data.
 * @param identifier    String identifier of element.
 * @param documentTag   String value of trailing text in file name. 
 * @return              Hashmap of updated sanitized request data
 */
  private Map<String, Object> sanitizeDocumentUpload(HttpServletRequest request,
      Map<String, Object> sanitized, String identifier, String documentTag) {
    sanitized.put(identifier, false);
    InputStream is = null;
    OutputStream os = null;

    try {
      if (StringUtils.isNotBlank(identifier) && StringUtils.isNotBlank(documentTag)) {
        Part part = request.getPart(identifier);
        if (part != null && part.getSize() > 0) {
          is = part.getInputStream();
          String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
          if (!Validator.isJPEG(is,contentType)) {
            is.close();
            sanitized.put(identifier, false);
            return sanitized;
          }
          // close and reset the input stream, so that the stream start from the beginning
          is.close();
          is = part.getInputStream();
          os = new FileOutputStream(String.format("%s/%s-%s."+contentType,
              System.getProperty("java.io.tmpdir"), getApplication(request).getId(), documentTag));
          byte[] buffer = new byte[1024];
          int bytesRead;
          while ((bytesRead = is.read(buffer)) != -1) {
            os.write(buffer, 0, bytesRead);
          }
          is.close();
          os.flush();
          os.close();
          sanitized.put(identifier, true);
          sanitized.put(identifier+"_previous",true);
        }else{

        	 ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
       	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
       		
           		   String idvField=String.format("%s/%s-%s."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId(), documentTag);
                    File idvFile=new File(idvField);
                    if(idvFile.exists()){
                    	sanitized.put(identifier, true);
                		sanitized.put(identifier+"_previous",true);
                        break;
                    }
       		
       	   }
        	
        }
      }
    } catch (Exception e) {
      logger.error("Exception occurred while closing sanitizeDocumentUpload inputstream " + e);
    } finally {
      if (is != null) {
        try {
          is.close();
        } catch (IOException e) {
          logger.error("Exception occurred while closing sanitizeDocumentUpload inputstream " + e);
        }
      }
      if (os != null) {
        try {
          os.close();
        } catch (IOException e) {
          logger.error("Exception occurred while closing sanitizeDocumentUpload outputstream " + e);
        }
      }
    }


    return sanitized;
  }


    /**
     * This method checks the existence of element "APPLICANT_ADDRESS_UPLOAD", validates 
     * the format to be JPEG and saves it to the temp folder.
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantAddressUpload(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized.put(FieldConstants.APPLICANT_ADDRESS_UPLOAD, false);
        InputStream is = null;
        OutputStream os = null;
        try {
            Part part = request.getPart(FieldConstants.APPLICANT_ADDRESS_UPLOAD);
            if (part != null && part.getSize() > 0) {
                 is = part.getInputStream();
                 String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
                 if (!Validator.isJPEG(is,contentType)) {
                    is.close();
                    sanitized.put(FieldConstants.APPLICANT_IDV_UPLOAD, false);
                    return sanitized;
                }
                //close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                os = new FileOutputStream(String.format("%s/%s-ApplicantAddress."+contentType,
                        System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId()));
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                is.close();
                os.flush();
                os.close();
                sanitized.put(FieldConstants.APPLICANT_ADDRESS_UPLOAD, true);
                sanitized.put("applicant_address_previous_upload",true);
            }else{    	
            	 ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
           	     for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
           		
               		   String idvField=String.format("%s/%s-ApplicantAddress."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                        File idvFile=new File(idvField);
                        if(idvFile.exists()){
                            sanitized.put(FieldConstants.APPLICANT_ADDRESS_UPLOAD, true);
                            sanitized.put("applicant_address_previous_upload",true);
                            break;
                        }
           		
           	   }
            	
           
        }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (is != null) {
                  is.close();
                }

              } catch (IOException e) {
                logger.error("Exception occurred while closing the inputstream in Step14Servlet sanitizeApplicantAddressUpload method"+ e);
                  ErrorHandler.handleError(this, e);
              }
              try {
                if (os != null) {
                  os.close();
                }
              } catch (IOException e) {
                logger.error("Exception occurred while closing the outputstream in Step14Servlet sanitizeApplicantAddressUpload method" + e);
                  ErrorHandler.handleError(this, e);
              }
            }

    return sanitized;
  }


  

    
    
    /**
     * This method checks the existence of element APPLICANT_SECOND_IDV_UPLOAD, validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
  private Map<String, Object> sanitizeApplicantSecondIdvUpload(HttpServletRequest request,
      Map<String, Object> sanitized) { // HSBCIDCU-661 addition of relevant pass field
    sanitized.put(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD, false);
    InputStream is = null;
    OutputStream os = null;
    try {
      Part part = request.getPart(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD);
      if (part != null && part.getSize() > 0) {
        is = part.getInputStream();
        String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
        if (!Validator.isJPEG(is,contentType)) {
          is.close();
          return sanitized;
        }
        // close and reset the input stream, so that the stream start from the beginning
        is.close();
        is = part.getInputStream();
        os = new FileOutputStream(String.format("%s/%s-ApplicantIDV2."+contentType,
            System.getProperty(FieldConstants.JAVA_TEMP_DIR), getApplication(request).getId()));
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = is.read(buffer)) != -1) {
          os.write(buffer, 0, bytesRead);
        }
        is.close();
        os.flush();
        os.close();
        sanitized.put(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD, true);
        sanitized.put("applicant_idv_previous_upload_2",true);
      }else{
        ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
  	    for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
  		
      		   String idvField=String.format("%s/%s-ApplicantIDV2."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
               File idvFile=new File(idvField);
               if(idvFile.exists()){
                   sanitized.put(FieldConstants.APPLICANT_SECOND_IDV_UPLOAD, true);
                   sanitized.put("applicant_idv_previous_upload_2",true);
                   break;
               }
  		
  	   }
 
      }
    } catch (Exception e) {
      logger.error(
          "Exception occurred in Step14Servlet sanitizeApplicantSecondIdvUpload method " + e);
    } finally {
      try {
        if (is != null) {
          is.close();
        }

      } catch (IOException e) {
        logger.error(
            "Exception occurred while closing the inputstream in Step14Servlet sanitizeApplicantSecondIdvUpload method"
                + e);
      }
      try {
        if (os != null) {
          os.close();
        }
      } catch (IOException e) {
        logger.error(
            "Exception occurred while closing the outputstream in Step14Servlet sanitizeApplicantSecondIdvUpload method"
                + e);
      }
    }
    return sanitized;
  }

    /**
     * This method checks the existence of element "applicant_nationality_two_idv_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantNationalityTwoUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("applicant_nationality_two_idv_upload", false);
      try {
          Part part = request.getPart("applicant_nationality_two_idv_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-ApplicantNationalityTwoIDV."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while((bytesRead = is.read(buffer)) !=-1){
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("applicant_nationality_two_idv_upload", true);
              sanitized.put("applicant_nationality_two_idv_previous_upload",true);
          }else{
     

            ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
      	    for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
      		
	      		   String idvField=String.format("%s/%s-ApplicantNationalityTwoIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	               File idvFile=new File(idvField);
	               if(idvFile.exists()){
	                   sanitized.put("applicant_nationality_two_idv_upload", true);
	                   sanitized.put("applicant_nationality_two_idv_previous_upload",true);
	                   break;
	               }
      		
      	   }
            
     
          }
      } catch (Exception e) {
          logger.error("Exception occurred in while uploading second nationality documents"+e);
      }

      return sanitized;
  }
    /**
     * This method checks the existence of element "applicant_nationality_three_idv_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantNationalityThreeUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("applicant_nationality_three_idv_upload", false);
      try {
          Part part = request.getPart("applicant_nationality_three_idv_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-ApplicantNationalityThreeIDV."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while((bytesRead = is.read(buffer)) !=-1){
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("applicant_nationality_three_idv_upload", true);
              sanitized.put("applicant_nationality_three_idv_previous_upload",true);
          }else{
     
            ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
      	    for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
      		
	      		   String idvField=String.format("%s/%s-ApplicantNationalityThreeIDV."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
	               File idvFile=new File(idvField);
	               if(idvFile.exists()){
	                   sanitized.put("applicant_nationality_three_idv_upload", true);
	                   sanitized.put("applicant_nationality_three_idv_previous_upload",true);
	                   break;
	               }
      		
      	   }

     
          }
      } catch (Exception e) {
          logger.error("Exception occurred in while uploading applicant's third nationality documents"+e);
      }

      return sanitized;
  }
    /**
     * This method checks the existence of element "applicant_permanent_address_upload", validates 
     * the format to be JPEG and  saves it to the temp folder
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantPermanentAddressUpload(HttpServletRequest request, Map<String, Object> sanitized) {
      sanitized.put("applicant_permanent_address_upload", false);
      try {
          Part part = request.getPart("applicant_permanent_address_upload");
          if (part != null && part.getSize() > 0) {
              InputStream is = part.getInputStream();
              String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
              if ( !Validator.isJPEG(is,contentType) ) {
                  is.close();
                  return sanitized;
              }
              //close and reset the input stream, so that the stream start from the beginning
              is.close();
              is = part.getInputStream();
              OutputStream os = new FileOutputStream(String.format("%s/%s-ApplicantPermanentAddress."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
              byte[] buffer = new byte[1024];
              int bytesRead;
              while ((bytesRead = is.read(buffer)) != -1) {
                  os.write(buffer, 0, bytesRead);
              }
              is.close();
              os.flush();
              os.close();
              sanitized.put("applicant_permanent_address_upload", true);
              sanitized.put("applicant_permanent_address_previous_upload",true);
          }else{

        	  ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
        	  for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
        		
        		 String idvField=String.format("%s/%s-ApplicantPermanentAddress."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                 
                 File idvFile=new File(idvField);
                 if(idvFile.exists()){
                     sanitized.put("applicant_permanent_address_upload", true);
                     sanitized.put("applicant_permanent_address_previous_upload",true);
                     break;
                 }
        		
        	}
           
            
     
          }
      } catch (Exception e) {
          e.printStackTrace();
      }

      return sanitized;
    }
 
    private Map<String, Object> sanitizeMailOption(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("mail_option");
        if(input != null){
        sanitized.put("mail_option", input);
        }
        else
        {
          sanitized.put("mail_option", 0);
        }
        return sanitized;
    }
    
    public void populateDataFromObject(HttpServletRequest request) {
        DirectValueTermApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");
        Map<String,String>  docUpload=new HashMap<String,String>();

      ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
  	   for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
  		String ext=iterator.next();
  	if(!docUpload.containsKey(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous")){
        String docField=String.format("%s/%s-%s."+ext,
                System.getProperty("java.io.tmpdir"), getApplication(request).getId(), FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD_DOCUMENT_TAG);
    	File docFile=new File(docField);
    	if(docFile.exists()){
    		docUpload.put(FieldConstants.APPLICANT_RELEVANT_PASS_BACK_UPLOAD+"_previous","true");
    	}
  	}
  	if(!docUpload.containsKey("applicant_idv_previous_upload")){ 
        String idvField=String.format("%s/%s-ApplicantIDV."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
    	File idvFile=new File(idvField);
    	if(idvFile.exists()){
    		docUpload.put("applicant_idv_previous_upload", "true");
    		
    	}
  	}
  	if(!docUpload.containsKey("applicant_address_previous_upload")){ 
    	String applicantAddressField=String.format("%s/%s-ApplicantAddress."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
    	File applicantAddressFile=new File(applicantAddressField);
    	
    	if(applicantAddressFile.exists()){
     	   docUpload.put("applicant_address_previous_upload", "true");
     	}
  	}
  	if(!docUpload.containsKey("applicant_idv_back_nric_previous_upload")){ 
    	String idvBackField=String.format("%s/%s-ApplicantIDVBackNric."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
        File idvBackFile=new File(idvBackField);
        if(idvBackFile.exists()){
            docUpload.put("applicant_idv_back_nric_previous_upload","true");
        } 
  	}
  	if(!docUpload.containsKey("applicant_permanent_address_previous_upload")){ 
        String permanentField=String.format("%s/%s-ApplicantPermanentAddress."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
        File permanentFile=new File(permanentField);
        if(permanentFile.exists()){
            docUpload.put("applicant_permanent_address_previous_upload","true");
        }
  	}
  	if(!docUpload.containsKey("applicant_nationality_three_idv_previous_upload")){ 
        String nationalityThree=String.format("%s/%s-ApplicantNationalityThreeIDV."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
        File nationalityThreeFile=new File(nationalityThree);
        if(nationalityThreeFile.exists()){
            docUpload.put("applicant_nationality_three_idv_previous_upload","true");
        }
  	} 
  	if(!docUpload.containsKey("applicant_nationality_two_idv_previous_upload")){ 
        String nationalityTwo=String.format("%s/%s-ApplicantNationalityTwoIDV."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
        File nationalityTwoFile=new File(nationalityTwo);
        if(nationalityTwoFile.exists()){
            docUpload.put("applicant_nationality_two_idv_previous_upload","true");
        }
  	} 
  	if(!docUpload.containsKey("applicant_idv_previous_upload_2")){ 
        String applicantIdv2=String.format("%s/%s-ApplicantIDV2."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
        File applicantIdv2File=new File(applicantIdv2);
        if(applicantIdv2File.exists()){
            docUpload.put("applicant_idv_previous_upload_2","true");
        }
  	}
  	if(!docUpload.containsKey("beneficial_idv_previous_upload")){ 
        String beneficialField=String.format("%s/%s-BeneficalOwnerIDV."+ext, System.getProperty("java.io.tmpdir"), getApplication(request).getId());
      	File beneficialFile=new File(beneficialField);
      	if(beneficialFile.exists()){
      		docUpload.put("beneficial_idv_previous_upload","true");
      	}
  	  }
  	 } 

    	request.setAttribute("docUpload", docUpload);
        
        
        data.put(FieldConstants.CC_TYPE, application.getCreditCardType());
        data.put(FieldConstants.CC_NAME, application.getCreditCardName());
        data.put(FieldConstants.CC_NUM, application.getCreditCardNum());
        String ccExpiry = application.getCreditCardExpiry();
        if (ccExpiry != null && !ccExpiry.isEmpty()) {
            data.put(FieldConstants.CC_EXPIRY_MONTH, ccExpiry.split("/")[0]);
            data.put(FieldConstants.CC_EXPIRY_YEAR, ccExpiry.split("/")[1]);
        }

        Map premiums = ProductCalculation.getPremiumAtAge(application.calculateAgeNextBirthday(application.getApplicantDob()), application);
        double premiumModal = (double)premiums.get("modalPremium");
        double premiumAnnual = premiumModal * application.getPaymentMode();
        
        double riderPremiumModal=0.0;
        double riderPremiumAnnual=0.0;
       if(application.getIncludeCriticalIllnessRider()){
        premiums = ProductCalculation.getCriticalIllnessRiderPremiumAtAge(application.calculateAgeNextBirthday(application.getApplicantDob()), application);
        riderPremiumModal= (double)premiums.get("modalPremium");
        riderPremiumAnnual = riderPremiumModal * application.getPaymentMode();
       }
        DecimalFormat df = new DecimalFormat("S$#,###.00");
        DecimalFormat df1 = new DecimalFormat("####.00");
        String premiumModalString=df1.format(premiumModal);
        float premiumModalTwo=Float.parseFloat(premiumModalString);
        
        data.put(FieldConstants.SUMMARY_MODAL, df.format(premiumModal));
        
        String premiumAnnualString=df1.format(premiumAnnual);
        float premiumAnnualTwo=Float.parseFloat(premiumAnnualString);
        
        data.put(FieldConstants.SUMMARY_ANNUALLY, df.format(premiumAnnual));
        
        String ciRiderPremiumModalString=df1.format(riderPremiumModal);
        float ciRiderPremiumModalTwo=Float.parseFloat(ciRiderPremiumModalString);
        
        data.put(FieldConstants.SUMMARY_CI_MODAL, df.format(riderPremiumModal));
        
        String ciRiderPremiumAnnualString=df1.format(riderPremiumAnnual);
        float ciRiderPremiumAnnualTwo=Float.parseFloat(ciRiderPremiumAnnualString);
        
        data.put(FieldConstants.SUMMARY_CI_ANNUALLY, df.format(riderPremiumAnnual));
        
        data.put(FieldConstants.SUMMARY_TOTAL_MODAL, df.format(premiumModalTwo + ciRiderPremiumModalTwo));
        data.put(FieldConstants.SUMMARY_TOTAL_ANNUALLY, df.format(premiumAnnualTwo + ciRiderPremiumAnnualTwo));
        data.put(FieldConstants.APPLICANT_ADDRESS_REGISTERED, application.getApplicantAddressIsRegistered());

        request.setAttribute("data", data);
    }
    
    /**
     * This method checks whether user has uploaded a copy of the back of nric, validates
     * the format to be JPEG and saves it to the temp folder.
     * It will update the hashmap(santized) with true when the element is saved. 
     * 
     * @param request    Hashmap of http request data.    
     * @param sanitized  Hashmap of santized request data
     * @return           Hashmap of updated sanitized request data
     */
    private Map<String, Object> sanitizeApplicantBackNricUpload(HttpServletRequest request, Map<String, Object> sanitized) {
        sanitized.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, false);
        try {
            Part part = request.getPart(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD);
            if (part != null && part.getSize() > 0) {
                InputStream is = part.getInputStream();
                String contentType=FilenameUtils.getExtension(part.getSubmittedFileName());
                if ( !Validator.isJPEG(is,contentType) ) {
                    is.close();
                    sanitized.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, false);
                    return sanitized;
                }
                //close and reset the input stream, so that the stream start from the beginning
                is.close();
                is = part.getInputStream();
                OutputStream os = new FileOutputStream(String.format("%s/%s-ApplicantIDVBackNric."+contentType, System.getProperty("java.io.tmpdir"), getApplication(request).getId()));
                try {
                	byte[] buffer = new byte[1024];
                	int bytesRead;
	                while((bytesRead = is.read(buffer)) !=-1){
	                    os.write(buffer, 0, bytesRead);
	                }
                } finally {
                	is.close();
                	os.flush();
                	os.close();
                }
               sanitized.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, true);
               sanitized.put("applicant_idv_back_nric_previous_upload",true);
            }else{
                
                
              ArrayList<String> extentsion=new ArrayList<>(Arrays.asList("jpg","jpeg","png","pdf"));
          	  for (Iterator<String> iterator = extentsion.iterator(); iterator.hasNext();) {
          		
          		 String idvField=String.format("%s/%s-ApplicantIDVBackNric."+iterator.next(), System.getProperty("java.io.tmpdir"), getApplication(request).getId());
                   
                   File idvFile=new File(idvField);
                   if(idvFile.exists()){
                       sanitized.put(FieldConstants.APPLICANT_IDV_BACK_NRIC_UPLOAD, true);
                       sanitized.put("applicant_idv_back_nric_previous_upload",true);
                       break;
                   }
          		
          	}

            }
        } catch (Exception e) {
            logger.error("sanitizeApplicantBackNricUpload:",e);
        }

        return sanitized;
    }

}
