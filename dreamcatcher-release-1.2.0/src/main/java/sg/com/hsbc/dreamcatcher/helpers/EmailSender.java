package sg.com.hsbc.dreamcatcher.helpers;

import java.util.Date;
import java.util.Properties;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.http.SdkHttpMetadata;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.BaseFormServlet;
import sg.com.hsbc.dreamcatcher.plans.directvalueterm.DirectValueTermApplication;
import sg.com.hsbc.dreamcatcher.plans.onlineprotector.OnlineProtectorApplication;

/**
 * Static methods to send email and SMS.
 */
public final class EmailSender {
	private static Logger logger = LoggerFactory.getLogger(EmailSender.class);

	public static void sendEmail(String toAddresses, String subject, String body, String attachmentPath) throws MessagingException {

        String host = Config.getString("dreamcatcher.smtp.host");
        String port = Config.getString("dreamcatcher.smtp.port");
        String userName = Config.getString("dreamcatcher.smtp.username");
        String password = Config.getString("dreamcatcher.smtp.password");
        String fromAddress = Config.getString("dreamcatcher.email.from");

        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };

        Session session = Session.getInstance(properties, auth);
        Message message = new MimeMessage(session);
		Address[] to = InternetAddress.parse(toAddresses);
        message.setFrom(new InternetAddress(fromAddress));
		message.setRecipients(Message.RecipientType.TO, to);
        message.setSubject(subject);
        message.setSentDate(new Date());

		if (attachmentPath != null) {
        	MimeMultipart multipart = new MimeMultipart("mixed");

			if(StringUtils.isNotBlank(attachmentPath)){
				BodyPart attachmentBodyPart = new MimeBodyPart();
				DataSource file = new FileDataSource(attachmentPath);
				attachmentBodyPart.setDataHandler(new DataHandler(file));
				attachmentBodyPart.setFileName(file.getName());
				multipart.addBodyPart(attachmentBodyPart);
			}

			BodyPart htmlBodyPart = new MimeBodyPart();
			htmlBodyPart.setContent(body , "text/html");
			multipart.addBodyPart(htmlBodyPart);

			message.setContent(multipart);
		} else {
			// NB text/plain
			message.setText(body);
		}
        Transport.send(message);
    }
	public static void sendSMS(String message, String phoneNumber, boolean HSBCInsure) throws MessagingException {
		AmazonSNS sns = AmazonSNSClient.builder().withRegion(Regions.AP_SOUTHEAST_1).build();
		Map<String, MessageAttributeValue> attributes = new HashMap<String, MessageAttributeValue>();
		if (HSBCInsure) {
			attributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
					.withStringValue("HSBCInsure")
					.withDataType("String"));
		}
		PublishResult result = sns.publish(new PublishRequest()
				.withMessage(message)
				.withPhoneNumber(phoneNumber)
				.withMessageAttributes(attributes));
		SdkHttpMetadata http = result.getSdkHttpMetadata();
		if (http != null) {
			int sc = http.getHttpStatusCode();
			if (sc != HttpServletResponse.SC_OK) {
				throw new MessagingException("AmazonSNS.publish returned status code " + sc);
			}
		}
	}

	public static void sendEmailUsingApplication(ApplicationForm application, String appType, BaseFormServlet baseFormServlet) {
		 String EMAIL_SUBJECT = "Thank you for visiting HSBC Insurance";
		 String EMAIL_CONTENT = "<table style='width:550px'><tr><td>Subject </td><td>: Feedback</td></tr>"+
					"<tr><td>Feedback Experience</td><td> : %s</td></tr>"+
				    "<tr><td>Feedback Ease </td><td>: %s</td></tr>"+
					"<tr><td>Feedback Recommend </td><td>: %s</td></tr>"+
				    "<tr><td>Feedback Norecommend Reason </td><td>: %s</td></tr>"+
					"<tr><td>Feedback Improve </td><td>: %s</td></tr>"+
				    "<tr><td>Feedback Additional</td><td> : %s</td></tr></table>";

		try{
			int feedbackExperience;
			int feedbackEase;
			boolean feedbackRecommend;
			String noFeedbackReason;
			String feedbackImprove;
			String feedbackAdditional;
			if(StringUtils.equalsIgnoreCase(appType, "OP")){
				OnlineProtectorApplication applicationCast=(OnlineProtectorApplication)application;
				feedbackExperience=applicationCast.getFeedbackExperience() == null ? 1 : applicationCast.getFeedbackExperience();
				feedbackEase=applicationCast.getFeedbackEase() == null ? 1 : applicationCast.getFeedbackEase();
				feedbackRecommend=applicationCast.getFeedbackRecommend() == null ? false : applicationCast.getFeedbackRecommend();
				noFeedbackReason=StringUtils.isBlank(applicationCast.getFeedbackNoRecommendReason()) ? "NA"
						: applicationCast.getFeedbackNoRecommendReason();
				feedbackImprove=StringUtils.isBlank(applicationCast.getFeedbackImprove()) ? "NA" : applicationCast.getFeedbackImprove();
				feedbackAdditional=StringUtils.isBlank(applicationCast.getFeedbackAdditional()) ? "NA"
						: applicationCast.getFeedbackAdditional();

			}else{
				DirectValueTermApplication applicationCast=(DirectValueTermApplication)application;
				feedbackExperience=applicationCast.getFeedbackExperience() == null ? 1 : applicationCast.getFeedbackExperience();
				feedbackEase=applicationCast.getFeedbackEase() == null ? 1 : applicationCast.getFeedbackEase();
				feedbackRecommend=applicationCast.getFeedbackRecommend() == null ? false : applicationCast.getFeedbackRecommend();
				noFeedbackReason=StringUtils.isBlank(applicationCast.getFeedbackNoRecommendReason()) ? "NA"
						: applicationCast.getFeedbackNoRecommendReason();
				feedbackImprove=StringUtils.isBlank(applicationCast.getFeedbackImprove()) ? "NA" : applicationCast.getFeedbackImprove();
				feedbackAdditional=StringUtils.isBlank(applicationCast.getFeedbackAdditional()) ? "NA"
						: applicationCast.getFeedbackAdditional();
			}
			String toEmail = Config.getString("dreamcatcher.email.feedback");
			String body=String.format(EMAIL_CONTENT,
					        feedbackExperience,
							feedbackEase,
							feedbackRecommend,
							noFeedbackReason,
							feedbackImprove,
							feedbackAdditional);
			ExecutorService threadPool = (ExecutorService) baseFormServlet.getServletConfig().getServletContext().getAttribute("threadPool");
			threadPool.execute(new EmailSenderThread(toEmail, EMAIL_SUBJECT, body, ""));

		}catch(Exception e){
			logger.error("error is thrown in sending the email "+e);
		}
	}
}
