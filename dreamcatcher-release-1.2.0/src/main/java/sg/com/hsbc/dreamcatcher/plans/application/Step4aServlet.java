package sg.com.hsbc.dreamcatcher.plans.application;

import sg.com.hsbc.dreamcatcher.plans.retirement.BaseFormServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = "/our-plans/onlineinvestor-application-step4a/", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/jsp/view/plans/application/step3a.jsp")
})
public class Step4aServlet extends BaseFormServlet {
}
