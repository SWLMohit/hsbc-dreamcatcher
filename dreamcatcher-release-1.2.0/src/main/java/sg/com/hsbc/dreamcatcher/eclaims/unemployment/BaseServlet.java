package sg.com.hsbc.dreamcatcher.eclaims.unemployment;

import sg.com.hsbc.dreamcatcher.ApplicationForm;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimBaseServlet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;

public abstract class BaseServlet extends EClaimBaseServlet {
    static String FORM_TITLE = "eClaims - Unemployment - Make a Claim";
    public static String APPLICATION_SESSION_ATTRIBUTE_NAME = "EClaimUnemployment";

    public String getFormTitle() {
        return BaseServlet.FORM_TITLE;
    }

    public String getApplicationSessionAttributeName() {
        return BaseServlet.APPLICATION_SESSION_ATTRIBUTE_NAME;
    }

    public String getBaseFormUrl() { return "/eclaims/7-unemployment"; }

    public ApplicationForm getById(String id) throws SQLException { return UnemploymentApplication.getById(id); }

    public UnemploymentApplication getApplication(HttpServletRequest request) {
        return (UnemploymentApplication)super.getApplication(request);
    }
}
