package sg.com.hsbc.dreamcatcher.plans.onlineprotector;

import sg.com.hsbc.dreamcatcher.helpers.FieldConstants;
import sg.com.hsbc.dreamcatcher.helpers.Validator;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/our-plans/online-protector/4a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/ourplans/onlineprotector/step4a.jsp")
})
public class Step4aServlet extends BaseServlet {

    public float getCurrentStep() {
        return 4.1f;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        sanitized = sanitizeInsuredGender(request, sanitized);
        sanitized = sanitizeInsuredNationality(request, sanitized);
        sanitized = sanitizeInsuredMultipleNationalities(request, sanitized);
        sanitized = sanitizeInsuredNationality2(request, sanitized);
        sanitized = sanitizeInsuredNationality3(request, sanitized);
        sanitized = sanitizeInsuredNric(request, sanitized);
        sanitized = sanitizeInsuredBirthCountry(request, sanitized); //Added Country of birth dropdown as a part of 892

        return sanitized;
    }

    public boolean validate(HttpServletRequest request) {
        Map<String, Object> data = sanitize(request);
        Map<String, String> errors = new HashMap<>();
        OnlineProtectorApplication application = getApplication(request);
        errors = validateInsuredGender(data, errors);
        errors = validateInsuredNationality(data, errors);
        errors = validateInsuredMultipleNationalities(data, errors);
        if (data.get("insured_multiple_nationalities") != null && ((boolean)data.get("insured_multiple_nationalities"))) {
        errors = validateInsuredNationality2(data, errors);
        errors = validateInsuredNationality3(data, errors);
        }
        //  fixed validations as a part of JIRA HSBCIDCU-978
        if (application.insuredIsSingaporean()|| application.insuredIsPermanentResident()) {
          errors = validateInsuredNric(data, errors);
        } else {
          errors = validateInsuredPassport(data, errors);
        }
       
        errors = validateInsuredBirthCountry(data, errors);

        request.setAttribute("data", data);
        request.setAttribute("errors", errors);

        return (errors.size() == 0);
    }

    public boolean process(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (Map<String, Object>) request.getAttribute("data");

        application.setInsuredGender(data.get("insured_gender").toString());
        application.setInsuredNationality(data.get("insured_nationality").toString());
        application.setInsuredNationality2(data.get("insured_nationality2").toString());
        application.setInsuredNationality3(data.get("insured_nationality3").toString());
        application.setInsuredNric(data.get("insured_nric").toString());
        application.setMaxStep(getCurrentStep() + 0.9f);
        application.setInsuredBirthCountry(data.get("insured_birth_country").toString());
        System.out.println(" application.setInsuredBirthCountry "+data.get("insured_birth_country").toString());
        System.out.println(application.getInsuredBirthCountry());
        if (data.get("insured_multiple_nationalities") != null) {
            application.setInsuredMultipleNationalities(data.get("insured_multiple_nationalities").toString().equals("true"));
        }
        return application.save();
    }

    private Map<String, String> validateInsuredGender(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_gender"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_gender", "Your spouse's gender is missing or invalid.");
                break;
        }

        return errors;
    }

    private Map<String, String> validateInsuredNationality(Map<String, Object> data, Map<String, String> errors) {
        String input = data.get("insured_nationality").toString();
        boolean hasError = false;

        switch (Validator.validateString(input)) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_nationality", "Your spouse's nationality is missing or invalid.");
                hasError = true;
                break;
        }

        if (!hasError) {
            if (input.equalsIgnoreCase("NA")) {
                errors.put("insured_nationality", "Your spouse's nationality is missing or invalid.");
                errors.put("dropout", "1");
            }
        }

        return errors;
    }

    private Map<String, String> validateInsuredMultipleNationalities(Map<String, Object> data, Map<String, String> errors) {
        switch (Validator.validateString(data.get("insured_multiple_nationalities"))) {
            case Validator.ERROR_STRING_EMPTY:
                errors.put("insured_multiple_nationalities", "Please indicate if your spouse has multiple nationalities.");
                break;
        }

        return errors;
    }
    
    /**
     * This method checks the request object (sanitized hashmap) for element 'insured_nationality2' to not be empty and
     * to be different from element 'insured_nationality'.
     * if not, then error messages will added to the error hashmap.
     * 
     * @param data    Hashmap of santized http request data. 
     * @param errors  Hashmap of error messages to be displayed to the end user.
     * @return        Updated Hashmap of error messages to be displayed to the end user.   
     */

    private Map<String, String> validateInsuredNationality2(Map<String, Object> data, Map<String, String> errors) {
        String input = data.get("insured_nationality2").toString();
        boolean hasError = false;

        if (data.get("insured_multiple_nationalities").toString().equalsIgnoreCase("true")) {
            switch (Validator.validateString(data.get("insured_nationality2"))) {
                case Validator.ERROR_STRING_EMPTY:
                    errors.put("insured_nationality2", "Your spouse's nationality is missing or invalid.");
                    hasError = true;
                    break;
            }
            String mainNationality = data.get("insured_nationality").toString();// checking for duplicate nationality JIRA HSBCIDCU-963
            String thirdNationality = data.get("insured_nationality3").toString();
            if (StringUtils.isNotBlank(mainNationality)&& StringUtils.isNotBlank(input)
                &&Validator.isUniqueNationality(mainNationality, input)){
                errors.put("insured_nationality2",
                    "Your nationality selected cannot be same as above.");
                hasError = true;
            }
            if (!hasError) {
                if (input.equalsIgnoreCase("NA")) {
                    errors.put("insured_nationality2", "We are unable to proceed with your online application.");
                    errors.put("dropout", "1");
                }
            }
        }

        return errors;
    }

    /**
     * This method checks the request object (sanitized hashmap) for element 'insured_nationality3' to be not empty and
     * to be different from element 'insured_nationality' and 'insured_nationality2' .
     * if not, then error messages will added to the error hashmap.
     * 
     * @param data    Hashmap of santized http request data. 
     * @param errors  Hashmap of error messages to be displayed to the end user.
     * @return        Updated Hashmap of error messages to be displayed to the end user.   
     */
    private Map<String, String> validateInsuredNationality3(Map<String, Object> data,
        Map<String, String> errors) {
      String input = data.get("insured_nationality3").toString();
      if (data.get("insured_multiple_nationalities").toString().equalsIgnoreCase("true")) {
      String mainNationality = data.get("insured_nationality").toString(); // checking for duplicate nationality JIRA HSBCIDCU-963
      String secondNationality = data.get("insured_nationality2").toString();
      if ((StringUtils.isNotBlank(mainNationality) && StringUtils.isNotBlank(input)
          && Validator.isUniqueNationality(mainNationality, input))
          ||(StringUtils.isNotBlank(secondNationality) && StringUtils.isNotBlank(input)
          && Validator.isUniqueNationality(secondNationality, input))){
        errors.put("insured_nationality3",
            "Your nationality selected cannot be same as above.");
      }
      }
      return errors;
    }
   
    /**
     * This method validates INSURED_NRIC from sanitized Hashmap for Nric rules and 
     * populates the error Hashmap with appropriate error messages.
     * 
     * @param data    Hashmap of santized http request data.
     * @param errors  Hashmap of error messages.
     * @return        Updated Hashmap of error messages 
     */
    
    private Map<String, String> validateInsuredNric(Map<String, Object> data, Map<String, String> errors) { //JIRA 625
      switch (Validator.validateNric(data.get(FieldConstants.INSURED_NRIC))) {
          case Validator.ERROR_STRING_EMPTY:
              errors.put(FieldConstants.INSURED_NRIC, "Your NRIC number is missing.");
              break;
          case Validator.ERROR_NRIC_FORMAT_INVALID:
              errors.put(FieldConstants.INSURED_NRIC, "Please enter a valid NRIC number.");            
              break;
      }

      return errors;
  }
    /**
     * This method validates INSURED_NRIC from sanitized Hashmap for passport rules and 
     * populates the error Hashmap with appropriate error messages.
     * 
     * @param data    Hashmap of santized http request data.
     * @param errors  Hashmap of error messages.
     * @return        Updated Hashmap of error messages 
     */
   
    private Map<String, String> validateInsuredPassport(Map<String, Object> data, Map<String, String> errors) { //JIRA 625
      switch (Validator.validatePassport(data.get(FieldConstants.INSURED_NRIC))) {
      
          case Validator.ERROR_STRING_EMPTY:
              errors.put(FieldConstants.INSURED_PASSPORT, "Your passport number is missing.");
              break;
          case Validator.ERROR_STRING_TOO_LONG:
              errors.put(FieldConstants.INSURED_PASSPORT, "Please enter a valid Passport number.");
              break;
          case Validator.ERROR_PASSPORT_FORMAT_INVALID:
              errors.put(FieldConstants.INSURED_PASSPORT, "Please enter a valid Passport number.");
              break;
             }
      return errors;
    }
     
     /** This method checks the http request object (sanitized hashmap) for the existence of the element insured_birth_country.
     * if the element does not exist, an error will be added to a map of error messages.
     *      
     * @param data     Hashmap of santized http request data. 
     * @param errors   Hashmap of error messages to be displayed to the end user.
     * @return         Updated Hashmap of error messages to be displayed to the end user.  
     */
    private Map<String, String> validateInsuredBirthCountry(Map<String, Object> data, Map<String, String> errors) {
      switch (Validator.validateString(data.get("insured_birth_country"))) {
          case Validator.ERROR_STRING_EMPTY:
              errors.put("insured_birth_country", "Your country of birth is missing or invalid.");
              break;
      }

      return errors;
  }
   



    private Map<String, Object> sanitizeInsuredGender(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_gender");
        String[] options = new String[] { "M", "F" };
        if (!Arrays.asList(options).contains(input)) {
            input = null;
        }

        sanitized.put("insured_gender", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredNationality(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_nationality");

        sanitized.put("insured_nationality", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredMultipleNationalities(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_multiple_nationalities");
        if (input != null) {
            sanitized.put("insured_multiple_nationalities", input.equals("Y"));
        } else {
            sanitized.put("insured_multiple_nationalities", null);
        }

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredNationality2(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_nationality2");

        sanitized.put("insured_nationality2", input);

        return sanitized;
    }

    private Map<String, Object> sanitizeInsuredNationality3(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_nationality3");

        sanitized.put("insured_nationality3", input);

        return sanitized;
    }

    /**
     * This method fetches the HttpServlet request element 'applicant_birth_country'
     * and updates the Hashmap of sanitized data.
     * 
     * @param request   HttpServletRequest request
     * @param sanitized Hashmap of santized http request data. 
     * @return          Updated HashMap of sanitized data.
     */
    private Map<String, Object> sanitizeInsuredBirthCountry(HttpServletRequest request, Map<String, Object> sanitized) {
      String input = request.getParameter("insured_birth_country");

      sanitized.put("insured_birth_country", input);

      return sanitized;
  }
    private Map<String, Object> sanitizeInsuredNric(HttpServletRequest request, Map<String, Object> sanitized) {
        String input = request.getParameter("insured_nric");

        sanitized.put("insured_nric", input);

        return sanitized;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        OnlineProtectorApplication application = getApplication(request);
        Map<String, Object> data = (HashMap)request.getAttribute("data");

        data.put("insured_gender", application.getInsuredGender());
        data.put("insured_nationality", application.getInsuredNationality());
        data.put("insured_multiple_nationalities", application.getInsuredMultipleNationalities());
        data.put("insured_nationality2", application.getInsuredNationality2());
        data.put("insured_nationality3", application.getInsuredNationality3());
        data.put("insured_nric", application.getInsuredNric());
        data.put("insured_birth_country", application.getInsuredBirthCountry());// added birth country as a part of HSBCIDCU-892

        request.setAttribute("data", data);
    }

}
