package sg.com.hsbc.dreamcatcher.faq;

import sg.com.hsbc.dreamcatcher.BaseServlet;
import sg.com.hsbc.dreamcatcher.helpers.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/faq/search", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/faq/search.jsp")
})
public class SearchServlet extends BaseServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String keywords = request.getParameter("term");
        String query = "SELECT id, question, answer FROM faq WHERE MATCH(question) AGAINST (? IN NATURAL LANGUAGE MODE)";

        Database db = new Database();
        Connection conn = db.getConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, keywords);
            rs = pstmt.executeQuery();
            ArrayList<Map<String, String>> results = new ArrayList<>();
            while(rs.next()) {
                Map hash = new HashMap();
                hash.put("question", rs.getString("question"));
                hash.put("answer", rs.getString("answer"));
                results.add(hash);
            }
            request.setAttribute("faqs", results);
        } catch (SQLException e) {
            ErrorHandler.handleError(this, e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }finally {
            db.closeAll(conn,pstmt, rs);
        }
        render(request, response);
    }
}
