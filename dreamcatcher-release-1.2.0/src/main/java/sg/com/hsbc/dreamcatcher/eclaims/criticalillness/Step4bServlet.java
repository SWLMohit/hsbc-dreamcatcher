package sg.com.hsbc.dreamcatcher.eclaims.criticalillness;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import sg.com.hsbc.dreamcatcher.eclaims.EClaimApplicationJob;

@WebServlet(urlPatterns = "/eclaims/3-critical-illness/4a", initParams = {
        @WebInitParam(name = "template", value = "/WEB-INF/eclaims/3_critical_illness/step4b.jsp")
}, asyncSupported = true)
public class Step4bServlet extends BaseServlet {
    public float getCurrentStep() {
        return (float) 4.5;
    }

    public Map<String, Object> sanitize(HttpServletRequest request) {
        Map<String, Object> sanitized = new HashMap<>();
        return sanitized;
    }

    public boolean process(HttpServletRequest request) {
        return true;
    }

    public void populateDataFromObject(HttpServletRequest request) {
        CriticalIllnessApplication application = (CriticalIllnessApplication) getApplication(request);
        Map<String, Object> data = (HashMap) request.getAttribute("data");

        data.put("referenceNumber", application.getReferenceNumber());
        data.put("fileSizeError", application.fileSizeExceeded()); //10MB message
        request.setAttribute("data", data);

        removeFromSession(request);

        // NB see comments in EClaimApplicationJob
        ExecutorService threadPool = (ExecutorService) getServletConfig().getServletContext().getAttribute("threadPool");
        threadPool.execute(new EClaimApplicationJob(application));
    }
}
