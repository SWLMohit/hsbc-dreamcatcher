# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Database

You need MySQL 5.6 or later.

On Debian you need to install the MySQL community version, follow steps [here](http://www.debiantutorials.com/install-mysql-server-5-6-debian-7-8/).
Also the GPG key for dev.mysql.com has expired, update it using [these instructions](https://linux-audit.com/how-to-solve-an-expired-key-keyexpired-with-apt/).
You will also need to run the MySQL upgrade if you had a previous version of MySQL installed. Run the following command:
mysql_upgrade -u root -p --force
and RESTART mysql.

Use the root password "letmein".

The database name is "dreamdev". Create the database from a dump file or as follows:
~~~~
mysql -u root -p
CREATE DATABASE dreamdev;
QUIT
mysql -u root -p dreamdev < src/main/db/data.sql
~~~~
Additional SQL files that may be necessary for development are in the src/main/db directory.

Create a named resource under META-INF/context.xml or server.xml
~~~~
<Resource name="jdbc/dreamcatcher" auth="Container" type="javax.sql.DataSource" username="root" password="letmein" driverClassName="com.mysql.jdbc.Driver" url="jdbc:mysql://127.0.0.1/dreamdev" maxActive="15" maxIdle="3"/>
~~~~

### Building ###

You need Apache Maven 3.5.0 or later. In the project directory run
~~~~
mvn package
~~~~

### Running ###

Use a recent version of Tomcat (8.5 is good). The web application is built into target/dreamcatcher or you can use target/dreamcatcher.war

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: [Sarita Dubey](mailto:sarita.dubey@mirumagency.com)
